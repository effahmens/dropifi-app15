function showProgress() {
    jQuery(".dropifi_submit_input").append('<div id="dropifi_widget_progress"><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/widget/images/ajax-loader.gif" alt="" /> </div>')
}

function showProgressTitle(e) {
    jQuery(".dropifi_submit_input").append('<div id="dropifi_widget_progress"><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/widget/images/ajax-loader.gif" alt="" /> ' + e + "</div>")
}

function hideProgress() {
    jQuery("#dropifi_widget_progress").remove()
}

function showProgressButton(e) {
    jQuery("#sendMail").css({
        display: "none"
    });
    jQuery("#sendMail").attr("disabled", "disabled");
    showProgressTitle(e)
}

function hideProgressButton() {
    jQuery("#sendMail").css({
        display: "inherit"
    });
    jQuery("#sendMail").removeAttr("disabled");
    hideProgress()
}

function IsEmail(e) {
    var t = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return t.test(e)
}

function checkEmailDomain(e, t, n) {
    var r = jQuery(t);
    var i = jQuery(e);
    var s = jQuery(t);
    var o = false;
    i.mailcheck({
        suggested: function(e, t) {
            var t = "Oops!, did you mean <span class='dropifiWidget2012_suggestion' style='color:brown;cursor:pointer' title='click to add'>" + t.address + "@" + t.domain + "</span>?";
            s.html(t);
            o = true
        }
    });
    var u = jQuery(".dropifiWidget2012_suggestion");
    if (u != undefined) {
        jQuery(".dropifiWidget2012_suggestion").click(function() {
            var e = jQuery(this).text();
            i.val(e);
            jQuery(t).html("")
        })
    }
    return o
}
var CreateMessage = {
    init: function() {
        this.fileName = null;
        this.fileSize = null;
        this.fileExtension = null;
        this.isBlockedFile = false
    },
    isSizeAllowed: function() {
        if (this.fileSize && this.getFileSize(this.fileSize).megabytes > 25) {
            return false
        }
        return true
    },
    getFileSize: function(e) {
        var t = {};
        if (e) {
            var n = e;
            var r = n / 1024;
            var i = r / 1024;
            var s = i / 1024;
            var o = s / 1024;
            var u = o / 1024;
            var a = u / 1024;
            var f = a / 1024;
            var l = f / 1024;
            t.bytes = n;
            t.kilobytes = r;
            t.megabytes = i;
            t.gigabytes = s;
            t.terabytes = o;
            t.petabytes = u;
            t.exabytes = a;
            t.zettabytes = f;
            t.yottabytes = l
        }
        return t
    },
    setupFileUpload: function() {
        jQuery("#clearfile").click(function() {
            var e = jQuery("#fileContent");
            e.html(e.html());
            jQuery("#widgetField_attachment").bind("change", function() {
                CreateMessage.afterFileChange(this)
            })
        });
        var e = {
            target: "",
            beforeSubmit: function() {
                var e = false;
                var t = "";
                var n = document.getElementById("widgetField_name");
                var r = document.getElementById("widgetField_email");
                var i = document.getElementById("widgetField_phone");
                var s = document.getElementById("widgetField_subject");
                var o = document.getElementById("widgetField_message");
                var e = false;
                if (n != "null" && n != null && jQuery.trim(n.value) == "") {
                    var u = jQuery("#widgetError_name").val();
                    if (u == "null" || jQuery.trim(u).length < 1) {
                        u = "* your name is required"
                    }
                    t += "<p>* " + u + "</p>";
                    e = true
                }
                if (r != "null" && r != null && jQuery.trim(r.value) == "") {
                    var a = jQuery("#widgetError_email").val();
                    if (a == "null" || jQuery.trim(a).length < 1) {
                        a = "your email is required"
                    }
                    t += "<p>* " + a + "</p>";
                    e = true
                } else if (IsEmail(r.value) == false) {
                    if (checkEmailDomain("#widgetField_email", "#widgetError_email", "#widgetError_email") == true) {
                        t += "<p>* your email format is not correct</p>"
                    }
                    e = true
                }
                if (s != "null" && s != null && jQuery.trim(s.value) == "") {
                    var f = jQuery("#widgetError_subject").val();
                    if (f == "null" || jQuery.trim(f).length < 1) {
                        f = "the subject of the message is required"
                    }
                    t += "<p>* " + f + "</p>";
                    e = true
                }
                if (o != "null" && o != null && jQuery.trim(o.value) == "") {
                    var l = jQuery("#widgetError_message").val();
                    if (l == "null" || jQuery.trim(l).length < 1) {
                        l = "your message is required"
                    }
                    t += "<p>* " + l + "</p>";
                    e = true
                }
                var c = jQuery("#widgetField_attachment");
                if (c && c.val()) {
                    jQuery("#fileType").val(CreateMessage.fileType + " ");
                    if (CreateMessage.isBlockedFile) {
                        t += "<p>* the selected file cannot be uploaded</p>";
                        e = true
                    } else if (!CreateMessage.isSizeAllowed()) {
                        t += "<p>* the selected file size is more than 25MB</p>";
                        e = true
                    }
                }
                var h = jQuery("#captchaResult");
                var p = Captcha;
                if (typeof h != "undefined" && typeof h != undefined && typeof p != "undefined" && typeof p != undefined) {
                    if (Captcha.code != jQuery.trim(h.val())) {
                        t += "* the answer you entered is not correct";
                        e = true
                    }
                }
                if (e) {
                    jQuery(".message-body").html(t + " ");
                    jQuery("#dropifiModal").modal();
                    return false
                }
                showProgressButton("")
            },
            success: function(e) {
                if (e) {
                    var e = JSON.parse(e);
                    if (e.callback.status != 200) {
                        if (e.Captcha != undefined) {
                            Captcha = e.Captcha;
                            jQuery("#captchaText").html(Captcha.query + "");
                            jQuery("#captchaResult").val("");
                            jQuery("#captchaCode").val(Captcha.accessCode)
                        }
                        jQuery(".message-body").html(e.callback.error + " ");
                        jQuery("#dropifiModal").modal()
                    } else {
                        var t = e.callback.message;
                        if (t == null || t == "null" || t == undefined) {
                            t = "Thank you."
                        }
                        jQuery("#contentInner").html("<div style='min-height:200px'>" + t + "</div>");
                        CreateMessage.changeHeight(true);
                        setTimeout(function() {
                            CreateMessage.closeWindow(true)
                        }, 3500);
                        _gaq.push(["_trackEvent", Dropifi.hostUrl, "Message Sent Successful"])
                    }
                } else {
                    jQuery(".message-body").html("Data not submitted ");
                    jQuery("#dropifiModal").modal()
                }
                hideProgressButton()
            }
        };
        jQuery("#fileupload").ajaxForm(e);
        jQuery("#widgetField_attachment").bind("change", function() {
            CreateMessage.afterFileChange(this)
        })
    },
    afterFileChange: function(e) {
        try {
            CreateMessage.isBlockedFile = false;
            if (e && e.files && e.files[0] && e.files[0].size) {
                CreateMessage.fileSize = e.files[0].size;
                CreateMessage.fileType = e.files[0].type
            }
            CreateMessage.fileName = jQuery("#widgetField_attachment").val();
            if (CreateMessage.fileName) {
                CreateMessage.fileExtension = CreateMessage.fileName.substr(CreateMessage.fileName.lastIndexOf(".") + 1);
                CreateMessage.isBlockedFile = CreateMessage.isBlockedAttachFile(CreateMessage.fileExtension)
            }
        } catch (t) {}
    },
    isBlockedAttachFile: function(e) {
        var t = false;
        if (e) {
            var n = "ade,adp,bat,chm,cmd,com,cpl,exe,hta,ins,isp,jse,lib,mde,msc,msp,mst,pif,scr,sct,shb,sys,vb,vbe,vbs,vxd,wsc,wsf,wsh".split(",");
            jQuery.each(n, function(n, r) {
                if (jQuery.trim(r) == jQuery.trim(e).toLowerCase()) {
                    t = true
                }
            })
        }
        return t
    },
    changeHeight: function(e) {
        var t = document.body.clientHeight || document.body.offsetHeight || document.body.scrollHeight;
        var n = {
            type: "changeHeight",
            height: t,
            heightOffset: "20",
            isReloadContent: e
        };
        socket.postMessage(JSON.stringify(n))
    },
    closeWindow: function(e) {
        var t = document.body.clientHeight || document.body.offsetHeight || document.body.scrollHeight;
        var n = {
            type: "closeWindow",
            height: t,
            heightOffset: "20",
            isReloadContent: e
        };
        socket.postMessage(JSON.stringify(n))
    },
    dropifiSerializeObject: function(e) {
        var t = {};
        var n = jQuery("#" + e).serializeArray();
        if (n && n.length > 1) {
            jQuery.each(n, function() {
                if (t[this.name] !== undefined) {
                    if (!t[this.name].push) {
                        t[this.name] = [t[this.name]]
                    }
                    t[this.name].push(this.value || "")
                } else {
                    t[this.name] = this.value || ""
                }
                alert(this.name + " " + this.value)
            })
        }
        return t
    }
};
var socket = new easyXDM.Socket({
    swf: "https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/widget/images/easyxdm.swf",
    onReady: function() {
        iframe = document.createElement("iframe");
        iframe.src = easyXDM.query.url
    },
    onMessage: function(e, t) {
        iframe.src = e
    },
    onHeightChange: function(e) {}
});
jQuery(document).ready(function() {
    jQuery("#captchaText").html(Dropifi.captchaText);
    jQuery("#captchaCode").val(Dropifi.captchaCode);
    jQuery("#attachmentId").val(Dropifi.attachmentId);
    jQuery("#pageUrl").val(Dropifi.pageUrl);
    jQuery("#widgetField_phone").keypress(function(e) {
        if (e.which == 45 || e.which == 43 || e.which == 8 || e.which == 0 || e.which >= 48 && e.which <= 57) {
            var t = jQuery(this).val();
            if (e.which == 43 && jQuery.trim(t) != "") {
                e.preventDefault()
            }
            if (e.which == 43 && t.indexOf("+") != -1) {
                e.preventDefault()
            }
        } else {
            e.preventDefault()
        }
    });
    jQuery("#captchaResult").keypress(function(e) {
        if (e.which == 45 || e.which == 8 || e.which == 0 || e.which >= 48 && e.which <= 57) {
            var t = jQuery(this).val();
            if (e.which == 45 && jQuery.trim(t) != "") {
                e.preventDefault()
            }
            if (e.which == 45 && t.indexOf("-") != -1) {
                e.preventDefault()
            }
        } else {
            e.preventDefault()
        }
    });
    var e = new Number(Dropifi.width);
    var t = new Number(Dropifi.height);
    var n = $("#contentInner");
    if (t <= 380 && e <= 330) {
        var r = t - n.height();
        if (r <= 30) {
            var i = document.getElementsByClassName("input-group");
            var s = i.length;
            if (s > 1) {
                jQuery(".input-group").css({
                    width: "50%",
                    "float": "left"
                });
                var o;
                for (o = 0; o < i.length; o++) {
                    if (o + 1 == s && s % 2 == 1) {
                        i[o].style.width = "inherit";
                        i[o].style.cssFloat = "none"
                    } else if (o + 1 == s) {
                        i[o].style.cssFloat = "none"
                    }
                }
            }
        }
    }

    CreateMessage.setupFileUpload();
    data.height = document.body.clientHeight || document.body.offsetHeight || document.body.scrollHeight; 
    jQuery.get("https://api.dropifi.com/jsonip/index.php", function(e) {
        jQuery("#ipAddress").val(e.ip)
    });
    jQuery("input, textarea").placeholder();
    _gaq.push(["_trackEvent", Dropifi.hostUrl, "Page View"]);

    socket.postMessage(JSON.stringify(data));
    setTimeout(function() {
        data.height = document.body.clientHeight || document.body.offsetHeight || document.body.scrollHeight;
        data.type = "changeHeight";
        data.hideOnLoad = true;
        data.showWidget = false;
        data.time = 3e3;
        socket.postMessage(JSON.stringify(data));
    }, 100);
})