/***
 * Dropifi Production Javascript
****/
;(function(e,t,n,r,i,s){function w(e,t){var n=typeof e[t];return n=="function"||!!(n=="object"&&e[t])||n=="unknown"}function E(e,t){return!!(typeof e[t]=="object"&&e[t])}function S(e){return Object.prototype.toString.call(e)==="[object Array]"}function x(){var e="Shockwave Flash",t="application/x-shockwave-flash";if(!q(navigator.plugins)&&typeof navigator.plugins[e]=="object"){var n=navigator.plugins[e].description;if(n&&!q(navigator.mimeTypes)&&navigator.mimeTypes[t]&&navigator.mimeTypes[t].enabledPlugin){y=n.match(/\d+/g)}}if(!y){var r;try{r=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");y=Array.prototype.slice.call(r.GetVariable("$version").match(/(\d+),(\d+),(\d+),(\d+)/),1);r=null}catch(i){}}if(!y){return false}var s=parseInt(y[0],10),o=parseInt(y[1],10);b=s>9&&o>0;return true}function A(){if(C){return}C=true;for(var e=0;e<k.length;e++){k[e]()}k.length=0}function M(e,t){if(C){e.call(t);return}k.push(function(){e.call(t)})}function _(){var e=parent;if(h!==""){for(var t=0,n=h.split(".");t<n.length;t++){e=e[n[t]]}}return e.easyXDM}function D(t){e.easyXDM=d;h=t;if(h){v="easyXDM_"+h.replace(".","_")+"_"}return p}function P(e){return e.match(f)[3]}function H(e){return e.match(f)[4]||""}function B(e){var t=e.toLowerCase().match(f);var n=t[2],r=t[3],i=t[4]||"";if(n=="http:"&&i==":80"||n=="https:"&&i==":443"){i=""}return n+"//"+r+i}function j(e){e=e.replace(c,"$1/");if(!e.match(/^(http||https):\/\//)){var t=e.substring(0,1)==="/"?"":n.pathname;if(t.substring(t.length-1)!=="/"){t=t.substring(0,t.lastIndexOf("/")+1)}e=n.protocol+"//"+n.host+t+e}while(l.test(e)){e=e.replace(l,"")}return e}function F(e,t){var n="",r=e.indexOf("#");if(r!==-1){n=e.substring(r);e=e.substring(0,r)}var i=[];for(var o in t){if(t.hasOwnProperty(o)){i.push(o+"="+s(t[o]))}}return e+(g?"#":e.indexOf("?")==-1?"?":"&")+i.join("&")+n}function q(e){return typeof e==="undefined"}function U(e,t,n){var r;for(var i in t){if(t.hasOwnProperty(i)){if(i in e){r=t[i];if(typeof r==="object"){U(e[i],r,n)}else if(!n){e[i]=t[i]}}else{e[i]=t[i]}}}return e}function z(){var e=t.body.appendChild(t.createElement("form")),n=e.appendChild(t.createElement("input"));n.name=v+"TEST"+u;m=n!==e.elements[n.name];t.body.removeChild(e)}function W(e){if(q(m)){z()}var n;if(m){n=t.createElement('<iframe name="'+e.props.name+'"/>')}else{n=t.createElement("IFRAME");n.name=e.props.name}n.id=n.name=e.props.name;delete e.props.name;if(typeof e.container=="string"){e.container=t.getElementById(e.container)}if(!e.container){U(n.style,{position:"absolute",top:"-2000px",left:"0px"});e.container=t.body}var r=e.props.src;e.props.src="javascript:false";U(n,e.props);n.border=n.frameBorder=0;n.allowTransparency=true;e.container.appendChild(n);if(e.onLoad){T(n,"load",e.onLoad)}if(e.usePost){var i=e.container.appendChild(t.createElement("form")),s;i.target=n.name;i.action=r;i.method="POST";if(typeof e.usePost==="object"){for(var o in e.usePost){if(e.usePost.hasOwnProperty(o)){if(m){s=t.createElement('<input name="'+o+'"/>')}else{s=t.createElement("INPUT");s.name=o}s.value=e.usePost[o];i.appendChild(s)}}}i.submit();i.parentNode.removeChild(i)}else{n.src=r}e.props.src=r;return n}function X(e,t){if(typeof e=="string"){e=[e]}var n,r=e.length;while(r--){n=e[r];n=new RegExp(n.substr(0,1)=="^"?n:"^"+n.replace(/(\*)/g,".$1").replace(/\?/g,".")+"$");if(n.test(t)){return true}}return false}function V(r){var i=r.protocol,s;r.isHost=r.isHost||q(I.xdm_p);g=r.hash||false;if(!r.props){r.props={}}if(!r.isHost){r.channel=I.xdm_c.replace(/["'<>\\]/g,"");r.secret=I.xdm_s;r.remote=I.xdm_e.replace(/["'<>\\]/g,"");i=I.xdm_p;if(r.acl&&!X(r.acl,r.remote)){throw new Error("Access denied for "+r.remote)}}else{r.remote=j(r.remote);r.channel=r.channel||"default"+u++;r.secret=Math.random().toString(16).substring(2);if(q(i)){if(B(n.href)==B(r.remote)){i="4"}else if(w(e,"postMessage")||w(t,"postMessage")){i="1"}else if(r.swf&&w(e,"ActiveXObject")&&x()){i="6"}else if(navigator.product==="Gecko"&&"frameElement"in e&&navigator.userAgent.indexOf("WebKit")==-1){i="5"}else if(r.remoteHelper){i="2"}else{i="0"}}}r.protocol=i;switch(i){case"0":U(r,{interval:100,delay:2e3,useResize:true,useParent:false,usePolling:false},true);if(r.isHost){if(!r.local){var o=n.protocol+"//"+n.host,a=t.body.getElementsByTagName("img"),f;var l=a.length;while(l--){f=a[l];if(f.src.substring(0,o.length)===o){r.local=f.src;break}}if(!r.local){r.local=e}}var c={xdm_c:r.channel,xdm_p:0};if(r.local===e){r.usePolling=true;r.useParent=true;r.local=n.protocol+"//"+n.host+n.pathname+n.search;c.xdm_e=r.local;c.xdm_pa=1}else{c.xdm_e=j(r.local)}if(r.container){r.useResize=false;c.xdm_po=1}r.remote=F(r.remote,c)}else{U(r,{channel:I.xdm_c,remote:I.xdm_e,useParent:!q(I.xdm_pa),usePolling:!q(I.xdm_po),useResize:r.useParent?false:r.useResize})}s=[new p.stack.HashTransport(r),new p.stack.ReliableBehavior({}),new p.stack.QueueBehavior({encode:true,maxLength:4e3-r.remote.length}),new p.stack.VerifyBehavior({initiate:r.isHost})];break;case"1":s=[new p.stack.PostMessageTransport(r)];break;case"2":r.remoteHelper=j(r.remoteHelper);s=[new p.stack.NameTransport(r),new p.stack.QueueBehavior,new p.stack.VerifyBehavior({initiate:r.isHost})];break;case"3":s=[new p.stack.NixTransport(r)];break;case"4":s=[new p.stack.SameOriginTransport(r)];break;case"5":s=[new p.stack.FrameElementTransport(r)];break;case"6":if(!y){x()}s=[new p.stack.FlashTransport(r)];break}s.push(new p.stack.QueueBehavior({lazy:r.lazy,remove:true}));return s}function $(e){var t,n={incoming:function(e,t){this.up.incoming(e,t)},outgoing:function(e,t){this.down.outgoing(e,t)},callback:function(e){this.up.callback(e)},init:function(){this.down.init()},destroy:function(){this.down.destroy()}};for(var r=0,i=e.length;r<i;r++){t=e[r];U(t,n,true);if(r!==0){t.down=e[r-1]}if(r!==i-1){t.up=e[r+1]}}return t}function J(e){e.up.down=e.down;e.down.up=e.up;e.up=e.down=null}var o=this;var u=Math.floor(Math.random()*1e4);var a=Function.prototype;var f=/^((http.?:)\/\/([^:\/\s]+)(:\d+)*)/;var l=/[\-\w]+\/\.\.\//;var c=/([^:])\/\//g;var h="";var p={};var d=e.easyXDM;var v="easyXDM_";var m;var g=false;var y;var b;var T,N;if(w(e,"addEventListener")){T=function(e,t,n){e.addEventListener(t,n,false)};N=function(e,t,n){e.removeEventListener(t,n,false)}}else if(w(e,"attachEvent")){T=function(e,t,n){e.attachEvent("on"+t,n)};N=function(e,t,n){e.detachEvent("on"+t,n)}}else{throw new Error("Browser not supported")}var C=false,k=[],L;if("readyState"in t){L=t.readyState;C=L=="complete"||~navigator.userAgent.indexOf("AppleWebKit/")&&(L=="loaded"||L=="interactive")}else{C=!!t.body}if(!C){if(w(e,"addEventListener")){T(t,"DOMContentLoaded",A)}else{T(t,"readystatechange",function(){if(t.readyState=="complete"){A()}});if(t.documentElement.doScroll&&e===top){var O=function(){if(C){return}try{t.documentElement.doScroll("left")}catch(e){r(O,1);return}A()};O()}}T(e,"load",A)}var I=function(e){e=e.substring(1).split("&");var t={},n,r=e.length;while(r--){n=e[r].split("=");t[n[0]]=i(n[1])}return t}(/xdm_e=/.test(n.search)?n.search:n.hash);var R=function(){var e={};var t={a:[1,2,3]},n='{"a":[1,2,3]}';if(typeof JSON!="undefined"&&typeof JSON.stringify==="function"&&JSON.stringify(t).replace(/\s/g,"")===n){return JSON}if(Object.toJSON){if(Object.toJSON(t).replace(/\s/g,"")===n){e.stringify=Object.toJSON}}if(typeof String.prototype.evalJSON==="function"){t=n.evalJSON();if(t.a&&t.a.length===3&&t.a[2]===3){e.parse=function(e){return e.evalJSON()}}}if(e.stringify&&e.parse){R=function(){return e};return e}return null};U(p,{version:"2.4.16.3",query:I,stack:{},apply:U,getJSONObject:R,whenReady:M,noConflict:D});p.DomHelper={on:T,un:N,requiresJSON:function(n){if(!E(e,"JSON")){t.write("<"+'script type="text/javascript" src="'+n+'"><'+"/script>")}}};(function(){var e={};p.Fn={set:function(t,n){e[t]=n},get:function(t,n){var r=e[t];if(n){delete e[t]}return r}}})();p.Socket=function(e){var t=$(V(e).concat([{incoming:function(t,n){e.onMessage(t,n)},callback:function(t){if(e.onReady){e.onReady(t)}}}])),n=B(e.remote);this.origin=B(e.remote);this.destroy=function(){t.destroy()};this.postMessage=function(e){t.outgoing(e,n)};t.init()};p.Rpc=function(e,t){if(t.local){for(var n in t.local){if(t.local.hasOwnProperty(n)){var r=t.local[n];if(typeof r==="function"){t.local[n]={method:r}}}}}var i=$(V(e).concat([new p.stack.RpcBehavior(this,t),{callback:function(t){if(e.onReady){e.onReady(t)}}}]));this.origin=B(e.remote);this.destroy=function(){i.destroy()};i.init()};p.stack.SameOriginTransport=function(e){var t,i,s,o;return t={outgoing:function(e,t,n){s(e);if(n){n()}},destroy:function(){if(i){i.parentNode.removeChild(i);i=null}},onDOMReady:function(){o=B(e.remote);if(e.isHost){U(e.props,{src:F(e.remote,{xdm_e:n.protocol+"//"+n.host+n.pathname,xdm_c:e.channel,xdm_p:4}),name:v+e.channel+"_provider"});i=W(e);p.Fn.set(e.channel,function(e){s=e;r(function(){t.up.callback(true)},0);return function(e){t.up.incoming(e,o)}})}else{s=_().Fn.get(e.channel,true)(function(e){t.up.incoming(e,o)});r(function(){t.up.callback(true)},0)}},init:function(){M(t.onDOMReady,t)}}};p.stack.FlashTransport=function(e){function c(e,t){r(function(){i.up.incoming(e,a)},0)}function d(n){var r=e.swf+"?host="+e.isHost;var i="easyXDM_swf_"+Math.floor(Math.random()*1e4);p.Fn.set("flash_loaded"+n.replace(/[\-.]/g,"_"),function(){p.stack.FlashTransport[n].swf=f=l.firstChild;var e=p.stack.FlashTransport[n].queue;for(var t=0;t<e.length;t++){e[t]()}e.length=0});if(e.swfContainer){l=typeof e.swfContainer=="string"?t.getElementById(e.swfContainer):e.swfContainer}else{l=t.createElement("div");U(l.style,b&&e.swfNoThrottle?{height:"20px",width:"20px",position:"fixed",right:0,top:0}:{height:"1px",width:"1px",position:"absolute",overflow:"hidden",right:0,top:0});t.body.appendChild(l)}var s="callback=flash_loaded"+n.replace(/[\-.]/g,"_")+"&proto="+o.location.protocol+"&domain="+P(o.location.href)+"&port="+H(o.location.href)+"&ns="+h;l.innerHTML="<object height='20' width='20' type='application/x-shockwave-flash' id='"+i+"' data='"+r+"'>"+"<param name='allowScriptAccess' value='always'></param>"+"<param name='wmode' value='transparent'>"+"<param name='movie' value='"+r+"'></param>"+"<param name='flashvars' value='"+s+"'></param>"+"<embed type='application/x-shockwave-flash' FlashVars='"+s+"' allowScriptAccess='always' wmode='transparent' src='"+r+"' height='1' width='1'></embed>"+"</object>"}var i,s,u,a,f,l;return i={outgoing:function(t,n,r){f.postMessage(e.channel,t.toString());if(r){r()}},destroy:function(){try{f.destroyChannel(e.channel)}catch(t){}f=null;if(s){s.parentNode.removeChild(s);s=null}},onDOMReady:function(){a=e.remote;p.Fn.set("flash_"+e.channel+"_init",function(){r(function(){i.up.callback(true)})});p.Fn.set("flash_"+e.channel+"_onMessage",c);e.swf=j(e.swf);var t=P(e.swf);var o=function(){p.stack.FlashTransport[t].init=true;f=p.stack.FlashTransport[t].swf;f.createChannel(e.channel,e.secret,B(e.remote),e.isHost);if(e.isHost){if(b&&e.swfNoThrottle){U(e.props,{position:"fixed",right:0,top:0,height:"20px",width:"20px"})}U(e.props,{src:F(e.remote,{xdm_e:B(n.href),xdm_c:e.channel,xdm_p:6,xdm_s:e.secret}),name:v+e.channel+"_provider"});s=W(e)}};if(p.stack.FlashTransport[t]&&p.stack.FlashTransport[t].init){o()}else{if(!p.stack.FlashTransport[t]){p.stack.FlashTransport[t]={queue:[o]};d(t)}else{p.stack.FlashTransport[t].queue.push(o)}}},init:function(){M(i.onDOMReady,i)}}};p.stack.PostMessageTransport=function(t){function a(e){if(e.origin){return B(e.origin)}if(e.uri){return B(e.uri)}if(e.domain){return n.protocol+"//"+e.domain}throw"Unable to retrieve the origin of the event"}function f(e){var n=a(e);if(n==u&&e.data.substring(0,t.channel.length+1)==t.channel+" "){i.up.incoming(e.data.substring(t.channel.length+1),n)}}var i,s,o,u;return i={outgoing:function(e,n,r){o.postMessage(t.channel+" "+e,n||u);if(r){r()}},destroy:function(){N(e,"message",f);if(s){o=null;s.parentNode.removeChild(s);s=null}},onDOMReady:function(){u=B(t.remote);if(t.isHost){var a=function(n){if(n.data==t.channel+"-ready"){o="postMessage"in s.contentWindow?s.contentWindow:s.contentWindow.document;N(e,"message",a);T(e,"message",f);r(function(){i.up.callback(true)},0)}};T(e,"message",a);U(t.props,{src:F(t.remote,{xdm_e:B(n.href),xdm_c:t.channel,xdm_p:1}),name:v+t.channel+"_provider"});s=W(t)}else{T(e,"message",f);o="postMessage"in e.parent?e.parent:e.parent.document;o.postMessage(t.channel+"-ready",u);r(function(){i.up.callback(true)},0)}},init:function(){M(i.onDOMReady,i)}}};p.stack.FrameElementTransport=function(i){var s,o,u,a;return s={outgoing:function(e,t,n){u.call(this,e);if(n){n()}},destroy:function(){if(o){o.parentNode.removeChild(o);o=null}},onDOMReady:function(){a=B(i.remote);if(i.isHost){U(i.props,{src:F(i.remote,{xdm_e:B(n.href),xdm_c:i.channel,xdm_p:5}),name:v+i.channel+"_provider"});o=W(i);o.fn=function(e){delete o.fn;u=e;r(function(){s.up.callback(true)},0);return function(e){s.up.incoming(e,a)}}}else{if(t.referrer&&B(t.referrer)!=I.xdm_e){e.top.location=I.xdm_e}u=e.frameElement.fn(function(e){s.up.incoming(e,a)});s.up.callback(true)}},init:function(){M(s.onDOMReady,s)}}};p.stack.NameTransport=function(e){function l(t){var r=e.remoteHelper+(n?"#_3":"#_2")+e.channel;i.contentWindow.sendMessage(t,r)}function c(){if(n){if(++o===2||!n){t.up.callback(true)}}else{l("ready");t.up.callback(true)}}function h(e){t.up.incoming(e,a)}function d(){if(u){r(function(){u(true)},0)}}var t;var n,i,s,o,u,a,f;return t={outgoing:function(e,t,n){u=n;l(e)},destroy:function(){i.parentNode.removeChild(i);i=null;if(n){s.parentNode.removeChild(s);s=null}},onDOMReady:function(){n=e.isHost;o=0;a=B(e.remote);e.local=j(e.local);if(n){p.Fn.set(e.channel,function(t){if(n&&t==="ready"){p.Fn.set(e.channel,h);c()}});f=F(e.remote,{xdm_e:e.local,xdm_c:e.channel,xdm_p:2});U(e.props,{src:f+"#"+e.channel,name:v+e.channel+"_provider"});s=W(e)}else{e.remoteHelper=e.remote;p.Fn.set(e.channel,h)}var t=function(){var n=i||this;N(n,"load",t);p.Fn.set(e.channel+"_load",d);(function s(){if(typeof n.contentWindow.sendMessage=="function"){c()}else{r(s,50)}})()};i=W({props:{src:e.local+"#_4"+e.channel},onLoad:t})},init:function(){M(t.onDOMReady,t)}}};p.stack.HashTransport=function(t){function d(e){if(!c){return}var n=t.remote+"#"+f++ +"_"+e;(s||!h?c.contentWindow:c).location=n}function m(e){a=e;n.up.incoming(a.substring(a.indexOf("_")+1),p)}function g(){if(!l){return}var e=l.location.href,t="",n=e.indexOf("#");if(n!=-1){t=e.substring(n)}if(t&&t!=a){m(t)}}function y(){o=setInterval(g,u)}var n;var i=this,s,o,u,a,f,l,c;var h,p;return n={outgoing:function(e,t){d(e)},destroy:function(){e.clearInterval(o);if(s||!h){c.parentNode.removeChild(c)}c=null},onDOMReady:function(){s=t.isHost;u=t.interval;a="#"+t.channel;f=0;h=t.useParent;p=B(t.remote);if(s){U(t.props,{src:t.remote,name:v+t.channel+"_provider"});if(h){t.onLoad=function(){l=e;y();n.up.callback(true)}}else{var i=0,o=t.delay/50;(function d(){if(++i>o){throw new Error("Unable to reference listenerwindow")}try{l=c.contentWindow.frames[v+t.channel+"_consumer"]}catch(e){}if(l){y();n.up.callback(true)}else{r(d,50)}})()}c=W(t)}else{l=e;y();if(h){c=parent;n.up.callback(true)}else{U(t,{props:{src:t.remote+"#"+t.channel+new Date,name:v+t.channel+"_consumer"},onLoad:function(){n.up.callback(true)}});c=W(t)}}},init:function(){M(n.onDOMReady,n)}}};p.stack.ReliableBehavior=function(e){var t,n;var r=0,i=0,s="";return t={incoming:function(e,o){var u=e.indexOf("_"),a=e.substring(0,u).split(",");e=e.substring(u+1);if(a[0]==r){s="";if(n){n(true);n=null}}if(e.length>0){t.down.outgoing(a[1]+","+r+"_"+s,o);if(i!=a[1]){i=a[1];t.up.incoming(e,o)}}},outgoing:function(e,o,u){s=e;n=u;t.down.outgoing(i+","+ ++r+"_"+e,o)}}};p.stack.QueueBehavior=function(e){function h(){if(e.remove&&n.length===0){J(t);return}if(o||n.length===0||a){return}o=true;var i=n.shift();t.down.outgoing(i.data,i.origin,function(e){o=false;if(i.callback){r(function(){i.callback(e)},0)}h()})}var t,n=[],o=true,u="",a,f=0,l=false,c=false;return t={init:function(){if(q(e)){e={}}if(e.maxLength){f=e.maxLength;c=true}if(e.lazy){l=true}else{t.down.init()}},callback:function(e){o=false;var n=t.up;h();n.callback(e)},incoming:function(n,r){if(c){var s=n.indexOf("_"),o=parseInt(n.substring(0,s),10);u+=n.substring(s+1);if(o===0){if(e.encode){u=i(u)}t.up.incoming(u,r);u=""}}else{t.up.incoming(n,r)}},outgoing:function(r,i,o){if(e.encode){r=s(r)}var u=[],a;if(c){while(r.length!==0){a=r.substring(0,f);r=r.substring(a.length);u.push(a)}while(a=u.shift()){n.push({data:u.length+"_"+a,origin:i,callback:u.length===0?o:null})}}else{n.push({data:r,origin:i,callback:o})}if(l){t.down.init()}else{h()}},destroy:function(){a=true;t.down.destroy()}}};p.stack.VerifyBehavior=function(e){function s(){n=Math.random().toString(16).substring(2);t.down.outgoing(n)}var t,n,r,i=false;return t={incoming:function(i,o){var u=i.indexOf("_");if(u===-1){if(i===n){t.up.callback(true)}else if(!r){r=i;if(!e.initiate){s()}t.down.outgoing(i)}}else{if(i.substring(0,u)===r){t.up.incoming(i.substring(u+1),o)}}},outgoing:function(e,r,i){t.down.outgoing(n+"_"+e,r,i)},callback:function(t){if(e.initiate){s()}}}};p.stack.RpcBehavior=function(e,t){function o(e){e.jsonrpc="2.0";n.down.outgoing(r.stringify(e))}function u(e,t){var n=Array.prototype.slice;return function(){var r=arguments.length,u,a={method:t};if(r>0&&typeof arguments[r-1]==="function"){if(r>1&&typeof arguments[r-2]==="function"){u={success:arguments[r-2],error:arguments[r-1]};a.params=n.call(arguments,0,r-2)}else{u={success:arguments[r-1]};a.params=n.call(arguments,0,r-1)}s[""+ ++i]=u;a.id=i}else{a.params=n.call(arguments,0)}if(e.namedParams&&a.params.length===1){a.params=a.params[0]}o(a)}}function f(e,t,n,r){if(!n){if(t){o({id:t,error:{code:-32601,message:"Procedure not found."}})}return}var i,s;if(t){i=function(e){i=a;o({id:t,result:e})};s=function(e,n){s=a;var r={id:t,error:{code:-32099,message:e}};if(n){r.error.data=n}o(r)}}else{i=s=a}if(!S(r)){r=[r]}try{var u=n.method.apply(n.scope,r.concat([i,s]));if(!q(u)){i(u)}}catch(f){s(f.message)}}var n,r=t.serializer||R();var i=0,s={};return n={incoming:function(e,n){var i=r.parse(e);if(i.method){if(t.handle){t.handle(i,o)}else{f(i.method,i.id,t.local[i.method],i.params)}}else{var u=s[i.id];if(i.error){if(u.error){u.error(i.error)}}else if(u.success){u.success(i.result)}delete s[i.id]}},init:function(){if(t.remote){for(var r in t.remote){if(t.remote.hasOwnProperty(r)){e[r]=u(t.remote[r],r)}}}n.down.init()},destroy:function(){for(var r in t.remote){if(t.remote.hasOwnProperty(r)&&e.hasOwnProperty(r)){delete e[r]}}n.down.destroy()}}};o.easyXDM=p})(window,document,location,window.setTimeout,decodeURIComponent,encodeURIComponent);
 
var dropifiTzdragg = function(){
    return {
        move : function(divid,xpos,ypos){
            var a = document.getElementById(divid);

            if(a !== null ){
	            document.getElementById(divid).style.left = xpos + 'px';
	            document.getElementById(divid).style.top = ypos + 'px';
        	}
        },
        startMoving : function(evt){  
            evt = evt || window.event;
            var posX = evt.clientX,
                posY = evt.clientY,
            a = document.getElementById('dropifiContactContent'),
            divTop = a.style.top,
            divLeft = a.style.left;
            divTop = divTop.replace('px','');
            divLeft = divLeft.replace('px','');

            var diffX = posX - divLeft,
                diffY = posY - divTop;
            document.onmousemove = function(evt){
                evt = evt || window.event;
                var posX = evt.clientX,
                    posY = evt.clientY,
                    aX = posX - diffX,
                    aY = posY - diffY;
                dropifiTzdragg.move('dropifiContactContent',aX,aY);
            }
        },
        stopMoving : function(){
            var a = document.createElement('script');
            document.onmousemove = function(){}
        },
    }
}();

var JSON = JSON || {};

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
  var t = typeof (obj);
  if (t != "object" || obj === null) {
    // simple data type
    if (t == "string") obj = '"'+obj+'"';
      return String(obj);
  } else {
  // recurse array or object
    var n, v, json = [], arr = (obj && obj.constructor == Array);
    for (n in obj) {
      v = obj[n]; t = typeof(v);
      if (t == "string") v = '"'+v+'"';
      else if (t == "object" && v !== null) v = JSON.stringify(v);
      json.push((arr ? "" : '"' + n + '":') + String(v));
    }
    return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
  }
};

// implement JSON.parse de-serialization
JSON.parse = JSON.parse || function (str) {
  if (str === "") str = '""';
    eval("var p=" + str + ";");
    return p;
};
 
/******Start Widget***************/

var dropifiContentHeight=300;
var iframeHeight;
var dropifiContentPosition="center";
var widgetData ={};

window.onresize = function(event) { 
	DropifiWidgetLoad.repositionWidget(widgetData); 
	var dropifiContactContent = document.getElementById("dropifiContactContent");
	if(dropifiContactContent!==null && dropifiContactContent.offsetWidth>6){ 
		DropifiWidgetLoad.initialWidgetPosition(300,dropifiContentHeight,iframeHeight,dropifiContentPosition);
	}
}


var DropifiWidgetLoad = {

	init: function(){ 
		this.winH = 0;
		this.winW = 0;
		this.isMobile = false;
		this.widgetWid = "390px";
		this.innerWid = "415px";
		this.contactformWid="418px";
		this.contactformHei="280px";
		this.tabSize ="35px";
		this.tabTextSize ="25px";
		this.position = "fixed";
		this.bPercent='110px';
		this.posi='right'; 
		this.isReloadContent=false;
		this.pluginType;
		this.publicKey;
	}, 	
 
	initialContactTab : function(data){
		var w = window.innerWidth;
		var h = window.innerHeight;
		var dropifiContactTab = document.getElementById("dropifiContactTab");

		if(dropifiContactTab !==null){
			dropifiContactTab.style.display="inline-block";
			dropifiContactTab.style.background = "none repeat scroll 0 0 "+data.color;
			var posi =data.position;

			if((posi=="right") || (posi=="left")){ 
				var percent = ((new Number(data.tabPercent))/100)*h;
			 	dropifiContactTab.style.minHeight ="100px";
				dropifiContactTab.style.minWidth ="10px"; 
				dropifiContactTab.style.width ="30px";
				 dropifiContactTab.innerHTML="<div id='dropifi2012_version_inner_label' " +
			 		"style='width:30px;margin-right:auto;margin-left:auto;padding-left:8px;'>"
			 		+"<img id='dropifi_widget_v1_imageText' style='padding:0px;display:inherit !important;border:0px;margin-top:5px;margin-bottom:8px;box-shadow: 0 0 0 transparent;cursor:pointer' src='"+data.imageUrl+"' /></div>";								 
				if(posi=='left'){
					dropifiContactTab.style.left="0px";
					dropifiContactTab.style.borderLeft="0px solid #ffffff";
				}else if(data.position='right'){
					dropifiContactTab.style.right="0px";
					dropifiContactTab.style.borderRight="0px solid #ffffff";
				} 
				dropifiContactTab.style.borderRadius = (posi == "left") ?'0px 3px 3px 0px':'3px 0px 0px 3px'; 
			}else{ 

				dropifiContactTab.style.minHeight ="10px";
				dropifiContactTab.style.minWidth ="100px"; 
				dropifiContactTab.style.height ="30px";
				 dropifiContactTab.innerHTML ="<div id='dropifi2012_version_inner_label' style='padding-left:8px;padding-top:8px;moz-box-sizing: content-box;cursor:pointer'>"+
						 "<img id='dropifi_widget_v1_imageText' style='padding:0px;display:inherit !important;border:0px;box-shadow: 0 0 0 transparent;cursor:pointer' src='"+data.imageUrl+"' /></div>";
				 
				if(posi=='top'){
					dropifiContactTab.style.top="0px";
					dropifiContactTab.style.borderTop="0px solid #ffffff";
				}else if(data.position='bottom'){
					dropifiContactTab.style.bottom="0px";
					dropifiContactTab.style.borderBottom="0px solid #ffffff";
				} 
				dropifiContactTab.style.borderRadius=(posi=='bottom')?'3px 3px 0px 0px':'0px 0px 3px 3px';	  
			}
			DropifiWidgetLoad.repositionWidget(data);  
		}
	},

	repositionWidget :function(data) {  
		var dropifiContactTab = document.getElementById("dropifiContactTab");
		if(dropifiContactTab !== null){
			if((data.position=="right") || (data.position=="left")){  
				dropifiContactTab.style.top = (((new Number(data.tabPercent))/100)*window.innerHeight)+"px";

				//Adjust of the Contact Tab.
				var addTop = dropifiContactTab.offsetTop+ dropifiContactTab.offsetHeight;
				if(addTop>=window.innerHeight){ 
					addTop = (dropifiContactTab.offsetTop - (addTop - window.innerHeight))-5; 
					dropifiContactTab.style.top = addTop+"px";
				}   
			}else{ 
				dropifiContactTab.style.left = (((new Number(data.tabPercent))/100)*window.innerWidth)+"px";

				var addLeft = dropifiContactTab.offsetLeft+ dropifiContactTab.offsetWidth;
				if(addLeft>=window.innerWidth){ 
					addLeft = (dropifiContactTab.offsetLeft - (addLeft - window.innerWidth))-5; 
					dropifiContactTab.style.left = addLeft+"px"; 
				} 
			} 
		}
	},
	resize : function(h,w,contentheight,iframeHeight,position){
		//Resize the content width 
		var dropifiContactContent = document.getElementById("dropifiContactContent");
		var dropifiIframeContainer =document.getElementById("dropifiIframeContainer");
		if(dropifiContactContent!== null && dropifiIframeContainer!==null ){
			if(window.innerWidth<=300){
				dropifiContactContent.style.width = (w-20)+"px";
				position = "center";
			}else{ 
				dropifiContactContent.style.width = (300)+"px";
			} 
			
			//Resize the content height
			if(h <= dropifiContactContent.offsetHeight){
			 	dropifiIframeContainer.style.height=(h-50)+"px";
				dropifiContactContent.style.height = (h-20)+"px";
				position = "center";
			}else{
				dropifiIframeContainer.style.height=(iframeHeight)+"px";
				dropifiContactContent.style.height = contentheight+"px";
			}
		}
	},
	//Set the initial position of the widget
	initialWidgetPosition :function(contentWidth, contentheight,iframeHeight,position){ 
		var w = window.innerWidth;
		var h = window.innerHeight;
		var toggleMargin = w<=300? -18:20; 
		var dropifiContactContent = document.getElementById("dropifiContactContent");
		
		if(BrowserDetect.mobileOS){
		   position = "center";
		}
		
		if(dropifiContactContent!==null){
			DropifiWidgetLoad.resize(h,w,contentheight,iframeHeight,position); 
			if(position == "left"){
			 	dropifiContactContent.style.margin ="0px";
			 	dropifiContactContent.style.left ="0px";
				dropifiContactContent.style.top = (h-contentheight-10)+"px";
			 }else if(position=="center"){
			 	dropifiContactContent.style.left =(w * 0.5)+"px";
				dropifiContactContent.style.top = (h * 0.5)+"px";
				dropifiContactContent.style.marginLeft = "-"+(dropifiContactContent.offsetWidth/2)+"px";
				dropifiContactContent.style.marginTop = "-"+(dropifiContactContent.offsetHeight/2)+"px";
			 }else{ 
			 	dropifiContactContent.style.margin ="0px";
				dropifiContactContent.style.left = (w-contentWidth-toggleMargin)+"px";
				dropifiContactContent.style.top = (h-contentheight-10)+"px";
			 }  
			 dropifiContactContent.style.border = "3px solid #ffffff";
		}
	}, 
	
	resizeHeight: function(position){
		
		var w = window.innerWidth;
		var h = window.innerHeight;
		var dropifiContactContent = document.getElementById("dropifiContactContent");
		var dropifiIframeContainer =document.getElementById("dropifiIframeContainer");
		//Resize the content height
		if(dropifiContactContent!== null && dropifiIframeContainer!==null ){
			if(h <= dropifiContactContent.offsetHeight){
			 	dropifiIframeContainer.style.height=(h-50)+"px";
				dropifiContactContent.style.height = (h-20)+"px";
				position = "center";
			}else{ 
				dropifiContactContent.style.height = dropifiContentHeight+"px";
				dropifiIframeContainer.style.height=(dropifiContentHeight-20)+"px";
			} 
		}
		return position;
	},
	showHideWidget : function(){
		var toggleWidth = window.innerWidth <=300 ? window.innerWidth-20:300;
		var dropifiContactContent = document.getElementById("dropifiContactContent");   
		var dropifiContactTab = document.getElementById("dropifiContactTab"); 
		
		if(dropifiContactContent!== null && dropifiContactTab!==null ){
			if(dropifiContactContent.offsetWidth<=6){
				//dropifiContactContent.style.margin ="0px";
				dropifiContactContent.style.border = "3px solid #ffffff";
				dropifiContactContent.style.width= toggleWidth+"px"; 
				dropifiContactTab.style.display = "none";  
				
				//reposition the widget
				DropifiWidgetLoad.initialWidgetPosition(300,dropifiContentHeight,iframeHeight,dropifiContentPosition);
			}else{
				toggleWidth	= 0;
				//dropifiContactContent.style.margin ="-20px";
				dropifiContactContent.style.border = "0px solid #ffffff";
				dropifiContactContent.style.width= "0px";
				dropifiContactTab.style.display = "inline-block";
			}
		}
	},
	close : function(isHidenOnLoad){ 
		DropifiWidgetLoad.dropifiWidgetCreate(DropifiWidgetLoad.publicKey,DropifiWidgetLoad.pluginType,isHidenOnLoad);
	}, 	
	dropifiWidgetCreate : function(publicKey,pluginType, hideOnLoad){
		if(publicKey!=null && publicKey !='' && publicKey !=undefined){
		var w = window.innerWidth;
		var h = window.innerHeight;
		var requestUrl = window.location.href;   
		var hostUrl = document.location.host; 
		DropifiWidgetLoad.removeElement('dropifiIframeContainer');
		var transport = new easyXDM.Socket(/** The configuration */{  
	        remote: "https://www.dropifi.com/clientmessenger/show_widget?publicKey="+publicKey+"&pluginType="+DropifiWidgetLoad.pluginType+"&mobileOS="+BrowserDetect.mobileOS+"&width="+w+"&height="+h+"&hostUrl="+hostUrl+"&requestUrl="+requestUrl,
	        //swf: "https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/widget/images/easyxdm.swf",
	        container: "dropifiIframeContainer",  
	        onMessage: function(message,origin){ 
	        	var data = JSON.parse(message);
	        	switch(data.type){
		        	case 'iframe':
		        	  var olddiv = document.getElementById("dropifi_widget_v1_contactable");
		        	  if(olddiv === null)
		        	  	return;

		        	  if((data.activated !=="false" && data.activated !==false) && data.tabText !=""){
			        	   var h = new Number(data.height);
			        	   iframeHeight = h+20;
				           var hOffset = new Number(data.heightOffset);
				           var iframe = this.container.getElementsByTagName("iframe")[0];
				           iframe.id ='dropifiContentInner'; 
				           iframe.frameBorder = 0;
				            //iframe.scrolling = 'no';
				           iframe.marginheight="0px";
				           iframe.marginwidth="0px";           
				           iframe.style.height ="100%";
				           iframe.style.width = "100%";
				           iframe.style.display='block';
				           iframe.style.visibility = "inherit";
				           iframe.style.overflowY="auto"; 
				           widgetData = data;
				            
				           var dropifiContentBar = document.getElementById("dropifiContentBar");
				           widgetData.height = h + dropifiContentBar.offsetHeight+hOffset;
				           dropifiContentHeight = widgetData.height;
	
				           var tabText = document.getElementById("dropifiTextContent");
				           tabText.textContent=widgetData.tabText; 
				           tabText.style.color = widgetData.textColor;
				           
				           dropifiContentBar.style.background=widgetData.color; 
				           dropifiContentBar.style.color = widgetData.textColor;  
				           
				           dropifiContentPosition = widgetData.position;
				           DropifiWidgetLoad.isReloadContent = data.isReloadContent; 
				           
				          DropifiWidgetLoad.resize(window.innerHeight,window.innerWidth, widgetData.height,iframeHeight,widgetData.position); 
				            
		        	  } else{ 
		        	 	 olddiv.style.display = "none";
		        	  }
		        	   
			          
			           break;
		        	case 'closeWindow':
		        		DropifiWidgetLoad.isReloadContent = data.isReloadContent;
		        		DropifiWidgetLoad.dropifiWidgetCreate(DropifiWidgetLoad.publicKey,DropifiWidgetLoad.pluginType,true);
		        		break;
		        	case 'changeHeight': 
		        		var olddiv = document.getElementById("dropifi_widget_v1_contactable");
			        	 if(olddiv === null)
			        	  	return;

		        		 if((data.activated !=="false" && data.activated !==false) && data.tabText !=""){ 
		        		 	olddiv.style.display = "block"; 
			        		var iframe = document.getElementById("dropifiIframeContainer"); 
			        		var dropifiContentBar = document.getElementById("dropifiContentBar"); 
			        		var dropifiContactContent =document.getElementById("dropifiContactContent");

			        		if(dropifiContactContent!== null && dropifiContentBar!==null ){
				        		var h = new Number(data.height); 
				        		iframeHeight = h+20;

				        		var hOffset = new Number(data.heightOffset);
				        		widgetData = data; 
				        		widgetData.height = h + hOffset + dropifiContentBar.offsetHeight ;
				        		dropifiContentHeight = widgetData.height;  
				        		iframe.style.height = iframeHeight+"px";
				        		dropifiContactContent.style.height = (widgetData.height)+"px";
				        		DropifiWidgetLoad.isReloadContent = data.isReloadContent;  
				        		
				        		if(data.isReloadContent == false){
					        		DropifiWidgetLoad.initialWidgetPosition(300,widgetData.height,iframeHeight, widgetData.position);
					        		DropifiWidgetLoad.initialContactTab(data); 
				        		
					        		if(data.hideOnLoad){ 
							       		dropifiContactContent.style.border = "0px solid #ffffff";
							       		dropifiContactContent.style.width= "0px";

							       		var dropifiContactTab = document.getElementById("dropifiContactTab");
							       		if(dropifiContactTab!==null)
							       			dropifiContactTab.style.display = "inline-block";
						            }   
				        		}
				        		olddiv.style.visibility = "visible";  
			        		}

			        	}else{
			        		olddiv.style.display = "none";
			        	}
		        	   break;
		        	 
		        }
	        },
	        onHeightChange: function(message,origin){
	        	 
	        }
	    });
		}
	},
	reloadContent: function(duration){ 
		if(DropifiWidgetLoad.isReloadContent == true){
			setTimeout(function(){ 
			  DropifiWidgetLoad.dropifiWidgetCreate(DropifiWidgetLoad.publicKey,DropifiWidgetLoad.pluginType,false); 
			},duration);
		}
	},
	removeElement: function(elementId){
		var node = document.getElementById(elementId);
		if(node !==null){
			while (node.hasChildNodes()) {
			    node.removeChild(node.firstChild);
			}
			return true;
		}
		return false;
	}

}; 

/******End Widget****************/
 

/***Strat Browser Detection***/
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
		// If the screen orientation is defined we are in a modern mobile OS
		this.mobileOS = typeof orientation != 'undefined' ? true : false;
		// If touch events are defined we are in a modern touch screen OS
		this.touchOS = ('ontouchstart' in document.documentElement) ? true : false;
		this.screenWidth = screen.width;
		this.screenHeight = screen.height;
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		},
		{
		    string: navigator.platform,
		    subString: "iPhone",
		    identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "iPad",
			identity: "iPad"
		},
		{
			string: navigator.userAgent,
			subString: "android",
			identity: "android"
		},
		{
		    string: navigator.userAgent,
		    subString: "symbian",
		    identity: "symbian"
	    },
	    {
		    string: navigator.userAgent,
		    subString: "windows ce",
		    identity: "windows mobile"
	    },
	    {
		    string: navigator.userAgent,
		    subString: "blackberry",
		    identity: "blackberry"
	    }
		
	]

}; 
BrowserDetect.init();

/***End Browser Detection****/

document.renderDropifiWidget = function(publicKey){  
	
		var olddiv = document.getElementById("dropifi_widget_v1_contactable");
		if(olddiv && olddiv.id=='dropifi_widget_v1_contactable'){		 
			//widget is loaded
		}else{		
		   var div = document.createElement('div');
		   div.id ="dropifi_widget_v1_contactable";
		   div.innerHTML = (function(){/* 
		    <div style='background-attachment:scroll;background-color:transparent;background-image:none;background-position:0 0;background-repeat:repeat;border-color:black;border-radius:0;border-style:none;border-width:medium;clear:none;clip:auto;color:inherit;counter-increment:none;counter-reset:none;cursor:auto;direction:inherit;display:inline;float:none;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:normal;font-weight:inherit;height:auto;letter-spacing:normal;line-height:inherit;list-style-image:none;list-style-position:inside;list-style-type:inherit;margin:0;max-height:none;max-width:none;opacity:1;overflow:visible;padding:0;position:static;quotes:"" "";table-layout:auto;text-align:inherit;text-decoration:inherit;text-transform:none;unicode-bidi:normal;vertical-align:baseline;visibility:inherit;white-space:normal;width:auto;word-spacing:normal;z-index:auto;cursor:pointer;display:none;margin:0;z-index:16776274;position:fixed;cursor:pointer;box-shadow:0 0 4px rgba(0,0,0,0.85);box-sizing:content-box;color:#fff;overflow:hidden;position:fixed;z-index:999999999;font-size:14px;text-align:center;border:1px solid #fff;box-shadow:0 0 3px 0 #948888,0 0 3px 0 white;' id="dropifiContactTab" onclick="DropifiWidgetLoad.showHideWidget(this)"></div>
	
		    <div style='background-attachment:scroll;background-color:transparent;background-image:none;background-position:0 0;background-repeat:repeat;border-color:black;border-radius:0;border-style:none;border-width:medium;clear:none;clip:auto;color:inherit;counter-increment:none;counter-reset:none;cursor:auto;direction:inherit;display:inline;float:none;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:normal;font-weight:inherit;height:auto;letter-spacing:normal;line-height:inherit;list-style-image:none;list-style-position:inside;list-style-type:inherit;margin:0;max-height:none;max-width:none;opacity:1;overflow:visible;padding:0;position:static;quotes:"" "";table-layout:auto;text-align:inherit;text-decoration:inherit;text-transform:none;unicode-bidi:normal;vertical-align:baseline;visibility:inherit;white-space:normal;width:auto;word-spacing:normal;z-index:auto;border-collapse:collapse;border-spacing:0;height:200px;left:0;top:0;margin:-20px;list-style:outside none none;outline:medium none;overflow:hidden;padding:0;position:fixed;width:0;z-index:2147483645;box-shadow:2px 2px 12px #444;background-color:white;border:3px solid #fff;border-radius:5px;box-shadow:0 0 6px 0 #948888,0 0 10px 0 white;margin-bottom:0;padding:0;box-sizing:content-box;' id="dropifiContactContent" onmousedown="dropifiTzdragg.startMoving(event);" onmouseup="dropifiTzdragg.stopMoving();">
	
		    	<div style='background-attachment:scroll;background-color:transparent;background-image:none;background-position:0 0;background-repeat:repeat;border-color:black;border-radius:0;border-style:none;border-width:medium;clear:none;clip:auto;color:inherit;counter-increment:none;counter-reset:none;cursor:auto;direction:inherit;display:inline;float:none;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:normal;font-weight:inherit;height:auto;letter-spacing:normal;line-height:inherit;list-style-image:none;list-style-position:inside;list-style-type:inherit;margin:0;max-height:none;max-width:none;opacity:1;overflow:visible;padding:0;position:static;quotes:"" "";table-layout:auto;text-align:inherit;text-decoration:inherit;text-transform:none;unicode-bidi:normal;vertical-align:baseline;visibility:inherit;white-space:normal;width:auto;word-spacing:normal;z-index:auto;color:white;padding:5px 10px;background-color:#1A87C7;box-shadow:0 0 2px 0 #948888,0 0 2px 0 #1A87C7;text-align:justify;font-size:14px;display:block;cursor:move;' id="dropifiContentBar">
	
		    		<p style ='background-attachment:scroll;background-color:transparent;background-image:none;background-position:0 0;background-repeat:repeat;border-color:black;border-radius:0;border-style:none;border-width:medium;clear:none;clip:auto;color:inherit;counter-increment:none;counter-reset:none;cursor:auto;direction:inherit;display:inline;float:none;font-family:inherit;font-size:14px;font-style:inherit;font-variant:normal;font-weight:normal;height:auto;letter-spacing:normal;line-height:inherit;list-style-image:none;list-style-position:inside;list-style-type:inherit;margin:0;max-height:none;max-width:none;opacity:1;overflow:visible;padding:0;position:static;quotes:"" "";table-layout:auto;text-align:left;text-decoration:inherit;text-transform:none;unicode-bidi:normal;vertical-align:baseline;visibility:inherit;white-space:normal;width:auto;word-spacing:normal;z-index:auto;padding-right:5px;overflow:auto;overflow-y:hidden;overflow-x:hidden;-ms-overflow-y:hidden;(hides vertical scrollbar);-ms-overflow-y:hidden;(hides vertical scrollbar);cursor:move;display:inline-block;width:90%;' id='dropifiTextContent'>Contact Us</p>
	
		    		 <p style='background-attachment:scroll;background-color:transparent;background-image:none;background-position:0 0;background-repeat:repeat;border-color:black;border-radius:0;border-style:none;border-width:medium;clear:none;clip:auto;color:inherit;counter-increment:none;counter-reset:none;cursor:auto;direction:inherit;display:inline;float:none;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:normal;font-weight:inherit;height:auto;letter-spacing:normal;line-height:inherit;list-style-image:none;list-style-position:inside;list-style-type:inherit;margin:0;max-height:none;max-width:none;opacity:1;overflow:visible;padding:0;position:static;quotes:"" "";table-layout:auto;text-align:inherit;text-decoration:inherit;text-transform:none;unicode-bidi:normal;vertical-align:baseline;visibility:inherit;white-space:normal;width:auto;word-spacing:normal;z-index:auto;float:right;cursor:pointer;position:relative;top:0px;margin-right:-5px;display:block'>
		    			
		    			<img style='background-attachment:scroll;background-color:transparent;background-image:none;background-position:0 0;background-repeat:repeat;border-color:black;border-radius:0;border-style:none;border-width:medium;clear:none;clip:auto;color:inherit;counter-increment:none;counter-reset:none;cursor:auto;direction:inherit;display:inline;float:none;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:normal;font-weight:inherit;height:auto;letter-spacing:normal;line-height:inherit;list-style-image:none;list-style-position:inside;list-style-type:inherit;margin:0;max-height:none;max-width:none;opacity:1;overflow:visible;padding:0;position:static;quotes:"" "";table-layout:auto;text-align:inherit;text-decoration:inherit;text-transform:none;unicode-bidi:normal;vertical-align:baseline;visibility:inherit;white-space:normal;width:auto;word-spacing:normal;z-index:auto;cursor:pointer;width:20px;'  src='https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/widget/images/close_button.png' id='dropifiCloseBar' onclick='DropifiWidgetLoad.showHideWidget(this)'>
		    		</p> 
	
		    	</div> 
	
		    	<div id='dropifiIframeContainer' style='background-image:url("https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/widget/images/wait_icon.gif");background-attachment:scroll;background-color:transparent;background-image:none;background-position:0 0;background-repeat:repeat;border-color:black;border-radius:0;border-style:none;border-width:medium;clear:none;clip:auto;color:inherit;margin:0;padding:0;background-repeat:no-repeat;background-size:100%;counter-increment:none;counter-reset:none;cursor:auto;direction:inherit;display:inline;float:none;font-family:inherit;font-size:inherit;font-style:inherit;font-variant:normal;font-weight:inherit;height:auto;letter-spacing:normal;line-height:inherit;list-style-image:none;list-style-position:inside;list-style-type:inherit;margin:0;max-height:none;max-width:none;opacity:1;overflow:visible;padding:0;position:static;quotes:"" "";table-layout:auto;text-align:inherit;text-decoration:inherit;text-transform:none;unicode-bidi:normal;vertical-align:baseline;visibility:inherit;white-space:normal;width:inherit;word-spacing:normal;z-index:auto;background-color:white;border:medium none;bottom:0;box-sizing:content-box;display:block;font-family:Arial,sans-serif;font-weight:normal;line-height:1em;opacity:1;overflow:auto;position:relative;right:0;visibility:inherit;z-index:16776280;border-bottom-right-radius:5px;border-bottom-left-radius:5px;'>
	
		    	</div>
		        
		    </div> 
		    */}).toString().replace("function",'').replace("()",'').replace("{/*",'').replace('*/}','');  
		    
		   div.style.visibility = "hidden";
		   DropifiWidgetLoad.publicKey = publicKey; 
		   window.onload = function(){
			   document.body.appendChild(div);
			   DropifiWidgetLoad.dropifiWidgetCreate(publicKey, DropifiWidgetLoad.pluginType,false); 
		   }

		}  
}

document.renderDropifiPluginWidget = function(options){ 
	DropifiWidgetLoad.pluginType = options.pluginType || options.plugin;
	document.renderDropifiWidget(options.apikey);
}

dropifi_widget_v1_loadAll = function(){ 
}

DropifiWidgetLoad.init();