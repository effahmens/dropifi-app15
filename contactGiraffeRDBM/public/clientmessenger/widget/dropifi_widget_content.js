var CreateMessage = {
	init:function(){ 
		this.fileName=null;
		this.fileSize=null;
		this.fileExtension=null;
		this.isBlockedFile=false;
	}, 
	isSizeAllowed: function(){
		 if(this.fileSize && (this.getFileSize(this.fileSize).megabytes>25)){ 
			 return false; 
		 }
		 return true;
	},
	getFileSize: function(size){ 
		var sizes = {};
		if(size){  
			var bytes = size;
			var kilobytes = (bytes / 1024);
			var megabytes = (kilobytes / 1024);
			var gigabytes = (megabytes / 1024);
			var terabytes = (gigabytes / 1024);
			var petabytes = (terabytes / 1024);
			var exabytes = (petabytes / 1024);
			var zettabytes = (exabytes / 1024);
			var yottabytes = (zettabytes / 1024);
			
			sizes.bytes=bytes; 
			sizes.kilobytes=kilobytes;
			sizes.megabytes=megabytes;
			sizes.gigabytes=gigabytes;
			sizes.terabytes=terabytes;
			sizes.petabytes=petabytes;
			sizes.exabytes=exabytes;
			sizes.zettabytes=zettabytes;
			sizes.yottabytes=yottabytes;
		} 
		return sizes;
	},
	setupFileUpload: function(){
		jQuery("#clearfile").click(function(){
			var fileContent = jQuery("#fileContent");
			fileContent.html(fileContent.html()); 
			jQuery('#widgetField_attachment').bind("change",function(){
				CreateMessage.afterFileChange(this);
			}); 
		}); 
		
		var options = { 
	        target:  '', //target element(s) to be updated with server response 
	        beforeSubmit:  function(){ //pre-submit callback    
	        	    var hasError=false;  
	        	    var message="";  
		    	   
	        	    var name = document.getElementById("widgetField_name"); 
		    		var email = document.getElementById("widgetField_email"); 
		    		var phone = document.getElementById("widgetField_phone"); 
		    		var subject = document.getElementById("widgetField_subject");	
		    		var txtMessage = document.getElementById("widgetField_message");
		    		
		    		var hasError= false; 
		    		
		    		if( (name  != "null" && name  != null) && jQuery.trim(name.value) == ""){
		    			var error_name = jQuery('#widgetError_name').val();
		    			if(error_name=='null' || jQuery.trim(error_name).length<1){
		    				error_name = "* your name is required";
		    			}
		    			message += "<p>* "+error_name+"</p>";
		    			hasError= true; 
		    		} 
		    		
		    	    if((email  != "null" && email  != null)  && jQuery.trim(email.value) == ""){
		    	    	var error_email = jQuery('#widgetError_email').val();
		    			if(error_email=='null' || jQuery.trim(error_email).length<1){
		    				error_email = "your email is required";
		    			}
		    			message += "<p>* "+error_email+"</p>";
		    			hasError= true;
		    		}else if(IsEmail(email.value) == false){
		    			 if(checkEmailDomain("#widgetField_email", "#widgetError_email", "#widgetError_email") == true){	
		    				 message += "<p>* your email format is not correct</p>"; 
		    			 }	 
		    			hasError = true;  
		    		}
		    	  
		    	    if((subject  != "null" && subject  != null) && jQuery.trim(subject.value) == ""){
		    			var error_subject = jQuery('#widgetError_subject').val();
		    			if(error_subject=='null' || jQuery.trim(error_subject).length<1){
		    				error_subject = "the subject of the message is required";
		    			}
		    			message += "<p>* "+error_subject+"</p>";
		    			hasError= true;
		    		} 	
		    		
		    		if((txtMessage  != "null" && txtMessage  != null)  && jQuery.trim(txtMessage.value) == ""){ 
		    			var error_message = jQuery('#widgetError_message').val();
		    			if(error_message=='null' || jQuery.trim(error_message).length<1){
		    				error_message = "your message is required";
		    			}
		    			message += "<p>* "+error_message+"</p>";
		    			hasError= true;
		    		}
		    		
		     	  var fileSelected = jQuery("#widgetField_attachment"); 
			      if(fileSelected && fileSelected.val()){ 
			    	   jQuery("#fileType").val(CreateMessage.fileType+" ");
				   	   //check if the file
				   	   if(CreateMessage.isBlockedFile){ 
				   		   message += "<p>* the selected file cannot be uploaded</p>";
				   		   hasError= true;
				   	   }else if(!CreateMessage.isSizeAllowed()){
				   		   message += "<p>* the selected file size is more than 25MB</p>";
				   		   hasError= true;
				   	   }   
			      } 
				
			      var captchaResult = jQuery("#captchaResult");
			      var captcha = Captcha;
			      if( (typeof captchaResult  != "undefined" && typeof captchaResult  != undefined) && (typeof captcha  != "undefined" && typeof captcha  != undefined)){
				    if(Captcha.code != jQuery.trim(captchaResult.val())){
						message += "* the answer you entered is not correct";
						hasError = true;
					}	
			      }   
			      
			   	  if(hasError){
				    jQuery(".message-body").html(message+" ");
					jQuery('#dropifiModal').modal(); 
			   		return false; 
			   	  }
				   	  
				  showProgressButton("");
	        },
	      success:function(data) {//post-submit callback  
	        	if(data){ 
	        		var data = JSON.parse(data);  
	        		if(data.callback.status != 200){ 
		 				if(data.Captcha!=undefined){
		 					Captcha = data.Captcha; 
		 					jQuery("#captchaText").html(Captcha.query+"");
		 					jQuery("#captchaResult").val("");
		 					jQuery("#captchaCode").val(Captcha.accessCode);
		 				}  
		 				
		 				jQuery(".message-body").html(data.callback.error+" ");
		 				jQuery('#dropifiModal').modal();
		 			}else{ 
		 				var message = data.callback.message;
		 				if(message==null || message=='null'|| message==undefined){
		 					message ="Thank you.";
		 				}
			 			jQuery("#contentInner").html("<div style='min-height:200px'>"+message+"</div>");
			        	CreateMessage.changeHeight(true);
			        	setTimeout(function(){
			        		CreateMessage.closeWindow(true);
		        		},3500);
			        	
			        	_gaq.push(['_trackEvent',Dropifi.hostUrl, 'Message Sent Successful']);
		 			} 
	        	}else{  
	        	  jQuery(".message-body").html("Data not submitted ");
	        	  jQuery('#dropifiModal').modal();
	        	}  
			    hideProgressButton();
	        }
		}; 

		//bind the form using ajaxForm
		jQuery('#fileupload').ajaxForm(options);
		
		jQuery('#widgetField_attachment').bind("change",function(){
			 CreateMessage.afterFileChange(this);
		}); 
		 
	},
	afterFileChange: function(e){ 
		try{ 
			 CreateMessage.isBlockedFile=false;
			 if(e && e.files && e.files[0] && e.files[0].size){
				 CreateMessage.fileSize = e.files[0].size;
				 CreateMessage.fileType = e.files[0].type;
			 } 
			 CreateMessage.fileName =jQuery("#widgetField_attachment").val();
			 if(CreateMessage.fileName){
				 CreateMessage.fileExtension = CreateMessage.fileName.substr( (CreateMessage.fileName.lastIndexOf('.') +1) );
				 CreateMessage.isBlockedFile = CreateMessage.isBlockedAttachFile(CreateMessage.fileExtension);
			 } 
		}catch(ee){ } 
	},
	isBlockedAttachFile: function(exten){
		var blocked = false;
		if(exten){
			var extens = "ade,adp,bat,chm,cmd,com,cpl,exe,hta,ins,isp,jse,lib,mde,msc,msp,mst,pif,scr,sct,shb,sys,vb,vbe,vbs,vxd,wsc,wsf,wsh".split(",");   
			jQuery.each(extens,function(key,val){
				if(jQuery.trim(val) == jQuery.trim(exten).toLowerCase()){  
					blocked = true;
				}  
			});
		}
		return blocked;
	},
	changeHeight: function(is_reloaded){
		var height = (document.body.clientHeight ||  document.body.offsetHeight) || document.body.scrollHeight ; 
        var data = {
 		    'type'           : 'changeHeight',
            'height'         :  height, 
            'heightOffset'   : '20',
            'isReloadContent': is_reloaded
         }
        socket.postMessage(JSON.stringify(data));
	},
	closeWindow: function(is_reloaded){
		var height = (document.body.clientHeight ||  document.body.offsetHeight) || document.body.scrollHeight ; 
        var data = {
 		    'type'           : 'closeWindow',
            'height'         :  height,
            'heightOffset'   : '20',
            'isReloadContent': is_reloaded
         }
        socket.postMessage(JSON.stringify(data));
	},
	dropifiSerializeObject: function(elementId){
	    var o = {};
	    var a = jQuery('#'+elementId).serializeArray();
	    if(a && a.length>1){
		    jQuery.each(a, function() {
		        if (o[this.name] !== undefined) {
		            if (!o[this.name].push) {
		                o[this.name] = [o[this.name]];
		            }
		            o[this.name].push(this.value || '');
		        } else {
		            o[this.name] = this.value || '';
		        } 
		    });
	    }
	    return o; 
	}

}

function showProgress(){jQuery(".dropifi_submit_input").append('<div id="dropifi_widget_progress"><img src="'+location.protocol+'//api.dropifi.com/widget/images/ajax-loader.gif" alt="" /> </div>');}
function showProgressTitle(a){jQuery(".dropifi_submit_input").append('<div id="dropifi_widget_progress"><img src="'+location.protocol+'//api.dropifi.com/widget/images/ajax-loader.gif" alt="" /> '+a+'</div>');}

function hideProgress(){jQuery("#dropifi_widget_progress").remove()}

function showProgressButton(title){
   jQuery("#sendMail").css({"display":"none"});
   jQuery("#sendMail").attr("disabled", "disabled");
   showProgressTitle(title);
}

function hideProgressButton(){
   jQuery("#sendMail").css({"display":"inherit"});
   jQuery("#sendMail").removeAttr("disabled");
   hideProgress();
} 

var socket = new easyXDM.Socket({
    swf: "https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/widget/images/easyxdm.swf",
    onReady: function(){
       iframe = document.createElement("iframe");
       iframe.src = easyXDM.query.url; 
    },
    onMessage: function(url, origin){
        iframe.src = url; 
    },
    onHeightChange: function(height){ 
    }
});
    
jQuery(window).load(function(){ 
	jQuery("#captchaText").html(Dropifi.captchaText);
	jQuery("#captchaCode").val(Dropifi.captchaCode);
	jQuery("#attachmentId").val(Dropifi.attachmentId);
	jQuery("#pageUrl").val(Dropifi.pageUrl);
	
	jQuery("#widgetField_phone").keypress(function (e){										 
	    //if the letter is not digit then display error and don't type anything	 
	    if(e.which == 45 || e.which == 43 || e.which == 8 || e.which == 0 || (e.which>=48 && e.which<=57) ){
	    	var phone = jQuery(this).val(); 
	    	if( e.which == 43 && jQuery.trim(phone) !=""){
	    		e.preventDefault(); 
	       	} 	
	    	
	    	if( e.which == 43 && phone.indexOf("+") !=-1){									    		 
	    		e.preventDefault();
	    	} 
	    }else{
	    	e.preventDefault(); 								    	 
	    }
	}); 
	
	jQuery("#captchaResult").keypress(function (e){
	    //if the letter is not digit then display error and don't type anything	 
	    if(e.which == 45 || e.which == 8 || e.which == 0 || (e.which>=48 && e.which<=57) ){
	    	var cap = jQuery(this).val(); 
	    	if( e.which == 45 && jQuery.trim(cap) !=""){
	    		e.preventDefault();
	       	} 	
	    	
	    	if( e.which == 45 && cap.indexOf("-") !=-1){									    		 
	    		e.preventDefault();
	    	} 
	    }else{
	    	e.preventDefault(); 								    	 
	    }
	}); 
	
	//responsive javascript
	var w = new Number(Dropifi.width);
	var h = new Number(Dropifi.height);
	var contentInner = $("#contentInner");
	
	//iphone potrait screen
	if(h<=380 && w <= 330){
		var diff = h -contentInner.height(); 
		if(diff<=30){
			var x =  document.getElementsByClassName("input-group");
			var inputGroups  = x.length;
			
			if(inputGroups>1){  
    			 jQuery(".input-group").css({
    				 'width':'50%',
    				 'float':'left'
    			 });  
    			 
    			var i;
    			for (i = 0; i < x.length; i++) {
    				if((i+1 == inputGroups) && (inputGroups %2==1)){
    					 x[i].style.width = "inherit";
    					 x[i].style.cssFloat = "none";
    				}else if((i+1 == inputGroups)){
    					 x[i].style.cssFloat = "none";
    				} 
    			}
			}
		} 
	}

    CreateMessage.setupFileUpload(); 
	jQuery.get("https://api.dropifi.com/jsonip/index.php", function(data) { 
		jQuery("#ipAddress").val(data.ip);
  	});  
	
	jQuery('input, textarea').placeholder();
	data.height =jQuery("#formContent").height();//document.body.scrollHeight || (document.body.clientHeight || document.body.offsetHeight) ;
    socket.postMessage(JSON.stringify(data));
    
	setTimeout(function(){
		data.height = jQuery("#formContent").height();//(document.body.clientHeight || document.body.offsetHeight) || document.body.scrollHeight;
		data.type = "changeHeight"; 
		data.hideOnLoad = true;
		data.showWidget =false;
		data.time = 3000; 
        socket.postMessage(JSON.stringify(data));
	},5);
	
	_gaq.push(['_trackEvent',Dropifi.hostUrl, 'Page View']);
}); 

function IsEmail(a){var b=/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;return b.test(a)}

function checkEmailDomain(a,b,c){
	var d = jQuery(b); var e = jQuery(a); var f= jQuery(b); var g=false;  
	
	e.mailcheck({suggested:function(a,b){ 
			var b="Oops!, did you mean <span class='dropifiWidget2012_suggestion' style='color:brown;cursor:pointer' title='click to add'>"+b.address+"@"+b.domain+"</span>?";
			f.html(b);
			g=true;
		}
	}); 
	
	var dropSuggestion = jQuery(".dropifiWidget2012_suggestion");	 
	if(dropSuggestion!=undefined){
		jQuery(".dropifiWidget2012_suggestion").click(function(){
			var email = jQuery(this).text(); 
			e.val(email); 
			jQuery(b).html("");	 		 
		});
	} 
	return g;
}
