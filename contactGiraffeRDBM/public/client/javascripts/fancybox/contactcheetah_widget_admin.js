jQuery(document).ready(function(){ 
	
	//convert serializeArray to json object
	$.fn.serializeObject = function(){
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};

	$.fn.center = function () {
	    this.css("position", "absolute");
	    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
	    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
	    return this;
	}

}); 


function showProgress() {
    $('body').append('<div id="progress"><img src="https://s3.amazonaws.com/ccheetah/images/ajax-loader.gif" alt="" width="30" height="30" /> Saving...</div>');
    $('#progress').center();
}

function showProgressTitle(title) {
    $('body').append('<div id="progress"><img src="https://s3.amazonaws.com/ccheetah/images/ajax-loader.gif" alt="" width="30" height="30" /> '+title+'...</div>');
    $('#progress').center();
}

function hideProgress() {
    $('#progress').remove();
}

function IsEmail(email) {
	  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
}


function checkEmailDomain(emailTagId, errorTagId, errorTagClass){
	
	var $hint = $(errorTagId);
	var	$email = $(emailTagId);
	var $error = $(errorTagId);
	var mailchecked = false; 
	$email.mailcheck({ 
	
	   suggested: function(element, suggestion) {
	
	       var suggestion = "Oops!, Did you mean <span class='suggestion' style='color:brown;cursor:pointer' title='click to add'>"  	
	                        + suggestion.address +"@"+ suggestion.domain + 
	                         "</span>?"; 	                              
			
	       $error.html(suggestion); 
	       mailchecked = true; 
	   }
	
	 });
	
	$hint.on('click', '.suggestion', function() {
	     // On click, fill in the field with the suggestion and remove the hint
		 var nemail = $(".suggestion").text();
		 $email.val(nemail); 	     
		 $(errorTagId).html("");     
	});
	
	return mailchecked; 
}

