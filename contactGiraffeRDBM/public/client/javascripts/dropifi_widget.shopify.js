if((typeof jQuery == 'undefined') || (typeof $ == 'undefined') ){		
	var dr = document.createElement('script'); dr.type = 'text/javascript'; dr.async = true;
	dr.src = "https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js"; 
	document.getElementsByTagName("head")[0].appendChild(dr);	 	 		 
	
} 		

var drw = document.createElement('script'); drw.type = 'text/javascript'; drw.async = true;
drw.src = "https://s3.amazonaws.com/dropifi/js/widget/dropifi_widget.min.js";
document.getElementsByTagName("head")[0].appendChild(drw); 					 

var readyStateCheckInterval = setInterval(function() { 
    if (document.readyState === "complete"){    	 
    	document.renderDropifiPluginWidget({'apikey':Shopify.shop,'plugin':'Shopify'});
        clearInterval(readyStateCheckInterval);   
    }     
},1000);  
 

