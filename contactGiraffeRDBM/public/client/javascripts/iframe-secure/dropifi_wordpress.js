//convert serializeArray to json object
jQuery(document).ready(function(){
 
	jQuery.fn.serializeObject = function(){
		var o = {};
		var a = this.serializeArray();
		alert(a);
		jQuery.each(a, function(){			
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || ''; 
			}
		});
		return o;
	}; 
	
	jQuery("#dropifi_create_new_account").click(function(){
		var requestUrl = jQuery("#requestUrl").val(); 
		var accessToken =jQuery("#accessToken").val();
		var userdata = {}; //jQuery("#dropifi_signup").serializeObject(); 
		
		userdata.hostUrl = window.location.host;
		userdata.requestType = "SIGNUP"; 
		userdata.accessToken = accessToken;
		userdata.requestUrl = requestUrl;
		userdata.displayName = jQuery("#displayName").val();
		userdata.user_email = jQuery("#user_email").val();
		userdata.user_re_password = jQuery("#user_re_password").val();
		userdata.user_password = jQuery("#user_password").val();
		userdata.user_domain = jQuery("#user_domain").val(); 
		 
		jQuery('body').css( 'cursor', 'wait' );
		jQuery("#dropifi_create_new_account").css( 'cursor', 'wait' );
		jQuery.ajax({ 
			type:"POST",
			url:requestUrl,
			dataType:"json",
			data :{'userdata': userdata}, 
			success :function(data){				 
				if(data.success==200){						
					 
					window.location.reload();  
				}else{ 
				
				jQuery("#dropifi_s_message_status").html(data.msg); 
				jQuery("#dropifi_s_message_status").css({'background-color': '#de4343','border-color': '#c43d3d'});
				jQuery('body').css( 'cursor', 'default' );
				jQuery("#dropifi_create_new_account").css( 'cursor', 'pointer' );
				}
			},
			error : function(data){ 
				 
				jQuery("#dropifi_s_message_status").html("An error occurred while submiting your details, try creating the account again"); 
				jQuery("#dropifi_s_message_status").css({'background-color': '#de4343','border-color': '#c43d3d'});
				jQuery('body').css( 'cursor', 'default' );
				jQuery("#dropifi_create_new_account").css( 'cursor', 'pointer' );
			}			
		});			
		 
	});
	
	jQuery("#dropifi_login_account").click(function(){		
		var requestUrl = jQuery("#l_requestUrl").val();
		var userdata = {}; 
		userdata.requestType="LOGIN";
		userdata.login_email=jQuery("#login_email").val();
		userdata.accessKey = jQuery("#accessKey").val();
		userdata.accessToken =  jQuery("#l_accessToken").val();
		userdata.requestUrl=requestUrl;
		
		jQuery('body').css( 'cursor', 'wait' );
		jQuery("#dropifi_login_account").css( 'cursor', 'wait' ); 
		jQuery.ajax({
			type:"POST",
			url:requestUrl, 
			dataType:'json',
			data:{'userdata':userdata}, 
			success : function(data){				 
				if(data.success==200){ 					
					window.location.reload(); 
					
				}else{
					jQuery("#dropifi_l_message_status").html(data.msg);
					jQuery("#dropifi_l_message_status").css({'background-color': '#de4343','border-color': '#c43d3d'});
					jQuery('body').css( 'cursor', 'default' );
					jQuery("#dropifi_login_account").css( 'cursor', 'pointer' ); 
				}
			},
			error:function(data){
				jQuery('body').css( 'cursor', 'default' );
				jQuery("#dropifi_login_account").css( 'cursor', 'pointer' );
				jQuery("#dropifi_l_message_status").html("An error occurred while submiting your details, try logging into your account again"); 
				jQuery("#dropifi_l_message_status").css({'background-color': '#de4343','border-color': '#c43d3d'});
			}
		});
		
		
	});
	
	jQuery("#reset_dropifi_account").click(function(){
		var requestUrl = jQuery("#r_requestUrl").val();
		var userdata = {};		
		userdata.requestType="RESET_DROPIFI_ACCOUNT";
		userdata.requestUrl=requestUrl;
		
		jQuery.ajax({
			type:"POST",
			url:requestUrl,
			dataType:'json',
			data:{'userdata':userdata}, 
			success : function(data){				 
				if(data.success==200){					
					window.location.reload(); 
				} 
 
			},
			error:function(data){
 
			}
		});
	});
		
	jQuery(".dropifi_l_msg_error").click(function(){ 
		jQuery("#dropifi_l_message_status").html("Once you submit your login details below, the Dropifi contact widget will be installed on your site. Login to your dropifi account to customize the look and feel of your widget.");
		jQuery("#dropifi_l_message_status").css({'background-color': '#4ea5cd','border-color': '#3b8eb5'}); 
	});
	//'background-color': '#4ea5cd','border-color': '#3b8eb5'
	//'background-color': '#61b832','border-color': '#55a12c'
	jQuery(".dropifi_s_msg_error").click(function(){ 
		jQuery("#dropifi_s_message_status").html("Once you submit the details below, the Dropifi contact widget will be installed on your site. Login to your dropifi account to customize the look and feel of your widget.");
		jQuery("#dropifi_s_message_status").css({'background-color': '#4ea5cd','border-color': '#3b8eb5'}); 
	});

//end
});