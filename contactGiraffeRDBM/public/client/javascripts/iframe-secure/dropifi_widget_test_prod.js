  /*
 * dropifi widget 2.0.1 
 * 
*/
dropifi_widget_v1_loadAll();
var dropifi_widget_is_close_2012 = true;
var dropifi_widget_is_old_version = true;
var dropifi_widget_plugin_type=null; 
var dropifi_widget_reload_content = false;

document.renderDropifiWidget = function (apikey){
	var dropifi_widget_is_loaded = false;
	
	if(typeof jQuery != undefined && typeof easyXDM !=undefined){ 
		//$(function(){ 
		
		//setTimeout(function(){	
			jQuery(document).ready(function(){ 
				jQuery("#dropifi_widget_v1_contactable").remove();					
				jQuery('body').append("<div id='dropifi_widget_v1_contactable'> <div id='cheetahWidget2012_contactable_inner'> </div> <form id = 'cheetahWidget2012_contactForm' name ='' method='post'></form> </div>");
			
				//jQuery('#dropifi_widget_v1_contactable').dropifiWidget({'apikey':apikey, 'buttonid':'contactable' }); 
				dropifiWidgetCreate(apikey,'contactable');
				dropifi_widget_is_loaded = true; 
				 
			});  
	 	//},200); 
		//});    
		
	}else{
		//loadjsfile('https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js');  
	}
	
	setTimeout(function(){	
		if(dropifi_widget_is_loaded == false){ 
			jQuery(document).ready(function(){
				jQuery("#dropifi_widget_v1_contactable").remove();			
				jQuery('body').append("<div id='dropifi_widget_v1_contactable'> <div id='cheetahWidget2012_contactable_inner'> </div> <form id = 'cheetahWidget2012_contactForm' name ='' method='post'></form> </div>");
				dropifiWidgetCreate(apikey,'contactable');
			});
		}
	},3500);   
	
}; 

document.renderDropifiPluginWidget = function(options){
	var dropifi_widget_is_loaded = false;
	var apikey = options.apikey;
	dropifi_widget_plugin_type = options.plugin;
	document.renderDropifiWidget(apikey); 	
};

function  isLoaded(id){
	jQuery('#'+id).load(function(){
		return true;
	}).error(function(){
		return false;
	});
}

function replaceAll(txt, replace, with_this) {
	return txt.replace(new RegExp(replace, 'g'),with_this); 
}

function dropifi_widget_v1_loadAll(){ 
	loadjsfile('https://s3.amazonaws.com/dropifi/js/widget/easyXDM.js?v=3'); 
	loadcssfile('https://s3.amazonaws.com/dropifi/css/widget/dropifi_widget.min.css');
}


function cheetahWidget2012_writeToIframe(apikey,buttonPosition,hostUrl,pluginType, outTime,innerTime){
	jQuery('#cheetahWidget2012_contactForm').html("<iframe id ='cheetahWidget2012_contactable_iframe' width='390px' scrolling='no' frameBorder='0' hspace='0' vspace='0' marginheight='0' marginwidth='0' " +
			"src='https://www.dropifi.com/clientswidgets/show_widget_content?publicKey="+apikey+"&buttonPosition="+buttonPosition+"&hostUrl="+hostUrl+"&pluginType="+pluginType+"'></iframe>");

	setTimeout(function(){
		var  myIframe = document.getElementById("cheetahWidget2012_contactable_iframe");			
		var iframeDoc = myIframe.contentWindow.document;
		iframeDoc.open();						 
		iframeDoc.write(widgetCode); 
		
		setTimeout(function(){ 
			var frameheight = iframeDoc.body.scrollHeight;			
			myIframe.height =  (frameheight)  +  'px';	
			//iframeDoc.body.scrollWidth = contentWidth + "px"
			iframeDoc.close(); 	
		}, innerTime); 								
		//return frameheight;  
	}, outTime);  
 
}

function isHostObject(property){
    return (typeof property != 'object' && property !=null);
}

var toArrayJSon = function(message){
	 var arr={};
	jQuery.each(message.split(','),function(key,val){	 
		var values = val.split(':');		 
		arr[values[0]]=values[1];	        		
	});
	return arr;
}

function cheetahWidget2012_IframeHeight(apikey,buttonPosition,hostUrl,pluginType,outTime){
	jQuery('#cheetahWidget2012_contactForm').html(" "); 
		
    var transport = new easyXDM.Socket(/** The configuration */{  
        remote: "https://www.dropifi.com/clientswidgets/show_widget_content?publicKey="+apikey+"&buttonPosition="+buttonPosition+"&hostUrl="+hostUrl+"&pluginType="+pluginType+"", 
        swf: "https://s3.amazonaws.com/dropifi/js/widget/easyxdm.swf",
        container: "cheetahWidget2012_contactForm",  
        onMessage: function(message,origin){       	
        	
        	var data = toArrayJSon(message);   
        	
        	switch(data.type){
	        	case 'iframe': 
		        	var h= new Number(data.height);
		            var iframe = this.container.getElementsByTagName("iframe")[0];
		            iframe.id ='cheetahWidget2012_contactable_iframe';  
		            iframe.frameBorder = 0; 
		            iframe.scrolling = 'no';  
		            iframe.marginheight="0px";
		            iframe.marginwidth="0px";           
		            iframe.style.height = h+"px";  
		            iframe.style.width = "390px";
		            iframe.style.overflow="hidden";
		            
		            jQuery("#cheetahWidget2012_contactForm").append('<div id="cheetahWidget2012_footer" style="margin-bottom: 10px; float:'+
		        		   buttonPosition+';"><a href="https://www.dropifi.com" target="_blank" style="text-decoration:none"><img src="https://s3.amazonaws.com/dropifi/images/powered_by_dropifi.png" id="dropifiWidget_powered_by_dropifi"></a> </div>');
		           	
		            break;
	        	case 'closeWindow':
	        		dropifi_widget_reload_content = true;
	        		setTimeout('auto_hide_drofpifi_widget()',3500);	        		
	        		break;
	        	case 'changeHeight':
	        		var iframe = this.container.getElementsByTagName("iframe")[0];
	        		 iframe.style.height = data.height+"px"; 
	        		break;
	        }
           
        },
        onHeightChange: function(message,origin){
        	 
        }
    });  
}

function reloadContent(apikey,posi,pluginType,height){
	if(dropifi_widget_reload_content==true){
		setTimeout(function(){
			cheetahWidget2012_IframeHeight(apikey,posi,pluginType, height); 
		},700);
	}
}

function compute_dropifi_widget_height(){
	var min_height = 280;

	var  myIframe = document.getElementById("cheetahWidget2012_contactable_iframe");
	var iframeDoc = myIframe.contentWindow.document; 
	//iframeDoc.open();		
	var frameheight = iframeDoc.body.scrollHeight;	
	
	//alert(formheight +" : " + frameheight); 
	var formheight = jQuery('#cheetahWidget2012_contactForm').height(); 
	
	myIframe.height =  (frameheight)  +  'px';	
	//jQuery('#cheetahWidget2012_contactForm').css({'height':(frameheight+5)+'px'});
	/*if(formheight > frameheight+5){ 
		alert(formheight +" : " + frameheight); 
	 	jQuery('#cheetahWidget2012_contactForm').css({'height':frameheight+'px'});
	 	jQuery('#cheetahWidget2012_contactForm').css({'height':'auto'}); 
	 }
	*/
	//jQuery('#cheetahWidget2012_contactForm').css({'height':'auto'}); 
}

function loadjsfile(filename){ 			 
	var co = document.createElement('script');  
	    co.type = 'text/javascript'; 
	    co.src = filename;	     
	    document.getElementsByTagName("head")[0].appendChild(co);
}

function loadcssfile(filename){			 
  var fileref=document.createElement("link");
	  fileref.setAttribute("rel", "stylesheet");
	  fileref.setAttribute("type", "text/css");
	  fileref.setAttribute("href", filename); 
	  document.getElementsByTagName("head")[0].appendChild(fileref);
}


function auto_hide_drofpifi_widget(){
	var this_id_prefix = '#dropifi_widget_v1_contactable' + " "; 
	if(dropifi_widget_is_close_2012 !=undefined && dropifi_widget_is_close_2012 == false){		
		jQuery(this_id_prefix+'div#cheetahWidget2012_contactable_inner').trigger('click'); 
	}
} 


function centerImages(theight){
	var valTh = (100-theight);
	var th =  valTh>0 ?(valTh/2):0;
	jQuery("#cheetahWidget2012_contactable_inner #dropifi2012_version_inner_label").css({
		'height':theight+'px',
		'margin-top':th+'px',
		'margin-bottom':th+'px'									
	});
}

function hexToR(h) {return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h) {return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h) {return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}

function dropifiWidgetCreate(apikey, userButtonid) { 	
	 
	//obtain the APIKEY of the company 
	var apikey = apikey;		 
	var buttonid = userButtonid;			
	var this_id_prefix = '#dropifi_widget_v1_contactable' + ' '; 
	var widgetCode = "";
	var cheetah_securekey = ""; 
	//$('#contactable').html("<div id='contactable_inner'> </div> <form id = 'contactForm' name ='' method='post'></form>");
 	 
 	return jQuery('#dropifi_widget_v1_contactable').each(function(){ 		 		
 	
 		
 		
		//TOBE IMPLEMENTED 			
		//query the server for the html code of the widget using the APIKEY
 		var hostUrl = document.location.host;
 		var requestUrl = document.location.href;
 		//alert("plugin " + dropifi_widget_plugin_type);
 		jQuery.ajax({
			  type: "GET",
			  url: "https://www.dropifi.com/clientwidgets/paremeters_test",
			  dataType: "jsonp",
			  data :{'apikey' : apikey,'hostUrl':hostUrl,'requestUrl':requestUrl,'pluginType':dropifi_widget_plugin_type},
			  jsonp : 'w', 
			  crossDomain : true,
			  success: function(data){							  
				if(data != undefined && data != null){
					 // $('#cheetahContent873646937').val(data.html);
					 widgetCode = data.iframeHtml;
								 
					 var buttonText = data.buttonText; 
					 var bcolor = '#222'; 
					 var bPercent = "40%";		
					 var bodyPercent = "50%";
					 var mappValue = mappingValue();							 
					 if(data.buttonColor != undefined){
						 bcolor = data.buttonColor; 
					 }
					 
					 if(data.buttonPercent != undefined){
						 bPercent = data.buttonPercent+"%"; 						 
						 bodyPercent = data.buttonPercent+10+"%"; 
					 }
					
					 var Rcolor = hexToR(bcolor);
					 var Bcolor = hexToB(bcolor);
					 var Gcolor = hexToG(bcolor);
					 var RGB = Rcolor+","+Gcolor+","+Bcolor;
					 				 
					 var posi = data.buttonPosition;

					  jQuery("#cheetahWidget2012_contactForm").css({'z-index':'999999999999999'}); 
					  							  
					  if(posi == "left" ){									 						  
						 //widget button appears at the left side
						  
						  jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner').css({
						 		'background-image':'url("https://s3.amazonaws.com/dropifi/images/widget/client/widget_left_new.png")',
						 		'background-repeat':'no-repeat',
						 		'color':'#FFFFFF',
						 		'padding-top':'40px',
						 		'background-color':bcolor,
						 		'cursor':'pointer',
						 		'height':'105px',
						 		'left':'0',
						 		'margin-left':'-5px',
						 		'*margin-left':'-5px', 	
						 		'overflow':'hidden',
						 		'position':'fixed',
						 		'*position':'absolute',
						 		//'text-indent':'-100000px', 
						 		'top':bPercent,
						 		'*margin-top':'10px',
						 		'width':'41px', 
						 		'z-index':'100000' 
						 }); 
						  
						 jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactForm').css({
								'background-color':'rgba('+RGB+', 0.8)',
							    'border-radius':' 10px 10px 10px 10px',
							    'box-shadow':' 2px 2px 12px #444444',
							    'padding':'8px',
								'color':'#999',
								'min-height':'280px', 
								'left':'0',
								'margin-left':'-430px',
								'margin-top':'-160px',
								'overflow':'hidden',
								'padding-left':'19px',
								'position':'fixed',
								'top':bodyPercent,
								'width':'390px',
								'z-index':'999999999999999',
								'padding-bottom':'5px' 
						 });
						  						 
						 jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_overlay').css({
								'background-color':'#666666',
								'display':'none',
								'height':'100%',
								'left':'0',
								'margin':'0',
								'padding':'0',
								'position':'absolute',
								'top':'0',
								'width':'100%',
								'z-index':'0'
						});
						  
														
						 if(buttonText !=undefined && jQuery.trim(buttonText) != ""){
							 buttonText = jQuery.trim(buttonText);
						 }else{
							 buttonText = " RCC.png;Ro.png;Rn.png;Rt.png;Ra.png;Rc.png;Rt.png;SP.png;RCU.png;Rs.png"
						 } 
						  
						 var btext = buttonText.split(";"); 
						 var theight = 0; 
						 jQuery("#cheetahWidget2012_contactable_inner").append("<div id='dropifi2012_version_inner_label' style='width:15px;margin-top:40px; margin:auto;margin:auto;'></div>");
						 jQuery.each(btext,function(key,val){
							 if(jQuery.trim(val) !=""){										 
								jQuery("#cheetahWidget2012_contactable_inner #dropifi2012_version_inner_label").prepend("<img style='padding:0px;margin:0px;float:left' src = 'https://s3.amazonaws.com/dropifi/images/widget/client/alphanumeric/"+val+"'/>");
								theight +=mappValue[val];
							 }								
						 }); 
					 
						 centerImages(theight);
						 cheetahWidget2012_IframeHeight(apikey, posi,hostUrl,dropifi_widget_plugin_type,500); 
						//show / hide function
						jQuery(this_id_prefix+'div#cheetahWidget2012_contactable_inner').toggle(function(){	
							//var widgetCode= $('#cheetahContent').val();
							//widgetCode
							
							jQuery(this_id_prefix+'#cheetahWidget2012_overlay').css({display: 'block'});
							jQuery(this).animate({"marginLeft": "-=5px"}, "fast"); 
							jQuery(this_id_prefix+'#cheetahWidget2012_contactForm').animate({"marginLeft": "-=0px"}, "fast");
							jQuery(this).animate({"marginLeft": "+=415px"}, "slow"); 									
							jQuery(this_id_prefix+'#cheetahWidget2012_contactForm').animate({"marginLeft": "+=418px"}, "slow"); 	
							
							dropifi_widget_is_close_2012 = false; 
							
						},  
						function(){								 
							jQuery(this_id_prefix+'#cheetahWidget2012_contactForm').animate({"marginLeft": "-=418px"}, "slow");
							jQuery(this).animate({"marginLeft": "-=415px"}, "slow").animate({"marginLeft": "+=5px"}, "fast"); 
							jQuery(this_id_prefix+'#cheetahWidget2012_overlay').css({display: 'none'});
							 dropifi_widget_is_close_2012 = true;
							 
							 reloadContent(apikey,posi,hostUrl,dropifi_widget_plugin_type,400);
						});
					 
					  
				}else if(posi == "right" || posi =="undefined"){ 							 
					//widget button appears at the right side					
	 					
					jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner').css({
				 		'background-image':'url("https://s3.amazonaws.com/dropifi/images/widget/client/widget_right_new.png")',	
				 		'background-repeat':'no-repeat',
				 		'padding-top':'40px',
				 		'color':'#FFFFFF',
				 		'background-color':bcolor,
				 		'cursor':'pointer',
				 		'height':'105px',
				 		'right':'0',
				 		'margin-right':'-5px', 
				 		'*margin-right':'-5px', 
				 		'overflow':'hidden',
				 		'position':'fixed',
				 		'*position':'absolute', 
				 		//'text-indent':'-100000px',
				 		'top':bPercent,
				 		'*margin-top':'10px',
				 		'width':'41px',
				 		'z-index':'100000' 
				 	});
													
					
					jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactForm').css({
						'background-color': 'rgba('+RGB+', 0.8)',
					    'border-radius': '10px 10px 10px 10px',
					    'box-shadow': '2px 2px 12px #444444',
					    'padding':'8px',
						'color':'#999',
						'min-height':'280px', 
						'right':'0',
						'margin-right':'-430px',
						'margin-top':'-160px',
						'overflow':'hidden',
						'padding-right':'19px',
						'position':'fixed',
						'top':bodyPercent,
						'width':'390px',
						'z-index':'999999999999999',
						'padding-bottom':'2px'
					});
												
					jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_overlay').css({
						'background-color':'#666666',
						'display':'none',
						'height':'100%',
						'right':'0',
						'margin':'0',
						'padding':'0',
						'position':'absolute',
						'top':'0',
						'width':'100%',
						'z-index':'0'
					});
											
					if(buttonText !=undefined && jQuery.trim(buttonText) != ""){
						  buttonText = jQuery.trim(buttonText);
					}else{
						  buttonText = " RCC.png;Ro.png;Rn.png;Rt.png;Ra.png;Rc.png;Rt.png;SP.png;RCU.png;Rs.png"
					} 
						 
					var btext = buttonText.split(";");
					var theight = 0; 
					 
					jQuery("#cheetahWidget2012_contactable_inner").append("<div id='dropifi2012_version_inner_label' style='width:15px;margin-left:auto;margin-right:auto;'> </div>");
					jQuery.each(btext,function(key,val){
						 var id ="dd_image_"+key;
						if(jQuery.trim(val) !="") {										 
							jQuery("#cheetahWidget2012_contactable_inner #dropifi2012_version_inner_label").prepend("<img id='" +id+ "' style='padding:0px;margin:0px;float:right' src = 'https://s3.amazonaws.com/dropifi/images/widget/client/alphanumeric/"+val+"'/>");	
							theight += mappValue[val];		
						}
					});
					
					centerImages(theight);
 
					cheetahWidget2012_IframeHeight(apikey,posi,hostUrl,dropifi_widget_plugin_type, 500); 
					//show / hide function
					jQuery(this_id_prefix+'div#cheetahWidget2012_contactable_inner').toggle(
						function() {									 
							//widgetCode;
													
							jQuery(this_id_prefix+'#cheetahWidget2012_overlay').css({display: 'block'});
							jQuery(this).animate({"marginRight": "-=5px"}, "fast"); 
							jQuery(this_id_prefix+'#cheetahWidget2012_contactForm').animate({"marginRight": "-=0px"}, "fast");
							jQuery(this).animate({"marginRight": "+=415px"}, "slow"); 
							jQuery(this_id_prefix+'#cheetahWidget2012_contactForm').animate({"marginRight": "+=418px"}, "slow"); 
							
							dropifi_widget_is_close_2012 = false;
 
						}, 
						function() {
							jQuery(this_id_prefix+'#cheetahWidget2012_contactForm').animate({"marginRight": "-=418px"}, "slow");
							jQuery(this).animate({"marginRight": "-=415px"}, "slow").animate({"marginRight": "+=5px"}, "fast"); 
							jQuery(this_id_prefix+'#cheetahWidget2012_overlay').css({display: 'none'});
							
							dropifi_widget_is_close_2012 = true;
							reloadContent(apikey,posi,hostUrl,dropifi_widget_plugin_type,400);
							 
					}); 									
			
				  } 
 
				  jQuery('#cheetahWidget2012_contactable_inner:hover').css({
						'margin-right':'-4px'
				  });
					
				}
			  },
			  error: function(){
			  	//alert("Error: "+ data); 
			  }
		}); 				 

		//end of
 	});			 
	
 //end of function

}

(function($,jQuery) {	
		//define the name of the widget	
	jQuery.fn.dropifiWidget = function(options){
		return dropifiWidgetCreate(options.apikey, options.buttonid);
	} 
	
})($,jQuery);

function mappingValue(){
	var mapp = new Array();
	
	mapp['SP.png']=11;
	mapp['RCA.png']=10;
	mapp['RCB.png']=11;
	mapp['RCC.png']=11;
	mapp['RCD.png']=11;
	mapp['RCE.png']=10;
	mapp['RCF.png']=9;
	mapp['RCG.png']=12;
	mapp['RCH.png']=11;
	mapp['RCI.png']=3;
	mapp['RCJ.png']=8;
	mapp['RCK.png']=11;
	mapp['RCL.png']=9;
	mapp['RCM.png']=13;
	mapp['RCN.png']=11;
	mapp['RCO.png']=13;
	mapp['RCP.png']=10;
	mapp['RCQ.png']=13;
	mapp['RCR.png']=10;
	mapp['RCS.png']=11;
	mapp['RCT.png']=10;
	mapp['RCU.png']=11;
	mapp['RCV.png']=11;
	mapp['RCW.png']=16;
	mapp['RCX.png']=11;
	mapp['RCY.png']=11;
	mapp['RCZ.png']=11;
	
	mapp['Ra.png']=9;
	mapp['Rb.png']=9;
	mapp['Rc.png']=9;
	mapp['Rd.png']=9;
	mapp['Re.png']=9;
	mapp['Rf.png']=6;
	mapp['Rg.png']=9;
	mapp['Rh.png']=8;
	mapp['Ri.png']=3;
	mapp['Rj.png']=4;
	mapp['Rk.png']=8;
	mapp['Rl.png']=3;
	mapp['Rm.png']=13;
	mapp['Rn.png']=8;
	mapp['Ro.png']=9;
	mapp['Rp.png']=9;
	mapp['Rq.png']=9;
	mapp['Rr.png']=6;
	mapp['Rs.png']=8;
	mapp['Rt.png']=5;
	mapp['Ru.png']=8;
	mapp['Rv.png']=8;
	mapp['Rw.png']=13;
	mapp['Rx.png']=9;
	mapp['Ry.png']=9;
	mapp['Rz.png']=9;
	 
	return mapp;
}

if(typeof jQuery != undefined){
	jQuery(document).ready(function(){	
		jQuery("#dropifi_widget_v1_contactable").remove();		 	
	 	jQuery('body').append("<div id='dropifi_widget_v1_contactable'> <div id='cheetahWidget2012_contactable_inner'> </div> <form id = 'cheetahWidget2012_contactForm' name ='' method='post'></form> </div>");
	 	dropifi_widget_is_old_version = false;	 	
	});	 	 
} 
 
//--------------------------------------------------------------------------------------------------------------------------------------------//