loadjsfile('https://s3.amazonaws.com/dropifi/js/widget/jquery.validate.pack.js');  
loadjsfile('https://s3.amazonaws.com/dropifi/js/widget/jquery.mailcheck.min.js'); 

var dropifi_error_catch = false;
var iframe;
var normalHeight;
var current_height_iframe;
var socket = new easyXDM.Socket({ 
    swf: "https://s3.amazonaws.com/dropifi/js/widget/easyxdm.swf",
    onReady: function(){ 
        iframe = document.createElement("iframe");
        //document.body.appendChild(iframe);
        iframe.src = easyXDM.query.url;
    },
    onMessage: function(url, origin){
        iframe.src = url; 
    },
    onHeightChange: function(height){
    	
    }
});
 
function loadjsfile(filename){ 			 
	var co = document.createElement('script');  
	    co.type = 'text/javascript'; 
	    co.src = filename;	     
	    document.getElementsByTagName("head")[0].appendChild(co);
}

function replaceAll(txt, replace, with_this) {
	return txt.replace(new RegExp(replace, 'g'),with_this); 
}

function createUploader(){            
	var uploader = new qq.FileUploader({ 
	    element: document.getElementById('file-uploader'),
	    action: 'https://www.dropifi.com/widget/client', 
	    debug: false
	});            
}

dropifiSerializeObject = function(elementId){
    var o = {};
    var a = jQuery('#'+elementId).serializeArray();    
   
    jQuery.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o; 
};

function checkEmailDomain(a,b,c){
	var d = jQuery(b); var e = jQuery(a); var f= jQuery(b); var g=false;  
	
	e.mailcheck({suggested:function(a,b){ 
			var b="Oops!, did you mean <span class='cheetahWidget2012_suggestion' style='color:brown;cursor:pointer' title='click to add'>"+b.address+"@"+b.domain+"</span>?";
			f.html(b);
			g=true;
		}
	}); 
	
	var dropSuggestion = jQuery(".cheetahWidget2012_suggestion");	 
	if(dropSuggestion!=undefined){
		jQuery(".cheetahWidget2012_suggestion").click(function(){
			var email = jQuery(this).text(); 
			e.val(email); 
			jQuery(b).html("");	 		 
		});
	} 
	return g;
}

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br>'; 
    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function doSaveCheetahWidget(publicKey){
 	var userdata = dropifiSerializeObject('cheetahWidget2012_contactForm');
 	userdata.message = jQuery("#cheetahWidget2012_div_textarea_helper").html(); 
	var txtMessage = jQuery("#cheetahWidgetField_message").val();  
 	
	//form validation 
	var name = userdata.name; 
	var email = userdata.email; 
	var phone = userdata.phone; 
	var subject = userdata.subject; 	
	var message = userdata.message;

	var hasError= false; 
	
	if(userdata.email==undefined || userdata.email=='undefined'){
		userdata.email = jQuery('#cheetahWidgetField_email').val();
		email = userdata.email;
	}
	
	if(userdata.name  != undefined && jQuery.trim(userdata.name) == ""){
		jQuery('#cheetahWidgetError_name').html("*your name is required");
		hasError= true; 
	}
	
    if(email != undefined && jQuery.trim(email) == ""){
		jQuery('#cheetahWidgetError_email').html("*your email is required");
		hasError= true; 
	}else if(IsEmail(email) == false){ 
		//alert($('#cheetahWidgetField_email').val());  		
		 if(checkEmailDomain("#cheetahWidgetField_email", "#cheetahWidgetError_email", ".cheetahWidgetError_email") == false){	    	  
			 jQuery('#cheetahWidgetError_email').html("*your email format is not correct");
		 }		 
		hasError = true; 
	}
	
	/*
	if(phone != undefined && phone.trim() == ""){
		$('#cheetahWidgetError_phone').html("*your phone is required");  
		hasError= true;
	}
	*/
	
	if(subject != undefined && jQuery.trim(subject) == ""){
		jQuery('#cheetahWidgetError_subject').html("*the subject of the message is required");
		hasError= true;
	} 	
	
	if(txtMessage != undefined && jQuery.trim(txtMessage) == ""){
		jQuery('#cheetahWidgetError_message').html("*your message is required");
		hasError= true;
	}
	
	if(hasError == true){
		if(dropifi_error_catch==false){
			dropifi_error_catch=true;
			current_height_iframe = (document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
			jQuery('#change_iframe_height').trigger('click');	
		} 
		return false;
	}
	
	//validation complete
	showProgressTitle("");  	
	jQuery(".cheetahWidget2012_button").css({"display":"none"});
	jQuery(".cheetahWidget2012_button").attr("disabled", "disabled");
	
	var pluginType = Dropifi.pluginType; 
	
	publicKey = Dropifi.publicKey;  
	
	if(jQuery.trim(userdata.message)==""){
    	userdata.message = txtMessage;
    }
	
	jQuery.ajax({ 
		  type:"POST", 
		  url:"https://www.dropifi.com/clientswidgets/widget_iframe_sending_message",  
		  data :{'publicKey':publicKey,'cs':userdata,'pluginType':pluginType},  
		  success: function(data) { 			  
			jQuery(".cheetahWidget2012_button").removeAttr("disabled");
			jQuery(".cheetahWidget2012_button").css({"display":"inherit"});
			var dropwidget_content =  jQuery('#cheetahWidget2012_content');
			var message = data.callback.message; 
			if(message==null || message=='null'|| message==undefined){
				message ="Thank you."; 
			}
			
			dropwidget_content.html("<div id='cheetahWidget2012_button_callbackmessage'>"+ message+"</div>"); 
			dropwidget_content.height('250px');   
			auto_hide_drofpifi_widget();
			//$('#cheetahWidget2012_callback').html(data.message); 			
			hideProgress();
			console.log("What is it","An error occured whilst returning data");
		  },
		  error:function(){
		    jQuery(".cheetahWidget2012_button").removeAttr("disabled"); 
		    jQuery(".cheetahWidget2012_button").css({"display":"inherit"}); 
		    hideProgress();			  
		}
	});	
}

var auto_hide_drofpifi_widget=function(){ 
	var height = (document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
	socket.postMessage("type:closeWindow,height:"+height);
}

var send_to_parent_iframe=function(type){ 
	var height = (document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
	send_to_parent_iframe_height(type,height);
}

var send_to_parent_iframe_height=function(type,height){ 	 
	socket.postMessage("type:"+type+",height:"+height); 
}

function showProgress(){jQuery(".cheetahWidget2012_submit_input").append('<div id="dropifi_widget_progress"><img src="https://s3.amazonaws.com/dropifi/images/ajax-loader.gif" alt="" /> </div>');}
function showProgressTitle(a){jQuery(".cheetahWidget2012_submit_input").append('<div id="dropifi_widget_progress"><img src="https://s3.amazonaws.com/dropifi/images/ajax-loader.gif" alt="" /> '+a+'</div>');}
function hideProgress(){jQuery("#dropifi_widget_progress").remove()}
function IsEmail(a){var b=/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;return b.test(a)}

/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(f,h,$){var a='placeholder' in h.createElement('input'),d='placeholder' in h.createElement('textarea'),i=$.fn,c=$.valHooks,k,j;if(a&&d){j=i.placeholder=function(){return this};j.input=j.textarea=true}else{j=i.placeholder=function(){var l=this;l.filter((a?'textarea':':input')+'[placeholder]').not('.placeholder').bind({'focus.placeholder':b,'blur.placeholder':e}).data('placeholder-enabled',true).trigger('blur.placeholder');return l};j.input=a;j.textarea=d;k={get:function(m){var l=$(m);return l.data('placeholder-enabled')&&l.hasClass('placeholder')?'':m.value},set:function(m,n){var l=$(m);if(!l.data('placeholder-enabled')){return m.value=n}if(n==''){m.value=n;if(m!=h.activeElement){e.call(m)}}else{if(l.hasClass('placeholder')){b.call(m,true,n)||(m.value=n)}else{m.value=n}}return l}};a||(c.input=k);d||(c.textarea=k);$(function(){$(h).delegate('form','submit.placeholder',function(){var l=$('.placeholder',this).each(b);setTimeout(function(){l.each(e)},10)})});$(f).bind('beforeunload.placeholder',function(){$('.placeholder').each(function(){this.value=''})})}function g(m){var l={},n=/^jQuery\d+$/;$.each(m.attributes,function(p,o){if(o.specified&&!n.test(o.name)){l[o.name]=o.value}});return l}function b(m,n){var l=this,o=$(l);if(l.value==o.attr('placeholder')&&o.hasClass('placeholder')){if(o.data('placeholder-password')){o=o.hide().next().show().attr('id',o.removeAttr('id').data('placeholder-id'));if(m===true){return o[0].value=n}o.focus()}else{l.value='';o.removeClass('placeholder');l==h.activeElement&&l.select()}}}function e(){var q,l=this,p=$(l),m=p,o=this.id;if(l.value==''){if(l.type=='password'){if(!p.data('placeholder-textinput')){try{q=p.clone().attr({type:'text'})}catch(n){q=$('<input>').attr($.extend(g(this),{type:'text'}))}q.removeAttr('name').data({'placeholder-password':true,'placeholder-id':o}).bind('focus.placeholder',b);p.data({'placeholder-textinput':q,'placeholder-id':o}).before(q)}p=p.removeAttr('id').hide().prev().attr('id',o).show()}p.addClass('placeholder');p[0].value=p.attr('placeholder')}else{p.removeClass('placeholder')}}}(this,document,jQuery));

jQuery(document).ready(function(){
	
	jQuery("#cheetahWidget2012_div_textarea").append("<div id='cheetahWidget2012_div_textarea_helper' style='display:none'></div>");
	jQuery('#cheetahWidgetField_message').change(function(){ 
	    jQuery('#cheetahWidget2012_div_textarea_helper').html( nl2br( jQuery(this).val(), 0 ) );
	}); 
	
	jQuery('.widget_clear_error').keypress(function(e){		
		jQuery('.cheetahWidgetError_field').html(""); 
		
		if(dropifi_error_catch==true){
			current_height_iframe = normalHeight;
			jQuery('#change_iframe_height').trigger('click');
			dropifi_error_catch=false; 
		}
	}); 
	 
	jQuery('#change_iframe_height').click(function(e) {
		send_to_parent_iframe_height("changeHeight",current_height_iframe); 
	});

	var posi = Dropifi.buttonPosition;
 
	if(posi=='left'){
		jQuery('.cheetahWidget2012_button').css({
			'float':'right' 
		});
	}
	
	jQuery("#cheetahWidgetField_phone").keypress(function (e){										 
	    //if the letter is not digit then display error and don't type anything										
	    if(e.which == 45 || e.which == 43 || e.which == 8 || e.which == 0 || (e.which>=48 && e.which<=57) ){
	    	var phone = jQuery("#cheetahWidgetField_phone").val(); 
	    	if( e.which == 43 && jQuery.trim(phone) !=""){
	    		e.preventDefault(); 
	       	} 	
	    	
	    	if( e.which == 43 && phone.indexOf("+") !=-1){									    		 
	    		e.preventDefault(); 
	    	}	
	    }else{
	    	e.preventDefault();									    	 
	    }
	});	
	normalHeight = (document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
	jQuery('input, textarea').placeholder();
	
});
