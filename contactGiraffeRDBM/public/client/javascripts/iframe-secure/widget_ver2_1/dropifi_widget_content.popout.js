loadjsfile('https://s3.amazonaws.com/dropifi/js/widget/jquery.validate.pack.js');  
loadjsfile('https://s3.amazonaws.com/dropifi/js/widget/jquery.mailcheck.min.js'); 

var dropifi_error_catch = false;
var iframe;
var normalHeight;
var disclaimerHeight = 0;
var current_height_iframe;
var containerHeight;
var userLocation={};

if (typeof String.prototype.startsWith != 'function') { 
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}

function loadjsfile(filename){ 			 
	var co = document.createElement('script');  
	    co.type = 'text/javascript'; 
	    co.src = filename;	     
	    document.getElementsByTagName("head")[0].appendChild(co);
}

function replaceAll(txt, replace, with_this) {
	return txt.replace(new RegExp(replace, 'g'),with_this); 
}

function createUploader(){            
	var uploader = new qq.FileUploader({ 
	    element: document.getElementById('file-uploader'),
	    action: 'https://www.dropifi.com/widget/client', 
	    debug: false
	});            
}
 
function checkEmailDomain(a,b,c){
	var d = jQuery(b); var e = jQuery(a); var f= jQuery(b); var g=false;  
	
	e.mailcheck({suggested:function(a,b){ 
			var b="Oops!, did you mean <span class='cheetahWidget2012_suggestion' style='color:brown;cursor:pointer' title='click to add'>"+b.address+"@"+b.domain+"</span>?";
			f.html(b);
			g=true;
		}
	}); 
	
	var dropSuggestion = jQuery(".cheetahWidget2012_suggestion");	 
	if(dropSuggestion!=undefined){
		jQuery(".cheetahWidget2012_suggestion").click(function(){
			var email = jQuery(this).text(); 
			e.val(email); 
			jQuery(b).html("");	 		 
		});
	} 
	return g;
}

/* function the close the popout */
function closePopup(){
	setTimeout(function(){
		Wix.closeWindow();
	},3000);
}

function set_height_of_wix_iframe(){
	normalHeight = document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight;
	Wix.setHeight(normalHeight);
	
}

function set_wix_parent_height(){
	var height = (document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
	window.resizeTo((height + 150), 450);
}

var CreateMessage = {
	init:function(publicKey,userdata,pluginType,userLocation){
		this.publicKey=publicKey;
		this.userdata=userdata;
		this.pluginType=pluginType;
		this.userLocation=userLocation;
	},
	send: function(){
		sendMessageToServer(this.publicKey, this.userdata, this.pluginType, this.userLocation);
	},
	isSizeAllowed: function(){
		 if(this.fileSize && (this.getFileSize(this.fileSize).megabytes>25)){ 
			 return false; 
		 }
		 return true;
	},
	getFileSize: function(size){ 
		var sizes = {};
		if(size){  
			var bytes = size;
			var kilobytes = (bytes / 1024);
			var megabytes = (kilobytes / 1024);
			var gigabytes = (megabytes / 1024);
			var terabytes = (gigabytes / 1024);
			var petabytes = (terabytes / 1024);
			var exabytes = (petabytes / 1024);
			var zettabytes = (exabytes / 1024);
			var yottabytes = (zettabytes / 1024);
			
			sizes.bytes =bytes; 
			sizes.kilobytes=kilobytes;
			sizes.megabytes=megabytes;
			sizes.gigabytes=gigabytes;
			sizes.terabytes=terabytes;
			sizes.petabytes=petabytes;
			sizes.exabytes=exabytes;
			sizes.zettabytes=zettabytes;
			sizes.yottabytes=yottabytes;
		} 
		return sizes;
	},
	dropifiSerializeObject: function(elementId,elementId_sub){ 
	    var o = {};
	    var a = jQuery('#'+elementId).serializeArray();  
	    if(!a || a.length<1){
	    	var a = jQuery('#'+elementId_sub).serializeArray();
	    }
	    jQuery.each(a, function() {
	        if (o[this.name] !== undefined) { 
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        } 
	    });
	    
	    return o; 
	}
}

function setupFileUpload(){
	jQuery("#cheetahWidgetField_attachment_close").click(function(){
		var fileContent = jQuery("#cheetahWidgetField_fileContent");
		fileContent.html(fileContent.html()); 
		$('#cheetahWidgetField_attachment').bind("change",function(){
			afterFileChange(this);
		 });
		 jQuery('.widget_clear_error').trigger("keypress");
	});
	
	var options = { 
	        target:  '#droppedfiles',   // target element(s) to be updated with server response 
	        beforeSubmit:  function(){ // pre-submit callback 
	        	 
	        },  
	        success:function(data) {//post-submit callback 
	        	jQuery("#droppedfiles").html("");
	        	if(data){
	        		var json = jQuery.parseJSON(data);  
	        		//check if the file
		        	if(json.status){
		        		var hasError =false;
	        		   if(json.status==305){
	        			   jQuery('#cheetahWidgetError_attachement').html("* the selected file cannot be uploaded");
	        			   hasError= true;
	        		   }else if(json.status==301) { 
	        			   jQuery('#cheetahWidgetError_attachment').html("* the selected file size is more than 25MB");
	        			   hasError= true;
	        		   }
	        		   
	        		   if(hasError){
	        			   showSendButton();
	        			   jQuery(".cheetahWidgetError_field").css({'display':'inherit'});
		    				if(dropifi_error_catch==false){
		    					dropifi_error_catch=true;
		    					current_height_iframe = jQuery("#cheetahWidget2012_contactForm").height()+20;
		    					jQuery('#change_iframe_height').trigger('click');	
		    				}  
		    				return false;
	        			} 
		        	}
	        	}
	        	
	        	CreateMessage.send();
	        }  
	};
	
	 //bind the form using ajaxForm
	 $('#fileupload').ajaxForm(options); 
	 
	 $('#cheetahWidgetField_attachment').bind("change",function(){
		 afterFileChange(this);
	 });
}

function afterFileChange(e){
	try{ 
		 CreateMessage.isBlockedFile=false;
		 if(e && e.files && e.files[0] && e.files[0].size){
			 CreateMessage.fileSize=e.files[0].size; 
			 CreateMessage.fileType=e.files[0].type;  
		 }
		 
		 CreateMessage.fileName =jQuery("#cheetahWidgetField_attachment").val();
		 if(CreateMessage.fileName){
			 CreateMessage.fileExtension = CreateMessage.fileName.substr( (CreateMessage.fileName.lastIndexOf('.') +1) );
			 CreateMessage.isBlockedFile = isBlockedAttachFile(CreateMessage.fileExtension);
		 }
		 jQuery('.widget_clear_error').trigger("keypress"); 
	}catch(ee){ }
}

function isBlockedAttachFile(exten){
	if(exten){
		var extens = ".ade, .adp, .bat, .chm, .cmd, .com, .cpl, .exe, .hta, .ins, .isp, .jse, .lib, .mde, .msc, .msp, .mst, .pif, .scr, .sct, .shb, .sys, .vb, .vbe, .vbs, .vxd, .wsc, .wsf, .wsh".split(",");   
		jQuery.each(extens,function(key,val){
			if(jQuery.trim(val)== jQuery.trim(exten).toLowerCase())
				return true;
		});
	}
	return false;
}

function sendMessageToServer(publicKey,userdata,pluginType,userLocation){
	jQuery.ajax({
		  type:"POST",
		  url:"https://www.dropifi.com/clientswidgets/widget_iframe_sending_message",
		  data :{'publicKey':publicKey,'cs':userdata,'pluginType':pluginType,'location':userLocation},  
		  success: function(data) {  			  
			jQuery(".cheetahWidget2012_button").removeAttr("disabled");
			jQuery(".cheetahWidget2012_button").css({"display":"inherit"});
			var dropwidget_content =  jQuery('#cheetahWidget2012_content');
			containerHeight = dropwidget_content.height()-20;
			var message = data.callback.message;
			if(message==null || message=='null'|| message==undefined || message==''){
				message ="Thank you.";
			}

			dropwidget_content.height(containerHeight);
			dropwidget_content.html("<div id='cheetahWidget2012_button_callbackmessage'>"+ message+"</div>"); 
			   
			//auto_hide_drofpifi_widget();
			//$('#cheetahWidget2012_callback').html(data.message); 			
			hideProgress();
			//_gaq.push(['_trackEvent',Dropifi.hostUrl, 'Message Sent Successful']);
			closePopup();			 
		  },
		  error:function(data){
			showSendButton();
		    //_gaq.push(['_trackEvent',Dropifi.hostUrl, 'Message Sent failed']);	    
		} 
	});	
}

function showSendButton(){ 
	jQuery(".cheetahWidget2012_button").removeAttr("disabled"); 
    jQuery(".cheetahWidget2012_button").css({"display":"inherit"});
    hideProgress();
}

function showProgressButton(title){
   jQuery(".cheetahWidget2012_button").css({"display":"none"});
   jQuery(".cheetahWidget2012_button").attr("disabled", "disabled");
   showProgressTitle(title); 
}

function get_body_height(){
	return (document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
}

function doSaveCheetahWidget(publicKey){ 
 	var userdata = CreateMessage.dropifiSerializeObject('cheetahWidget2012_contactForm','cheetahWidget2012_contactForm_sub');
	userdata.message = jQuery("#cheetahWidget2012_div_textarea_helper").html();
	userdata.pageUrl = Dropifi.requestUrl;
	var txtMessage = jQuery("#cheetahWidgetField_message").val(); 
	//jQuery('#cheetahWidget2012_contactForm').serializeObject();  	
	//form validation 
	 
	var name = userdata.name; 
	var email = userdata.email; 
	var phone = userdata.phone; 
	var subject = userdata.subject; 	
	var message = userdata.message;
	
	var hasError= false; 
	
	if(userdata.email == undefined || userdata.email=='undefined'){
		userdata.email = jQuery('#cheetahWidgetField_email').val();
		email = userdata.email;
	}
	
	if(userdata.name  != undefined && jQuery.trim(userdata.name) == ""){
		var error_name = jQuery('#ename').val();
		if(error_name=='null' || jQuery.trim(error_name).length<1){
			error_name = "your name is required";
		}
		jQuery('#cheetahWidgetError_name').html("* "+error_name); 
		hasError= true; 
	}
	
    if(email != undefined && jQuery.trim(email) == ""){
    	var error_email = jQuery('#eemail').val();
		if(error_email=='null' || jQuery.trim(error_email).length<1){
			error_email = "your email is required";
		}
		jQuery('#cheetahWidgetError_email').html("* "+error_email);
		hasError= true; 
	}else if(IsEmail(email) == false){		   		
		 if(checkEmailDomain("#cheetahWidgetField_email", "#cheetahWidgetError_email", ".cheetahWidgetError_email") == false){			 
			 jQuery('#cheetahWidgetError_email').html("* your email format is not correct");
		 }		 
		hasError = true;  
	}
	
	/*
	if(phone != undefined && phone.trim() == ""){
		var error_phone = jQuery('#ephone').val();
		if(error_phone=='null' || jQuery.trim(error_phone).length<1){
			error_phone = "your phone is required";
		}
		$('#cheetahWidgetError_phone').html("*"+error_phone);
		hasError= true;
	}
	*/
	
	if(subject != undefined && jQuery.trim(subject) == ""){
		var error_subject = jQuery('#esubject').val();
		if(error_subject=='null' || jQuery.trim(error_subject).length<1){
			error_subject = "the subject of the message is required";
		}
		jQuery('#cheetahWidgetError_subject').html("* "+error_subject);
		hasError= true;
	} 	 
	
	if(txtMessage != undefined && jQuery.trim(txtMessage) == ""){ 
		var error_message = jQuery('#emessage').val();
		if(error_message=='null' || jQuery.trim(error_message).length<1){
			error_message = "your message is required";
		}
		jQuery('#cheetahWidgetError_message').html("* "+error_message);
		hasError= true;
	}
	
	if(hasError){
		if(dropifi_error_catch==false){
			dropifi_error_catch=true;
			jQuery('.cheetahWidgetError_field').css({'display':'inherit'});
		} 
		set_height_of_wix_iframe();
		return false;
	}
	
	//validation complete  
	var pluginType = Dropifi.pluginType; 
	publicKey = Dropifi.publicKey;  
    if(jQuery.trim(userdata.message)==""){
    	userdata.message = txtMessage;
    }

    Dropifi.sender=userdata.email;
    CreateMessage.init(publicKey, userdata, pluginType, userLocation);
    
    var fileSelected = jQuery("#cheetahWidgetField_attachment"); 
    if(fileSelected && fileSelected.val()){
 	   
 	   jQuery("#attachmentId").val(Dropifi.attachmentId);
 	   jQuery("#sender").val(userdata.email);
 	   CreateMessage.userdata.attachmentId=Dropifi.attachmentId;
 	  
 	   hasError = false;
 	   //check if the file
 	   if(CreateMessage.isBlockedFile){
 		   jQuery('#cheetahWidgetError_attachement').html("* the selected file cannot be uploaded");
 		   hasError= true;
 	   }else if(!CreateMessage.isSizeAllowed()){
 		   jQuery('#cheetahWidgetError_attachment').html("* the selected file size is more than 25MB");
 		   hasError= true;
 	   }
 	   
 	   if(hasError){ 
 			jQuery(".cheetahWidgetError_field").css({'display':'inherit'});
 			if(dropifi_error_catch==false){
 				dropifi_error_catch=true;
 			} 
 			set_height_of_wix_iframe();
 			return false; 
 	   }  
 	   showProgressButton("Attaching");
 	   jQuery("#submitAttachment").trigger("click");
    }else{
 	   //no file selected so submit message without a file 
 	   showProgressButton("");
 	   CreateMessage.userdata.attachmentId=null;
 	   CreateMessage.send();
    }
	
}

var auto_hide_drofpifi_widget=function(){ 
	//var height = jQuery("#cheetahWidget2012_contactForm").height();
	//(document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
	//var formHeight = jQuery("#cheetahWidget2012_contactForm").height() -0;// disclaimerHeight;
	//socket.postMessage("type:closeWindow, height:"+normalHeight);
}

var send_to_parent_iframe=function(type){ 
	var height = (document.body.scrollHeight || document.body.clientHeight || document.body.offsetHeight);
	send_to_parent_iframe_height(type,height);
}

var send_to_parent_iframe_height=function(type,height){ 	 
	socket.postMessage("type:"+type+",height:"+height);
}



function turnTextAreaIntoDiv(elem){
    var div = document.createElement("div");
    // Copy significant attributes, customize.
    div.className = elem.className;
    div.style.cssText = elem.style.cssText;
    div.id = elem.id;
    div.name = elem.name;
    div.contentEditable ="true"; 
    div.innerHTML = elem.innerHTML; //'<a>' turns in  &lt;a&gt;
    elem.parentNode.replaceChild(div, elem); 
}

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br>'; 

    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function showProgress(){jQuery(".cheetahWidget2012_submit_input").append('<div id="dropifi_widget_progress"><img src="https://s3.amazonaws.com/dropifi/images/ajax-loader.gif" alt="" /> </div>');}
function showProgressTitle(a){jQuery(".cheetahWidget2012_submit_input").append('<div id="dropifi_widget_progress"><img src="https://s3.amazonaws.com/dropifi/images/ajax-loader.gif" alt="" /> '+a+'</div>');}
function hideProgress(){jQuery("#dropifi_widget_progress").remove()}
function IsEmail(a){var b=/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;return b.test(a)}

/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(f,h,$){var a='placeholder' in h.createElement('input'),d='placeholder' in h.createElement('textarea'),i=$.fn,c=$.valHooks,k,j;if(a&&d){j=i.placeholder=function(){return this};j.input=j.textarea=true}else{j=i.placeholder=function(){var l=this;l.filter((a?'textarea':':input')+'[placeholder]').not('.placeholder').bind({'focus.placeholder':b,'blur.placeholder':e}).data('placeholder-enabled',true).trigger('blur.placeholder');return l};j.input=a;j.textarea=d;k={get:function(m){var l=$(m);return l.data('placeholder-enabled')&&l.hasClass('placeholder')?'':m.value},set:function(m,n){var l=$(m);if(!l.data('placeholder-enabled')){return m.value=n}if(n==''){m.value=n;if(m!=h.activeElement){e.call(m)}}else{if(l.hasClass('placeholder')){b.call(m,true,n)||(m.value=n)}else{m.value=n}}return l}};a||(c.input=k);d||(c.textarea=k);$(function(){$(h).delegate('form','submit.placeholder',function(){var l=$('.placeholder',this).each(b);setTimeout(function(){l.each(e)},10)})});$(f).bind('beforeunload.placeholder',function(){$('.placeholder').each(function(){this.value=''})})}function g(m){var l={},n=/^jQuery\d+$/;$.each(m.attributes,function(p,o){if(o.specified&&!n.test(o.name)){l[o.name]=o.value}});return l}function b(m,n){var l=this,o=$(l);if(l.value==o.attr('placeholder')&&o.hasClass('placeholder')){if(o.data('placeholder-password')){o=o.hide().next().show().attr('id',o.removeAttr('id').data('placeholder-id'));if(m===true){return o[0].value=n}o.focus()}else{l.value='';o.removeClass('placeholder');l==h.activeElement&&l.select()}}}function e(){var q,l=this,p=$(l),m=p,o=this.id;if(l.value==''){if(l.type=='password'){if(!p.data('placeholder-textinput')){try{q=p.clone().attr({type:'text'})}catch(n){q=$('<input>').attr($.extend(g(this),{type:'text'}))}q.removeAttr('name').data({'placeholder-password':true,'placeholder-id':o}).bind('focus.placeholder',b);p.data({'placeholder-textinput':q,'placeholder-id':o}).before(q)}p=p.removeAttr('id').hide().prev().attr('id',o).show()}p.addClass('placeholder');p[0].value=p.attr('placeholder')}else{p.removeClass('placeholder')}}}(this,document,jQuery));


jQuery(document).ready(function(){
	$("body").css({"background-color":"#"+Dropifi.bgColor}); 
	jQuery("#cheetahWidget2012_div_textarea").append("<div id='cheetahWidget2012_div_textarea_helper' style='display:none'></div>");
	  
	jQuery('#cheetahWidgetField_message').change(function(){
		jQuery('#cheetahWidget2012_div_textarea_helper').html( nl2br( jQuery(this).val(), 0 ) );
	});
	
	jQuery('.widget_clear_error').keypress(function(e){		
		jQuery('.cheetahWidgetError_field').html(""); 
		jQuery('.cheetahWidgetError_field').css({'display':'none'});
		if(dropifi_error_catch==true){
			dropifi_error_catch=false; 
			set_height_of_wix_iframe();
		}
	}); 
	 
	jQuery('#change_iframe_height').click(function(e) {
		set_height_of_wix_iframe(); 
	});
	 
	
	var posi = Dropifi.buttonPosition;
	//if(posi=='left'){
    jQuery('.cheetahWidget2012_button').attr({'style':'float:right;'});
	//}
 
	jQuery("#cheetahWidgetField_phone").keypress(function (e){										 
	    //if the letter is not digit then display error and don't type anything	
		 
	    if(e.which == 45 || e.which == 43 || e.which == 8 || e.which == 0 || (e.which>=48 && e.which<=57) ){
	    	var phone = jQuery("#cheetahWidgetField_phone").val(); 
	    	if( e.which == 43 && jQuery.trim(phone) !=""){
	    		e.preventDefault(); 
	       	} 	
	    	
	    	if( e.which == 43 && phone.indexOf("+") !=-1){									    		 
	    		e.preventDefault(); 
	    	}
	    	
	    }else{
	    	e.preventDefault(); 								    	 
	    }
	}); 
	
	jQuery('input, textarea').placeholder();
		
	var bgcolor = jQuery(".cheetahWidget2012_button").attr("ref_color");
	var bgshadow = jQuery(".cheetahWidget2012_button").attr("ref_shadow");
	var fontColor = jQuery(".cheetahWidget2012_button").attr("ref_fontColor");
	if(bgcolor && bgshadow && fontColor){		
		var textColor = {};
		textColor[0] ='-moz-linear-gradient(center bottom , '+bgcolor +' 100%, '+bgshadow+' 100%)';
		textColor[1] ='-webkit-gradient(linear, 0 100%, 0 0%, color-stop(1, '+bgcolor+'), to('+bgshadow+'))';
		textColor[2] ='-o-linear-gradient(linear, 0 100%, 0 0%, color-stop(1, '+bgcolor+'), to('+bgshadow+'))';
		textColor[3] ='linear-gradient(linear, 0 100%, 0 0%, color-stop(1, '+bgcolor+'), to('+bgshadow+'))';
		textColor[4] ='1px solid '+bgcolor; 
		 
		jQuery(".cheetahWidget2012_button").css({ 
			'background-color':bgcolor,
			'background-image':textColor[0],
			'border':textColor[4],
			'color':fontColor
		});
		
		jQuery(".cheetahWidget2012_button").css({'background-image':textColor[1]});
		jQuery(".cheetahWidget2012_button").css({'background-image':textColor[2]});
		jQuery(".cheetahWidget2012_button").css({'background-image':textColor[3]});
		
	}
	
	if(userLocation){ 
		userLocation.city=geoip_city();
		userLocation.region=geoip_region();
		userLocation.regionName=geoip_region_name();
		userLocation.postalCode=geoip_postal_code();
		userLocation.countryCode=geoip_country_code();
		userLocation.countryName=geoip_country_name();
		userLocation.latitude=geoip_latitude();
		userLocation.longitude=geoip_longitude();
	} 
	
	setupFileUpload();
	set_height_of_wix_iframe();
	//_gaq.push(['_trackEvent',Dropifi.hostUrl, 'Page View']);
});


/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-39173030-1']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
*/
