jQuery(document).ready(function(){
	jQuery("#formContent").css({'display':'none'});
	var data = jQuery.parseJSON(params); 
	if(data){ 
		Wix.getSiteInfo(function(siteInfo){
			var normalHeight = formHeight(data);
		// do something with the siteInfo
			$("#dropifi_widget_v1_contactable").click(function(){
				var bg="";
				if(data.buttonColor)
					bg=data.buttonColor.replace("#","");
				
				Wix.openModal('http://www.dropifi.com/clientswidgets/show_widget_content_popout?publicKey='+publickey+'&buttonPosition='+data.buttonPosition+'&pluginType='+pluginType+'&popout=POPOUT&bgcolor='+bg+'&hostUrl='+siteInfo.url, 400, normalHeight);
			});
		
			jQuery.ajax({
				"type":"POST",
				"url":"http://www.dropifi.com/websitebuilder/wix/updateurl",
				"data":{'publicKey':publickey,'baseUrl':siteInfo.baseUrl,'url':siteInfo.url},
				"success":function(data){ 
				},
				"error":function(data){
				}
			});
		});
		//var uid = Wix.Utils.getUid();
		//var instanceId = Wix.Utils.getInstanceId();
		
		 renderWidgetTab(data);
	}else{
		resetWidgetTab();
	}

});

function formHeight(data){
	
	var h = jQuery("#formContent").height(); //- jQuery(".cheetahWidget2012_disclaimer").outerHeight(true);	
	jQuery("#formContent").html("");
	jQuery("#formContent").css({'display':'none'});

	if(BrowserDetect.OS == 'iPhone/iPod'){
		//return (h + 50);
	}

	/*if(data.buttonPosition=="bottom" || data.buttonPosition=="top"){
		return (h+50);
	}*/
    return (h+25);
}

function bdt(){
	var dvc = BrowserDetect.OS;
}

function renderWidgetTab(data){  
	var posi = data.buttonPosition;
	var bcolor = data.buttonColor;
	var borderColor = '1px solid '+ data.buttonTextColor;	
	var len = (data.buttonText).length;
    
		jQuery("#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner").css({
			 'overflow':'hidden',
		 	 'position':'fixed',
		 	 '*position':"fixed",		
			 'z-index':'999999999',
			 '-moz-box-sizing':'content-box',
			 'box-sizing':'content-box',
			 '-webkit-box-sizing':'content-box',
			 'width':"35px",
			 'color':'#FFFFFF',
		 	 'background-color':bcolor,
		 	 'cursor':'pointer',
		 	 'top':'2%'
		});
	 
		 if((posi=="left") || (posi=="right")){
			 var padRight = (posi=='right')?'padding-right:0px':'padding-left:8px';
			 jQuery("#cheetahWidget2012_contactable_inner").html("<div id='dropifi2012_version_inner_label' " +
				"style='width:25px;margin-right:auto;margin-left:auto;"+padRight+"'>"
				+"<img style='padding:0px;max-width:25px;min-width:25px;width:35px;display:inherit !important;border:0px;margin-top:5px;margin-bottom:8px;box-shadow: 0 0 0 transparent;' src='"+data.imageText+"' /></div>"
			 );
			 borderRadius = (posi == "left" )?'0px 3px 3px 0px':'3px 0px 0px 3px'; 
		 }else{ 
			 var padRight = (posi=='bottom')?'margin-top:4px;margin-bottom:8px;':'margin-top:8px;margin-bottom:4px;';
			 jQuery("#cheetahWidget2012_contactable_inner").html("<div id='dropifi2012_version_inner_label' style='font-size:18px;text-align:center;"+padRight+"min-height:25px;max-height:25px;height:30px;'>"+data.buttonText+"</div>");
			 borderRadius=(posi=='bottom')?'3px 3px 0px 0px':'0px 0px 3px 3px';
		 }

		 jQuery("#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner").css({
			 'border-top':borderColor,
			 'border-bottom':borderColor,
			 'background': 'none repeat scroll 0 0 '+bcolor,
		     'border-radius':borderRadius,
		     'box-shadow': '0 0 4px rgba(0, 0, 0, 0.85)'
		 });
		 
		 if(posi == "left"){									 						  
			 //widget button appears at the right side						   
			  jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner').css({ 						 		 					 		
			 		'left':'0',
			 		'margin-left':'-5px',
			 		'*margin-left':'-5px', 								 								 		
			 		//'top':bPercent,
			 		'*margin-top':'10px',
			 		'border-right':borderColor
			 }); 
		 }else if(posi=="right"){
			 jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner').css({						 							 		 
			 		'right':'0',
			 		'margin-right':'-5px', 
			 		'*margin-right':'-5px', 						 		 					 		 
			 		//'top':bPercent,
			 		'*margin-top':'10px',
			 		'border-left':borderColor 
			 });
		 }else if(posi=="bottom"){
			 jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner').css({						 							 		 
			 		'bottom':'0',
			 		'margin-bottom':'-5px', 
			 		'*margin-bottom':'-5px', 						 		 					 		 
			 		//'top':bPercent,
			 		'*margin-top':'10px',
			 		'border-left':borderColor,
			 		'border-right':borderColor,
			 		'border-bottom':'none',
			 		//'width':(len + 100),
			 		'width':"98%",
			 		'height':'30px'			 		 
			 });
		 }else if(posi=="top"){
			 jQuery('#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner').css({						 							 		 
			 		'top':'0',
			 		'margin-top':'-5px', 
			 		'*margin-top':'-5px', 						 		 					 		 
			 		//'top':bPercent,
			 		'*margin-top':'10px',
			 		'border-left':borderColor,
			 		'border-right':borderColor,
			 		'border-top':'none',
			 		//'width':(len + 100),
			 		'width':"98%",
			 		'height':'30px'			 		 
			 });
		 }
}

function resetWidgetTab(){
	jQuery("#dropifi_widget_v1_contactable #cheetahWidget2012_contactable_inner").removeAttr('style');
	jQuery("#cheetahWidget2012_contactable_inner").html("");
	jQuery("#formContent").html("");
    jQuery("#formContent").css({'display':'none'});
}
