loadjsfile('//j.maxmind.com/js/geoip.js' type='text/javascript');
loadjsfile('//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js');
$(document).ready(function(){
	var geolocation = geoip_country_name();
    $("#dropifi_create_new_account").click(function(){
        var e=$("#requestUrl").val();
        var t=$("#accessToken").val();
        var n={};
        n.hostUrl=$("#hostUrl").val();
        n.requestType="SIGNUP";
        n.accessToken=t;
        n.requestUrl=e;
        n.displayName=$("#displayName").val();
        n.user_email=$("#user_email").val();
        n.user_re_password=$("#user_re_password").val();
        n.user_password=$("#user_password").val();
        n.user_domain=$("#user_domain").val();
        n.site_url=$("#site_url").val();
        n.location = geolocation;
        $("body").css("cursor","wait");
        $("#dropifi_create_new_account").css("cursor","wait");

        $.ajax({
            type:"GET",
            url:"https://www.dropifi.com/ecommerce/prestashop/signup.json",
            dataType:"jsonp",jsonp:"s",crossDomain:true,
            data:{'displayName':n.displayName,'user_email':n.user_email,'user_password':n.user_password,
                'user_re_password':n.user_re_password,'user_domain':n.user_domain,'hostUrl':n.hostUrl,
                'requestUrl':n.requestUrl,'accessToken':n.accessToken,'site_url':n.site_url,'type':"json"},
            success:function(t){
                if(t.status==200){
                    $("#up_requestType").val(n.requestType);
                    $("#up_userEmail").val(t.userEmail);
                    $("#up_temToken").val(t.temToken);
                    $("#up_status").val(t.status);
                    $("#up_publicKey").val(t.publicKey);    
                    $("#up_msg").val(t.msg);
                    $("#up_password").val("12345");

                    $("body").css("cursor","default");
                    $("#dropifi_create_new_account").css("cursor","pointer");
                    
                    $("#up_submit").trigger('click');
                    
                }else{
                    $("#dropifi_s_message_status").html(t.msg);
                    $("#dropifi_s_message_status").css({"background-color":"#de4343","border-color":"#c43d3d"});
                    $("body").css("cursor","default");
                    $("#dropifi_login_account").css("cursor","pointer")
                }
            },
            error:function(e){
                $("#dropifi_s_message_status").html("An error occurred while submiting your details, try creating the account again");
                $("#dropifi_s_message_status").css({"background-color":"#de4343","border-color":"#c43d3d"});
                $("body").css("cursor","default");
                $("#dropifi_create_new_account").css("cursor","pointer");
            }
        });
    });

    $("#dropifi_login_account").click(function(){
        var e=$("#l_requestUrl").val();
        var t={};
        t.requestType="LOGIN";
        t.login_email=$("#login_email").val();
        t.accessKey=$("#accessKey").val();
        t.accessToken=$("#l_accessToken").val();
        t.requestUrl=e;
        t.site_url=$("#l_site_url").val();
        t.location = geolocation;
        
        $("body").css("cursor","wait");
        $("#dropifi_login_account").css("cursor","wait");

        $.ajax({
            type:"GET",
            url:"https://www.dropifi.com/ecommerce/prestashop/loginToken.json",
            dataType:"jsonp",
            data:{'login_email':t.login_email,'accessKey':t.accessKey,'requestUrl':t.requestUrl,'accessToken':t.accessToken,'site_url':t.site_url,'type':"json"},
            jsonp:"s",
            crossDomain:true,
            success:function(n){
                if(n.status==200){
                    //update_publickey(e,t.requestType,n,"up_dropifi_login")
                    $("#up_requestType").val(n.requestType);
                    $("#up_userEmail").val(n.userEmail);
                    $("#up_temToken").val(n.temToken);
                    $("#up_status").val(n.status);
                    $("#up_publicKey").val(n.publicKey);    
                    $("#up_msg").val(n.msg);
                    $("#up_password").val("12345");

                    $("body").css("cursor","default");
                    $("#dropifi_login_account").css("cursor","pointer");
                    
                    //alert($("#up_msg").val());
                    $("#up_submit").trigger('click');
                                                         
                }else{
                    $("#dropifi_l_message_status").html(n.msg);
                    $("#dropifi_l_message_status").css({"background-color":"#de4343","border-color":"#c43d3d"});
                    $("body").css("cursor","default");
                    $("#dropifi_login_account").css("cursor","pointer");
                }
            },
            error:function(e){
                $("body").css("cursor","default");
                $("#dropifi_login_account").css("cursor","pointer");
                $("#dropifi_l_message_status").html("An error occurred while submiting your details, try logging into your account again");
                $("#dropifi_l_message_status").css({"background-color":"#de4343","border-color":"#c43d3d"});
            }
        });
    });
    
});
    