/*!
 * dropifibox - dropifiQuery Plugin
 * version: 2.0.6 (16/04/2012)
 * @requires dropifiQuery v1.6 or later
 *
 * Examples at http://fancyapps.com/dropifibox/
 * License: www.fancyapps.com/dropifibox/#license
 *
 * Copyright 2012 Janis Skarnelis - janis@fancyapps.com
 *
 */

(function (window, document, dropifiQuery, undefined) {
	"use strict";

	var W = dropifiQuery(window),
		D = dropifiQuery(document),
		F = dropifiQuery.dropifibox = function () {
			F.open.apply( this, arguments );
		},
		didResize	= false,
		resizeTimer	= null,
		isTouch		= document.createTouch !== undefined,
		isString	= function(str) {
			return dropifiQuery.type(str) === "string";
		},
		isPercentage = function(str) {
			return isString(str) && str.indexOf('%') > 0;
		},
		getValue = function(value, dim) {
			if (dim && isPercentage(value)) {
				value = F.getViewport()[ dim ] / 100 * parseInt(value, 10);
			}

			return Math.round(value) + 'px';
		};

	dropifiQuery.extend(F, {
		// The current version of dropifibox
		version: '2.0.5',

		defaults: {
			padding: 15,
			margin: 20,

			width: 800,
			height: 600,
			minWidth: 100,
			minHeight: 100,
			maxWidth: 9999,
			maxHeight: 9999,

			autoSize: true,
			autoResize: !isTouch,
			autoCenter : !isTouch,
			fitToView: true,
			aspectRatio: false,
			topRatio: 0.5,

			fixed: false,
			scrolling: 'auto', // 'auto', 'yes' or 'no'
			wrapCSS: '',

			arrows: true,
			closeBtn: true,
			closeClick: false,
			nextClick : false,
			mouseWheel: true,
			autoPlay: false,
			playSpeed: 3000,
			preload : 3,

			modal: false,
			loop: true,
			ajax: { dataType: 'html', headers: { 'X-dropifibox': true } },
			keys: {
				next: [13, 32, 34, 39, 40], // enter, space, page down, right arrow, down arrow
				prev: [8, 33, 37, 38], // backspace, page up, left arrow, up arrow
				close: [27] // escape key
			},

			// Override some properties
			index: 0,
			type: null,
			href: null,
			content: null,
			title: null,

			// HTML templates
			tpl: {
				wrap: '<div class="dropifibox-wrap"><div class="dropifibox-skin"><div class="dropifibox-outer"><div class="dropifibox-inner"></div></div></div></div>',
				image: '<img class="dropifibox-image" src="{href}" alt="" />',
				iframe: '<iframe class="dropifibox-iframe" name="dropifibox-frame{rnd}" frameborder="0" hspace="0"' + (dropifiQuery.browser.msie ? ' allowtransparency="true"' : '') + '></iframe>',
				swf: '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="wmode" value="transparent" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{href}" /><embed src="{href}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="100%" height="100%" wmode="transparent"></embed></object>',
				error: '<p class="dropifibox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
				closeBtn: '<div title="Close" class="dropifibox-item dropifibox-close"></div>',
				next: '<a title="Next" class="dropifibox-nav dropifibox-next"><span></span></a>',
				prev: '<a title="Previous" class="dropifibox-nav dropifibox-prev"><span></span></a>'
			},

			// Properties for each animation type
			// Opening dropifibox
			openEffect: 'fade', // 'elastic', 'fade' or 'none'
			openSpeed: 300,
			openEasing: 'swing',
			openOpacity: true,
			openMethod: 'zoomIn',

			// Closing dropifibox
			closeEffect: 'fade', // 'elastic', 'fade' or 'none'
			closeSpeed: 300,
			closeEasing: 'swing',
			closeOpacity: true,
			closeMethod: 'zoomOut',

			// Changing next gallery item
			nextEffect: 'elastic', // 'elastic', 'fade' or 'none'
			nextSpeed: 300,
			nextEasing: 'swing',
			nextMethod: 'changeIn',

			// Changing previous gallery item
			prevEffect: 'elastic', // 'elastic', 'fade' or 'none'
			prevSpeed: 300,
			prevEasing: 'swing',
			prevMethod: 'changeOut',

			// Enabled helpers
			helpers: {
				overlay: {
					speedIn: 0,
					speedOut: 300,
					opacity: 0.8,
					css: {
						cursor: 'pointer'
					},
					closeClick: true
				},
				title: {
					type: 'float' // 'float', 'inside', 'outside' or 'over'
				}
			},

			// Callbacks
			onCancel: dropifiQuery.noop, // If canceling
			beforeLoad: dropifiQuery.noop, // Before loading
			afterLoad: dropifiQuery.noop, // After loading
			beforeShow: dropifiQuery.noop, // Before changing in current item
			afterShow: dropifiQuery.noop, // After opening
			beforeClose: dropifiQuery.noop, // Before closing
			afterClose: dropifiQuery.noop // After closing
		},

		//Current state
		group: {}, // Selected group
		opts: {}, // Group options
		coming: null, // Element being loaded
		current: null, // Currently loaded element
		isOpen: false, // Is currently open
		isOpened: false, // Have been fully opened at least once
		wrap: null,
		skin: null,
		outer: null,
		inner: null,

		player: {
			timer: null,
			isActive: false
		},

		// Loaders
		ajaxLoad: null,
		imgPreload: null,

		// Some collections
		transitions: {},
		helpers: {},

		/*
		 *	Static methods
		 */

		open: function (group, opts) {
			//Kill existing instances
			F.close(true);

			//Normalize group
			if (group && !dropifiQuery.isArray(group)) {
				group = group instanceof dropifiQuery ? dropifiQuery(group).get() : [group];
			}

			F.isActive = true;

			//Extend the defaults
			F.opts = dropifiQuery.extend(true, {}, F.defaults, opts);

			//All options are merged recursive except keys
			if (dropifiQuery.isPlainObject(opts) && opts.keys !== undefined) {
				F.opts.keys = opts.keys ? dropifiQuery.extend({}, F.defaults.keys, opts.keys) : false;
			}

			F.group = group;

			F._start(F.opts.index || 0);
		},

		cancel: function () {
			if (F.coming && false === F.trigger('onCancel')) {
				return;
			}

			F.coming = null;

			F.hideLoading();

			if (F.ajaxLoad) {
				F.ajaxLoad.abort();
			}

			F.ajaxLoad = null;

			if (F.imgPreload) {
				F.imgPreload.onload = F.imgPreload.onabort = F.imgPreload.onerror = null;
			}
		},

		close: function (a) {
			F.cancel();

			if (!F.current || false === F.trigger('beforeClose')) {
				return;
			}

			F.unbindEvents();

			//If forced or is still opening then remove immediately
			if (!F.isOpen || (a && a[0] === true)) {
				dropifiQuery('.dropifibox-wrap').stop().trigger('onReset').remove();

				F._afterZoomOut();

			} else {
				F.isOpen = F.isOpened = false;

				dropifiQuery('.dropifibox-item, .dropifibox-nav').remove();

				F.wrap.stop(true).removeClass('dropifibox-opened');
				F.inner.css('overflow', 'hidden');

				F.transitions[F.current.closeMethod]();
			}
		},

		// Start/stop slideshow
		play: function (a) {
			var clear = function () {
					clearTimeout(F.player.timer);
				},
				set = function () {
					clear();

					if (F.current && F.player.isActive) {
						F.player.timer = setTimeout(F.next, F.current.playSpeed);
					}
				},
				stop = function () {
					clear();

					dropifiQuery('body').unbind('.player');

					F.player.isActive = false;

					F.trigger('onPlayEnd');
				},
				start = function () {
					if (F.current && (F.current.loop || F.current.index < F.group.length - 1)) {
						F.player.isActive = true;

						dropifiQuery('body').bind({
							'afterShow.player onUpdate.player': set,
							'onCancel.player beforeClose.player': stop,
							'beforeLoad.player': clear
						});

						set();

						F.trigger('onPlayStart');
					}
				};

			if (F.player.isActive || (a && a[0] === false)) {
				stop();
			} else {
				start();
			}
		},

		next: function () {
			if (F.current) {
				F.jumpto(F.current.index + 1);
			}
		},

		prev: function () {
			if (F.current) {
				F.jumpto(F.current.index - 1);
			}
		},

		jumpto: function (index) {
			if (!F.current) {
				return;
			}

			index = parseInt(index, 10);

			if (F.group.length > 1 && F.current.loop) {
				if (index >= F.group.length) {
					index = 0;

				} else if (index < 0) {
					index = F.group.length - 1;
				}
			}

			if (F.group[index] !== undefined) {
				F.cancel();

				F._start(index);
			}
		},

		reposition: function (e, onlyAbsolute) {
			var pos;

			if (F.isOpen) {
				pos = F._getPosition(onlyAbsolute);

				if (e && e.type === 'scroll') {
					delete pos.position;

					F.wrap.stop(true, true).animate(pos, 200);

				} else {
					F.wrap.css(pos);
				}
			}
		},

		update: function (e) {
			if (!F.isOpen) {
				return;
			}

			// Run this code after a delay for better performance
			if (!didResize) {
				resizeTimer = setTimeout(function () {
					var current = F.current, anyway = !e || (e && e.type === 'orientationchange');

					if (didResize) {
						didResize = false;

						if (!current) {
							return;
						}

						if ((!e || e.type !== 'scroll') || anyway) {
							if (current.autoSize && current.type !== 'iframe') {
								F.inner.height('auto');
								current.height = F.inner.height();
							}

							if (current.autoResize || anyway) {
								F._setDimension();
							}

							if (current.canGrow && current.type !== 'iframe') {
								F.inner.height('auto');
							}
						}

						if (current.autoCenter || anyway) {
							F.reposition(e);
						}

						F.trigger('onUpdate');
					}
				}, 200);
			}

			didResize = true;
		},

		toggle: function () {
			if (F.isOpen) {
				F.current.fitToView = !F.current.fitToView;

				F.update();
			}
		},

		hideLoading: function () {
			D.unbind('keypress.fb');

			dropifiQuery('#dropifibox-loading').remove();
		},

		showLoading: function () {
			F.hideLoading();

			//If user will press the escape-button, the request will be canceled
			D.bind('keypress.fb', function(e) {
				if (e.keyCode === 27) {
					e.preventDefault();
					F.cancel();
				}
			});

			dropifiQuery('<div id="dropifibox-loading"><div></div></div>').click(F.cancel).appendTo('body');
		},

		getViewport: function () {
			// See http://bugs.dropifiQuery.com/ticket/6724
			return {
				x: W.scrollLeft(),
				y: W.scrollTop(),
				w: isTouch && window.innerWidth ? window.innerWidth : W.width(),
				h: isTouch && window.innerHeight ? window.innerHeight : W.height()
			};
		},

		// Unbind the keyboard / clicking actions
		unbindEvents: function () {
			if (F.wrap) {
				F.wrap.unbind('.fb');
			}

			D.unbind('.fb');
			W.unbind('.fb');
		},

		bindEvents: function () {
			var current = F.current,
				keys = current.keys;

			if (!current) {
				return;
			}

			W.bind('resize.fb orientationchange.fb' + (current.autoCenter && !current.fixed ? ' scroll.fb' : ''), F.update);

			if (keys) {
				D.bind('keydown.fb', function (e) {
					var code, target = e.target || e.srcElement;

					// Ignore key combinations and key events within form elements
					if (!e.ctrlKey && !e.altKey && !e.shiftKey && !e.metaKey && !(target && (target.type || dropifiQuery(target).is('[contenteditable]')))) {
						code = e.keyCode;

						if (dropifiQuery.inArray(code, keys.close) > -1) {
							F.close();
							e.preventDefault();

						} else if (dropifiQuery.inArray(code, keys.next) > -1) {
							F.next();
							e.preventDefault();

						} else if (dropifiQuery.inArray(code, keys.prev) > -1) {
							F.prev();
							e.preventDefault();
						}
					}
				});
			}

			if (dropifiQuery.fn.mousewheel && current.mouseWheel && F.group.length > 1) {
				F.wrap.bind('mousewheel.fb', function (e, delta) {
					var target = e.target || null;

					if (delta !== 0 && (!target || target.clientHeight === 0 || (target.scrollHeight === target.clientHeight && target.scrollWidth === target.clientWidth))) {
						e.preventDefault();

						F[delta > 0 ? 'prev' : 'next']();
					}
				});
			}
		},

		trigger: function (event, o) {
			var ret, obj = o || F[ dropifiQuery.inArray(event, ['onCancel', 'beforeLoad', 'afterLoad']) > -1 ? 'coming' : 'current' ];

			if (!obj) {
				return;
			}

			if (dropifiQuery.isFunction( obj[event] )) {
				ret = obj[event].apply(obj, Array.prototype.slice.call(arguments, 1));
			}

			if (ret === false) {
				return false;
			}

			if (obj.helpers) {
				dropifiQuery.each(obj.helpers, function (helper, opts) {
					if (opts && dropifiQuery.isPlainObject(F.helpers[helper]) && dropifiQuery.isFunction(F.helpers[helper][event])) {
						F.helpers[helper][event](opts, obj);
					}
				});
			}

			dropifiQuery.event.trigger(event + '.fb');
		},

		isImage: function (str) {
			return isString(str) && str.match(/\.(jpe?g|gif|png|bmp)((\?|#).*)?dropifiQuery/i);
		},

		isSWF: function (str) {
			return isString(str) && str.match(/\.(swf)((\?|#).*)?dropifiQuery/i);
		},

		_start: function (index) {
			var coming = {},
				element = F.group[index] || null,
				isDom,
				href,
				type,
				rez,
				hrefParts;

			if (element && (element.nodeType || element instanceof dropifiQuery)) {
				isDom = true;

				if (dropifiQuery.metadata) {
					coming = dropifiQuery(element).metadata();
				}
			}

			coming = dropifiQuery.extend(true, {}, F.opts, {index : index, element : element}, (dropifiQuery.isPlainObject(element) ? element : coming));

			// Re-check overridable options
			dropifiQuery.each(['href', 'title', 'content', 'type'], function(i,v) {
				coming[v] = F.opts[ v ] || (isDom && dropifiQuery(element).attr( v )) || coming[ v ] || null;
			});

			// Convert margin property to array - top, right, bottom, left
			if (typeof coming.margin === 'number') {
				coming.margin = [coming.margin, coming.margin, coming.margin, coming.margin];
			}

			// 'modal' propery is just a shortcut
			if (coming.modal) {
				dropifiQuery.extend(true, coming, {
					closeBtn : false,
					closeClick: false,
					nextClick : false,
					arrows : false,
					mouseWheel : false,
					keys : null,
					helpers: {
						overlay : {
							css: {
								cursor : 'auto'
							},
							closeClick : false
						}
					}
				});
			}

			//Give a chance for callback or helpers to update coming item (type, title, etc)
			F.coming = coming;

			if (false === F.trigger('beforeLoad')) {
				F.coming = null;
				return;
			}

			type = coming.type;
			href = coming.href || element;

			///Check if content type is set, if not, try to get
			if (!type) {
				if (isDom) {
					type = dropifiQuery(element).data('dropifibox-type');

					if (!type) {
						rez = element.className.match(/dropifibox\.(\w+)/);
						type = rez ? rez[1] : null;
					}
				}

				if (!type && isString(href)) {
					if (F.isImage(href)) {
						type = 'image';

					} else if (F.isSWF(href)) {
						type = 'swf';

					} else if (href.match(/^#/)) {
						type = 'inline';
					}
				}

				// ...if not - display element itself
				if (!type) {
					type = isDom ? 'inline' : 'html';
				}

				coming.type = type;
			}

			// Check before try to load; 'inline' and 'html' types need content, others - href
			if (type === 'inline' || type === 'html') {
				if (!coming.content) {
					if (type === 'inline') {
						coming.content = dropifiQuery( isString(href) ? href.replace(/.*(?=#[^\s]+dropifiQuery)/, '') : href ); //strip for ie7

					} else {
						coming.content = element;
					}
				}

				if (!coming.content || !coming.content.length) {
					type = null;
				}

			} else if (!href) {
				type = null;
			}

			/*
			 * Add reference to the group, so it`s possible to access from callbacks, example:
			 * afterLoad : function() {
			 *     this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
			 * }
			 */

			if (type === 'ajax' && isString(href)) {
				hrefParts = href.split(/\s+/, 2);

				href = hrefParts.shift();
				coming.selector = hrefParts.shift();
			}

			coming.href  = href;
			coming.group = F.group;
			coming.isDom = isDom;

			switch (type) {
				case 'image':
					F._loadImage();
					break;

				case 'ajax':
					F._loadAjax();
					break;

				case 'inline':
				case 'iframe':
				case 'swf':
				case 'html':
					F._afterLoad();
					break;

				default:
					F._error( 'type' );
			}
		},

		_error: function ( type ) {
			F.hideLoading();

			dropifiQuery.extend(F.coming, {
				type      : 'html',
				autoSize  : true,
				minWidth  : 0,
				minHeight : 0,
				padding   : 15,
				hasError  : type,
				content   : F.coming.tpl.error
			});

			F._afterLoad();
		},

		_loadImage: function () {
			// Reset preload image so it is later possible to check "complete" property
			var img = F.imgPreload = new Image();

			img.onload = function () {
				this.onload = this.onerror = null;

				F.coming.width  = this.width;
				F.coming.height = this.height;

				F._afterLoad();
			};

			img.onerror = function () {
				this.onload = this.onerror = null;

				F._error( 'image' );
			};

			img.src = F.coming.href;

			if (img.complete === undefined || !img.complete) {
				F.showLoading();
			}
		},

		_loadAjax: function () {
			F.showLoading();

			F.ajaxLoad = dropifiQuery.ajax(dropifiQuery.extend({}, F.coming.ajax, {
				url: F.coming.href,
				error: function (jqXHR, textStatus) {
					if (F.coming && textStatus !== 'abort') {
						F._error( 'ajax', jqXHR );

					} else {
						F.hideLoading();
					}
				},
				success: function (data, textStatus) {
					if (textStatus === 'success') {
						F.coming.content = data;

						F._afterLoad();
					}
				}
			}));
		},

		_preloadImages: function() {
			var group = F.group,
				current = F.current,
				len = group.length,
				item,
				href,
				i,
				cnt = Math.min(current.preload, len - 1);

			if (!current.preload || group.length < 2) {
				return;
			}

			for (i = 1; i <= cnt; i += 1) {
				item = group[ (current.index + i ) % len ];
				href = item.href || dropifiQuery( item ).attr('href') || item;

				if (item.type === 'image' || F.isImage(href)) {
					new Image().src = href;
				}
			}
		},

		_afterLoad: function () {
			F.hideLoading();

			if (!F.coming || false === F.trigger('afterLoad', F.current)) {
				F.coming = false;

				return;
			}

			if (F.isOpened) {
				dropifiQuery('.dropifibox-item, .dropifibox-nav').remove();

				F.wrap.stop(true).removeClass('dropifibox-opened');
				F.inner.css('overflow', 'hidden');

				F.transitions[F.current.prevMethod]();

			} else {
				dropifiQuery('.dropifibox-wrap').stop().trigger('onReset').remove();

				F.trigger('afterClose');
			}

			F.unbindEvents();

			F.isOpen    = false;
			F.current   = F.coming;

			//Build the neccessary markup
			F.wrap  = dropifiQuery(F.current.tpl.wrap).addClass('dropifibox-' + (isTouch ? 'mobile' : 'desktop') + ' dropifibox-type-' + F.current.type + ' dropifibox-tmp ' + F.current.wrapCSS).appendTo('body');
			F.skin  = dropifiQuery('.dropifibox-skin', F.wrap).css('padding', getValue(F.current.padding));
			F.outer = dropifiQuery('.dropifibox-outer', F.wrap);
			F.inner = dropifiQuery('.dropifibox-inner', F.wrap);

			F._setContent();
		},

		_setContent: function () {
			var current = F.current,
				content = current.content,
				type    = current.type,
				minWidth    = current.minWidth,
				minHeight   = current.minHeight,
				maxWidth    = current.maxWidth,
				maxHeight   = current.maxHeight,
				loadingBay;

			switch (type) {
				case 'inline':
				case 'ajax':
				case 'html':
					if (current.selector) {
						content = dropifiQuery('<div>').html(content).find(current.selector);

					} else if (content instanceof dropifiQuery) {
						if (content.parent().hasClass('dropifibox-inner')) {
							content.parents('.dropifibox-wrap').unbind('onReset');
						}

						content = content.show().detach();

						dropifiQuery(F.wrap).bind('onReset', function () {
							content.appendTo('body').hide();
						});
					}

					if (current.autoSize) {
						loadingBay = dropifiQuery('<div class="dropifibox-wrap ' + F.current.wrapCSS + ' dropifibox-tmp"></div>')
							.appendTo('body')
							.css({
								minWidth    : getValue(minWidth, 'w'),
								minHeight   : getValue(minHeight, 'h'),
								maxWidth    : getValue(maxWidth, 'w'),
								maxHeight   : getValue(maxHeight, 'h')
							})
							.append(content);

						current.width = loadingBay.width();
						current.height = loadingBay.height();

						// Re-check to fix 1px bug in some browsers
						loadingBay.width( F.current.width );

						if (loadingBay.height() > current.height) {
							loadingBay.width(current.width + 1);

							current.width = loadingBay.width();
							current.height = loadingBay.height();
						}

						content = loadingBay.contents().detach();

						loadingBay.remove();
					}

					break;

				case 'image':
					content = current.tpl.image.replace('{href}', current.href);

					current.aspectRatio = true;
					break;

				case 'swf':
					content = current.tpl.swf.replace(/\{width\}/g, current.width).replace(/\{height\}/g, current.height).replace(/\{href\}/g, current.href);
					break;

				case 'iframe':
					content = dropifiQuery(current.tpl.iframe.replace('{rnd}', new Date().getTime()) )
						.attr('scrolling', current.scrolling)
						.attr('src', current.href);

					current.scrolling = isTouch ? 'scroll' : 'auto';

					break;
			}

			if (type === 'image' || type === 'swf') {
				current.autoSize = false;
				current.scrolling = 'visible';
			}

			if (type === 'iframe' && current.autoSize) {
				F.showLoading();

				F._setDimension();

				F.inner.css('overflow', current.scrolling);

				content.bind({
					onCancel : function() {
						dropifiQuery(this).unbind();

						F._afterZoomOut();
					},
					load : function() {
						F.hideLoading();

						try {
							if (this.contentWindow.document.location) {
								F.current.height = dropifiQuery(this).contents().find('body').height();
							}
						} catch (e) {
							F.current.autoSize = false;
						}

						F[ F.isOpen ? '_afterZoomIn' : '_beforeShow']();
					}
				}).appendTo(F.inner);

			} else {
				F.inner.append(content);

				F._beforeShow();
			}
		},

		_beforeShow : function() {
			F.coming = null;

			//Give a chance for helpers or callbacks to update elements
			F.trigger('beforeShow');

			//Set initial dimensions and hide
			F._setDimension();
			F.wrap.hide().removeClass('dropifibox-tmp');

			F.bindEvents();

			F._preloadImages();

			F.transitions[ F.isOpened ? F.current.nextMethod : F.current.openMethod ]();
		},

		_setDimension: function () {
			var wrap      = F.wrap,
				inner     = F.inner,
				current   = F.current,
				viewport  = F.getViewport(),
				margin    = current.margin,
				padding2  = current.padding * 2,
				width     = current.width,
				height    = current.height,
				maxWidth  = current.maxWidth + padding2,
				maxHeight = current.maxHeight + padding2,
				minWidth  = current.minWidth + padding2,
				minHeight = current.minHeight + padding2,
				ratio,
				height_;

			viewport.w -= (margin[1] + margin[3]);
			viewport.h -= (margin[0] + margin[2]);

			if (isPercentage(width)) {
				width = (((viewport.w - padding2) * parseFloat(width)) / 100);
			}

			if (isPercentage(height)) {
				height = (((viewport.h - padding2) * parseFloat(height)) / 100);
			}

			ratio  = width / height;
			width  += padding2;
			height += padding2;

			if (current.fitToView) {
				maxWidth  = Math.min(viewport.w, maxWidth);
				maxHeight = Math.min(viewport.h, maxHeight);
			}

			if (current.aspectRatio) {
				if (width > maxWidth) {
					width = maxWidth;
					height = ((width - padding2) / ratio) + padding2;
				}

				if (height > maxHeight) {
					height = maxHeight;
					width = ((height - padding2) * ratio) + padding2;
				}

				if (width < minWidth) {
					width = minWidth;
					height = ((width - padding2) / ratio) + padding2;
				}

				if (height < minHeight) {
					height = minHeight;
					width = ((height - padding2) * ratio) + padding2;
				}

			} else {
				width = Math.max(minWidth, Math.min(width, maxWidth));
				height = Math.max(minHeight, Math.min(height, maxHeight));
			}

			width = Math.round(width);
			height = Math.round(height);

			//Reset dimensions
			dropifiQuery(wrap.add(inner)).width('auto').height('auto');

			inner.width(width - padding2).height(height - padding2);
			wrap.width(width);

			height_ = wrap.height(); // Real wrap height

			//Fit wrapper inside
			if (width > maxWidth || height_ > maxHeight) {
				while ((width > maxWidth || height_ > maxHeight) && width > minWidth && height_ > minHeight) {
					height = height - 10;

					if (current.aspectRatio) {
						width = Math.round(((height - padding2) * ratio) + padding2);

						if (width < minWidth) {
							width = minWidth;
							height = ((width - padding2) / ratio) + padding2;
						}

					} else {
						width = width - 10;
					}

					inner.width(width - padding2).height(height - padding2);
					wrap.width(width);

					height_ = wrap.height();
				}
			}

			current.dim = {
				width	: getValue(width),
				height	: getValue(height_)
			};

			current.canGrow		= current.autoSize && height > minHeight && height < maxHeight;
			current.canShrink	= false;
			current.canExpand	= false;

			if ((width - padding2) < current.width || (height - padding2) < current.height) {
				current.canExpand = true;

			} else if ((width > viewport.w || height_ > viewport.h) && width > minWidth && height > minHeight) {
				current.canShrink = true;
			}

			F.innerSpace = height_ - padding2 - inner.height();
		},

		_getPosition: function (onlyAbsolute) {
			var current		= F.current,
				viewport    = F.getViewport(),
				margin      = current.margin,
				width       = F.wrap.width() + margin[1] + margin[3],
				height      = F.wrap.height() + margin[0] + margin[2],
				rez         = {
					position: 'absolute',
					top  : margin[0] + viewport.y,
					left : margin[3] + viewport.x
				};

			if (current.autoCenter && current.fixed && !onlyAbsolute && height <= viewport.h && width <= viewport.w) {
				rez = {
					position: 'fixed',
					top  : margin[0],
					left : margin[3]
				};
			}

			rez.top     = getValue(Math.max(rez.top, rez.top + ((viewport.h - height) * current.topRatio)));
			rez.left    = getValue(Math.max(rez.left, rez.left + ((viewport.w - width) * 0.5)));

			return rez;
		},

		_afterZoomIn: function () {
			var current = F.current, scrolling = current ? current.scrolling : 'no';

			if (!current) {
				return;
			}

			F.isOpen = F.isOpened = true;

			F.wrap.addClass('dropifibox-opened');

			F.inner.css('overflow', scrolling === 'yes' ? 'scroll' : (scrolling === 'no' ? 'hidden' : scrolling));

			F.trigger('afterShow');

			F.update();

			//Assign a click event
			if (current.closeClick || current.nextClick) {
				F.inner.css('cursor', 'pointer').bind('click.fb', function(e) {
					if (!dropifiQuery(e.target).is('a') && !dropifiQuery(e.target).parent().is('a')) {
						F[ current.closeClick ? 'close' : 'next' ]();
					}
				});
			}

			//Create a close button
			if (current.closeBtn) {
				dropifiQuery(current.tpl.closeBtn).appendTo(F.skin).bind('click.fb', F.close);
			}

			//Create navigation arrows
			if (current.arrows && F.group.length > 1) {
				if (current.loop || current.index > 0) {
					dropifiQuery(current.tpl.prev).appendTo(F.outer).bind('click.fb', F.prev);
				}

				if (current.loop || current.index < F.group.length - 1) {
					dropifiQuery(current.tpl.next).appendTo(F.outer).bind('click.fb', F.next);
				}
			}

			if (F.opts.autoPlay && !F.player.isActive) {
				F.opts.autoPlay = false;

				F.play();
			}
		},

		_afterZoomOut: function () {
			var current = F.current;

			F.wrap.trigger('onReset').remove();

			dropifiQuery.extend(F, {
				group: {},
				opts: {},
				current: null,
				isActive: false,
				isOpened: false,
				isOpen: false,
				wrap: null,
				skin: null,
				outer: null,
				inner: null
			});

			F.trigger('afterClose', current);
		}
	});

	/*
	 *	Default transitions
	 */

	F.transitions = {
		getOrigPosition: function () {
			var current = F.current,
				element = current.element,
				padding = current.padding,
				orig    = dropifiQuery(current.orig),
				pos     = {},
				width   = 50,
				height  = 50,
				viewport;

			if (!orig.length && current.isDom && dropifiQuery(element).is(':visible')) {
				orig = dropifiQuery(element).find('img:first');

				if (!orig.length) {
					orig = dropifiQuery(element);
				}
			}

			if (orig.length) {
				pos = orig.offset();

				if (orig.is('img')) {
					width = orig.outerWidth();
					height = orig.outerHeight();
				}

			} else {
				viewport = F.getViewport();

				pos.top  = viewport.y + (viewport.h - height) * 0.5;
				pos.left = viewport.x + (viewport.w - width) * 0.5;
			}

			pos = {
				top     : getValue(pos.top - padding),
				left    : getValue(pos.left - padding),
				width   : getValue(width + padding * 2),
				height  : getValue(height + padding * 2)
			};

			return pos;
		},

		step: function (now, fx) {
			var prop = fx.prop, value, ratio;

			if (prop === 'width' || prop === 'height') {
				value = Math.ceil(now - (F.current.padding * 2));

				if (prop === 'height') {
					ratio = (now - fx.start) / (fx.end - fx.start);

					if (fx.start > fx.end) {
						ratio = 1 - ratio;
					}

					value -= F.innerSpace * ratio;
				}

				F.inner[prop](value);
			}
		},

		zoomIn: function () {
			var wrap     = F.wrap,
				current  = F.current,
				effect   = current.openEffect,
				elastic  = effect === 'elastic',
				dim      = current.dim,
				startPos = dropifiQuery.extend({}, dim, F._getPosition( elastic )),
				endPos   = dropifiQuery.extend({opacity : 1}, startPos);

			//Remove "position" property that breaks older IE
			delete endPos.position;

			if (elastic) {
				startPos = this.getOrigPosition();

				if (current.openOpacity) {
					startPos.opacity = 0;
				}

				F.outer.add(F.inner).width('auto').height('auto');

			} else if (effect === 'fade') {
				startPos.opacity = 0;
			}

			wrap.css(startPos)
				.show()
				.animate(endPos, {
					duration : effect === 'none' ? 0 : current.openSpeed,
					easing   : current.openEasing,
					step     : elastic ? this.step : null,
					complete : F._afterZoomIn
				});
		},

		zoomOut: function () {
			var wrap     = F.wrap,
				current  = F.current,
				effect   = current.openEffect,
				elastic  = effect === 'elastic',
				endPos   = {opacity : 0};

			if (elastic) {
				if (wrap.css('position') === 'fixed') {
					wrap.css(F._getPosition(true));
				}

				endPos = this.getOrigPosition();

				if (current.closeOpacity) {
					endPos.opacity = 0;
				}
			}

			wrap.animate(endPos, {
				duration : effect === 'none' ? 0 : current.closeSpeed,
				easing   : current.closeEasing,
				step     : elastic ? this.step : null,
				complete : F._afterZoomOut
			});
		},

		changeIn: function () {
			var wrap     = F.wrap,
				current  = F.current,
				effect   = current.nextEffect,
				elastic  = effect === 'elastic',
				startPos = F._getPosition( elastic ),
				endPos   = { opacity : 1 };

			startPos.opacity = 0;

			if (elastic) {
				startPos.top = getValue(parseInt(startPos.top, 10) - 200);
				endPos.top = '+=200px';
			}

			wrap.css(startPos)
				.show()
				.animate(endPos, {
					duration : effect === 'none' ? 0 : current.nextSpeed,
					easing   : current.nextEasing,
					complete : F._afterZoomIn
				});
		},

		changeOut: function () {
			var wrap     = F.wrap,
				current  = F.current,
				effect   = current.prevEffect,
				endPos   = { opacity : 0 },
				cleanUp  = function () {
					dropifiQuery(this).trigger('onReset').remove();
				};

			wrap.removeClass('dropifibox-opened');

			if (effect === 'elastic') {
				endPos.top = '+=200px';
			}

			wrap.animate(endPos, {
				duration : effect === 'none' ? 0 : current.prevSpeed,
				easing   : current.prevEasing,
				complete : cleanUp
			});
		}
	};

	/*
	 *	Overlay helper
	 */

	F.helpers.overlay = {
		overlay: null,

		update: function () {
			var width, scrollWidth, offsetWidth;

			//Reset width/height so it will not mess
			this.overlay.width('100%').height('100%');

			if (dropifiQuery.browser.msie || isTouch) {
				scrollWidth = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth);
				offsetWidth = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth);

				width = scrollWidth < offsetWidth ? W.width() : scrollWidth;

			} else {
				width = D.width();
			}

			this.overlay.width(width).height(D.height());
		},

		beforeShow: function (opts) {
			if (this.overlay) {
				return;
			}

			opts = dropifiQuery.extend(true, {}, F.defaults.helpers.overlay, opts);

			this.overlay = dropifiQuery('<div id="dropifibox-overlay"></div>').css(opts.css).appendTo('body');

			if (opts.closeClick) {
				this.overlay.bind('click.fb', F.close);
			}

			if (F.current.fixed && !isTouch) {
				this.overlay.addClass('overlay-fixed');

			} else {
				this.update();

				this.onUpdate = function () {
					this.update();
				};
			}

			this.overlay.fadeTo(opts.speedIn, opts.opacity);
		},

		afterClose: function (opts) {
			if (this.overlay) {
				this.overlay.fadeOut(opts.speedOut || 0, function () {
					dropifiQuery(this).remove();
				});
			}

			this.overlay = null;
		}
	};

	/*
	 *	Title helper
	 */

	F.helpers.title = {
		beforeShow: function (opts) {
			var title, text = F.current.title;

			if (text) {
				title = dropifiQuery('<div class="dropifibox-title dropifibox-title-' + opts.type + '-wrap">' + text + '</div>').appendTo('body');

				if (opts.type === 'float') {
					//This helps for some browsers
					title.width(title.width());

					title.wrapInner('<span class="child"></span>');

					//Increase bottom margin so this title will also fit into viewport
					F.current.margin[2] += Math.abs(parseInt(title.css('margin-bottom'), 10));
				}

				title.appendTo(opts.type === 'over' ? F.inner : (opts.type === 'outside' ? F.wrap : F.skin));
			}
		}
	};

	// jQuery plugin initialization
	dropifiQuery.fn.dropifibox = function (options) {
		var that     = dropifiQuery(this),
			selector = this.selector || '',
			index,
			run      = function(e) {
				var what = this, idx = index, relType, relVal;

				if (!(e.ctrlKey || e.altKey || e.shiftKey || e.metaKey) && !dropifiQuery(what).is('.dropifibox-wrap')) {
					e.preventDefault();

					relType = options.groupAttr || 'data-dropifibox-group';
					relVal  = dropifiQuery(what).attr(relType);

					if (!relVal) {
						relType = 'rel';
						relVal  = what[ relType ];
					}

					if (relVal && relVal !== '' && relVal !== 'nofollow') {
						what = selector.length ? dropifiQuery(selector) : that;
						what = what.filter('[' + relType + '="' + relVal + '"]');
						idx  = what.index(this);
					}

					options.index = idx;

					F.open(what, options);
				}
			};

		options = options || {};
		index   = options.index || 0;

		if (selector) {
			D.undelegate(selector, 'click.fb-start').delegate(selector, 'click.fb-start', run);

		} else {
			that.unbind('click.fb-start').bind('click.fb-start', run);
		}

		return this;
	};

	// Test for fixedPosition needs a body at doc ready
	dropifiQuery(document).ready(function() {
		F.defaults.fixed = dropifiQuery.support.fixedPosition || (!(dropifiQuery.browser.msie && dropifiQuery.browser.version <= 6) && !isTouch);
	});

}(window, document, dropifiQuery));