var userLocation = {}; 
var CreateMessage = {
	init: function(options){ 
		this.publicKey=options.publicKey;
		this.pluginType =options.pluginType;
		this.userdata = {};
		this.elementId =options.elementId;
		this.userLocation = userLocation;
		this.url=options.url;
		this.requestUrl = location.href;
	},
	send: function(onSuccess,onError){
		alert("Url "+this.requestUrl);
		this.userdata = this.dropifiSerializeObject(this.elementId);
		this.userdata.pageUrl = location.href;
		jQuery.ajax({
			type:"GET", 
			url:"//appengine.dropifi.com/clientswidgets/widget_sending_message.json", 
			dataType: "jsonp",
			async:true,
			data :{'publicKey':this.publicKey,'cs':this.userdata,'pluginType':this.pluginType,'location':this.userLocation,'requestUrl':this.requestUrl},  
			jsonp : 'w',
			crossDomain : true,
			success: function(data) {
				return onSuccess(data);
			},
			error:function(data){ 
				return onError(data);
			}
		}); 
	}, 
	dropifiSerializeObject: function(elementId){
	    var o = {};
	    var a = jQuery('#'+elementId).serializeArray(); 
	    if(a && a.length>1){
		    jQuery.each(a, function() {
		    	 
		        if (o[this.name] !== undefined) {
		            if (!o[this.name].push) {
		                o[this.name] = [o[this.name]];
		            }
		            o[this.name].push(this.value || '');
		        } else {
		            o[this.name] = this.value || '';
		        }
		    });
	    }
	    return o; 
	}
} 
jQuery.get("//api.dropifi.com/jsonip/index.php", function(data) { userLocation.ipAddress = data.ip;});
