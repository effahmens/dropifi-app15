(function() { 
    var jQuery;
    var host = "dev2.kudobuzz.com";
    var product_id;
    var serverFQDN = location.protocol + "//" + host + "/public/javascripts/kudos";
    var options, user_id, url, wdg_contact_url;
    var product_id;
    var shop_uid;
    var platform = 0;

    if (!window.Kudos) {
        window.Kudos = {}
    }
    if ("Shopify" in window) {
        user_id = Shopify.shop;
        platform = 2;
        url = location.protocol + "//" + host + "/template?platform=2&uid=" + Shopify.shop + "&url=" + location.protocol + "//" + location.host
    } else {
        Kudos.Widget = function(opts) {
            options = opts;
            user_id = options.uid;
            url = location.protocol + "//" + host + "/template?uid=" + user_id + "&url=" + location.protocol + "//" + location.host
            
            if (user_id != "undefined") {
                init();
                //alert("Kudobuzz initialised")
            }
        }
    }
    function init() {
        shop_uid = user_id;

        if (window.jQuery === undefined) {
            //console.log("Loading Kudobuzz app, jquery not present");
            var script_tag = document.createElement("script");
            script_tag.setAttribute("type", "text/javascript");
            script_tag.setAttribute("src", location.protocol + "//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js");
            if (script_tag.attachEvent) {
                script_tag.onreadystatechange = function() {
                    if (this.readyState == "complete" || this.readyState == "loaded") {
                        this.onreadystatechange = null;
                        scriptLoadHandler()
                    }
                }
            } else {
                script_tag.onload = scriptLoadHandler
            }
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag)
        } else {
            //console.log("Loading Kudobuzz app, using existing jquery");
            jQuery = window.jQuery;
            main()
        }
    }
    function scriptLoadHandler() {
        jQuery = window.jQuery.noConflict();
        main()
    }
    function main() {
        jQuery(document).ready(function() {
            jQuery.getScript(serverFQDN + "/vendor/json2.js");
            url = url + "&class_type=0";
            var iframe = document.createElement("iframe");
            iframe.setAttribute("src", url);
            iframe.setAttribute("id", "kudo_widget");
            iframe.setAttribute("class", "kudo_widget");
            iframe.setAttribute("width", "500px");
            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("scrolling", "no");
            iframe.setAttribute("height", "0px");
            jQuery("body").append(iframe);
            window.addEventListener("message", function(e) {
                $ = jQuery.noConflict();
                if (typeof str === "object") {
                } else {
                    var kwv = e.data.split("-");
                    var widget_type_id = kwv[0];
                    var i = kwv[1];
                    if (widget_type_id == 3) {
                        jQuery(".kudo_widget").css({
                            position: "fixed",
                            bottom: "0px",
                            "z-index": "9999999999999999999999"
                        });
                        if (i == "step1") {
                            jQuery(".kudo_widget").css({
                                height: "50px",
                                width: "311px"
                            })
                        } else {
                            if (i == "step2") {
                                jQuery(".kudo_widget").css({
                                    height: "185px",
                                    width: "295px"
                                })
                            } else {
                                if (i == "finished") {
                                    window.parent.jQuery("#wdg-save-btn").show();
                                    iframe.setAttribute("style", "display: block; position: fixed; bottom: 0px; z-index: 9999999999999999999999")
                                } else {
                                    if (i == "fullscreen") {
                                        $(".kudo_widget").css({
                                            width: "100%",
                                            height: "100%",
                                            position: "fixed",
                                            bottom: "0px",
                                            display: "block",
                                            border: "0px solid red"
                                        })
                                    } else {
                                        if (i == "close_full_screen") {
                                            $(".kudo_widget").css({
                                                height: "185px",
                                                width: "311px",
                                                border: "0px solid lime",
                                                bottom: "0px"
                                            })
                                        }
                                    }
                                }
                            }
                        }
                        if (kwv[2] == "ismobile") {
                            iframe.setAttribute("style", "display: none !important;")
                        }
                    } else {
                        if (widget_type_id == 12) {
                            jQuery(".kudo_widget").css({
                                height: "400px",
                                right: "0px",
                                position: "fixed"
                            });
                            if (i == "finished") {
                                jQuery(".kudo_widget").css({
                                    width: "358px",
                                    height: "400px"
                                })
                            } else {
                                if (i == "step1") {
                                    jQuery(".kudo_widget").css({
                                        width: "45px"
                                    })
                                } else {
                                    if (i == "step2") {
                                        jQuery(".kudo_widget").css({
                                            width: "358px"
                                        })
                                    } else {
                                        if (i == "fullscreen") {
                                            $(".kudo_widget").css({
                                                width: "100%",
                                                height: "100%",
                                                position: "fixed",
                                                bottom: "0px",
                                                top: "0px",
                                                display: "block"
                                            })
                                        } else {
                                            if (i == "close_full_screen") {
                                                $(".kudo_widget").css({
                                                    height: "400px",
                                                    bottom: "100px",
                                                    width: "358px"
                                                })
                                            }
                                        }
                                    }
                                }
                            }
                            if (i !== "fullscreen") {
                                $(window).resize(function() {
                                    $(".kudo_widget").center();
                                });
                                $(function() {
                                    $(".kudo_widget").center();
                                });
                            }
							
                            if (kwv[2] == "ismobile") {
                                iframe.setAttribute("style", "display: none !important;")
                            }
                        } else {
                            if (widget_type_id == 8) {
                                if (i == "fullscreen") {
                                    $("#kudobuzz-fullpage-widget>iframe").css({
                                        visibility: "visible",
                                        padding: "0px",
                                        margin: "0px",
                                        "z-index": 21474,
                                        height: "100%",
                                        width: "100%",
                                        right: "0px",
                                        top: "0px",
                                        left: "0px",
                                        bottom: "0px",
                                        position: "fixed"
                                    })
                                } else {
                                    if (i == "close_full_screen") {
                                        var wdg_height = kwv[2];
                                        var wdg_width = kwv[3];
//                                          $("body").css("visibility", "visible");
                                        $("#kudobuzz-fullpage-widget>iframe").css({
                                            position: "relative",
                                            width: "100%",
                                            height: wdg_height + "px"
                                        })
                                    }
                                }
                            }
                        }
                    }
                    if (kwv[2] == "left|a") {
                        jQuery(".kudo_widget").css("left", "0px")
                    } else {
                        if (kwv[2] == "right|a") {
                            jQuery(".kudo_widget").css("right", "0px")
                        }
                    }
                }
            }, false);
            jQuery(iframe).css({
                padding: 0,
                margin: 0,
                display: "block",
                "z-index": "999999999999999999999999999999",
                overflow: "hidden"
            });
            jQuery.fn.center = function() {
                this.css("position", "fixed");
                if ($(window).height() < 420) {
                    this.css("top", $(window).height() / 2 - this.outerHeight() / 2)
                } else {
                    this.css("top", 110)
                }
                return this
            };
            var full_page_embedded = parent.top.document.getElementById("kudobuzz-fullpage-widget");
            var slider_embedded = parent.top.document.getElementById("kudobuzz-slider-widget");
            if (full_page_embedded || slider_embedded) {
                if (full_page_embedded) {
                    var wdg_contact_url = location.protocol + "//" + host + "/reviews/one_page_widget?uid=" + user_id + "&url=" + location.protocol + "//" + location.host + "&platform=" + platform

                } else {
                    var wdg_contact_url = location.protocol + "//" + host + "/reviews/slider_widget?uid=" + user_id + "&url=" + location.protocol + "//" + location.host + "&platform=" + platform
                }
                var contact_wdg_iframe = document.createElement("iframe");
                var frm_url = wdg_contact_url + "&class_type=3";
                contact_wdg_iframe.setAttribute("src", frm_url);
                contact_wdg_iframe.setAttribute("width", "100%");
                contact_wdg_iframe.setAttribute("frameborder", "0");
                contact_wdg_iframe.setAttribute("scrolling", "no");
                if (full_page_embedded) {
                    jQuery("#kudobuzz-fullpage-widget").append(contact_wdg_iframe)
                } else {
                    jQuery("#kudobuzz-slider-widget").append(contact_wdg_iframe)
                }
                window.addEventListener("message", function(e) {
                    $ = jQuery.noConflict();
                    if (typeof str === "object") {
                    } else {
                        var kwv = e.data.split("-");
                        if (kwv[0] === "review") {
                            if (kwv[1]) {
                                $(".kudobuzz-contact-widget").css({
                                    height: kwv[1] + "px"
                                });
                                $("#kudobuzz-fullpage-widget iframe").css({
                                    height: kwv[1] + "px"
                                });
                                $("#kudobuzz-slider-widget iframe").css({
                                    height: kwv[1] + "px"
                                })
                            }
                        }
                    }
                }, false)
            }
            if (parent.top.document.getElementById("kudobuzz_reviews")) {
                var form_url = location.protocol + "//" + host + "/reviews/render_form?";
                review_frame = document.createElement("iframe");
                form_data_url = "uid=" + encodeURIComponent(parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-uid")) + "&domain=" + encodeURIComponent(parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-domain")) + "&product-id=" + encodeURIComponent(parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-product-id")) + "&name=" + encodeURIComponent(parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-name")) + "&data-url=" + encodeURIComponent(parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-url")) + "&handle=" + encodeURIComponent(parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-handle")) + "&protocol=" + location.protocol + "//" + location.host + "&bread-crumbs=" + encodeURIComponent(parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-bread-crumbs"));
                form_url = form_url + form_data_url;
                review_frame.setAttribute("src", form_url);
                review_frame.setAttribute("id", "kudobuzz_review_form");
                review_frame.setAttribute("width", "100%");
                review_frame.setAttribute("frameborder", "0");
                review_frame.setAttribute("scrolling", "no");
                review_frame.setAttribute("height", "0");
                jQuery("#kudobuzz_reviews").append(review_frame);
                product_id = parent.top.document.getElementById("kudobuzz_reviews").getAttribute("data-product-id");
                window.addEventListener("message", function(e) {
                    $ = jQuery.noConflict();
                    if (e.origin === "https://" + host || e.origin === "http://" + host) {
                        if (typeof str === "object") {
                        } else {
                            var kwv = e.data.split("-");
                            if (kwv[0] === "review") {
                                $("#kudobuzz_reviews iframe").css({
                                    height: kwv[1] + "px"
                                })
                            }
                            if (kwv[0] === "review_expanded") {
                                var style = "height: 100%; width: 100%; top: 0px; bottom:0px; right: 0px; left: 0px; position: fixed;";
                                var elem = parent.top.document.getElementById("kudobuzz_review_form");
                                elem.setAttribute("style", style)
                            }
                            if (kwv[0] === "review_collapsed") {
                                var style = "height: " + button_height + "px; width: " + button_width + "px; top: 0px; bottom:0px; right: 0px; left: 0px; ";
                                var elem = parent.top.document.getElementById("kudobuzz_review_form");
                                elem.setAttribute("style", style)
                            }
                        }
                    }
                }, false)
            }
            $ = jQuery.noConflict();
            if ($('.k_product_rating').length > 0) {
                var css_tag = document.createElement('link');
                css_tag.setAttribute("type", "text/css");
                css_tag.setAttribute("rel", "stylesheet");
                css_tag.setAttribute("href", location.protocol + "//" + host + "/public/stylesheets/rating/kbz_rating.css");
                (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(css_tag);
                var product_ids = [];
                jQuery(".k_product_rating").each(function(index, value) {
                    product_ids.push($(this).attr('id'));
                    var rating_cntxt = '<span class="kbzr-bdg" id="kbzr_bdg_' + $(this).attr('id') + '" data-rating="0">';
                    rating_cntxt += '<span class="kbzr-kbz_star_rating kbzr-bdg-kbz_star_rating">';
                    for (i = 0; i < 5; i++) {
                        rating_cntxt += '<i class="kbzr-icon kbzr-icon-star-empty" style="color: #FEC601;"></i>';
                    }
                    rating_cntxt += '</span><span class="kbzr-bdg-caption">No reviews</span></span>';
                    jQuery("#" + $(this).attr('id')).append(rating_cntxt);
                });
                var collection_url = location.protocol + "//" + host + '/collection/rating?uid=' + user_id + "&platform=" + platform + "&product_ids=" + product_ids;
                $.ajax({
                    url: collection_url,
                    type: 'GET',
                    dataType: "json",
                    error: function() {return true;},
                    success: function(collection_response) {
                        jQuery.each(collection_response, function(i, value) {
                            var star_type = '';
                            var count_msg = "No reviews";
                            if (parseInt(value.count) === 1) {
                                count_msg = '1 review';
                            } else if (parseInt(value.count) > 1) {
                                count_msg = value.count + ' reviews';
                            }
							 console.log(value.count+' reviewers');
                            var rating_cntxt = '<span class="kbzr-bdg" id="kbzr_bdg_' + value.product_id + '" data-rating="' + value.rating + '">';
                            rating_cntxt += '<span class="kbzr-kbz_star_rating kbzr-bdg-kbz_star_rating">';
                            for (i = 0; i < 5; i++) {
                                if (parseFloat(value.rating) - i <= 0.49) {
                                    star_type = '-empty';
                                } else if (parseFloat(value.rating) - i === 0.5) {
                                    star_type = '-half-alt';
                                } else if (parseFloat(value.rating) - i > 0.5) {
                                    star_type = '';
                                }
                                rating_cntxt += '<i class="kbzr-icon kbzr-icon-star' + star_type + '" style="color: #FEC601;"></i>';
                            }

                            rating_cntxt += '</span><span class="kbzr-bdg-caption">' + count_msg + '</span></span>';
                            jQuery("#" + value.product_id).html('');
                            jQuery("#" + value.product_id).append(rating_cntxt);
                        });
                    },
                    async: true
                 });
            }

            if (parent.top.document.getElementById("kudobuzz-review-widget")) {
                var contact_wdg_iframe = document.createElement("iframe");
                var frm_url = location.protocol + "//" + host + "/kudobuzz-site-review-form?uid=" + user_id + "&url=" + location.protocol + "//" + location.host;
                +"&class_type=2";
                contact_wdg_iframe.setAttribute("src", frm_url);
                contact_wdg_iframe.setAttribute("id", "kudobuzz-contact-widget");
                contact_wdg_iframe.setAttribute("class", "kudobuzz-contact-widget");
                contact_wdg_iframe.setAttribute("width", "100%");
                contact_wdg_iframe.setAttribute("frameborder", "0");
                contact_wdg_iframe.setAttribute("scrolling", "no");
                jQuery("#kudobuzz-review-widget").append(contact_wdg_iframe);
                window.addEventListener("message", function(e) {
                    $ = jQuery.noConflict();
                    $("#kudobuzz-review-widget").css({
                        overflow: "hidden", width: "100%"
                    });
                    if (typeof str === "object") {
                    } else {
                        var kwv = e.data.split("-");
                        if (kwv[0] === "review") {
                            $(".kudobuzz-contact-widget").css({
                                height: kwv[1] + "px"
                            });
                            $("#kudobuzz-review-widget iframe").css({
                                height: kwv[1] + "px"
                            })
                        }
                    }
                }, false)
            }
	
            if ($('#kudobuzz-site-review-snippet').length > 0) {
                var api_url = location.protocol + "//" + host + '/review-snippets?uid=' + shop_uid;
                 if (typeof product_id != 'undefined') {
                    api_url += '&product_id=' + product_id;
                }
                $.ajax({
                    url: api_url,
                    type: 'GET',
                    dataType: "json",
                    error: function() {return true;},
                    success: function(obj) { },
                    async: true
                 });
            }
        });
    }
   /* if (user_id != "undefined") {
        init()
    }*/
})();
