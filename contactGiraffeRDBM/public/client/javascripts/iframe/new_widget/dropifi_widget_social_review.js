(function() {
    var dropifiQuery;
    var host = "social.dropifi.com"; //kudobuzz.com
    var product_id;
    var serverFQDN = location.protocol + "//" + host + "/public/javascripts/kudos";
    var options, user_id, url, wdg_contact_url;
    var product_id;
    var shop_uid;
    var platform = 0;

    if (!window.Kudos) {
        window.Kudos = {}
    }
    if ("Shopify" in window) {
        user_id = Shopify.shop;
        platform = 2;
        url = location.protocol + "//" + host + "/template?platform=2&uid=" + Shopify.shop + "&url=" + location.protocol + "//" + location.host
    } else {
        Kudos.Widget = function(opts) {
            options = opts;
            user_id = options.uid;
            url = location.protocol + "//" + host + "/template?uid=" + user_id + "&url=" + location.protocol + "//" + location.host
        }
    }
    function init() {
        shop_uid = user_id;

        if (window.dropifiQuery === undefined) {
            var script_tag = document.createElement("script");
            script_tag.setAttribute("type", "text/javascript");
            script_tag.setAttribute("src", location.protocol + "//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js");
            if (script_tag.attachEvent) {
                script_tag.onreadystatechange = function() {
                    if (this.readyState == "complete" || this.readyState == "loaded") {
                        this.onreadystatechange = null;
                        scriptLoadHandler()
                    }
                }
            } else {
                script_tag.onload = scriptLoadHandler
            }
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag)
        } else {
            dropifiQuery = window.dropifiQuery;
            main()
        }
    }
    function scriptLoadHandler() {
        dropifiQuery = window.dropifiQuery.noConflict();
        main()
    }
    function main() {
        dropifiQuery(document).ready(function() {
            dropifiQuery.getScript(serverFQDN + "/vendor/json2.js");
            url = url + "&class_type=0";
            var iframe = document.createElement("iframe");
            iframe.setAttribute("src", url);
            iframe.setAttribute("id", "kudo_widget");
            iframe.setAttribute("class", "kudo_widget");
            iframe.setAttribute("width", "500px");
            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("scrolling", "no");
            iframe.setAttribute("height", "0px");
            dropifiQuery("body").append(iframe);
            window.addEventListener("message", function(e) {
                $ = dropifiQuery.noConflict();
                if (typeof str === "object") {
                } else {
                    var kwv = e.data.split("-");
                    var widget_type_id = kwv[0];
                    var i = kwv[1];
                    if (widget_type_id == 3) {
                        dropifiQuery(".kudo_widget").css({
                            position: "fixed",
                            bottom: "0px",
                            "z-index": "9999999999999999999999"
                        });
                        if (i == "step1") {
                            dropifiQuery(".kudo_widget").css({
                                height: "50px",
                                width: "311px"
                            })
                        } else {
                            if (i == "step2") {
                                dropifiQuery(".kudo_widget").css({
                                    height: "185px",
                                    width: "295px"
                                })
                            } else {
                                if (i == "finished") {
                                    window.parent.dropifiQuery("#wdg-save-btn").show();
                                    iframe.setAttribute("style", "display: block; position: fixed; bottom: 0px; z-index: 9999999999999999999999")
                                } else {
                                    if (i == "fullscreen") {
                                       dropifiQuery(".kudo_widget").css({
                                            width: "100%",
                                            height: "100%",
                                            position: "fixed",
                                            bottom: "0px",
                                            display: "block",
                                            border: "0px solid red"
                                        })
                                    } else {
                                        if (i == "close_full_screen") {
                                           dropifiQuery(".kudo_widget").css({
                                                height: "185px",
                                                width: "311px",
                                                border: "0px solid lime",
                                                bottom: "0px"
                                            })
                                        }
                                    }
                                }
                            }
                        }
                        if (kwv[2] == "ismobile") {
                            iframe.setAttribute("style", "display: none !important;")
                        }
                    } else {
                        if (widget_type_id == 12) {
                            dropifiQuery(".kudo_widget").css({
                                height: "500px",
                                right: "0px",
                                position: "fixed"
                            });
                            if (i == "finished") {
                                dropifiQuery(".kudo_widget").css({
                                    width: "570px",
                                    height: "500px"
                                })
                            } else {
                                if (i == "step1") {
                                    dropifiQuery(".kudo_widget").css({
                                        width: "45px"
                                    })
                                } else {
                                    if (i == "step2") {
                                        dropifiQuery(".kudo_widget").css({
                                            width: "570px"
                                        })
                                    } else {
                                        if (i == "fullscreen") {
                                           dropifiQuery(".kudo_widget").css({
                                                width: "100%",
                                                height: "100%",
                                                position: "fixed",
                                                bottom: "0px",
                                                top: "0px",
                                                display: "block"
                                            })
                                        } else {
                                            if (i == "close_full_screen") {
                                               dropifiQuery(".kudo_widget").css({
                                                    height: "500px",
                                                    bottom: "100px",
                                                    width: "570px"
                                                })
                                            }
                                        }
                                    }
                                }
                            }
                            if (i !== "fullscreen") {
                               dropifiQuery(window).resize(function() {
                                   dropifiQuery(".kudo_widget").center();
                                });
                               dropifiQuery(function() {
                                   dropifiQuery(".kudo_widget").center();
                                });
                            }
                            if (kwv[3] == "ismobile") {
                                iframe.setAttribute("style", "display: none !important;")
                            }
                        } else {
                            if (widget_type_id == 8) {
                                if (i == "fullscreen") {
                                   dropifiQuery("#social-fullpage-widget>iframe").css({
                                        visibility: "visible",
                                        padding: "0px",
                                        margin: "0px",
                                        "z-index": 21474,
                                        height: "100%",
                                        width: "100%",
                                        right: "0px",
                                        top: "0px",
                                        left: "0px",
                                        bottom: "0px",
                                        position: "fixed"
                                    })
                                } else {
                                    if (i == "close_full_screen") {
                                        var wdg_height = kwv[2];
                                        var wdg_width = kwv[3];
//                                         dropifiQuery("body").css("visibility", "visible");
                                       dropifiQuery("#social-fullpage-widget>iframe").css({
                                            position: "relative",
                                            width: "100%",
                                            height: wdg_height + "px"
                                        })
                                    }
                                }
                            }
                        }
                    }
                    if (kwv[2] == "left|a") {
                        dropifiQuery(".kudo_widget").css("left", "0px")
                    } else {
                        if (kwv[2] == "right|a") {
                            dropifiQuery(".kudo_widget").css("right", "0px")
                        }
                    }
                }
            }, false);
            dropifiQuery(iframe).css({
                padding: 0,
                margin: 0,
                display: "block",
                "z-index": "999999999999999999999999999999",
                overflow: "hidden"
            });
            dropifiQuery.fn.center = function() {
                this.css("position", "fixed");
                if ($(window).height() < 420) {
                    this.css("top",dropifiQuery(window).height() / 2 - this.outerHeight() / 2)
                } else {
                    this.css("top", 110)
                }
                return this
            };
            var full_page_embedded = parent.top.document.getElementById("social-fullpage-widget");
            var slider_embedded = parent.top.document.getElementById("social-slider-widget");
            if (full_page_embedded || slider_embedded) {
                if (full_page_embedded) {
                    var wdg_contact_url = location.protocol + "//" + host + "/reviews/one_page_widget?uid=" + user_id + "&url=" + location.protocol + "//" + location.host + "&platform=" + platform

                } else {
                    var wdg_contact_url = location.protocol + "//" + host + "/reviews/slider_widget?uid=" + user_id + "&url=" + location.protocol + "//" + location.host + "&platform=" + platform
                }
                var contact_wdg_iframe = document.createElement("iframe");
                var frm_url = wdg_contact_url + "&class_type=3";
                contact_wdg_iframe.setAttribute("src", frm_url);
                contact_wdg_iframe.setAttribute("width", "100%");
                contact_wdg_iframe.setAttribute("frameborder", "0");
                contact_wdg_iframe.setAttribute("scrolling", "no");
                if (full_page_embedded) {
                    dropifiQuery("#social-fullpage-widget").append(contact_wdg_iframe)
                } else {
                    dropifiQuery("#social-slider-widget").append(contact_wdg_iframe)
                }
                window.addEventListener("message", function(e) {
                    $ = dropifiQuery.noConflict();
                    if (typeof str === "object") {
                    } else {
                        var kwv = e.data.split("-");
                        if (kwv[0] === "review") {
                            if (kwv[1]) {
                               dropifiQuery(".social-contact-widget").css({
                                    height: kwv[1] + "px"
                                });
                               dropifiQuery("#social-fullpage-widget iframe").css({
                                    height: kwv[1] + "px"
                                });
                               dropifiQuery("#social-slider-widget iframe").css({
                                    height: kwv[1] + "px"
                                })
                            }
                        }
                    }
                }, false)
            }
            

            if (parent.top.document.getElementById("social-review-widget")) {
                var contact_wdg_iframe = document.createElement("iframe");
                var frm_url = location.protocol + "//" + host + "/social-site-review-form?uid=" + user_id + "&url=" + location.protocol + "//" + location.host;
                +"&class_type=2";
                contact_wdg_iframe.setAttribute("src", frm_url);
                contact_wdg_iframe.setAttribute("id", "social-contact-widget");
                contact_wdg_iframe.setAttribute("class", "social-contact-widget");
                contact_wdg_iframe.setAttribute("width", "100%");
                contact_wdg_iframe.setAttribute("frameborder", "0");
                contact_wdg_iframe.setAttribute("scrolling", "no");
                dropifiQuery("#social-review-widget").append(contact_wdg_iframe);
                window.addEventListener("message", function(e) {
                    $ = dropifiQuery.noConflict();
                   dropifiQuery("#social-review-widget").css({
                        overflow: "hidden", width: "100%"
                    });
                    if (typeof str === "object") {
                    } else {
                        var kwv = e.data.split("-");
                        if (kwv[0] === "review") {
                           dropifiQuery(".social-contact-widget").css({
                                height: kwv[1] + "px"
                            });
                           dropifiQuery("#social-review-widget iframe").css({
                                height: kwv[1] + "px"
                            })
                        }
                    }
                }, false)
            }
        });
    }
    if (user_id != "undefined") {
        init()
    }
})();