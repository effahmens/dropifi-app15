(function(){ 
	var viewUrl,contactViewUrl,contactFilterUrl, dafault_custsomer_image,viewSentUrl; //, inbox_selected_mailbox;
	var open_message_id= undefined;
	var open_contact_id = undefined;
	var inbox_mail_panel = "ref_inbox";
	var is_inbox_panel_click = false;
	var selected_contact_list = "letter_All"; 
	var inbox_selected_mailbox ="all_mailboxes";
	var selected_customer_mode ="everyone_selected";
	var dropifi_total_messages = undefined;
	var dropifi_next_inbox = 0;
	var dropifi_previous_inbox = 0;
	var next_end = false;
	var previous_end = true;
	var is_dropifi_inbox = undefined;
	var pullOfMessages = new Array(); 
	var pullOfContacts = new Array(); 
	var newpendingresolved_0304 = undefined;
	var mailbox_filters_0309_inbox = undefined;
	var reply_current_messageId = undefined;
	var reply_current_msgId = undefined;

	
	fetchAllMessages = function(start,end requestUrl){
		 
		$.ajax({
			url:requestUrl,
			data:{'start':start, 'end':end},
			success: function(data){
				if(data.status == 200){
					dropifi_next_inbox = 0;
			    	inbox_selected_mailbox ="all_mailboxes";
			    	selected_customer_mode ="everyone_selected";
			    	populateMessages(data);   
			    	
			    	if(data.messages.length>0){
			    		 viewMessage("mail_id_"+data.messages[0].id);
			    	}else{					
						clearContactDetail(); 
			    	}
			    	 setNumofSentMsg(data.sentMessages);			    	
				}else{
					$("#incoming_scroll").html("<h3>No Messages Loaded</h3>");	
			   		$("#selected_mail_scroll").html("");	 
				}
			},
			error: function(data){
				$("#incoming_scroll").html("<h3>No Messages Loaded</h3>");	
		   		$("#selected_mail_scroll").html("");	 
			}
		});
	},
	
	populateMessages = function(data){
		if(data.status == 200){ 
			
			if(data.count != undefined){
				setCounters(data.count);
			}

			if(data.sources != undefined){
				fillMailboxes(data.sources);
			}
			
			pullOfMessages = new Array();
			pullOfMessages = data.messages;
			 		
			displayNextInboxMessages(0, 50); 
		}
	},
	
	viewMessage = function(id) {
		try{ 
			
			showProgressTitle("");
			
			if(id != undefined){
				var divId = "#"+id;
				var idValue = divId.substring("mail_id_".length+1, divId.length);		 
				var email = $(divId).attr("ref_email");
				var msgId = $(divId).attr("ref_id");
				var status = $(divId).attr("ref_status"); 	
				var sentiment =$(divId).attr("ref_sentiment");
				var source = $(divId).attr("ref_source");			
				var index = $(divId).attr("ref_index"); 
				reply_current_messageId = idValue;
				reply_current_msgId = msgId;
				$.ajax({
				     type: "GET", 
				     url:viewUrl,    
				     data: {'id':idValue, 'msgId':msgId,'email':email,'msgStatus':status,'sentiment':sentiment,'source':source,'mailbox':inbox_selected_mailbox},  
				     success: function(data){  
				    	
				    	if(data.status == 200){ 
				    		if(data.count != undefined){ 
				    			setCounters(data.count); 
				    		} 			    				    	 		    		
				    		contactProfile(data, divId, "inbox");
				    		setMessage(data.message,data.domain,data.replyer);
				    		if(data.message !=undefined && index>=0){		    			 
				    			pullOfMessages[index].status = data.message.status; 
				    		}			    		
				    		 
				    	} 
				    	hideProgress();
				   	},
				   	error:function(data){		   		
				   		hideProgress(); 			   		 
				   	}
				   });	
		 
			}else{
				hideProgress();
			}
		}catch(ee){
			hideProgress();
		}
	},
	
	fillMailboxes = function(sources){ 
		if(sources != undefined) {
			$("#filter_comboBox").html("");
			$("#filter_comboBox").append("<option id='all_mailboxes' value ='All Mailboxes' mailbox_length ='"+sources.length+"' class ='inbox_selected_mailbox' >All Mailboxes</option> ");
			inbox_selected_mailbox ='All Mailboxes';
			
			$.each(sources,function(key,val){	
				$("#filter_comboBox").append("<option id = '"+val+"' value ='"+val+"' class ='inbox_selected_mailbox' >"+val+"</option> ");
			});
			
			$("#filter_comboBox").change(function(){			 
				inbox_selected_mailbox = $(this).val(); 
				is_inbox_panel_click = false;
				dropifi_next_inbox = 1;
				filterByMailbox(0,200);
			});
		}
	}
	
	setNumOfSubject = function (subjectCount){
		if(subjectCount!=undefined){
			
			$("#widget_subject").html("");
			$.each(subjectCount, function(key,val){
				$("#widget_subject").append(
				'<li class="inbox_mail_panel_subject" id="ref_'+val.subject+'" ref_panel="subjects"> '+
				'<a href="#subject_'+val.subject+'" class="panel">'+val.subTitle+'<span class="mail_count" ' +
				' id="count_sub_'+val.subject+'"> (' + val.numOfMsg + ') </span></a></li>');
			}); 
			
			//set the click action for the panels
		    $(".inbox_mail_panel_subject").click(function() {
	   			inbox_mail_panel = this.id; 
	   			is_inbox_panel_click = true; 
	   			
	   			if(this.id != "ref_sent" && this.id != "ref_drafts")
	   				filterByMailbox(0,50);
		   			
		   	}); 
		}	
	},
	
	setCounters = function(count){
		if(count != undefined){
			$("#count_inbox").text("("+count.inbox+")");
			//$("#count_sent").text("("+count.sent+")");
			//$("#count_draft").text("("+count.draft+")");
			$("#count_negative").text("("+count.negative);
			$("#count_neutral").text("("+count.neutral);
			$("#count_positive").text("("+count.positive);
			$("#count_new").text("("+count.newMsg+")");
			$("#count_open").text("("+count.openMsg+")");
			$("#count_pending").text("("+count.pendingMsg+")");
			$("#count_resolved").text("("+count.resolvedMsg+")");
		}
	},
	
	setViewUrl = function(url){
		viewUrl =url;
	},
	
	defaultCustomerImage = function(image){
		dafault_custsomer_image = image;
	}
	
})();