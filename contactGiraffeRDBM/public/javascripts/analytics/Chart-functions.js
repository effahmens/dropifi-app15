/*
 *	Javascript File
 * 
 *	Analytics functions responsible for drawing the various charts 
 *
 */

/***************** Function for displaying message analytics for each contact personal ************/
	function LineChart_ForContacts(tagId,title,subtitle,xAxisLabel, yAxisLabel, dateFormat, intervalValue, CatARRAY,SERIESARRAY)
	{	
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type:'spline' },
			title: { text: title },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				dateTimeLabelFormats:{
					hour:'%H:%M',
					day:'%e. %b',
					week:'%e. %b',
					month:'%b,%y',
					year:'%Y'					
				},
				tickInterval: intervalValue,
				tickWidth: 1,
				lineColor: '#000',
			    tickColor: '#000',
				title:{
					text: xAxisLabel
				},
				labels: {
					overflow:'justify',
					y:20,
					rotation:-45,
					align: 'right',
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis: { 
				title: {text: yAxisLabel },
				min:0,
				lineColor: '#000',
	            lineWidth: 1
			},
			tooltip: {
				shared: false,
				crosshairs: false
			},
			legend: {
				itemStyle: {
					font: '9pt Trebuchet MS, Verdana, sans-serif'
				},
				layout: 'horizontal',
				backgroundColor: '#FFFFFF',
				margin:30
            },				
			plotOptions: {
				series: {
					marker: {
						lineWidth: 1
					},
					events:{
						legendItemClick: function(event){
							//event.preventDefault();
						}
					}
				},
				spline: {						
					dataLabels: {
						enabled: false 
					} 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});		
	}
/**************************************************************************************************/
	
	
	
	
/***************** Function for displaying line charts ********************************************/
	function LineChart(chartType,tagId,title,subtitle,xAxisLabel, yAxisLabel,intervalValue, CatARRAY,SERIESARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line',marginRight:180 },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					dateTimeLabelFormats:{
						hour:'%H:%M',
						day:'%e. %b',
						week:'%e. %b',
						month:'%b,%y',
						year:'%Y'					
					},
					tickInterval: intervalValue,
					showFirstLabel:true,
					lineColor: '#000',
				    tickColor: '#000',
					title: {
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',
						y:20,
						rotation: -45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { 
					title: {text: yAxisLabel },
					min:0
				},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					backgroundColor: '#FFFFFF',
					align: 'right',	
					verticalAlign: 'top',
					x: 0,y: 70,
					floating: false,
					shadow: true
	            },	
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e. %b, %Y', this.x) +'</b><br/>'+this.series.name+' : '+ this.y;
					}
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								//event.preventDefault();
							}
						}
					},
					line: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'area':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'area',marginRight:180 },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					dateTimeLabelFormats:{
						hour:'%H:%M',
						day:'%e. %b',
						week:'%e. %b',
						month:'%b,%y',
						year:'%Y'					
					},
					tickInterval: intervalValue,
					lineColor: '#000',
				    tickColor: '#000',
					title: {
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',y:20,
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel },
				min:0	},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					backgroundColor: '#FFFFFF',
					align: 'right',	
					verticalAlign: 'top',
					x: 0,y: 70,
					floating: false,
					shadow: true
	            },	
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e. %b, %Y', this.x) +'</b><br/>'+this.series.name+' : '+ this.y;
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								//event.preventDefault();
							}
						}
					},
					area: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type:'spline', marginRight:180 },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					dateTimeLabelFormats:{
						hour:'%H:%M',
						week:'%e. %b',
						month:'%b,%y',
						year:'%Y'					
					},
					tickInterval: intervalValue,
					showFirstLabel:true,
					lineColor: '#000',
				    tickColor: '#000',
					title:{
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',
						y:20,
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { 
					title: {text: yAxisLabel },
					min:0	
				},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e. %b, %Y', this.x) +'</b><br/>'+this.series.name+' : '+ this.y; 
					}
				},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					backgroundColor: '#FFFFFF',
					align: 'right',	
					verticalAlign: 'top',
					x: 0,
					y: 70,
					floating: false,
					shadow: true
	            },				
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								//event.preventDefault();
							}
						}
					},
					spline: {						
						dataLabels: {
							enabled: false 
						} 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'areaspline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'areaspline',marginRight:180 },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					dateTimeLabelFormats:{
						hour:'%H:%M',
						day:'%e. %b',
						week:'%e. %b',
						month:'%b,%y',
						year:'%Y'					
					},
					tickInterval: intervalValue,
					lineColor: '#000',
				    tickColor: '#000',
					title: {
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',
						y:20,
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { 
					title: {text: yAxisLabel },
					min:0	
				},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					backgroundColor: '#FFFFFF',
					align: 'right',	
					verticalAlign: 'top',
					x: 0,
					y: 70,
					floating: false,
					shadow: true
	            },	
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e. %b, %Y', this.x) +'</b><br/>'+this.series.name+' : '+ this.y;
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					areaspline: { 
						fillOpacity: 0.5,
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}
/**************************************************************************************************/
	
