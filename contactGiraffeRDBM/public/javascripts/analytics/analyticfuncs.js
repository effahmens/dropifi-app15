// analytic functions

Highcharts.theme = {
		colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
		chart: {
			backgroundColor: {
				linearGradient: [0, 0, 500, 500],
				stops: [ [0, 'rgb(255, 255, 255)'], [1, 'rgb(240, 240, 255)'] ]
		    },
		    borderWidth: 2,
		    plotBackgroundColor: 'rgba(255, 255, 255, .9)',
		    plotShadow: true,
		    plotBorderWidth: 1,
		    marginRight:120
		},
		title: {
			style: {
		         color: '#000',
		         font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
		    }
		},
		subtitle: {
			style: {
		         color: '#666666',
		         font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
		    }
		},
		xAxis: {
			gridLineWidth: 1,
		    lineColor: '#000',
		    tickColor: '#000',
		    labels: {
		       style: {
		          color: '#000',
		          font: '11px Trebuchet MS, Verdana, sans-serif'
		       }
		    },
		    title: {
		       style: {
		          color: '#333',
		          fontWeight: 'bold',
		          fontSize: '12px',
		          fontFamily: 'Trebuchet MS, Verdana, sans-serif'
		       }
		    }
		},
		yAxis: {
		      minorTickInterval: 'auto',
		      lineColor: '#000',
		      lineWidth: 1,
		      tickWidth: 1,
		      tickColor: '#000',
		      labels: {
		         style: {
		            color: '#000',
		            font: '11px Trebuchet MS, Verdana, sans-serif'
		         }
		      },
		      title: {
		         style: {
		            color: '#333',
		            fontWeight: 'bold',
		            fontSize: '12px',
		            fontFamily: 'Trebuchet MS, Verdana, sans-serif'
		         }
		      }
		   },
		   legend: {
		      itemStyle: {
		         font: '9pt Trebuchet MS, Verdana, sans-serif',
		         color: 'black'

		      },
		      itemHoverStyle: {
		         color: '#039'
		      },
		      itemHiddenStyle: {
		         color: 'gray'
		      }
		   },
		   labels: {
		      style: {
		         color: '#99b'
		      }
		   }
};


function func_LineChartSeriesArray(chartType, tagId, title, subtitle, xAxisLabel, yAxisLabel, intervalValue, CatARRAY, SERIESARRAY)
{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					title: {
						text: xAxisLabel
					},
					labels: {
						rotation: -45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									hs.htmlExpand(null, {
										pageOrigin: {
											x: this.pageX,
											y: this.pageY
										},
										headingText: this.series.name,
										maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
											this.y +' visits',
										width: 200
									});
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					line: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'area':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'area' },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					title: {
						text: xAxisLabel
					},
					labels: {
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					area: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'spline' },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue,  //3600000, // one hour
					tickWidth: 0,
					title:{
						text: xAxisLabel
					},
					labels: {
						rotation:-45,
						align: 'left',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					line: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'areaspline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'areaspline' },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					title: {
						text: xAxisLabel
					},
					labels: {
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					areaspline: { 
						fillOpacity: 0.5,
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}


function func_drawColumnChart(chartType,stackType,tagId,title,yAxisLabel,CatARRAY,SERIESARRAY)
{
	switch (chartType) {		
	case 'column':
		// Apply the theme
		Highcharts.setOptions(Highcharts.theme);
		chart1 = new Highcharts.Chart({		
			chart:{
				renderTo:tagId, 
				type:'column'
			},
			title:{
				text: title.toUpperCase()
			},
			xAxis:{
				categories:CatARRAY,
				labels:{
					rotation: -45,align:'right',
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis:{
				min:0,
				title:{
					text:yAxisLabel
				}
			},
			legend: {
				layout: 'vertical',backgroundColor: '#FFFFFF',
				align: 'right',	verticalAlign: 'top',
				x: 0,y: 70,
				floating: true,
				shadow: true
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.series.name + '</b><br/>'+
                        this.x +': '+ this.y;
                }
            },
			plotOptions:{
				column:{						
					dataLabels:{
						enabled: false,
						rotation: 0,color: '#FFFFFF',
						align: 'top',
						x: -3,y: 10,
						formatter: function(){
							return this.y;
						},
						style: {font: 'normal 13px Verdana, sans-serif'}
					}
				}
            },
			series:SERIESARRAY
		});
		break;
	case 'stacked column':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'column'},
			title: { text: title },
			xAxis: { categories: CatARRAY,labels:{rotation: -45,align: 'right',style:{font: 'normal 13px Verdana, sans-serif'}} },
			yAxis: { min: 0, title: { text: yAxisLabel	},
				stackLabels: {	enabled: true,	style: { fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' } }
			},
			legend: { align: 'right', x: -100, verticalAlign: 'top', y: 20, floating: true,
				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white', borderColor: '#CCC', borderWidth: 1,
				shadow: false
			},
			tooltip: {
				formatter: function() {
					return '<b>'+ this.x +'</b><br/>'+
						this.series.name +': '+ this.y +'<br/>'+
						'Total: '+ this.point.stackTotal;
				}
			},
			plotOptions: {
				column: {
					stacking: stackType,
					dataLabels: { enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white' }
				}
			},
			series: [SERIESARRAY]
		});
		break;
	default:
		break;
	}		
}


















		