// Analytics Chart Javascript 


	// Line and spline chart creation
	
	

	function drawlineChartSeriesArray(chartType,tagId,title,subtitle,yAxisLabel, CatARRAY,SERIESARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CatARRAY,lineColor: '#000',
				    tickColor: '#000' },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					line: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'spline' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					categories: CatARRAY,lineColor: '#000',
				    tickColor: '#000',
					labels:{
						rotation: -45,align:'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }},
				tooltip: { 
					enabled: true,
					formatter: function() {
						return '<b>'+ this.series.name.toUpperCase() +'</b><br/>'+ this.x +': '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},				
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					spline: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}

	
	function drawlineChart_OneDataArrayEntry(chartType,tagId,title,subtitle,yAxisLabel, CatARRAY, D1NAME, D1ARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY,lineColor: '#000',
				    tickColor: '#000' },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					line: { dataLabels: {enabled: true }, enableMouseTracking: true }
				},
				series: [{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'spline' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY,lineColor: '#000',
				    tickColor: '#000' },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C'; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					spline: { dataLabels: {enabled: true }, enableMouseTracking: true }
				},
				series: [{name: D1NAME, data: D1ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}	
	function drawlineChart_TwoDataArrayEntry(chartType,tagId,title,subtitle,yAxisLabel, CatARRAY, D1NAME, D1ARRAY, D2NAME, D2ARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C'; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					line: { dataLabels: {enabled: true }, enableMouseTracking: true
					}
				},
				series: [{name: D1NAME,data: D1ARRAY},{name:D2NAME,data:D2ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'spline' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C'; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					spline: { dataLabels: {enabled: true }, enableMouseTracking: true
					}
				},
				series: [{name: D1NAME,data: D1ARRAY},{name:D2NAME,data:D2ARRAY}]
			});
			break;
		default:
			break;
		}   
	}	
	function drawlineChart_ThreeDataArrayEntry(chartType,tagId,title,subtitle,yAxisLabel, CatARRAY, D1NAME, D1ARRAY, D2NAME, D2ARRAY, D3NAME, D3ARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C'; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					line: { dataLabels: {enabled: true }, enableMouseTracking: true
					}
				},
				series: [{name: D1NAME,data: D1ARRAY},{name:D2NAME,data:D2ARRAY},{name:D3NAME,data:D3ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'spline' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C'; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					spline: { dataLabels: {enabled: true }, enableMouseTracking: true
					}
				},
				series: [{name: D1NAME,data: D1ARRAY},{name:D2NAME,data:D2ARRAY},{name:D3NAME,data:D3ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}	
	function drawlineChart_FourDataArrayEntry(chartType,tagId,title,subtitle,yAxisLabel, CatARRAY, D1NAME, D1ARRAY, D2NAME, D2ARRAY, D3NAME, D3ARRAY, D4NAME, D4ARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C'; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					line: { dataLabels: {enabled: true }, enableMouseTracking: true
					}
				},
				series: [{name: D1NAME,data: D1ARRAY},{name:D2NAME,data:D2ARRAY},{name:D3NAME,data:D3ARRAY},{name:D4NAME,data:D4ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { categories: CATEGORIESARRAY },
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { enabled: true, formatter: function() {
					return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'°C'; }
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					spline: { dataLabels: {enabled: true }, enableMouseTracking: true
					}
				},
				series: [{name: D1NAME,data: D1ARRAY},{name:D2NAME,data:D2ARRAY},{name:D3NAME,data:D3ARRAY},{name:D4NAME,data:D4ARRAY}]
			});
			break;
		default:
			break;
		}
			   
	}
		
	function drawBarChart(tagId,title,yAxisLabel,CatARRAY,D1NAME,D1ARRAY)
	{
		chart = new Highcharts.Chart({
            chart: {
                renderTo: tagId,
                type: 'bar'
            },
            title: {
                text: title.toUpperCase()
            },
            xAxis: {
                categories: CatARRAY,
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: yAxisLabel,
                    align: 'high'
                }
            },
            tooltip: {
                formatter: function() {
                    return ''+
                        this.series.name +': '+ this.y;
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
                x: -50,
                y: 50,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: D1NAME,
                data: D1ARRAY
            }]
        });
	}
	function drawBarChartSeriesArray(tagId,title,yAxisLabel,CatARRAY,SERIESARRAY)
	{
		chart = new Highcharts.Chart({
            chart: {
                renderTo: tagId,
                type: 'bar'
            },
            title: {
                text: title.toUpperCase()
            },
            xAxis: {
                categories: CatARRAY,
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: yAxisLabel,
                    align: 'high'
                }
            },
            tooltip: {
                formatter: function() {
                    return ''+
                        this.series.name +': '+ this.y;
                }
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
                x: -50,
                y: 50,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: SERIESARRAY
        });
	}
	
	// Column and Stacked Column and Group Column
	function drawColumnChart(chartType,stackType,tagId,title,yAxisLabel,CatARRAY,SERIESARRAY)
	{
		switch (chartType) {		
		case 'column':
			chart1 = new Highcharts.Chart({		
				chart:{
					renderTo:tagId, 
					type:'column',
					marginRight:150
				},
				title:{
					text: title.toUpperCase()
				},
				xAxis:{
					categories:CatARRAY,
					lineColor: '#000',
				    tickColor: '#000',
					labels:{
						rotation: -45,align:'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis:{
					min:0,
					title:{
						text:yAxisLabel
					}
				},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:130,
					layout: 'vertical',
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF']
		                ]
		            },
					align: 'right',	
					verticalAlign: 'top',
					x: 0,y: 70,
					floating: false,
					shadow: true
	            },
	            tooltip: {
	            	enabled: true,
					style: {
		                padding: 10,
		                fontWeight: 'normal',
		                font: '9px	Trebuchet MS, Verdana, sans-serif'	                	
		            },
	            	borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF'],
		                    [1, '#E0E0E0']
		                ]
		            },
	                formatter: function() {
	                    return '<b>'+ this.series.name + '</b><br/>'+ this.x +': '+ this.y;
	                }
	            },
				plotOptions:{
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					column:{						
						dataLabels:{
							enabled: false,
							rotation: 0,color: '#FFFFFF',
							align: 'top',
							x: -3,y: 10,
							formatter: function(){
								return this.y;
							},
							style: {font: 'normal 13px Verdana, sans-serif'}
						}
					}
	            },
				series:SERIESARRAY
			});
			break;
		case 'stacked column':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'column'},
				title: { text: title },
				xAxis: { categories: CatARRAY,labels:{rotation: -45,align: 'right',style:{font: 'normal 13px Verdana, sans-serif'}} },
				yAxis: { min: 0, title: { text: yAxisLabel	},
					stackLabels: {	enabled: true,	style: { fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' } }
				},
				legend: { align: 'right', x: -100, verticalAlign: 'top', y: 20, floating: true,
					backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white', borderColor: '#CCC', borderWidth: 1,
					shadow: false
				},
				tooltip: {
					formatter: function() {
						return '<b>'+ this.x +'</b><br/>'+
							this.series.name +': '+ this.y +'<br/>'+
							'Total: '+ this.point.stackTotal;
					}
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					column: {
						stacking: stackType,
						dataLabels: { enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white' }
					}
				},
				series: [SERIESARRAY]
			});
			break;
		default:
			break;
		}		
	}
	
	function comment_drawColumnChart(chartType,stackType,tagId,title,yAxisLabel,CatARRAY,SERIESARRAY)
	{
//		switch (chartType) {		
//		case 'column':
//			chart1 = new Highcharts.Chart({		
//				chart:{
//					renderTo:tagId, 
//					type:'column'
//				},
//				title:{
//					text: title.toUpperCase()
//				},
//				xAxis:{
//					categories:CatARRAY,
//					labels:{
//						rotation: -45,align:'right',
//						style:{
//							font: 'normal 11px Verdana, sans-serif'
//						}
//					}
//				},
//				yAxis:{
//					min:0,
//					title:{
//						text:yAxisLabel
//					}
//				},
//				legend: {
//					layout: 'vertical',backgroundColor: '#FFFFFF',
//					align: 'right',	verticalAlign: 'top',
//					x: 0,y: 70,
//					floating: false,
//					shadow: true
//	            },
//	            tooltip: {
//	                formatter: function() {
//	                    return '<b>'+ this.series.name + '</b><br/>'+
//	                        this.x +': '+ this.y;
//	                }
//	            },
//				plotOptions:{
//					column:{						
//						dataLabels:{
//							enabled: false,
//							rotation: 0,color: '#FFFFFF',
//							align: 'top',
//							x: -3,y: 10,
//							formatter: function(){
//								return this.y;
//							},
//							style: {font: 'normal 13px Verdana, sans-serif'}
//						}
//					}
//	            },
//				series:SERIESARRAY
//			});
//			break;
//		case 'stacked column':
//			chart = new Highcharts.Chart({
//				chart: { renderTo: tagId, type: 'column'},
//				title: { text: title },
//				xAxis: { categories: CatARRAY,labels:{rotation: -45,align: 'right',style:{font: 'normal 13px Verdana, sans-serif'}} },
//				yAxis: { min: 0, title: { text: yAxisLabel	},
//					stackLabels: {	enabled: true,	style: { fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' } }
//				},
//				legend: { align: 'right', x: -100, verticalAlign: 'top', y: 20, floating: true,
//					backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white', borderColor: '#CCC', borderWidth: 1,
//					shadow: false
//				},
//				tooltip: {
//					formatter: function() {
//						return '<b>'+ this.x +'</b><br/>'+
//							this.series.name +': '+ this.y +'<br/>'+
//							'Total: '+ this.point.stackTotal;
//					}
//				},
//				plotOptions: {
//					column: {
//						stacking: stackType,
//						dataLabels: { enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white' }
//					}
//				},
//				series: [SERIESARRAY]
//			});
//			break;
//		default:
//			break;
//		}		
	}
	
	
	function drawColumnChart_OneDataARRAYEntry(chartType,stackType,tagId,title,yAxisLabel,CatARRAY,D1NAME,D1ARRAY)
	{
		switch (chartType) 
		{
		case 'column':
			chart1 = new Highcharts.Chart({		
				chart:{renderTo:tagId, type:'column',marginRight:150},
				title:{text:title.toUpperCase()},
				xAxis:{categories:CatARRAY,labels:{rotation: -72,align: 'right',style:{font: 'normal 13px Verdana, sans-serif'}}},
				yAxis:{title:{text:yAxisLabel}},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:130,
					layout: 'vertical',
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF']
		                ]
		            },
					align: 'right',	
					verticalAlign: 'top',
					x: 0,y: 70,
					floating: false,
					shadow: true
	            },
	            tooltip:{
	            	enabled: true,
					style: {
		                padding: 10,
		                fontWeight: 'normal',
		                font: '9px	Trebuchet MS, Verdana, sans-serif'	                	
		            },
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF'],
		                    [1, '#E0E0E0']
		                ]
		            },
	            	formatter:function(){
	            		return '<b>'+ this.series.name +'</b><br/><b>'+ this.x +': </b>'+ this.y;
	            	}
	            },
				plotOptions:{
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					column:{dataLabels:{enabled: false,rotation: 0,color: '#FFFFFF',align: 'right',x: -3,y: 10,formatter: function(){return this.y;},style: {font: 'normal 13px Verdana, sans-serif'}}}},
				series:[{name:D1NAME,data:D1ARRAY}]
			});
			break;
		case 'stacked column':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'column'},
				title: { text: title.toUpperCase() },
				xAxis: { categories: CatARRAY,labels:{rotation: -45,align: 'right',style:{font: 'normal 13px Verdana, sans-serif'}} },
				yAxis: { min: 0, title: { text: yAxisLabel	},
					stackLabels: {	enabled: true,	style: { fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' } }
				},
				legend: { align: 'right', x: -100, verticalAlign: 'top', y: 20, floating: true,
					backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white', borderColor: '#CCC', borderWidth: 1,
					shadow: false
				},
				tooltip: {
					formatter: function() {
						return '<b>'+ this.x +'</b><br/>'+
							this.series.name +': '+ this.y +'<br/>'+
							'Total: '+ this.point.stackTotal;
					}
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					column: {
						stacking: stackType,
						dataLabels: { enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white' }
					}
				},
				series: [{ name: D1NAME, data: D1ARRAY }]
			});
			break;
		default:
			break;
		}		
	}
	
	function drawColumnChart_TwoDataARRAYEntry(chartType,stackType,tagId,title,yAxisLabel,CatARRAY,D1NAME,D1ARRAY,D2NAME,D2ARRAY){
		switch (chartType) {
		case 'column':
			chart1 = new Highcharts.Chart({		
				chart:{renderTo:tagId, type:'column'},
				title:{text:title.toUpperCase()},
				xAxis:{categories:CatARRAY,labels:{rotation: -45,align: 'right',style:{font: 'normal 13px Verdana, sans-serif'}}},
				yAxis:{title:{text:yAxisLabel}},
				plotOptions:{
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								event.preventDefault();
							}
						}
					},
					column:{dataLabels:{enabled: true,rotation: -90,color: '#FFFFFF',align: 'right',x: -3,y: 10,formatter: function(){return this.y;},style: {font: 'normal 13px Verdana, sans-serif'}}}},
				series:[{name:D1NAME,data:D1ARRAY},{name:D2NAME,data:D2ARRAY}] //dataLabels:{enabled: true,rotation: -90,color: '#FFFFFF',align: 'right',x: -3,y: 10,formatter: function(){return this.y;},style: {font: 'normal 13px Verdana, sans-serif'}}
			});
			break;
		case 'stacked column':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'column' },
				title: { text: title.toUpperCase() },
				xAxis: { categories: CatARRAY },
				yAxis: { min: 0, title: { text: yAxisLabel	},
					stackLabels: {	enabled: true,	style: { fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' } }
				},
				legend: { align: 'right', x: -100, verticalAlign: 'top', y: 20, floating: true,
					backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white', borderColor: '#CCC', borderWidth: 1,
					shadow: false
				},
				tooltip: {
					formatter: function() {
						return '<b>'+ this.x +'</b><br/>'+
							this.series.name +': '+ this.y +'<br/>'+
							'Total: '+ this.point.stackTotal;
					}
				},
				plotOptions: {
					column: {
						stacking: stackType,
						dataLabels: { enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white' }
					}
				},
				series: [{ name: D1NAME, data: D1ARRAY }, { name: D2NAME, data: D2ARRAY	}]
			});
			break;
		default:
			break;
		}
	}
	function drawColumnChart_ThreeDataARRAYEntry(chartType,stackType,tagId,title,yAxisLabel,CatARRAY,D1NAME,D1ARRAY,D2NAME,D2ARRAY,D3NAME,D3ARRAY){
		switch (chartType) {
		case 'column':
			chart1 = new Highcharts.Chart({		
				chart:{renderTo:tagId, type:'column'},
				title:{text:title.toUpperCase()},
				xAxis:{categories:CatARRAY,labels:{rotation: -45,align: 'right',style:{font: 'normal 13px Verdana, sans-serif'}}},
				yAxis:{title:{text:yAxisLabel}},
				plotOptions:{column:{dataLabels:{enabled: true,rotation: -90,color: '#FFFFFF',align: 'right',x: -3,y: 10,formatter: function(){return this.y;},style: {font: 'normal 13px Verdana, sans-serif'}}}},
				series:[{name:D1NAME,data:D1ARRAY},{name:D2NAME,data:D2ARRAY},{name:D3NAME,data:D3ARRAY}] //dataLabels:{enabled: true,rotation: -90,color: '#FFFFFF',align: 'right',x: -3,y: 10,formatter: function(){return this.y;},style: {font: 'normal 13px Verdana, sans-serif'}}
			});
			break;
		case 'stacked column':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'column' },
				title: { text: title.toUpperCase() },
				xAxis: { categories: CatARRAY },
				yAxis: { min: 0, title: { text: yAxisLabel	},
					stackLabels: {	enabled: true,	style: { fontWeight: 'bold', color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray' } }
				},
				legend: { align: 'right', x: -100, verticalAlign: 'top', y: 20, floating: true,
					backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white', borderColor: '#CCC', borderWidth: 1,
					shadow: false
				},
				tooltip: {
					formatter: function(){
						return '<b>'+ this.x +'</b><br/>'+
							this.series.name +': '+ this.y +'<br/>'+
							'Total: '+ this.point.stackTotal;
					}
				},
				plotOptions: {
					column: {
						stacking: stackType,
						dataLabels: { enabled: true, color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white' }
					}
				},
				series: [{ name: D1NAME, data: D1ARRAY }, { name: D2NAME, data: D2ARRAY	}, { name: D3NAME, data: D3ARRAY }]
			});
			break;
		default:
			break;
		}
			
		
	}
	
	
	// Pie chart
	function drawPieChart(dataArray,tagId,title,chatName, percentageSize)
	{
		chart = new Highcharts.Chart({
			chart: {
				renderTo: tagId,
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: title.toUpperCase()
			},
			tooltip: {
				formatter: function() {
					return '<b>'+ this.point.name.toUpperCase() +'</b>: '+ this.y;
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					showInLegend: true			
				}
			},
			legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF'
            },
			series: [{
				type: 'pie',
				name: chatName,
				data: dataArray,
				size: percentageSize,
				dataLabels: {
						enabled: true,
						color: '#000000',
						connectorColor: '#000000',
						formatter: function() {
							return '<b>'+ this.point.name.toUpperCase() +':</b> '+ this.y;
						}
				}
				
			
			}]
		});	
	}
	
	function drawPieChart_main(dataArray,tagId,title,chatName, percentageSize)
	{
		chart = new Highcharts.Chart({
			chart: {
				renderTo: tagId,
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			title: {
				text: title.toUpperCase()
			},
			tooltip: {
				formatter: function() {
					return '<b>'+ this.point.name.toUpperCase() +'</b>: '+ this.y;
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					showInLegend: true			
				}
			},
			legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom',
                x: 10,
                y: 5,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF'
            },
			series: [{
				type: 'pie',
				name: chatName,
				data: dataArray,
				size: percentageSize,
				dataLabels: {
						enabled: true,
						distance:10,
						color: '#000000',
						connectorColor: '#000000',
						formatter: function() {
							return '<b>'+ this.point.name.toUpperCase() +':</b> '+ this.y;
						}
				}
				
			
			}]
		});	
	}

	
	/*********************** Utility functions ************************************/
	function AROptions(e){
		switch(e){
			case 0:
				$("#par2 option[value ='ageRange']").remove();
				$("#par2 option[value ='location']").remove();
				$("#par2 option[value ='gender']").remove();
				$("#par2 option[value ='social']").remove();						
				break;
			case 1:
				$("#par2").append('<option value="ageRange">Age Range</option>');
				$("#par2").append('<option value="location">Location</option>');
				$("#par2").append('<option value="gender">Gender</option>');
				$("#par2").append('<option value="social">Social Network</option>');
				break;
		}				
	}
	
	function Show_Last_30days_Date_String_Info(numberOfDays)
	{
		var dn = new Date();
		var pn = new Date((new Date()).setDate((new Date()).getDate() - numberOfDays));
				
		var dnstr = myDate(dn.getDate()) + " " + myMonth(dn.getMonth() + 1) + ", " + dn.getFullYear();
		var pnstr = myDate(pn.getDate()) + " " + myMonth(pn.getMonth() + 1) + ", " + pn.getFullYear();		
		
		return "Last " + numberOfDays + " Days : " + pnstr + "  to  " + dnstr;
	}
	
	function Show_Specific_Date_String_Info(format)
	{
		return  myDateString(format);
	}
	
	function myDate(d)
	{		
		var d = parseInt(d.toString().substr(0,1) == "0" ? d.toString().substr(1, 1) : d);
		
		if(d == 1 || d == 21 || d == 31){
			return d + "st";
		}
		else if(d == 2 || d == 22 ){
			return d + "nd";
		}
		else if(d == 3 || d == 23 ){
			return d + "rd";
		}
		
		return d + "th";
	}
	
	function myMonth(m)
	{
		var strMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		m = parseInt(m.toString().substr(0,1) == "0" ? m.toString().substr(1, 1) : m);
		return strMonths[m - 1];
	}
	
	function myDateString(dateString)
	{
		var year = dateString.substr(0, 4), month = dateString.substr(5, 2), day = dateString.substr(8,2);				
		return  myDate(day) + "  " + myMonth(month) + ", " + year ;		
	}
	
	function Show_Date_Range_String_Info(fromDate, toDate)
	{
		if((new Date(fromDate) > (new Date(toDate))) ){
			return "Please check the date selections.";
		}
		return myDateString(fromDate) + " to " + myDateString(toDate);
	}
	
	function Show_No_Data(tagId){
		$(tagId).css({"text-align":"center","font-size":"1.5em",  "color":"red"});
		$(tagId).html("No data to display.");
	}
	
	function Reduce_Array_Content(array, maxNumber)
	{
		array.sort();
		while(array.length > maxNumber)
		{
			array.pop();
		}
	}
	
	//Check an array of values to determine whether any one value is greater than zero (0)
	//returns true if a value is > 0 else false.
	function IsValueGreaterThanZero(valuearray){
		var flag = false;
		for(var i=0; i < valuearray.length; i++){
			if(valuearray[i] > 0){
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	
	/******************************************************************************/
	//used for dashboard bar graph
  	function BarChartForTheDashBoard(tagId,title,yAxisLabel,CatARRAY,D1NAME,D1ARRAY)
	{
		chart = new Highcharts.Chart({
            chart: { renderTo: tagId, type: 'bar' },
            title: { text: title.toUpperCase() },
            xAxis: { categories: CatARRAY, title: { text: null }},
            yAxis: { min: 0,  title: { text: yAxisLabel, align: 'high' }},
            tooltip: { 	backgroundColor:'#ffd',
            	formatter: function() {
                    return ''+ this.series.name.toUpperCase() + '<br/><b>'+
                        this.x +'</b>: '+ this.y + ' ';
                }
            },
            plotOptions: { bar: { dataLabels: { enabled: true }}},
            legend: { layout: 'horizontal', align: 'right', verticalAlign: 'top', x: -20,
            	y: 50, floating: true, borderWidth: 1, backgroundColor: '#FFFFFF', shadow: true
            },
            credits: {  enabled: false},
            series: [{ name: D1NAME,  data: D1ARRAY  }]
        });
	}
	
	function LineChartSeriesArray(chartType,tagId,title,subtitle,xAxisLabel, yAxisLabel,intervalValue, CatARRAY,SERIESARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line',marginRight:180 },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, 
					tickWidth: 0,
					lineColor: '#000',
				    tickColor: '#000',
					title: {
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',
						y:20,
						rotation: -45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { 
					title: {text: yAxisLabel },
					min:0},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF']
		                ]
		            },
					align: 'right',	
					verticalAlign: 'top',
					x: 0,y: 70,
					floating: false,
					shadow: true
	            },	
				tooltip: { 
					enabled: true,
					style: {
		                padding: 10,
		                fontWeight: 'normal',
		                font: '9px	Trebuchet MS, Verdana, sans-serif'	                	
		            },
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF'],
		                    [1, '#E0E0E0']
		                ]
		            },
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e %B, %Y',this.x) +'</b><br/>'+ this.series.name +' : '+ this.y; 
					}
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								//event.preventDefault();
							}
						}
					},
					line: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'area':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'area',marginRight:180 },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					lineColor: '#000',
				    tickColor: '#000',
					title: {
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',y:20,
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel },
				min:0	},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF']
		                ]
		            },
					align: 'right',	
					verticalAlign: 'top',
					x: 0,y: 70,
					floating: false,
					shadow: true
	            },	
				tooltip: { 
					enabled: true,
					style: {
		                padding: 10,
		                fontWeight: 'normal',
		                font: '9px	Trebuchet MS, Verdana, sans-serif'	                	
		            },
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF'],
		                    [1, '#E0E0E0']
		                ]
		            },
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e %B, %Y',this.x) +'</b><br/>'+ this.series.name +' : '+ this.y; 
					}
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								//event.preventDefault();
							}
						}
					},
					area: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type:'spline', marginRight:180 },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, 
					tickWidth: 0,
					lineColor: '#000',
				    tickColor: '#000',
					title:{
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',
						y:20,
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { 
					title: {text: yAxisLabel },
					min:0	
				},
				tooltip: { 
					enabled: true,
					style: {
		                padding: 10,
		                fontWeight: 'normal',
		                font: '9px	Trebuchet MS, Verdana, sans-serif'	                	
		            },
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF'],
		                    [1, '#E0E0E0']
		                ]
		            },
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e %B, %Y',this.x) +'</b><br/>'+ this.series.name +' : '+ this.y; 
					}
				},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF']
		                ]
		            },
					align: 'right',	
					verticalAlign: 'top',
					x: 0,
					y: 70,
					floating: false,
					shadow: true
	            },				
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								//event.preventDefault();
							}
						}
					},
					spline: {						
						dataLabels: {
							enabled: false 
						} 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'areaspline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'areaspline',marginRight:180 },
				title: { text: title },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					lineColor: '#000',
				    tickColor: '#000',
					title: {
						text: xAxisLabel
					},
					labels: {
						overflow:'justify',
						y:20,
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { 
					title: {text: yAxisLabel },
					min:0	
				},
				legend: {
					itemStyle: {
						font: '9pt Trebuchet MS, Verdana, sans-serif'
					},
					width:160,
					layout: 'vertical',
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF']
		                ]
		            },
					align: 'right',	
					verticalAlign: 'top',
					x: 0,
					y: 70,
					floating: false,
					shadow: true
	            },	
				tooltip: { 
					enabled: true,
					style: {
		                padding: 10,
		                fontWeight: 'normal',
		                font: '9px	Trebuchet MS, Verdana, sans-serif'	                	
		            },
					borderRadius: 0,
					borderWidth: 1,
		            borderColor: '#AAA',
					backgroundColor: {
		                linearGradient: [0, 0, 0, 60],
		                stops: [
		                    [0, '#FFFFFF'],
		                    [1, '#E0E0E0']
		                ]
		            },
					formatter: function() {
						return '<b>'+ Highcharts.dateFormat('%e %B, %Y',this.x) +'</b><br/>' + this.series.name +' : '+ this.y;
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						marker: {
							lineWidth: 1
						},
						events:{
							legendItemClick: function(event){
								//event.preventDefault();
							}
						}
					},
					areaspline: { 
						fillOpacity: 0.5,
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}
	
	
	function Sentiment_LineChartSeriesArray(chartType,tagId,title,subtitle,xAxisLabel, yAxisLabel,intervalValue, CatARRAY,SERIESARRAY)
	{
		switch (chartType) {
		case 'line':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'line' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					title: {
						text: xAxisLabel
					},
					labels: {
						rotation: -45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									hs.htmlExpand(null, {
										pageOrigin: {
											x: this.pageX,
											y: this.pageY
										},
										headingText: this.series.name,
										maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
											this.y +' visits',
										width: 200
									});
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					line: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'area':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'area' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					title: {
						text: xAxisLabel
					},
					labels: {
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					area: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'spline' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue,  //3600000, // one hour
					tickWidth: 0,
					title:{
						text: xAxisLabel
					},
					labels: {
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					line: { 
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		case 'areaspline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'areaspline' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					type: 'datetime',
					tickInterval: intervalValue, //3600 * 1000, // one hour,
					tickWidth: 0,
					title: {
						text: xAxisLabel
					},
					labels: {
						rotation:-45,
						align: 'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true, 
					formatter: function() {
						return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: function(event) {
									
								}
							}
						},
						marker: {
							lineWidth: 1
						}
					},
					areaspline: { 
						fillOpacity: 0.5,
						dataLabels: {
							enabled: false 
						},
						enableMouseTracking: true 
					}
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}
	
	
	function LineChartSeriesArrayForContacts(tagId,title,subtitle,xAxisLabel, yAxisLabel,intervalValue, CatARRAY,SERIESARRAY)
	{
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type:'spline' },
			title: { text: title },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue, 
				tickWidth: 0,
				lineColor: '#000',
			    tickColor: '#000',
				title:{
					text: xAxisLabel
				},
				labels: {
					overflow:'justify',
					y:20,
					rotation:-45,
					align: 'right',
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis: { 
				title: {text: yAxisLabel },
				min:0	
			},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return "<b>Emotions on Messages</b><br/>"+ 
						this.series.name +' : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			legend: {
				itemStyle: {
					font: '9pt Trebuchet MS, Verdana, sans-serif'
				},
				layout: 'horizontal',
				backgroundColor: '#FFFFFF',
				margin:30
            },				
			plotOptions: {
				series: {
					marker: {
						lineWidth: 1
					},
					events:{
						legendItemClick: function(event){
							event.preventDefault();
						}
					}
				},
				spline: {						
					dataLabels: {
						enabled: false 
					} 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});		
	}
	
	/***************** new function for displaying contact analytic ****/
	function LineChartSeriesArrayForContacts_one(tagId,title,subtitle,xAxisLabel, yAxisLabel, dateFormat, intervalValue, CatARRAY,SERIESARRAY)
	{
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type:'spline' },
			title: { text: title },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				dateTimeLabelFormats:{
					hour:'%H:%M',
					day:'%e. %b',
					week:'%e. %b',
					month:'%b,%y',
					year:'%Y'					
				},
				tickWidth: 0,
				lineColor: '#000',
			    tickColor: '#000',
				title:{
					text: xAxisLabel
				},
				labels: {
					overflow:'justify',
					y:20,
					rotation:-45,
					align: 'right',
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis: { 
				title: {text: yAxisLabel },
				min:0	
			},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return "<b>Emotions on Messages</b><br/>"+ 
						this.series.name +' : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			legend: {
				itemStyle: {
					font: '9pt Trebuchet MS, Verdana, sans-serif'
				},
				layout: 'horizontal',
				backgroundColor: '#FFFFFF',
				margin:30
            },				
			plotOptions: {
				series: {
					marker: {
						lineWidth: 1
					},
					events:{
						legendItemClick: function(event){
							event.preventDefault();
						}
					}
				},
				spline: {						
					dataLabels: {
						enabled: false 
					} 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});		
	}
	/*******************************************************************/
	
	