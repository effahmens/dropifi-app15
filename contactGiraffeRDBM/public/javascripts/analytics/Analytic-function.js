/*
 *	Javascript File
 *
 * 	Analytic functions responsible for performing
 * 	various computations before the drawing of charts.
 */

/***************** Function for performing computations for each contact personal ************/
function DrawingSentimentChartForContacts(data, tagId, trendtype, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, f, t)
{	
	var startdate = new Date(f.getFullYear(),f.getMonth(), f.getDate()); 
	var utc = new Date(f.getFullYear(),f.getMonth(), f.getDate());
	var enddate = new Date(t.getFullYear(),t.getMonth(), t.getDate());
	
	var myIntervalArray = [];
	var myIntervalValue = 0;
	var onedaymillisec = 24*3600*1000;
	var ndays = (enddate.getTime() - startdate.getTime())/ onedaymillisec; 							
	 
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();	
	var dateformat = "";	
	
	if(ndays == 0 || ndays == 1){
		myIntervalArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		myIntervalValue = 3600*1000;
		dateformat = "hour";
		
		$.each(data, function(key, val){
			var type = val.type == "N/A" ? "Not Set" : val.type;
			if($.inArray(type, kNameArray) == -1){
				kNameArray.push(type);
			}									
		});	
		
		for(var n=0;n<myIntervalArray.length;n++){
			kDataArray.push(0);
		}	
		
		if(trendtype == "location" || trendtype == "subjectwidget"){
			Reduce_Array_Content(kNameArray, 10);
		}
		
		for(var i=0 ;i < kNameArray.length; i++){									
			for(var j=0 ;j < myIntervalArray.length; j++){
				var sum = 0;
				$.each(data , function(key2, val2){
					var type1 = val2.type == "N/A"? "Not Set" : val2.type;
					var hour = (new Date(val2.timestamp)).getHours();																										
					if((kNameArray[i] == type1) && (hour == myIntervalArray[j])){
						sum++;
					}											
				});
				kDataArray[j] = sum;
			}	
			
			var useThis = new Array();
			for(var a=0; a < kDataArray.length; a++){
				useThis[a] = kDataArray[a];
			}
			
			if(trendtype == "sentiment"){
				kSeriesArray.push({
					name:kNameArray[i],
					color:determineSeriesColour(kNameArray[i]),
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					pointInterval:myIntervalValue,
					data: useThis
				});
			}
			else
			{		
				kSeriesArray.push({
					name:kNameArray[i],
					pointInterval:myIntervalValue,
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					data: useThis
				});
			}																				
		}
		
		if(kSeriesArray.length != 0){
			LineChart_ForContacts(tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, dateformat, myIntervalValue, '',kSeriesArray);
		}
		else{
			Show_No_Data("#" + tagId);
		}
	}
	else
	{		
		if(ndays <= 14)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n++;				
			}
			myIntervalValue = 24*3600*1000;
			dateformat = "day";
		}
		else if(ndays <= 31)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n += 7;				
			}
			myIntervalValue = 7*24*3600*1000;
			dateformat = "week";
		}
		else if(ndays <= 365)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n += 31;				
			}
			myIntervalValue = 31*24*3600*1000;
			dateformat = "month";
		}
		else if(ndays > 365)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n += 365;				
			}
			myIntervalValue = 12*31*24*3600*1000;
			dateformat = "year";
		}	
		
		myIntervalArray.reverse();
			
		$.each(data, function(key, val){
			var type = val.type == "N/A" ? "Not Set" : val.type;
			if($.inArray(type, kNameArray) == -1){
				kNameArray.push(type);
			}									
		});	
		
		for(var n=0;n<myIntervalArray.length;n++){
			kDataArray.push(0);
		}
		
		if(trendtype == "location" || trendtype == "subjectwidget")
		{
			Reduce_Array_Content(kNameArray, 10);
		}
		
		for(var i=0 ;i < kNameArray.length; i++)
		{
			var bound = f.getTime();
			for(var j=0 ;j < myIntervalArray.length; j++)
			{
				var sum = 0;
				$.each(data , function(key2, val2)
				{
					var type1 = val2.type == "N/A" ? "Not Set" : val2.type;
					var dyd = new Date(val2.timestamp);																									
					if((kNameArray[i] == type1) && ((dyd.getTime() >= bound) && (dyd.getTime() <= myIntervalArray[j].getTime()))){
						sum++;
					}											
				});
				kDataArray[j] = sum;
				bound = myIntervalArray[j].getTime();
			}
			
			var useThis = new Array();
			for(var a=0; a < kDataArray.length; a++){
				useThis.push([Date.UTC(myIntervalArray[a].getFullYear(),myIntervalArray[a].getMonth(),myIntervalArray[a].getDate()), kDataArray[a]]);
				//useThis[a] = kDataArray[a];
			}
			
			if(trendtype == "sentiment")
			{
				kSeriesArray.push({
					name:kNameArray[i],
					color:determineSeriesColour(kNameArray[i]),
					data: useThis
					//pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					//pointInterval:myIntervalValue,
					//data: useThis
				});
			}
			else
			{		
				kSeriesArray.push({
					name:kNameArray[i],
					data:useThis
					//pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					//pointInterval:myIntervalValue,
					
				});
			}																				
		}
		
		if(kSeriesArray.length != 0){
			LineChart_ForContacts(tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel,dateformat, myIntervalValue, '',kSeriesArray);
		}
		else{
			Show_No_Data("#"+tagId);
		}		
	}
}
/**********************************************************************************************************/



/********************** function to draw Sentiment trend charts *****************************/
/*
 * 	data is the array containing the information to be plotted.
 *	trendtype can be "location","AgeRange","Sentiment","Emotion" etc
 *	lineChartType can be "line", "area", "spline", "areaspline" etc
 *
 */

function func_DrawingSentimentInitialChart(data, interval, tagId, trendtype, lineChartType, ChartTitle, ChartSubTitle, xAxisLabel, yAxisLabel)
{
	//alert(interval);
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data, function(key, val)	{
		//check if a value is N/A and change it to Not Set
		var type = val.type == "N/A" ? "Not Set" : val.type;
		
		if($.inArray(type, kNameArray) == -1){
			kNameArray.push(type);
		}									
	});	
	
	for(var n=0;n < interval.length;n++){
		kDataArray.push(0);
	}
	
	if(trendtype == "location" || trendtype == "subjectwidget"){
		Reduce_Array_Content(kNameArray, 10);
	}	
	
	
	for(var i=0 ;i < kNameArray.length; i++){
		var bound = interval[0].getTime();
		
		for(var j=0 ;j < interval.length; j++){
			var sum = 0;
			$.each(data , function(key2, val2){
				var type1 = val2.type == "N/A" ? "Not Set" : val2.type;
				var dyd = new Date(val2.timestamp);																										
				if((kNameArray[i] == type1) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}												
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}	
		
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis.push([Date.UTC(interval[a].getFullYear(),interval[a].getMonth(),interval[a].getDate()), kDataArray[a]]);
			//useThis[a] = kDataArray[a];
		}	
		
		
		if(trendtype == "sentiment"){
			kSeriesArray.push({
				name:kNameArray[i],
				color:determineSeriesColour(kNameArray[i]),
				data: useThis
				//pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
				//pointInterval:7*24*3600*1000,
			});
		}else{		
			kSeriesArray.push({
				name:kNameArray[i],
				data: useThis
				//pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
				//pointInterval:7*24*3600*1000,
			});
		}		
	}
	
	if(kSeriesArray.length != 0){
		LineChart(lineChartType, tagId, ChartTitle, ChartSubTitle, xAxisLabel, yAxisLabel, 604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
	
}

/***************************************************************************************************************************************/



/**************************** function to draw Sentiment trend charts for single date select *******************************************/
function func_drawingSentimentSingleDateChart(data, interval, tagId, trendtype, lineChartType, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, chartDate)
{
	var yr = chartDate.getFullYear();
	var mm = chartDate.getMonth();
	var dd = chartDate.getDate();								
								
	
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data, function(key, val)
	{
		var type = val.type == "N/A" ? "Not Set" : val.type;
		if($.inArray(type, kNameArray) == -1){
			kNameArray.push(type);
		}									
	});			
	
	for(var n=0;n<interval.length;n++){
		kDataArray.push(0);
	}
	
	if(trendtype == "location" || trendtype == "subjectwidget")
	{
		Reduce_Array_Content(kNameArray, 10);
	}
	
	for(var i=0 ;i < kNameArray.length; i++)
	{									
		for(var j=0 ;j < interval.length; j++)
		{
			var sum = 0;
			$.each(data , function(key2, val2)
			{
				var type1 = val2.type == "N/A"? "Not Set" : val2.type;
				var hour = (new Date(val2.timestamp)).getHours();																										
				if((kNameArray[i] == type1) && (hour == interval[j])){
					sum++;
				}											
			});
			kDataArray[j] = sum;
		}
		
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}
		
		if(trendtype == "sentiment")
		{
			kSeriesArray.push({
				name:kNameArray[i],
				color:determineSeriesColour(kNameArray[i]),
				pointStart:Date.UTC(yr,mm,dd),
				pointInterval:3600*1000,
				data: useThis
			});
		}
		else
		{		
			kSeriesArray.push({
				name:kNameArray[i],
				pointStart:Date.UTC(yr,mm,dd),
				pointInterval:3600*1000,
				data: useThis
			});
		}																					
	}
	if(kSeriesArray.length != 0){
		LineChart(lineChartType, tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, 3600000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}
/**************************************************************************************************************************************/



/**************************** function to draw Sentiment trend charts for two selected date range *************************/
function func_drawingSentimentDoubleDateChart(data, tagId, trendtype, lineChartType, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, f, t)
{
	var startdate = new Date(f.getFullYear(),f.getMonth(), f.getDate()); 
	var utc = new Date(f.getFullYear(),f.getMonth(), f.getDate());
	var enddate = new Date(t.getFullYear(),t.getMonth(), t.getDate());						
	
	var myIntervalArray = [];
	var myIntervalValue = 0;
	var onedaymillisec = 24*3600*1000;
	var ndays = (enddate.getTime() - startdate.getTime())/ onedaymillisec;							
	
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();	
	
	if(ndays == 0 || ndays == 1){
		myIntervalArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		myIntervalValue = 3600*1000;		
		
		$.each(data, function(key, val){
			var type = val.type == "N/A" ? "Not Set" : val.type;
			if($.inArray(type, kNameArray) == -1){
				kNameArray.push(type);
			}									
		});	
		
		for(var n=0;n<myIntervalArray.length;n++){
			kDataArray.push(0);
		}	
		
		if(trendtype == "location" || trendtype == "subjectwidget"){
			Reduce_Array_Content(kNameArray, 10);
		}
		
		for(var i=0 ;i < kNameArray.length; i++){									
			for(var j=0 ;j < myIntervalArray.length; j++){
				var sum = 0;
				$.each(data , function(key2, val2){
					var type1 = val2.type == "N/A"? "Not Set" : val2.type;
					var hour = (new Date(val2.timestamp)).getHours();																										
					if((kNameArray[i] == type1) && (hour == myIntervalArray[j])){
						sum++;
					}											
				});
				kDataArray[j] = sum;
			}	
			
			var useThis = new Array();
			for(var a=0; a < kDataArray.length; a++){
				useThis[a] = kDataArray[a];
			}
			
			if(trendtype == "sentiment")
			{
				kSeriesArray.push({
					name:kNameArray[i],
					color:determineSeriesColour(kNameArray[i]),
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					pointInterval:myIntervalValue,
					data: useThis
				});
			}
			else
			{		
				kSeriesArray.push({
					name:kNameArray[i],
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					pointInterval:myIntervalValue,
					data: useThis
				});
			}																				
		}
		
		if(kSeriesArray.length != 0){
			LineChart(lineChartType, tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, myIntervalValue, '',kSeriesArray);
		}
		else{
			Show_No_Data("#" + tagId);
		}
	}
	else
	{		
		if(ndays <= 14)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n++;				
			}
			myIntervalValue = 24*3600*1000;			
		}
		else if(ndays <= 31)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n += 7;				
			}
			myIntervalValue = 7*24*3600*1000;
		}
		else if(ndays <= 365)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n += 31;				
			}
			myIntervalValue = 31*24*3600*1000;			
		}
		else if(ndays > 365)
		{
			n = 0;			
			while(n <= ndays)
			{
				var newDate = new Date(enddate.setDate(enddate.getDate()));
				var date = new Date(newDate.setDate(newDate.getDate() - n));					
				myIntervalArray.push(date);
				n += 365;				
			}
			myIntervalValue = 12*31*24*3600*1000;
		}		
		
		myIntervalArray.reverse();
			
		$.each(data, function(key, val){
			var type = val.type == "N/A" ? "Not Set" : val.type;
			if($.inArray(type, kNameArray) == -1){
				kNameArray.push(type);
			}									
		});	
		
		for(var n=0;n<myIntervalArray.length;n++){
			kDataArray.push(0);
		}
		
		if(trendtype == "location" || trendtype == "subjectwidget")
		{
			Reduce_Array_Content(kNameArray, 10);
		}
		
		for(var i=0 ;i < kNameArray.length; i++)
		{
			var bound = f.getTime();
			for(var j=0 ;j < myIntervalArray.length; j++)
			{
				var sum = 0;
				$.each(data , function(key2, val2)
				{
					var type1 = val2.type == "N/A" ? "Not Set" : val2.type;
					var dyd = new Date(val2.timestamp);																									
					if((kNameArray[i] == type1) && ((dyd.getTime() >= bound) && (dyd.getTime() <= myIntervalArray[j].getTime()))){
						sum++;
					}											
				});
				kDataArray[j] = sum;
				bound = myIntervalArray[j].getTime();
			}
			
			var useThis = new Array();
			for(var a=0; a < kDataArray.length; a++){
				useThis.push([Date.UTC(myIntervalArray[a].getFullYear(),myIntervalArray[a].getMonth(),myIntervalArray[a].getDate()), kDataArray[a]]);
				//useThis[a] = kDataArray[a];
			}
			
			if(trendtype == "sentiment")
			{
				kSeriesArray.push({
					name:kNameArray[i],
					color:determineSeriesColour(kNameArray[i]),
					data: useThis
					//pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					//pointInterval:myIntervalValue,
				});
			}
			else
			{		
				kSeriesArray.push({
					name:kNameArray[i],
					data: useThis
					//pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					//pointInterval:myIntervalValue,
				});
			}																				
		}
		
		if(kSeriesArray.length != 0){
			LineChart(lineChartType, tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, myIntervalValue, '',kSeriesArray);
		}
		else{
			Show_No_Data("#"+tagId);
		}		
	}
}
/*************************************************************************************************************************************/