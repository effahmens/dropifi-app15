// Javascript file for handling all charts

function drawlineChartSeriesArray(chartType,tagId,title,subtitle,yAxisLabel, CatARRAY,SERIESARRAY)
	{
		switch (chartType) 
		{		
		case 'spline':
			chart = new Highcharts.Chart({
				chart: { renderTo: tagId, type: 'spline' },
				title: { text: title.toUpperCase() },
				subtitle: { text: subtitle },
				xAxis: { 
					categories: CatARRAY,
					labels:{
						rotation: -45,align:'right',
						style:{
							font: 'normal 11px Verdana, sans-serif'
						}
					}
				},
				yAxis: { title: {text: yAxisLabel }	},
				tooltip: { 
					enabled: true,
					formatter: function() {
						return '<b>'+ this.series.name.toUpperCase() +'</b><br/>'+ this.x +': '+ this.y; 
					}
				},
				tooltip: {
					shared: true,
					crosshairs: true
				},
				
				plotOptions: {
					spline: { dataLabels: {enabled: false }, enableMouseTracking: true }
				},
				series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
			});
			break;
		default:
			break;
		}	   
	}


function messages_mailbox_LineChartSeriesArray(chartType,tagId,title,subtitle,xAxisLabel, yAxisLabel,intervalValue, CatARRAY,SERIESARRAY)
{
	switch (chartType) {
	case 'line':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'line' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue, //3600 * 1000, // one hour,
				tickWidth: 0,
				title: {
					text: xAxisLabel
				},
				labels: {
					align: 'left',
					x: 3,
					y: -3
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								hs.htmlExpand(null, {
									pageOrigin: {
										x: this.pageX,
										y: this.pageY
									},
									headingText: this.series.name,
									maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
										this.y +' visits',
									width: 200
								});
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				line: { 
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	case 'area':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'area' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue, //3600 * 1000, // one hour,
				tickWidth: 0,
				title: {
					text: xAxisLabel
				},
				labels: {
					align: 'left',
					x: 3,
					y: -3
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				area: { 
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	case 'spline':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'spline' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue,  //3600000, // one hour
				tickWidth: 0,
				title:{
					text: xAxisLabel
				},
				labels: {
					rotation: -45,
					align: 'left',
					x: 3,
					y: -3					
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				line: { 
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	case 'areaspline':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'areaspline' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue, //3600 * 1000, // one hour,
				tickWidth: 0,
				title: {
					text: xAxisLabel
				},
				labels: {
					align: 'left',
					x: 3,
					y: -3
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				areaspline: { 
					fillOpacity: 0.5,
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	default:
		break;
	}	   
}

function messages_mailbox_LineChartSeriesArray_date(chartType,tagId,title,subtitle,xAxisLabel, yAxisLabel,intervalValue, CatARRAY,SERIESARRAY)
{
	switch (chartType) {
	case 'line':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'line' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue, //3600 * 1000, // one hour,
				tickWidth: 0,
				title: {
					text: xAxisLabel
				},
				labels: {
					rotation: -45,
					align: 'left',
					x: 3,
					y: -3,
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								hs.htmlExpand(null, {
									pageOrigin: {
										x: this.pageX,
										y: this.pageY
									},
									headingText: this.series.name,
									maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
										this.y +' visits',
									width: 200
								});
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				line: { 
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	case 'area':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'area' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue, //3600 * 1000, // one hour,
				tickWidth: 0,
				title: {
					text: xAxisLabel
				},
				labels: {
					rotation: -45,
					align: 'left',
					x: 3,
					y: -3,
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				area: { 
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	case 'spline':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'spline' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue,  //3600000, // one hour
				tickWidth: 0,
				title:{
					text: xAxisLabel
				},
				labels: {
					rotation: -45,
					align: 'right',
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				line: { 
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	case 'areaspline':
		chart = new Highcharts.Chart({
			chart: { renderTo: tagId, type: 'areaspline' },
			title: { text: title.toUpperCase() },
			subtitle: { text: subtitle },
			xAxis: { 
				type: 'datetime',
				tickInterval: intervalValue, //3600 * 1000, // one hour,
				tickWidth: 0,
				title: {
					text: xAxisLabel
				},
				labels: {
					rotation: -45,
					align: 'left',
					x: 3,
					y: -3,
					style:{
						font: 'normal 11px Verdana, sans-serif'
					}
				}
			},
			yAxis: { title: {text: yAxisLabel }	},
			tooltip: { 
				enabled: true, 
				formatter: function() {
					return '<b>'+ this.series.name +'</b><br/> : '+ this.y; 
				}
			},
			tooltip: {
				shared: true,
				crosshairs: true
			},
			plotOptions: {
				series: {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {
								
							}
						}
					},
					marker: {
						lineWidth: 1
					}
				},
				areaspline: { 
					fillOpacity: 0.5,
					dataLabels: {
						enabled: false 
					},
					enableMouseTracking: true 
				}
			},
			series: SERIESARRAY //[{name: D1NAME, data: D1ARRAY}]
		});
		break;
	default:
		break;
	}	   
}

function messages_mailbox_drawPieChart(dataArray,tagId,title,chatName, percentageSize)
{
	chart = new Highcharts.Chart({
		chart: {
			renderTo: tagId,
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
		},
		title: {
			text: title.toUpperCase()
		},
		tooltip: {
			formatter: function() {
				return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.percentage, 1) + '% <br/> Total : ' + this.y;
			}
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				center:['50%','50%'],
				showInLegend: true			
			}
		},
		legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'bottom',
            x: 10,
            y: 5,
            floating: true,
            borderWidth: 1,
            backgroundColor: '#FFFFFF'
        },
		series: [{
			type: 'pie',
			name: chatName,
			data: dataArray,
			size: percentageSize,
			dataLabels: {
					enabled: true,
					distance:-40,
					color: '#000000',
					connectorColor: '#000000',
					formatter: function() {
						return '<b>' + Highcharts.numberFormat(this.percentage, 1) + '% </b>';
					}
			}
			
		
		}]
	});	
}

/********************** function to draw Sentiment trend charts *****************************/
// data is the array containing the information to be plotted.
// trendtype can be "location","AgeRange","Sentiment","Emotion" etc
// lineChartType can be "line", "area", "spline", "areaspline" etc

function drawingSentimentInitialChart(data, interval, tagId, trendtype, lineChartType, ChartTitle, ChartSubTitle, xAxisLabel, yAxisLabel)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data, function(key, val)
	{
		//check if a value is N/A and change it to Not Set
		var type = val.type == "N/A" ? "Not Set" : val.type;
		
		if($.inArray(type, kNameArray) == -1){
			kNameArray.push(type);
		}									
	});	
	
	for(var n=0;n < interval.length;n++){
		kDataArray.push(0);
	}
	
	if(trendtype == "location" || trendtype == "subjectwidget")
	{
		Reduce_Array_Content(kNameArray, 10);
	}	
	
	
	for(var i=0 ;i < kNameArray.length; i++)
	{
		var bound = interval[0].getTime();
		
		for(var j=0 ;j < interval.length; j++)
		{
			var sum = 0;
			$.each(data , function(key2, val2)
			{
				var type1 = val2.type == "N/A" ? "Not Set" : val2.type;
				var dyd = new Date(val2.timestamp);																										
				if((kNameArray[i] == type1) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}												
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}	
		
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}	
		
		
		if(trendtype == "sentiment")
		{
			kSeriesArray.push({
				name:kNameArray[i],
				color:determineSeriesColour(kNameArray[i]),
				pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
				pointInterval:7*24*3600*1000,
				data: useThis
			});
		}
		else
		{		
			kSeriesArray.push({
				name:kNameArray[i],
				pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
				pointInterval:7*24*3600*1000,
				data: useThis
			});
		}		
	}
	
	if(kSeriesArray.length != 0){
		LineChartSeriesArray(lineChartType, tagId, ChartTitle, ChartSubTitle, xAxisLabel, yAxisLabel, 604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
	
}

function drawingSentimentSingleDateChart(data, interval, tagId, trendtype, lineChartType, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, chartDate)
{
	var yr = chartDate.getFullYear();
	var mm = chartDate.getMonth();
	var dd = chartDate.getDate();								
								
	
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data, function(key, val)
	{
		var type = val.type == "N/A" ? "Not Set" : val.type;
		if($.inArray(type, kNameArray) == -1){
			kNameArray.push(type);
		}									
	});			
	
	for(var n=0;n<interval.length;n++){
		kDataArray.push(0);
	}
	
	if(trendtype == "location" || trendtype == "subjectwidget")
	{
		Reduce_Array_Content(kNameArray, 10);
	}
	
	for(var i=0 ;i < kNameArray.length; i++)
	{									
		for(var j=0 ;j < interval.length; j++)
		{
			var sum = 0;
			$.each(data , function(key2, val2)
			{
				var type1 = val2.type == "N/A"? "Not Set" : val2.type;
				var hour = (new Date(val2.timestamp)).getHours();																										
				if((kNameArray[i] == type1) && (hour == interval[j])){
					sum++;
				}											
			});
			kDataArray[j] = sum;
		}
		
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}
		
		if(trendtype == "sentiment")
		{
			kSeriesArray.push({
				name:kNameArray[i],
				color:determineSeriesColour(kNameArray[i]),
				pointStart:Date.UTC(yr,mm,dd),
				pointInterval:3600*1000,
				data: useThis
			});
		}
		else
		{		
			kSeriesArray.push({
				name:kNameArray[i],
				pointStart:Date.UTC(yr,mm,dd),
				pointInterval:3600*1000,
				data: useThis
			});
		}																					
	}
	if(kSeriesArray.length != 0){
		LineChartSeriesArray(lineChartType, tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, 3600000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}

function drawingSentimentDoubleDateChart(data, tagId, trendtype, lineChartType, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, f, t)
{	
	var startdate = new Date(f.getFullYear(),f.getMonth(), f.getDate()); 
	var utc = new Date(f.getFullYear(),f.getMonth(), f.getDate());
	var enddate = new Date(t.getFullYear(),t.getMonth(), t.getDate());						
	
	
	var myIntervalArray = [];
	var myIntervalValue = 0;
	var onedaymillisec = 24*3600*1000;
	var ndays = (enddate.getTime() - startdate.getTime())/ onedaymillisec; 							
	
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();	
	
	if(ndays == 1){
		myIntervalArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		myIntervalValue = 3600*1000;		
		
		$.each(data, function(key, val)
		{
			var type = val.type == "N/A" ? "Not Set" : val.type;
			if($.inArray(type, kNameArray) == -1){
				kNameArray.push(type);
			}									
		});	
		
		for(var n=0;n<myIntervalArray.length;n++){
			kDataArray.push(0);
		}	
		
		if(trendtype == "location" || trendtype == "subjectwidget")
		{
			Reduce_Array_Content(kNameArray, 10);
		}
		
		for(var i=0 ;i < kNameArray.length; i++)
		{									
			for(var j=0 ;j < myIntervalArray.length; j++)
			{
				var sum = 0;
				$.each(data , function(key2, val2)
				{
					var type1 = val2.type == "N/A"? "Not Set" : val2.type;
					var hour = (new Date(val2.timestamp)).getHours();																										
					if((kNameArray[i] == type1) && (hour == myIntervalArray[j])){
						sum++;
					}											
				});
				kDataArray[j] = sum;
			}	
			
			var useThis = new Array();
			for(var a=0; a < kDataArray.length; a++)
			{
				useThis[a] = kDataArray[a];
			}
			
			if(trendtype == "sentiment")
			{
				kSeriesArray.push({
					name:kNameArray[i],
					color:determineSeriesColour(kNameArray[i]),
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					pointInterval:myIntervalValue,
					data: useThis
				});
			}
			else
			{		
				kSeriesArray.push({
					name:kNameArray[i],
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					pointInterval:myIntervalValue,
					data: useThis
				});
			}																				
		}
		
		if(kSeriesArray.length != 0){
			LineChartSeriesArray(lineChartType, tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, myIntervalValue, '',kSeriesArray);
		}
		else{
			Show_No_Data("#" + tagId);
		}
	}
	else
	{		
		if(ndays <= 14)
		{			
			var n = 0;
			while(n <= ndays){				
				myIntervalArray.push(startdate);
				startdate = new Date(startdate.setDate(startdate.getDate() + 1));									
				n += 1;
			}
			myIntervalValue = 24*3600*1000;
		}
		else if(ndays <= 31)
		{			
			var n = 0; 
			while(n <= ndays){
				myIntervalArray.push(startdate);
				startdate = new Date(startdate.setDate(startdate.getDate() + 7));									
				n += 7;
			}
			myIntervalValue = 7*24*3600*1000;
		}
		else if(ndays <= 365)
		{	
			var n = 0; 
			while(n <= ndays){
				myIntervalArray.push(startdate);
				startdate = new Date(startdate.setDate(startdate.getDate() + 31));									
				n += 31;
			}
			myIntervalValue = 31*24*3600*1000;
		}
		else if(ndays > 365){
			var n = 0;
			while(n <= ndays){
				myIntervalArray.push(startdate);
				startdate = new Date(startdate.setDate(startdate.getDate() + 365));										
				n += 365;
			}
			myIntervalValue = 12*31*24*3600*1000;
		}		
			
		$.each(data, function(key, val){
			var type = val.type == "N/A" ? "Not Set" : val.type;
			if($.inArray(type, kNameArray) == -1){
				kNameArray.push(type);
			}									
		});	
		
		for(var n=0;n<myIntervalArray.length;n++){
			kDataArray.push(0);
		}
		
		if(trendtype == "location" || trendtype == "subjectwidget")
		{
			Reduce_Array_Content(kNameArray, 10);
		}
		
		for(var i=0 ;i < kNameArray.length; i++)
		{
			var bound = f.getTime();
			for(var j=0 ;j < myIntervalArray.length; j++)
			{
				var sum = 0;
				$.each(data , function(key2, val2)
				{
					var type1 = val2.type == "N/A" ? "Not Set" : val2.type;
					var dyd = new Date(val2.timestamp);																									
					if((kNameArray[i] == type1) && ((dyd.getTime() >= bound) && (dyd.getTime() <= myIntervalArray[j].getTime()))){
						sum++;
					}											
				});
				kDataArray[j] = sum;
				bound = myIntervalArray[j].getTime();
			}
			
			var useThis = new Array();
			for(var a=0; a < kDataArray.length; a++){
				useThis[a] = kDataArray[a];
			}
			
			if(trendtype == "sentiment")
			{
				kSeriesArray.push({
					name:kNameArray[i],
					color:determineSeriesColour(kNameArray[i]),
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					pointInterval:myIntervalValue,
					data: useThis
				});
			}
			else
			{		
				kSeriesArray.push({
					name:kNameArray[i],
					pointStart:Date.UTC(utc.getFullYear(),utc.getMonth(),utc.getDate()),
					pointInterval:myIntervalValue,
					data: useThis
				});
			}																				
		}
		
		if(kSeriesArray.length != 0){
			LineChartSeriesArray(lineChartType, tagId, chartTitle, chartSubTitle, xAxisLabel, yAxisLabel, myIntervalValue, '',kSeriesArray);
		}
		else{
			Show_No_Data("#"+tagId);
		}		
	}
}


/************************* Display chart functions for sentiments ******************************************/
//drawing sentiment chart
function drawSentimentTrend(data, interval, tagId)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data.trdsenmsg, function(key, val){									
		if($.inArray(val.type, kNameArray) == -1){
			kNameArray.push(val.type);
		}									
	});	
	
	for(var n=0;n<interval.length;n++){
		kDataArray.push(0);
	}
	
	for(var i=0 ;i < kNameArray.length; i++)
	{	
		var bound = interval[0].getTime();
		
		for(var j=0 ;j < interval.length; j++)
		{
			var sum = 0;
			$.each(data.trdsenmsg , function(key2, val2){
				var dyd = new Date(val2.timestamp);																										
				if((kNameArray[i] == val2.type) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}											
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}	
		
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}	
		
		kSeriesArray.push({
			name:kNameArray[i],
			color:determineSeriesColour(kNameArray[i]),
			pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
			pointInterval:7*24*3600*1000,
			data: useThis
		});																				
	}
	
	//console.log(kSeriesArray);
	if(kSeriesArray.length != 0){
		LineChartSeriesArray('spline',tagId,'Sentiment trends across all messages received','Last 30 Days','Weekly', 'No of Messages',604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#"+tagId);
	}			
}

function drawEmotionsTrend(data, interval, tagId)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data.trdemomsg, function(key, val){									
		if($.inArray(val.type, kNameArray) == -1){
			kNameArray.push(val.type);
		}									
	});	
	
	for(var n=0;n<interval.length;n++){
		kDataArray.push(0);
	}
	
	for(var i=0 ;i < kNameArray.length; i++)
	{
		var bound = interval[0].getTime();
		
		for(var j=0 ;j < interval.length; j++)
		{
			var sum = 0;
			$.each(data.trdemomsg , function(key2, val2){
				var dyd = new Date(val2.timestamp);																										
				if((kNameArray[i] == val2.type) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}											
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}	
		
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}
		
		kSeriesArray.push({
			name:kNameArray[i],
			pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
			pointInterval:7*24*3600*1000,
			data: useThis
		});																				
	}
	if(kSeriesArray.length != 0){
		LineChartSeriesArray('line',tagId,'Trending Emotions and Number of Messages','Last 30 Days','Weekly', 'No of Messages',604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}

function drawSocialNetworkTrend(data, interval, tagId)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data.trdsocmsg, function(key, val){									
		if($.inArray(val.type, kNameArray) == -1){
			kNameArray.push(val.type);
		}									
	});	
	
	for(var n=0;n<interval.length;n++){
		kDataArray.push(0);
	}	
	
	for(var i=0 ;i < kNameArray.length; i++){	
		var bound = interval[0].getTime();
		for(var j=0 ;j < interval.length; j++){
			var sum = 0;
			$.each(data.trdsocmsg , function(key2, val2){
				var dyd = new Date(val2.timestamp);																										
				if((kNameArray[i] == val2.type) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}											
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}	
		
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}
		
		kSeriesArray.push({
			name:kNameArray[i],
			pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
			pointInterval:7*24*3600*1000,
			data: useThis
		});																				
	}
	if(kSeriesArray.length != 0){
		LineChartSeriesArray('areaspline',tagId,'Trending social networks across messages received','Last 30 Days','Weekly', 'No of Messages',604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}

function drawAgeRangeTrend(data, interval, tagId)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data.trdagemsg, function(key, val){	
		var age = val.type == "N/A" ? "Not Set" : val.type; 
		if($.inArray(age, kNameArray) == -1){
			kNameArray.push(age);
		}									
	});	
	
	for(var n=0;n<interval.length;n++){
		kDataArray.push(0);
	}	
	
	for(var i=0 ;i < kNameArray.length; i++){	
		var bound = interval[0].getTime();
		for(var j=0 ;j < interval.length; j++){
			var sum = 0;
			$.each(data.trdagemsg , function(key2, val2){
				var dyd = new Date(val2.timestamp);
				var age1 = val2.type == "N/A" ? "Not Set" : val2.type;
				if((kNameArray[i] == age1) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}											
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}											
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}										
		kSeriesArray.push({
			name:kNameArray[i],
			pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
			pointInterval:7*24*3600*1000,
			data: useThis
		});																				
	}
	if(kSeriesArray.length != 0){
		LineChartSeriesArray('spline',tagId,'Trending Age Range And Number of Messages','Last 30 Days','Weekly', 'No of Messages',604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}

function drawWidgetSubjectTrend(data, interval, tagId)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data.trdsubmsg, function(key, val){									
		if($.inArray(val.type, kNameArray) == -1){
			kNameArray.push(val.type);
		}									
	});		
	
	for(var n=0;n<interval.length;n++){
		kDataArray.push(0);
	}	
	
	for(var i=0 ;i < kNameArray.length; i++){
		var bound = interval[0].getTime();
		for(var j=0 ;j < interval.length; j++){
			var sum = 0;
			$.each(data.trdsubmsg , function(key2, val2){
				var dyd = new Date(val2.timestamp);																										
				if((kNameArray[i] == val2.type) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}											
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}											
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}										
		kSeriesArray.push({
			name:kNameArray[i],
			pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
			pointInterval:7*24*3600*1000,
			data: useThis
		});																				
	}
	if(kSeriesArray.length != 0){
		LineChartSeriesArray('spline',tagId,'Trends associated with your widget subjects','Last 30 Days','Weekly', 'No of Messages',604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}

function drawLocationTrend(data, interval, tagId)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data.trdlocmsg, function(key, val){
		var type = val.type == "N/A" ? "Not Set" : val.type;
		if($.inArray(type, kNameArray) == -1){
			kNameArray.push(type);
		}									
	});	
	
	for(var n=0;n < interval.length;n++){
		kDataArray.push(0);
	}
	
	Reduce_Array_Content(kNameArray, 10);
	
	for(var i=0 ;i < kNameArray.length; i++){	
		var bound = interval[0].getTime();
		for(var j=0 ;j < interval.length; j++){
			var sum = 0;
			$.each(data.trdlocmsg , function(key2, val2){
				var dyd = new Date(val2.timestamp);
				var type1 = val2.type == "N/A" ? "Not Set" : val2.type;
				if((kNameArray[i] == type1) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}											
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}											
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}										
		kSeriesArray.push({
			name:kNameArray[i],
			pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
			pointInterval:7*24*3600*1000,
			data: useThis
		});																				
	}
	if(kSeriesArray.length != 0){
		LineChartSeriesArray('area',tagId,'Trends associated with locations and number of messages received','Last 30 Days','Weekly', 'No of Messages',604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}

function drawKeywordTrend(data, interval, tagId)
{
	var kNameArray = new Array();		
	var kDataArray = new Array();	
	var kSeriesArray = new Array();
	
	$.each(data.trdkwdmsg, function(key, val){									
		if($.inArray(val.type, kNameArray) == -1){
			kNameArray.push(val.type);
		}									
	});	
	
	for(var n=0;n < interval.length;n++){
		kDataArray.push(0);
	}
	
	for(var i=0 ;i < kNameArray.length; i++){
		var bound = interval[0].getTime();
		for(var j=0 ;j < interval.length; j++){
			var sum = 0;
			$.each(data.trdkwdmsg , function(key2, val2){
				var dyd = new Date(val2.timestamp);																										
				if((kNameArray[i] == val2.type) && ((dyd.getTime() >= bound) && (dyd.getTime() <= interval[j].getTime()))){
					sum++;
				}												
			});
			kDataArray[j] = sum;
			bound = interval[j].getTime();
		}											
		var useThis = new Array();
		for(var a=0; a < kDataArray.length; a++){
			useThis[a] = kDataArray[a];
		}										
		kSeriesArray.push({
			name:kNameArray[i],
			pointStart:Date.UTC(interval[0].getFullYear(),interval[0].getMonth(),interval[0].getDate()),
			pointInterval:7*24*3600*1000,
			data: useThis
		});																				
	}
	if(kSeriesArray.length != 0){
		LineChartSeriesArray('spline',tagId,'Trends associated with keywords','Last 30 Days','Weekly', 'No of Messages',604800000, '',kSeriesArray);
	}
	else{
		Show_No_Data("#" + tagId);
	}
}