var UserEventTrack = { 
	init: function(data){
		if(data){
			this.userEmail = data.userEmail;
			this.userType = data.userType;
			this.widgetType = data.widgetType;
			this.userName = data.userName
			
			mixpanel.people.set({ 
			    "LastPath": window.location.pathname
			},function(){
				 
			}); 
			
		   mixpanel.identify(data.userEmail);
		}
	},
	initPageView: function(data){
		mixpanel.people.set({ 
			 "referrer": document.referrer
		},function(){
			 
		});
	},
	identify:function(){
		if(this.userEmail){
			mixpanel.identify(this.userEmail);
		}

	}, 
	countSetting: function(data){
		if(data){
			mixpanel.people.set(data.event,data.count); 
			this.identify();
		}
	},
	click: function(data){
		if(data){
			mixpanel.track(data.event,{
				'UserEmail':this.userEmail, 				 
				'UserType':this.userType,
				'WidgetType':this.widgetType,
				'$direct':document.referrer
			},function(){ 
				  
			});
			
			this.googleAnalyticEvent(data);
			
		}
	}, 
	pageView: function(data){ 
		if(data){
			mixpanel.track(data.event, {
				'PageName' : document.title, 
				'PageUrl' : window.location.pathname
			},function(){
				 
			});
			
			this.initPageView(data);
			this.identify();
			this.googleAnalyticEvent(data);
		}
	},
	googleAnalyticEvent: function(data){
		//create event in google analytics 
		var _ga = ga || undefined;
		if(_ga!=undefined && _ga!=null){
			ga('send', 'event', data.event, this.widgetType, this.userEmail);
		}
	}
}
