$(document).ready(function(){
    $('#tip').tipsy({gravity:'w'}); 
    $('.data_view').tipsy({gravity:'n'});
		  
     //activated toggle start
	 $('body').attr("class", "js");

	selectedAllRules();
    loadfancybox();
    
    $("#add_user_btn").click(function(){
        //mixpanel - track user clicking on add widget rule 
        UserEventTrack.click({'event':'ADD WIDGET-RULE CLICK'}); 
    });
    
    //mixpanel - track user viewing/loading of widget rule page 
    UserEventTrack.click({'event':'WIDGET-RULE PAGE VIEW'}); 
});

function replaceAll(txt, replace, with_this) {
  return txt.replace(new RegExp(replace, 'g'),with_this);
}

function loadfancybox(){ 	
	
	$(".fancy_window").fancybox({
		maxWidth	: 900,
		maxHeight	: 800,
		fitToView	: true,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		:  0,
		afterClose	: function(){ 

			selectedAllRules();
			if(!DropifiToolTips.hasError){
                //mixpanel - track success add/update of widget rule         
                UserEventTrack.click({'event':DropifiToolTips.event}); 
            }
		}
	});
	
}

function activatedMsgRule(title,activated){
	if(title!=undefined && title != ""){
		  $.ajax({    		  
		      type: "POST", 
		      url: url.activated,  
		      data: {'title':title,'activated': activated},
		      success: function(data){ 
		    	  if(data.status ==200){ 		    		  
		    	  }     	    	  
		      },
		      
		      error:function(data){ 
		      }
		      
		  });
	}
	//mixpanel - track user clicking on activate widget rule
    UserEventTrack.click({'event':'ACTIVATE WIDGET-RULE CLICK'}); 
}

function confirm(message, callback) {
	$('#confirm').modal({
		closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
		position: ["20%",],
		overlayId: 'confirm-overlay',
		containerId: 'confirm-container', 
		onShow: function (dialog) {
			var modal = this;

			$('.message', dialog.data[0]).append(message);

			// if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}
				// close the dialog
				modal.close(); // or $.modal.close();
			});
		}
	});
}

function selectedAllRules(){
	
	$.getJSON(url.populate, function(data) {
		var service = data.service;
		UserVolumeService.init(service); 
		if(service){   
			  if(service.hasExceeded){
			  	$("#btnAdd").html("<a class='get_btn_small get_upgrade_small' href='"+url.pricing+"'>Upgrade to add more Widget Rules</a>");
			  }else{ 
				$("#btnAdd").html('<a href="'+url.show +'?msgrule=new" title = "Widget Rules" class="get_btn_small fancy_window fancybox.iframe" id="add_user_btn" >Add a Widget Rule</a>');  
			  }
		}
		
		  $('#number_of_rules').text('Message Routing Rules ('+data.size+")");
		  
		  var items = [];
		  var msgrules = "";
		  $.each(data.msgRules, function(key, val) {	
			   var titleName = val.name.replace("&","~~"); 
			   var dmsg =  (val.id<=service.threshold.volume)?"":"deactivated";
			   msgrules += '<div class="data_view ui-state-default" style="background:inherit" title="Reorder widget rule by dragging it up or down">'+
	             '<div class="data_activated">'+
	               '<p><span>activated?</span>'+
	               '<input type="hidden" id="activatede'+val.id+'"  value ="'+val.activated+'" />'+
	        		'<input class="checkbox" type="checkbox" name="'+val.name+'" value="'+val.name+'" id="e'+val.id+'" checked="checked" />'+
	               '</p>'+
	             '</div>'+	             
	             '<div class="data_view_content" id="e'+val.id+'">'+
	             '<p class="admin_element"><span class="'+dmsg+'">'+val.name+'</span> <span class="admin_option">'+dmsg+'</span></p>';
   
			   if(val.id<=service.threshold.volume){
				 msgrules +='<p class="admin_element_option"><span class="element_option">rule:</span> <span class="element_value">';
				 
	             var ruleGroup = val.ruleGroup.toLowerCase() == "all"?"AND":"OR";
	             var condAction =" IF ";
	             
	             var condLen = val.ruleConditions.length;
	             var condCounter = 1;
	             $.each(val.ruleConditions,function(key, value){
	             //if(val.ruleConditions != undefined && val.ruleConditions.length>0){ 
	            	// var value = val.ruleConditions[0];
	            	 condAction += '( '+ value.condition + "  "+ value.comparison +" "+ (value.expectation==undefined?" ":value.expectation) +" ) "; 
	            	 if(condCounter<condLen){
	            		 condAction +=" AND "; 
	            	 }
	            	 condCounter++; 
	            // } 
	             });
	             condAction += "<br/>THEN ";
	            $.each(val.ruleActions,function(key,value){ 
	        	// if(val.ruleActions != undefined && val.ruleActions.length>0){ 
	        		// var value = val.ruleActions[0]; 
	            	 condAction += ' ( '+ value.action + " "+ (value.expectation == undefined ? " ": " - "+value.expectation) + " )  ";
	        	// } 	        	 
	            });
	            
	             condAction = replaceAll(condAction,'_',' ');
	             msgrules += condAction; 
			  
	             msgrules += '</span>'+
	             '</p>'+
	             '<p class="admin_element_option">'+
				 '<span class="element_option">created:</span> <span class="element_value">'+ val.created +'</span>'+
				 '<span class="element_option">updated:</span> <span class="element_value">'+ val.updated +'</span>'+  
			 	 '</p>'+
	             '<p class="admin_element_option">'+
	                '<span class="action_options"><a href="'+url.show+'?msgrule='+titleName+'"title= "Widget Rules" class="fancy_window fancybox.iframe edit_widgetrule">edit</a>'+ 
	                '<a href="#" class="confirmDelete" id ="'+val.name+'">delete</a></span>'+
	                '<input type="hidden" id = "drage'+val.id+'" name="draggable_rule" value="'+val.name+'" class="draggable_rule" ref_activated="'+val.activated+'"/>'+
	             '</p>'+
	             '</div>'+
	            ' </div>';  
	             UserVolumeService.setRefExceeded('e'+val.id, UserVolumeService.active);
			   }else{
				   msgrules += '</span>'+
		             '</p>'+
		             '<p class="admin_element_option">'+
		             '<span class="element_option"><a class="get_upgrade_small" href="'+url.pricing+'">Upgrade to activate this widget rule</a></span>'+
				 	 '</p>'+
		             '<p class="admin_element_option">'+
		                '<span class="action_options">'+
		                /*'<a href="'+url.show+'?msgrule='+val.name+'"title= "Widget Rules" class="fancy_window fancybox.iframe edit_widgetrule">edit</a>'+ */
		                '<a href="#" class="confirmDelete" id ="'+val.name+'">delete</a></span>'+
		                '<input type="hidden" id = "drage'+val.id+'" name="draggable_rule" value="'+val.name+'" class="draggable_rule" ref_activated="'+val.activated+'"/>'+
		             '</p>'+
		             '</div>'+ 
		            ' </div>';
				   
				   if(val.activated) {activatedMsgRule(val.name,false);}
		           UserVolumeService.setRefExceeded('e'+val.id,UserVolumeService.inactive);
			   }
		  });
		  
		  $('#right_work_area').html(msgrules); 

		    $('.checkbox').after(function() { 
		    	var checkId = $(this).attr("id");
		    	var activated = new String($('#activated'+checkId).val());
		    	var toggle_view=$('div#'+checkId);	 
		    	
		        if (activated == "true") {	
		        	toggle_view.removeClass("ticked");	
		           return "<a href='#' class='toggle checked' ref='" + checkId + "'></a>";
		        } else {	
		        	toggle_view.addClass("ticked");	
		        		
		           return "<a href='#' class='toggle' ref='" + checkId + "'></a>";
		        } 
		    	
		    });
		    
		    $('.toggle').click(function(e) {
		    	 
		        var checkboxID = $(this).attr("ref");
		        var checkbox = $('input#' + checkboxID);
				var toggle_view=$('div#'+checkboxID);							
				var me = new String($('#activated'+checkboxID).val());
				var refExceeded = UserVolumeService.getRefExceeded(checkbox.attr('id')); 
				//checkbox.is(":checked")
				
				if(refExceeded != UserVolumeService.inactive){
			        if (me=="true") {		        	
			        	//checkbox.attr("value", "false");
			            checkbox.attr("checked",false);
			            $('#activated'+checkboxID).val('false');          
			           toggle_view.addClass("ticked");		            
			        } else {		        	
			        	checkbox.attr("checked", true);
			        	$('#activated'+checkboxID).val('true');		             
						toggle_view.removeClass("ticked");						
			        }
			        
			        //update the changes to activated
			        var title =$('#'+checkboxID).val();	
			        var activated =  $('#activated'+checkboxID).val(); 
			        $("#drag"+checkboxID).attr("ref_activated",activated); 
			        
			        //checkbox.is(":checked"); 
			       activatedMsgRule(title,activated); 
			       $(this).toggleClass("checked");	
				}
		        e.preventDefault();

		    });

		    $('#showCheckboxes').click(function(e) {
		        $('.checkbox').toggle();         
		        e.preventDefault();
		    });
		    
		    $('.confirmDelete').click(function (e) {
		    	 
				e.preventDefault();
				var name = $(this).attr("id");		    	
				// example of calling the confirm function
				// you must use a callback function to perform the "yes" action
				confirm("Are you sure you want to delete ("+name+") widget rule?", function () {
					
					//delete the mailbox					
			    	$.ajax({ 
			    		type:"POST",
			    		url: url.deleteService,
			    		data:{"name" : name},
			    		success: function(data){				    			 
			    			selectedAllRules();
			    		},
			    		error:function(data){
			    		}
			    	});
					
				});
				
				//mixpanel - track user clicking on delete widget rule
	            UserEventTrack.click({'event':'DELETE WIDGET-RULE CLICK'});
			});
		    
		    //loadfancybox();
		    jQuery(".info_tooltip_close").click(function(){
		        jQuery("#btn_tooltip").css({'display':'none'}); 
		        //jQuery("#${_idHelp}").css({'display':'none'}); 
		    });
		    
		    $(".edit_widgetrule").click(function(){
		    	//mixpanel - track user clicking on edit widget rule 
	            UserEventTrack.click({'event':'EDIT WIDGET-RULE CLICK'});  
		    });
		    
		    $( "#right_work_area" ).sortable();
		    $( "#right_work_area" ).disableSelection();
		    $( "#right_work_area" ).on( "sortupdate", function( event, ui ) {
		    	  var drules = [];
		    	  $(".draggable_rule").each(function(key,value){
		    		  var json = new Object();
		    		  json.name = $(this).val();
		    		  json.activated = $(this).attr("ref_activated"); 
		    		  drules.push(JSON.stringify(json));
		    	  }); 
		    	  
		    	 $.ajax({		    		
		    		type:"POST",
		    		url: url.orderService,
		    		data:{"rules" : drules}, 
		    		success: function(data){
		    			selectedAllRules();
		    		},
		    		error:function(data){
		    			//alert("error: "+data.error);
		    		}
			     });
		    });
		    
		    UserEventTrack.countSetting({'event':'Widget-Rules Added','count':data.msgRules.length});
	});
  
}