$(document).ready(function() {
	//Tips for all the sub feature settings
	$('#tip').tipsy({gravity:'w'});
	$('.data_view').tipsy({gravity:'n'});
	
	//activate toggle start
	$('body').attr("class", "js");
	
	selectedAllRecipients();
	loadfancybox();
    
	$("#add_user_btn").click(function(){
       //mixpanel - track user clicking on add message recipient 
       UserEventTrack.click({'event':'ADD MESSAGE-RECIPIENT CLICK'}); 
    });
    
    //mixpanel - track user viewing/loading of message recipient page 
    UserEventTrack.click({'event':'MESSAGE-RECIPIENT PAGE VIEW'});
 
}); 

function loadfancybox(){
	$(".fancy_window").fancybox({
		maxWidth	: 580,
		minHeight	: 300, 
		fitToView	: true,
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		:  0,
		afterClose	: function(){   
			selectedAllRecipients();
			if(!DropifiToolTips.hasError){
                //mixpanel - track success add/update of message recipients      
                UserEventTrack.click({'event':DropifiToolTips.event});
            } 
		}
	});
	
}

function selectedAllRecipients(){
	
	$.getJSON(url.populate, function(data){
		  var service = data.service;
		  UserVolumeService.init(service);
		  
		  if(service){
			  if(service.hasExceeded){
			  	$("#btnAdd").html("<a class='get_btn_small get_upgrade_small' href='"+url.pricing+"'>Upgrade to add more Recipients</a>");
			  }else{
				$("#btnAdd").html('<a href="'+url.show +'?email=new" class="get_btn_small fancy_window fancybox.iframe" id="add_user_btn" >Add a Recipient</a>');  
			  }
		  }
		  
		  $('.numAgents').text('Message Recipients ('+data.size+")");
		  
		  var items = [];
		  var recipientList="";
		  $.each(data.recipients, function(key, val) {	
			  var dexlimit = val.id>service.threshold.volume?"data_exceeded_limit":"";
			  if(val.id<=service.threshold.volume){
				  recipientList +=
		             '<div class="data_view '+dexlimit+' ui-state-default" style="background:inherit" id="view'+val.id+'" title="Reorder recipient by dragging it up or down">'+
			             '<div class="data_activated">'+
			             '<p> <input type="hidden" id="activatede'+ val.id +'"  value ="'+val.activated+'" />'+
			             '<input class="checkbox" type="checkbox"  name= "'+val.email+'" value="'+val.email+'" id="e'+val.id+'" checked = "checked" />'+
			             '</p>'+
			             '</div>'+
		             
			         '<div class="data_view_content"  id="e'+val.id+'">'+   
			             '<p class="admin_element">'+val.username+'<span>'+val.email+'</span></p>'+	             
			             '<p class="admin_element_option">'+
			                '<span class="element_option">created: </span> <span class="element_value">'+val.created+ '</span>'+
			                '<span class="element_option">updated: </span> <span class="element_value">'+val.updated+'</span>'+
			             '</p>'+ 
			             '<p class="admin_element_option">'+
			                '<span class="action_options">'+
			             	'<a href="'+url.show+'?email='+val.email +'" class="fancy_window fancybox.iframe edit_recipient">edit</a>'+
			                '<a href="#"  class="confirmDelete" id="'+ val.email +'">delete</a>'+
			                '</span>'+  	
			                '<input type="hidden" id = "drage'+val.id+'" name="draggable_recipient" value="'+val.email+'" class="draggable_recipient" ref_username="'+val.username+'" ref_activated="'+val.activated+'"/>'+
			             '</p>'+  
		             '</div>' + 
	             '</div>'; 
	             UserVolumeService.setRefExceeded('e'+val.id, UserVolumeService.active);
			  }else{
				  recipientList +=
					  '<div class="data_view data_exceeded_limit ui-state-default" style="background:inherit" id="view'+val.id+'" title="Reorder recipient by dragging it up or down">'+
			             '<div class="data_activated">'+
			             '<p> <input type="hidden" id="activatede'+ val.id +'"  value ="'+val.activated+'" />'+
			             '<input class="checkbox" type="checkbox"  name= "'+val.email+'" value="'+val.email+'" id="e'+val.id+'" checked = "checked" />'+
			             '</p>'+
			             '</div>'+
		             
			         '<div class="data_view_content"  id="e'+val.id+'">'+  
			             '<p class="admin_element"><span class="deactivated">'+val.username+' '+val.email+'</span> <span class="admin_option">deactivated</span></p>'+	             
			             '<p class="admin_element_option">'+
			                '<span class="admin_option"><a class="get_upgrade_small" href="'+url.pricing+'">Upgrade to activate this message recipient</a></span>'+
			             '</p>'+ 
			             '<p class="admin_element_option">'+
			                '<span class="action_options">'+
			                //*{'<a href="/#{get "domain" /}/admin@{Recipients.show}?email='+val.email +'" class="fancy_window fancybox.iframe edit_recipient">edit</a>'+}*
			                '<a href="#" class="confirmDelete" id="'+ val.email +'">delete</a>'+ 
			                '</span>'+ 	 
			                '<input type="hidden" id = "drage'+val.id+'" name="draggable_recipient" value="'+val.email+'" class="draggable_recipient" ref_username="'+val.username+'" ref_activated="'+val.activated+'"/>'+
			             '</p>'+   
		             '</div>' + 
	             '</div>';
	             if(val.activated) {activatedRecipient(val.email,false)}; 
	             UserVolumeService.setRefExceeded('e'+val.id,UserVolumeService.inactive);
			  }
		  });
		  
		  $('#right_work_area').html(recipientList);
		  
		  $('.checkbox').after(function() {
		    	var checkId = $(this).attr("id");
		    	var activated = new String($('#activated'+checkId).val());
		    	var toggle_view=$('div#'+checkId);	 
		    	
		        if (activated == "true") {	
		        	toggle_view.removeClass("ticked");	
		           return "<a href='#' class='toggle checked' ref='" + checkId + "'></a>";
		        } else {	
		        	toggle_view.addClass("ticked");		
		           return "<a href='#' class='toggle' ref='" + checkId + "'></a>";
		        } 
		    	
		    });
		    
		    $('.toggle').click(function(e) {
		    	var checkboxID = $(this).attr("ref");
		        var checkbox = $('input#' + checkboxID);
				var toggle_view=$('div#'+checkboxID);							
				var me = new String($('#activated'+checkboxID).val()); 
				var refExceeded = UserVolumeService.getRefExceeded(checkbox.attr('id')); 
				//checkbox.is(":checked") 
				
				if(refExceeded != UserVolumeService.inactive){
			        if (me=="true") {		        	
			        	//checkbox.attr("value", "false");
			            checkbox.attr("checked",false);
			            $('#activated'+checkboxID).val('false');          
			           toggle_view.addClass("ticked");		            
			        } else {		        	
			        	checkbox.attr("checked", true);
			        	$('#activated'+checkboxID).val('true');		             
						toggle_view.removeClass("ticked");						
			        }
			        
			        //update the changes to activated 
			        var email = $('#'+checkboxID).val(); 	
			        var activated =  $('#activated'+checkboxID).val(); 
			        $("#drag"+checkboxID).attr("ref_activated",activated); 
			        //checkbox.is(":checked"); 
			        
			        activatedRecipient(email,activated);
	
			        $(this).toggleClass("checked");		        
				}
		        e.preventDefault();

		    });

		    $('#showCheckboxes').click(function(e) {
		        $('.checkbox').toggle();         
		        e.preventDefault();
		    });
		    
		    
	      $('.confirmDelete').click(function (e) {
	    	 
			e.preventDefault(); 
			
			var recip = $(this).attr("id");					 
			// example of calling the confirm function
			// you must use a callback function to perform the "yes" action
			confirm("Are you sure you want to delete the recipient - ("+recip+") ?", function (){  	
				//delete the agent						    	
		    	$.ajax({			    		
		    		type:"POST",
		    		url: url.deleteService, 
		    		data:{"email" : recip}, 
		    		success: function(data){			    			 
		    			selectedAllRecipients();
		    		},
		    		error:function(data){
		    			//alert(data.error);
		    		}
		    	});
				
			});
            UserEventTrack.click({'event':'DELETE MESSAGE-RECIPIENT CLICK'});
		});
		
	    $(".edit_recipient").click(function(){ 
            UserEventTrack.click({'event':'EDIT MESSAGE-RECIPIENT CLICK'});  
	    });
	    
	    $( "#right_work_area" ).sortable();
	    $( "#right_work_area" ).disableSelection();
	    $( "#right_work_area" ).on( "sortupdate", function( event, ui ) {
	    	  var drecipients = []; 
	    	  $(".draggable_recipient").each(function(key,value){
	    		  var json = new Object();
	    		  json.email = $(this).val();
	    		  json.username = $(this).attr("ref_username");
	    		  json.activated = $(this).attr("ref_activated"); 
	    		  drecipients.push(JSON.stringify(json)); 
	    	  }); 
	    	  
	    	 $.ajax({ 			    		
	    		type:"POST",
	    		url: url.orderService,
	    		data:{"recipients" : drecipients}, 
	    		success: function(data){
	    			selectedAllRecipients();
	    		},
	    		error:function(data){
	    			//alert("error: "+data.error);
	    		}
		     });
	    });
		 
	    UserEventTrack.countSetting({'event':'Recipients Added','count':data.recipients.length}); 
	});
  
}

function activatedRecipient(email,activated){ 
	 
  $.ajax({    		  
      type: "POST", 
      url: url.activated, 
      data: {'email':email,'activated': activated},
      success: function(data){ 	     	    	     	    	  
      }, 	      
      error:function(data){	
      }
      
   });
	  
   //mixpanel - track user clicking on activate message recipients 
   UserEventTrack.click({'event':'ACTIVATE MESSAGE-RECIPIENT CLICK'}); 
}

function confirm(message, callback) {
	$('#confirm').modal({
		closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
		position: ["20%",],
		overlayId: 'confirm-overlay',
		containerId: 'confirm-container', 
		onShow: function (dialog) {
			var modal = this; 
			$('.message', dialog.data[0]).append(message);

			// if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}
				// close the dialog
				modal.close(); // or $.modal.close();
			});
		}
	});
}

/*
$(function() {
	//a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore
	$( "#dialog:ui-dialog" ).dialog( "destroy" );

	$( "#dialog-confirm" ).dialog({
		resizable: false,
		height:140,
		modal: true,
		buttons: {
			"Delete all items": function() {
				$( this ).dialog( "close" );
			},
			
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		}
	});
});
*/