var UserVolumeService = {		
	init :function(service){
		if(service){			
			this.name = service.name;
			this.volume = service.threshold.volume;
			this.hasExceeded = service.hasExceeded;
			this.consumedVolume = service.consumedVolume;
			this.refExceeded = [];
		}
		this.active = "activatedVolume";
		this.inactive = "deactivatedVolume";
	},
	
	setRefExceeded: function(key,value){
		this.refExceeded[key]=value; 
	},
	
	getRefExceeded: function(key){
		return this.refExceeded[key];
	}
	
}

var UserActivatedService ={
	init: function(service){
		if(service){
			this.name = service.name;
			this.activated = service.activated;
		}
	}
}

var url={
	init: function(data){
		if(data){
			this.domain = data.domain;
			this.show = data.show;
			this.pricing = data.pricing;
			this.populate = data.populate;
			this.deleteService = data.deleteService; 
			this.activated = data.activated;
			this.orderService = data.orderService;
			
			if(data.mailbox)
				this.mailbox=data.mailbox;
		}
	},
	setSelMailbox: function(mb){
		this.mailbox =mb;
	}
}
