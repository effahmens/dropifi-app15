$(document).ready(function() {
	
	//Tips for all the sub feature settings
	$('#tip').tipsy({gravity:'w'});   
	selectedAllAgents();
	loadfancybox(); 

	$("#add_user_btn").click(function(){
        //mixpanel - track user clicking on add user
        UserEventTrack.click({'event':'ADD USER CLICK'});
    });
    
    //mixpanel - track user viewing/loading of widget rule page 
    UserEventTrack.click({'event':'USER PAGE VIEW'}); 

}); 

function loadfancybox(){	
	$(".fancy_window").fancybox({ 
		maxWidth	: 550,
		minHeight	: 290, 
		fitToView	: true,
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none', 
		padding		:  0,
		afterClose	: function(){ 
			selectedAllAgents(); 
			if(!DropifiToolTips.hasError){
	            //mixpanel - track success add/update user         
	            UserEventTrack.click({'event':DropifiToolTips.event});
	        }       
		}
	});
	
}

function selectedAllAgents(){
	
	$.getJSON(url.populate, function(data) {
		  var service = data.service;
		  UserVolumeService.init(service); 
		  
		  if(service){
			  if(service.hasExceeded){ 
				  $("#btnAdd").html("<a class='get_upgrade_small get_btn_small' href='"+url.pricing+"'>Upgrade to add more Agents</a>");
			  }else{ 
				 $("#btnAdd").html('<a href="'+url.show +'?email=new" class="get_btn_small fancy_window fancybox.iframe" id="add_user_btn" >Add an Agent</a>');  
			  }
		  }
		  		  
		  $('.numAgents').text('Agents ('+data.size+")"); 
		  
		  var items = [];
		  var agent="";
		  $.each(data.agents, function(key, val) {
			  var dmsg =  (val.uid<=service.threshold.volume)?"":"deactivated"; 
			  var lastLogin;
	          if(val.lastLogin){lastLogin = val.lastLogin;}else{lastLogin = "Not Login";}	
	          
			  agent += 
	             '<div class="data_view">'+
	             '<p class="admin_element"><span class="'+dmsg+'">'+val.username+' '+val.email+'</span><span class="admin_option">'+dmsg+'</span></p>';
			  
			  if(val.uid<=service.threshold.volume){
	            agent += '<p class="admin_element_option"><span class="admin_option">assigned to</span> <span class="admin_value">'; 
	            
	             var roles="";
	             var n  = val.agentRoles.length;
	             var counter = 0;
	             $.each( val.agentRoles, function(key, val) {
	            	 roles += val.roleName; 
	            	 if((n) != (++counter))
	            		 roles += " , "; 
	             });
	             
	             agent += roles
	             +'</span></p>'+
	             '<p class="admin_element_option"><span class="admin_option">date created</span> <span class="admin_value">'+val.created+ '</span>'+
	                '<span class="admin_option">last login</span> <span class="admin_value">'+lastLogin+'</span>'+
	                '<span class="action_options"><a href="'+url.show+'?email='+val.email+'" class="fancy_window fancybox.iframe edit_user">edit</a>'+
	                '<a href="#"  class="confirmDelete" id="'+val.email+'">delete</a></span>'+	                
	             '</p>';
			  }else{
				  agent += '<p class="admin_element_option">'+
				  '<span class="element_option"><a class="get_upgrade_small" href="'+url.pricing+'">Upgrade to activate this agent</a></span>'+
	               '<span class="action_options">'+
	                //'<a href="'+url.show+'?email='+val.email+'" class="fancy_window fancybox.iframe edit_user">edit</a>'+
	                '<a href="#"  class="confirmDelete" id="'+val.email+'">delete</a></span>'+	                
	             '</p>';
				 
				 if(val.activated==true || val.activated == undefined){
					 activatedAgent(val.email,false);
				 } 
			  }
			  
             agent+='</div>';                 	 
		  }); 
		  
		  $('#right_work_area').html(agent);
		    
		    $('.confirmDelete').click(function (e) {
		    	 
				e.preventDefault();
				
				var agent = $(this).attr("id");					 
				// example of calling the confirm function
				// you must use a callback function to perform the "yes" action
				confirm("Are you sure you want to delete the agent - ("+agent+") ?", function (){  
					
					//delete the agent						    	
			    	$.ajax({			    		
			    		type:"POST",
			    		url: url.deleteService,
			    		data:{"email" : agent}, 
			    		success: function(data){			    			 
			    			selectedAllAgents();
			    		},
			    		error:function(data){
			    		}
			    	}); 
					
				});
				//mixpanel - track user clicking on delete user
                UserEventTrack.click({'event':'DELETE USER CLICK'});
			});
		    
		    $(".edit_user").click(function(){
		    	//mixpanel - track user clicking on edit user
                UserEventTrack.click({'event':'EDIT USER CLICK'});  
		    });
		    //loadfancybox();
		 
		    UserEventTrack.countSetting({'event':'Users Added','count':data.agents.length});
	});
  
}

function activatedAgent(email,activated){ 
  $.ajax({    		  
      type: "POST",
      url: url.activated, 
      data: {'email':email,'activated': activated},
      success: function(data){ 	     	    	     	    	  
      }, 	      
      error:function(data){	
      }
      
   });
	  
   //mixpanel - track user clicking on activate message recipients 
   UserEventTrack.click({'event':'ACTIVATE USER CLICK'}); 
}

function confirm(message, callback) {
	$('#confirm').modal({
		closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
		position: ["20%",],
		overlayId: 'confirm-overlay',
		containerId: 'confirm-container', 
		onShow: function (dialog) {
			var modal = this;

			$('.message', dialog.data[0]).append(message);

			// if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}
				// close the dialog
				modal.close(); // or $.modal.close();
			});
		}
	});
}
