$(document).ready(function() { 
	 $('#tip').tipsy({gravity:'w'}); 
	 $('.data_view').tipsy({gravity:'n'});
	 
	 //activated toggle start
	 $('body').attr("class", "js");
	 
	//select the mailboxes
	selectedAllMailboxes();  	
	loadfancybox();
	
	$("#btn_add_mailboxes").click(function(){
		//mixpanel - track user clicking on add mailbox 
        UserEventTrack.click({'event':'ADD MAILLBOX CLICK'}); 
	});
	
	
	//mixpanel - track user viewing/loading of mailbox page 
    UserEventTrack.click({'event':'MAILBOX PAGE VIEW'});
    $("#sel_mailboxes").trigger("click");
});

function loadfancybox(){ 	
	
	$(".fancy_window").fancybox({
		maxWidth	: 590, 
		minHeight	: 500,
		fitToView	: true,
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		: 0,
		afterClose	: function(){ 			
			selectedAllMailboxes(); 			 
			if(!DropifiToolTips.hasError){
				//mixpanel - track success update of mailbox         
                UserEventTrack.click({'event':DropifiToolTips.event}); 
			}
		}
	});
}

function openFancy(id){ 
	$(id).fancybox({
		maxWidth	: 590, 
		minHeight	: 500,
		fitToView	: true,
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		padding		: 0,
		afterClose	: function(){ 			
			selectedAllMailboxes(); 			 
			if(!DropifiToolTips.hasError){
				//mixpanel - track success update of mailbox         
                UserEventTrack.click({'event':DropifiToolTips.event}); 
			}
		}
	}).click();
}
 
function activatedMailbox(email,activated){ 
	 
	  $.ajax({    		  
	      type: "POST", 
	      url: url.activated, 
	      data: {'email':email,'activated': activated},
	      success: function(data){ 	     	    	  
	    	 // alert("Update: "+ data.status + ": " + data.activated);    	    	  
	    	  if(data.status ==200){     	    		  
	    		  //update the message mailbox key 
	    		 // $('#mailbox_key').val(data.email); 	    	    		  
	    	  }     	    	  
	      },	      
	      error:function(data){
	    		//alert(data.error); 
	      }
	      
	  });
	  //mixpanel - track user clicking on activate mailbox 
	  UserEventTrack.click({'event':'ACTIVATE MAILBOX CLICK'});  
}

function confirm(message, callback) {
	$('#confirm').modal({
		closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
		position: ["20%",],
		overlayId: 'confirm-overlay',
		containerId: 'confirm-container', 
		onShow: function (dialog) {
			var modal = this;  
			$('.message', dialog.data[0]).append(message);

			// if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}
				// close the dialog
				modal.close(); // or $.modal.close();
			});
		}
	});
}

function selectedAllMailboxes(){
	
	$.getJSON(url.populate, function(data) {
		  var service = data.service;
		  UserVolumeService.init(service);
		  
		  $('#number_of_mailboxes').text('Mailboxes ('+data.size+")");
		  
		  var items = [];
		  var mailbox="";
		  $.each(data.mailboxes, function(key, val) {
			  var monitored = '';//(val.monitored==true)?'<span style="float:right">Managed</span>':'';
			   mailbox += 
			   '<div class="data_view ui-state-default" style="background:inherit" title="Reorder mailbox by dragging it up or down">'+
			   		'<div class="data_activated">'+ 
						'<p> <input type="hidden" id="activatede'+val.id+'"  value ="'+val.activated+'" />'+
	        				'<input class="checkbox" type="checkbox" name="'+val.email+'" value="'+val.email+'" id="e'+val.id+'" checked = "checked" />'+
					 	'</p>'+ 
					'</div>'+
						'<div class="data_view_content" id="e'+val.id+'">'+ 
             			 '<p class="admin_element">'+ val.username +'<span>'+val.email+'</span>'+monitored+' </p>'+
						 '<p class="admin_element_option">'+
							 '<span class="element_option">created:</span> <span class="element_value">'+ val.created +'</span>'+
							 '<span class="element_option">updated:</span> <span class="element_value">'+ val.updated +'</span>'+  
						 '</p>'+
						 '<p class="admin_element_option">'+
							'<span class="action_options">'+
						 	'<a href="'+url.show+'?email=' + val.email + '" class="fancy_window fancybox.iframe edit_mailbox" id="edit_'+val.id+'">edit</a>'+
						 			'<a href="#" class="confirmDelete" id="'+val.email+'"> delete</a></span>'+
						 	'<input type="hidden" id = "drage'+val.id+'" name="draggable_mailbox" value="'+val.email+'" class="draggable_mailbox" ref_activated="'+val.activated+'"/>'+
						 '</p>'+
            		 '</div>'+
             '</div>';
                  	 
		  });
 
		  $('#right_work_area').html(mailbox); 

		    $('.checkbox').after(function() {
		    	var checkId = $(this).attr("id");
		    	var activated = new String($('#activated'+checkId).val());
		    	var toggle_view=$('div#'+checkId);	 
		    	
		        if (activated == "true") {	
		        	toggle_view.removeClass("ticked");	
		           return "<a href='#' class='toggle checked' ref='" + checkId + "'></a>";
		        } else {	
		        	toggle_view.addClass("ticked");	
		        		
		           return "<a href='#' class='toggle' ref='" + checkId + "'></a>";
		        } 
		    	
		    });
		    
		    $('.toggle').click(function(e) {
		    	 
		        var checkboxID = $(this).attr("ref");
		        var checkbox = $('input#' + checkboxID);
				var toggle_view=$('div#'+checkboxID);							
				var me = new String($('#activated'+checkboxID).val());
				
				//checkbox.is(":checked")
		        if (me=="true") {		        	
		        	//checkbox.attr("value", "false");
		            checkbox.attr("checked",false);
		            $('#activated'+checkboxID).val('false');          
		           toggle_view.addClass("ticked");		            
		        } else {		        	
		        	checkbox.attr("checked", true);
		        	$('#activated'+checkboxID).val('true');		             
					toggle_view.removeClass("ticked");						
		        }
		        
		        //update the changes to activated
		        var email =$('#'+checkboxID).val(); 	
		        var activated =  $('#activated'+checkboxID).val(); 
		        $("#drag"+checkboxID).attr("ref_activated",activated); 
		        //checkbox.is(":checked"); 
		        
		        activatedMailbox(email,activated);

		        $(this).toggleClass("checked");
 
		        e.preventDefault();

		    });

		    $('#showCheckboxes').click(function(e) {
		        $('.checkbox').toggle();         
		        e.preventDefault();
		    });
		    
		    $('.confirmDelete').click(function(e) {
		    	 
				e.preventDefault(); 
				var email = $(this).attr("id");		    	
				// example of calling the confirm function
				// you must use a callback function to perform the "yes" action
				confirm("Are you sure you want to delete the  mailbox ("+email+")?", function () {
					
					//delete the mailbox 
					
			    	$.ajax({			    		
			    		type:"POST",
			    		url: url.deleteService,
			    		data:{"email" : email},
			    		success: function(data){				    			 
			    			selectedAllMailboxes();
			    		},
			    		error:function(data){
			    			// 
			    		}
			    	});
					
				}); 
				
				//mixpanel - track user clicking on delete mailbox 
			    UserEventTrack.click({'event':'DELETE MAILBOX CLICK'}); 
			});
		    
		    $(".edit_mailbox").click(function(){
		    	//mixpanel - track user clicking on edit mailbox 
		        UserEventTrack.click({'event':'EDIT MAILBOX CLICK'});  
		    });
		    
		    $( "#right_work_area" ).sortable();
		    $( "#right_work_area" ).disableSelection();
		    $( "#right_work_area" ).on( "sortupdate", function( event, ui ) {
		    	  var dmailbox = []; 
		    	  $(".draggable_mailbox").each(function(key,value){
		    		  var json = new Object();
		    		  json.email = $(this).val(); 
		    		  json.activated = $(this).attr("ref_activated"); 
		    		  dmailbox.push(JSON.stringify(json)); 
		    	  }); 
		    	  
		    	 $.ajax({ 			    		
		    		type:"POST",
		    		url: url.orderService,
		    		data:{"mailboxes" : dmailbox}, 
		    		success: function(data){
		    			selectedAllMailboxes();
		    		},
		    		error:function(data){
		    			//alert("error: "+data.error);
		    		}
			     });
		    });
		 
		    UserEventTrack.countSetting({'event':'Mailboxes Added','count':data.mailboxes.length});
		    UserEventTrack.countSetting({'event':'Managed-Mailboxes Added','count':data.mmsize});
			
	});
  
}