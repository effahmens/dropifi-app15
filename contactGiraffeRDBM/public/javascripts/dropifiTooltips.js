var DropifiToolTips = {
	init: function(){
		this.hasError ="";
		this.success = "";		
		this.error = "";
		this.event = "";
	},	
	fancyboxClose: function(data){
		this.hasError = data.hasError;
		this.success = data.success;		
		this.error = data.error;
		this.event = data.event;
	},
	clear: function(){
		this.hasError ="";
		this.success = "";		
		this.error = "";
		this.event = "";
	}
};
DropifiToolTips.init();  