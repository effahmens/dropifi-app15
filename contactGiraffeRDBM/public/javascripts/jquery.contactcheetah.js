/*
 * cheetah widget 1.0.1
*/
/*
(function() { 	
	//loadcssfile('http://xkapecom:9000/public/client/stylesheets/contactcheetah.css'); 
	loadcssfile('http://xkapecom:9000/public/client/stylesheets/fancybox/jquery.fancybox.css'); 
	
	//loadjsfile('http://xkapecom:9000/public/client/javascripts/jquery-1.7.2.min.js');
	 })();

 */

function loadjsfile(filename){			 
	var co = document.createElement('script'); co.type = 'text/javascript'; co.async = true;
	    co.src = ('https:' == document.location.protocol ? 'https://www' : 'http://www') + '.'+filename;	     
	document.getElementsByTagName("head")[0].appendChild(co);
}

function loadcssfile(filename){			 
  var fileref=document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", filename);
  document.getElementsByTagName("head")[0].appendChild(fileref);
}

(function($){
	loadcssfile('http://xkapecom:9000/public/client/stylesheets/fancybox/jquery.fancybox.css'); 

	loadjsfile('http://xkapecom:9000/public/client/javascripts/jquery.fancybox.js');  
	loadjsfile('http://xkapecom:9000/public/client/javascripts/jquery.fancybox.pack.js');  

	$(document).ready(function(){
	 	$('body').append("<div id='contactable'> <div id='contactable_inner'> </div> <div id='contactForm'></div> </div>");
		
	 	$('#contactable #contactable_inner').css({
	 		'background-image':'url("http://xkapecom:9000/public/images/feedback.png")',
	 		'color':'#FFFFFF',
	 		'background-color':'#333333',
	 		'cursor':'pointer',
	 		'height':'102px',
	 		'left':'0',
	 		'margin-left':'-5px', 
	 		'*margin-left':'-5px', 
	 		'overflow':'hidden',
	 		'*position':'fixed',
	 		'position':'absolute',
	 		'text-indent':'-100000px',
	 		'top':'250px',
	 		'*margin-top':'10px',
	 		'width':'44px',
	 		'z-index':'100000',
	 		'float':'right'
	 	});
	 	
	 	$('#contactable #contactForm').css({
	 		'background-color':'navy',
	 		'border':'2px solid #FFFFFF',
	 		'color':'#FFFFFF',
	 		'height':'450px',
	 		'left':'0',
	 		'margin-left':'-400px',
	 		'*margin-left':'-434px',
	 		'margin-top':'-160px',
	 		'overflow':'hidden',
	 		'padding-left':'30px',
	 		'position':'fixed',
	 		'top':'250px',
	 		'width':'360px',
	 		'*width':'394px',
	 		'z-index':'99',
	 		'float':'right'
	 	});

	 	
	//define the name of the widget
	
	$.fn.contactcheetah = function(options){
		
		//obtain the APIKEY of the company
		var apikey = options.apikey;		 
		var buttonid = options.buttonid;
		var this_id_prefix = '#'+buttonid+' ';
		    	
		//query the server for the html code of the widget using the APIKEY
		$('div#contactForm').html("<iframe id='formdata'  src='http://xkapecom:9000/clientwidgets/load?apikey="+apikey+"'></iframe>");						
		$('#formdata').css({'width':'360px','height':'450px','border-style': 'none','overflow':'hidden'});

		return this.each(function(){ 					
			
			//show / hide function
			$(this_id_prefix+'div#contactable_inner').toggle(
					
				function() {	
					
					$(this_id_prefix+'#overlay').css({display: 'block'}); 
					$(this).animate({"marginLeft": "-=5px"}, "fast"); 
					$(this_id_prefix+'#contactForm').animate({"marginLeft": "-=0px"}, "fast");
					$(this).animate({"marginLeft": "+=387px"}, "slow"); 
					$(this_id_prefix+'#contactForm').animate({"marginLeft": "+=390px"}, "slow"); 	
					
				}, 
			
				function() {
					$(this_id_prefix+'#contactForm').animate({"marginLeft": "-=390px"}, "slow");
					$(this).animate({"marginLeft": "-=387px"}, "slow").animate({"marginLeft": "+=5px"}, "fast"); 
					$(this_id_prefix+'#overlay').css({display: 'none'});
				}
			);	
					 					
	 		//----------------------------------------------	
		});					
		
		function pausecomp(ms){
			ms += new Date().getTime();
			while (new Date() < ms){}
		}
		
		
//end of function
	}
	});	
})(jQuery);
