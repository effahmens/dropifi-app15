/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(f,h,$){var a='placeholder' in h.createElement('input'),d='placeholder' in h.createElement('textarea'),i=$.fn,c=$.valHooks,k,j;if(a&&d){j=i.placeholder=function(){return this};j.input=j.textarea=true}else{j=i.placeholder=function(){var l=this;l.filter((a?'textarea':':input')+'[placeholder]').not('.placeholder').bind({'focus.placeholder':b,'blur.placeholder':e}).data('placeholder-enabled',true).trigger('blur.placeholder');return l};j.input=a;j.textarea=d;k={get:function(m){var l=$(m);return l.data('placeholder-enabled')&&l.hasClass('placeholder')?'':m.value},set:function(m,n){var l=$(m);if(!l.data('placeholder-enabled')){return m.value=n}if(n==''){m.value=n;if(m!=h.activeElement){e.call(m)}}else{if(l.hasClass('placeholder')){b.call(m,true,n)||(m.value=n)}else{m.value=n}}return l}};a||(c.input=k);d||(c.textarea=k);$(function(){$(h).delegate('form','submit.placeholder',function(){var l=$('.placeholder',this).each(b);setTimeout(function(){l.each(e)},10)})});$(f).bind('beforeunload.placeholder',function(){$('.placeholder').each(function(){this.value=''})})}function g(m){var l={},n=/^jQuery\d+$/;$.each(m.attributes,function(p,o){if(o.specified&&!n.test(o.name)){l[o.name]=o.value}});return l}function b(m,n){var l=this,o=$(l);if(l.value==o.attr('placeholder')&&o.hasClass('placeholder')){if(o.data('placeholder-password')){o=o.hide().next().show().attr('id',o.removeAttr('id').data('placeholder-id'));if(m===true){return o[0].value=n}o.focus()}else{l.value='';o.removeClass('placeholder');l==h.activeElement&&l.select()}}}function e(){var q,l=this,p=$(l),m=p,o=this.id;if(l.value==''){if(l.type=='password'){if(!p.data('placeholder-textinput')){try{q=p.clone().attr({type:'text'})}catch(n){q=$('<input>').attr($.extend(g(this),{type:'text'}))}q.removeAttr('name').data({'placeholder-password':true,'placeholder-id':o}).bind('focus.placeholder',b);p.data({'placeholder-textinput':q,'placeholder-id':o}).before(q)}p=p.removeAttr('id').hide().prev().attr('id',o).show()}p.addClass('placeholder');p[0].value=p.attr('placeholder')}else{p.removeClass('placeholder')}}}(this,document,jQuery));

jQuery(document).ready(function(){ 	   
	//convert serializeArray to json object
	$.fn.serializeObject = function(){
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};

	$.fn.center = function () {
	    this.css("position", "absolute");
	    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
	    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
	    return this;
	} 
	 
	$.fn.tclick = function (onclick) {
	    this.bind("touchstart", function (e) { onclick.call(this, e); e.stopPropagation(); e.preventDefault(); });
	    this.bind("click", function (e) { onclick.call(this, e); });   //substitute mousedown event for exact same result as touchstart         
	    return this;
	};
		 
	
	$("#request_invite_code").click(function(){
        var this_id_prefix = '#dropifi_widget_v1_contactable' + " ";     
        if(dropifi_widget_is_close_2012 !=undefined && dropifi_widget_is_close_2012 == true){       
            $(this_id_prefix+'div#cheetahWidget2012_contactable_inner').trigger('click');  
        }
    });
	
	jQuery('input, textarea').placeholder(); 
	
	// Initially, hide them all
    hideAllMessages_Notify();
});   

function showProgress(title) {
	if(title)
		showProgressTitle(title);
	else
		showProgressTitle("");
}

function writeToIframe(iframeId,content, contentWidth,contentEditable){
	var myIframe = document.getElementById(iframeId); 
	return iframeContent(myIframe,contentWidth,contentEditable);	 
}

function iframeContent(myIframe,contentWidth,contentEditable){
	var iframeDoc = myIframe.contentWindow.document;
	iframeDoc.open();
	iframeDoc.write('<base target="_blank" />');
	iframeDoc.write(content);  			
	var frameheight = iframeDoc.body.scrollHeight;			
	myIframe.height =  (frameheight+40)  +  'px';	
	iframeDoc.body.scrollWidth = contentWidth + "px"; 
	iframeDoc.body.contentEditable = contentEditable;	
	iframeDoc.designMode = "On";
	iframeDoc.close(); 	
	return frameheight;
}

function writeToIframeContent(iframeName,content){
	var myIframe = document.getElementsByName(iframeName); 
	var iframeDoc = myIframe[0].contentWindow.document; 
	iframeDoc.open();
	iframeDoc.write(content);
	iframeDoc.close(); 	
}

function replaceAll(txt, replace, with_this) {
	return txt.replace(new RegExp(replace, 'g'),with_this);
}

function showProgressTitle(title) {
	hideProgress();
    $('body').append('<div id="dropifi_progress"><div id="dropifi_progress_content"><p><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/ajax-loader-circular.gif" alt="Loading" /></p>'+
    		'<p style="padding-top:15px;font-size:14px;">'+title+' </p></div></div>');
    $('#dropifi_progress').center(); 
}

function hideProgress() {
    $('#dropifi_progress').remove();
}

function IsEmail(email) {
	  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
}

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br>'; 
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function br2nl(str){
	return str.replace(/<br>/g, "\r");
}

function checkEmailDomain(emailTagId, errorTagId, errorTagClass){
	
	var $hint = $(errorTagId);
	var	$email = $(emailTagId);
	var $error = $(errorTagClass);
	var mailchecked = false;
	$email.mailcheck({ 	
	   suggested: function(element, suggestion) {
	
	       var suggestion = "<div class='msg_info'> Oops!, Did you mean <div class='suggestion' style='color:brown;cursor:pointer'>"  	
	                        + suggestion.address +"@"+ suggestion.domain + 
	                         "</div> ?</div>"; 	                              
			
	       $error.html(suggestion);
	       mailchecked = true; 
	   }	
	 }); 
	
	$hint.on('click', '.suggestion', function() {
	     // On click, fill in the field with the suggestion and remove the hint
		 var nemail = $(".suggestion").text();
		 $email.val(nemail); 	     
		 $(errorTagClass).html("");  	     
	});
	
	return mailchecked; 
}

function checkEmailDomainV2(emailTagId, errorTagId, errorTagClass){
	
	var $hint = $(errorTagId);
	var	$email = $(emailTagId);
	var $error = $(errorTagId);
	var mailchecked = false; 
	$email.mailcheck({ 
	
	   suggested: function(element, suggestion) {
	
	       var suggestion = "<div class='jserror'>Oops!, Did you mean <span class='suggestion' style='color:yellow;cursor:pointer' title='click to add'>"  	
	                        + suggestion.address +"@"+ suggestion.domain + 
	                         "</span>?</div>"; 	                              
			
	       $error.html(suggestion); 
	       mailchecked = true; 
	   }
	
	 });
	
	$hint.on('click', '.suggestion', function() { 
	     // On click, fill in the field with the suggestion and remove the hint
		 var nemail = $(".suggestion").text();
		 $(emailTagId).val(nemail); 	     
		 //$error.html("");     
	});
	
	return mailchecked; 
}

function checkEmailDomainV3(emailTagId, errorTagId, errorTagClass){
	
	var $hint = $(errorTagId);
	var	$email = $(emailTagId);
	var $error = $(errorTagId);
	var mailchecked = false; 
	$email.mailcheck({ 
	
	   suggested: function(element, suggestion) {
	
	       var suggestion = "<div class='"+errorTagClass+"'>Oops!, Did you mean <span class='suggestion' style='color:yellow;cursor:pointer' title='click to add'>"  	
	                        + suggestion.address +"@"+ suggestion.domain + 
	                         "</span>?</div>"; 	                              
			
	       $error.html(suggestion); 
	       mailchecked = true; 
	   }
	
	 });
	
	$hint.on('click', '.suggestion', function() { 
	     // On click, fill in the field with the suggestion and remove the hint
		 var nemail = $(".suggestion").text();
		 $(emailTagId).val(nemail); 	     
		 //$error.html("");     
	});
	
	return mailchecked; 
}

function auto_hide_widget(){
	var this_id_prefix = '#dropifi_widget_v1_contactable' + " "; 	 
	if(dropifi_widget_is_close_2012 !=undefined && dropifi_widget_is_close_2012 == true){		
		$(this_id_prefix+'div#cheetahWidget2012_contactable_inner').trigger('click');  
	}	 
} 


//message notifications
var myMessages = ['info_notify','warning_notify','error_notify','success_notify'];

function hideAllMessages_Notify()
{
	var messagesHeights = new Array(); // this array will store height for each 
	 for (i=0; i<myMessages.length; i++){
	      messagesHeights[i] = $('.' + myMessages[i]).outerHeight() | $('.' + myMessages[i]).height() | 0; //fill array
	      $('.' + myMessages[i]).css('top', - messagesHeights[i]);   //move element outside viewport
	 } 
}

function showMessage_Notify(option) {
	 $('.'+ option.type).html("<div>"+option.msg+"</div>");
     hideAllMessages_Notify();  
 
     $('.'+option.type).animate({top:option.topValue}, 500); 
  
	  if(option.autohide==true){
		  setTimeout(function(){ 
			  $(".message_notify").animate({top: -$(this).outerHeight()}, 500); 
		  },option.timeout)
	  }
	  
	  if(option.clickClose){
		  $('.message_notify').click(function(){    
		     $(this).animate({top: -$(this).outerHeight()}, 500); 
		  }); 
	  }
}

function getCheckedValues(chkboxName){
	  var checkboxes = document.getElementsByName(chkboxName);
	  var checkboxesChecked = [];
	 
	  for (var i=0; i<checkboxes.length; i++) {
	     
	     if (checkboxes[i].checked) {
	        checkboxesChecked.push(checkboxes[i].value);
	     }
	  }
	  //Return the array if it is non-empty, or null
	  return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

function myIP() {
	var IP= "";
	var xmlhttp = ajaxRequest("GET","http://api.hostip.info/get_html.php",false);
	
	if(xmlhttp){
	    xmlhttp.send();	
	    hostipInfo = xmlhttp.responseText.split("\n"); 	    
	    
	    for (i=0; hostipInfo.length >= i; i++) {
	    	if(hostipInfo[i]){	    	 
		        ipAddress = hostipInfo[i].split(":");
		        if ( ipAddress[0] == "IP" ) {
		        	  IP = ipAddress[1]; 
		        }	        
	    	}
	    }
	}  
    return IP;
}

function ajaxRequest(method, url, accessControl){
	var xmlhttp =null;
	if(window.XDomainRequest) {xmlhttp = new XDomainRequest();}
	else if (window.XMLHttpRequest) {xmlhttp = new XMLHttpRequest(); }   
    else { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
	
	if(accessControl!=undefined && accessControl!=null)
		xmlhttp.open(method,url,accessControl); 
	else
		xmlhttp.open(method,url);
	
	return xmlhttp;
}

function showReview(requestUrl, requestUpdateUrl,top){ 
	
	if(Dropifi.page != 'Integration' && Dropifi.hasWidget){
		
		$(".message_notify").css({
			 'z-index':'1'
		});
		
		setTimeout(function(){	
			$.getJSON(requestUrl, function(data) { 
				if(data.status==200 && data.showReview){ 
					var reviewMessage ='<div class="container_12 review_container"><span ="review_content">'; 
					var reviewContent = "Hi "+data.loginName+", we would love it if you can take a minute to give us a review on "+data.pluginType +" (Click Here)";
					
					if(data.pluginType=="Wordpress"){
						reviewMessage +="<a class='review_widget' href='http://wordpress.org/support/view/plugin-reviews/dropifi-contact-widget' target='_blank'>"+reviewContent+"</a>";
					}else if(data.pluginType=="Shopify"){
						reviewMessage +="<a class='review_widget' href='http://apps.shopify.com/dropifi-contact-widget' target='_blank'>"+reviewContent+"</a>";
					}else if(data.pluginType=="Magento"){
						reviewMessage +="<a class='review_widget' href='http://www.magentocommerce.com/magento-connect/catalog/product/view/id/16246/s/dropifi-contact-widget-5749/' target='_blank'>"+reviewContent+"</a>"; 
					}else if(data.pluginType=="Wix"){
						reviewMessage +="<a class='review_widget' href='http://www.wix.com/app-market/dropifi-contact-widget/reviews' target='_blank'>"+reviewContent+"</a>"; 
					}else if(data.pluginType=="PrestaShop"){
						reviewMessage +="<a class='review_widget' href='#' target='_blank'>"+reviewContent+"</a>"; 
					}
					
					reviewMessage+='</span> <span class="review_close">x</span></div>';
					if(data.pluginType != "PrestaShop" && data.pluginType != "Tictail"){	
						showMessage_Notify({'type':"info_notify",'msg':reviewMessage,'topValue':top,'autohide':false,'timeout':'10000'}); 
					}
					
					$(".review_widget").click(function(){
						updateReview(true, requestUpdateUrl);
						hideAllMessages_Notify();
						//mixpanel - track user viewing/loading of keyword page 
					    UserEventTrack.click({'event': 'REVIEW_'+data.pluginType+' CLICK'});
					}); 
					
					$(".review_close").click(function(){
						hideAllMessages_Notify();
						updateReview(false,requestUpdateUrl);
						UserEventTrack.click({'event': 'REVIEW_'+data.pluginType+' CANCEL'});
					});
					
					//mixpanel - track user viewing/loading of keyword page 
				    UserEventTrack.click({'event': 'REVIEW_'+data.pluginType+' SHOWN'});  
				}
			});
		}, 1000);
	}
}

function updateReview(isReviewed,requestUrl){	 
	$.ajax({
		url: requestUrl,
		type:'POST',
		data:{'isReviewed':isReviewed},
		success: function(data){ 
		},
		error: function(data){
		}
	});
}

function writeToIframe(iframeId,content, contentWidth, offset,designMode){ 
	var  myIframe = document.getElementById(iframeId);			
	var iframeDoc = myIframe.contentWindow.document;
	if(designMode)
		iframeDoc.designMode=designMode;
	iframeDoc.open();
	iframeDoc.write(('<base target="_blank" />'+content));  
	
	var frameheight = iframeDoc.body.scrollHeight || iframeDoc.body.offsetHeight; 	//		
	myIframe.height =  (frameheight+offset)  +  'px'; 
	myIframe.frameBorder = 0;  
	if(BrowserDetect.browser=="Firefox") 
		iframeDoc.close();

}

function listOfCountries(){ 
	 var countries= '<option value="Afghanistan">Afghanistan</option>'+
	 '<option value="Åland Islands">Åland Islands</option>'+
	 '<option value="Albania">Albania</option>'+
	 '<option value="Algeria">Algeria</option>'+
	 '<option value="American Samoa">American Samoa</option>'+
	 '<option value="Andorra">Andorra</option>'+
	 '<option value="Angola">Angola</option>'+
	 '<option value="Anguilla">Anguilla</option>'+
	 '<option value="Antarctica">Antarctica</option>'+
	 '<option value="Antigua and Barbuda">Antigua and Barbuda</option>'+
	 '<option value="Argentina">Argentina</option>'+
	 '<option value="Armenia">Armenia</option>'+
	 '<option value="Aruba">Aruba</option>'+
	 '<option value="Australia">Australia</option>'+
	 '<option value="Austria">Austria</option>'+
	 '<option value="Azerbaijan">Azerbaijan</option>'+
	 '<option value="Bahamas">Bahamas</option>'+
	 '<option value="Bahrain">Bahrain</option>'+
	 '<option value="Bangladesh">Bangladesh</option>'+
	 '<option value="Barbados">Barbados</option>'+
	 '<option value="Belarus">Belarus</option>'+
	 '<option value="Belgium">Belgium</option>'+
	 '<option value="Belize">Belize</option>'+
	 '<option value="Benin">Benin</option>'+
	 '<option value="Bermuda">Bermuda</option>'+
	 '<option value="Bhutan">Bhutan</option>'+
	 '<option value="Bolivia">Bolivia</option>'+
	 '<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>'+
	 '<option value="Botswana">Botswana</option>'+
	 '<option value="Bouvet Island">Bouvet Island</option>'+
	 '<option value="Brazil">Brazil</option>'+
	 '<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>'+
	 '<option value="Brunei Darussalam">Brunei Darussalam</option>'+
	 '<option value="Bulgaria">Bulgaria</option>'+
	 '<option value="Burkina Faso">Burkina Faso</option>'+
	 '<option value="Burundi">Burundi</option>'+
	 '<option value="Cambodia">Cambodia</option>'+
	 '<option value="Cameroon">Cameroon</option>'+
	 '<option value="Canada">Canada</option>'+
	 '<option value="Cape Verde">Cape Verde</option>'+
	 '<option value="Cayman Islands">Cayman Islands</option>'+
	 '<option value="Central African Republic">Central African Republic</option>'+
	 '<option value="Chad">Chad</option>'+
	 '<option value="Chile">Chile</option>'+
	 '<option value="China">China</option>'+
	 '<option value="Christmas Island">Christmas Island</option>'+
	 '<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>'+
	 '<option value="Colombia">Colombia</option>'+
	 '<option value="Comoros">Comoros</option>'+
	 '<option value="Congo">Congo</option>'+
	 '<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>'+
	 '<option value="Cook Islands">Cook Islands</option>'+
	 '<option value="Costa Rica">Costa Rica</option>'+
	 '<option value="Cote D\'ivoire">Cote D\'ivoire</option>'+
	 '<option value="Croatia">Croatia</option>'+
	 '<option value="Cuba">Cuba</option>'+
	 '<option value="Cyprus">Cyprus</option>'+
	 '<option value="Czech Republic">Czech Republic</option>'+
	 '<option value="Denmark">Denmark</option>'+
	 '<option value="Djibouti">Djibouti</option>'+
	 '<option value="Dominica">Dominica</option>'+
	 '<option value="Dominican Republic">Dominican Republic</option>'+
	 '<option value="Ecuador">Ecuador</option>'+
	 '<option value="Egypt">Egypt</option>'+
	 '<option value="El Salvador">El Salvador</option>'+
	 '<option value="Equatorial Guinea">Equatorial Guinea</option>'+
	 '<option value="Eritrea">Eritrea</option>'+
	 '<option value="Estonia">Estonia</option>'+
	 '<option value="Ethiopia">Ethiopia</option>'+
	 '<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>'+
	 '<option value="Faroe Islands">Faroe Islands</option>'+
	 '<option value="Fiji">Fiji</option>'+
	 '<option value="Finland">Finland</option>'+
	 '<option value="France">France</option>'+
	 '<option value="French Guiana">French Guiana</option>'+
	 '<option value="French Polynesia">French Polynesia</option>'+
	 '<option value="French Southern Territories">French Southern Territories</option>'+
	 '<option value="Gabon">Gabon</option>'+
	 '<option value="Gambia">Gambia</option>'+
	 '<option value="Georgia">Georgia</option>'+
	 '<option value="Germany">Germany</option>'+
	 '<option value="Ghana">Ghana</option>'+
	 '<option value="Gibraltar">Gibraltar</option>'+
	 '<option value="Greece">Greece</option>'+
	 '<option value="Greenland">Greenland</option>'+
	 '<option value="Grenada">Grenada</option>'+
	 '<option value="Guadeloupe">Guadeloupe</option>'+
	 '<option value="Guam">Guam</option>'+
	 '<option value="Guatemala">Guatemala</option>'+
	 '<option value="Guernsey">Guernsey</option>'+
	 '<option value="Guinea">Guinea</option>'+
	 '<option value="Guinea-bissau">Guinea-bissau</option>'+
	 '<option value="Guyana">Guyana</option>'+
	 '<option value="Haiti">Haiti</option>'+
	 '<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>'+
	 '<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>'+
	 '<option value="Honduras">Honduras</option>'+
	 '<option value="Hong Kong">Hong Kong</option>'+
	 '<option value="Hungary">Hungary</option>'+
	 '<option value="Iceland">Iceland</option>'+
	 '<option value="India">India</option>'+
	 '<option value="Indonesia">Indonesia</option>'+
	 '<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>'+
	 '<option value="Iraq">Iraq</option>'+
	 '<option value="Ireland">Ireland</option>'+
	 '<option value="Isle of Man">Isle of Man</option>'+
	 '<option value="Israel">Israel</option>'+
	 '<option value="Italy">Italy</option>'+
	 '<option value="Jamaica">Jamaica</option>'+
	 '<option value="Japan">Japan</option>'+
	 '<option value="Jersey">Jersey</option>'+
	 '<option value="Jordan">Jordan</option>'+
	 '<option value="Kazakhstan">Kazakhstan</option>'+
	 '<option value="Kenya">Kenya</option>'+
	 '<option value="Kiribati">Kiribati</option>'+
	 '<option value="Korea, Democratic People\'s Republic of">Korea, Democratic People\'s Republic of</option>'+
	 '<option value="Korea, Republic of">Korea, Republic of</option>'+
	 '<option value="Kuwait">Kuwait</option>'+
	 '<option value="Kyrgyzstan">Kyrgyzstan</option>'+
	 '<option value="Lao People\'s Democratic Republic">Lao People\'s Democratic Republic</option>'+
	 '<option value="Latvia">Latvia</option>'+
	 '<option value="Lebanon">Lebanon</option>'+
	 '<option value="Lesotho">Lesotho</option>'+
	 '<option value="Liberia">Liberia</option>'+
	 '<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>'+
	 '<option value="Liechtenstein">Liechtenstein</option>'+
	 '<option value="Lithuania">Lithuania</option>'+
	 '<option value="Luxembourg">Luxembourg</option>'+
	 '<option value="Macao">Macao</option>'+
	 '<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>'+
	 '<option value="Madagascar">Madagascar</option>'+
	 '<option value="Malawi">Malawi</option>'+
	 '<option value="Malaysia">Malaysia</option>'+
	 '<option value="Maldives">Maldives</option>'+
	 '<option value="Mali">Mali</option>'+
	 '<option value="Malta">Malta</option>'+
	 '<option value="Marshall Islands">Marshall Islands</option>'+
	 '<option value="Martinique">Martinique</option>'+
	 '<option value="Mauritania">Mauritania</option>'+
	 '<option value="Mauritius">Mauritius</option>'+
	 '<option value="Mayotte">Mayotte</option>'+
	 '<option value="Mexico">Mexico</option>'+
	 '<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>'+
	 '<option value="Moldova, Republic of">Moldova, Republic of</option>'+
	 '<option value="Monaco">Monaco</option>'+
	 '<option value="Mongolia">Mongolia</option>'+
	 '<option value="Montenegro">Montenegro</option>'+
	 '<option value="Montserrat">Montserrat</option>'+
	 '<option value="Morocco">Morocco</option>'+
	 '<option value="Mozambique">Mozambique</option>'+
	 '<option value="Myanmar">Myanmar</option>'+
	 '<option value="Namibia">Namibia</option>'+
	 '<option value="Nauru">Nauru</option>'+
	 '<option value="Nepal">Nepal</option>'+
	 '<option value="Netherlands">Netherlands</option>'+
	 '<option value="Netherlands Antilles">Netherlands Antilles</option>'+
	 '<option value="New Caledonia">New Caledonia</option>'+
	 '<option value="New Zealand">New Zealand</option>'+
	 '<option value="Nicaragua">Nicaragua</option>'+
	 '<option value="Niger">Niger</option>'+
	 '<option value="Nigeria">Nigeria</option>'+
	 '<option value="Niue">Niue</option>'+
	 '<option value="Norfolk Island">Norfolk Island</option>'+
	 '<option value="Northern Mariana Islands">Northern Mariana Islands</option>'+
	 '<option value="Norway">Norway</option>'+
	 '<option value="Oman">Oman</option>'+
	 '<option value="Pakistan">Pakistan</option>'+
	 '<option value="Palau">Palau</option>'+
	 '<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>'+
	 '<option value="Panama">Panama</option>'+
	 '<option value="Papua New Guinea">Papua New Guinea</option>'+
	 '<option value="Paraguay">Paraguay</option>'+
	 '<option value="Peru">Peru</option>'+
	 '<option value="Philippines">Philippines</option>'+
	 '<option value="Pitcairn">Pitcairn</option>'+
	 '<option value="Poland">Poland</option>'+
	 '<option value="Portugal">Portugal</option>'+
	 '<option value="Puerto Rico">Puerto Rico</option>'+
	 '<option value="Qatar">Qatar</option>'+
	 '<option value="Reunion">Reunion</option>'+
	 '<option value="Romania">Romania</option>'+
	 '<option value="Russian Federation">Russian Federation</option>'+
	 '<option value="Rwanda">Rwanda</option>'+
	 '<option value="Saint Helena">Saint Helena</option>'+
	 '<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>'+
	 '<option value="Saint Lucia">Saint Lucia</option>'+
	 '<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>'+
	 '<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>'+
	 '<option value="Samoa">Samoa</option>'+
	 '<option value="San Marino">San Marino</option>'+
	 '<option value="Sao Tome and Principe">Sao Tome and Principe</option>'+
	 '<option value="Saudi Arabia">Saudi Arabia</option>'+
	 '<option value="Senegal">Senegal</option>'+
	 '<option value="Serbia">Serbia</option>'+
	 '<option value="Seychelles">Seychelles</option>'+
	 '<option value="Sierra Leone">Sierra Leone</option>'+
	 '<option value="Singapore">Singapore</option>'+
	 '<option value="Slovakia">Slovakia</option>'+
	 '<option value="Slovenia">Slovenia</option>'+
	 '<option value="Solomon Islands">Solomon Islands</option>'+
	 '<option value="Somalia">Somalia</option>'+
	 '<option value="South Africa">South Africa</option>'+
	 '<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>'+
	 '<option value="Spain">Spain</option>'+
	 '<option value="Sri Lanka">Sri Lanka</option>'+
	 '<option value="Sudan">Sudan</option>'+
	 '<option value="Suriname">Suriname</option>'+
	 '<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>'+
	 '<option value="Swaziland">Swaziland</option>'+
	 '<option value="Sweden">Sweden</option>'+
	 '<option value="Switzerland">Switzerland</option>'+
	 '<option value="Syrian Arab Republic">Syrian Arab Republic</option>'+
	 '<option value="Taiwan, Province of China">Taiwan, Province of China</option>'+
	 '<option value="Tajikistan">Tajikistan</option>'+
	 '<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>'+
	 '<option value="Thailand">Thailand</option>'+
	 '<option value="Timor-leste">Timor-leste</option>'+
	 '<option value="Togo">Togo</option>'+
	 '<option value="Tokelau">Tokelau</option>'+
	 '<option value="Tonga">Tonga</option>'+
	 '<option value="Trinidad and Tobago">Trinidad and Tobago</option>'+
	 '<option value="Tunisia">Tunisia</option>'+
	 '<option value="Turkey">Turkey</option>'+
	 '<option value="Turkmenistan">Turkmenistan</option>'+
	 '<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>'+
	 '<option value="Tuvalu">Tuvalu</option>'+
	 '<option value="Uganda">Uganda</option>'+
	 '<option value="Ukraine">Ukraine</option>'+
	 '<option value="United Arab Emirates">United Arab Emirates</option>'+
	 '<option value="United Kingdom">United Kingdom</option>'+
	 '<option value="United States">United States</option>'+
	 '<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>'+
	 '<option value="Uruguay">Uruguay</option>'+
	 '<option value="Uzbekistan">Uzbekistan</option>'+
	 '<option value="Vanuatu">Vanuatu</option>'+
	 '<option value="Venezuela">Venezuela</option>'+
	 '<option value="Viet Nam">Viet Nam</option>'+
	 '<option value="Virgin Islands, British">Virgin Islands, British</option>'+
	 '<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>'+
	 '<option value="Wallis and Futuna">Wallis and Futuna</option>'+
	 '<option value="Western Sahara">Western Sahara</option>'+
	 '<option value="Yemen">Yemen</option>'+
	 '<option value="Zambia">Zambia</option>'+
	 '<option value="Zimbabwe">Zimbabwe</option>';
	 return countries; 
}

var Dropifi = {
	init: function(data){
		this.hasWidget = data.hasWidget;
		this.userName = data.userName;
	},
	stripe:function(){
		return "pk_live_qhwqQZElN9AobEVuZkyBhwqB";
	},
	get_time_zone_offset: function() {
	    var current_date = new Date();
	    var gmt_offset = (current_date.getTimezoneOffset() / 60) * -1;
	    return gmt_offset;
	}
}

function confirm(message, callback) {
	$('#confirm').modal({
		closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
		position: ["20%",],
		overlayId: 'confirm-overlay',
		containerId: 'confirm-container', 
		onShow: function (dialog) {
			var modal = this;

			$('.message', dialog.data[0]).append(message);

			//if the user clicks "yes"
			$('.yes', dialog.data[0]).click(function () {
				// call the callback
				if ($.isFunction(callback)) {
					callback.apply();
				}
				// close the dialog
				modal.close(); // or sss$.modal.close();
			});
		}
	});
}

function determineSeriesColour(seriesName){
	var color = "";
	if(seriesName == "Positive" || seriesName == "postive")
	{
		color = "#08F714";
	}
	else if(seriesName == "Negative" || seriesName == "negative")
	{
		color = "#F70313";
	}
	else if(seriesName == "Neutral" || seriesName == "neutral")
	{
		color = "#0808F7";
	}
	return color;
}


function subscribeToNewsletter(e){ 
	var userdata = $('#wf-form-newsletter').serializeObject();
   $.ajax({
   	type:"post",
   	url:"@{Security.subscribeToNewsletter()}",
   	data:{"email":userdata.email,"location":userdata.location,"browser":userdata.browser,"name":userdata.name},
   	success:function(data){
   		//alert("Newsletter subscribed: "+ data.status+" "+ data.error); 
   	},
   	error:function(data){   
   	}
   });
  
   return false;
}
 
