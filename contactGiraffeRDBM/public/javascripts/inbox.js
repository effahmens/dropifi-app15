var viewUrl,contactViewUrl,contactFilterUrl, dafault_custsomer_image,viewReplyUrl,countMessageUrl;  
var open_message_id= undefined;
var open_contact_id = undefined;
var inbox_mail_panel = "ref_inbox";
var is_inbox_panel_click = false;
var selected_contact_list = "letter_All"; 
var inbox_selected_mailbox ="all_mailboxes";
var selected_customer_mode ="everyone_selected";
var dropifi_total_messages = undefined;
var dropifi_next_inbox = 0;
var dropifi_previous_inbox = 0;
var next_end = false;
var previous_end = true;
var is_dropifi_inbox = undefined;
var pullOfMessages = new Array(); 
var pullOfContacts = new Array(); 
var newpendingresolved_0304 = undefined;
var mailbox_filters_0309_inbox = undefined; 
var reply_current_messageId = undefined;
var reply_current_msgId = undefined; 
var reply_current_id = undefined; 
var is_save_sent=false;
var is_replied_click =false; 
var compose_type =undefined; 
var dropifi_timezone = get_time_zone_offset();

function setViewUrl(url){
	viewUrl = url;
}

function setContactViewUrl(url){
	contactViewUrl = url; 
}

function setContactFilterUrl(url){
	contactFilterUrl = url; 
}

function defaultCustomerImage(image){
	dafault_custsomer_image = image;
}

function resizeMailHeight(){
	var ssHeight= $("#selected_mail").height();
	var mailHeight =(ssHeight -$("#selected_mail_head").height())-20; 
	var incHeight =(ssHeight- $("#incoming_mail_head").height())-30;
    $("#selected_mail_scroll").height(mailHeight);
     
    $("#incoming_scroll").height(incHeight);
    var inboxPanelHeight =incHeight-70;
 
    $("#inbox_main_panel_scroll").height(inboxPanelHeight);
  
    var selectedWidth = $("#inbox_main").width()- ($("#inbox_menu").width()+$("#incoming_mail").width())-15;
    var sMw = $("#selected_mail").width();
    $("#selected_mail").width(selectedWidth);
   
    var image_width = $("#full_contact_image").width();
    
    var contact_bio_width = 0.32* (selectedWidth-image_width);var contact_data_width = 0.65* (selectedWidth-image_width);
    $("#full_contact_bio").width(contact_bio_width); $("#full_contact_data").width(contact_data_width);
    
}

$(document).ready(function(){
	
	$('#customers_message').tipsy({gravity:'n'}); 
	$('#not_cust_message').tipsy({gravity:'n'});
	$('#markas_icon').tipsy({gravity:'s'});
	$('#select_icon').tipsy({gravity:'s'});
	$('#subject_icon').tipsy({gravity:'s'});
	$('#delete_icon').tipsy({gravity:'s'});
	$('#contact_email').tipsy({gravity:'s'});
	$('#contact_location').tipsy({gravity:'s'}); 
	$('#contact_company').tipsy({gravity:'s'});
	$('#contact_title').tipsy({gravity:'s'});
	$('#contact_bio').tipsy({gravity:'n'});
	
	$('#intense_message').tipsy({gravity:'s'});	
    
	resizeMailHeight();
	$(window).resize(function(){	 
		resizeMailHeight();
	});
	 
});

function get_time_zone_offset(){
    var current_date = new Date();
    var gmt_offset = (current_date.getTimezoneOffset() / 60) * -1;
    return gmt_offset;
} 

//--------------------------------------------------------------------------------------------------------
//
function fetchAllMessages(start,end, requestUrl,scrollToId){
	hideProgress();
	showProgressTitle("LOADING");  	
	console.log("Start Time: "+ new Date());
	$.ajax({
	     type: "GET", 
	     url:requestUrl,    
	     data: {'start':start, 'end':end,'timezone':dropifi_timezone}, 
	     success: function(data){
	    	 console.log("Data Return Start Time: "+ new Date());
	    	 if(data.status ==200){  
	    		 dropifi_next_inbox = 0;
		    	 inbox_selected_mailbox ="all_mailboxes";
		    	 selected_customer_mode ="everyone_selected"; 
		    	 populateMessages(data);   	   
		    	 console.log("Data Return End Time: "+ new Date());
		    	 
		    	 if(data.messages.length>0){
		    		 if(scrollToId){
		    			 //viewMessage("mail_id_"+data.messages[0].id);
		    		 }else{
		    			 viewMessage("mail_id_"+data.messages[0].id);
		    		 }
		    	 }else{
		 	   		 $("#selected_mail_scroll").html("");	
					clearContactDetail(); 
		    	 }
		    	 
	    	 }else{
	    		 
	 	   		 $("#selected_mail_scroll").html("");	
	 	   		clearContactDetail();  
	    	 }
	    	 
	    	hideProgress();
	    	console.log("End Time: "+ new Date());
	    },
	   	error:function(data){
	   		hideProgress(); 	   		 
	   		$("#selected_mail_scroll").html("");	
	   		clearContactDetail();  
	   		
	   	}
	  });	
} 

function setNumOfSubject(subjectCount){ 
	if(subjectCount!=undefined){ 
		
		$("#widget_subject").html("");
		$.each(subjectCount, function(key,val){
			var subId = "subject_"+ val.id;
			$("#widget_subject").append( 			
			'<li class="inbox_mail_panel_subject" id="ref_'+val.subject+'" ref_panel="subjects"> '+
			'<a href="#subject_'+val.subject+'" class="panel" id ="'+subId+'" >'+val.subTitle+'<span class="mail_count" ' + 
			' id="count_sub_'+val.subject+'">' + convertCount(val.numOfMsg) + '</span></a></li>');
			
			$("#"+subId).click(function(){
				$('a.panel').removeClass('selected');
		        $(this).addClass('selected');
			});
		}); 
 
		//set the click action for the panels
	    $(".inbox_mail_panel_subject").click(function() {
   			inbox_mail_panel = this.id; 
   			is_inbox_panel_click = true; 
   			
   			if(this.id != "ref_sent" && this.id != "ref_drafts")
   				filterByMailbox(0,50);	   			
	   	}); 
	}	
}

function setNumofSentMsg(sentMessages){
	if(sentMessages!=undefined){
		var sent = sentMessages.sent!=undefined?sentMessages.sent:0;
		var draft = sentMessages.draft !=undefined?sentMessages.draft:0;
		//$("#count_sent").html("("+sent+")");
		$("#count_draft").html(convertCount(draft)); 
	}
}

//================================================next previous messages =====================================

function displayNextInboxMessagesOld(start, end){ 
	if(pullOfMessages !=undefined && start!=undefined && end != undefined && pullOfMessages.length>0){
		
		var plen =  pullOfMessages.length;
		var start = dropifi_next_inbox;
		var end = (dropifi_next_inbox+50)>=plen?plen:(dropifi_next_inbox+50);
		
		if(next_end==false){ 			
			$("#incoming_scroll").html("");	
			for(var i = start, len = end; i < len; i++){ 
				
				var val = pullOfMessages[i];
				
				if(val.status == "New"){
					$("#incoming_scroll").append(newMessage(val,i));
				}else {
					$("#incoming_scroll").append(openMessage(val,i));
				}
				//$("#incoming_scroll").append(pullOfMessages[i]); 
			} 
			
			dropifi_previous_inbox = dropifi_next_inbox;
			dropifi_next_inbox = end; 
			next_end = (end ==plen)?true:false; 
			previous_end = (start<=0)?true:false;			 
			$('#dropifi_showing_records').html( (start+1) + " - "+ end  + " of "+ plen );
			
			$("#incoming_scroll").scrollTo(0, 0, {queue:true} ); 
		}
	}else{			
		clearContactDetail();
	}
}

function displayNextInboxMessages(start, end){ 
	if(pullOfMessages !=undefined && start!=undefined && end != undefined && pullOfMessages.length>0){
		
		var plen =  pullOfMessages.length;
		var start = dropifi_next_inbox;
		var end = (dropifi_next_inbox+50)>=plen?plen:(dropifi_next_inbox+50);
		
		if(next_end==false){			
			
			var msg = "";  
			for(var i = start, len = end; i < len; i++){				
				 msg += pullOfMessages[i].content; 
			} 			
			$("#incoming_scroll").html(msg);
			
			dropifi_previous_inbox = dropifi_next_inbox;
			dropifi_next_inbox = end; 
			next_end = (end ==plen)?true:false; 
			previous_end = (start<=0)?true:false;			 
			$('#dropifi_showing_records').html( (start+1) + " - "+ end  + " of "+ plen );
			
			$("#incoming_scroll").scrollTo(0, 0, {queue:true} ); 
		}
	}else{			
		clearContactDetail(); 
	}
}

function displayPreviousInboxMessagesOld(start, end){
	
	if(pullOfMessages !=undefined && start!=undefined && end != undefined && pullOfMessages.length>0){
		
		var plen =  pullOfMessages.length;	 
		var start = (dropifi_previous_inbox-50)<=0?0:(dropifi_previous_inbox-50);
		var end = dropifi_previous_inbox; 
		
		if(previous_end==false){
			 
			$("#incoming_scroll").html(""); 		
			for(var i = start , len = end ; i < len; i++){ 
				var val = pullOfMessages[i];
				if(val.status == "New"){
					$("#incoming_scroll").append(newMessage(val, i));
				}else{
					$("#incoming_scroll").append(openMessage(val, i));
				}			 
				//$("#incoming_scroll").append(pullOfMessages[i]);
			}
			dropifi_next_inbox = dropifi_previous_inbox;
			dropifi_previous_inbox = start; 
			previous_end = (start == 0)?true:false;  
			next_end = false;			 
			$('#dropifi_showing_records').html( (start+1) + " - "+ end  + " of "+ plen );
		}
	}else{					 
		clearContactDetail();
	}
	
}

function displayPreviousInboxMessages(start, end){
	
	if(pullOfMessages !=undefined && start!=undefined && end != undefined && pullOfMessages.length>0){
		
		var plen =  pullOfMessages.length;	 
		var start = (dropifi_previous_inbox-50)<=0?0:(dropifi_previous_inbox-50);
		var end = dropifi_previous_inbox; 
		
		if(previous_end == false){
			 
			$("#incoming_scroll").html(""); 
			var msg = "";  
			for(var i = start , len = end ; i < len; i++){ 
				msg += pullOfMessages[i].content; 
			}
			$("#incoming_scroll").html(msg);
			dropifi_next_inbox = dropifi_previous_inbox;
			dropifi_previous_inbox = start; 
			previous_end = (start == 0)?true:false;  
			next_end = false;			 
			$('#dropifi_showing_records').html( (start+1) + " - "+ end  + " of "+ plen );
			
			$("#incoming_scroll").scrollTo(0, 0, {queue:true} ); 
		}
	}else{					 
		clearContactDetail();
	}
	
}

function displayNextContactMessages(start, end){

	if(pullOfContacts !=undefined && start!=undefined && end != undefined && pullOfContacts.length>0){
		
		var plen =  pullOfContacts.length;	 
		var start = dropifi_next_inbox;
		var end = (dropifi_next_inbox+50)>=plen?plen:(dropifi_next_inbox+50); 
		
		if(next_end==false){  			
			$("#incoming_scroll").html("");
			var lastContact = undefined;
			for(var i = start, len = end; i < len; i++){
				var val = pullOfContacts[i];
				$("#incoming_scroll").append(userContact(val));	
				lastContact = val;
			}
			
			if(lastContact!=undefined){
				var idvalue = "#contact_id_"+lastContact.id;				 
				$(idvalue).css({
					'margin-bottom':'30px'
				});
			}	 
			
			dropifi_previous_inbox = dropifi_next_inbox;
			dropifi_next_inbox = end; 
			next_end = (end ==plen)?true:false; 
			previous_end = (start<=0)?true:false;			 
			$('#dropifi_showing_records').html( (start+1) + " - "+ end  + " of "+ plen );
			
			$("#incoming_scroll").scrollTo(0, 0, {queue:true} ); 
		}
	}else{
		clearContactPageDetail(); 
	}
}

function displayPreviousContactMessages(start, end) {
	
	if(pullOfContacts !=undefined && start!=undefined && end != undefined && pullOfContacts.length>0){
		
		var plen =  pullOfContacts.length;	 
		var start = (dropifi_previous_inbox-50)<=0?0:(dropifi_previous_inbox-50); ;
		var end = dropifi_previous_inbox;
		
		if(previous_end==false){
			 
			$("#incoming_scroll").html(""); 
			var lastContact = undefined;
			for(var i = start , len = end ; i < len; i++){
				var val = pullOfContacts[i];			 
				$("#incoming_scroll").append(userContact(val));		
				lastContact = val;
			}
			
			if(lastContact!=undefined){
				var idvalue = "#contact_id_"+lastContact.id;				 
				$(idvalue).css({
					'margin-bottom':'30px'
				}); 
			}	
	
			dropifi_next_inbox = dropifi_previous_inbox; 
			dropifi_previous_inbox = start; 
			previous_end = (start == 0)?true:false;  
			next_end = false;			 
			$('#dropifi_showing_records').html( (start+1) + " - "+ end  + " of "+ plen ); 
			
			$("#incoming_scroll").scrollTo(0, 0, {queue:true} ); 
		}
	}else{					 
		clearContactPageDetail();  
	}
}


//===================================populateMessage ===================================================================

function populateMessages(data){
	
	if(data.status == 200){ 
		
		if(data.count){
			//setCounters(data.count);
		}

		if(data.sources){
			fillMailboxes(data.sources);
		}
		
		if(data.subjectCount){
			//setNumOfSubject(data.subjectCount);
		}
		
		pullOfMessages = {};
		pullOfMessages = data.messages;
		 		
		displayNextInboxMessages(0, 50);  
		$("#delete_icon").css({'display':'inherit'});
		
		setMessageCounters(); 		
		
	}
}

function populateMessagesOld(data){
	
	if(data.status == 200){ 
		
		if(data.count != undefined){
			setCounters(data.count);
		}

		if(data.sources != undefined){
			fillMailboxes(data.sources);
		}
		
		pullOfMessages = {};
		pullOfMessages = data.messages;
		 		
		displayNextInboxMessagesOld(0, 50);  
	}
}
function setCounters(count){
	if(count != undefined){
		$("#count_inbox").text(convertCount(count.inbox));
		$("#count_sent").text(convertCount(count.sent));
		$("#count_draft").text(convertCount(count.draft));
		$("#count_negative").text(convertCount(count.negative));
		$("#count_neutral").text(convertCount(count.neutral));
		$("#count_positive").text(convertCount(count.positive));
		$("#count_new").text(convertCount(count.newMsg));
		$("#count_open").text(convertCount(count.openMsg));
		$("#count_pending").text(convertCount(count.pendingMsg));
		$("#count_resolved").text(convertCount(count.resolvedMsg));
	}
}

function convertCount(count){
	return count>0?"("+count+")":"";
}

function fillMailboxes(sources){ 
	if(sources!=undefined){
		$("#filter_comboBox").html("");
		$("#filter_comboBox").append("<option id='all_mailboxes' value ='All Mailboxes' mailbox_length ='"+sources.length+"' class ='inbox_selected_mailbox' >All Mailboxes</option> ");
		inbox_selected_mailbox ='All Mailboxes';
		
		$.each(sources,function(key,val){	
			$("#filter_comboBox").append("<option id = '"+val+"' value ='"+val+"' class ='inbox_selected_mailbox' >"+val+"</option> ");
		});
		
		$("#filter_comboBox").change(function(){			 
			inbox_selected_mailbox = $(this).val(); 
			is_inbox_panel_click = false;
			dropifi_next_inbox = 1;
			filterByMailbox(0,200);
		});
	}
}

//----------------------------------------------------------------------------------------------------

function fillContactMailboxes(sources){ 
	if(sources!=undefined){
		$("#filter_comboBox").html("");
		$("#filter_comboBox").append("<option id='all_mailboxes' value ='All Mailboxes' mailbox_length ='"+sources.length+"' class ='inbox_selected_mailbox' >All Mailboxes</option> ");
		inbox_selected_mailbox ='All Mailboxes';
		
		$.each(sources,function(key,val){	
			$("#filter_comboBox").append("<option id = '"+val+"' value ='"+val+"' class ='inbox_selected_mailbox' >"+val+"</option> ");
		});
		
		$("#filter_comboBox").change(function(){		 
			inbox_selected_mailbox = $(this).val(); 
			is_inbox_panel_click = false; 
			$('a.panel').removeClass('selected'); 
			fetchAllContactsLike(1,200,5,contactFilterUrl);
		});
	}
}

function newMessage(message,index){
	return "<div class='mail_item' id ='mail_id_"+message.id+"_color' ref_index='"+index+"'>"+userMessage(message,index);
}

function openMessage(message,index){
	var msg ="";
	 
	if(message.type == "InboxMessage")
		msg = userMessage(message,index);
	else if(message.type == "SentMessage")
		msg = sentMessage(message,index);
	
	return "<div class='mail_item mail_read' id ='mail_id_"+message.id+"_color' ref_index='"+index+"'>"+msg;
} 

function userMessage(message,index){
	
	var subject=message.subject!=undefined?message.subject:"--", fullname=message.fullname!=undefined?message.fullname:"--";
	 		
	var msg =
	'<input type="checkbox" id = "chk_mail_id_' + message.id + '"/>'+ 
	'<div class="mail_content"  ref_index = "'+index+'"  id ="mail_id_' + message.id + '" ref_id="' + message.msgId + '" ref_email="'+ message.email +
	'" ref_status="'+ message.status +'" ref_sentiment="'+message.sentiment+'" ref_source="'+ message.source +'" onclick ="javascript:viewMessage(this.id)">'+    
	'<p class="hide_overflow_text"><span class="mail_fullname">'+fullname+'</span> <span class="mail_time" style="float:right;">'+ message.created +'</span></p>'+
	'<p class="dominant_subject hide_overflow_text">'+subject+'</p>'+
	'<p class="hide_overflow_text" style="font-weight:normal;"><span>'+sentimentEmotion(message)+'<span><span class="" style="float:right">'+message.responses+'</span></p>'+  
	'</div></div>';
	
	return msg; 
}

function userContact(contact){
	var image = "https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/default_customer.png";
	if(contact.image != undefined && jQuery.trim(contact.image) != "")	
		image = contact.image; 
 
	
	var cont = 
	//'<div class= "contact_item" id = "contact_id_'+ contact.id +'_color" ref_selected_customer ="is_selectedcont_id_'+contact.id+'"><input type="checkbox" id ="chk_contact_id_' +contact.id+'" />'+
	'<div class= "contact_item" id = "contact_id_'+ contact.id +'_color" ref_selected_customer ="is_selectedcont_id_'+contact.id+'">'+
	'<div class= "mail_content" id="contact_id_'+ contact.id +'" ref_email="'+contact.email+'" onclick="javascript:viewContactMessages(this.id,5)">'+    
	'<div class= "small_img" style ="width:45px;height:45px"><img style ="width:45px;height:45px" src="'+image+'" alt="contact image"/> </div>'+
	'<div class= "contact_display">'+
	'<p class= "contact_fullname hide_overflow_text">'+ (contact.fullName!=undefined?contact.fullName:"..") +'</p>'+  
	'<p class= "contact_email hide_overflow_text">'+   (contact.email?contact.email:"..") +'</p>';
	
	if(contact.isCustomer == true){
		cont += '<p><span class="is_customer" id = "is_selectedcont_id_'+contact.id+ '" ref_status ="'+contact.isCustomer+'">Customer</span>'; 

	}else if(contact.isCustomer == false){
		cont += '<p><span class="is_prospect" id = "is_selectedcont_id_'+contact.id+'" ref_status ="'+contact.isCustomer+'">Prospect</span>';
	}
	cont += '</div></div></div>'; 	
	return cont;
}

function setContactCounters(count){
	if(count !=undefined){	 
		$("#count_negative").html(convertCount(count.negative));
		$("#count_positive").html(convertCount(count.positive)); 
		$("#count_neutral").html(convertCount(count.neutral));
	}
}

function populateContacts(data){ 
	if(data.status == 200){ 
		
		pullOfContacts = new Array()
		pullOfContacts = data.contacts;
		setContactCounters(data.count); 
		
		if(pullOfContacts!=undefined)
			$("#total_number_of_contacts").html(pullOfContacts.length);
 
		displayNextContactMessages(0, 50); 	
	}
}

function substrValue(text,limit){
	if(text != undefined && jQuery.trim(text) !=""){ 		 
		var len = text.length; 
		if(len>limit){
			return text.substring(0, limit);
		}
		return text;
	}
	return "---"; 
}

function substrValueDot(text,limit, dots){  
	if(text != undefined && jQuery.trim(text) !=""){ 	
		text = jQuery.trim(text);
		var len = text.length; 
		if(len>limit){
			return text.substring(0, limit) + dots; 
		}
		return text; 
	} 
	return "---";
}

function msgSentiment(sentiment){
	if(sentiment == "Positive"){
		return "https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/features/high_pos.png";  
	}else if(sentiment == "Negative"){  
		return "https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/features/high_neg.png";  
	}else if (sentiment == "Neutral"){
		return "https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/features/neutral.png";   
	}else{
		
	}
}

function clearContactDetail(){
	$('#dropifi_showing_records').html(0 + " - "+ 0  + " of "+ 0 ); 
	$("#selected_mail_scroll").html(" ");	
	$("#incoming_scroll").html('<div ref_index="0" id="mail_id_23_color" class="mail_item mail_read mail_showing"><p id="no_mail_found">No Message Found</p></div>'); 
	$("#delete_icon").css({'display':'none'});
	clearContactProfile();
}

function clearContactPageDetail(){ 
	$('#dropifi_showing_records').html(0 + " - "+ 0  + " of "+ 0 ); 
	$("#analytic_posi").html("0 %");
	$("#analytic_nega").html("0 %");
	$("#analytic_neut").html("0%");
	$("#analytics_new_msg").html("0");
	$("#analytics_resolved_msg").html("0");
	Show_No_Data("#analytics_emotions"); 
	$("#incoming_scroll").html('<div ref_index="0" id="mail_id_23_color" class="mail_item mail_read mail_showing"><p id="no_mail_found">No Contact Found</p></div>'); 
	clearContactProfile()
}

function clearContactProfile(){

	$("#contact_image").attr("src","https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/default_customer.png");
	$("#contact_bio").text(" "); 		    		
	$("#contact_name").text(" ");
	$("#contact_gender").text("--");
	$("#contact_age_range").text("(--)");
	
	$("#contact_email").text("--");
	$("#contact_email").attr("original-title","Email");
	
	$("#contact_phone").text("--");
	$("#contact_phone").attr("original-title","Phone");
	
	$("#contact_location").text("--");
	$("#contact_location").attr("original-title","Location"); 
	
	$("#contact_company").text("--");
	$("#contact_company").attr("original-title","Company");
	
	$("#contact_title").text("--");
	$("#contact_title").attr("original-title","Title"); 	
	$("#contact_social").html(""); 	
	
	setAsCustomer(undefined);
	$("#_is_cust").val(undefined);
}

function contactProfile(data, divId, typeOfView){ 
	var InCont ="";
	if(typeOfView == "inbox"){		
 		setAsShowing(data.mstatus ,divId); 
 		InCont ="INBOX-";
 	}else if(typeOfView == "contact"){
 		setAsShowingContact(divId); 
 		InCont ="CONTACT-";
 	}else{
 		 //
 	}
	
	if(data.contact == undefined || data.contact == null){
		clearContactProfile();
		return false;
	}
	
	var contact = data.contact;
	
 	if(contact.image!=null && jQuery.trim(contact.image)!=""){ 
		$("#contact_image").attr("src", contact.image);
	}else{
		$("#contact_image").attr("src","https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/default_customer.png");
	}
	
 	 	
	var len =30;		    		
	
	$("#contact_name").text(contact.fullName == undefined ? " ":contact.fullName);
	var bio=" ";
	if(contact.bio != undefined){
		$("#contact_bio").attr("original-title", contact.bio);
		bio = contact.bio; 
	}else{
		$("#contact_bio").attr("original-title", " "); 
	} 
	
	$("#contact_bio").text(bio); 		    		
	
	$("#contact_gender").text(contact.gender== undefined?"--":contact.gender);
	$("#contact_age_range").text("("+ (contact.age== undefined||contact.age=="0"?"--":contact.age)+")");
	
	$("#contact_email").text(contact.email);
	$("#contact_email").attr("original-title",contact.email == undefined?"Email":contact.email);
	
	if(contact.phone!=undefined && contact.phone!=""){
		$("#btn_contact_phone").css({'display':'inherit'});
		$("#contact_phone").text(contact.phone);
		$("#contact_phone").attr("original-title",contact.email == undefined?"Phone":contact.phone);
	}else{ 
		$("#btn_contact_phone").css({'display':'none'});
	}
	
	$("#contact_location").text(contact.location == undefined?"--": contact.location);
	$("#contact_location").attr("original-title",contact.location == undefined?"Location":contact.location);
	
	$("#contact_company").text(contact.company == undefined?"--":contact.company);
	$("#contact_company").attr("original-title",contact.company == undefined?"Company":contact.company);
	
	$("#contact_title").text(contact.title == undefined?"--":contact.title);
	$("#contact_title").attr("original-title",contact.title == undefined?"Title":contact.title);  
	
	$("#contact_social").html(""); 
	
	setAsCustomer(contact); 
	if(contact.socials !=undefined && contact.socials!= null){
		$.each(contact.socials, function(key,val){ 
			
			var photo = val.name+".png";
			var social ="<strong>"+ val.name+"</strong><br>followers : "+val.followers+"<br>following : "+val.following;
			var social_id="social_"+val.name;
			var socialName = val.name.toUpperCase(); 
			$("#contact_social").append('<a id="'+social_id+'" target="_blank" href="'+val.url+'" class="contact_social_link" ref_event="'+socialName+'" title ="'+social+'"><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/social/'+photo+'" /></a>');
			$("#"+social_id).tipsy({gravity:'n',html:true}); 
		});  
		
		$(".contact_social_link").click(function(){
			//mixpanel - track user 
	        UserEventTrack.click({'event':InCont+$(this).attr("ref_event")+'-LINK CLICK'});
		});
	}
}
 
function setAsShowing(status ,messageId){
	try{	
		if(open_message_id != undefined){ 
			$(open_message_id).removeClass("mail_showing");			
		}
		
		open_message_id = messageId+"_color"; 
		
		if(status !="New"){
			$(open_message_id).addClass("mail_read");  
		} 		
		$(open_message_id).addClass("mail_showing");
		
	}catch(err){
		 
	}
}

function setAsShowingContact(contactId){
	if(open_contact_id != undefined){ 
		$(open_contact_id).removeClass("contact_showing");			 
	}	
	
	open_contact_id = contactId +"_color"; 	 
	$(open_contact_id).addClass("contact_showing"); 
	
}

function setCustomerShowMode(selected_mode_id){
	try{	
		if(selected_customer_mode != undefined){ 
			$("#"+selected_customer_mode).removeClass("selected_mode");			
		}
		
		selected_customer_mode = selected_mode_id; 
		$("#"+selected_customer_mode).addClass("selected_mode");
		
	}catch(err){
		
	}
}

function setAsCustomer(contact){
	 var checkboxID = "_is_cust";
     var checkbox = $('input#' + checkboxID);
     var toggle_view= $('div#'+checkboxID);	
     
	if(contact != undefined){
		 $("#activated_is_cust").val(contact.isCustomer);
		 $("#_is_cust").val(contact.email);   								
	     //var me = new String($('#activated'+checkboxID).val());
			
		 //checkbox.is(":checked")
	     if (contact.isCustomer == true) {		        	
	         checkbox.attr("checked",true);        
	         //$('#activated'+checkboxID).val(contact.email);          
	         $(".is_customer").addClass("checked"); 
	     } else {		        	
	     	checkbox.attr("checked", false);
	     	//$('#activated'+checkboxID).val(contact.email);		             
	     	$(".is_customer").removeClass("checked");  
	     }  
	}else{
   	 	checkbox.attr("checked", false);
   	 	//$('#activated'+checkboxID).val(contact.email);		             
   	 	$(".is_customer").removeClass("checked"); 
	}  	
	
}

function setPendingResolved(status){
	if(status!= undefined){
		$("#current_message_resolved").removeClass("resolved_message");
		$("#current_message_pending").removeClass("pending_message"); 
	 
		if(status == "Pending"){
			$("#current_message_pending").removeClass("rect");
			$("#current_message_pending").addClass("pending_message");			
		}else if(status =="Resolved"){	
			$("#current_message_resolved").removeClass("rect");
			$("#current_message_pending").removeClass("rect"); 
			$("#current_message_resolved").addClass("resolved_message");
			$("#current_message_pending").addClass("no_pending_message");			
		}
		
		$("#current_message_resolved").attr("ref_current_status",status);
		$("#current_message_pending").attr("ref_current_status",status);
		
	}
}

function sentimentEmotion(message){ 
	var sent_image, sent_class;
	
	switch(message.sentiment){ 
		case "Positive":
			sent_image = "high_pos.png";
			sent_class = "pos_sentiment";
			break;
		case "Negative":
			sent_image = "high_neg.png";
			sent_class = "neg_sentiment";
			break;
		case "Neutral":
			sent_image = "neutral.png";
			sent_class = "neu_sentiment";
			break;
		default:
			sent_image = "";
			sent_class = "no_sentiment";  
			break;
	}
	
	var sent = ((sent_image!=null && sent_image!="") ? '<img id="sentiment_image_message" src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/features/'+sent_image+'" width="17" height="15"/>':"")+ 
			'<span id="sentiment_message" class="'+sent_class+'">'+message.emotion+'</span>';
	return sent;
}

function sentimentEmotionSource(message){
	var sent_image, sent_class;
	
	switch(message.sentiment){
		case "Positive":
			sent_image = "high_pos.png";
			sent_class = "pos_sentiment";
			break;
		case "Negative":
			sent_image = "high_neg.png";
			sent_class = "neg_sentiment";
			break;
		case "Neutral":
			sent_image = "neutral.png";
			sent_class = "neu_sentiment";
			break;
		default:
			sent_image = "";
			sent_class = "no_sentiment";
			break;
	}
	var intense = "";
	if(message.intense !=undefined && jQuery.trim(message.intense) != "") 
		intense = "("+message.intense+")"; 
	
	var senti ='<img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/features/'+ sent_image+
			   '" width="17" height="15"/> <span class="'+sent_class+'">'+message.emotion+'</span>'+
			   //'<span style= "margin-left:40px;"> to: '+message.source +'</span>'+ 
			   ' <span class="mail_time" style="float:right" >'+message.created+'</span>'+
			   '<p><span id="intense_message"  original-title="intense sentence in this message" >'+message.subject+
			   '</span><span style="font-weight:600; margin-left:10px">'+intense+'</span> </p>';  
	
	return senti;
} 

function writeToIframe(iframeId,content, contentWidth, offset){
	var  myIframe = document.getElementById(iframeId);			
	var iframeDoc = myIframe.contentWindow.document;
	//iframeDoc.designMode="on";
	iframeDoc.open();
	iframeDoc.write(('<base target="_blank" />'+content));  
	
	var frameheight = iframeDoc.body.scrollHeight || iframeDoc.body.offsetHeight; 	//		
	myIframe.height =  (frameheight+offset)  +  'px'; 
	myIframe.frameBorder = 0; 
	//iframeDoc.body.scrollWidth = contentWidth+ "px"; 	
	//myIframe.width = contentWidth + "px"; 
	
	if(BrowserDetect.browser=="Firefox") 
		iframeDoc.close();  

}

function writeToReplyIframe(iframeId,content, contentWidth, offset){
	var  myIframe = document.getElementById(iframeId);			
	var iframeDoc = myIframe.contentWindow.document;
	//iframeDoc.designMode="on";
	iframeDoc.open();
	iframeDoc.write(('<base target="_blank" />'+content));  
	
	var frameheight = iframeDoc.body.scrollHeight; 
	myIframe.height =  (frameheight+offset)  +  'px'; 
	myIframe.frameBorder = 0; 
	iframeDoc.body.scrollWidth = contentWidth + "px"; 	
	
	if(BrowserDetect.browser=="Firefox")
		iframeDoc.close(); 
	 
 
} 

function repliedBy(replyer){
	if(replyer != null && replyer != undefined) {
		return '<div id="replyer"> <label>Last Replied By: </label> <strong>'+replyer.userName+'</strong> on <span>'+replyer.updated+'</span></div>';
	}
	return ""; 
}

function setMessage(message,domain,replyer) {

	if(message != undefined){ 		
		var numOfReply = $("#mail_id_"+message.id+"_reply").attr("replies");
		var reText=numOfReply>1?"Replies":"Reply";
		var reVisibility = numOfReply>0?"inline":"none";
		var fileAttachemnt = (message.attachments && message.attachments.length>0)?'<img title='+message.attachments[0].fileName+' src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/contact_attachment.png">':'';
		var msg ='<div id="incoming_msg_content" class="mail_view">'+   
		'<div class="mail_view_menu">'+ 
		'<a class="rect pending_resolved" href="#" title="'+(message.status!="Resolved"?"Mark as resolved":"")+'" id="current_message_resolved" ref_current_status="'+ message.status +'">Resolved</a>'+ 
		'<a class="rect pending_resolved" href="#" title="'+(message.status=="Open"?"Mark as pending":"")+'" id="current_message_pending" ref_current_status="'+ message.status +'">Pending</a>'+ 
		'<div class="message_icons">'+ 
		'<span class="message_attachment">'+fileAttachemnt+'</span>'+
		'<span class="mail_time">'+message.created+'</span>'+  
		'<a id="foward_to" href="/'+domain+'/inbox/compose?type=inbox_forward&msgId='+message.msgId+'&messageId='+message.id+'&email='+message.email+'&subject='+message.subject+'&message='+""+'"  class="reply_btn fancy_window fancybox.iframe styled-button-3" ><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_reply.png" width="15" height="13" /> Forward </a>'+ 
		'<a id="reply_to" href="/'+domain+'/inbox/compose?type=reply&msgId='+message.msgId+'&messageId='+message.id+'&email='+message.email+'&subject='+message.subject+'&message='+""+'"    class="forward_btn fancy_window fancybox.iframe styled-button-3" ><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_forward.png" width="15" height="13" /> Reply </a>'+
		'<a id="view_replies" style="display:'+reVisibility+';" href="#" title="View Replies"class="btn_reply_messages styled-button-3 style-view-reply" ref_msgId="'+message.msgId+'" ref_email = "'+message.email+'" ref_count="'+numOfReply+'"><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_view.png" width="13" height="11" /><span id="view_replies_count"> View '+reText+' ('+numOfReply+')</span></a>'; 
		 
		//'<a href="" title="delete" class="delete_btn"><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_delete.png" width="19" height="17" /></a>'+ 
		msg += '</div>'+ 
		'</div>'+
		'<div id="message_sentiment">'+  
		'<table id="table_design">'+ 
			'<tbody>'+ 
				'<tr class="odd">'+
					'<td class="td_1">Source of Message</td>'+ 
					'<td class="td_2">'+message.source +'</td>'+  
				'</tr>'+
				'<tr class="even">'+ 
					'<td class="td_1">Subject</td>'+ 
					'<td class="td_2">'+message.subject+'</td>'+ 
				'</tr>'+ 
				'<tr class="odd">'+
					'<td class="td_1">Sentiment</td>'+ 
					'<td class="td_2">'+sentimentEmotion(message)+' </td>'+
				'</tr>'+ 
		        '<tr class="even">'+ 
					'<td class="td_1">Intense Sentence</td>'+ 
					'<td class="td_2"><p><span>'+message.intense+ '</span> </p></td>'+ 
				'</tr>';
		if(message.pageUrl){
			msg+='<tr class="odd">'+ 
					'<td class="td_1">Sent From</td>'+ 
					'<td class="td_2"><p><span><a href="'+message.pageUrl+'" target="_blank">'+message.pageUrl+ '</a></span> </p></td>'+ 
				'</tr>';
		}
		msg+='</tbody>'+ 
		'</table>'+
		'</div><div style="padding-bottom:10px;"><span id="btn_collaspse_reply_main" class="btn_collapse_reply" style="margin-left:10px;" title="Hide message"> - - - </span></div>'+ 
		'<iframe id="message_body" style="overflow-y:auto;width: 99%;" border="0" ></iframe>'+
		'<div id="selected_mail_footer">'+
			appendAttachedFiles(message.attachments);
		+'</div>';
		/*
		if(numOfReply>0){
			msg+='<a href="#" title="View Replies"class="btn_reply_messages styled-button-3 style-view-reply" ref_msgId="'+message.msgId+'" ref_email = "'+
			message.email+'"><img src="/../public/images/view_icon.png" width="19" height="17" />View Replies ('+numOfReply+')</a>'; 
		} 
		msg+='<a id="reply_to" href="/'+domain+'/inbox/compose?type=reply&msgId='+message.msgId+'&messageId='+message.id+'&email='+message.email+'&subject='+message.subject+'&message='+""+'" title = "Reply to '+message.email+'"  class="forward_btn fancy_window fancybox.iframe styled-button-3" ><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_forward.png" width="19" height="17" /> Reply </a></div>';
		*/
		
		msg+='</div><div id="view_message_replies" class="mail_view" style="visibility:collapse;"></div>';
		
		$("#selected_mail_scroll").html(msg);
		$('#subject_message').tipsy({gravity:'s'}); 
		$('#intense_message').tipsy({gravity:'s'});   
		
		//$('#reply_to').tooltip(); 
		//$('#foward_to').tooltip();
		//$('#replyer').tipsy({gravity:'sn',html: true,title: function(){return (replyer!=null||replyer !=undefined)?replyer.message:""} });   
		//$('#replyer').tipsy({html: true });  
		 	
		var contentWidth = $("#selected_mail_scroll").width();		  
		writeToIframe("message_body",message.htmlBody, contentWidth,40);  
		
		$("#reply_to").click(function(){
			is_replied_click =true;
			
			//mixpanel - track user 
	        UserEventTrack.click({'event':'INBOX-REPLY-MSG CLICK'});
		});
		
		$("#foward_to").click(function(){
			is_replied_click = false;
			//mixpanel - track user 
	        UserEventTrack.click({'event':'INBOX-FOWARD-MSG CLICK'});
		});
		 
		//$("#selected_mail_scroll").css({'scroll':1});
		setPendingResolved(message.status); 
		$("#btn_collaspse_reply_main").toggle(
			function(){
				$(this).attr({"title":"Show message"}); 
				$("#message_body").hide();
			},	
			function(){
				$(this).attr({"title":"Hide message"});  
				$("#message_body").show();
			}
		);
		 
		$("#current_message_resolved").click(function(){
			var status = $(this).attr("ref_current_status");	
			if(status !="Resolved" && status !="New"){							 
				updatePendingResolved(message.id,status,"Resolved",message.source,message.msgId);
			}
		}); 
		
		$("#current_message_pending").click(function(){	
			var status = $(this).attr("ref_current_status");
			if(status != "Resolved" && status !="New" && status !="Pending"){								 
				updatePendingResolved(message.id, status, "Pending", message.source, message.msgId);  
			}
		});
		
		$(".btn_reply_messages").click(function(){
			var msgId = $(this).attr("ref_msgId");
			var email = $(this).attr("ref_email"); 
			var scrolH =$("#incoming_msg_content").height();
			showProgressTitle("LOADING");
			$.ajax({
			     type: "GET", 
			     url: viewReplyUrl,   
			     data: {'msgId':msgId, 'email':email,'timezone':dropifi_timezone},
			     success: function(data){
			    	
			    	 if(data.status == 200){
			    		 $("#view_message_replies").css({'visibility':'visible'});  
			    		if(data.messages!=undefined&&data.messages.length>0){ 
				    		 var reContentWidth = $("#view_message_replies").width();	
				    			$("#view_message_replies").html(""); 
				    		$.each(data.messages, function(key,val){
				    			 var contentId = "reply_message_body_"+val.id;
				    			 var replyMsg= "<div id='mail_view_reply_"+val.id+"' class='mail_view_reply'> " +
				    			  		"<div class='reply_header'><span class= 'reply_date_person'>Replied by </span><strong>"+val.userName+"</strong><span class= 'reply_date_person'> on "+val.created+
				    			  		"</span><p class='reply_date_person'>To : "+val.to+"</p><p class='reply_date_person'>Subject &nbsp;&nbsp;: "+val.subject+"</p></div>" +
				    			  		"<div><span id='btn_collaspse_reply_"+contentId+"'  class='btn_collapse_reply' title='Hide message'> - - - </span></div>"+
				    			 		//"<div id='"+contentId+"' class='reply_body_content'>"+val.message+"</div> </div>"+
				    			        "<iframe class='reply_body_content' id='"+contentId+"' width='98%' height='0px' style='overflow-y: hidden;' ></iframe></div>"+
				    			 		"<div class='mail_divider_horizontal'> </div>";
				    			 		$("#view_message_replies").append(replyMsg); 
				    			 		writeToReplyIframe(contentId,val.message, reContentWidth, 10); 
				    			 						    			 		
				    			 		$("#btn_collaspse_reply_"+contentId).toggle(
			    			 				function(){
			    			 					$(this).attr({"title":"Show message"}); 
			    			 					$("#"+contentId).hide(); 
			    			 				},	
			    			 				function(){
			    			 					$(this).attr({"title":"Hide message"});  
			    			 					$("#"+contentId).show();
			    			 				}
				    			 		);				    			 		
				    				    			 
				    		}); 			    		
				    		
				    		$("#selected_mail_scroll").scrollTo(scrolH, 0, {queue:true} );
			    		}else{
			    			$("#view_message_replies").html("<div class=''>No Reply On This Message</div>"); 
			    			$("#selected_mail_scroll").scrollTo(scrolH, 0, {queue:true} );
			    		}
			    	 }else{
			    		 $("#view_message_replies").html("<div class=''>No Reply On This Message</div>");
			    			$("#selected_mail_scroll").scrollTo(scrolH, 0, {queue:true} );
			    	 }
			    	hideProgress();
			    }, 
			   	error:function(data){			   		 
			   		hideProgress(); 
			   	}
			  });
			
			
			//mixpanel - track user 
	        UserEventTrack.click({'event':'INBOX-VIEW-REPLY CLICK'});
		});
		
				
	}
}

function appendAttachedFiles(attachments){
	var attContent ="";
	if(attachments && attachments.length>0){
		attContent+="<div>";
		var attachLogo ='<img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/contact_attachment.png">';
		var titleMsg="Click to view or Drag to your desktop to save";
		//attContent +="<p><span class='fileName'>"+attachments.length+" attachments"; 
		$.each(attachments, function(key, val){
			var downloadUrl ="/inbox/message/attachment/download/?id="+val.id+"&msgId="+val.msgId+"&sender="+val.email+"&fileName="+val.fileName+"&attachmentId="+val.attachmentId; 
			var viewUrl ="/inbox/message/attachment/view/?id="+val.id+"&msgId="+val.msgId+"&sender="+val.email+"&fileName="+val.fileName+"&attachmentId="+val.attachmentId; 
			attContent+="<p>"+attachLogo+" <span class='fileName' >"+val.fileName+"</span></p><p style='padding-left:15px;'> <a target='_blank' href='"+viewUrl+"'  title='"+titleMsg+"' class='downloadAttachment'>View</a> <a href='"+downloadUrl+"' title='"+titleMsg+"' class='downloadAttachment'>Download</a></p>"
		}); 
		attContent+="</div>";
	}
	return attContent;
}

function setSentMessage(message,domain,replyer){  
	 
	if(message != undefined){ 		
	 
		var msg ='<div class="mail_view">'+   
		'<div class="mail_view_menu">'+
		'<a class="" href="#" id="current_message_resolved" ref_current_status="'+ message.status +'">&nbsp; </a>'+ 
		'<a class="" href="#" id="current_message_pending" ref_current_status="'+ message.status +'">&nbsp;</a>'+  
		'<div class="message_icons">'+ 
		'<span class="mail_time">'+message.created+'</span>'+  
		'<a href="/'+domain+'/inbox/compose?isDraft=true&respId='+message.id+'&isSent='+message.isSent+'&type=sent_forward&msgId='+message.msgId+'&messageId='+message.messageId+'&email='+message.to+'&subject='+message.subject+'&message=""'+
		' class="styled-button-3 forward_btn fancy_window fancybox.iframe" > <img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_reply.png" width="19" height="17"/>Forward</a>'+ 
		
		'<a href="/'+domain+'/inbox/compose?isDraft=true&respId='+message.id+'&isSent='+message.isSent+'&type=reply&msgId='+message.msgId+'&messageId='+message.messageId+'&email='+message.to+'&subject='+message.subject+'&message=""'+ 
		' class="styled-button-3 reply_btn fancy_window fancybox.iframe" ><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_forward.png" width="19" height="17"  />Send</a>'+ 
		//'<a href="" title="delete" class="delete_btn"><img src="https://52237386d618d7516e37-1675ae92bbfacf872a5b5649fb939199.ssl.cf5.rackcdn.com/images/msg_delete.png" width="19" height="17" /></a>'+ 
		'</div>'+ 
		'</div>'+  
		'<div id="message_sentiment">'+  
		'<table id="table_design">'+
			'<tbody>'+ 
				 '<tr class="odd">'+
					'<td class="td_1">From</td>'+ 
					'<td class="td_2">'+message.from +'</td>'+  
				'</tr>'+ 
				'<tr class="even">'+ 
					'<td class="td_1">To</td>'+ 
					'<td class="td_2">'+message.to+'</td>'+ 
				'</tr>'+ 
				'<tr class="odd">'+ 
					'<td class="td_1">CC</td>'+ 
					'<td class="td_2">'+message.cc+' </td>'+
				'</tr>'+ 
		        '<tr class="even">'+ 
					'<td class="td_1">Subject</td>'+ 
					'<td class="td_2"><p><span>'+message.subject+ '</span> </p></td>'+ 
				'</tr>'+
				'</tbody>'+ 
		'</table>'+
		'</div>'+ 
		'<iframe id="message_body" style="overflow-y: auto;width: 98%;" border="0" >	</iframe>'+	
	    //'<div id="replied_message">'+repliedBy(replyer)+'</div>'+		 
		'</div>'; 
		 
		$("#selected_mail_scroll").html(msg);
		$('#subject_message').tipsy({gravity:'s'});
		$('#intense_message').tipsy({gravity:'s'});		
		//$('#replyer').tipsy({gravity:'sn',html: true }); 
		 	
		var contentWidth = $("#selected_mail_scroll").width(); 
		writeToIframe("message_body",message.message, contentWidth,40);
		resizeMailHeight();
		/*
			setPendingResolved(message.status); 
			
			$("#current_message_resolved").click(function(){
				var status = $(this).attr("ref_current_status");	
				if(status !="Resolved" && status !="New"){							 
					updatePendingResolved(message.id,status,"Resolved",message.source,message.msgId);
				}
			});
			
			$("#current_message_pending").click(function(){	
				var status = $(this).attr("ref_current_status");
				if(status != "Resolved" && status !="New" && status !="Pending"){								 
					updatePendingResolved(message.id, status, "Pending", message.source, message.msgId); 
				}
			});	
		*/	
	}
}

function setContactMessages(messages){
	if(messages != undefined) {
		
		var contentWidth = $("#selected_mail_scroll").width();
	 
		$("#selected_mail_scroll").html(" ") 
		$.each(messages, function(key, val){ 	
			var iframeId ="iframe_body_"+val.id;  
			var msgs = 
			'<div class="mail_view contact_msg">'+
			'<div id="message_sentiment">'+sentimentEmotionSource(val)+'</div>'+			
			'<div id="message_body" style="margin:auto"><iframe id="'+iframeId+'" style="width:100%; overflow-y: hidden;" >	</iframe></div>';
			$("#selected_mail_scroll").append(msgs);	
			
			writeToIframe(iframeId,val.htmlBody, contentWidth,40);  
			 
		});  
	}
}

//-----------------------------------------------------AJAX----------------------------------------------------
function fetchAllContacts(start,end,maxi,requestUrl){
	hideProgress(); 
	//showProgressTitle("LOADING"); 
	$.ajax({
	     type: "GET", 
	     url: requestUrl,   
	     data: {'start':start, 'end':end},  
	     success: function(data){
	    	 if(data.status==200){
		    	 populateContacts(data); 		    	
		    	 if(data.contacts.length>0){
		    		 var idvalue = "contact_id_"+data.contacts[0].id;
		    		 viewContactMessages(idvalue ,maxi); 
		    		 UserEventTrack.countSetting({'event':'Total Contacts','count':data.contacts.length});
		    	 }else{	
		    		 
		    		 clearContactPageDetail(); 
		    	 } 
		    	 fillContactMailboxes(data.sources);
	    	 }else{
	    		 
	    		 clearContactPageDetail();   
	    	 }
	    	hideProgress();
	    }, 
	   	error:function(data){
	   	    hideProgress(); 
	   		clearContactPageDetail();  
	   		
	   	}
	  });
	
}

function fetchAllContactsLike(start,end,maxi,requestUrl){ 
		hideProgress(); 
		showProgressTitle("LOADING"); 		 
		var source = inbox_selected_mailbox; 
		var like = $("#"+selected_contact_list).attr("ref_letter");
		var mode = $("#"+selected_customer_mode).attr("ref_customer"); 
		 
		if(like != undefined && mode!=undefined){
			 
		$.ajax({
		     type: "GET", 
		     url: requestUrl,   
		     data: {'like':like,'mode':mode,'source':source,'start':start, 'end':end},   
		     success: function(data){	 
		    	 
		    	 if(data.status ==200){
		    		 dropifi_next_inbox = 0;
		    		 next_end = false;
		    		 previous_end = true;
			    	 populateContacts(data); 
			    	 
			    	 if(data.contacts !=undefined){
				    	 if(data.contacts.length>0){ 
				    		 var idvalue = "contact_id_"+data.contacts[0].id;
				    		viewContactMessages(idvalue ,maxi); 
				    	 }else{	
				    		 $("#incoming_scroll").html(""); 	
				    		 clearContactPageDetail();  
				    	 } 
			    	 } 
		    	 }else{
		    		 $("#incoming_scroll").html(""); 	
		    		 clearContactPageDetail(); 
		    	 }
		    	hideProgress();
		    },
		   	error:function(data){
		   		$("#incoming_scroll").html("");
		   		clearContactPageDetail(); 
		   		hideProgress(); 
		   	}
		});	
	}
}

function fetchAllContactsWithSentiment(start,end,maxi,requestUrl){
	
	showProgressTitle("LOADING"); 		 
	var source = inbox_selected_mailbox; 
	var like = $("#"+selected_contact_list).attr("ref_letter");
	var mode = $("#"+selected_customer_mode).attr("ref_customer"); 
	var sentiment = $("#"+inbox_mail_panel).attr("ref_value");
	
	if(sentiment != undefined && mode != undefined ){
		$.ajax({
		     type: "GET", 
		     url: requestUrl,   
		     data: {'mode':mode,'source':source,'sentiment':sentiment,'like':like,'start':start, 'end':end},     
		     success: function(data){	 
		    	 if(data.status==200){
		    		 dropifi_next_inbox = 0;
		    		 next_end = false;
		    		 previous_end = true;
			    	 populateContacts(data); 
			    
			    	 if(data.contacts !=undefined){ 
				    	 if(data.contacts.length>0){
				    		 var idvalue = "contact_id_"+data.contacts[0].id;
				    		 viewContactMessages(idvalue ,maxi); 
				    	 }else{			
				    		// $("#incoming_scroll").html("");
				    		 clearContactPageDetail();
				    	 } 
			    	 }
		    	 }else{
		    		 //$("#incoming_scroll").html(""); 	
		    		 clearContactPageDetail();    
		    	 }
		    	hideProgress();
		    },
		   	error:function(data){
		   		//$("#incoming_scroll").html(""); 
		   		clearContactPageDetail();   
		   		hideProgress(); 
		   	}
		});	
	}
}

function viewContactMessages(id , maxi){
	try{
		hideProgress();
		showProgressTitle("LOADING");
		if(id != undefined){	
			
			var divId = "#"+id;
			var email = $(divId).attr("ref_email");
			//var source = inbox_selected_mailbox; 
			//var panel = is_inbox_panel_click; 
			//var sentiment = $("#"+inbox_mail_panel).attr("ref_value");
			 
			if( (email !=undefined)){
				$.ajax({
				     type: "GET", 
				     url:contactViewUrl,    
				     data: {'email':email}, 
				     success: function(data){  
				    	
				    	if(data.status == 200) {
				    		 
				    		contactProfile(data,divId, "contact"); 
				    		//setContactMessages(data.messages);	
				    		var pos = 0;	var neg = 0;	var neu = 0;
							$.each(data.sentiment,function(key,val){
								if(val.sentiment == "Positive" && val.score != null)
									pos = val.score.toFixed(2);
								if(val.sentiment == "Negative"  && val.score != null)
									neg = val.score.toFixed(2);
								if(val.sentiment == "Neutral" && val.score != null)
									neu = val.score.toFixed(2); 														
							});
							
							$("#analytic_posi").html(pos+" %");
							$("#analytic_nega").html(neg+" %");
							$("#analytic_neut").html(neu+" %");
							
							$("#analytics_new_msg").html(data.nmsgs);
							$("#analytics_resolved_msg").html(data.rmsgs);

							/*// individual Trending Emotions and Messages
							var emotionData = [];
							var emotionCate = [];
							
							$.each(data.trdemomsg, function(key,val){
								emotionCate.push(val.emotion);
								emotionData.push(val.number);									
							});
							
							if(emotionCate.length != 0){
								BarChartForTheDashBoard('analytics_emotions','Trending Emotions and Number of Messages','No of Messages',emotionCate,'Emotions',emotionData);
							}
							else{
								Show_No_Data("#analytics_emotions"); 
							}
							*/
							if(data.trdemomsg.length!=0){ 
								
								/*var f = new Date();
								var t = new Date(); 
								
								$.each(data.trdemomsg , function(key3, val3)
								{								
									if(f > (new Date(val3.timestamp)))
									{
										f = new Date(val3.timestamp);
									}									
								});	 
								
								
								drawingSentimentDoubleDateChart(data.trdemomsg, 'analytics_emotions', 'emotion', 'spline', 'Trending Emotions and Number of Messages', '', '', 'Number of Message', f, t);
								*/
								
								var f = new Date();
								var t = new Date();
								
								$.each(data.trdemomsg , function(key3, val3)
								{								
									if(f > (new Date(val3.timestamp)))
									{
										f = new Date(val3.timestamp);
									}									
								});
								DrawingSentimentChartForContacts(data.trdemomsg, 'analytics_emotions', 'emotion', 'Trending Emotions and Number of Messages', '', '', 'Number of Message', f, t);

 				 
							}else{
								Show_No_Data("#analytics_emotions"); 
							}
							
				    	}else{				    		
					   		clearContactPageDetail(); 
				    	}				    	
				    	hideProgress();
				    	resizeMailHeight();
				   	},
				   	error:function(data){	
				   		clearContactPageDetail(); 
				   		hideProgress();				   		 
				   	}
				   });	
			}
	 
		}
		 
	}catch(ee){
		hideProgress();
	}
}

function activatedCustomer(email,activated,requestURL,typeOfView){ 
	if(email!=undefined && email != "" && activated !=undefined && requestURL != undefined){
	  $.ajax({    		  
	      type: "POST", 
	      url:requestURL, 
	      data: {'email':email,'activated': activated},
	      success: function(data){ 	     	    	  
	    	 
	    	  if(data.status ==200){     	    		  
	    		  if(typeOfView == "inbox"){
	    			
	    		  }	else if(typeOfView == "contact"){ 
	    			  if(open_contact_id != undefined) { 
	    				 var customerId =$(open_contact_id).attr("ref_selected_customer");
	    			 
	    				 if(customerId!=undefined){
	    					 var custId = "#"+customerId;	    					 
	    					 $(custId).removeClass("is_prospect");
	    					 $(custId).removeClass("is_customer");	    					  
	    					  
		    				  if(activated == "true"){	
		    					 $(custId).addClass("is_customer");
		    					 $(custId).attr("ref_status", "true");			    					 		    					 
		    					 $(custId).html("Customer"); 		    				 
		    				  }	else if(activated == "false"){
	    					  	$(custId).addClass("is_prospect");
	    					 	$(custId).attr("ref_status", "false");	
	    						$(custId).html("Prospect");  		    					  					  
		    				  } 
	    				 }
	    			  }
	    		  }
	    	  }     	    	  
	      },
	      
	      error:function(data){
	    	
	      }
	      
	   });
	}
	 //mixpanel - track user 
    UserEventTrack.click({'event':'ACTIVATE_CUSTOMER-LINK CLICK'});
}

function viewMessage(id){
	try{
		hideProgress();
		showProgressTitle("LOADING");
		if(id != undefined){
			var divId = "#"+id;
			var idValue = divId.substring("mail_id_".length+1, divId.length);		 
			var email = $(divId).attr("ref_email");
			var msgId = $(divId).attr("ref_id");
			var status = $(divId).attr("ref_status"); 	
			var sentiment =$(divId).attr("ref_sentiment");
			var source = $(divId).attr("ref_source");			
			var index = $(divId).attr("ref_index"); 
			reply_current_messageId = idValue;
			reply_current_msgId = msgId;
			
			$.ajax({
			     type: "GET",  
			     url:viewUrl,    
			     data: {'id':idValue, 'msgId':msgId,'email':email,'msgStatus':status,'sentiment':sentiment,'source':source,'mailbox':inbox_selected_mailbox,'timezone':dropifi_timezone},  
			     success: function(data){  
			    	//alert("Load view message");
			    	if(data.status == 200){ 
			    		if(data.count){ 
			    			//setCounters(data.count); 
			    		} 			    				    	 		    		
			    		contactProfile(data, divId,"inbox");
			    		setMessage(data.message,data.domain,data.replyer);
			    		
			    		if(data.message !=undefined && index>=0){		    			 
			    			pullOfMessages[index].status = data.message.status; 
			    		}			    		
			    		resizeMailHeight();
			    	}else{
			    		clearContactProfile();
						$("#selected_mail_scroll").html(" "); 
			    	} 
			    	hideProgress();
			   	},
			   	error:function(data){		   		
			   		hideProgress(); 			   		 
			   	}
			   });	
	 
		}else{
			hideProgress();
		}
	}catch(ee){
		hideProgress();
	}
}

function viewMessage(id,idValue,email,msgId,status,sentiment,source,index){
	try{
		hideProgress();
		showProgressTitle("LOADING");
		if(id != undefined){
			var divId = "#"+id;
			var idValue = divId.substring("mail_id_".length+1, divId.length);		 
			var email = $(divId).attr("ref_email");
			var msgId = $(divId).attr("ref_id");
			var status = $(divId).attr("ref_status"); 	
			var sentiment =$(divId).attr("ref_sentiment");
			var source = $(divId).attr("ref_source");			
			var index = $(divId).attr("ref_index"); 
			reply_current_messageId = idValue;
			reply_current_msgId = msgId;
			
			$.ajax({
			     type: "GET",  
			     url:viewUrl,    
			     data: {'id':idValue, 'msgId':msgId,'email':email,'msgStatus':status,'sentiment':sentiment,'source':source,'mailbox':inbox_selected_mailbox,'timezone':dropifi_timezone},  
			     success: function(data){  
			    	//alert("Load view message");
			    	if(data.status == 200){ 
			    		if(data.count){ 
			    			//setCounters(data.count); 
			    		} 			    				    	 		    		
			    		contactProfile(data, divId,"inbox");
			    		setMessage(data.message,data.domain,data.replyer);
			    		
			    		if(data.message !=undefined && index>=0){		    			 
			    			pullOfMessages[index].status = data.message.status; 
			    		}			    		
			    		resizeMailHeight();
			    	}else{
			    		clearContactProfile();
						$("#selected_mail_scroll").html(" "); 
			    	} 
			    	hideProgress();
			   	},
			   	error:function(data){		   		
			   		hideProgress(); 			   		 
			   	}
			   });	
	 
		}else{
			hideProgress();
		}
	}catch(ee){
		hideProgress();
	}
}

function viewSentMessage(id){
	try{
		hideProgress();
		showProgressTitle("LOADING");
		if(id != undefined){
			var divId = "#"+id;
			
			var idValue = divId.substring("mail_id_".length+1, divId.length);
			var email = $(divId).attr("ref_to");
			var msgId = $(divId).attr("ref_id");
			var isSent = $(divId).attr("ref_isSent"); 
			reply_current_id =idValue; 
			$.ajax({
				type:"GET",
				url:viewSentUrl,
				data:{"id":idValue,"msgId":msgId,"email":email,'isSent':isSent,'timezone':dropifi_timezone},
				success: function(data){					 
					if(data.status == 200){					
						contactProfile(data, divId,"inbox");						 
						setSentMessage(data.message,data.domain,data.replyer);
					}else{
						setAsShowing(data.mstatus ,divId);
						clearContactProfile();
						$("#selected_mail_scroll").html(" ");
					}  					 
					hideProgress();	
				},
				error: function(data){ 
					hideProgress();
				}
			});
			
		}else{
			hideProgress();
		}
	}catch(ee){
		hideProgress();
	}
}

function filterByMailbox(start, end){ 
	 hideProgress(); 
	if(inbox_selected_mailbox !=undefined && inbox_selected_mailbox !=  undefined && is_inbox_panel_click !=undefined){
		
		var mode = $("#"+selected_customer_mode).attr("ref_customer"); 
		var source = inbox_selected_mailbox; 
		var isPanel = is_inbox_panel_click;
		var panel = inbox_mail_panel.substring("ref_".length,inbox_mail_panel.length); 
		var typeOfPanel = $("#"+inbox_mail_panel).attr("ref_panel");  
		showProgressTitle("LOADING");
	
		 $.ajax({    		  
		      type: "POST", 
		      url: mailbox_filters_0309_inbox, 
		      data: {'source':source,'mode':mode,'isPanel':isPanel,'panel':panel,'typeOfPanel':typeOfPanel,'start':start, 'end':end,'inboxPanel':inbox_mail_panel,'timezone':dropifi_timezone},
		      success: function(data){
		    	 if(data.status ==200){
		    		 dropifi_next_inbox = 0;
		    		 next_end = false;
		    		 previous_end = true;
		    		 ///setNumOfSubject(data.subjectCount);
			    	if(data.messages.length>0){			    			    	 
			    		populateMessages(data); 
			    		viewMessage("mail_id_"+data.messages[0].id); 			    		
			    	 }else{	
			    		 	    		 
						clearContactDetail();   
			    	 } 
			    	//setNumofSentMsg(data.sentMessages);
			    	 
		    	 }else{
		    		  
		    		      		 
					 clearContactDetail();    
		    	 }
		    	 hideProgress();
		      },
		      
		      error:function(data){	
		    	  		    		 
				clearContactDetail();   
		    	hideProgress();
		      }
		        
		  }); 
	}
}	 

function updatePendingResolved(id,msgStatus,typeOfUpdate,source,msgId){ 		
	
	if(id !=undefined && msgStatus !=undefined && typeOfUpdate != undefined && source,msgId!=undefined && newpendingresolved_0304!=undefined){
		var mailbox = inbox_selected_mailbox;			 
	 $.ajax({    		  
	      type: "POST",  
	      url: newpendingresolved_0304,
	      data: {'id':id,'msgStatus':msgStatus,'typeOfUpdate':typeOfUpdate, 'source':source, 'msgId':msgId,"mailbox":mailbox}, 
	      success: function(data){		    	 
	    	  if(data.status=200){
	    		setPendingResolved(data.mstatus);    		  
	    		//setCounters(data.count);    		  
	  			//mixpanel - track user 
	  	        UserEventTrack.click({'event':'INBOX-'+typeOfUpdate+'-BTN CLICK'}); 
	  	        setMessageCounters();
	    	  }		    	   	    	  
	      },		      
	      error:function(data){
	    	 
	      }
	      
	  }); 
	}	
} 

//------------------------------------------------ SENT MESSAGES------------------------------------------------
function fetchSentNessages(requestUrl, id){
	hideProgress();
	var isSent = (id =="ref_sent"?true:false); 
	showProgressTitle("LOADING"); 
	$.ajax({
	     type: "GET",
	     url:requestUrl,   
	     data: {'isSent':isSent,'timezone':dropifi_timezone}, 
	     success: function(data){ 
	    	
	    	if(data.status ==200){     		 
	    		 dropifi_next_inbox = 0;
	    		 next_end = false;
	    		 previous_end = true; 
		    	 populateMessages(data);     	    	 
		    	 if(data.messages.length>0){
		    		 viewSentMessage("mail_id_"+data.messages[0].id);
		    	 }else{					
					
					//$("#incoming_scroll").html("<h3>No Sent Messages</h3>");	
		 	   		 $("#selected_mail_scroll").html("");
		 	   		clearContactDetail();
		    	 }
		    	 setNumofSentMsg(data.sentMessages);  		    	 
	    	 }else{ 
	    		// $("#incoming_scroll").html("<h3>No Sent Messages</h3>");	
	 	   		 $("#selected_mail_scroll").html("");	 
	 	   		clearContactDetail();
	    	 }
	    	hideProgress();
	    },
	   	error:function(data){
	   		//$("#incoming_scroll").html("<h3>No Data Loaded</h3>");	
	   		$("#selected_mail_scroll").html("");
	   		hideProgress();
	   		clearContactDetail();
	   	}
	  });	
}

function sentMessage(message,index){
 	 		
	var subject = message.subject!=undefined? message.subject : "..";  
	var recipient =  message.to !=undefined?message.to : "..";	
	var userEmail =  (message.isSent==true?"Replied By : ":"Drafted By : ")+message.userEmail;
	//var email  =   substrValueDot(message.email,30,".."); 
	
	var msg = 
	'<input type="checkbox" id = "chk_mail_id_' + message.id + '"/>'+ 
	'<div class="mail_content"  ref_index = "'+index+'"  id ="mail_id_' + message.id + '" ref_id="' + message.msgId + '" ref_to="'+ message.to +
	'" onclick ="javascript:viewSentMessage(this.id)">'+ '<p class="hide_overflow_text"><span class="mail_to">'+recipient+
	'</span><span class="mail_time" style="float:right;">'+ message.created +'</span></p>'+'<p class="dominant_subject hide_overflow_text">'+subject+'</p>'+
	'<p class="hide_overflow_text" style="font-weight:normal;">'+userEmail+'</p>'+'</div></div>';
	
	return msg;
}

function deleteMessage(requestUrl,e){
	 hideProgress();  
	  var messageIds = getCheckedValues('msgArchives'); 
	  
	  if(messageIds){ 
			e.preventDefault();
			confirm("Are you sure you want to delete the selected messages?", function () {
				console.info("Testing",messageIds[0]);
				showProgressTitle("DELETING"); 
				$.ajax({ 
				     type: "DELETE", 
				     url:requestUrl,    
				     data: {'typeOfPanel':inbox_mail_panel,'messageIds':messageIds}, 
				     success: function(data){ 
				    	 hideProgress();				    	 
				    	 if(data.status==200){				    		
				    		//mixpanel - track user 
				    		UserEventTrack.click({'event':'INBOX-DELETE-MSG SUCCESSFUL'});
				    		$("#"+inbox_mail_panel+" a.panel").trigger('click'); 	
				    	 }else{				    	
				    	 //mixpanel - track user  
				    	 UserEventTrack.click({'event':'INBOX-DELETE-MSG FAILED'});
				    	 }
				    	 
				     },
				     error: function(data){
				    	 hideProgress();
				    	 //mixpanel - track user  
				    	 UserEventTrack.click({'event':'INBOX-DELETE-MSG FAILED'});
				     }
				}); 			 
			});
		 
	  }else{ 
		  //console.info("Testing","no value selected");
	  // return checkboxesChecked.length > 0 ? checkboxesChecked : null;
	  }
	  
	//mixpanel - track user viewing/loading of keyword page 
	UserEventTrack.click({'event':'INBOX-DELETE-MSG CLICK'});
}

function searchMessage(requestUrl,search,e){
	hideProgress();  
	showProgressTitle("SEARCHING");
	
	 $.ajax({    		  
	      type: "GET", 
	      url: requestUrl, 
	      data: {'search':search,'timezone':dropifi_timezone},
	      success: function(data){
	    	 if(data.status ==200){
	    		 dropifi_next_inbox = 0;
	    		 next_end = false;
	    		 previous_end = true;
	    		 
		    	if(data.messages.length>0){			    			    	 
		    		populateMessages(data);
		    		viewMessage("mail_id_"+data.messages[0].id);  	
		    		
		    		//mixpanel - track  
		    		UserEventTrack.click({'event':'INBOX-SEARCH-MSG SUCCESSFUL'});
		    	 }else{	
		    		 	    		 
					clearContactDetail();   
		    	 } 		    	 
	    	 }else{	    		  
	    		      		 
				 clearContactDetail();  
				//mixpanel - track user 
				UserEventTrack.click({'event':'INBOX-SEARCH-MSG FAILED'});
	    	 }
	    	 hideProgress();
	      },
	      
	      error:function(data){	
	    	hideProgress();  		    		 
			clearContactDetail();  
			//mixpanel - track user 
			UserEventTrack.click({'event':'INBOX-SEARCH-MSG FAILED'});
	      }
	        
	  }); 
	
	//mixpanel - track user 
	UserEventTrack.click({'event':'INBOX-SEARCH-MSG CLICK'});
}

function searchContact(requestUrl,search,e){
	hideProgress(); 
	showProgressTitle("SEARCHING");
	$.ajax({
	     type: "GET", 
	     url: requestUrl,   
	     data: {'search':search,'timezone':dropifi_timezone},  
	     success: function(data){
	    	 if(data.status==200){	    		 
	    		 dropifi_next_inbox = 0;
	    		 next_end = false;
	    		 previous_end = true;
		    	 populateContacts(data); 
		    
		    	 if(data.contacts !=undefined){ 
			    	 if(data.contacts.length>0){
			    		 var idvalue = "contact_id_"+data.contacts[0].id;
			    		 viewContactMessages(idvalue ,50); 
			    		//mixpanel - track user 
			    		UserEventTrack.click({'event':'INBOX-SEARCH-CONTACT SUCCESSFUL'});
			    	 }else{			
			    		// $("#incoming_scroll").html("");
			    		 clearContactPageDetail();
			    	 } 
		    	 }
	    	 }else{	    		 
	    		 clearContactPageDetail();   
	    	 }
	    	hideProgress();
	    }, 
	   	error:function(data){
	   	    hideProgress(); 
	   		clearContactPageDetail();  
	   		//mixpanel - track user  
	   		UserEventTrack.click({'event':'INBOX-SEARCH-CONTACT FAILED'});
	   	}
	  });
	
	//mixpanel - track user  
	UserEventTrack.click({'event':'INBOX-SEARCH-CONTACT CLICK'});
}


function addSearchTerms(requestUrl){
	$.ajax({    		  
	      type: "GET", 
	      url: requestUrl, 
	      data: {'timezone':dropifi_timezone},
	      success: function(data){
	    	 if(data.status ==200){	    		 
	    		AutoComplete_Create('search_autoComplete', data.search);
	    	 } 
	      },	      
	      error:function(data){	
	    	     	
	      }
	        
	  }); 
}

function panelClick(){
	$('.panel').click(function () { 
        //reset and highlight the clicked link
        $('a.panel').removeClass('selected');
        $(this).addClass('selected');  
		
		current = $(this).attr('id'); 
        $('div.item').hide();
		 
	    $('div#'+current).show();		
		
	});
}

function setMessageCounters(){ 
	$.ajax({    		  
	      type: "GET", 
	      url: countMessageUrl, 
	      data: {},
	      success: function(data){
	    	 if(data.status ==200){	    		 
	    		 if(data.count){
	    			 setCounters(data.count); 
	    		 }
	    		 if(data.subjectCount){
	    			 setNumOfSubject(data.subjectCount);
	    		 }
	    	 } 
	      }, 	      
	      error:function(data){	
	    	     	
	      }
	        
	  }); 
	
}

