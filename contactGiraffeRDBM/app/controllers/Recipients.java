package controllers;

import static akka.actor.Actors.actorOf;

import java.util.List;

import job.process.WidgetActor;
import akka.actor.ActorRef;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Company;
import models.MessageManager;
import models.MsgSubject;
import models.WMsgRecipient;
import models.WidgetTracker;
import models.dsubscription.SubConsumeService;
import models.dsubscription.SubServiceName;
import models.dsubscription.SubServiceType;
import play.cache.Cache;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.UserType;
import resources.helper.ServiceSerializer;
import resources.helper.ServiceVolumeSerializer;
import view_serializers.RecipientSerializer;

@With(Compress.class)
public class Recipients extends Controller{
	 	
	@Before
	static void setArgs(String domain){
		String apikey = Security.connectedApikey();	
		String foundApikey = Company.findApikey(apikey);
		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
			if(UserType.isAgent(Security.connectedUserType()))
 				Dashboard.index(domain);
			
	        renderArgs.put("page","admin");
	        renderArgs.put("sub_page","recipients");
	        renderArgs.put("title","Admin - Message Recipients"); 
		}else{
			//clear the session
	    	session.clear();
	    	//response.removeCookie("rememberme"); 
			try {
				Secure.login("");
			}catch(Throwable e) {
				Application.index();
			}
		}
    }
	
	public static void index(String domain){
		ForceSSL.redirectToHttps();
		
		String apikey = Security.connectedApikey();
		if(!validation.hasErrors() && apikey !=null){
			String connected = Security.connectedDomain(); 	
			domain =domain!=null? domain.trim():"";
 			//if(connected.equalsIgnoreCase(domain)){
			if(!connected.equalsIgnoreCase(domain)){			
 				index(connected);
 			}
				String connectedUsername = Security.connected(); 
				String userType = Security.connectedUserType().name();
				String ecomBlogPlugin = Security.connectedPlugin();
				//check if the widget is installed on the site of the user
 				boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
 				render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget); 

 			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
		Apps.page_not_found("");
	}
	
	public static void populate(){
		//TODO	 			 
		JsonObject json = new JsonObject();
		Gson gson = new  Gson();
		int status = 404;
		String apikey = Security.connectedApikey();
		int size = 0;
		String jsonString = "";
		if(apikey !=null){
			
			//get the message recipients from cache
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Recipients, DropifiTools.POPULATE);
			jsonString = Cache.get(cacheKey, String.class); 
			
			if(jsonString==null || jsonString.trim().isEmpty()){
				
				List<WMsgRecipient> recipients = WMsgRecipient.findSortedRecipients(apikey);			
			
				//check the status of the quickResponse: if size 0 then return 200 - quickResponse(s) is/are available
				size = recipients == null ? 0:recipients.size();
				status = (size>0) ? 200:501;
				json.add("recipients", WMsgRecipient.toJsonArray(recipients));
				json.addProperty("status", status); 
				json.addProperty("size", size); 
				
				//update the total volume consumed
				try{
					ServiceVolumeSerializer volSerializer = new ServiceVolumeSerializer(apikey, SubServiceName.Recipient, SubServiceType.FixedVolume, size);
					/*ActorRef widgetActor = actorOf(WidgetActor.class).start(); 
					widgetActor.tell(volSerializer);*/
					SubConsumeService.updateConsumedVolume(volSerializer); 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);  
				}
				
				ServiceSerializer service = SubConsumeService.findServiceSerializer(apikey, SubServiceName.Recipient);
				json.add("service", gson.toJsonTree(service));
				
				//cache the service
				CacheKey.cacheSubService(apikey,CacheKey.Recipients,service, DropifiTools.SUBSERVICE);
				
				try{
					//set the recipients to cache
					jsonString = json.toString(); 
					Cache.set(cacheKey, jsonString);
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
				play.Logger.log4j.info("Populate Message Recipients : Not Queried from cache");
			}else{
				play.Logger.log4j.info("Populate Message Recipients : Queried from cache");
			}
		}else{
			json.addProperty("status", status); 
			json.addProperty("size", size);
			
			jsonString = json.toString();
		}

		renderJSON(jsonString);
	}
	
	public static void show(@Required String email, @Required String domain){
		ForceSSL.redirectToHttps();
		String connected = Security.connectedDomain(); 			
		String apikey = Security.connectedApikey();
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){					
		//TODO:  
			String btnsave = "";	
			WMsgRecipient recipient =null;
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			
			if(validation.required(email).ok){
				
				//check if a recipient is specified			
				if(email.equals("new")){
					 btnsave ="Save"; 
					 //render(btnsave,email,domain);
				}else{
					//search for the recipient profile 
					recipient = WMsgRecipient.findRecipient(apikey, email);								
					btnsave ="Update"; 
					//render(btnsave,email,recipient,domain);
				} 			
			}else{
				email = "new";
				btnsave = "Save"; 
				//render(btnsave,email,domain);  
			}
			
			ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Recipients, SubServiceName.Recipient);
			render(btnsave,email,recipient,domain,connectedUsername,ecomBlogPlugin,userType,service); 
		}
		Apps.popout_not_found("");
	}
	
	public static void create(@Valid RecipientSerializer re){
		
		//TODO: provide method for save the quickResponse
		Gson gson = new Gson(); 				 
		JsonObject json = new JsonObject();
		int status = 404;
		String apikey = Security.connectedApikey();
		if(!validation.hasErrors()){
			if(validation.required(re.getEmail()).ok && validation.required(re.getUsername()).ok){ 
				if(apikey != null){
					//check if the
					ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Recipients, SubServiceName.Recipient);
					String msg="";
					String domain = Security.connectedDomain();
					if(service!=null && !service.hasExceeded){
						WMsgRecipient recipient = new WMsgRecipient(apikey, re);
						status = recipient.saveRecipient();
						msg = (status == 200 ? "none" : (status==302 ? "duplication of recipients - ": "update was not successful"));
						
						//delete the list of recipients from the cache
						CacheKey.delListOfRepFromCache(apikey,CacheKey.Recipients);
					}else{
						msg = "<div class='main_upgrade'><a class='link_upgrade' href=/"+domain+"/admin/pricing/index' target='_blank'>Upgrade to add more Message Recipients</a></div>";
					}
					
					json.addProperty("error", msg); 
				}
			}else{ 
				status = 501;
			}
		}else{
			json.addProperty("error", "");
		}
		
		json.addProperty("status", status);
		json.add("content", gson.toJsonTree(re)); 
		 
		renderJSON(json.toString());
	}
	
	public static void update(@Required String email, @Valid RecipientSerializer re){
		//TODO: 
		
		Gson gson = new Gson(); 				 
		JsonObject json = new JsonObject();
		int status = 404;
		String apikey = Security.connectedApikey();
		
		if(!validation.hasErrors()){
			if(validation.required(re.getEmail()).ok && validation.required(re.getUsername()).ok){ 	
				if(apikey != null){
					WMsgRecipient recipient = WMsgRecipient.findRecipient(apikey, email);
					if(recipient!=null){
						status = recipient.updateRecipient(re);
						json.addProperty("error", (status == 200 ? "none" : (status==302 ? "duplication of recipients - ": "update was not successful")) );
						
						//delete the list of recipients from the cache
						CacheKey.delListOfRepFromCache(apikey,CacheKey.Recipients);
					}
				}
			}else{
				status = 501;
			}
		}else{
			json.addProperty("error", "");
		}
		
		json.addProperty("status", status);
		json.add("content", gson.toJsonTree(re)); 		
		renderJSON(json.toString());
	}
	
	public static void delete(@Required String email){ 
		JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();
			
			if(apikey != null){	
				status = WMsgRecipient.deleteRecipient(apikey, email);
				json.addProperty("error", (status==200? "none" : "update was not successful") );
				
				//delete the list of recipients from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Recipients);
			}
		}
		json.addProperty("status", status); 
		json.addProperty("email", email); 
		
		renderJSON(json.toString()); 
	}
	
	public static void activateRecipient(@Required String email, @Required boolean activated){
		//Gson gson = new Gson();				 
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				WMsgRecipient recipient = WMsgRecipient.findRecipient(apikey, email);
				if(recipient != null){ 	
					status = recipient.activateRecipient(activated); 				
					json.addProperty("error", (status==200?"none":"update was not successful") );
					
					//delete the list of recipients from the cache
					CacheKey.delListOfRepFromCache(apikey,CacheKey.Recipients);
				} 
			} else {
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("activated", activated);
		json.addProperty("status", status); 	
		renderJSON(json.toString());
	}
	
	public static void orderRecipients(@Required List<String> recipients){
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();	 
			if(apikey != null){
				status = WMsgRecipient.orderRecipients(apikey, recipients);
				json.addProperty("error", (status==200?"none":"update was not successful") );
				
				//delete the list of recipients from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Recipients);
			}
		}else{
			json.addProperty("error", "required fields are not specified");
		} 
		
		json.addProperty("status", status); 	
		renderJSON(json.toString());
	}
	
}
