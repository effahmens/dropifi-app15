package controllers;

import static akka.actor.Actors.actorOf;

import java.net.URI;
import java.sql.Timestamp; 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import job.process.EventTrackActor;
import job.process.MailingActor;
import job.process.WidgetTrackerActor;
import models.Company;
import models.CompanyProfile;
import models.EcomBlogPlugin;
import akka.actor.ActorRef;

import com.google.api.client.json.Json;
import com.google.gson.JsonObject;
import com.shopifyOAuthen.api.APIAuthorization;
import com.shopifyOAuthen.api.client.ShopifyClient;
import com.shopifyOAuthen.api.credentials.Credential;
import com.shopifyOAuthen.api.credentials.ShopifyCredentialsStore;
import com.shopifyOAuthen.api.endpoints.AuthAPI;
import com.shopifyOAuthen.api.endpoints.ProductsService;
import com.shopifyOAuthen.api.endpoints.ScriptTagsService;
import com.shopifyOAuthen.api.endpoints.ShopsService;
import com.shopifyOAuthen.api.resources.Product;
import com.shopifyOAuthen.api.resources.ScriptTag;
import com.shopifyOAuthen.api.resources.Shop;
import com.shopifyOAuthen.connectors.ShopifyAPIConnect;

import play.data.validation.Required;
import play.libs.Codec;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.*;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import resources.WidgetTrackerSerializer;
import view_serializers.MailSerializer;

@With(Compress.class)
public class ShopifyOAuth extends Controller {
	
	public static void index(String shop,String new_useremail,boolean isEmail, String phone){
		if(flash.get("new_account")==null && flash.get("existing_account")==null)
			flash.put("new_account", true);
		
		String shop_s = session.get("shopify_shop");
		 
		if(shop_s == null || shop_s.trim().isEmpty()) {		
			Security.signup("");
		}else{
			shop = shop_s;
		}
		
		render(shop, new_useremail,isEmail,phone);	 
	}
	
	public static void show_new(String shop,String email,boolean isEmail,String phone, String error){
		flash.put("new_account", true);
		flash.put("new_useremail", email); 
		if(error!=null) 
			flash.error(error);
		
		flash.keep();
		index(shop, email,isEmail,phone);  
	}
	
	public static void show_existing(String shop,String email,boolean isEmail,String phone){
		flash.put("existing_account", true);
		index(shop, email,isEmail,phone);
	} 
	
	public static void login_oauth2(String code){
		String accessToken = session.get("shopify_accessToken"); 
		//play.Logger.log4j.info("Code: "+code);
		
		if(accessToken ==null){	  
			session.remove("shopify_accessToken"); 
		}else{ 
			
		} 
	}
	
    public static void login(String shop,String code,String timestamp, String signature) { 
    	
    	//remove all data from the session
    	String accessToken = session.get("shopify_accessToken");
    	String email = "";
    	String phone=null;
    	boolean isEmail = false; 
		Credential cred = new Credential(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey,  shop);
		AuthAPI auth = new AuthAPI(new APIAuthorization(cred));			
    	
		if(accessToken == null){	 
			URI url = auth.getAuthRequestURI();		 
			removeFromSession(); 
			session.put("shopify_accessToken",shop);  
			redirect(url.toString()); 			
    	}else{
    		  
    		try{ 
    			//get the access token
        		String access_token = auth.getAccessToken(code); 
        		session.clear(); 
        		
				ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, shop,access_token,timestamp,signature);
				
				//request for the shop info
				ShopsService myShop = connect.GetClient().constructService(ShopsService.class); 
				
				if(myShop != null){
					Shop sh = null;
					 
					try{ 
						sh = myShop.getShops(); 
						session.remove("shopify_accessToken");
					}catch(Exception e) {
						//shop authentication failed. Repeat authentication
						//play.Logger.log4j.info(e.getMessage(),e);  
						session.remove("shopify_accessToken");		 
						String msg = "<p>Could not access your store - "+shop+". <br>Ensure that your are installing Dropifi contact widget " +
								"from the Shopify App store.<br> <a href='http://apps.shopify.com/dropifi-contact-widget'>Click here to view the app on Shopify</a></p>";
						
						Apps.page_not_found(msg);
					}
					
					if(sh != null){			 
						
						addToSession(shop,code, timestamp, signature,access_token);						
									
						EcomBlogType ecomType = EcomBlogType.Shopify;
						Company company = Company.findByEcomBlog(shop, ecomType);
						
						//check if the user shopify account is created 
						if(company != null && company.getId()!=null){
							EcomBlogType pluginType = company.getEcomBlogPlugin().getEb_pluginType();

							//returning user
							phone = company.getProfile().getPhone();
							if(phone==null || phone.trim().isEmpty())
								phone = sh.getPhone();
							
							EcomBlogPlugin plugin = new EcomBlogPlugin(company.getEmail(), sh.getMyshopifyDomain(), access_token, code, EcomBlogType.Shopify);
							CompanyProfile profile = new CompanyProfile(sh.getShopOwner(), sh.getMyshopifyDomain(), sh.getTimezone(),sh.getPlanName(), sh.getCountry(),phone);					
							
							//make changes the account
							company.setProfile(profile); 
							company.setEcomBlogPlugin(plugin);
							company.setDomain(plugin.getDomain()); 
							company = company.save();
							
							//redirect the owner to the inbox page
							//if(company.getDomain().equalsIgnoreCase(plugin.getDomain())){
								//add the js script to shopify
								company.getEcomBlogPlugin().addShopifyOAuthScriptTag(access_token,timestamp,signature);
								
							try{
								WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),sh.getMyshopifyDomain(),sh.getMyshopifyDomain());
								ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
								trackActor.tell(track);  
							}catch(Exception e){
								play.Logger.log4j.error(e.getMessage(), e);
							}
							
							//clear the seesion
							ShopifyOAuth.removeFromSession(); 		
							session.clear(); 
							
							//set the session
							Security.setLoginSession(company,"","LOGIN",""); 
							
							PrestaShop.revertToFree(company.getApikey(),pluginType, plugin.getEb_pluginType());
							
							Dashboard.index(company.getDomain()); 
							//}
							    
						} 
						
						//isEmail = Company.isEmailAvailable(sh.getEmail());
						
						email = sh.getEmail();
						phone = sh.getPhone();
						//play.Logger.info("has shop info" + sh.getMyshopifyDomain()); 
						//ask the user to create a dropifi account  
					}else{
						//play.Logger.info("Access Token 1:  shop authentication failed. Repeat authentication");
						//shop authentication failed. Repeat authentication
						session.remove("shopify_accessToken");						
						login(shop, code, timestamp, signature);
					}
				} 				
				
			}catch (Exception e){
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(),e);  
			}finally{    		 
				session.remove("shopify_accessToken");
			}
		}
    	
        show_new(shop,email,isEmail,phone,""); 
    }
    
    public static void createNewAccount(@Required String new_useremail, @Required String userpassword, @Required String confirmPassword,String location, String phoneNumber){    	    	    	
    	String shop 	 = session.get("shopify_shop"),
    		   code   	 = session.get("shopify_code"),
    	       timestamp = session.get("shopify_timestamp"),
    	       signature    = session.get("shopify_signature"),     	
    	 	   access_token = session.get("shopify_access_token");
    	
    	if(validation.hasErrors()){
    		//boolean isEmail = new_useremail!=""?Company.isEmailAvailable(new_useremail):false;
    		show_new(shop,new_useremail,true,phoneNumber,"Oops, email address and password are required");
    	}else if(!userpassword.equals(confirmPassword)) {//check password match
    		    		 
    		//boolean isEmail = new_useremail!=""?Company.isEmailAvailable(new_useremail):false; 
    		show_new(shop,new_useremail,true,phoneNumber,"Oops, password and re-passoword do not match");		 
    	}else if(Company.isEmailAvailable(new_useremail)){		 
    		show_new(shop,new_useremail,true,phoneNumber,"Oops, the email  address "+new_useremail+" belongs to an existing account.");	 
    	} 
    	
    	//check if the account is not created					 		
		if(EcomBlogPlugin.hasEcomBlog(new_useremail,shop,EcomBlogType.Shopify)){
			//play.Logger.log4j.info("An account with the email address "+new_useremail+" and shop ("+shop+") belongs to an existing account."); 
    		//boolean isEmail = true;    		 
    		show_new(shop,new_useremail,true,phoneNumber,"An account with the email address "+new_useremail+" and shop name ("+shop+") belongs to an existing account.");	
		}
    	
    	Shop sh = null;     	
		try {
			
			if(shop!=null){
				ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, shop,access_token,timestamp,signature);

				sh = connect.getShop();
				if(sh!=null){ 
					
					EcomBlogPlugin plugin = new EcomBlogPlugin(new_useremail, sh.getMyshopifyDomain(), access_token, code, EcomBlogType.Shopify);
					CompanyProfile profile = new CompanyProfile(sh.getShopOwner(), sh.getMyshopifyDomain(), sh.getTimezone(),sh.getPlanName(), sh.getCountry(),(phoneNumber!=null && !phoneNumber.trim().isEmpty() ?phoneNumber:sh.getPhone()));
 
					if(Company.isDomainAvailable(plugin.getDomain())){
						show_new(shop,new_useremail,true,phoneNumber,"Oops, your shop name "+plugin.getEb_name()+" is associated with a Dropifi account.");
					}
					
					//create a new account for the shop
					Company company = new Company(plugin, userpassword,profile); 
					company = company.createCompany();				
					if(company!=null && company.getId()!=null){
							
						//add the widget js file to the shop's script tag 	
			    		company.getEcomBlogPlugin().addShopifyOAuthScriptTag(access_token,timestamp,signature);	 
			    					    		
			    		try{
			    			//send an initial message to the user			    			 
							company.sendWelcomeMsg(EcomBlogType.Shopify); 
							 
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						}
			    		
			    		try{
							WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),sh.getMyshopifyDomain(),sh.getMyshopifyDomain());
							ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
							trackActor.tell(track);  
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						}
			    		
			    		try{   					
							//send an email to the shop owner
				    	    MailSerializer mail = new MailSerializer(company.getProfile().getName(), company.getEmail(),"", "Welcome to Dropifi",company.getShopifySignUpMsg());								
							ActorRef mailActor = actorOf(MailingActor.class).start();   
							mailActor.tell(mail); 
							
			    	    }catch(Exception e){
			    	    	play.Logger.log4j.error(e.getMessage(), e);
			    	    } 
			    		
			    		Shopify.removeFromSession();	
			    		session.clear(); 			    		
			    		Security.setLoginSession(company,"","SIGNUP",location);  

			    		//redirect the owner to the widget customization page
						redirect("/"+company.getDomain()+"/admin/widgets");
					}else{					 
						show_new(shop,new_useremail,true,phoneNumber,"Oops, the account can not be created.");
					}
				}else{
					login(shop,code,timestamp,signature);
				} 
			
			}else{
				//user login credentials not found. repeat shop authentication
				login(shop, code, timestamp, signature);
			}
			
		}catch (Exception e) {
			//TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(),e);
			flash.put("error", "An error occurred while installing dropifi contact widget for your store. Try installing it again");			
		}finally{			 
			//boolean isEmail = new_useremail!=""?Company.isEmailAvailable(new_useremail):false;			 
			show_new(shop,new_useremail,true,phoneNumber,"");
		}
    }
    
    public static void createExistingAccount(@Required String user_email, @Required String password,String location){
    	 
    	String shop      = session.get("shopify_shop"),
	    	   code        = session.get("shopify_code"),
	    	   timestamp = session.get("shopify_timestamp"),
	    	   signature = session.get("shopify_signature"),
	    	   access_token =session.get("shopify_access_token");     	
    	
    	boolean isEmail = false;
    	flash.put("user_email", user_email);
    	
    	if(validation.hasErrors()){    		
    		flash.put("error", "user email address and password required");    		
    	}if(!validation.email(user_email).ok){
    		flash.put("error", "user email address required");
    	}else{
    		
	    	//find the user information
	    	 Map<String,Object> userdata = Company.findByEmailPassword(user_email,UserType.Admin);
	    	    	 
	    	if(userdata == null){
	    		flash.put("error", "Oops, there is no account associated with this email");
	    		isEmail = true;
	    	}else{	    		
	    		Object ph = userdata.get("password");	
	    		String pass = ph!=null?(String)ph:null;
	    		
	    		String hPassword = Codec.hexMD5(password);
	    		
	    		if(!pass.equals(hPassword) || pass==null){ 
	    			flash.put("error", "Oops, the password you entered is not correct");
		    		isEmail = true;
		    	}else{		    			
		    		Company company = (Company) userdata.get("company");
		    		
		    		try{
		    			
		    			if(shop!=null){
				    		ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, shop,access_token,timestamp,signature);
						    Shop sh = connect.getShop();
						    
							if(sh != null){ 
								EcomBlogType pluginType = company.getEcomBlogPlugin().getEb_pluginType();
										
								String phone = company.getProfile().getPhone();
								if(phone==null || phone.trim().isEmpty())
									phone =sh.getPhone();
								
							 	EcomBlogPlugin plugin = new EcomBlogPlugin(company.getEmail(), sh.getMyshopifyDomain(), access_token, code, EcomBlogType.Shopify);
								CompanyProfile profile = new CompanyProfile(sh.getShopOwner(), sh.getMyshopifyDomain(), sh.getTimezone(),sh.getPlanName(), sh.getCountry(),phone);
														
								try{
									
					    			//reset the EcomBlogPlugin
									company.setEcomBlogPlugin(plugin);									 
									company.setProfile(profile);
									//company.setDomain(plugin.getDomain()); 
									company.save();
									 
					    			//add the javascript file
						    		company.getEcomBlogPlugin().addShopifyOAuthScriptTag(access_token,timestamp,signature);	
						    		
						    		try{
										WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),sh.getMyshopifyDomain(),sh.getMyshopifyDomain());
										ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
										trackActor.tell(track);  
									}catch(Exception e){
										play.Logger.log4j.error(e.getMessage(), e);
									}  
						    		
						    		Shopify.removeFromSession();						    		
						    		session.clear();
						    		
						    		company.loginSubscription();
						    		Security.setLoginSession(company,"","LOGIN",location);
						    		
						    		PrestaShop.revertToFree(company.getApikey(),pluginType, plugin.getEb_pluginType());
						    		
						    		//redirect the owner to the widget customization page
									redirect("/"+company.getDomain()+"/admin/widgets"); 
		
					    		}catch(Exception e){    			
					    			//unable to create the account 			    			
					    			flash.put("error" , "An error occurred while adding dropifi contact widget to your store. Try creating the account again");     			
					    		} 	
								
							}else{		
								//play.Logger.log4j.error(sh + "1. getting the shop's information failed. repeat shop authentication");
								//getting the shop's information failed. repeat shop authentication
								login(shop,code,timestamp,signature);
							}
		    			}else{
		    				//play.Logger.log4j.error("2. getting the shop's information failed. repeat shop authentication");
		    				//getting the shop's information failed. repeat shop authentication
		    				login(shop,code,timestamp,signature);
		    			}
		    		}catch(Exception e){
		    			play.Logger.log4j.error(e.getMessage(),e);
		    		}
	    		}
	    	}    	
    	}
    	
    	flash.put("existing_account", true);
		flash.keep();		
		index(shop,user_email,isEmail,null);
    	
    }
    
    /*
    public static void updateShopifyPlugin(String shopName) {
    	JsonObject json = new JsonObject();
    	 EcomBlogPlugin  ecom = EcomBlogPlugin.findEcomBlogPlugin(shopName,EcomBlogType.Shopify);
    	 ecom.addShopifyOAuthScriptTag(); 
    	 renderJSON(json.toString());
    }
    
    public static void updateShopifyPluginAll(String shopName) {
    	JsonObject json = new JsonObject();
    	List<EcomBlogPlugin> ecoms = EcomBlogPlugin.findEcomBlogPlugin(EcomBlogType.Shopify);
    	if(ecoms!=null) {
    		play.Logger.info("Total: "+ecoms.size());
    		for(EcomBlogPlugin ecom : ecoms) {
    			if(ecom.addShopifyOAuthScriptTag())
    				json.addProperty(ecom.getEb_name(), ecom.getEb_email());
    		}
    		play.Logger.info("Completed");
    	} 
    	renderJSON(json.toString());
    }
    
    public static void haswidget() {
    	JsonObject json = new JsonObject();
    	List<EcomBlogPlugin> ecoms = EcomBlogPlugin.findEcomBlogPlugin(EcomBlogType.Shopify);
    	if(ecoms!=null) {
    		play.Logger.info("Total: "+ecoms.size());
    		for(EcomBlogPlugin ecom : ecoms) {
    			try {
    				WSRequest rpxRequest = WS.url("http://"+ecom.getEb_name());
    				 
	    			HttpResponse res = rpxRequest.get();
	    			String content = res!=null?res.getString():"";	
	    			if(content.contains("dropifi_widget.shopify.js")) {
	    				json.addProperty(ecom.getEb_name(), ecom.getEb_email());
	    				ecom.addShopifyOAuthScriptTag();
	    			}
    			}catch(Exception e) {
    				play.Logger.info(ecom.getEb_name()+" not found");
    			}
    		}
    		play.Logger.info("Completed");
    	} 
    	renderJSON(json.toString());
    	
    }
    */
    
    @Util
    protected static void addToSession(String shop,String code,String timestamp, String signature,String access_token){
    	session.put("shopify_shop", shop);
    	session.put("shopify_code", code);
    	session.put("shopify_timestamp", timestamp);
    	session.put("shopify_signature", signature);
    	session.put("shopify_access_token", access_token);
    	
    }
    
    @Util
    protected static void removeFromSession(){
    	session.remove("shopify_shop");
    	session.remove("shopify_code");
    	session.remove("shopify_timestamp");
    	session.remove("shopify_signature");
    	session.remove("shopify_access_token");
    }
    
    
}
