/**
 * @author philips
 * @description 
 * @created 27/2/2012
 */


package controllers;

import static akka.actor.Actors.actorOf;
import akka.actor.ActorRef;
import api.tictail.TictailStore;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.shopify.api.client.ShopifyClient;
import com.shopify.api.credentials.Credential;
import com.shopify.api.credentials.JsonDirectoryCredentialsStore;

import play.cache.CacheFor;
import play.data.validation.*;
import play.libs.Codec;
import play.libs.Crypto;
import play.mvc.Before;
import play.mvc.Http;
import play.mvc.Util;
import play.mvc.With;
import resources.AccountType;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import view_serializers.LoginSerializer;
import view_serializers.MailSerializer;
import view_serializers.SignUpSerializer;
import models.*;
import models.dsubscription.SubPlan;

import java.util.*;

import job.process.EventTrackActor;
import job.process.LoginActor;
import job.process.MailingActor;
import job.process.WidgetActor;
@With(Compress.class)
public class Security extends Secure.Security {	 
	
	@Before
	public static void onload(){
		ForceSSL.secureDevMode();
	}  
	/**
	 * This method is called during the authentication process. This is where you check if
	 * the user is allowed to log in into the system. This is the actual authentication process
	 * against a third party system (most of the time a DB).
	 * 
	 */	
	
	public static int authenticate(String useremail,String password, String IPAddress,String location, String timezone) {		 
		Map<String,String> credentials = Company.authenticate(useremail, password);
		if(credentials != null){
			try{
				String hPassword = Codec.hexMD5(password); 	
				String credPassword = credentials.get("hashPassword");
				String userType = credentials.get("userType");
				Boolean activated = Boolean.valueOf(credentials.get("activated"));
				
				if(userType!=null && userType.equals(UserType.Agent.name()) && activated!=null && activated==false){
					 return 203; //account deactivated
				}
				
				if(credPassword!=null && hPassword!=null && hPassword.compareTo(credPassword) == 0){
					//Mark user as connected
					credentials.put("userEmail", useremail);
					credentials.put("IPAddress", IPAddress);
					credentials.put("location", location);
					Security.setLoginSession(credentials); 
					return 200; 
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
			return 201; 			 
		} 
		return 501; 
    }
	
	public static int reset(String email){ 
		
		int status =501;
		AccountUser user = AccountUser.findByEmail(email);
		
		if(user != null){			
			status = user.requestPasswordReset();
			if(status == 200){				
				MailSerializer mail = new MailSerializer(user.getProfile().getUsername(),user.getProfile().getEmail(),"","Dropifi.com Password Reset",user.getResetMessage());								
				ActorRef mailActor = actorOf(MailingActor.class).start();  
				mailActor.tell(mail);  
			}
		}else{
			status = 404;
		}
		return status; 
	}
	  
	/**
     * This method is called after a successful authentication.     * 
     */
 	public static void onAuthenticated(String url,String error,String prestashop){	
		//perform checks on the user and provide the redirecting url 		 
		String domain = Security.connectedDomain();
		String apikey = Security.connectedApikey();
		String useremail = Security.connected();
		if(apikey !=null && !apikey.trim().isEmpty()){
			//update the user login date
			
			 try{
		    	 LoginSerializer login = new LoginSerializer(Security.connected(), apikey)	;					
				 ActorRef loginActor = actorOf(LoginActor.class).start();   
				 loginActor.tell(login); 
	    	    }catch(Exception e){
	    	    	
	    	    } 
			 
			 if(url!=null && !url.trim().isEmpty()){
				 url = url.replace("|", "&");
				 url = url.startsWith("/")?url:"/"+url;
				 redirect("/"+domain+url); 
			 }
			 
			//redirect to Dashboard 
			boolean newAccount = Boolean.valueOf(session.get("newAccount")); 
			//session.remove("newAccount");  
			session.put("prestashop", prestashop);
			
			//play.Logger.log4j.info(Security.connectedPrestashop());
			
			//logout if the user is logging as prestashop demo and the account is not prestashop
			if(prestashop!=null && prestashop.equals("true") && !Security.connectedPlugin().equals(EcomBlogType.PrestaShop.name())){
				try {
					flash.put("secure.error", "You have been logout because the account is not a prestshop account");
					Secure.logout();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					Apps.page_not_found("");
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
			
			if( domain != null && !newAccount){ 
				//check if the user has the widget code actively on their site
				WidgetTracker track = WidgetTracker.findByApikey(apikey);
				
				if(track==null || !track.hasWidgetInstalled(EcomBlogType.valueOf(Security.connectedPlugin()))){
					redirect("/"+domain+"/dashboard/welcome/"); 
				} 
				Dashboard.index(domain);
				
			}else if(domain != null && newAccount){
				redirect("/"+domain+"/dashboard/welcome/");
			} 	
		}
		
		try {			 
	        session.clear();
	        //response.removeCookie("rememberme"); 
			//Secure.login("");
	        renderTemplate("Secure/login.html", url,useremail,error);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			Application.page("home");
		}
	}
 	
 	public static void onDisconnect(){
 		
 	}
	
    //when signup fails, redirect to this url
    public static void redirectToSignup(String url){
    	//flash.keep('url');
    	params.flash();
        validation.keep();     
        //redirect to signup page
        redirect(url.trim().equals("")?"/":url);
    }
    
    //---------------------------------------------------------------------------------------------
	//signup
    
    //@CacheFor("1d")
    public static void signup(String... args){
    	//ForceSSL.redirectToHttps();
    	renderArgs.put("page", "signup"); 
    	String plan = request.params.get("plan", String.class);
    	
    	if(plan==null || plan.trim().isEmpty()){
    		plan = (args!=null && args.length>0)?args[0] :null; 
    		plan = (plan==null || plan.trim().isEmpty())? AccountType.Free.name():plan ;
    	}  
		List<SubPlan> subPlans = null;
		SubPlan sPlan =null;
		if (!plan.equalsIgnoreCase("Free")){ 
			subPlans = SubPlan.findAllPlans();
			sPlan = SubPlan.findSubPlan(plan, subPlans); 
		}
		
		String asignName = DropifiTools.changePlanName(plan);
    	
    	render(plan,subPlans,sPlan,asignName); 
    }
    
    public static void profile(){ 
    	UserType userType = Security.connectedUserType();
    	String apikey = Security.connectedApikey();
    	
    	if(apikey != null){
	    	if(UserType.isAgent(userType))
					Dashboard.index(Security.connectedDomain());
	    	
	    	
	    	CompanyProfile profile = new CompanyProfile();
	    	String username="";
	    	String connectedUser="";
	    	if(apikey !=null){
	    		try{
		    		Map<String,Object> pro = Company.findProfile(apikey);
		    		profile =(CompanyProfile) pro.get("profile"); //Company.findProfileByApikey(apikey); 
		    		username = (String)pro.get("username");
		    		connectedUser = TictailStore.resetEmail(Security.connected());
	    		}catch(Exception e){
	    			play.Logger.log4j.info(e.getMessage(),e);
	    		}	    		
	    	}
	    	
	    	render(profile,username,connectedUser); 
    	}
    	
    	Apps.popout_not_found("");
    } 
	
    public static void subscription(){ 
    	UserType userType = Security.connectedUserType();
    	if(userType!=null && UserType.isAgent(userType))
				Dashboard.index(Security.connectedDomain());
    	
    	render();
    } 

    public static void changepassword(){   	
    	render();
    } 
    
    public static void changeemail(){   	
    	render();
    }
    
    public static void savePassword(@Required String currentPassword, @Required String newPassword,@Required String confirmPassword) {
    	JsonObject json = new JsonObject();
    	int status =404;
    	if(!validation.hasErrors()){
	    	String apikey = Security.connectedApikey();
	    	
	    	if(apikey !=null){
	    		AccountUser user = AccountUser.findByEmailAndApikey(Security.connected(), Security.connectedApikey());
	    		 if(user !=null){    			 
	    			 
	    			 if(newPassword.compareTo(confirmPassword) ==0){
	    				 status = user.changePassword(currentPassword, newPassword);
	    				 if(status == 501){
	    					 json.addProperty("error","the current password specified is not correct.");
	    				 }
	    			 }else{    				 
	    				 json.addProperty("error","the new and confirm password you entered is do not match.");
	    			 }  			 
	    		}else{
	    			json.addProperty("error","Oops, your password could not be save.");
	    		}
	    	}else{ 
	    		json.addProperty("error","Oops, your password could not be save."); 
	    	}
    	}else{
    		
    		json.addProperty("error","Oops, Complete all fields. They are required to change your password."); 
    	} 
    	json.addProperty("status", status);
    	renderJSON(json.toString()); 
    }
    
    public static void reset_user_email(@Required String current, @Required String email, @Required String resetkey) {
    	String titleMsg = "Dropifi.com - Reset Account Email", headMsg = "Reset Account Email", subMsg = "Your account email could not be changed.", contentMsg = "";
    	int status =404;

    	if(!validation.hasErrors()){ 
    		AccountUser user = AccountUser.findByEmailResetKey(current, resetkey); 
    		if(user !=null){  	
				if(!Company.isEmailAvailable(email)){		    		 		 
	    			 status = user.changeEmail(email); 
	    			if(status==200){
	    				contentMsg= "Login to your account with the email address "+email+" and your current password";
	    				subMsg = "Your account email has been changed.";	
	    				status=1260; 
	    				//force the user to logout and login with the new email and the current password
	    				session.clear();
	    			}else{
	    				contentMsg = "Oops, the request to reset your Dropifi accoount email failed";
	    			}
	    		}else{
	    			contentMsg = "Oops, the email  address "+email+" belongs to an existing account.";    			
	    		}	
			}else{
    			contentMsg = "Oops, you tried resetting your email from an invalid or out-dated link";
    		}
    	}else{    		
    		contentMsg = "Oops, you tried resetting your password from an invalid link"; 
    	}    	
 
    	Application.message_display(titleMsg,  headMsg,subMsg, contentMsg, email,  status);
    }	
    
    public static void resetEmail(@Required String newEmail,@Required String currentPassword){
    	JsonObject json = new JsonObject(); 
    	int status =404;
    	if(!validation.hasErrors()){
	    	String apikey = Security.connectedApikey();
	    	
	    	if(apikey !=null){
	    		if(!Security.connected().equalsIgnoreCase(newEmail)){
		    		if(!Company.isEmailAvailable(newEmail)){ 
		    			 AccountUser user = AccountUser.findByPassword(apikey, Security.connected(), currentPassword);
		    			 if(user != null){
		    				 if(user.isAdmin()){
		    					status = user.requestEmailReset();
		    					if(status == 200){				
		    						MailSerializer mail = new MailSerializer(user.getProfile().getUsername(), newEmail,"","Dropifi.com Email Reset",user.getResetEmailMessage(newEmail));								
		    						ActorRef mailActor = actorOf(MailingActor.class).start(); 
		    						mailActor.tell(mail);   
		    						json.addProperty("error","A reset link has been sent to your email address. "+newEmail+"<br/> Click the link to reset your email");
		    					}else{
		    						json.addProperty("error","Oops, the request to reset your email failed. Try resetting your email address again");
		    					}
		    				 }else{
		    					 json.addProperty("error","Oops, you do not have the right to change the account email");
		    				 }
		    				}else{
		    					json.addProperty("error","Oops, the password you entered is not correct");
		    				}
		    		}else{
		    			json.addProperty("error","Oops, the email  address "+newEmail+" belongs to an existing account."); 
		    		}
	    		}else{
	    			json.addProperty("error","Oops, the email address "+newEmail+" is the same as your current account email"); 
	    		}
	    	}else{ 
	    		json.addProperty("error","Oops, your email address could not be change."); 
	    	}
    	}else{
    		json.addProperty("error","Oops, Complete all fields. They are required to change your email."); 
    	} 
    	json.addProperty("status", status);
    	renderJSON(json.toString()); 
    }
     
    public static void saveProfile(String name, String url,String country,String industry,String timezone, @Required String username, String phoneNumber){ 
    	
    	JsonObject json = new JsonObject();
    	String apikey = Security.connectedApikey();
    	int status =404;
    	
    	if(validation.hasErrors()){
    		json.addProperty("error","your full name is required");
    		json.addProperty("status", status);
        	renderJSON(json.toString());
    	}
    	
    	if(apikey !=null){
    		Company comp = Company.findByApikey(apikey); 
    		if(comp !=null ){
    			status = 200;
    			 
    			if(status == 200){
    				status = comp.updateProfilePhone(name, url, industry, country, timezone,phoneNumber);    				
    				if(AccountUser.updateFullName(username, apikey)>=1){
    					Security.setConnectedName(username);
    				}
    			}else{
    				status = 405; 
    				json.addProperty("error","could not save the profile");
    			}
    		}
    	}else{
    		json.addProperty("error","could not save the profile");
    	}
    	
    	json.addProperty("status", status);
    	renderJSON(json.toString());
    } 
    
    public static void saveSignup(@Required String username,@Required @Email String signup_email, @Required String domain,
    		@Required String signup_password, @Required String confirmPassword,String phoneNumber,String IPAddress, String plan,String location) {  
       try{
    	if(plan==null){ plan = "Free";   } 
    	flash.put("signup_email", signup_email);
		flash.put("domain",domain);
		flash.put("username", username);
		flash.put("phone", phoneNumber);
		
    	//ensure that the email and password are not empty
    	if(validation.hasErrors()){
    		//redirect to signup page
    		if(validation.hasError("email") || !validation.email(signup_email).ok)
    			flash.put("error", "Oops, the email address " +signup_email +" is not valid");  
    		
    		else if(validation.hasError("username"))
    			flash.put("error", "Oops, email address is required"); 
    		
    		else if(validation.hasError("signup_password"))
    			flash.put("error", "Oops, password is required");  
    		
    		validation.keep();
    		
    		//redirect to the signup page 
    		signup(plan); 
    		
    		//redirectToSignup("");
    	}else if(!signup_password.equals(confirmPassword)) {//check password match
    		flash.put("error", "Oops, password and confirm-passoword mismatch");      		 
    		validation.keep();
    		signup(plan);  		 
    	}
    	
    	String companyName = domain;
	   	domain = domain.trim().replace(" ", "").toLowerCase();
	   	//play.Logger.log4j.info("String inviteCode");   
	    //check if email is available
	   	domain = Company.replaceSpecialChar(domain);
	   	if(!Company.isValidDomain(domain)){
	   		flash.put("error", "Oops, your company name should not contain special characters. Only letters and numbers are allowed. Eg retailtower");	  
    		validation.keep();
    		//redirect to the signup page 
    		signup(plan);
		}else	if(Company.isEmailAvailable(signup_email)){
    		flash.put("error", "Oops, the email  address "+signup_email+" belongs to an existing account.");	  
    		validation.keep();
    		//redirect to the signup page
    		signup(plan);
    		
    		  //check if domain is available
    	}else if(Company.isDomainAvailable(domain) || Company.usedDomains().contains(domain.toLowerCase())){
    		flash.put("error", "Ooops, the company name "+domain+" belongs to an existing account.");    
    		validation.keep();
    		signup(plan);
    	}else{ 
    		
    		String timezone = request.params.get("timezone");
    		
    		//all validations meet. create the company and redirect to the dashboard
	    	//all sensitive data should be encrypted 
	    	
	    	//create an account using the specified email and password. 
	    	//Returns null if an account with the same email or domain exist
	    	CompanyProfile profile = new CompanyProfile(companyName, "", timezone, "", "",phoneNumber);
	    	Company company = new Company(signup_email,domain,username, signup_password,AccountType.Free,profile);
	    	
	    	EcomBlogPlugin ecomBlogPlugin = new EcomBlogPlugin(domain, "", "", EcomBlogType.Dropifi);	    	
	    	company.setEcomBlogPlugin(ecomBlogPlugin);
	    	
	    	try{	    	
	    		company = company.createCompany();  
	    	}catch(Exception e){
	    		flash.put("error", "Oops, the account could not be created.");
	    		validation.keep();
	    		play.Logger.log4j.info(e.getMessage(),e);
	    		//redirect to the signup page
	    		signup(plan); 
	    	}
	    	
	    	//redirect to signup page if the status is not 200 (unsuccessful creation of account)    	 
	    	if(company == null){ 
	    		flash.put("error", "Oops, the account can not be created.");
	    		validation.keep();
	    		//redirect to the signup page
	    		signup(plan);  		    		 
	    	}else{ 	    		 
	    		//play.Logger.log4j.info("New customer: "+signup_email+" added on"+ new Date());
		    	//account creation successful. authenticate user
	 
	    	    try{
		    	    MailSerializer mail = new MailSerializer(username,signup_email,"","Welcome to Dropifi",company.getSignUpMsg());								
					ActorRef mailActor = actorOf(MailingActor.class).start();     
					mailActor.tell(mail);
	    	    }catch(Exception e){
	    	    	play.Logger.log4j.info(e.getMessage(),e);
	    	    }
	    	    
	    	    //track user login
		        try{
		    	    EventTrackSerializer event = new EventTrackSerializer(username,signup_email,"SIGNUP",IPAddress,signup_email,UserType.Admin.name(),true);
		    	    event.setWidgetType(EcomBlogType.Dropifi.name());
		    	    event.setAccountType(company.getAccountType().name());
		    	    event.setCreated(company.getCreated().toString());
		    	    event.location = location;
		    	    event.setPhone(phoneNumber);
					ActorRef eventActor = actorOf(EventTrackActor.class).start();   
					eventActor.tell(event); 
	    	    }catch(Exception e){
	    	    	play.Logger.log4j.info(e.getMessage(),e);
	    	    } 
		        
		        //send a welcome email to user 	   
		        try{
	    	    	company.sendWelcomeMsg(EcomBlogType.Dropifi);	
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
		        
		        setLoginSession(company,IPAddress,"SIGNUP",location);
		        session.put("newAccount", true);
	    	  
		        //set the to the cookie
		        String cookieKey = CacheKey.genCookiekey(CacheKey.Secure,company.getDomain(), "Plan");
		        setCookie(cookieKey,company.getApikey(), plan, "5min");
		        
		        onAuthenticated("", "","");			    	    	    		 	    		 
	    	}
	    	 
    	//profile();
    	}
	    
       }catch (Exception e){
    	   	flash.put("error", "Oops, all fields are required.");   
    	   	play.Logger.log4j.info(e.getMessage(),e);
   			validation.keep();
   			signup(plan); 
       }
    }
    
    public static void loginShopify(Company company, String timestamp,String IPAddress,String location){
 		
    	if(company!=null){  
			//add the javascript file
			company.getEcomBlogPlugin().addShopifyScriptTag(timestamp);	    		
			Shopify.removeFromSession(); 
			
			setLoginSession(company,IPAddress,"LOGIN",location); 
			Company.clearSubscriptionFromCache(company.getApikey());
			try{
				long countMsg = company.countMessages(); 
				play.Logger.log4j.info("Users: "+countMsg);
				if(countMsg <= 0){
					company.sendWelcomeMsg(EcomBlogType.Shopify);  
				} 
			}catch(Exception e){}
			
			//redirect the owner to the widget customization page
			redirect("/"+company.getDomain()+"/admin/inbox");    		 
		}    	
    }

    /**
     * Authentication parameters: provides information about the current login user
     * @param company
     */
    @Util
    protected static void setLoginSession(Company company,String IPAddress,String eventType,String location){ 
    	//redirect to the profile page 		    		
    	if(company!=null){    		
    		session.clear(); 
    		
			session.put("username", company.getEmail()); 
			session.put("domain", company.getDomain());
		    session.put("apikey", company.getApikey());	    		
		    session.put("companyId",String.valueOf(company.getId()));
		    session.put("userType",UserType.Admin.name());
		    session.put("created",company.getCreated());
		    session.put("loginName", company.getProfile().getName()); 
		    
		    EcomBlogPlugin ecom = company.getEcomBlogPlugin();
		    EcomBlogType ecomType = EcomBlogType.Dropifi;
		    if(ecom!=null){
		    	session.put("ecomBlogPlugin", ecom.getEb_pluginType().name());
		    	ecomType =  ecom.getEb_pluginType();
		    }else{
		    	session.put("ecomBlogPlugin", EcomBlogType.Dropifi.name());	    	
		    }
 	   
		    //track user login
	        try{
	    	    EventTrackSerializer event = new EventTrackSerializer(company.getProfile().getName(), company.getEmail(),eventType,
	    	    		IPAddress!=null?IPAddress:"",company.getEmail(),UserType.Admin.name(),true);
	    	    event.setWidgetType(ecomType.name());
	    	    event.setAccountType(company.getAccountType().name());
	    	    event.setUsername(company.getDomain());
	    	    event.setCreated(new Date(company.getCreated().getTime()).toString());
	    	    event.setUrl(company.getProfile().getUrl());
	    	    event.location = location;
	    	    event.setPhone(company.getProfile().getPhone());
				ActorRef eventActor = actorOf(EventTrackActor.class).start();   
				eventActor.tell(event); 
    	    }catch(Exception e){
    	    	
    	    } 	
	        
	        Company.clearSubscriptionFromCache(company.getApikey());	
    	}
    }
    
    public static void subscribeToNewsletter(String email,String name, String location,String browser){
    	//track user login
    	 JsonObject json = new JsonObject(); 
        try{
    	    EventTrackSerializer event = new EventTrackSerializer(email,"NEWSLETTER",name,location,browser);
			ActorRef eventActor = actorOf(EventTrackActor.class).start();   
			eventActor.tell(event); 
	    }catch(Exception e){
	    	play.Logger.log4j.info(e.getMessage(),e); 
	    }
        json.addProperty("status", 200);
        renderJSON(json.toString());
    }
    
    /**
     * Authentication parameters: provides information about the current login user
     * @param credentials
     */  
    @Util
    protected static void setLoginSession(Map<String,String> credentials){ 
    	//redirect to the profile page 		    		
    	if(credentials!=null){ 
    		session.clear(); 
			
    		
			// Mark user as connected				
	        session.put("username", credentials.get("userEmail"));       
	        session.put("domain",credentials.get("domain"));
	        session.put("apikey",credentials.get("apikey")); 
	        session.put("companyId",credentials.get("companyId")); 
	        session.put("userType",credentials.get("userType"));	        
	        session.put("created",credentials.get("created"));
	        session.put("loginName", credentials.get("username"));   
	        
	        EcomBlogType ecomType = EcomBlogType.getValue(credentials.get("ecomBlogPlugin"));	       
		    session.put("ecomBlogPlugin", ecomType.name());
		   	      
		    //track user login 
	        try{
	    	    EventTrackSerializer event = new EventTrackSerializer(credentials.get("username"),credentials.get("userEmail"),"LOGIN",
	    	    		credentials.get("IPAddress"),credentials.get("userEmail"),credentials.get("userType"),true);
	    	    
	    	    event.setWidgetType(ecomType.name());
	    	    event.setAccountType(credentials.get("accountType"));
	    	    event.setUsername(credentials.get("domain"));
	    	    event.setCreated(credentials.get("created"));
	    	    event.setUrl(credentials.get("url"));
	    	    event.location =credentials.get("location");
	    	    event.setPhone(credentials.get("phone"));
	    	    
				ActorRef eventActor = actorOf(EventTrackActor.class).start();   
				eventActor.tell(event); 
    	    }catch(Exception e){
    	    	play.Logger.log4j.error(e.getMessage(),e);
    	    }	 
	        
		    Company.clearSubscriptionFromCache(credentials.get("apikey"));

    	}
    }
    
}
    
