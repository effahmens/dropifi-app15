package controllers;

import static akka.actor.Actors.actorOf;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import job.process.MailingActor;
import models.CustomerProfile;
import models.InvitationCode;
import akka.actor.ActorRef;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import play.Play;
import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.With;
import resources.ContactPullTask;
import resources.DropifiTools;
import view_serializers.MailSerializer;
@With(Compress.class)
public class SystemAdmin extends Controller{
	
	public static void index(){
		ForceSSL.redirectToHttps();
		render();
	}
	
	public static void invite_code(){
		
		ForceSSL.redirectToHttps();
		
		String fpassword = flash.get("whenwekill");
		String fusername = flash.get("whenwedie");
						
		if(fpassword == null && fusername==null){
			
			flash.put("whenwekill", "women");
			flash.put("whenwedie", "asmen");
			unauthorized("Dropifi - Invitation Code Generator");
		} else{

			flash.remove("whenwekill");
			flash.remove("whenwedie"); 
		
			String username = request.user;
			String password = request.password; 
			
			String adminName = DropifiTools.adminName;
			String adminPassword = DropifiTools.adminPassword; 
			
			if(username.compareTo(adminName)==0 && password.compareTo(adminPassword)==0){
				//report admin login by sending email and sms
				String mailName = DropifiTools.mailName; Play.configuration.getProperty("dropifi.mail.username");
				String mailEmail = DropifiTools.mailEmail; Play.configuration.getProperty("dropifi.mail.email");
				
				MailSerializer mail = new MailSerializer(mailName,mailEmail,"","Invite Code",
						DropifiTools.reportAdminLogin(request.url,"Invite code", request.remoteAddress));								
				ActorRef mailActor = actorOf(MailingActor.class).start();   
				mailActor.tell(mail); 
			}else{ 
				invite_code(); 
			}
		}
		
		//Application.index();
		render(); 
	}
	
	public static void generateCode(@Required String callback){
		JsonObject json = new JsonObject();
		
		 String accessKey = Play.configuration.getProperty("invite.accesskey");
		 if(callback != null && callback.equals(accessKey)){
			 
			String code = DropifiTools.generateCode(8);
 
			InvitationCode incode = new InvitationCode(false, code); 
			int status = incode.saveInvitation();
			json.addProperty("status", status);
			json.addProperty("code", code);  
			
		}else{
			json.addProperty("status", 707);
			json.addProperty("error", "unknown access key");
		}
		 
		renderJSON(json.toString());
	}
	
	public static void generateMultiCode(@Required String callback, Integer number){
		JsonObject json = new JsonObject();
		
		Gson gson = new Gson();
		List<String> codes = new LinkedList<String>();
		int status =200;
		String accessKey = Play.configuration.getProperty("invite.accesskey");
		if(callback != null && callback.equals(accessKey) && validation.required(number).ok){
			
			for(int i=0;i<number;i++){
				Timestamp time = new Timestamp(new Date().getTime()); 
				String code = Codec.hexSHA1(String.valueOf(time));
				
				if(code.length()>7){
					code = code.substring(0, 7); 
				}
				
				InvitationCode incode = new InvitationCode(false, code); 
				 				 
				if(incode.saveInvitation() ==200){
					//jArray.add(gson.toJsonTree(code));
					codes.add(code); 
				}
			}		   
			json.add("codes", gson.toJsonTree(codes)); 
			
		}else{
			status =707;
			json.addProperty("error", "unknown access key");
		}
		json.addProperty("status", status);
		 
		renderJSON(json.toString());
	}
	
	public static void updateContactProfile(@Required String callback){
		JsonObject json = new JsonObject();
		int status = 501;
		String accessKey = Play.configuration.getProperty("invite.accesskey");
		if(callback != null && callback.equals(accessKey) ){
			List<CustomerProfile> customers = CustomerProfile.findAll();
			if(customers != null){ 				
				 
				int size = customers.size();
				play.Logger.log4j.info("Customer size: "+ size);
				
				if(size>0){					
					//create an instance of the ContactPullTask to execute each customer on a separate thread 					
					ContactPullTask pullContact = new ContactPullTask(customers, 0, size); 
					
					ForkJoinPool mainPool = new ForkJoinPool();			
					mainPool.invoke(pullContact);
					status = 200;
					json.addProperty("numOfContacts", size); 
				}
			}
		}
		json.addProperty("status", status);
		
		renderJSON(json.toString()); 

	}
	
}
