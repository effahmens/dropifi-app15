package controllers;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import models.AccountUser;
import models.Company;
import models.addons.AddonSubscription;
import play.Play;
import play.mvc.*;
import play.cache.CacheFor;
import play.data.validation.*;
import play.libs.*;
import play.utils.*;
import resources.Check;
import resources.DropifiTools;
import resources.UserType;
import resources.addons.AddonStatus;

@With(Compress.class)
public class Secure extends Controller {

	@Before
	public static void onload(){
		ForceSSL.secureDevMode();
	}
	
    @Before(unless={"login", "authenticate", "logout","signup","forgot_password","reset","reset_user_password","create_new_password"})
    static void checkAccess() throws Throwable{
        
    	//Authentication
        if(!session.contains("username")) {
            flash.put("url", "GET".equals(request.method) ? request.url : "/"); 
            //seems a good default
            //String domain = Security.connectedDomain(); 
            login("");
        } 
        
        // Checks
        Check check = getActionAnnotation(Check.class);
        if(check != null) {
            check(check);
        }
        check = getControllerInheritedAnnotation(Check.class);
        if(check != null) {
            check(check);
        }
    }
    
    private static void check(Check check) throws Throwable {
        for(String profile : check.value()) {
            boolean hasProfile = (Boolean)Security.invoke("check", profile);
            
            if(!hasProfile) {
                Security.invoke("onCheckFailed", profile);
            }
        }
    }
       
    // ~~~ Login
    //@CacheFor("1d")
    public static void login(String url,String... args)throws Throwable {
    	//ForceSSL.redirectToHttps();  
    	
    	if(url==null)url="";
    	
		//check that the user is already login. if true direct user to the dashboard
		String apikey = Security.connectedApikey();  
        if(apikey != null && !apikey.trim().isEmpty()){        	
        	Security.invoke("onAuthenticated",url,"","");
        }
        
        String useremail = Security.getCookie("rememberme", Crypto.sign("rememberlogin"));  
        if(useremail!=null){
        	flash.put("useremail", useremail);
            flash.put("remember", true); 
            flash.keep();
        }else{
        	useremail="";
        	flash.put("remember", false); 
        } 
        String error=null;
	    if(args!=null){
	    	try{
	    		useremail = args.length>1?args[0]:null; 
	    	}catch(Exception e){
	    		play.Logger.log4j.info(e.getMessage(),e);
	    	}
	    	
	    	try{
	    		error = args.length>2?args[1]:null; 
	    	}catch(Exception e){
	    		play.Logger.log4j.info(e.getMessage(),e);
	    	}
	    }
	    
	    //flash.keep("url");
	    renderArgs.put("page", "login");  
	    render(url,useremail,error);
    }

    public static void forgot_password(){
    	renderArgs.put("page", "reset_password"); 
   	 	render();
    }
    
    public static void reset_user_password(@Required String email,@Required String pwd_reset_token,@Required Boolean is_reset){
    	 
    	if(validation.hasErrors()){
    		flash.error("Oops, you tried resetting your password from an invalid link");
    		flash.keep();
    		forgot_password();
    	} 
    	
    	renderArgs.put("page", "reset_user_password"); 
    	render(pwd_reset_token,email,is_reset);     	
    }
    
    public static void create_new_password(@Required String password,@Required String confirmPassword, 
    		@Required String pwd_reset_token, @Required String email, @Required Boolean is_reset,String IPAddress,String location){
    	
    	 if(validation.hasErrors()){
    		 if(validation.hasError("password"))
     			flash.put("error", "Oops, new password is required"); 
     		
     		else if(validation.hasError("confirmPassword"))
     			flash.put("error", "Oops, confirm password is required");  
     		
     		validation.keep(); 
     		
     		flash.keep();
     		reset_user_password(email, pwd_reset_token, is_reset);
    	 }  	
    	 
    	if(password.compareTo(confirmPassword) !=0){
    		flash.put("error", "Oops, new and confirm password do not match"); 
    		 validation.keep();  
    		 reset_user_password(email, pwd_reset_token, is_reset);
    	}
    	 
    	 AccountUser user = AccountUser.findByResetKey(pwd_reset_token, email);
    	 
    	 if(user==null){
    		 flash.put("error", "Oops, the link you followed to reset the password is not a valid link. " +
    		 		"<a href='/forgot_password' style='color:black;text-decoration:underline;'>Click here to reset password</a>"); 
    		 validation.keep();
    		 flash.keep();
    		 reset_user_password(email, pwd_reset_token, is_reset); 
    	 }
    	 
    	 int status = user.changePassword(password);
    	 if(status != 200){
    		 flash.put("error","Oops, unable to change password. Try again"); 
     		 validation.keep(); 
     		 flash.keep();
     		 reset_user_password(email, pwd_reset_token, is_reset);
    	 }
    	 
    	 try {
    		 //log the user in
			authenticate(email, password, false,IPAddress,"",location);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			try {
				login("");
			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				Application.index();
			}
		}
    }
    
    /**
	 * This method is called during the authentication process. This is where you check if
	 * the user is allowed to log in into the system. This is the actual authentication process
	 * against a third party system (most of the time a DB).
	 * 
	 */	
    public static void authenticate(@Required @Email String useremail, @Required String password,boolean remember,String IPAddress,String url,String location) throws Throwable {
    	
        // Check tokens
        Integer allowed = 501;           
        String error=null;
        try {        	
        	String timezone = request.params.get("timezone");
        	
        	IPAddress =IPAddress!=null?IPAddress:"";
        	location =location!=null?location:"";
        	timezone =timezone!=null?timezone:"";
        	
        	allowed = (Integer)Security.invoke("authenticate", useremail,password,IPAddress,location,timezone);          
        } catch (Exception e ) {
        	play.Logger.log4j.info(e.getMessage(),e);
        }
        
        try{        
	        if(validation.hasErrors() || allowed!=200 ){
	            //flash.keep("url");
	            if(allowed ==201){ 
	            	error ="Oops, invalid account password";
	            	//renderArgs.put("error", "Oops, invalid account password"); 
	            }else if(allowed==203){
	            	error ="Oops, your login credentials have been deactiavted. Contact your dropifi account manager";
	            }else{
	            	error ="Oops, there is no account associated with this email";
	            	//renderArgs.put("error", "Oops, there is no account associated with this email"); 
	            }
	            
	            flash.error(error);
	            flash.keep();
	            
	            login(url,useremail);
	            
	            //renderTemplate("Secure/login.html", url,useremail,error);
	        }
	        
	        // Remember if needed
	        if(remember) {
	        	Security.setCookie("rememberme", Crypto.sign("rememberlogin"), useremail, "15d");
	            //response.setCookie("rememberme", Crypto.sign(useremail) + "-" + useremail, "30d");
	        }else{ //or remove remember if not needed
	        	response.removeCookie("rememberme");
	        }
	        
	        if(url==null) url=""; 
	        
	        if(error==null)error="";
	        	flash.error(error);
	        //Redirect to the original URL (or /)
	        Security.invoke("onAuthenticated",url,error,"");
        
        }catch(Exception e){
        	play.Logger.log4j.info(e.getMessage(),e); 
        	if(error==null)error="";
        		flash.error(error);
        	
        	login(url,useremail);
        	//renderTemplate("Secure/login.html", url,useremail,error="");
        }
    }
    
    public static void reset(@Required String username){ 
    	if(validation.hasErrors()){
    		flash.error("Oops, email address is required");
    		params.flash();
    		forgot_password(); 
    	}
    	Integer allowed =501;
    	
    	try {
			allowed = (Integer)Security.invoke("reset", username);
		} catch (Throwable e) {
			// TODO Auto-generated catch block			 
		};
		
		if(allowed !=200){
			flash.error("Oops, the email address you specified does not exit");
    		params.flash();
    		forgot_password(); 
		} 
		
		flash.put("success", "Success. Please check your email for a link to reset your password");
		forgot_password();	 
    }
    
    @Util
    public static void accountActivation(@Required String email,@Required String code){ 
    }
   
   @Util
   public static void securelogin(String username, String domain,boolean remember) throws Throwable{
    	//Mark user as connected
        session.put("username", username); 
        session.put("domain", domain);
        
        // Remember if needed
        if(remember) {
        	Security.setCookie("rememberme", Crypto.sign("rememberlogin"), username, "15d");
            //response.setCookie("rememberme", Crypto.sign(username) + "-" + username, "30d");
        }
        
        // Redirect to the original URL (or /)
        redirectToOriginalURL();
    }
 
    
    public static void logout() throws Throwable{  
    	
       if(flash.get("secure.logout")!=null)
    	   flash.success(flash.get("secure.logout"));  
       else if(flash.get("secure.error")!=null)
    	   flash.error(flash.get("secure.error")); 
       
       if(Security.connectedPrestashop()!=null && Security.connectedPrestashop().equals("true"))
    	   PrestaShop.back_office();
       
       String socialUID = AddonSubscription.getUID(Security.connectedApikey(),DropifiTools.SOCIAL_REVIEW, AddonStatus.Installed);
       if(socialUID!=null){
    	   flash.put("uid",socialUID);
       }
       
       session.clear();
       redirect("/");
    }
 
    // ~~~ Utils 
    @Util
    static void redirectToOriginalURL() throws Throwable {
        Security.invoke("onAuthenticated","","","");
        
       /* String url = flash.get("url");
        if(url == null) {
            url = "/";
        }
        redirect(url);*/
    }

    public static class Security extends Controller {

        /**
         * @Deprecated
         * 
         * @param username
         * @param password
         * @return
         */
    	
        /*
         @Deprecated
         static boolean authentify(String username,String domain,String password) {
            throw new UnsupportedOperationException();
        }*/

        /**
         * This method is called during the authentication process. This is where you check if
         * the user is allowed to log in into the system. This is the actual authentication process
         * against a third party system (most of the time a DB).
         *
         * @param username
         * @param password
         * @return true if the authentication process succeeded
         */
        static int authenticate(String username,String password,String IPAddress,String location,String timezone) {            
        	return -1; 
        }
        
        static int reset(String email){
        	return -1;
        }

        /**
         * This method checks that a profile is allowed to view this page/method. This method is called prior
         * to the method's controller annotated with the @Check method. 
         *
         * @param profile
         * @return true if you are allowed to execute this controller method.
         */
        static boolean check(String profile) {
            return true;
        }

        /**
         * This method returns the current connected user email
         * @return
         */
        @Util
        static String connected() {
            return session.get("username");
        }
        
        @Util
        static void setConnected(String email) {
            session.put("username",email);
        }
        
        
        /**
         * This method returns the current connected user name
         * @return
         */
        @Util
        static String connectedName() { 
        	String name = session.get("loginName");
            return name!=null?name:"";
        }
        
        @Util
        static void setConnectedName(String loginName) { 
        	  session.put("loginName",loginName);
        }
        
        /**
         * Set a value to indicate that the user login through prestashop back office demo
         * @param prestashop
         */
        @Util
        static void setPrestashop(String prestashop) {
            session.put("prestashop",prestashop);
        }
        /**
         * This method return the apikey of the current connected company
         * @return
         */
        @Util
        static String connectedApikey(){
        	return session.get("apikey"); 
        }
        
        @Util
        static String connectedDomain(){
        	return session.get("domain");
        }
        
        @Util
        static UserType connectedUserType(){
        	String user = session.get("userType");
        	if(user !=null){
	        	if(user.equalsIgnoreCase(UserType.Admin.name()))
	        		return UserType.Admin;
	        	else
	        		return UserType.Agent;
        	}
        	return null;
        }
        
        @Util
        static Long connectedCompanyId(){
        	try{
        		return Long.parseLong(session.get("companyId"));
        	}catch(Exception e){
        		return null;
        	}
        }
        
        @Util
        static String connectedPlugin(){
        	return session.get("ecomBlogPlugin");
        }
        
        @Util
        static String connectedPrestashop(){
        	return session.get("prestashop");
        }
        
        /**
         * This method returns the date the user account was created
         * @return
         */
        @Util
        static Timestamp connectedCreated(){
        	try{
        		//Date date = new Date();         		
        		return new Timestamp(Long.valueOf(session.get("created")));
        	}catch(Exception e){        		 
        		return null;
        	}
        }
     
        @Util
        static void setCookie(String name,String privateKey, String value, String duration) {
           try{
        	// Setting cookie
        	if(privateKey==null || privateKey.length()<=15)
        		return;
        	
        	privateKey = (privateKey.length()==16)?privateKey: privateKey.substring(0, 16);
            response.setCookie(name, Crypto.encryptAES(value, privateKey), duration);
           }catch(Exception e){
        	   play.Logger.log4j.info(e.getMessage(),e);
           }
        }

        @Util
        static String getCookie(String key,String privateKey) {
            try{
        	// retrieving cookie by key
        	if(privateKey==null ||privateKey.length()<16)
        		return null;
        	
        	privateKey = (privateKey.length()==16)?privateKey: privateKey.substring(0, 16);
        	Http.Cookie ck = request.cookies.get(key); 
            String value = ck!=null?Crypto.decryptAES(ck.value, privateKey):null; 
             return value;
            }catch(Exception e){ 
            	play.Logger.log4j.info(e.getMessage(),e);
            }
            return null;
        }
        
        @Util
        static void removeCookie(String key) {
        	try{
        		request.cookies.remove(key);
        		response.cookies.remove(key);
        	}catch(Exception e){
        		play.Logger.log4j.info(e.getMessage(),e);
        	}
        }
   
        
        /**
         * Indicate if a user is currently connected
         * @return  true if the user is connected
         */
        static boolean isConnected() {
            return session.contains("username");
        }

        /**
         * This method is called after a successful authentication.
         * You need to override this method if you with to perform specific actions (eg. Record the time the user signed in)
         */
        static void onAuthenticated(String url,String error,String prestashop){ 
        	
        }

         /**
         * This method is called before a user tries to sign off.
         * You need to override this method if you wish to perform specific actions (eg. Record the name of the user who signed off)
         */
        static void onDisconnect() {
        }

         /**
         * This method is called after a successful sign off.
         * You need to override this method if you wish to perform specific actions (eg. Record the time the user signed off)
         */
        static void onDisconnected() {
        }

        /**
         * This method is called if a check does not succeed. By default it shows the not allowed page (the controller forbidden method).
         * @param profile
         */
        static void onCheckFailed(String profile) {
            forbidden();
        }
        
        static void signup(String username,String domain, String password){	
        } 
        
        public static Object invoke(String m, Object... args) throws Throwable {
            try {
                return Java.invokeChildOrStatic(Security.class, m, args);        
            } catch(InvocationTargetException e) {
                throw e.getTargetException();
            }
        }

    }

}
