package controllers;

import static akka.actor.Actors.actorOf;

import java.util.Date;
import java.util.Map;

import com.shopifyOAuthen.api.resources.Shop;
import com.shopifyOAuthen.connectors.ShopifyAPIConnect;

import job.process.MailingActor;
import job.process.WidgetTrackerActor;
import models.Company; 
import models.CompanyProfile;
import models.EcomBlogPlugin;
import akka.actor.ActorRef;
import api.tictail.TictailClient;
import api.tictail.TictailSerializer;
import play.cache.Cache;
import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.UserType;
import resources.WidgetTrackerSerializer;
import view_serializers.MailSerializer;

@With(Compress.class)
public class Tictail extends Controller{

	public static void oauth(@Required String code){ 
		//ForceSSL.redirectToHttps();
		if(!validation.hasErrors()){ 
			session.put("tictail_code", code); 
			//String email = "";
			String tictailKey="";
			TictailClient client = new TictailClient(code);
			TictailSerializer tictail = client.GetClient(); 
			if(tictail!=null && tictail.getAccessToken()!=null && tictail.getStore()!=null){
				tictail.setCode(code);
				tictailKey = addToCache(tictail); 
				
				Company company = Company.findByEcomBlog(tictail.getStore().getSubdomain(), EcomBlogType.Tictail);
				if(company!=null){ 
					
					//play.Logger.log4j.info("Subdomain: "+tictail.getStore().getSubdomain());
					EcomBlogPlugin plugin = new EcomBlogPlugin(tictail.getStore().getEmail(), tictail.getStore().getSubdomain(), tictail.getAccessToken(), tictail.getStore().getId(), EcomBlogType.Tictail);
					CompanyProfile profile = new CompanyProfile(tictail.getStore().getName(), tictail.getStore().getUrl(),tictail.getStore().getVat().getRegion(),null, tictail.getStore().getCountry(),null);					
					
					//make changes the account
					company.setProfile(profile); 
					company.setEcomBlogPlugin(plugin); 
					company = company.save(); 
					 
					//redirect the owner to the inbox page
					//if(company.getDomain().equalsIgnoreCase(plugin.getDomain())){ 
						company.subscribeEcomPluginToPaidPlan(EcomBlogType.Tictail);
						try{
							WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),tictail.getStore().getUrl(),tictail.getStore().getUrl());
							ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
							trackActor.tell(track);  
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						}
						
						//clear the seesion 		
						CacheKey.delete(tictailKey);
						session.clear();
						
						//set the session
						Security.setLoginSession(company,"","LOGIN","");    
						//Dashboard.index(company.getDomain()); 
						Widgets.index(company.getDomain(), false);
						
					//}
				} 
				//email = tictail.getStore().getEmail();
				//show_new(tictailKey, email,null);
				createDirectAccount(tictail,null);
				
			}else{
				redirect(client.getRedirectURL());
			}
		}
		Apps.page_not_found(""); 
	}
	
	public static void index(String tictailKey,String email, boolean isLogin,String error){  
		if(isLogin==false){
			flash.put("new_account", true); 
			flash.put("new_useremail", email);
		}else{ 
			flash.put("user_email", email);
			flash.put("existing_account", true); 
		} 
		
		flash.put("error", error);
		
		if(tictailKey == null || tictailKey.trim().isEmpty()) {		
			Security.signup("");
		}  
		render(tictailKey, email,isLogin);	 
	}
	
	public static void show_new(String tictailKey,String email,String error){ 
		//ForceSSL.redirectToHttps(); 
		index(tictailKey, email,false,error);
	}
	
	public static void show_existing(String tictailKey,String email,String error){ 
		//ForceSSL.redirectToHttps(); 
		index(tictailKey, email,true,error);
	}
	
	@Util
	private static void createDirectAccount(TictailSerializer tictail,String location){ 
		EcomBlogPlugin plugin = new EcomBlogPlugin(tictail.getStore().getEmail(), tictail.getStore().getSubdomain(), tictail.getAccessToken(), tictail.getStore().getId(), EcomBlogType.Tictail);
		CompanyProfile profile = new CompanyProfile(tictail.getStore().getName(), tictail.getStore().getUrl(),tictail.getStore().getVat().getRegion(),null, tictail.getStore().getCountry(),null);					
		 
		//create a new account for the shop
		Company company = new Company(plugin,DropifiTools.generateCode(4),profile,tictail.getStore().getLoginEmail()).createCompany();
		if(company!=null && company.getId()!=null){   
    		try{
    			company.subscribeEcomPluginToPaidPlan(EcomBlogType.Tictail);
    			//send an initial message to the user			    			 
				company.sendWelcomeMsg(EcomBlogType.Tictail);  
			}catch(Exception e){
				play.Logger.log4j.error(e.getMessage(), e);
			}
    		
    		try{
				WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),tictail.getStore().getUrl(),tictail.getStore().getUrl());
				ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
				trackActor.tell(track);
			}catch(Exception e){
				play.Logger.log4j.error(e.getMessage(), e);
			}
    		
    		try{   					
				//send an email to the shop owner
	    	    MailSerializer mail = new MailSerializer(company.getProfile().getName(), company.getEmail(),"", "Welcome to Dropifi",company.getEcomBlogWelcomMessage());								
				ActorRef mailActor = actorOf(MailingActor.class).start();   
				mailActor.tell(mail);  
    	    }catch(Exception e){
    	    	play.Logger.log4j.error(e.getMessage(), e);
    	    }  
    		
    		session.clear(); 			    		
    		Security.setLoginSession(company,"","SIGNUP",location);  

    		//redirect the owner to the widget customization page
			redirect("/"+company.getDomain()+"/admin/widgets");
			  
		}
	}
	public static void createNewAccount(@Required String tictailKey, @Required String new_useremail, @Required String userpassword, @Required String confirmPassword,String location, String phoneNumber){
		 //play.Logger.log4j.info("key 1 "+tictailKey+" new_useremail: "+new_useremail);
		 
		if(validation.hasErrors()){ 
			 //play.Logger.log4j.info("Oops, email address and password are required");
			//flash.error("Oops, email address and password are required");
	   		show_new(tictailKey,new_useremail,"Oops, email address and password are required");
	   	}else if(!userpassword.equals(confirmPassword)) {//check password match  
	   		// play.Logger.log4j.info("Oops, password and re-passoword do not match");
	   		//flash.error("Oops, password and re-passoword do not match");
	   		show_new(tictailKey,new_useremail,"Oops, password and re-passoword do not match");	 
	   	}else if(Company.isEmailAvailable(new_useremail)){	
	   		//play.Logger.log4j.info("Oops, the email  address "+new_useremail+" belongs to an existing account.");
	   		//flash.error("Oops, the email  address "+new_useremail+" belongs to an existing account.");
	   		show_new(tictailKey,new_useremail,"Oops, the email  address "+new_useremail+" belongs to an existing account.");
	   	} 
		
		//play.Logger.log4j.info("key 2 "+tictailKey);
		TictailSerializer tictail = Cache.get(tictailKey, TictailSerializer.class);
		//play.Logger.log4j.info("tail object "+tictail);
		
		if(tictail!=null){
	    	//check if the account is not created					 		
			if(EcomBlogPlugin.hasEcomBlog(new_useremail,tictail.getStore().getSubdomain(),EcomBlogType.Tictail)){
				//flash.error("An account with the email address "+new_useremail+" and store id ("+tictail.getStore().getId()+") belongs to an existing account."); 		 
				show_new(tictailKey,new_useremail,"An account with the email address "+new_useremail+" and store id ("+tictail.getStore().getId()+") belongs to an existing account.");
			} 
			
			EcomBlogPlugin plugin = new EcomBlogPlugin(new_useremail, tictail.getStore().getSubdomain(), tictail.getAccessToken(), tictail.getStore().getId(), EcomBlogType.Tictail);
			CompanyProfile profile = new CompanyProfile(tictail.getStore().getName(), tictail.getStore().getUrl(),tictail.getStore().getVat().getRegion(),null, tictail.getStore().getCountry(),null);					
			
			if(Company.isDomainAvailable(plugin.getDomain())){
				//flash.error("Oops, your store id"+tictail.getStore().getId()+" is associated with a Dropifi account.");
				show_new(tictailKey,new_useremail,"Oops, your store id"+tictail.getStore().getId()+" is associated with a Dropifi account.");
			}
			
			//create a new account for the shop
			Company company = new Company(plugin, userpassword,profile); 
			company = company.createCompany();	
			if(company!=null && company.getId()!=null){   
	    		try{
	    			company.subscribeEcomPluginToPaidPlan(EcomBlogType.Tictail);
	    			//send an initial message to the user			    			 
					company.sendWelcomeMsg(EcomBlogType.Tictail);  
				}catch(Exception e){
					play.Logger.log4j.error(e.getMessage(), e);
				}
	    		
	    		try{
					WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),tictail.getStore().getUrl(),tictail.getStore().getUrl());
					ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
					trackActor.tell(track);  
				}catch(Exception e){
					play.Logger.log4j.error(e.getMessage(), e);
				}
	    		
	    		try{   					
					//send an email to the shop owner
		    	    MailSerializer mail = new MailSerializer(company.getProfile().getName(), company.getEmail(),"", "Welcome to Dropifi",company.getEcomBlogWelcomMessage());								
					ActorRef mailActor = actorOf(MailingActor.class).start();   
					mailActor.tell(mail); 
					
	    	    }catch(Exception e){
	    	    	play.Logger.log4j.error(e.getMessage(), e);
	    	    } 
	    		
	    		CacheKey.delete(tictailKey);
	    		session.clear(); 			    		
	    		Security.setLoginSession(company,"","SIGNUP",location);  

	    		//redirect the owner to the widget customization page
				redirect("/"+company.getDomain()+"/admin/widgets");
			} 					 
			//flash.error("Oops, the account can not be created.");
			show_new(tictailKey,new_useremail,"Oops, the account can not be created."); 
		}
		
		if(session.get("tictail_code")!=null){ 
			redirect(new TictailClient(session.get("tictail_code")).getRedirectURL());
		}
		
		Apps.page_not_found("");
	}
	
	public static void createExistingAccount(@Required String tictailKey,@Required String user_email, @Required String password,String location){
		 play.Logger.log4j.info("key 1 "+tictailKey+" new_useremail: "+user_email);
	   	if(validation.hasErrors()){    		
	   		//flash.put("error", "user email address and password required");  
	   		show_existing(tictailKey,user_email, "user email address and password required"); 
	   	}if(!validation.email(user_email).ok){
	   		//flash.put("error", "user email address required");
	   		show_existing(tictailKey,user_email, "user email address required");
	   	}else{
	   		
	    	//find the user information
	    	 Map<String,Object> userdata = Company.findByEmailPassword(user_email,UserType.Admin);
	    	    	 
	    	if(userdata == null){
	    		//flash.put("error", "Oops, there is no account associated with this email"); 
	    		show_existing(tictailKey,user_email, "Oops, there is no account associated with this email");
	    	}else{	    		
	    		Object ph = userdata.get("password");	
	    		String pass = ph!=null?(String)ph:null;
	    		String hPassword = Codec.hexMD5(password);
	    		
	    		if(!pass.equals(hPassword) || pass==null){ 
	    			//flash.put("error", "Oops, the password you entered is not correct"); 
	    			show_existing(tictailKey,user_email, "Oops, the password you entered is not correct");
		    	}else{ 
		    		Company company = (Company) userdata.get("company"); 
		    		try{ 
		    			TictailSerializer tictail = Cache.get(tictailKey, TictailSerializer.class); 
		    			if(tictail!=null){
							//EcomBlogType pluginType = company.getEcomBlogPlugin().getEb_pluginType(); 
							
							EcomBlogPlugin plugin = new EcomBlogPlugin(user_email, tictail.getStore().getSubdomain(), tictail.getAccessToken(), tictail.getStore().getId(), EcomBlogType.Tictail);
							CompanyProfile profile = new CompanyProfile(tictail.getStore().getName(), tictail.getStore().getUrl(),tictail.getStore().getVat().getRegion(),null, tictail.getStore().getCountry(),company.getProfile().getPhone());					
													
							try{
				    			//reset the EcomBlogPlugin
								company.setEcomBlogPlugin(plugin);									 
								company.setProfile(profile);
								company.save(); 
					    		try{
									WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),tictail.getStore().getUrl(),tictail.getStore().getUrl());
									ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
									trackActor.tell(track);  
								}catch(Exception e){
									play.Logger.log4j.error(e.getMessage(), e);
								}  
					    		
					    		company.subscribeEcomPluginToPaidPlan(EcomBlogType.Tictail);
					    		
					    		CacheKey.delete(tictailKey);				    		
					    		session.clear();
					    		
					    		company.loginSubscription();
					    		Security.setLoginSession(company,"","LOGIN",location);
					    		 	
					    		//redirect the owner to the widget customization page
								redirect("/"+company.getDomain()+"/admin/widgets"); 
	
				    		}catch(Exception e){    			
				    			//unable to create the account 			    			
				    			//flash.put("error" , "An error occurred while adding dropifi contact widget to your store. Try creating the account again");    
				    			show_existing(tictailKey,user_email, "An error occurred while adding dropifi contact widget to your store. Try creating the account again");
				    		} 
						} 
		    		}catch(Exception e){
		    			play.Logger.log4j.error(e.getMessage(),e);
		    		}
	    		}
	    	}    	
   	}
   	
   	if(session.get("tictail_code")!=null){ 
			redirect(new TictailClient(session.get("tictail_code")).getRedirectURL());
		} 
   	Apps.page_not_found("");
	}

	 
	@Util
    protected static String addToCache(TictailSerializer tictail){
		String key = CacheKey.genCacheKey(tictail.getAccessToken(), CacheKey.Tictail, tictail.getStore().getId());
		CacheKey.set(key, tictail);
		return key;
    }
	
}
