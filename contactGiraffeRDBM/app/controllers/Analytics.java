package controllers;

import play.*;
import play.data.validation.Required;
import play.mvc.*;
import resources.CacheKey;
import resources.EcomBlogType;
import resources.helper.ServiceSerializer;

import java.util.*;

import models.*;
import models.dsubscription.SubServiceName;

@With(Compress.class)
public class Analytics extends Controller {
    @Before
    static void setArgs(String domain){
    	ForceSSL.redirectToHttps();
    	
        String apikey = Security.connectedApikey();	
		String foundApikey = Company.findApikey(apikey);
		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0){   
			 renderArgs.put("page","analytics");
		     renderArgs.put("title","Business Insights");
 			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		     //play.Logger.log4j.info(foundApikey);
		}else{
			//clear the session
	    	session.clear();
	    	//response.removeCookie("rememberme");			
			try {
				Secure.login("");
			} catch (Throwable e) {
				Application.index(); 
			}
		} 
		
    }
        //messages
    public static void index(String domain) {    
    	 	
    	String connected = Security.connectedDomain(); 
    	domain =domain!=null? domain.trim():"";
		if(!connected.equalsIgnoreCase(domain)){			
			index(connected);
		}
		String apikey = Security.connectedApikey();	
    	ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Analytics, SubServiceName.AnalyticsPage);
   
    	if(service!=null && !service.hasExceeded){ 
	        renderArgs.put("analytics_page","messages"); 
	        String connectedUsername = Security.connected();
	        String userType = Security.connectedUserType().name(); 
	        String ecomBlogPlugin=Security.connectedPlugin();
	        
	        //check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
			render(userType, domain, connectedUsername,hasWidget,ecomBlogPlugin); 
		}

    	Analytics.contacts(domain);
    }
    
    public static void contacts(String domain) {
    	String connected = Security.connectedDomain(); 
    	domain =domain!=null? domain.trim():"";
		if(!connected.equalsIgnoreCase(domain)){			
			contacts(connected);
		}
		
        renderArgs.put("analytics_page","contacts");
        String apikey = Security.connectedApikey();	 
        String connectedUsername = Security.connected();
        String userType = Security.connectedUserType().name(); 
        String ecomBlogPlugin=Security.connectedPlugin();
        
        //check if the widget is installed on the site of the user
		boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.valueOf(ecomBlogPlugin));
		ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Analytics, SubServiceName.AnalyticsPage);
        
		render(userType, domain, connectedUsername,hasWidget,ecomBlogPlugin,service); 
    }
        
    public static void sentiments(String domain) {
    	String connected = Security.connectedDomain(); 
    	domain =domain!=null? domain.trim():"";
		if(!connected.equalsIgnoreCase(domain)){			
			sentiments(connected);
		}
		
    	String apikey = Security.connectedApikey();	 
    	ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Analytics, SubServiceName.AnalyticsPage);
		
		if(service!=null && !service.hasExceeded){
	    	renderArgs.put("analytics_page","sentiments");
	        
	        String connectedUsername = Security.connected();
	        String userType = Security.connectedUserType().name(); 
	        String ecomBlogPlugin=Security.connectedPlugin();
	        
	        //check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.valueOf(ecomBlogPlugin)); 
			render(userType, domain, connectedUsername,hasWidget,ecomBlogPlugin,service);
		}
		
		Analytics.contacts(domain);
    }
    
    public static void agents(String domain) {
    	String connected = Security.connectedDomain(); 
		domain =domain!=null? domain.trim():"";
		if(!connected.equalsIgnoreCase(domain)){			
			agents(connected);
		}
		
    	String apikey = Security.connectedApikey();
    	ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Analytics, SubServiceName.AnalyticsPage);
    	
    	if(service!=null && !service.hasExceeded){
	    	renderArgs.put("analytics_page","agents");
	        String connectedUsername = Security.connected();
	        String userType = Security.connectedUserType().name(); 
	        String ecomBlogPlugin=Security.connectedPlugin();
	        
	        //check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.valueOf(ecomBlogPlugin));
			render(userType, domain, connectedUsername,hasWidget,ecomBlogPlugin,service); 
    	} 
    	
		Analytics.contacts(domain);
    	 
    }
        
    public static void industry(String domain) { 
    	String connected = Security.connectedDomain(); 
		domain =domain!=null? domain.trim():"";
		if(!connected.equalsIgnoreCase(domain)){			
			industry(connected);
		}
    	String apikey = Security.connectedApikey(); 
    	ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Analytics, SubServiceName.AnalyticsPage);

    	if(service!=null && !service.hasExceeded){
	    	renderArgs.put("analytics_page","industry");
	        String connectedUsername = Security.connected();
	        String userType = Security.connectedUserType().name(); 
	        String ecomBlogPlugin=Security.connectedPlugin();
	        
	        //check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.valueOf(ecomBlogPlugin));
			render(userType, domain, connectedUsername,hasWidget,ecomBlogPlugin,service); 
    	}
    	
		Analytics.contacts(domain);
    }

}