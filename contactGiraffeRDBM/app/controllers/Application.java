package controllers;

import static akka.actor.Actors.actorOf;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import job.process.MailingActor;

import models.InvitationCode;
import models.dsubscription.SubPlan;
import models.dsubscription.SubService;
import models.dsubscription.SubServiceName;
 
import akka.actor.ActorRef; 
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import play.Play;
import play.cache.Cache;
import play.cache.CacheFor;
import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.*; 
import resources.DropifiTools;
import view_serializers.MailSerializer;

@With(Compress.class)
public class Application extends Controller {

	@Before
	public static void onload(){
		ForceSSL.secureDevMode();
	}
	 
    public static void index(){   
    	ApplicationTwo.index();
    	//renderArgs.put("page","index"); 
    	//render();  
    }
    
    //@CacheFor("1h")
    public static void pricing(){ 
    	ApplicationTwo.index();
    	/*
    	renderArgs.put("page","pricing");
    	List<SubPlan> subPlans = SubPlan.getOrderedSubPlans(); 
    	Map<String,List<SubService>> mapServices = SubService.getMapService(subPlans);
    	Set<String> keys = mapServices.keySet(); 
    	Map<Object,String> tooltips=SubService.getServiceTooltips(keys.toArray()); 
    	render(mapServices,keys,subPlans,tooltips); */
    }	
    
   
    public static void company(){
    	ApplicationTwo.company();
    	/*
    	renderArgs.put("page","company"); 
    	render(); 	 */	
    }	
	
	public static void customers(){
    	renderArgs.put("page","customers");
    	ApplicationTwo.index();
    	//render();
    }	
    
	@CacheFor("24h")
    public static void features(){
		ApplicationTwo.features();
		/*
    	renderArgs.put("page","features");
    	ApplicationTwo.features();
    	render();*/
    }
    
	public static void page(String page){ 
		renderArgs.put("page",page);
        render("Application/"+page+".html");
    } 
    	
	public static void sitemap() {
		render();
	}
	
	public static void robots(){
		render();
	}
	 

	//@CacheFor("48h")
	public static void legal_terms(){
		ApplicationTwo.legal_terms();
		/*renderArgs.put("page","legal_terms"); 
		render();*/
	}
 
	public static void message_display(@Required String titleMsg, @Required String headMsg,@Required String subMsg,@Required String contentMsg,String email, int status){
		renderArgs.put("page","message_display");		 
		if(validation.hasErrors() ){
			renderArgs.put("titleMsg","Dropifi.com - Page Not Found");
			renderArgs.put("headMsg","Page Not Found");
			renderArgs.put("subMsg","The page you requested was not found.");
			renderArgs.put("contentMsg","You may have clicked an expired link or mistyped the address. Some web addresses are case sensitive");
		}else{ 
			if(validation.email(email).ok &&  status==1260){
				renderArgs.put("emailReset", true);
				renderArgs.put("email", email);
			} 
			
			renderArgs.put("titleMsg",titleMsg);
			renderArgs.put("headMsg",headMsg);
			renderArgs.put("subMsg",subMsg);
			renderArgs.put("contentMsg",contentMsg);
		}		
		render();
	}
	
	public static void elto(){
		redirect("https://www.elto.com/dropifi");
	}
	
}