package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

@With(Compress.class)
public class AjaxWindow extends Controller {

    public static void page(String page){
        render("AjaxWindow/"+page+".html");
    }

}