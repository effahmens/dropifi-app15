package controllers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.With;
import play.cache.Cache;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import models.AccountUser;
import models.Company;

import org.joda.time.DateTime;

import query.analytics.CustSegSelector;
import query.analytics.MainAnalyticsSelector;
import resources.Cache.Cache_Analytics;
import resources.RangeType;
@With(Compress.class)
public class TempAnalytics extends Controller{
	
	public static void index(){
		Application.index();
		//Dashboard.index(Security.connectedDomain());
		//render();
	}
	
	public static void message(){
		Application.index();
		//Dashboard.index(Security.connectedDomain());
		//render();
	}
		
	public static void dash_board_data()
	{
		//String mailboxes = AccountUser.getQueryMailboxes(Security.connectedApikey(), Security.connected(), Security.connectedUserType());
		//List<String> mailb = AccountUser.getAssignedMailboxes(Security.connectedApikey(), Security.connected(), Security.connectedUserType());
		JsonObject json = new JsonObject();
		try{			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				int no = 30;
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId, no, 3));
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId, no,5));
				json.add("sentiment", CustSegSelector.custMsgSentiments(companyId,no));
				json.add("emotion", CustSegSelector.custMsgEmotions(companyId,no));
				json.add("tIssue", CustSegSelector.compTroublingIssues(companyId, no, 3));
				json.addProperty("nmsgs", CustSegSelector.numberOfNewMessages(companyId, no));
				json.addProperty("rmsgs", CustSegSelector.UnResolvedMessages(companyId, no));
				json.addProperty("ncust", CustSegSelector.numberOfCustomers(companyId, no));
				json.addProperty("mailbox", CustSegSelector.NumberOfMailBoxes(companyId));
				json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, no));
				status = 200;
			}			
			json.addProperty("status", status);
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);			
		}
		renderJSON(json.toString());
	}
	
	// messages controller
	public static void message_analytic_data(int days)
	{
		JsonObject json = new JsonObject();
		try{
			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.addProperty("newmsgs", CustSegSelector.numberOfNewMessages(companyId, days));
				json.addProperty("nonresolvedmsgs", CustSegSelector.UnResolvedMessages(companyId, days));
				json.addProperty("resolvedmsgs", CustSegSelector.ResolvedMessages(companyId, days));
				json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, days));
				json.add("trnMbox", CustSegSelector.trendingMailBoxMessage(companyId, days));
				//json.add("mboxstat", CustSegSelector.mailBoxMessagesStatistics(companyId, days));				
				
				status = 200;
			}	
			json.addProperty("status", status);
		}
		catch(Exception e){ 
			play.Logger.log4j.error(e.getMessage(), e);
			
		}
		renderJSON(json.toString());
	}		
	
	public static void message_analytic_data_specific_date(String specDate){
		JsonObject json = new JsonObject();
		try{
			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			Timestamp fts = null;		
			if(!specDate.equals("")){			
				fts = new Timestamp((DateTime.parse(specDate)).getMillis());
			}		
			
			if(companyId != null){
				json.addProperty("newmsgs", CustSegSelector.numberOfNewMessages(companyId, fts));
				json.addProperty("nonresolvedmsgs", CustSegSelector.UnResolvedMessages(companyId, fts));
				json.addProperty("resolvedmsgs", CustSegSelector.ResolvedMessages(companyId, fts));
				json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, fts));
				//json.add("mboxstat", CustSegSelector.mailBoxMessagesStatistics(companyId, fts));
				json.add("trnMbox", CustSegSelector.trendingMailBoxMessage(companyId, fts));
				
				status = 200;
			}	
			json.addProperty("status", status);
		}
		catch(Exception e){ 
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}

	
	public static void message_analytic_data_range_date(String startDate, String endDate )
	{
		JsonObject json = new JsonObject();
		try
		{			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			Timestamp fts = null;
			Timestamp tts = null;
			
			if(!startDate.equals("")  &&  endDate.equals("")){			
				fts = new Timestamp((DateTime.parse(startDate)).getMillis());
				tts = null;
			}
			else if(!startDate.equals("") && !endDate.equals("")){
				fts = new Timestamp((DateTime.parse(startDate)).getMillis());
				tts = new Timestamp((DateTime.parse(endDate)).getMillis());
			}		
			
			if(companyId != null){
				json.addProperty("newmsgs", CustSegSelector.numberOfNewMessages(companyId, fts, tts));
				json.addProperty("nonresolvedmsgs", CustSegSelector.UnResolvedMessages(companyId, fts, tts));
				json.addProperty("resolvedmsgs", CustSegSelector.ResolvedMessages(companyId, fts, tts));
				json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, fts, tts));
				json.add("mboxstat", CustSegSelector.mailBoxMessagesStatistics(companyId, fts, tts));
				json.add("trnMbox", CustSegSelector.trendingMailBoxMessage(companyId, fts, tts));
				
				status = 200;
			}	
			json.addProperty("status", status);			
		}
		catch(Exception e){ 
			play.Logger.log4j.error(e.getMessage(), e);	
		}		
		renderJSON(json.toString());
	}
	
	
	public static void message_analytic(int days, String startDate, String endDate)
	{
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				if((days != 0) && (startDate.equals("")) && (endDate.equals("")))
				{
					json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, days));
					json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, days));
					json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, days));
					
					json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, days));
					json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, days));
				}
				else if((days == 0) && (!startDate.equals("")) && (endDate.equals("")))
				{
					Timestamp fts = new Timestamp((DateTime.parse(startDate)).getMillis());
					
					json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, fts));
					json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, fts));
					json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, fts));
					
					json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, fts));
					json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, fts));
				}
				else if((days == 0) && (startDate.equals("")) && (!endDate.equals("")))
				{
					Timestamp fts = new Timestamp((DateTime.parse(endDate)).getMillis());
					
					json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, fts));
					json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, fts));
					json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, fts));
					
					json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, fts));
					json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, fts));
				}
				else if((days == 0) && (!startDate.equals("")) && (!endDate.equals("")))
				{
					Timestamp fts = new Timestamp((DateTime.parse(startDate)).getMillis());
					Timestamp tts = new Timestamp((DateTime.parse(endDate)).getMillis());
					
					json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, fts, tts));
					json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, fts, tts));
					json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, fts, tts));
					
					json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, fts, tts));
					json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, fts, tts));
				}
				
				status = 200;
			}
			
			json.addProperty("status", status);
		}
		catch(Exception e){ 
			play.Logger.log4j.error(e.getMessage(), e);			
		}
		renderJSON(json.toString());
	}
	
	// contacts controllers
	public static void contact_analytic_static_chart_data_one(int days)
	{
		JsonObject json = new JsonObject();
		try{			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null){			
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId, days,10));
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId, days, 10));
				json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId,days));
				json.add("gender", CustSegSelector.custGenderToJsonArray(companyId,days));
				
				//json.add("socialsentiment", MainAnalyticsSelector.DefaultSocialAndMessage(companyId));
				//json.add("sentiment", CustSegSelector.custMsgSentiments(companyId,30));
				status = 200;
			}	
			json.addProperty("status", status);
		}
		catch(Exception e){			 
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	public static void contact_analytic_static_chart_data_two(String dfrom, int top)
	{
		JsonObject json = new JsonObject();
		try{
			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			Timestamp fts = null;		
			if(!dfrom.equals("")){			
				fts = new Timestamp((DateTime.parse(dfrom)).getMillis());
			}
					
			if(companyId != null){			
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId, fts ,top));
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId, fts, top));
				json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId,fts));
				json.add("gender", CustSegSelector.custGenderToJsonArray(companyId,fts));
				
				status = 200;
			}	
			json.addProperty("status", status);
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	public static void contact_analytic_static_chart_data_three(@Required String dfrom, @Required String dto, int top)
	{
		JsonObject json = new JsonObject();
		try{
			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			//if(!validation.hasErrors())
			Timestamp fts = null;
			Timestamp tts = null;
			
			if(dfrom != null && !dfrom.equals("") && dto != null && !dto.equals("")){
				fts = new Timestamp((DateTime.parse(dfrom)).getMillis());
				tts = new Timestamp((DateTime.parse(dto)).getMillis());
			}
					
			if(companyId != null)
			{			
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId,fts, tts,top));
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId,fts,tts,top));
				json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId,fts,tts));
				json.add("gender", CustSegSelector.custGenderToJsonArray(companyId,fts,tts));			
				status = 200;
			}	
			json.addProperty("status", status);			
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		renderJSON(json.toString());
	}
	
		
	// sentiment controllers
	
	public static void main_sentiment_data(){
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				int no = 30;
				json.add("tIssue", CustSegSelector.compTroublingIssues(companyId, no, 3));
				json.add("keywords", CustSegSelector.compKeywords(companyId, no, 3));
				json.add("mboxstat", CustSegSelector.mailBoxStatistics(companyId, no, 5));
			
			 status = 200;
			}
			
			json.addProperty("status", status);
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());		
	}
	
	public static void trial_page_data()
	{
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				int no = 30;
				json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId,no));
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId, no, 3));
				json.add("gender", CustSegSelector.custGenderToJsonArray(companyId,no));
				json.add("maritalstatus", CustSegSelector.custMaritalStatusToJsonArray(companyId, no));
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId, no,5));
				json.add("sentiment", CustSegSelector.custMsgSentiments(companyId,no));
				json.add("emotion", CustSegSelector.custMsgEmotions(companyId,no));
				json.addProperty("score", CustSegSelector.custMsgSentimentScore(companyId,no));
				json.add("tIssue", CustSegSelector.compTroublingIssues(companyId, no, 3));
				json.add("keywords", CustSegSelector.compKeywords(companyId, no, 3));
				json.add("mboxstat", CustSegSelector.mailBoxStatistics(companyId, no, 5));
				json.addProperty("nmsgs", CustSegSelector.numberOfNewMessages(Security.connectedCompanyId(), no));
				json.addProperty("rmsgs", CustSegSelector.UnResolvedMessages(Security.connectedCompanyId(), no));
				json.addProperty("ncust", CustSegSelector.numberOfCustomers(Security.connectedCompanyId(), no));
				json.addProperty("avgSentScore", CustSegSelector.AverageSentimentScore(companyId));
				json.addProperty("resRate", MainAnalyticsSelector.ResolutionRate(companyId));
				status = 200;
			}			
			json.addProperty("status", status);
			
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);			
		}
		renderJSON(json.toString());
	}
	
	public static void trial_ind_data(String email)
	{
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null && email != null)
			{
				json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessagesFromContact(companyId, email, null, null));
				//json.add("trdemomsg", CustSegSelector.custMsgEmotionsFromContact(companyId, email, null, null));
				json.add("sentiment", CustSegSelector.custMsgSentimentsFromContact(companyId, email, null, null));				
				json.addProperty("nmsgs", CustSegSelector.numberOfMessagesFromContact(Security.connectedCompanyId(), email, null, null));
				json.addProperty("rmsgs", CustSegSelector.UnResolvedMessagesFromContact(Security.connectedCompanyId(), email, null, null));
				json.addProperty("avgSentScore", CustSegSelector.AverageSentimentScoreFromContact(companyId, email, null, null));
								
				status = 200;
			}			
			json.addProperty("status", status);
			
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		
		renderJSON(json.toString());
	}
	
	public static void cust_age_range_from_to(Timestamp fromDate,Timestamp toDate){
		JsonObject json = new JsonObject();
		try {			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId, fromDate, toDate));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}	
		renderJSON(json.toString());	
	}
	
	public static void cust_age_range_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId, 30));
				status = 200;
			}
			
			json.addProperty("status", status);
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}	
		renderJSON(json.toString());	
	}
	
	public static void cust_age_range(){
		
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}	
		renderJSON(json.toString());	
	}
	
	public static void cust_location_from_to(Timestamp fromDate,Timestamp toDate){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId, fromDate, toDate));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}		
		renderJSON(json.toString());
	}	
	
	public static void cust_location_nod_top(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId, 30, 3));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}		
		renderJSON(json.toString());
	}
	
	public static void cust_location_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId, 30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}
		renderJSON(json.toString());		
	}
	
	public static void cust_location(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("location", CustSegSelector.custLocationToJsonArray(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}		
		renderJSON(json.toString());
	}
	
	public static void cust_gender_from_to(Timestamp fromDate,Timestamp toDate)
	{
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("gender", CustSegSelector.custGenderToJsonArray(companyId, fromDate, toDate));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
			
		}
		renderJSON(json.toString());
	}
	
	public static void cust_gender_nod()
	{
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("gender", CustSegSelector.custGenderToJsonArray(companyId, 30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);			
		}
		renderJSON(json.toString());
	}
	

	public static void cust_gender()
	{
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("gender", CustSegSelector.custGenderToJsonArray(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);			
		}
		renderJSON(json.toString());
	}
 	
	public static void cust_marital_status_from_to(Timestamp fromDate, Timestamp toDate){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("maritalstatus", CustSegSelector.custMaritalStatusToJsonArray(companyId, fromDate, toDate));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}
		renderJSON(json.toString());
	}	
	public static void cust_marital_status_nod_top(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("maritalstatus", CustSegSelector.custMaritalStatusToJsonArray(companyId, 30, 5));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}
		renderJSON(json.toString());
	}
	public static void cust_marital_status_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("maritalstatus", CustSegSelector.custMaritalStatusToJsonArray(companyId, 30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	public static void cust_marital_status(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("maritalstatus", CustSegSelector.custMaritalStatusToJsonArray(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}	
		renderJSON(json.toString());	
	}

	
	public static void cust_socialnetwork(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}
		renderJSON(json.toString());
	}
	public static void cust_socialnetwork_nod_top(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId,30,3));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}
		renderJSON(json.toString());
	}
	public static void cust_socialnetwork_from_to(Timestamp fromDate,Timestamp toDate){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId,fromDate,toDate));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {			 
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	
	
	/******************************************************************************************************/
	// Controllers for Trending sentiments
	public static void cust_msg_sentiment(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("sentiment", CustSegSelector.custMsgSentiments(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	public static void cust_msg_sentiment_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("sentiment", CustSegSelector.custMsgSentiments(companyId,30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	public static void cust_msg_emotions(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("emotion", CustSegSelector.custMsgEmotions(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	public static void cust_msg_emotions_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("emotion", CustSegSelector.custMsgEmotions(companyId,30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);				
		}
		renderJSON(json.toString());
	}
	
	public static void cust_msg_sentiment_score_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.addProperty("score", CustSegSelector.custMsgSentimentScore(companyId,30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	
	
	public static void comp_troubling_issues_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("tIssue", CustSegSelector.compTroublingIssues(companyId, 30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	public static void comp_troubling_issues_nod_top(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("tIssue", CustSegSelector.compTroublingIssues(companyId, 30, 3));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	
	public static void comp_keywords(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("tIssue", CustSegSelector.compKeywords(companyId));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	public static void comp_keywords_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("tIssue", CustSegSelector.compKeywords(companyId, 30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	public static void comp_keywords_nod_top(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("tIssue", CustSegSelector.compKeywords(companyId, 30, 3));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	/******************************************************************************************************/
	// Mailbox statistics
	public static void mailbox_statistics_nod(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("mboxstat", CustSegSelector.mailBoxStatistics(companyId, 30));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	public static void mailbox_statistics_nod_top(){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("mboxstat", CustSegSelector.mailBoxStatistics(companyId, 30, 5));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	public static void mailbox_statistics_from_to(Timestamp from, Timestamp to){
		JsonObject json = new JsonObject();
		try {
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null)
			{
				json.add("mboxstat", CustSegSelector.mailBoxStatistics(companyId, from, to));
				status = 200;
			}
			
			json.addProperty("status", status);
			
		} catch (Exception e) {
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	
	/*****************************************************************************************************/
	
	public static void initial_cust_analytic_load()
	{
		JsonObject json = new JsonObject();
		try{			
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			int status = 405;
			
			if(companyId != null){					
				json.add("socialsentiment", MainAnalyticsSelector.DefaultSocialAndMessage(companyId, 30));
				status = 200;
			}	
			json.addProperty("status", status);
			
		}
		catch(Exception e){			 
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}
	
	public static void main_cust_analytic(String par1, String par2, String datefrom, String dateto)
	{
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			
			int status = 405;
			
			Timestamp fts = null;
			Timestamp tts = null;
			
			if(!datefrom.equals("")  &&  dateto.equals("")){			
				fts = new Timestamp((DateTime.parse(datefrom)).getMillis());
				tts = null;
			}
			else if(!datefrom.equals("") && !dateto.equals("")){
				fts = new Timestamp((DateTime.parse(datefrom)).getMillis());
				tts = new Timestamp((DateTime.parse(dateto)).getMillis());
			}
			
			//play.Logger.log4j.info("Par1 : " + par1 + ", Par2 : " + par2 + ", FTS : " + fts + ", TTS : " + tts);		
			if(companyId != null){			
				json.add("mcustanalytic", MainAnalyticsSelector.custProfileAnalytics(companyId, fts, tts, par1, par2));			
				status = 200;			
			}
			
			json.addProperty("status", status);
			
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}	
	
	public static void main_cust_analytic_no_date(int days,int top)
	{
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			
			int status = 405;	
			
			if(companyId != null)
			{
				//json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, days));
				//json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, days));
				//json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, days));
				
				//json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, days));
				//json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, days));
				json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessages(companyId, days));
				json.add("trdsenmsg", MainAnalyticsSelector.TrendingSentimentsAndMessages(companyId, days));
				//json.add("trdemo", MainAnalyticsSelector.TrendingEmotions(companyId, days, 3));
				//json.add("trbiss", CustSegSelector.compTroublingIssuesImp(companyId,days, top));				
				
				status = 200;
			}
			json.addProperty("status", status);			
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		renderJSON(json.toString());
	}
	
	public static void main_cust_analytic_single_date(String datefrom, int top)
	{		
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			
			int status = 405;
			
			Timestamp fts = null;		
			if(!datefrom.equals("")){			
				fts = new Timestamp((DateTime.parse(datefrom)).getMillis());
			}			
			
			if(companyId != null){
				//json.add("trdemo", MainAnalyticsSelector.TrendingEmotions(companyId,fts,3));
				//json.add("trbiss", CustSegSelector.compTroublingIssues(companyId, fts, top));
				//json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, fts));
				//json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, fts));
				//json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, fts));
				
				//json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, fts));
				//json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, fts));
				json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessages(companyId, fts));
				json.add("trdsenmsg", MainAnalyticsSelector.TrendingSentimentsAndMessages(companyId, fts));
				
				status = 200;
			}
			json.addProperty("status", status);
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		renderJSON(json.toString());
	}
	
	public static void main_cust_analytic_double_date(String datefrom, String dateto, int top){
		JsonObject json = new JsonObject();
		try{
			Long companyId = Company.findCompanyId(Security.connectedApikey());
			
			int status = 405;
			
			Timestamp fts = null;
			Timestamp tts = null;
			
			if(!datefrom.equals("")  &&  dateto.equals("")){			
				fts = new Timestamp((DateTime.parse(datefrom)).getMillis());
				tts = null;
			}
			else if(!datefrom.equals("") && !dateto.equals("")){
				fts = new Timestamp((DateTime.parse(datefrom)).getMillis());
				tts = new Timestamp((DateTime.parse(dateto)).getMillis());
			}
			
			if(companyId != null){
				//json.add("trdemo", MainAnalyticsSelector.TrendingEmotions(companyId,fts, tts, 3));
				//json.add("trbiss", CustSegSelector.compTroublingIssues(companyId, fts, tts, top));
				//json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, fts, tts));
				//json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, fts, tts));
				//json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, fts, tts));
				
				//json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, fts, tts));
				//json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, fts, tts));
				json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessages(companyId, fts, tts));
				json.add("trdsenmsg", MainAnalyticsSelector.TrendingSentimentsAndMessages(companyId, fts, tts));
				
				status = 200;
			}
			json.addProperty("status", status);
			
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);	
		}
		renderJSON(json.toString());
	}	
	
}
