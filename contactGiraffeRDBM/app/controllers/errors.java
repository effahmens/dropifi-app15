package controllers;

 
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
@With(Compress.class)
public class errors extends Controller {

	@Before
	public static void setArgs(){
		renderArgs.put("page","page not found");
	}
}
