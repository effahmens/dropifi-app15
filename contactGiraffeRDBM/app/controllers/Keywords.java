package controllers;

import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import resources.EcomBlogType;
import resources.UserType;
import models.Company;
import models.MessageManager;
import models.MsgSearchKeyword;
import models.MsgSubject;
import models.WidgetTracker;
@With(Compress.class)
public class Keywords extends Controller {	
    @Before
	public static void setArgs(String domain){     
    	 String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
 			if(UserType.isAgent(Security.connectedUserType()))
 				Dashboard.index(domain); 
 			
 			renderArgs.put("page","admin");
 			renderArgs.put("sub_page","mail");
 			renderArgs.put("title","Admin - Mail Management (Keywords)");
 		}else{
 			//clear the session
 	    	session.clear();
 	    	//response.removeCookie("rememberme");
 			
 			try {
 				Secure.login("");
 			} catch (Throwable e) {
 				Application.index();
 			}
 		}
		
	 }
    
	public static void index(String domain){	
		ForceSSL.redirectToHttps();
		
		String apikey = Security.connectedApikey();
		if(!validation.hasErrors() && apikey !=null){				
			
			String connected = Security.connectedDomain(); 
			domain =domain!=null? domain.trim():"";
 			//if(connected.equalsIgnoreCase(domain)){
			if(!connected.equalsIgnoreCase(domain)){			
				index(connected);
			}
			String connectedUsername = Security.connected();	 
			String userType = Security.connectedUserType().name();			 
			String ecomBlogPlugin = Security.connectedPlugin();
			
			//check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
			render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget); 
 	 		    
 			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
		
		Apps.page_not_found("");
	}
	
	public static void populate(){
		//TODO implemented security checks
		
		//Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status = 404;
		String apikey = Security.connectedApikey(); 
		
		//String domain =Security.connectedDomain();
		List keywords = null; 
		int size =0;
		if(apikey !=null){
			MessageManager msg = MessageManager.findByApikey(apikey);
			keywords =(List) (msg==null?null: msg.getKeywords());		
		
			//check the status of the keywords: if size 0 then return 200 - keywords(s) is/are available
			size = keywords == null? 0: keywords.size();
			status = (size>0) ? 200:501;	
				
			json.add("keywords", MsgSearchKeyword.toJsonArray(keywords)); 
		}
		
		json.addProperty("status", status); 
		json.addProperty("size", size);
		
		renderJSON(json.toString());
	}
	
	public static void create(MsgSearchKeyword mk){
		//TODO: provide method for save the quickResponse
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		if(!validation.hasErrors()){
			
			if(validation.required(mk.getKeyword()).ok && validation.required(mk.getActivated()).ok){
				
				String apikey = Security.connectedApikey();
				if(apikey != null){
					//create a new quickResponse
					MessageManager msg = MessageManager.findByApikey(apikey);
					if(msg != null){
						status =msg.addKeyword(mk);
						json.addProperty("error", (status == 200? "none" : (status==302 ? "duplication of keyword name": "update was not successful")) );
					}
				}else{
					status = 405;
				}
				
			}else{
				status = 501;			
			}
			
		}else{
			json.addProperty("error", "required fields not specified");
		}
		
		json.addProperty("status", status);
		json.add("content", gson.toJsonTree(mk)); 
		renderJSON(json.toString());
	}
	
	public static void update(@Required String keyword, MsgSearchKeyword mk){
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		if(!validation.hasErrors()){  
				 					
		 	//check if the email is a valid one
			if(validation.required(mk.getKeyword()).ok && validation.required(mk.getActivated()).ok /*&&
					validation.required(mk.getPriority()).ok*/){	 
				 
				//search for the connected company	
				String apikey = Security.connectedApikey();
				
				if(apikey != null){				
					
					MessageManager msg = MessageManager.findByApikey(apikey);
					
					if(msg != null){ 							
						//update the changes to the quick response
						status = msg.updateKeyword(keyword, mk)	;				
						json.addProperty("error", (status==200? "none" : (status==302 ? "duplication of keyword name": "update was not successful")) );
					} 
				}else{
					status=405;
				}
				
				//redirect to login page
			}else{
				json.addProperty("error", "required fields not specified");
			}
		
		}else{
			json.addProperty("error", "none");
		}
		
		json.add("content", gson.toJsonTree(mk)); 
		json.addProperty("status", status); 
		
		renderJSON(json.toString()); 	
	}
	
	public static void delete(@Required String keyword){
		JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();
			
			if(apikey != null){				
				
				MessageManager msg = MessageManager.findByApikey(apikey);
				
				if(msg != null){ 	
					 					
					//update the changes to the mailbox
					status = msg.removeKeyword(keyword);
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}
			
		}
		
		json.addProperty("status", status); 
		json.addProperty("keyword", keyword); 
		renderJSON(json.toString());
	}
	
	//displays an editable details of a message keyword
	public static void show(@Required String keyword , @Required String domain){
		ForceSSL.redirectToHttps();
		//search for the connected company	
		String apikey = Security.connectedApikey(); 
		String connected = Security.connectedDomain(); 
		String btnsave = ""; 
		
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			MsgSearchKeyword msgKeyword = null;
			
			if(validation.required(keyword).ok){ 
				
				//check if a keyword is specified 			
				keyword = keyword.replace("~~","&");
				if(keyword.equals("new")){
					 btnsave ="Save"; 				 
					 //render(btnsave,keyword,domain);
				}else{
					//search for the message keyword
					MessageManager msg = MessageManager.findByApikey(apikey);		
					msgKeyword = (msg == null ? null : msg.findKeywordByName(keyword)); 				
					
					btnsave ="Update"; 			
					//render(btnsave, keyword, msgKeyword,domain); 				
				}  			
			}else{
				keyword="new";
				btnsave ="Save"; 				 
				//render(btnsave,keyword,domain);
			}
			render(btnsave, keyword, msgKeyword,domain,connectedUsername,ecomBlogPlugin,userType); 	
		}
		
		Apps.popout_not_found("");
	} 
	
	//enable or disable mailbox
	public static void activateKeyword(@Required String keyword, @Required Boolean activated){
		
		//Gson gson = new Gson();				 
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				MessageManager msg = MessageManager.findByApikey(apikey);
				if(msg != null){ 	
					 			
					//update the changes to the mailbox
					status = msg.updateKeywordActivated(keyword, activated);	
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("activated", activated);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
	public static void orderKeywords(@Required List<String> keywords){
		JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors()){
			//search for the connected company	
			String apikey = Security.connectedApikey();	 
			if(apikey != null){
				MessageManager msg = MessageManager.findByApikey(apikey);
				if(msg != null){ 	 
					//update the changes to the mailbox
					status = msg.addKeywords(MsgSearchKeyword.getMsgSearchKeyword(keywords));	
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
		}else{
			json.addProperty("error", "required fields are not specified");
		} 
		
		json.addProperty("status", status); 	
		renderJSON(json.toString());  
	}
	
}
