package controllers;

import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.*;

import adminModels.DropifiMailer;
import api.tictail.TictailStore;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import models.AccountUser;
import models.Company;
import models.FileDocument;
import models.InboundMailbox;
import models.MessageManager;
import models.MsgSearchKeyword;
import models.PreviewWidget;
import models.WiParameter;
import models.Widget;
import models.WidgetControl;
import models.WidgetDefault; 
import models.WidgetTracker;
import models.WixDefault;
import models.dsubscription.SubConsumeService;
import models.dsubscription.SubServiceName;
import play.cache.Cache;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import play.utils.HTML;
import resources.AccountType;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.FileType;
import resources.MailComposer;
import resources.MailTemplate;
import resources.UserType;
import resources.WidgetField; 
import resources.WixDefaultSerializer;
import resources.helper.ServiceSerializer;
import view_serializers.CoProfileSerializer;
import view_serializers.ConSerializer;
import view_serializers.MbSerializer;
import view_serializers.WiSerializer; 
@With(Compress.class)
public class Widgets extends Controller{
	
	@Before
    static void setArgs(String domain){
		String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0){
 			
 			if(UserType.isAgent(Security.connectedUserType()))
 				Dashboard.index(domain);
 			
 			 renderArgs.put("page","admin");
 		     renderArgs.put("sub_page","mail");
 		     renderArgs.put("title","Admin - Mail Management (Widget Customization)");
 		}else{ 
 			//clear the session
 	    session.clear();  
 			try {
 				Secure.login("");
 			} catch (Throwable e) {
 				Application.index(); 
 			}
 		}      
    }
	
	//contains sections for editing, previewing and updating a widget
	public static void index(String domain,boolean isAutoReply){
		ForceSSL.redirectToHttps();
		
		try{		
			String apikey = Security.connectedApikey();
			
			if(!validation.hasErrors() && apikey !=null){
				
					String connected = Security.connectedDomain(); 
					domain =domain!=null? domain.trim():"";
					if(!connected.equalsIgnoreCase(domain)){			
		 				index(connected,isAutoReply);
		 			}
					String connectedUsername = Security.connected();  

					//select company widget settings
					//Widget widget  = Widget.findByApiKey(apikey);
					
					PreviewWidget widget = PreviewWidget.createUpdate(Widget.findByApiKey(apikey), domain);
					
					if(Security.connectedPlugin().equals(EcomBlogType.Wix.name())){
						WixDefault wix = WixDefault.findByApikey(widget.getApikey());
						if(wix!=null && wix.getWiParameter()!=null){
							widget.setWiParameter(wix.getWiParameter());
						} 
					}
					
					//Widget widget  = Widget.searchByApiKey(apikey);
					
					if(widget !=null) {
						ServiceSerializer service = CacheKey.getPremiumServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
						 
						//set widget properties to be render in the view
						String wheader = widget.getTitle();	
						
						WiParameter wiParam = widget.getWiParameter();
						
						String btext="",bcolor="", bfont=DropifiTools.defaultFont,bposition="";
						Integer bpercent = 35;
						boolean isBlack=false, isWhite=false, activatedWidget = Widget.findActivated(apikey);
						
						if(wiParam!=null){
							btext = wiParam.getButtonText(); 
							bcolor	 = wiParam.getButtonColor();
							bfont	 = (wiParam.getButtonFont()!=null)?wiParam.getButtonFont():DropifiTools.defaultFont;
							//bfont	 = (wiParam.getButtonFont()!=null && service!=null && !service.hasExceeded)?wiParam.getButtonFont():DropifiTools.defaultFont;
							bposition = wiParam.getButtonPosition(); 
							bpercent= wiParam.getButtonPercent();
							isBlack = wiParam.getButtonTextColor()!=null && wiParam.getButtonTextColor().equals("BLACK")?true:false;
							isWhite = wiParam.getButtonTextColor() ==null || wiParam.getButtonTextColor().equals("WHITE")?true:false;
							//check if the widget is live or off
			 			    //activatedWidget = wiParam.getActivated()==null?true:wiParam.getActivated();
						}
						
						String defaultEmail = widget.getDefaultEmail();
						String defaultMessage = widget.getDefaultMessage();
						
						String responseSubject  = widget.getResponseSubject();
						String responseMessage = widget.getResponseMessage();
						Boolean responseActivated = widget.getResponseActivated();
						String responseColor = widget.getResponseColor()!=null?widget.getResponseColor():"#1E7BB8";
						
						String bSendText = widget.getSendButtonText()!=null?widget.getSendButtonText():"Send Message";
						String bSendColor = widget.getSendButtonColor()!=null?widget.getSendButtonColor():"#1E7BB8";
						String messageTemplate = widget.getMessageTemplate()!=null?widget.getMessageTemplate():"Dropifi Template";
						
						Boolean hasCaptcha = widget.getHasCaptcha()!=null?widget.getHasCaptcha():false;
						String captchaValue = widget.getCaptchaValue()!=null?widget.getCaptchaValue():"What's the result of";
						
						if(messageTemplate.equals("DropifiDefault"))
							messageTemplate="Dropifi Template";
						else if(messageTemplate.equals("PlainText"))
							messageTemplate="Plain Text";
						
						List<ConSerializer> controls = widget.getSerializeControls();
						
						//select the company created mailbox;
						List<String> mailboxes = InboundMailbox.findAllEmail(apikey); 
						
						if(!mailboxes.contains(connectedUsername))
							mailboxes.add(TictailStore.resetEmail(connectedUsername));
						
						List<String>subjects = MessageManager.listOfMsgSubjects(apikey,true);
						
						if(subjects==null || subjects.size()<=0)
							subjects =null;
						
						 
						//WidgetDefault wid = WidgetDefault.findByApiKey(apikey); 
						String publickey ="";	//wid.getPublicKey();
						String plugin	="";	//widget.generatePlugin(publickey);
						
						String userType = Security.connectedUserType().name();
						
						//String username ="";// AccountUser.findByName(connectedUsername, apikey);
						String embedMsg = ""; // widget.embedCodeMsg(username, connectedUsername);
						
						String ecomBlogPlugin = Security.connectedPlugin();
						WixDefaultSerializer wix =null;
						if(ecomBlogPlugin.equals("Wix")){
							wix = WixDefault.getWixDefaultSerializer(apikey); 
						}
						
						//check if the widget is installed on the site of the user
		 				boolean hasWidget = WidgetTracker.hasWidgetInstalled(apikey, EcomBlogType.getValue(ecomBlogPlugin));
		 				boolean isWidgetPage=true;
		 				String fonts[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		 		
		 				//JsonObject jservice = new Gson().toJsonTree(service).getAsJsonObject();
		 				
						render(userType,domain,controls,wheader,btext,bfont,bSendText,isBlack,isWhite ,bcolor,bSendColor,bposition,
								bpercent,defaultEmail,messageTemplate, mailboxes,hasWidget,subjects,isAutoReply,activatedWidget,
								defaultMessage,plugin,publickey,responseSubject,responseMessage,responseActivated,
								responseColor,connectedUsername,embedMsg,ecomBlogPlugin,service,fonts,wix,isWidgetPage,hasCaptcha,captchaValue);
					}
	 			/*}else{
	 				//check if the domain is available 
	 				boolean hasDomain = Company.isDomainAvailable(domain);
	 				if(hasDomain){	
	 				//show domain not found page
	 					Apps.multiple_login(connected);
	 				}else{
	 					Apps.domain_not_found(domain);
	 				}
	 			}*/ 
			} 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e); 
			Apps.page_not_found("");		
		}
		
		Apps.page_not_found("");			
	}
	
	//save the changes to the widget
	public static void update(WiSerializer wi, String type){ 
		JsonObject json = new JsonObject();
		Gson gson = new Gson();
		
		String apikey = Security.connectedApikey();
		String plugin="";
		int status = 404;
		if(apikey != null){
			wi.setButtonTextColor(wi.getButtonColor());
			wi.setDomain(Security.connectedDomain());
			 
			if(type.equals("PROD")){
				//ServiceSerializer service = CacheKey.getPremiumServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
				
				//if(!service.getAccountType().equals(AccountType.Free.name())){
					Widget widget = Widget.findByApiKey(apikey);  
					if(widget !=null){   
						/**
						 * NB Make this service available to free users
						 */
						/*if(service!=null && service.hasExceeded){
							wi.setSendButtonColor(widget.getSendButtonColor());
							wi.setSendButtonText(widget.getSendButtonText());
						}*/
						
						status = widget.updateWidget(wi);
						plugin = widget.getPlugin();
						
						//delete the widget from the cache
						CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget);
						updateWix(status,widget); 
						
					}else{ 
						status = 203;
					}
				/*}else{
					status = 305; 
					json.add("service", gson.toJsonTree(service));
				}*/
				//json.add("service", gson.toJsonTree(service));
			}else{ //Save the widget settings as preview  
				status = PreviewWidget.createUpdate(wi, apikey); 
			}
		}
		
		json.addProperty("status", status);
		json.addProperty("type", type);
		json.addProperty("plugin", plugin);
		json.add("widget", gson.toJsonTree(wi));
		renderJSON(json.toString());
	}
	
	public static void updatePreview(WiSerializer wi){
		
	} 
	
	//enable or disable widget control
	public static void activatedControl(String condId, boolean activated,String type){	
		 			 
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){ 
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				if(type.equals("PROD")){
					Widget widget = Widget.findByApiKey(apikey);
					if(widget != null){ 	
						 			
						//update the changes to the mailbox
						status = widget.updateConActivated(condId, activated); 	 
						//delete the widget from the cache
						CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget);
						updateWix(status,widget);
					} 
				}else{
					PreviewWidget widget = PreviewWidget.findByApiKey(apikey);
					if(widget!=null){
						status = widget.updateConActivated(condId, activated);
					}
				}
				json.addProperty("error", (status==200? "none" : "update was not successful") );
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("condId", condId);
		json.addProperty("activated", activated);
		json.addProperty("type", type);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
	public static void activatedCaptcha(boolean activated,String type) {
		JsonObject json = new JsonObject(); 
	int status = 404; 
		
		if(!validation.hasErrors()){ 
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				if(type.equals("PROD")){
					Widget widget = Widget.findByApiKey(apikey);
					if(widget != null){ 	
						 			
						//update the changes to the mailbox
						status = widget.updateHasCaptcha(activated); 
						//delete the widget from the cache
						CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget);
						updateWix(status,widget);
					} 
				}else{
					PreviewWidget widget = PreviewWidget.findByApiKey(apikey);
					if(widget!=null){
						status = widget.updateHasCaptcha(activated);
					}
				}
				json.addProperty("error", (status==200? "none" : "update was not successful") );
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		} 
		json.addProperty("activated", activated);
		json.addProperty("type", type);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
	//enable or disable widget control
	public static void activatedResponse(String respId, boolean activated){	
		//Gson gson = new Gson();				 
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				Widget widget = Widget.findByApiKey(apikey);
				if(widget != null){ 	
					 			
					//update the changes to the mailbox
					status = widget.updateRespActivated(activated);	 			
					json.addProperty("error", (status==200? "none" : "update was not successful") ); 
					
					//delete the widget from the cache
					CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget);
					updateWix(status,widget);
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("condId", respId);
		json.addProperty("activated", activated);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
	public static void activatedWidget(boolean activated){
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				Widget widget = Widget.findByApiKey(apikey);
				if(widget != null){ 	
					 			
					//update the changes to the mailbox
					status = widget.updateWidgetActivated(activated); 			
					json.addProperty("error", (status==200? "none" : "update was not successful") ); 
					
					//delete the widget from the cache
					CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget);
					updateWix(status,widget);
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		 
		json.addProperty("activated", activated);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
	//send a copy of the widget plugin to the developer of the company 
	public static void sendToDeveloper(@Required String email, @Required String message, @Required String pluginCode){
		
		JsonObject json = new JsonObject();		 
		int status = 404; 
		
		if(!validation.hasErrors()){			
			//send the plugin to developer using dropifi default mailbox
			
			if(validation.email(email).ok){
				//search for the default mailbox of the company
				MbSerializer cMb = null;//InboundMailbox.findByApikey(Security.connectedApikey());
				
				//search for the default mailbox of dropifi
				DropifiMailer mailer = (cMb!=null && !cMb.getEmail().isEmpty())? new DropifiMailer(cMb) : DropifiMailer.getDefault();
				
				if(mailer !=null) { 
					 String compose ="";
					try {
						AccountUser user = AccountUser.findByAdmin(Security.connected(), Security.connectedApikey());
						HTML html = new HTML(); 	 
						String msg = MailTemplate.developerCode(email, user.getProfile().getUsername(), user.getProfile().getEmail(),message,html.htmlEscape(pluginCode));					
					    compose = MailComposer.html(msg.toString(), null, null); 
					    play.Logger.log4j.info(mailer.getMailbox().getUsername());
						status = mailer.sendSimpleMail(email, " ", "Embed the code snippet in the header of the html page", compose,null); 	
						
					} catch (Exception e) {
						// TODO Auto-generated catch block 
						try {
							mailer.sendSimpleMail(email, " ", "Embed the code snippet in the header of the html page", compose,null); 
						} catch (Exception e1){
							// TODO Auto-generated catch block
							status = 602;
						} 
					}				    
				}
				
			}else{
				status = 405; //invalid email
				json.addProperty("error", "invalid email or email do not exist");
			}						
		}else{
			json.addProperty("error", "some required fields are not specified");
		}
	
		json.addProperty("status", status);
		renderJSON(json.toString()); 				
	}
	
	public static void show_error_msg(@Required String domain,String response){
		//search for the connected company	
		String apikey = Security.connectedApikey(); 
		String connected = Security.connectedDomain(); 
				
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			//if(widget == null)
			Widget	widget =  Widget.findByApiKey(apikey);		
			
			Collection<WidgetControl> controls = widget.getWidgetControls();
			
			render(domain,connectedUsername,ecomBlogPlugin,userType,controls,response); 	
		} 
		Apps.popout_not_found("");
	}
	
	public static void saveErrorMsg(HashMap<String,String> controls){  
		String connected = Security.connectedDomain(); 
		String apikey = Security.connectedApikey(); 
		Widget widget = Widget.findByApiKey(apikey);
		
		int status = widget.updateWidgetErrorMsg(controls); 
		String response = status==200?"error messages updated successfully":"Could not save the changes";
		updateWix(status,widget);
		show_error_msg(connected,response);
	}
	
	public static void preview_autoresponse(){ 
		String apikey = Security.connectedApikey(); 
		String responseTemplate = "";
		if(apikey!=null){
			CoProfileSerializer profile = Company.findInfoByApikey(apikey);
			//select company widget settings
			Widget widget = Widget.findByApiKey(apikey);
			responseTemplate = MailTemplate.autoResponse(profile, "johndoe@somedomain.com", "John Doe", widget.getResponseSubject(), widget.getResponseMessage());
			render(responseTemplate);
		}
		Apps.popout_not_found(""); 
	}
	
	public static void show_upload_logo(String msg){ 
		ForceSSL.redirectToHttps();
		render(msg);
	}
	
	public static void deleteAutoReplyLogo(){
		JsonObject json = new JsonObject();
		String apikey =Security.connectedApikey();
		int status =501;
		String msg ="<p class='error'>Try removing the logo again</p>";
		if(apikey!=null){
			status = FileDocument.deleteFileDocument(apikey, FileType.AutoReplyLogo)!=null?200:201;
			msg = status==200?"<p class='success'>Auto reply logo removed</p>":"<p class='warning'>Auto reply logo has been removed</p>";
		}
		
		json.addProperty("status", status);
		json.addProperty("msg", msg);
		
		renderJSON(json.toString());
	}
	
	@Util
	public static void updateWix(int status,Widget widget){
		if(status==200){
			try{
				WixDefault wix = WixDefault.findByApikey(widget.getApikey());
			if(wix!=null){
				widget.setWiParameter(wix.getWiParameter());
				wix.updateWixDefault(widget);
			}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
	}
	
}
