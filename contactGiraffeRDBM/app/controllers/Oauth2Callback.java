package controllers;

import javax.mail.Message;
import javax.mail.MessagingException;

import models.AccountUser;
import models.Channel;
import models.InboundMailbox;
import models.dsubscription.SubServiceName;

import adminModels.MailboxConfig;

import com.google.api.services.oauth2.model.Userinfo;

import gmailoauth2grant.DGOauthGrant;
import gmailoauth2grant.DGOauthMail;
import play.mvc.Controller;
import play.mvc.With;
import resources.AccountType;
import resources.CacheKey;
import resources.UserType;
import resources.helper.ServiceSerializer;
import view_serializers.MbSerializer;

public class Oauth2Callback extends Controller{ 

	public static void index_gmail(){
		String url = DGOauthGrant.authorization_url();
        render(url);
	}
	
	public static void gmail(){   
		String code = request.params.get("code");
    	String error = request.params.get("error");
    	String state = request.params.get("state");
    	String domain = Security.connectedDomain();
    	String disp = error;
    	if(error != null){
    		Mailboxes.index(domain,"new");
    	}else{
    		
    		String apikey = Security.connectedApikey();
    		if(apikey!=null){
    			Userinfo user = DGOauthGrant.authorize(apikey,code, state);
    			
    			if(user!=null){
    				
	    			Channel channel = Channel.findByApikey(apikey);
	    			MailboxConfig config = MailboxConfig.findByMailServer("Gmail");
	    			MbSerializer mb = new MbSerializer(user.getEmail(), user.getName(), code, config,true);
	    			
	    			//Set all new created mailboxes as managed
	    			mb.setMonitored(false);
					if(mb.getMonitored()){
						ServiceSerializer service = CacheKey.getMMServiceSerializer(apikey,CacheKey.MonitoredMailboxes, SubServiceName.MonitoredMailbox);
						if(service==null || service.hasExceeded){
							mb.setMonitored(false);
						}
					}
					
	    			int status = channel.addInboundMb(mb.getInboundMailbox(), Security.connectedDomain(), true);
	    			CacheKey.delListOfRepFromCache(apikey,CacheKey.MonitoredMailboxes);
	    			
	    			
	    			//if the user is an agent add as role
	    			if(Security.connectedUserType().equals(UserType.Agent)){
	    				AccountUser acc = AccountUser.findByAgent(Security.connected(), apikey);
	    				if(acc!=null){
	    					 acc.addAgentRole(user.getEmail());
	    					 Inbox.index(domain, null, null);
	    				}
	    			}
	    			
	    			if(status==200)
	    				Mailboxes.index(domain,mb.getEmail());
	    			
    			}  
    			Mailboxes.index(domain,"new"); 
    		} 
    	}
    	
		render(disp);
	} 
	
	public static void exchangecode(String state, String code, String error){
		for(String key :request.params.allSimple().keySet()){
			play.Logger.log4j.info(key+" : "+request.params.allSimple().get(key));
		}
		
		renderJSON("");
	}
}
