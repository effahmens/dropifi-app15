package controllers;

import play.mvc.Controller;
import play.mvc.With;

@With(Compress.class)
public class Blogger extends Controller {
	
	public static void index(String publickey){			 
		render(publickey);
	}
}
