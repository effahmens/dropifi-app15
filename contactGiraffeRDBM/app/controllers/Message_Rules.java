package controllers; 
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import models.Channel;
import models.Company;
import models.MessageManager;
import models.MsgRule;
import models.MsgSubject;
import models.RuleAction;
import models.RuleCondition;
import models.WidgetTracker;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import resources.DCON;
import resources.EcomBlogType;
import resources.RuleGroup;
import resources.UserType;
import view_serializers.MbSerializer;
import view_serializers.RuleSerializer;
@With(Compress.class)
public class Message_Rules extends Controller{ 
	@Before
    static void setArgs(String domain){
		 String apikey = Security.connectedApikey();	
	 		String foundApikey = Company.findApikey(apikey);
	 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
	 			
	 			if(UserType.isAgent(Security.connectedUserType()))
	 				Dashboard.index(domain);
	 			
	 			 renderArgs.put("page","admin");
	 	        renderArgs.put("sub_page","mail");
	 	        renderArgs.put("title","Admin - Mail Management (Message Rules)");
	 		}else{
	 			//clear the session
	 	    	session.clear();
	 	    	//response.removeCookie("rememberme");
	 			
	 			try {
	 				Secure.login("");
	 			} catch (Throwable e) {
	 				Application.index();
	 			}
	 		}
       
    }	
	
	public static void index(String  domain){
		ForceSSL.redirectToHttps();
		
		String apikey = Security.connectedApikey();		
		if(!validation.hasErrors() && apikey !=null){
			String connected = Security.connectedDomain(); 
			domain =domain!=null? domain.trim():"";  			
 			//if(connected.equalsIgnoreCase(domain)){ 
			if(!connected.equalsIgnoreCase(domain)){			
				index(connected);
			}
			String connectedUsername = Security.connected();
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			//check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
			render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget); 

 			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		} 
		
		Apps.page_not_found("");
	}
	
	public static void populate(){
		JsonObject json = new JsonObject();
		int status = 404;
		String apikey = Security.connectedApikey();
		int size =0;
		if(apikey !=null){			 
			List msgrules = MsgRule.findAll(Security.connectedCompanyId()); 		
			//check the status of the mailbox: if size 0 then return 200 meaning mailbox(s) is/are available
			size = msgrules.size(); 
			status = (msgrules!= null && size>0) ? 200:501; 					
			json.add("msgRules",RuleSerializer.toJsonArray(msgrules));
		}		
		json.addProperty("status", status); 
		json.addProperty("size", size);
		
		renderJSON(json.toString()); 
	}
	
	public static void create(@Required String title,@Required boolean activated,@Required String ruleGroup, @Valid List<RuleCondition>conditions, @Valid List<RuleAction>actions){
		//TODO: provide method for save the quickResponse
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		play.Logger.log4j.info("Condition: "+title);
		 
		if(!validation.hasErrors()){ 				
			String apikey = Security.connectedApikey();
			if(apikey != null){
				//create a new quickResponse
				MessageManager msg = MessageManager.findByApikey(apikey); 
				if(msg != null){  
					long companyId = Security.connectedCompanyId();
					MsgRule rule = new MsgRule(companyId,title,activated,RuleGroup.valueOf(ruleGroup)); 
					rule.addConditions(conditions); 
					rule.addActions(actions);				
					status = msg.addMsgRule(rule); 
					json.addProperty("error", (status== 200? "none" : (status==305? "duplication of rule title. Change the title of the rule":"could not save the successful")) );
				}
			}else{
				status = 405; 
			}
			
		}else{
			json.addProperty("error", "invalid title");
		}
		
		if(status == 200){
			json.addProperty("title",title); 
		}else{
			json.addProperty("title","new"); 
		}
		
		json.addProperty("status", status);  		 
		renderJSON(json.toString());
	}
	
	public static void update(@Required String originalTitle, @Required String title,@Required boolean activated,@Required String ruleGroup, @Valid List<RuleCondition>conditions, @Valid List<RuleAction>actions){
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		if(!validation.hasErrors()){   
			//search for the connected company	
			String apikey = Security.connectedApikey(); 
			
			if(apikey != null){						
				//update the changes to the quick response 
				status = MsgRule.updateRule(Security.connectedCompanyId(), originalTitle, title, activated, RuleGroup.valueOf(ruleGroup), conditions, actions); 
				json.addProperty("error", (status==200? "none" : (status==305 ? "duplication of title. A rule with similar title is already created": "update was not successful")) ); 
			
			}else{ 
				status=405; 
			}
		}else{
			json.addProperty("error", "none");
		}
		
		if(status == 200){
			json.addProperty("title",title); 
		}else{
			json.addProperty("title",originalTitle); 
		}
		
		json.addProperty("status", status); 		
		renderJSON(json.toString());	
	}
	
	public static void delete(@Required String name){ 
		JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey(); 			
			if(apikey != null){					 					
				//update the changes to the mailbox
				long companyId = Security.connectedCompanyId();
				status = MsgRule.removeMsgRule(companyId,name);
				json.addProperty("error", (status==200? "none" : "update was not successful") );
			}
			
		}
		
		json.addProperty("status", status); 
		json.addProperty("title", name); 
		renderJSON(json.toString());
	}
	
	public static void activateRule(String title, boolean activated){
		JsonObject json = new JsonObject(); 
		int status = 404;
		if(!validation.hasErrors()){			
			//search for the connected company
			String apikey = Security.connectedApikey();			
			if(apikey != null) {				
			 	long companyId = Security.connectedCompanyId();				 					
				//update the changes to the mailbox
				status =MsgRule.updateActivated(companyId, title, activated);
				json.addProperty("error", (status==200? "none" : "update was not successful") ); 			
			}			
		}
		
		json.addProperty("status", status); 
		json.addProperty("title", title); 
		renderJSON(json.toString());
	}
	
	public static void show(@Required String msgrule, @Required String domain){
		ForceSSL.redirectToHttps();
		String btnSave = ""; 
		String connected = Security.connectedDomain(); 			
		String apikey = Security.connectedApikey();
			if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){	
			if(validation.required(msgrule).ok){
				List<String> quickResponses = MessageManager.listOfQuickResponseTitles(Security.connectedApikey());
				
				//check if a rule is specified 		
				if(msgrule.equals("new")){
					 btnSave ="Save";  
					 render(btnSave,quickResponses,msgrule); 
				}else{
					 long companyId= Security.connectedCompanyId(); 
					 MsgRule messageRule = MsgRule.findByName(companyId, msgrule);
					 btnSave ="Update"; 			
					 render(btnSave,quickResponses,messageRule,msgrule,domain); 				
				}
				 
			}else{
				msgrule = "new";
				btnSave = "Save"; 
				render(btnSave,msgrule,domain);
			}
		}
		Apps.popout_not_found("");
	}

}
