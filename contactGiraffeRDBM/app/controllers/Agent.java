package controllers;
 
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import models.AccountUser;
import models.AgentRole;
import models.Channel;
import models.Company;
import models.InboundMailbox;
import models.MessageManager;
import models.WMsgRecipient;
import models.WidgetTracker;
import models.dsubscription.SubConsumeService;
import models.dsubscription.SubServiceName; 
import models.dsubscription.SubServiceType;
import play.cache.Cache;
import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.*;  
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.UserType;
import resources.helper.ServiceSerializer;
import resources.helper.ServiceVolumeSerializer;
import view_serializers.MbSerializer;
import view_serializers.UserRoleSerializer;

/**
 * An Agent is a customer support staff from your company whom you want to be able to collaborate on customer cases.
 * @author philips
 *
 */
@With(Compress.class)
public class Agent extends Controller{
	
	@Before
	static void setArgs(String domain){
		String apikey = Security.connectedApikey();	
		String foundApikey = Company.findApikey(apikey);
		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
			
			if(UserType.isAgent(Security.connectedUserType()))
 				Dashboard.index(domain);
			
	        renderArgs.put("page","admin");
	        renderArgs.put("sub_page","team");
	        renderArgs.put("title","Admin - Users");
		}else{
			//clear the session
	    	session.clear(); 
			try {
				Secure.login("");
			} catch (Throwable e) {
				Application.index();
			}
		}
    } 
        
    public static void index(String domain) {
    	ForceSSL.redirectToHttps();
    	
    	String apikey = Security.connectedApikey();	 
    	if(!validation.hasErrors() && apikey !=null) {
    		String connected = Security.connectedDomain();
    		domain =domain!=null? domain.trim():"";
    		if(!connected.equalsIgnoreCase(domain)){			
				index(connected);
			}
			
		    	String connectedUsername = Security.connected();
		    	String userType = Security.connectedUserType().name();
		    	
		    	String ecomBlogPlugin = Security.connectedPlugin();
		    	boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.getValue(ecomBlogPlugin));
				render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget);
 			/*}else{
 				//check if the domain is available
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
    	
    	Apps.page_not_found("");
    }
    
    public static void populate(){
    	JsonObject json = new JsonObject();
    	Gson gson = new Gson();
    	
		int status = 404;
		String apikey = Security.connectedApikey(); 
		List agents = null; 
		int size = 0;
		String jsonString = "";
		
		if(apikey !=null){
			//get the message recipients from cache
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Agent, DropifiTools.POPULATE);
			jsonString = Cache.get(cacheKey, String.class);
			
			if(jsonString==null || jsonString.trim().isEmpty()){
				agents = AccountUser.findAllAgentProfile(apikey);	
			
				//check the status of the mailbox: if size 0 then return 200 meaning agent(s) is/are available
				size = agents.size();
				status = (agents!= null && size>0) ? 200:501;					
				json.add("agents", AccountUser.toJsonArray(agents));
				json.addProperty("status", status); 
				json.addProperty("size", size);
				
				//update the total volume consumed
				try{
					ServiceVolumeSerializer volSerializer = new ServiceVolumeSerializer(apikey, SubServiceName.SupportAgent, SubServiceType.FixedVolume, size);
					SubConsumeService.updateConsumedVolume(volSerializer); 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e); 
				}
				
				ServiceSerializer service = SubConsumeService.findServiceSerializer(apikey, SubServiceName.SupportAgent);
				json.add("service", gson.toJsonTree(service)); 
				
				//cache the service
				CacheKey.cacheSubService(apikey,CacheKey.Agent,service,DropifiTools.SUBSERVICE);
				
				try{
					//set the recipients to cache
					jsonString = json.toString(); 
					Cache.set(cacheKey, jsonString); 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
				
				//play.Logger.log4j.info("Populate Message Recipients : Not Queried from cache");
			}/*else{
				play.Logger.log4j.info("Populate Message Recipients : Queried from cache");
			}*/
		}else{ 
			json.addProperty("status", status); 
			json.addProperty("size", size);
			jsonString = json.toString();
		}
		renderJSON(jsonString);
    }
    
    public static void create(@Required String username, @Required String email, List<String> userRoles){
    	//TODO:
    	
		JsonObject json = new JsonObject();
		int status= 404; 		 
		if(!validation.hasErrors() && validation.email(email).ok){
		
 			String apikey = Security.connectedApikey();
 			
			if(apikey != null){
				//check if the limit on support-staff service has exceeded
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Agent, SubServiceName.SupportAgent);
				String msg="";
				String domain = Security.connectedDomain();
				
				if(service!=null && !service.hasExceeded){
					//create a new agent 
					AccountUser user = new AccountUser(username,email,apikey);
					status = user.createAgent(null); 				
				 	msg = (status==200 ? ("The user has been added successfully and login credentials has been sent to "+user.getProfile().getEmail()) :(status==305? "the email address "+email+" is taken" : "adding of new agent failed"));		
				 	
				 	//delete the list of recipients from the cache
					CacheKey.delListOfRepFromCache(apikey,CacheKey.Agent);
				}else{
					msg = "<div class='main_upgrade'><a class='link_upgrade' href=/"+domain+"/admin/pricing/index' target='_blank'>Upgrade to add more users</a></div>";
				}
				json.addProperty("error",msg);
			}else{ 
				status = 405;
			}
			
		}else{
			json.addProperty("error", "invalid email address format");
		}
		
		json.addProperty("status", status);	
		json.addProperty("email", email);	
		renderJSON(json.toString());
    }
    
    public static void update(@Required String username, @Required String email, List<String> userRoles){ 
    		 
    	JsonObject json = new JsonObject();
		int status= 404; 		 
		if(!validation.hasErrors() && validation.email(email).ok){
 			String apikey = Security.connectedApikey();
			if(apikey != null){
				
				//create a new agent				
				status = AccountUser.updateAgent(email, apikey, username, userRoles); 				
			 	json.addProperty("error", (status==200? "Agent updated successfully" :(status==501? "There is no user with the email address "+email: "update was not successful")) );	
			 	
			 	//delete the list of recipients from the cache 
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Agent); 
			}else{
				status = 405;
			}
			
		}else{
			json.addProperty("error", "invalid email address format");
		}
		
		json.addProperty("status", status);	
		json.addProperty("email", email);	
		renderJSON(json.toString());
    }
    
    public static void delete(@Required String email){
    	JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors() && validation.email(email).ok){ 
			
			//search for the connected company	
			String apikey = Security.connectedApikey();
			
			if(apikey != null){				 
				status = AccountUser.deleteAgent(email, apikey);					
				json.addProperty("error", (status==200? "none" : (status==501? "unknown user email "+email: "delete was not successful")) );	
				
				//delete the list of recipients from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Agent);
			}		
		} 	
		
		json.addProperty("status", status); 
		json.addProperty("email", email); 
		renderJSON(json.toString()); 
    }
    
    public static void show(@Required String email, @Required String domain){
    	ForceSSL.redirectToHttps();
    	//search for the connected company	
    	String connected = Security.connectedDomain(); 			
		String apikey = Security.connectedApikey();
		
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){
	    	String btnsave = "";    
	    	String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			
			ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Agent, SubServiceName.SupportAgent);	
			
			if(validation.required(email).ok){ 
				//check if an email is specified			 
				if (validation.email(email).ok && !email.equals("new")){
					
					//search for the account details
					AccountUser user = AccountUser.findByEmailAndApikey(email, apikey);
					
					if(user !=null){  
						Collection<UserRoleSerializer> roles = user.assignedRoles(); 
						
						String username = user.getProfile().getUsername();
						String useremail = user.getProfile().getEmail();
						btnsave ="Update";				
						render(btnsave,email,useremail,username,roles,domain,connectedUsername,ecomBlogPlugin,userType,service);
					}
				} 			
			}
			
			List<UserRoleSerializer> roles = AccountUser.unAssignedRoles(apikey);
			email="new";
			btnsave ="Save"; 
			render(btnsave, email,roles,domain,connectedUsername,ecomBlogPlugin,userType,service); 		 
		}
		
		Apps.popout_not_found("");
    }
    
    public static void activateAgent(@Required String email, @Required boolean activated){
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){  
				//search for the account details
				AccountUser user = AccountUser.findByEmailAndApikey(email, apikey);
				
				if(user != null){ 	
					status = user.activateUser(activated); 				
					json.addProperty("error", (status==200?"none":"update was not successful") );
					play.Logger.log4j.info(request.method+" : "+ status);
					//delete the list of recipients from the cache
					CacheKey.delListOfRepFromCache(apikey, CacheKey.Agent); 
				} 
			} else {
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error","required fields are not specified");
		}
		
		json.addProperty("activated", activated);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
    
    public static void orderAgents(@Required List<String> agents){
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();	 
			if(apikey != null){
				//status = AccountUser.findByEmailAndApikey(email, apikey);
				json.addProperty("error", (status==200?"none":"update was not successful") );
				
				//delete the list of recipients from the cache
				CacheKey.delListOfRepFromCache(apikey, CacheKey.Agent); 
			}
		}else{
			json.addProperty("error", "required fields are not specified");
		} 
		
		json.addProperty("status", status); 	
		renderJSON(json.toString());
	}
}
