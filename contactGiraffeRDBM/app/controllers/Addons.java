package controllers;

import static akka.actor.Actors.actorOf;

import java.util.List;
import java.util.Map;

import job.process.EventTrackActor;

import org.json.JSONException;  

import akka.actor.ActorRef;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Company;
import models.MessageManager;
import models.WidgetDefault;
import models.WidgetTracker;
import models.addons.AddonDownGrade;
import models.addons.AddonPlatform;
import models.addons.AddonSubscription;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubServiceName;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.AddonHelper;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType; 
import resources.EventTrackSerializer;
import resources.addons.AddonStatus;
import resources.addons.SocialReviewClient;
import resources.helper.ServiceSerializer;
import view_serializers.ClientWidgetSerializer;
import view_serializers.SubjectSerializer;

@With(Compress.class)
public class Addons extends Controller{
	
	@Before
	static void setArgs(@Required String domain){	
		if(validation.hasError("domain"))
			ForceSSL.logout(); 
		
		String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
 			renderArgs.put("page","addons");					 		     
 		}else{ 		
 			ForceSSL.logout();
 		}
	} 
	 
	
	public static void index(String domain){
		Dashboard.index(domain); 
		
		ForceSSL.redirectToHttps();
		String connected = Security.connectedDomain(); 
    	domain =domain!=null? domain.trim():"";
    	
		if(!connected.equalsIgnoreCase(domain)){			
			index(connected);
		}
		
		renderArgs.put("title","Admin - Addons"); 
		renderArgs.put("sub_page","addons"); 
		String apikey = Security.connectedApikey();
		
		if(apikey!=null){
			String connectedUsername = Security.connected();
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.getValue(ecomBlogPlugin));
			List<AddonPlatform> addons = AddonPlatform.findAllAddons(); 
			boolean hasCard = Company.hasCard(apikey); 
			render(connectedUsername,userType,ecomBlogPlugin,hasWidget,domain,addons,hasCard);
		} 
	}
	
	public static void installs(String domain){
		Dashboard.index(domain); 
		
		ForceSSL.redirectToHttps(); 
		String connected = Security.connectedDomain(); 
    	domain =domain!=null? domain.trim():"";
    	
		if(!connected.equalsIgnoreCase(domain)){			
			installs(connected);
		}
		renderArgs.put("title","Admin - Addons"); 
		renderArgs.put("sub_page","addons");
		String apikey = Security.connectedApikey();
		
		if(apikey!=null){ 
			String connectedUsername = Security.connected();
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.getValue(ecomBlogPlugin));
			
			render(connectedUsername,userType,ecomBlogPlugin,hasWidget,domain);
		}
	}
	
	public static void page(String domain, String title){
		Dashboard.index(domain); 
		
		ForceSSL.redirectToHttps();
		String connected = Security.connectedDomain(); 
    	domain =domain!=null? domain.trim():"";
		if(!connected.equalsIgnoreCase(domain)){			
			page(connected,title);
		}
		renderArgs.put("title","Admin - Addons"); 
		renderArgs.put("sub_page","addons");
		String apikey = Security.connectedApikey();
		
		if(apikey!=null){   
			String connectedUsername = Security.connected();
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			boolean hasCard = Company.hasCard(apikey);
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.getValue(ecomBlogPlugin));
			AddonPlatform addon = AddonPlatform.findByTitle(title);    
			
			render(connectedUsername,userType,ecomBlogPlugin,hasWidget,domain,addon,hasCard);
		}
	} 
	
	public static void populate(){ 
		JsonObject json = new JsonObject();
		String apikey = Security.connectedApikey(); 
		int size=0;
		if(apikey!=null){
			JsonArray addons = AddonSubscription.toJsonArray(apikey); 
			json.add("addons", addons);
			if(addons!=null){
				size = addons.size();
			}
		} 
		json.addProperty("size", size);
		renderJSON(json.toString());
	}
	
	public static void delete(@Required String platformKey,@Required String title){
		JsonObject json = new JsonObject(); 
		int status = 404;
		
		String apikey = Security.connectedApikey();
		if(!validation.hasErrors() && apikey!=null){
			SubCustomer customer = SubCustomer.findByAppikey(apikey);
			if(customer!=null){
				AddonSubscription found = AddonSubscription.findAddon(apikey, platformKey);
				if(found!=null){ 
					boolean isSubCancelled = true;
					if(found.getSubscriptionId()!=null){
						isSubCancelled = customer.cancelAddonSubscription(DropifiTools.StripeSecretKey, found.getSubscriptionId())!=null?true:false;
					}
					
					if(isSubCancelled && found.deleteAddon()==200){
						if(DropifiTools.SOCIAL_REVIEW.equals(title)){
							EventTrackSerializer.trackValueActor(customer.getEmail(),"SocialReview",AddonStatus.Deleted.name()); 
							resetWidget(apikey);
						}
					}
				} 
			}
		}
		
		json.addProperty("status", status);  
		renderJSON(json.toString());
	}
	
	public static void activateAddon(@Required String platformKey,@Required String title, @Required  boolean activated){
		JsonObject json = new JsonObject(); 
		int status = 404;
		String apikey = Security.connectedApikey();
		if(!validation.hasErrors() && apikey!=null){
			status = AddonSubscription.updateActivated(apikey, platformKey, activated);
			
			if(DropifiTools.SOCIAL_REVIEW.equals(title)){
				resetWidget(apikey);
			}
		}
		
		json.addProperty("status", status);  
		renderJSON(json.toString());
	}
	
	public static void subscription(String domain,  String platformKey, String title, String token,String msg){
		
		ForceSSL.redirectToHttps();
		String connected = Security.connectedDomain(); 
    	domain =domain!=null? domain.trim():"";
		if(!connected.equalsIgnoreCase(domain)){			
			domain = connected;
		}
		renderArgs.put("title","Admin - Addon Subscription");
		renderArgs.put("sub_page","addons");  
		String apikey = Security.connectedApikey();
		
		if(apikey!=null){ 
			
			String connectedUsername = Security.connected();
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			Boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.getValue(ecomBlogPlugin));
			AddonPlatform addon = AddonPlatform.findByTitle(title);
			List<String> years = DropifiTools.listOfYears(); 
			String stripePublickey = DropifiTools.StripePublisahblekey;
			if(domain==null) domain = Security.connectedDomain();
			if(msg==null) msg = title+" addon subscription was not created. Add your Debit/Credit card information";
			
			render(connectedUsername,userType,ecomBlogPlugin,hasWidget,domain,addon,years,stripePublickey,msg);
		}
	}
	
	public static void subscribeToPlan(String domain,String platformKey,String title,String token,String coupon,String msg){ 
		String apikey = Security.connectedApikey();
		if(apikey!=null){
			try{ 
				switch(title){
					case DropifiTools.SOCIAL_REVIEW:
						if(AddonHelper.installSocialReview(apikey, platformKey,title,token, coupon)==200){
							AddonDownGrade.updateAfterPayment(platformKey, apikey,30);
							CacheKey.delAddonFromCache(apikey);
							resetWidget(apikey);
							social_inbox(domain);
					}
					break; 
				}  
				page(domain, title); 				 
			}catch(Exception e){ 
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		index(domain); 
	}
	
	public static void subscribeToAddonPlan(@Required String platformKey,@Required String title,String domain,String token,@Required String subscriptionPlan, @Required String stripeToken, String stripeCardType, String cardName,String creditcode){
		String msg = "Some of the required fields are not specified";

		String apikey = Security.connectedApikey();
		if(!validation.hasErrors() && apikey!=null){
			SubCustomer cus = Pricing.isSubscribed(apikey);
			
       		Map<String,Object> subcribed = Pricing.subscribeToPlan(cus.getCurrentPlan().name(), cus.getSubscriptionType().name(), stripeToken, stripeCardType, cardName, creditcode,false);
       		
       		if(subcribed!=null){
				int status =(int)subcribed.get("status");
				msg = (String)subcribed.get("msg");
				if(status==200){ 
					subscribeToPlan(domain, platformKey, title, stripeToken, creditcode,msg);
				}
			}
		}
		
		subscription(domain, platformKey, title, token, msg);
	}
 
	public static void social_inbox(String domain){
		Dashboard.index(domain);
		
		ForceSSL.redirectToHttps();
		renderArgs.put("page","social");
		renderArgs.put("title","Social Reviews - Inbox");
		
		String apikey = Security.connectedApikey();	
		
	 	if(apikey!=null){	
	 		String connected = Security.connectedDomain();
	 		domain =domain!=null? domain.trim():"";
	 		if(!connected.equalsIgnoreCase(domain)){			
	 			social_inbox(connected);
			}
			
				 //check if the social review addon is installed.
				AddonSubscription addonSub = AddonSubscription.findAddonByTitle(apikey,DropifiTools.SOCIAL_REVIEW,AddonStatus.Installed); 
				if(addonSub!=null){
					//Check if the user free try has expired and redirect the user to the addon subscription page
					if(!CacheKey.hasAddonExpired(apikey,addonSub.getPlatformKey())){
					
						 String connectedUsername = Security.connected();
						 String userType = Security.connectedUserType().name();
						 String ecomBlogPlugin = Security.connectedPlugin();
						 //check if the widget is installed on the site of the user
						 boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
						 String uid = addonSub.getUid();
						 
						 //check if the social account is created on Kudobuzz
						 SocialReviewClient client = SocialReviewClient.getSocialUID(addonSub.getSecretKey(),connectedUsername);
						  					 
						 if(client!=null && client.getUid()!=null && !client.getUid().isEmpty())
							 render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget,uid);
						 else
							page(domain,DropifiTools.SOCIAL_REVIEW); 
					}else{
						subscription(domain, addonSub.getPlatformKey(), DropifiTools.SOCIAL_REVIEW, " ", "Your trial period has expired. Add your Debit/Credit card information");
					}
				}else{
					page(domain,DropifiTools.SOCIAL_REVIEW);
				}   
			/*}else{
				//check if the domain is available  
				boolean hasDomain = Company.isDomainAvailable(domain);
				if(hasDomain){ //show domain not found page
					Apps.multiple_login(connected); 
				}else{
					Apps.domain_not_found(domain);
				}
			}*/
		} 
	 	
	 	Apps.page_not_found(" "); 
	}
	
	
	@Util
	public static void resetWidget(String apikey){
		WidgetDefault defaultWidget = WidgetDefault.findByApiKey(apikey);
		if(defaultWidget!=null){ 
			defaultWidget.updateSocialUID(AddonSubscription.getUID(apikey, DropifiTools.SOCIAL_REVIEW,AddonStatus.Installed,true));
			//String cacheKey = CacheKey.getWidgetTabKey(defaultWidget.getPublicKey());
			if(defaultWidget.getWiParameter().getButtonFont()==null){ 
				defaultWidget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
			} 
			ClientWidgetSerializer clientWidget = new ClientWidgetSerializer(defaultWidget.getApikey(), defaultWidget.getPublicKey(), defaultWidget.getWiParameter(),defaultWidget.getSocialUID());
			CacheKey.set(CacheKey.getWidgetTabKey(defaultWidget.getPublicKey()), clientWidget,"1440mn"); 
		}
	}
}
