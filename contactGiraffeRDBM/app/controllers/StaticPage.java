/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import play.mvc.Controller;
import play.mvc.With;

/**
 * 
 * @author davidosei
 * 
 */
@With(Compress.class)
public class StaticPage extends Controller {
    
    public static void page(String page){
        render(page+".html");
    }
}
