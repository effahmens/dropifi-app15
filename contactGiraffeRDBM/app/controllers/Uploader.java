package controllers;

import java.io.File;
import java.net.URI;

import api.contactexport.ContactExportClient;

import com.google.gson.JsonObject;

import models.Company;
import models.FileDocument;
import models.dsubscription.SubServiceName;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.libs.Images;
import play.libs.MimeTypes;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import query.inbox.InboxSelector;
import resources.CacheKey;
import resources.DropifiTools;
import resources.FileType;
import resources.UserType;
import resources.helper.ServiceSerializer;
import view_serializers.AttSerializer;

//@With(Compress.class) 
public class Uploader extends Controller{
	
	//@Before
	@Util
	public static void setArgs(String domain){     
    	String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
 			//if(UserType.isAgent(Security.connectedUserType()))
 				//Dashboard.index(domain); 
 		}else{
 			//clear the session
 	    	session.clear();
 	    	//response.removeCookie("rememberme");
 			try {
 				Secure.login("");
 			} catch (Throwable e) {
 				Application.index();
 			}
 		}
	 }
	
	public static void handleFileUpload(@Required File file){
		int status = 501;
		String msg="<p class='error'><strong>Upload failed!</strong> An error ocurred while loading the file. Try again</p>";
		try{
			if(!validation.hasErrors()){
				String domain = Security.connectedDomain();
				String apikey = Security.connectedApikey();
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
				if(service!=null && !service.hasExceeded){
					FileDocument doc = new FileDocument(apikey,FileType.AutoReplyLogo.name().toLowerCase()+".png", FileType.AutoReplyLogo);
					if(doc.isImageFile(doc.retrieveFileExtension(file))){
						
						Double size = doc.getFileSize(file).get("kilobytes"); 
						
						if(size!=null){  
							if(size<=211){
								String BUCKETNAME = play.Play.mode.isProd()?DropifiTools.AWSWidgetTabBucketProd :DropifiTools.AWSWidgetTabBucketDev;
								if(doc.uploadPublicFileToRackspace(BUCKETNAME, domain, file)!=null){
									msg ="<p class='success'><strong>Uploaded successfully</strong></p>";
									status =200; 
								}else{
									msg ="<p class='error'><strong>Upload failed!</strong> Try uploading the image again.</p>";
								}
							}else{
								msg ="<p class='error'><strong>Upload failed!</strong> The file size should be less than 210KB</p>";
							}
						}else{
							msg="<p class='error'><strong>Upload failed!</strong> The file does not exist</p>";
						}
					}else{
						msg="<p class='error'><strong>Upload failed!</strong> The file is not an image file</p>"; 
					}
				}else{
					msg="<p class='info'><strong><a href='/"+domain+"/admin/pricing/index' target='_blank' style='color:#ffffff;font-size:12px;'>Upgrade to replace dropifi logo with your logo on Auto Reply</a></strong></p>";
				} 
			}else{
				msg="<p class='error'><strong>Upload failed!</strong> No file selected </p>";
			}
		}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}
		
		//Widgets.show_upload_logo(msg); 
		JsonObject json = new JsonObject();
		json.addProperty("status", status);
		json.addProperty("msg", msg);
		renderHtml(json.toString());
	}
	
	public static void downloadAttachment(@Required long id,@Required String msgId,@Required String sender,@Required String fileName, @Required String attachmentId){
		try{
			if(!validation.hasErrors()){ 
				AttSerializer attachfile = new AttSerializer(id, Security.connectedDomain(), sender, msgId, fileName, attachmentId);
				attachfile.apikey=Security.connectedApikey(); 
				
				response.setContentTypeIfNotSet(new MimeTypes().getContentType(fileName)); 
				URI uri = attachfile.getRackspaceURI();
				if(uri!=null) {
					redirect(uri.toString());
				} 
				renderBinary(attachfile.getFileFromCloud().get(),fileName); 
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		renderHtml("The file you are trying to download does not exit");
	}
	
	public static void viewAttachment(@Required long id,@Required String msgId,@Required String sender,@Required String fileName, @Required String attachmentId){
		try{
			if(!validation.hasErrors()){ 
				AttSerializer attachfile = new AttSerializer(id, Security.connectedDomain(), sender, msgId, fileName, attachmentId);
				attachfile.apikey=Security.connectedApikey();  
				
				response.setContentTypeIfNotSet(new MimeTypes().getContentType(fileName));
				URI uri = attachfile.getRackspaceURI();
				if(uri!=null) {
					redirect(uri.toString());
				}
				renderBinary(attachfile.getFileFromCloud().get()); 
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		renderHtml("The file you are trying to view does not exit");
	}
	
	public static void downloadAllAttachments(@Required long id,@Required String msgId,@Required String sender,@Required String fileName, @Required String attachmentId){
		render();
	}
	
	public static void downloadContacts() {
		setArgs(Security.connectedDomain());
		Long companyId = Security.connectedCompanyId();
		if(companyId!=null) { 
			ContactExportClient export = new ContactExportClient(Security.connectedDomain());
			String fileName = export.saveExcel(CacheKey.findServiceSerializerBrand(Security.connectedApikey(), SubServiceName.Branding),
					export.contactKeyOrder(),InboxSelector.findContacts(companyId));
			
			response.setContentTypeIfNotSet(new MimeTypes().getContentType(fileName)); 
			renderBinary(new File(fileName),fileName);  
		}
		renderHtml("you are not authorized to download this file.");
	}
}
