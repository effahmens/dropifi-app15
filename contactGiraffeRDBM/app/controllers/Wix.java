package controllers;

import static akka.actor.Actors.actorOf;

import java.util.Arrays;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.Cookie;

import job.process.EventTrackActor;
import job.process.MailingActor;
import job.process.WidgetTrackerActor;
import models.Company;
import models.CompanyProfile;
import models.EcomBlogPlugin;
import models.WiParameter;
import models.Widget;
import models.WidgetDefault;
import models.WixDefault;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import akka.actor.ActorRef;
import api.wix.WixClient;

import com.apache.commons.codec.binary.Base64;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import play.cache.Cache;
import play.data.validation.Required;
import play.libs.Codec;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import resources.WidgetTrackerSerializer;
import resources.WixSerializer;
import resources.AccountType;
import view_serializers.CaptchaSerializer;
import view_serializers.ClientWidgetSerializer;
import view_serializers.MailSerializer;
import view_serializers.WCParameter;

@With(Compress.class)
public class Wix extends Controller{
	
	public static void index(String uid,String instanceId,String compId, String origCompId){ 
		String email = null;
		AccountType accountType=null;
		WCParameter wixParam = null;
		WixDefault wix = WixDefault.findWix(instanceId);
		if(wix!=null){
			accountType = wix.getAccountType();
			email = wix.getEmail();
			wixParam = new WCParameter(wix.getWiParameter());
		}
		String serverUrl =DropifiTools.ServerURL;
		render(email,accountType,wixParam,uid,instanceId,compId,origCompId,serverUrl);  
	} 
	
	public static void login(@Required String userEmail,@Required String temToken,String IPAddress,String page){ 
		
		if(!validation.hasErrors()){ 
			try{	 
				 //find the login credentials of the user
				 Map<String,String> credentials = Company.authenticate(userEmail, temToken,EcomBlogType.Wix);
				 if(credentials != null){
					String credPassword = (String)credentials.get("hashPassword"); 
						
					if(credPassword!=null && temToken!=null && temToken.compareTo(credPassword) == 0){
						credentials.put("userEmail", userEmail); 
						credentials.put("IPAddress", IPAddress!=null?IPAddress:""); 
						Security.setLoginSession(credentials);
						//redirect the owner to the widget customization page
						if(page==null || page.trim().isEmpty())
							redirect("/"+credentials.get("domain")+"/admin/widgets");
						else
							redirect("/"+credentials.get("domain")+"/admin/"+page);
					}  					 			 
				 } 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		try {
			session.all().clear();  
			Secure.login("");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}	 
		
	}
	
	public static void signup(String instanceId,String uid,String compId, String origCompId,String baseUrl,boolean accountExist,String error){
		Boolean existing_account =null;
		Boolean new_account =null;
		if( accountExist==false)
			new_account=true;
		else
			existing_account = true;
		
		render(instanceId,uid,compId, origCompId,baseUrl,new_account,existing_account,error);
	} 
	
	public static void settings(String instance, Integer width, String compId,String origCompId){            
		String instanceId=null;
		String uid =null;
		try{
			JsonNode json  = WixClient.getInstance(instance, DropifiTools.WixSecretkey);
			if(json!=null){
				instanceId = WixClient.getString(json.get("instanceId")); 
				uid = WixClient.getString(json.get("uid")); 
				
				//update the user account type if he has subscribe to wix premium
				String vendorProductId = WixClient.getVendorId(json.get("vendorProductId"));
				if(instanceId!=null && vendorProductId!=null && vendorProductId.equalsIgnoreCase(DropifiTools.WixVendorProductId)){
					Company found = Company.findByEcomBlogKey(instanceId, EcomBlogType.Wix);
					if(found!=null){
						found.subscribeWixToPlan(vendorProductId,instanceId, EcomBlogType.Wix);
					}
				}
			} 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e); 
		}
		
		index(uid,instanceId,compId, origCompId);
	}
	
	public static void widget_mobile_page(@Required String instance, Integer width, String compId,String origCompId,String viewMode,String target){
		 
		widget_page(instance, width, compId, origCompId, viewMode, target);
	}

	public static void widget_page(@Required String instance, Integer width, String compId,String origCompId,String viewMode,String target){
		if(!validation.hasErrors()){			
			JsonNode json  = WixClient.getInstance(instance, DropifiTools.WixSecretkey); 
			ClientWidgetSerializer clientWidget = null;
			if(json!=null){
				try{
					
					String instanceId = WixClient.getString(json.get("instanceId")); 
					String uid = WixClient.getString(json.get("uid"));
					String sKey = CacheKey.getWixKey(instanceId);
					String publicKey = Cache.get(sKey,String.class);  
					String serverUrl = DropifiTools.ServerURL; 
					
					if(publicKey==null || publicKey.trim().isEmpty()){
						if(uid!=null && instanceId!=null){
							publicKey = Company.findPublickey(uid, instanceId,EcomBlogType.Wix);
						}else{
							publicKey = Company.findPublicBySecretkey(instanceId, EcomBlogType.Wix);
						}
						
						if(publicKey!=null && !publicKey.trim().isEmpty()){
							Cache.set(sKey, publicKey); 
						}
					} 
					
					//load data from cache  				
					if(publicKey!=null && !publicKey.trim().isEmpty()){ 
						WCParameter param = null;
						 
						String apikey = null;
						if(viewMode!=null && viewMode.equals("site")){
							//load data from caches
							String cacheKey = CacheKey.getWidgetTabKey(publicKey);
							try{
								clientWidget =  Cache.get(cacheKey, ClientWidgetSerializer.class); 
							}catch(Exception e){
								play.Logger.log4j.info(e.getMessage(),e);
							}
							
							if(clientWidget == null){
								WidgetDefault defaultWidget = WidgetDefault.findByPublicKey(publicKey);
								if(defaultWidget.getWiParameter().getButtonFont()==null){
									defaultWidget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
								}
								
								clientWidget = new ClientWidgetSerializer(defaultWidget.getApikey(), defaultWidget.getPublicKey(), defaultWidget.getWiParameter());
								clientWidget.getParam().setHtml(defaultWidget.getHtml());
								CacheKey.set(cacheKey, clientWidget,"1440mn");
								Cache.delete(sKey);
							}
							
							param = clientWidget.getParam(); 
							param.setHtml("");
							apikey = clientWidget.getApikey();
						}else{
							WixDefault wix = WixDefault.findWix(instanceId);
							if(wix==null){ 
								wix = WixDefault.getWixDefault(instanceId, uid,compId,origCompId);
							}
							param = new WCParameter(wix.getWiParameter());
							param.setHtml("");
							apikey = wix.getApikey();
						}
						
						EcomBlogType pluginType = EcomBlogType.Wix;
						Gson gson  = new Gson();
						String data = gson.toJson(param);  
						
						try{
							WixSerializer trackPremium = new WixSerializer(apikey, pluginType.name(), instanceId,WixClient.getString(json.get("vendorProductId")));
							ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
							trackActor.tell(trackPremium);
						}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}  
					  
						render(publicKey,pluginType,data,instanceId,uid,serverUrl);
						
					}else{
						origCompId = (origCompId!=null && !origCompId.trim().isEmpty())?origCompId:compId; 
						//there is no dropifi account associated with this wix user. create a temporarily account
						WixDefault wix = WixDefault.getWixDefault(instanceId, uid,compId,origCompId);
						EcomBlogType pluginType = EcomBlogType.Wix;
						WCParameter param = new WCParameter(wix.getWiParameter()); 
						param.setHtml("");
						Gson gson  = new Gson();
						String data = gson.toJson(param);
						render(pluginType,data,instanceId,uid,serverUrl);
					}
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		}
		render();
	}
	
	public static void page_app(@Required String instance,Integer height, Integer width, String compId,String origCompId,String viewMode,String target){
		render(); 
	}
	
	public static void page_mobile_app(@Required String instance, Integer width, String compId,String origCompId,String viewMode,String target){
		render();
	}
	
	@Deprecated
	public static void show_widget_content_popout(String publicKey,String buttonPosition,@Required String hostUrl,String pluginType,String requestUrl,String xdm_e,String bgcolor,String uid,String instanceId,String viewMode){	
	 
		String html=""; 
		String widget_content=null;	
		String cacheKey = "";  
		
		if(publicKey!=null && viewMode!=null && viewMode.equals("site")){
			cacheKey = CacheKey.getWidgetContentKey(publicKey);
			widget_content = Cache.get(cacheKey, String.class);  
			if(widget_content == null){ 
				html = WidgetDefault.findHtmlCodeByPKey(publicKey);
				if(html!=null && !html.trim().isEmpty()){
					widget_content = WidgetDefault.renderWixWidgetPopoutContent(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor);
	
					//update the widget cache key
					try{
						Cache.set(cacheKey, widget_content, "1440mn");
					}catch(Exception e){
						play.Logger.log4j.info(e.getMessage(),e);
					}
				}
			}
		} 
		
		if(widget_content==null){//render temporarily widget content
			WixDefault wix = WixDefault.getWixDefault(instanceId, uid,null,null);
			if(wix!=null){
				buttonPosition= wix.getWiParameter().getButtonPosition();
				widget_content = WidgetDefault.renderWixWidgetPopoutContent(wix.getHtml(),buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor);
				 
			}
		}
		
		if(requestUrl==null && xdm_e!=null){
			requestUrl = xdm_e;
		}
		
		String strCaptcha =null;
		if(CacheKey.getHasCaptcha(publicKey)) {
			CaptchaSerializer captcha = CaptchaSerializer.getCaptch();
			ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
			strCaptcha = captcha.getJson();
		}
		String attachmentId = DropifiTools.generateCode(12);
		render(widget_content,buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor,attachmentId,strCaptcha);
	}
	
	public static void widgetHtmlContent(String publicKey,String instanceId,String uid,String pluginType,String hostUrl,String requestUrl,String bgcolor){
		String widget_content=null;	
		String cacheKey ="";
		
		if(publicKey!=null){
			cacheKey = CacheKey.genCacheKey(publicKey, CacheKey.Widget, "HtmlContent");
			widget_content = Cache.get(cacheKey, String.class); 
			 
			if(widget_content == null) {
				widget_content = WidgetDefault.findHtmlCodeByPKey(publicKey);
				
				try{
					Cache.set(cacheKey, widget_content, "1440mn");
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e); 
				}
			}
		} 
			
		if(widget_content==null){//render temporarily widget content
			WixDefault wix = WixDefault.getWixDefault(instanceId, uid,null,null); // WixDefault.findByInstanceId(instanceId); 
			if(wix!=null) widget_content = wix.getHtml();
		}
		
		JsonObject json = new JsonObject();
		json.addProperty("widgetContent", widget_content);
		renderJSON(json.toString());
	}
	
	public static void show_existing(String instanceId,String uid,String compId, String origCompId,String baseUrl, String error){
		signup(instanceId, uid,compId, origCompId,baseUrl,true,error);
	}
	
	public static void show_new(String instanceId,String uid,String compId, String origCompId,String baseUrl,String error){
		signup(instanceId, uid,compId, origCompId,baseUrl,false,error);
	} 
	
	public static void createNewAccount(@Required String user_name,@Required String new_useremail,@Required String userpassword,@Required String company_name,String confirmPassword,@Required String eb_apikey,@Required String eb_secretKey, String baseUrl,String compId,String origCompId,String url,String location,String phoneNumber){
		try{
			flash.put("new_useremail", new_useremail);
			flash.put("user_name", user_name);
			if(validation.hasError("user_name") || validation.hasError("new_useremail") || validation.hasError("userpassword")){
				show_new(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Oops, email address and password are required");
			}else if(validation.hasError("company_name")){  
				show_new(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Oops, company name is required");
			}else if(Company.isEmailAvailable(new_useremail)){		 
	    		show_new(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Oops, the email  address "+new_useremail+" belongs to an existing account.");	 
	    	}else if(!validation.hasErrors()){ 
	    		String companyName = company_name;
	    		company_name = company_name.trim().replace(" ", "").toLowerCase();
	    	    //check if email is available
	    		company_name = Company.replaceSpecialChar(company_name);
	    	   	
				//String domain = //WixClient.generateDomain(baseUrl);
				
				if(Company.isDomainAvailable(company_name)){
					show_new(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Ooops, an account with company name "+companyName+" already exist.");
		    	} 
				
				EcomBlogPlugin plugin = new EcomBlogPlugin(new_useremail, baseUrl, eb_apikey, eb_secretKey, EcomBlogType.Wix);
				CompanyProfile profile = new CompanyProfile(companyName,baseUrl, null,null, null,phoneNumber); 
				
				//create a new account for the shop
				Company company = new Company(plugin,company_name, userpassword,profile,user_name);
				company = company.createCompany();
				
				if(company!=null){
					updateToDefaultSettings(company.getApikey(),company.getDomain(),eb_secretKey);
					
					try{
			    	    MailSerializer mail = new MailSerializer(user_name,new_useremail,"","Welcome to Dropifi",company.getSignUpMsg());								
						ActorRef mailActor = actorOf(MailingActor.class).start();     
						mailActor.tell(mail);
		    	    }catch(Exception e){
		    	    	play.Logger.log4j.info(e.getMessage(),e);
		    	    }
					
					try{
						WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),baseUrl,url);
						ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
						trackActor.tell(track);  
					}catch(Exception e){
						play.Logger.log4j.error(e.getMessage(), e);
					}
					
					try{
			    	    EventTrackSerializer event = new EventTrackSerializer(user_name,company.getEmail(),"SIGNUP","",company.getEmail(),UserType.Admin.name(),true);
			    	    event.setWidgetType(EcomBlogType.Wix.name());
			    	    event.setAccountType(company.getAccountType().name());
			    	    event.setCreated(company.getCreated().toString());
			    	    event.location = location;
			    	    event.setPhone(phoneNumber);
						ActorRef eventActor = actorOf(EventTrackActor.class).start();   
						eventActor.tell(event); 
		    	    }catch(Exception e){
		    	    	play.Logger.log4j.info(e.getMessage(),e);
		    	    }
					
					index(company.getEcomBlogPlugin().getEb_apikey(),company.getEcomBlogPlugin().getEb_secretKey(),compId, origCompId);
				}
	    	}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
		String msg ="Account could not be created. Try deleting the app and install again.";
		
		if(baseUrl==null || baseUrl.trim().isEmpty()){
			msg="You have to save your wix site before you can create an account";
		}
		show_new(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,msg);	
	}
	
	public static void createExistingAccount(@Required String user_email,@Required String password,@Required String eb_apikey,@Required String eb_secretKey, String baseUrl,String compId,String origCompId,String url,String location){
		flash.put("user_email", user_email);  
		 
    	if(validation.hasErrors()){   		
    		show_existing(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Oops, email address and password are required");
    	}if(!validation.email(user_email).ok){ 
    		show_existing(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"invalid email address format");
    	}else{
    		
	    	//find the user information
	    	 Map<String,Object> userdata = Company.findByEmailPassword(user_email, UserType.Admin); 
	    	    	 
	    	if(userdata == null){
	    		show_existing(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Oops, there is no account associated with this email");
	    	}else{	 
	    		try{  
		    		String ph = (String) userdata.get("password");	
		    		String pass = ph!=null?(String)ph:null;
		    		
		    		String hPassword = Codec.hexMD5(password);
		    		
		    		if(hPassword==null || pass==null || !pass.equals(hPassword) ){ 
		    			show_existing(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Oops, the password you entered is not correct");
			    	}else{
			    		Company company = (Company) userdata.get("company");
			    		
			    		EcomBlogType pluginType = company.getEcomBlogPlugin().getEb_pluginType();
			    		
			    		String username = (String) userdata.get("username");
			    		EcomBlogPlugin plugin = new EcomBlogPlugin(user_email, baseUrl, eb_apikey, eb_secretKey, EcomBlogType.Wix);				
			    		
		    			//reset the EcomBlogPlugin
						company.setEcomBlogPlugin(plugin);									 
						if(baseUrl!=null) company.getProfile().setUrl(baseUrl);
						company.save();						
						 					
						updateToDefaultSettings(company.getApikey(),company.getDomain(),eb_secretKey);
						try{
							WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),company.getProfile().getUrl(),baseUrl);
							ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
							trackActor.tell(track);  
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						}
						
						try{
				    	    EventTrackSerializer event = new EventTrackSerializer(username,company.getEmail(),"LOGIN","",company.getEmail(),UserType.Admin.name(),true);
				    	    event.setWidgetType(EcomBlogType.Wix.name());
				    	    event.setAccountType(company.getAccountType().name());
				    	    event.setCreated(company.getCreated().toString());
				    	    event.location = location;
							ActorRef eventActor = actorOf(EventTrackActor.class).start();   
							eventActor.tell(event); 
			    	    }catch(Exception e){
			    	    	play.Logger.log4j.info(e.getMessage(),e);
			    	    }
						
						PrestaShop.revertToFree(company.getApikey(),pluginType, plugin.getEb_pluginType());

						
						index(company.getEcomBlogPlugin().getEb_apikey(),company.getEcomBlogPlugin().getEb_secretKey(),compId, origCompId);	
			    	} 
		    	}catch(Exception e){
		    		play.Logger.log4j.info(e.getMessage(),e);
		    		show_existing(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,"Oops, there is no account associated with this email");
				}
	    	}
    	}
    	
    	String msg="An error occurred while installing dropifi contact widget for your site. Try installing it again";
    	show_existing(eb_secretKey, eb_apikey, compId, origCompId,baseUrl,msg);
	}
	
	public static void updateUrl(@Required String publicKey, String baseUrl,String url){
		if(!validation.hasError("publicKey")){
			try{ 
				String apikey = WidgetDefault.getApikey(publicKey);
				WidgetTrackerSerializer track = new WidgetTrackerSerializer(apikey,baseUrl,url);
				ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
				trackActor.tell(track);  
			}catch(Exception e){
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}
		JsonObject json = new JsonObject();
		json.addProperty("status", 200); 
		renderJSON(json.toString());
	}
	
	public static void saveDefaultSetting(@Required String uid,@Required String instanceId,String buttonPosition,String buttonColor,String buttonText,String viewMode){
		JsonObject json = new JsonObject();
		if(!validation.hasErrors()){
			try{
				if(buttonPosition!=null && !buttonPosition.trim().isEmpty()){ 
					switch(buttonPosition){
						case "TOP_LEFT":
							buttonPosition ="left";
							break;
						case "TOP_CENTER":
							buttonPosition ="top";
							break;
						case "TOP_RIGHT":
							buttonPosition ="right";
							break; 	
						case "CENTER_LEFT":
							buttonPosition ="left";
							break;
						case "CENTER_RIGHT":
							buttonPosition ="right";
							break;
						case "BOTTOM_LEFT":
							buttonPosition ="left";
							break;
						case "BOTTOM_CENTER":
							buttonPosition ="bottom";
							break;
						case "BOTTOM_RIGHT":
							buttonPosition ="right";
							break;
					}
				}
				
				WixDefault wix = WixDefault.findByInstanceId(instanceId);
				if(wix==null){
					wix = new WixDefault(instanceId, uid).createWixDefault();
				}else{
					wix = wix.updateWixDefault(buttonPosition,buttonColor,buttonText);
				} 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		} 
		
		json.addProperty("status", 200); 
		renderJSON(json.toString());
	}
	
	public static void publishSetting(@Required String uid,@Required String instanceId,String viewMode){
		JsonObject json = new JsonObject();
		int status =501;
		if(!validation.hasErrors()){
			try{
				Map<String,String> found = Company.findApikeyDomain(instanceId); 
				if(found!=null){
					String apikey = found.get("apikey");
					String domain = found.get("domain");
					Widget widget = Widget.findByApiKey(apikey);
					WixDefault wix = WixDefault.findWix(instanceId);
					
					//save the published settings only if the user is on the premium version
					//if(wix!=null && !wix.getAccountType().equals(AccountType.Free)){
						widget.updateWidget(wix.getWiParameter(), domain); 
						status = 200;
					//}
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		json.addProperty("status", status); 
		renderJSON(json.toString());
	}
	
	public static void resetAccount(@Required String email,@Required String uid, @Required String instanceId,String compId,String origCompId){
		if(!validation.hasErrors()){
			try {
				Company company = Company.findByEcomBlogKey(instanceId, EcomBlogType.Wix);
				
				if(company!=null){
					company.resetWixAccount(instanceId);
				}
				
				WixDefault wix = WixDefault.findByInstanceId(instanceId);
				if(wix!=null){
					wix.setApikey("");
					wix.setAccountType(AccountType.Free);
					wix.setEmail(null); 
					wix.updateWixDefault(new Widget("",0l).getUnSavedWidget(instanceId));	
				}
			}catch(Exception e) {
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		index(uid,instanceId,compId,origCompId);
	}
	
	@Util
	protected static void updateToDefaultSettings(String apikey,String domain,String instanceId){
		try{
			WixDefault wix = WixDefault.findByInstanceId(instanceId);
			if(wix!=null){
				
				//if(!wix.getAccountType().equals(AccountType.Free)){
					Widget widget = Widget.findByApiKey(apikey);
					WiParameter wiParam = widget.getWiParameter();
					wiParam.setButtonColor(wix.getWiParameter().getButtonColor());
					wiParam.setButtonText(wix.getWiParameter().getButtonText());
					wiParam.setButtonPosition(wix.getWiParameter().getButtonPosition());
					widget.updateWidget(wiParam, domain);
				//}
				wix.setApikey(apikey);
				wix.save(); 
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}

}
