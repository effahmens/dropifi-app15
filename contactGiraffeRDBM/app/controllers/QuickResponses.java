package controllers;

import java.util.*;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import models.*;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import resources.EcomBlogType;
import resources.UserType;
import view_serializers.MbSerializer;
import view_serializers.QrSerializer;

@With(Compress.class)
public class QuickResponses extends Controller{ 
	
	   @Before
     static void setArgs(String domain){ 
		   String apikey = Security.connectedApikey();	
	 		String foundApikey = Company.findApikey(apikey);
	 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
	 			
	 			if(UserType.isAgent(Security.connectedUserType()))
	 				Dashboard.index(domain);
	 			
	 			 renderArgs.put("page","admin");
	 		    renderArgs.put("sub_page","mail");
	 		    renderArgs.put("title","Admin - Mail Management (Quick Responses)");
	 		}else{
	 			//clear the session
	 	    	session.clear();
	 	    	//response.removeCookie("rememberme");
	 			
	 			try {
	 				Secure.login("");
	 			} catch (Throwable e) {
	 				Application.index();
	 			}
	 		}
	   
     }
	   
	//displays list of all QuickeReponses created
	public static void index(String domain){		
		ForceSSL.redirectToHttps();
		
		//TODO: find all created quick responses
		
		String apikey = Security.connectedApikey();	
		if(!validation.hasErrors() && apikey !=null){	
			String connected = Security.connectedDomain(); 	
			domain =domain!=null? domain.trim():"";
			if(!connected.equalsIgnoreCase(domain)){			
 				index(connected);
 			}
				String connectedUsername = Security.connected();	 
				 
				String userType = Security.connectedUserType().name();
				String ecomBlogPlugin = Security.connectedPlugin();
				//check if the widget is installed on the site of the user
 				boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
 				render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget); 
 
			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
		Apps.page_not_found("");
	}
	
	public static void populate(){
		//TODO implemented security checks
		
		//Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status = 404;
		String apikey = Security.connectedApikey();
		
		//String domain =Security.connectedDomain();
		List quickResponses = null; 
		int size = 0;
		if(apikey !=null){
			MessageManager msg = MessageManager.findByApikey(apikey);
			quickResponses = (List) (msg==null?null: msg.getQuickResponses());		
		
			//check the status of the quickResponse: if size 0 then return 200 - quickResponse(s) is/are available
			size = (quickResponses == null) ? 0 : quickResponses.size();
			status = (size>0) ? 200:501;	
				
			json.add("quickResponses", QuickResponse.toJsonArray(quickResponses)); 
		}
		
		json.addProperty("status", status); 
		json.addProperty("size", size);
		
		renderJSON(json.toString());
	}
	
	//add a new quick response
	public static void create(@Required QuickResponse qr) {
		
		//TODO: provide method for save the quickResponse
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		if(!validation.hasErrors()){
			
			if(validation.required(qr.getTitle()).ok && validation.required(qr.getSubject()).ok 
					&&   validation.required(qr.getMessage()).ok){
				
				String apikey = Security.connectedApikey();
				if(apikey != null){
					//create a new quickResponse
					MessageManager msg = MessageManager.findByApikey(apikey);
					if(msg != null){
						status =msg.addQuickResponse(qr);
						json.addProperty("error", (status==200? "none" : (status==302 ? "duplication of title": "update was not successful")) );
					}
				}else{
					status = 405;
				}
				
			}else{
				status = 501;			
			}
			
		}else{
			json.addProperty("error", "invalid title");
		}
		
		json.addProperty("status", status);
		json.add("content", gson.toJsonTree(qr));
		renderJSON(json.toString());
	}
	
	public static void update(@Required String qrtitle, @Required QuickResponse qr){ 
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		if(!validation.hasErrors()){ 
				 					
		 	//check if the email is a valid one
			if(validation.required(qr.getTitle()).ok && validation.required(qr.getSubject()).ok 
					&&   validation.required(qr.getMessage()).ok){	 
				 
				//search for the connected company	
				String apikey = Security.connectedApikey();
				
				if(apikey != null){				
					
					MessageManager msg = MessageManager.findByApikey(apikey);
					
					if(msg != null){ 							
						//update the changes to the quick response
						status = msg.updateQuickResponse(qrtitle, qr); 						
						json.addProperty("error", (status==200? "none" : (status==302 ? "duplication of title": "update was not successful")) );
					} 
				}else{
					status=405;
				}
				
				//redirect to login page
			}else{
				json.addProperty("error", "invalid email");
			}
		
		}else{
			json.addProperty("error", "none");
		}
		
		json.add("content", gson.toJsonTree(qr)); 
		json.addProperty("status", status); 
		
		renderJSON(json.toString());	
	}
	
	public static void delete(@Required String qrtitle){
		JsonObject json = new JsonObject();
		int status = 404;
		play.Logger.log4j.info("Checking for errors");
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();
			
			if(apikey != null) {				
				
				MessageManager msg = MessageManager.findByApikey(apikey);
				
				if(msg != null){ 	
					 					
					//update the changes to the mailbox
					status = msg.removeQuickResponse(qrtitle);
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}			
		}
		
		json.addProperty("status", status); 
		json.addProperty("title", qrtitle); 
		renderJSON(json.toString());
	}
	
	//displays an editable details of a quick response
	public static void show(@Required String qrtitle, @Required String domain){
		ForceSSL.redirectToHttps();
		String connected = Security.connectedDomain(); 			
		String apikey = Security.connectedApikey();
		
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){				

			//TODO: provide method for save the quickResponse
			String btnsave ="";
			QuickResponse quickResponse  = null; 
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			
			if(validation.required(qrtitle).ok){ 
				//check if an email is specified				
				if(qrtitle.equals("new")){
					 btnsave ="Save"; 
					 //render(btnsave,qrtitle,domain);
				}else{
					 //search for the quickResponse					
					MessageManager msg = MessageManager.findByApikey(apikey);		
					quickResponse = (msg == null ? null : msg.findQRByTitle(qrtitle));				
					
					btnsave ="Update"; 				
					//render(btnsave,qrtitle,quickResponse,domain);
				} 			
			}else{
				 qrtitle="new";
				 btnsave ="Save"; 
				 //render(btnsave,qrtitle,domain);
			}	
			render(btnsave,qrtitle,quickResponse,domain,connectedUsername,ecomBlogPlugin,userType); 
		}
		
		Apps.popout_not_found("");
		//provide a redirect page
	}
	
	//enable or disable mailbox
	public static void activateQuickResponse(@Required String qrtitle, @Required Boolean activated){
		
		//Gson gson = new Gson();				 
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				MessageManager msg = MessageManager.findByApikey(apikey);
				if(msg != null){ 	
					 			
					//update the changes to the mailbox
					status = msg.updateQRActivated(qrtitle, activated); 					
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("activated", activated);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
}
