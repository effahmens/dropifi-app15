package controllers;

import static akka.actor.Actors.actorOf;
import helper.redis.Redis;

import java.io.File;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import job.process.WidgetActor;
import job.process.WidgetTrackerActor;
import akka.actor.ActorRef;
import flexjson.JSONSerializer;
import models.Company;
import models.EcomBlogPlugin;
import models.FileDocument;
import models.GeoLocation; 
import models.PreviewWidget;
import models.WiParameter;
import models.Widget;
import models.WidgetDefault;
import models.WixDefault;
import play.cache.Cache;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.mvc.Controller;
import play.mvc.With;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.WidgetRespSerializer;
import resources.WidgetTrackerSerializer;
import view_serializers.AttSerializer;
import view_serializers.CaptchaSerializer;
import view_serializers.ClientSerializer;
import view_serializers.ClientWidgetSerializer;
import view_serializers.WCParameter;
import view_serializers.WidgetCallbackSerializer;

@With(Compress.class)
public class ClientMessenger extends Controller{
	
	public static void show_widget(@Required String publicKey,String pluginType,String requestUrl,String hostUrl,Integer width,Integer height,String xdm_e) {
 
		String widget_content = null;
		try { 
			if(!validation.hasErrors()) {
				//check if the pluginType is shopify or tictail
				if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){
					String shopName = publicKey; 
					EcomBlogType ptype = EcomBlogType.valueOf(pluginType); 
					//load data from cache 
					String sKey = CacheKey.getPluginKey(ptype,shopName);					
					publicKey = Cache.get(sKey,String.class);  
					
					if(publicKey==null){
						publicKey = Company.findPublicKey(shopName, ptype);
						Cache.set(sKey, publicKey);
					}
				}
				
				//load widget data from cache
				ClientWidgetSerializer clientWidget = null; 
				WCParameter wiparam=null;
				String socialUID =null;
				String cacheKey = CacheKey.getWidgetTabKey(publicKey);
				 
				try{
					clientWidget = Cache.get(cacheKey, ClientWidgetSerializer.class); 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
				
				try {
					//Reset the content of the widget 
					//String isReset = Cache.get(cacheKey+"123",String.class);
					
					if(clientWidget == null || clientWidget.getPublickey()!=null 
							|| !clientWidget.getParam().getImageText().contains("s3-us-west-2.amazonaws.com")) {
						
						WidgetDefault defaultWidget = WidgetDefault.findByPublicKey(publicKey);	 
						if(defaultWidget!=null) {
							
							if(defaultWidget.getWiParameter().getButtonFont()==null){
								defaultWidget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
							}
							
							//if(isReset==null) {
							Widget widget = Widget.findByPublickey(publicKey);
							if(widget!=null) {  
								
								if( widget.getWiParameter()!= null && widget.getWiParameter().getImageText()!=null && !widget.getWiParameter().getImageText().contains("s3-us-west-2.amazonaws.com")) { 
									widget.updateWidget(widget.getWiParameter() , Company.findDomain(widget.getApikey())); 
								}
								 
								defaultWidget = defaultWidget.updateWidgetDefault(widget); 
								widget_content = defaultWidget.getHtml(); 
							}
							//}  
							
							clientWidget = new ClientWidgetSerializer(defaultWidget.getApikey(), defaultWidget.getPublicKey(), defaultWidget.getWiParameter() , defaultWidget.getSocialUID());
							CacheKey.set(cacheKey, clientWidget,"1440mn");
						} 
					} 
					
				}catch(Exception e) {
					play.Logger.log4j.info(e.getMessage(),e);
				}
				
				//Track that the widget is installed
				if(clientWidget != null){   
					wiparam = clientWidget.getParam();
					socialUID = clientWidget.getParam().getSocialUID();
					
					try{
						WidgetTrackerSerializer track = new WidgetTrackerSerializer(clientWidget.getApikey(),hostUrl,requestUrl,publicKey,clientWidget.getParam());
						ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
						trackActor.tell(track);
					}catch(Exception e){
						play.Logger.log4j.info(e.getMessage(),e);
					}  
				}
				
				String attachmentId = DropifiTools.generateCode(12);
				String serverURL=  DropifiTools.ServerURL;
				String strCaptcha = null;
				
				CaptchaSerializer captcha =null;
				if(CacheKey.getHasCaptcha(publicKey)){
					captcha = CaptchaSerializer.getCaptch();
					ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
					strCaptcha = captcha.getJson();
				}
				
				if((requestUrl==null || requestUrl.trim().isEmpty()) && xdm_e!=null){
					requestUrl = xdm_e;
				}
				
				if((hostUrl==null || hostUrl.trim().isEmpty()) && xdm_e!=null){
					hostUrl = xdm_e;
				}
				 
				render(wiparam,widget_content,publicKey,captcha,requestUrl,hostUrl,serverURL,attachmentId,socialUID,width,height,strCaptcha);
				
			}
		}catch(Exception e) {
			play.Logger.log4j.info(e.getMessage(),e);
		}
		 
		render(publicKey,width,height,widget_content);
	}
	
	public static void show_widget_wix(String buttonText,String publicKey,String instanceId,String uid,String pluginType,String requestUrl,String hostUrl,String viewMode,Integer height,Integer width,String buttonPosition,String bgcolor) {
		 
		String widget_content=null;	
		String cacheKey = "";  
		try {
			//play.Logger.log4j.info(viewMode+" <> "+publicKey);
			if(publicKey!=null && viewMode!=null && viewMode.contains("site")){
				
				cacheKey = CacheKey.getWidgetContentKey(publicKey);
				widget_content = Cache.get(cacheKey, String.class);  
				
				if(widget_content == null|| widget_content.trim().isEmpty()){
					
					WidgetDefault defaultWidget = WidgetDefault.findByPublicKey(publicKey); 
					 
						Widget widget = Widget.findByPublickey(publicKey);
						if(widget!=null) {  
							
							if( widget.getWiParameter()!= null && widget.getWiParameter().getImageText()!=null && widget.getWiParameter().getImageText().contains("s3.amazonaws")) {
								widget.updateWidget(widget.getWiParameter() , Company.findDomain(widget.getApikey())); 
							} 
							
							defaultWidget = defaultWidget.updateWidgetDefault(widget); 
						} 
						widget_content = defaultWidget.getHtml(); 
						CacheKey.set(cacheKey, widget_content, "1440mn"); 
				}
				//play.Logger.log4j.info("Testing content");
			} 
			
			if(widget_content==null || widget_content.trim().isEmpty()){
				
				//render temporarily widget content
				WixDefault wix = WixDefault.getWixDefault(instanceId, uid,null,null);
				if(wix!=null){  
					widget_content = (wix.getHtml()!=null?wix.getHtml().trim():"");  
				} 
				//play.Logger.log4j.info("Wix content");
			}    
			
			String strCaptcha =null;
			CaptchaSerializer captcha =null;
			if(CacheKey.getHasCaptcha(publicKey)) {
				 captcha = CaptchaSerializer.getCaptch();
				ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
				strCaptcha = captcha.getJson();
			}
			
			String serverURL = DropifiTools.ServerURL; 
			String attachmentId = DropifiTools.generateCode(13);
			 
			render(widget_content,buttonPosition,publicKey,pluginType,captcha,requestUrl,hostUrl,serverURL,attachmentId,bgcolor,width,height,strCaptcha,buttonText);
		
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		} 
		render(widget_content,publicKey,pluginType);
	}
	
	public static void show_widget_preview(@Required String publicKey,String pluginType,String requestUrl,String hostUrl,Integer width,Integer height) {
		String html = ""; 
		String widget_content=null;
		if(!validation.hasErrors()) {
			//check if the pluginType is shopify or tictail
			if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){
				String shopName = publicKey;
				EcomBlogType ptype = EcomBlogType.valueOf(pluginType);  
				publicKey = Company.findPublicKey(shopName, ptype); 
			}
			
			//load widget data from cache   
			WiParameter wiparam=null;
			String socialUID =null;
			PreviewWidget preWidget = PreviewWidget.findByApiKey(Security.connectedApikey());
			if(preWidget!=null){
				
				if(Security.connectedPlugin().equals(EcomBlogType.Wix.name())){
					WixDefault wix = WixDefault.findByApikey(preWidget.getApikey());
					if(wix!=null && wix.getWiParameter()!=null){
						preWidget.setWiParameter(wix.getWiParameter());
					} 
				}
				
				WidgetDefault defaultWidget = preWidget.getWidgetDefault(publicKey); 
				if(defaultWidget.getWiParameter().getButtonFont()==null){
					defaultWidget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
				}
				
				//clientWidget = new ClientWidgetSerializer(defaultWidget.getApikey(), defaultWidget.getPublicKey(), defaultWidget.getWiParameter(),defaultWidget.getSocialUID());	
				wiparam =defaultWidget.getWiParameter();
				socialUID =defaultWidget.getSocialUID();
				html = preWidget.getWidgetDefault(publicKey).getHtml();   
				widget_content = (html!=null) ? html:"";
			}  
			
			String attachmentId = DropifiTools.generateCode(12);
			String serverURL=  DropifiTools.ServerURL;
			String strCaptcha = null;
			CaptchaSerializer captcha =null;
			if(CacheKey.getHasCaptcha(publicKey)) {
				captcha = CaptchaSerializer.getCaptch();
				ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
				strCaptcha = captcha.getJson(); 
			}
			 
			render(wiparam,widget_content,publicKey,captcha,strCaptcha,requestUrl,serverURL,attachmentId,socialUID,requestUrl,width,height);
		}
		
		render(publicKey,width,height);
	}
	 
	/**
	 * Used for receiving messages sent from the widget: IFRAME implementation  
	 */
	public static void widget_iframe_sending_message(@Required String publicKey, ClientSerializer cs, String pluginType, 
		GeoLocation location, boolean isPreview, File uploadfile,String attachmentId, String fileType){
		JsonObject json = new JsonObject(); 
		int status = 200;  
		
		WidgetCallbackSerializer callback = new WidgetCallbackSerializer();  
		CaptchaSerializer captcha = CaptchaSerializer.getCaptch(); 
		if(!Validation.hasErrors()){
			
			//Reset the captcha value to a different value 
			boolean hasCaptcha = (isPreview==true)?PreviewWidget.findHasCaptch(publicKey):CacheKey.getHasCaptcha(publicKey); 
			if(hasCaptcha){
				try {   
					if(cs !=null && cs.captchaCode !=null && cs.captchaResult != null) {
						
						if( ClientWidgets.getCaptcha(cs.captchaCode) != cs.captchaResult) { 
							callback.setError("The answer you entered is not correct.");
							callback.setStatus(491);  
							json.add("callback", new Gson().toJsonTree(callback));
							json.add("Captcha", new Gson().toJsonTree(captcha)); 
							ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
							renderHtml(json.toString());
						}
					}else {
						callback.setError("You tried to send a message from an unauthorized source.");
						callback.setStatus(491);  
						json.add("callback", new Gson().toJsonTree(callback));
						json.add("Captcha", new Gson().toJsonTree(captcha));
						ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
						renderHtml(json.toString());
					} 
				}catch(Exception e) { 
					callback.setError("The answer you entered is not correct.");
					callback.setStatus(491);  
					json.add("callback", new Gson().toJsonTree(callback));
					json.add("Captcha", new Gson().toJsonTree(captcha));
					ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
					renderHtml(json.toString());
				}
			}
			
			if(uploadfile!=null){
				//verify that the file extension is allowed.
				if(!FileDocument.isBlockedAttachFile(uploadfile.getName())){
		    		//verify that the size of the file is allowed
					if(FileDocument.getFileSize(uploadfile).get("megabytes")<=25){
						String fileName = uploadfile.getName(); 
			    		fileType = params.get("fileType");  
			    		if(cs.getAttachmentId()!=null)
			    			attachmentId = cs.getAttachmentId(); 
			    		else if(attachmentId!=null)
			    			cs.setAttachmentId(attachmentId); 
			    		
			    		Map<String,String> company = Company.findApikeyDomain(publicKey, true);
			    		if(company!=null){
			    			AttSerializer att = new AttSerializer(null, company.get("domain"), cs.getEmail(), null, fileName, fileType, uploadfile, null, null, attachmentId);
			    			att.apikey = company.get("apikey");
			    			att.uploadFileToRackspace();
			    			callback.setStatus(200); 
			    		}  
					}else{
						callback.setError("The size of the file is larger than 25MB");
						callback.setStatus(301);  
					}
				}else{
					callback.setError("You can not upload this type of file");
					callback.setStatus(305);
				}
				
				if(callback.getStatus()!=200) {
					json.add("callback", new Gson().toJsonTree(callback));
					json.add("Captcha", new Gson().toJsonTree(captcha));
					ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
					renderHtml(json.toString());
				}
			}
			
			try{			
				
				if(publicKey !=null && cs != null && cs.getEmail()!=null && !cs.getEmail().trim().isEmpty()){ 			
					
					if(validation.email(cs.getEmail()).ok){
						//find the connected widget using the publicKey
						Widget widget =  Widget.findByPublickey(publicKey); 
						
						if(widget!=null){
							
							//Serialize the message and submit it to AKKA actor to perform sentiment analysis
							WidgetRespSerializer resp = new WidgetRespSerializer(widget.getCompanyId(), widget.getApikey(),
									widget.getId(),	cs, widget.getDefaultEmail(),widget.getResponseSubject(),widget.getResponseMessage(),
									widget.getResponseActivated()); 									
							resp.setLocation(location);
							ActorRef widgetActor = actorOf(WidgetActor.class).start();  
							widgetActor.tell(resp);			
							status = 200;
							 
							callback.setMessage(widget.getDefaultMessage());  
						}else{
							status = 405;
							callback.setError("unknown widget settings, check your public in the widget on your site");  
						}  
					}else{
						status = 406;
						callback.setError("invalid email address format");
					} 
				}else{
					callback.setError("required fields not specified");
				}		 		
			}catch(Exception e){
				callback.setError("your message was not delivered, try sending it again");
			}
			
		}else {
			callback.setError("Required fields not specified.");  
		}
		
		//play.Logger.log4j.info(location +" of contact");
		ClientWidgets.setCaptcha(captcha.accessCode, captcha.code);
		callback.setStatus(status);  
		json.add("callback", new Gson().toJsonTree(callback));
		json.add("Captcha", new Gson().toJsonTree(captcha));
		 
		renderHtml(json.toString());
	}
}
