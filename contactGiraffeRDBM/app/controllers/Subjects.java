package controllers;

import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import play.data.validation.*;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.CacheKey;
import resources.EcomBlogType;
import resources.UserType;
import models.Company;
import models.MessageManager;
import models.MsgSubject; 
import models.PreviewWidget;
import models.QuickResponse;
import models.Widget;
import models.WidgetTracker;
@With(Compress.class)
public class Subjects extends Controller{
	@Before
    static void setArgs(String domain) {
		String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
 			if(UserType.isAgent(Security.connectedUserType())){
 				redirect("/"+domain+"/dashboard/index"); 
 				//Dashboard.index(domain);
 			}
 			
 		   renderArgs.put("page","admin");
 		   renderArgs.put("sub_page","mail");
 		   renderArgs.put("title","Admin - Subjects");
 		}else{
 			//clear the session
 	    	session.clear();
 	    	//response.removeCookie("rememberme");
 			
 			try {
 				Secure.login("");
 			} catch (Throwable e) {
 				Application.index();
 			}
 		}
   
    }
	//retrieves and display all the message subject 
	public static void index(String domain){	
		ForceSSL.redirectToHttps();
		
		String apikey = Security.connectedApikey();
		if(!validation.hasErrors() && apikey !=null){
			String connected = Security.connectedDomain(); 	
			domain =domain!=null? domain.trim():"";
 			//if(connected.equalsIgnoreCase(domain)){
			if(!connected.equalsIgnoreCase(domain)){			
 				index(connected);
 			}
			
			String connectedUsername = Security.connected(); 
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			
			//check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
			
			render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget); 
 			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
		
		Apps.page_not_found("");
	}
	
	public static void populate(){
		//TODO  
		
		//Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status = 404;
		String apikey = Security.connectedApikey();
		
		//String domain =Security.connectedDomain();
		
		int size =0;
		if(apikey !=null){
			List subjects = null; 
			MessageManager msg = MessageManager.findByApikey(apikey);
			subjects = (List) (msg==null?null: msg.getSubjects());		
		
			//check the status of the quickResponse: if size 0 then return 200 - quickResponse(s) is/are available
			size = subjects == null ? 0:subjects.size();
			status = (size>0) ? 200:501;	
				
			json.add("subjects", MsgSubject.toJsonArray(subjects)); 
		}
		
		json.addProperty("status", status); 
		json.addProperty("size", size);
		
		renderJSON(json.toString());
	}
	
	//add a new message subject to the existing subjects
	public static void create(MsgSubject ms){ 

		//TODO: provide method for save the quickResponse
		Gson gson = new Gson(); 				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		if(!validation.hasErrors()){
			
			if(validation.required(ms.getSubject()).ok && validation.required(ms.getActivated()).ok ){
				
				String apikey = Security.connectedApikey();
				if(apikey != null){
					//create a new quickResponse
					MessageManager msg = MessageManager.findByApikey(apikey);
					if(msg != null){
						
						status = msg.addSubject(ms); 
						
						//update the widget 						 
						status = updateWidget(apikey); 
						deleteCache(apikey);
						json.addProperty("error", (status==200? "none" : "update was not successful") );
					}
				}else{
					status = 405;
				}
				
			}else{
				status = 501;			
			}
			
		}else{
			json.addProperty("error", "invalid title");
		}
		
		json.addProperty("status", status);
		json.add("content", gson.toJsonTree(ms));
		renderJSON(json.toString());
	}

	//save the changes to an existing message subject	
	public static void update(@Required String subject, MsgSubject ms){
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject(); 
		int status= 404;
		
		if(!validation.hasErrors()){ 
				 					
		 	//check if the email is a valid one
			if(validation.required(ms.getSubject()).ok && validation.required(ms.getActivated()).ok){	 
				 
				//search for the connected company	
				String apikey = Security.connectedApikey();
				
				if(apikey != null){				
					
					MessageManager msg = MessageManager.findByApikey(apikey);
					
					if(msg != null){ 							
						//update the changes to the quick response
						status = msg.updateSubject(subject, ms);	
						
						if(status == 200){
							//update the widget
							status = updateWidget(apikey); 
							deleteCache(apikey);
						} 				
						json.addProperty("error", (status==200? "none" : (status==302 ? "duplication of subject name": "update was not successful")) );
					}else{
						
					} 
				}else{
					status=405;
				}
				
				//redirect to login page
			}else{
				json.addProperty("error", "required fields not specified");
			}
		
		}else{
			json.addProperty("error", "none");
		}
		
		json.add("content", gson.toJsonTree(ms)); 
		json.addProperty("status", status); 
		
		renderJSON(json.toString()); 	
	}
	
	public static void delete(@Required String subject){
		JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();
			
			if(apikey != null){				
				
				MessageManager msg = MessageManager.findByApikey(apikey);
				
				if(msg != null){ 	
					 					
					//update the changes to the mailbox
					status = msg.removeSubject(subject);
					
					//update the widget
					status = updateWidget(apikey); 					
					deleteCache(apikey);
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}
			
		}
		
		json.addProperty("status", status); 
		json.addProperty("subject", subject); 
		renderJSON(json.toString());
	}
	
	@Util
	private static  int updateWidget(String apikey){ 
		Widget widget = Widget.findByApiKey(apikey);
		if(widget !=null){
			return widget.updateWidget();
		} 
		return 360;
	}
	
	//displays an editable details of a message subject
	public static void show(@Required String subject, @Required String domain){
		ForceSSL.redirectToHttps();
		String connected = Security.connectedDomain(); 			
		String apikey = Security.connectedApikey();

		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){				

			//TODO:  
			String btnsave = "";
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			MsgSubject msgSubject =null;
			if(validation.required(subject).ok){
				subject = subject.replace("~~","&");
				//check if a subject is specified			
				if(subject.equals("new")){
					 btnsave ="Save";  
				}else{				
					//search for the message subject
					MessageManager msg = MessageManager.findByApikey(apikey);		
					msgSubject = (msg == null ? null : msg.findSubjectByName(subject)); 				
					
					btnsave ="Update"; 
				} 			
			}else{
				subject ="new";
				btnsave ="Save"; 
			}	
			render(btnsave,subject,msgSubject,domain,connectedUsername,ecomBlogPlugin,userType); 
		} 
		
		Apps.popout_not_found("");
	}
	
	//enable or disable mailbox
	public static void activateSubject(@Required String subject, @Required Boolean activated){			 
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				MessageManager msg = MessageManager.findByApikey(apikey);
				if(msg != null){ 	
					 			
					//update the changes to the mailbox
					status = msg.updateSubjectActivated(subject, activated);
					
					//update the widget
					status =updateWidget(apikey); 
					deleteCache(apikey);
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("activated", activated);
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
	public static void orderSubjects(@Required List<String> subjects){
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				MessageManager msg = MessageManager.findByApikey(apikey);
				if(msg != null){  
					//update the changes to the mailbox 
					status = msg.addSubjects(MsgSubject.getMsgSubject(subjects));
					
					//update the widget
					status =updateWidget(apikey); 
					deleteCache(apikey);
					json.addProperty("error", (status==200? "none" : "update was not successful") );
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("status", status); 	
		renderJSON(json.toString()); 
	}
	
	@Util
	protected static void deleteCache(String apikey){
		CacheKey.deleteCache(CacheKey.genCacheKey(apikey, CacheKey.Subjects,"listSer"));
		CacheKey.deleteCache(CacheKey.genCacheKey(apikey, CacheKey.Subjects, "listOfMsgSubjects"));
	}
}
