package controllers;

import static akka.actor.Actors.actorOf;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import job.process.EventTrackActor;
import job.process.MailingActor;
import job.process.WidgetTrackerActor;
import models.Company;
import models.CompanyProfile;
import models.EcomBlogPlugin;
import models.WidgetDefault;

import org.json.JSONObject;

import akka.actor.ActorRef;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import flexjson.JSONSerializer;
import play.Logger;
import play.data.validation.Required;
import play.libs.Codec;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Controller;
import play.mvc.With;
import play.mvc.Http.Header;
import play.mvc.Util;
import resources.DCON;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import resources.WidgetTrackerSerializer;
import view_serializers.EcomParameter;
import view_serializers.MailSerializer;
@With(Compress.class) 
public class Wordpress extends Controller{
	
	public static void index(){
		render(); 
	}
	
	public static void signup(@Required String displayName,@Required String user_email,	@Required String user_password,
			@Required String user_re_password,@Required String user_domain,@Required String hostUrl, @Required String requestUrl,
			@Required String accessToken,String site_url, String type, String s,String location, String phoneNumber){
		
		Integrations.signup(displayName, user_email, user_password, user_re_password, user_domain, hostUrl, requestUrl, accessToken, site_url, type, s, location, EcomBlogType.Wordpress,phoneNumber);
	}
	
	public static void login(@Required String temToken,@Required String userEmail,String IPAddress,String location){ 
		Integrations.login(temToken, userEmail, IPAddress, location, EcomBlogType.Wordpress); 
	}
	
	public static void loginToken(@Required String login_email,@Required String accessKey,@Required String requestUrl,@Required String accessToken,String site_url,String type, String s){
		Integrations.loginToken(login_email, accessKey, requestUrl, accessToken, site_url, type, s, EcomBlogType.Wordpress);
	} 
}
