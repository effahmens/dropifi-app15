package controllers; 
import static akka.actor.Actors.actorOf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import job.cache.WRCacheActor;
import job.process.WidgetActor;

import org.h2.store.Data;

import akka.actor.ActorRef;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import models.Channel;
import models.Company;
import models.MessageManager;
import models.MsgSubject;
import models.RuleAction;
import models.RuleCondition;
import models.WMsgRecipient;
import models.WidgetRule;
import models.WidgetTracker;
import models.dsubscription.SubConsumeService;
import models.dsubscription.SubServiceName;
import models.dsubscription.SubServiceType;
import play.cache.Cache;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import resources.CacheKey;
import resources.DCON;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.RuleGroup;
import resources.UserType;
import resources.helper.ServiceSerializer;
import resources.helper.ServiceVolumeSerializer;
import view_serializers.MbSerializer;
import view_serializers.RuleSerializer;
@With(Compress.class) 
public class Widget_Rules extends Controller{ 
	@Before
    static void setArgs(String domain){
		String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) { 
 			
 			if(UserType.isAgent(Security.connectedUserType()))
 				Dashboard.index(domain);
 			
 	        renderArgs.put("page","admin");
 	        renderArgs.put("sub_page","mail");
 	        renderArgs.put("title","Admin - Mail Management (Message Rules)");
 		}else{
 			//clear the session
 	    	session.clear();
 	    	//response.removeCookie("rememberme");
 			
 			try {
 				Secure.login("");
 			} catch (Throwable e) {
 				Application.index();
 			}
 		}

    }	
	
	public static void index(String  domain){
		ForceSSL.redirectToHttps();
		
		String apikey = Security.connectedApikey();		
		if(!validation.hasErrors() && apikey !=null){
			
			String connected = Security.connectedDomain(); 	
			domain =domain!=null? domain.trim():"";
 			//if(connected.equalsIgnoreCase(domain)){
			if(!connected.equalsIgnoreCase(domain)){			
 				index(connected);
 			}
				String connectedUsername = Security.connected();
				String userType = Security.connectedUserType().name();
				String ecomBlogPlugin = Security.connectedPlugin();
				 
				//check if the widget is installed on the site of the user
 				boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));

				render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget); 
			   
 			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		} 
		
		Apps.page_not_found("");
	}
	
	public static void populate(){ 
		JsonObject json = new JsonObject();
		Gson gson = new  Gson(); 
		int status = 404;
		String apikey = Security.connectedApikey();
		int size =0;
		String jsonString = "";
		
		if(apikey !=null){
			//get the message recipients from cache
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Widget_Rules, DropifiTools.POPULATE);
			jsonString = Cache.get(cacheKey, String.class); 

			if(jsonString == null || jsonString.isEmpty()){
				 				
				JsonArray arrayJson = new JsonArray(); 
				arrayJson = RuleSerializer.toWidgetJsonArray(Security.connectedCompanyId());
				size = arrayJson.size(); 
				status =200;
				
				//update the total volume consumed
				try{
					ServiceVolumeSerializer volSerializer = new ServiceVolumeSerializer(apikey, SubServiceName.BusinessRules, SubServiceType.FixedVolume, size);
					SubConsumeService.updateConsumedVolume(volSerializer); 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(), e); 
				}
				
				ServiceSerializer service = SubConsumeService.findServiceSerializer(apikey, SubServiceName.BusinessRules);
				json.add("service", gson.toJsonTree(service)); 
				json.add("msgRules",arrayJson);
				json.addProperty("status", status);  
				json.addProperty("size",size);
				
				//cache the service
				CacheKey.cacheSubService(apikey,CacheKey.Widget_Rules, service, DropifiTools.SUBSERVICE);
				
				try{
					//set the recipients to cache
					jsonString = json.toString(); 
					Cache.set(cacheKey, jsonString);
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
				//play.Logger.log4j.info("Populate Widget Rule : Not Queried from cache");
			}/*else{
				play.Logger.log4j.info("Populate Widget Rule : Queried from cache");
			}*/
			
		}else{ 
			json.addProperty("status", status);  
			json.addProperty("size",size);
			jsonString = json.toString();
		}
		
		renderJSON(jsonString);  
	}
	
	public static void create(@Required String title,@Required boolean activated,@Required String ruleGroup, @Valid List<RuleCondition>conditions, @Valid List<RuleAction>actions){
		//TODO: provide method for save the widget rules 
		JsonObject json = new JsonObject();
		int status= 404;
		play.Logger.log4j.info("Condition: "+title);
		 
		if(!validation.hasErrors()){ 				
			String apikey = Security.connectedApikey();
			if(apikey != null){
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget_Rules, SubServiceName.BusinessRules);
				String msg_info="";
				String domain = Security.connectedDomain();
				if(service!=null && !service.hasExceeded){
					//create a new quickResponse
					MessageManager msg = MessageManager.findByApikey(apikey); 
					if(msg != null){  
						long companyId = Security.connectedCompanyId();
						WidgetRule rule = new WidgetRule(companyId,title,activated,RuleGroup.valueOf(ruleGroup)); 
						rule.addConditions(conditions); 
						rule.addActions(actions);				
						status = msg.addWidgetRule(rule);
						msg_info = (status== 200? "none" : (status==305? "duplication of rule title. Change the title of the rule":"an error occured while saving the rule. Try saving it again"));
						
						//delete the list of recipients from the cache
						CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget_Rules);
					}
				}else{
					msg_info = "<div class='main_upgrade'><a class='link_upgrade' href=/"+domain+"/admin/pricing/index' target='_blank'>Upgrade to add more Widget Rules</a></div>";
				}
				json.addProperty("error",msg_info); 
			}else{
				status = 405; 
			}
			
		}else{
			json.addProperty("error", "invalid title");
		}
		
		if(status == 200){
			json.addProperty("title",title); 
		}else{
			json.addProperty("title","new"); 
		}
		
		json.addProperty("status", status);  		 
		renderJSON(json.toString());
	}
	
	public static void update(@Required String originalTitle, @Required String title,@Required boolean activated,@Required String ruleGroup, @Valid List<RuleCondition>conditions, @Valid List<RuleAction>actions){
		 				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		if(!validation.hasErrors()){   
			//search for the connected company	
			String apikey = Security.connectedApikey(); 
			
			if(apikey != null){
				
				//update the changes to the widget rules  
				long companyId =Security.connectedCompanyId();
				status = WidgetRule.updateRule(companyId, originalTitle, title, activated, RuleGroup.valueOf(ruleGroup), conditions, actions); 
				json.addProperty("error", (status==200? "none" : (status==305 ? "duplication of title. A rule with similar title is already created": "update was not successful")) ); 				
				
				//delete the list of recipients from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget_Rules);
			}else{ 
				status=405; 
			}
		}else{
			json.addProperty("error", "none");
		}
		
		if(status == 200){
			json.addProperty("title",title); 
		}else{
			json.addProperty("title",originalTitle); 
		}
		
		json.addProperty("status", status); 		
		renderJSON(json.toString());	
	}
	
	public static void delete(@Required String name){ 
		JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey(); 			
			if(apikey != null){					 					
				//update the changes to the mailbox
				long companyId = Security.connectedCompanyId();
				status = WidgetRule.removeMsgRule(companyId,name);
				json.addProperty("error", (status==200? "none" : "delete was not successful") );
				
				//delete the list of widget rules from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget_Rules);
			}
			
		} 
		json.addProperty("status", status); 
		json.addProperty("title", name); 
		renderJSON(json.toString());
	}
	
	public static void activateRule(String title, boolean activated){
		JsonObject json = new JsonObject(); 
		int status = 404;
		if(!validation.hasErrors()){			
			//search for the connected company
			String apikey = Security.connectedApikey();			
			if(apikey != null) {				
			 	long companyId = Security.connectedCompanyId();				 					
				//update the changes to the mailbox
				status = WidgetRule.updateActivated(companyId, title, activated);
				json.addProperty("error", (status==200? "none" : "update was not successful") ); 
				
				//delete the list of widget rules from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget_Rules);
			}			
		}
		
		json.addProperty("status", status); 
		json.addProperty("title", title); 
		renderJSON(json.toString());
	}
	
	public static void show(@Required String msgrule, @Required String domain){ 
		
		String connected = Security.connectedDomain(); 			
		String apikey = Security.connectedApikey();
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){				
			String btnSave = ""; 	
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget_Rules, SubServiceName.BusinessRules);
			WidgetRule messageRule = null;
			List<String> quickResponses =null;
			
			if(validation.required(msgrule).ok){	
				msgrule = msgrule.replace("~~", "&");
				quickResponses = MessageManager.listOfQuickResponseTitles(Security.connectedApikey());
				
				//check if a rule is specified 		
				if(msgrule.equals("new")){
					 btnSave ="Save";   
				}else{
					 messageRule = WidgetRule.findByName(Security.connectedCompanyId(), msgrule);
					 btnSave ="Update"; 					 			
				}
			}else{
				msgrule = "new";
				btnSave = "Save";  
			}
			render(btnSave,quickResponses,messageRule,msgrule,domain,connectedUsername,ecomBlogPlugin,userType,service); 	
		
		}
		
		Apps.popout_not_found("");
	}
	
	public static void orderRules(@Required List<String> rules){
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();	 
			if(apikey != null){
				status = WidgetRule.orderRules(Security.connectedCompanyId(), rules);
				json.addProperty("error", (status==200?"none":"update was not successful") );
				
				//delete the list of recipients from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget_Rules);
			}
		}else{
			json.addProperty("error", "required fields are not specified");
		} 
		
		json.addProperty("status", status); 	
		renderJSON(json.toString());
	}

}
