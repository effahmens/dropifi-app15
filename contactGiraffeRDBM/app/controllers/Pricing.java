package controllers;

import static akka.actor.Actors.actorOf;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;

import job.process.EventTrackActor;

import akka.actor.ActorRef;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.stripe.model.Card;
import com.stripe.model.Subscription;

import models.AccountUser;
import models.Company;
import models.WidgetTracker;
import models.dsubscription.DownGradePlan;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubPlan;
import models.dsubscription.SubService;
import models.dsubscription.SubscriptionType;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.CardSerializer;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.MailTemplate;
import resources.UserType;
import resources.com.AccountType;
import resources.dstripe.StCustomer;
import resources.pricing.CancelAccountSerializer;
import resources.pricing.WebhookEvent;
import view_serializers.MailSerializer;

@With(Compress.class)
public class Pricing extends Controller {
	
	@Before 
	static void setArgs(String domain){		
	  	String apikey = Security.connectedApikey();
		String foundApikey = Company.findApikey(apikey); 
		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0){
			 renderArgs.put("page","plan");
			 renderArgs.put("title","Subscription - Plan");
		}else{			
			//clear the session
			try {
				session.clear();
		    	//response.removeCookie("rememberme"); 
				Secure.login(""); 
			} catch (Throwable e) {
				Application.index();
			}
		}			
	}
	
	public static void index(String domain){ 
		//TODO implemented security checks
		ForceSSL.redirectToHttps();
		
		
		UserType userTypes = Security.connectedUserType();
		if(UserType.isAgent(userTypes) ){	
			redirect("/"+domain+"/dashboard/index");			 
		} 
		
		renderArgs.put("sub_page","dashboard");
		String apikey = Security.connectedApikey();		
		 		
		if(!validation.hasErrors() && apikey !=null){			
			//check if domain is a valid
			
 			String connected = Security.connectedDomain();
 			domain =domain!=null? domain.trim():"";
 			if(!connected.equalsIgnoreCase(domain)){			
 				index(connected);
 			}
 				String connectedUsername = Security.connected(); 				
 				String ecomBlogPlugin = Security.connectedPlugin(); 
 				String userType = userTypes.name();
 				
 				//redirect the user to the dashboard plugin type is Prestashop
 				if(ecomBlogPlugin.equals(EcomBlogType.PrestaShop.name()) || ecomBlogPlugin.equals(EcomBlogType.Tictail.name()))
 					Dashboard.index(domain);
 				
 				//check if the widget is installed on the site of the user
 				//boolean hasWidget = WidgetTracker.hasWidgetInstalled(apikey,EcomBlogType.getValue(ecomBlogPlugin));  
 				
 				List<SubPlan> subPlans = SubPlan.getOrderedSubPlans();
 				///List<String> services = SubPlan.planServices(subPlans); 
 				
 				//select the current subscription plan
 				AccountType currentPlan = SubCustomer.findCurrentPlan(apikey); 
 				if(currentPlan==null){
 					currentPlan = AccountType.Free;
 				}else if(currentPlan.equals(AccountType.Enterprise) && !ecomBlogPlugin.equals("Wix")){
 					currentPlan = AccountType.Business;
 				}				
 				 
 		    	Map<String,List<SubService>> mapServices = SubService.getMapService(subPlans);
 		    	Set<String> keys = mapServices.keySet(); 
 		    	Map<Object,String> tooltips =SubService.getServiceTooltips(keys.toArray());
 				
 				render(userType,domain,connectedUsername,ecomBlogPlugin,subPlans,currentPlan,mapServices,keys,tooltips);  	 		    
 			/*}else{ 	
 				
 				//check if the domain is available  				
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
		
		Apps.page_not_found("");
	}
	
	public static void activity_summary(@Required String domain){
		//TODO implemented security checks
				ForceSSL.redirectToHttps();
				
				UserType userTypes = Security.connectedUserType();
				if(UserType.isAgent(userTypes) ){	
					redirect("/"+domain+"/dashboard/index");			 
				} 
				
				renderArgs.put("sub_page","dashboard");
				String apikey = Security.connectedApikey();		
				 		
				if(!validation.hasErrors() && apikey !=null){			
					//check if domain is a valid
					
		 			String connected = Security.connectedDomain(); 			  			
		 			if(connected.equalsIgnoreCase(domain)){
		 				String connectedUsername = Security.connected(); 				
		 				String ecomBlogPlugin = Security.connectedPlugin(); 
		 				String userType = userTypes.name();
		 				//check if the widget is installed on the site of the user
		 				//boolean hasWidget = WidgetTracker.hasWidgetInstalled(apikey,EcomBlogType.getValue(ecomBlogPlugin));
		 				WebhookEvent event = new WebhookEvent(DropifiTools.StripeSecretKey);
		 				List<Map<String,Object>> receipts = event.getPaymentReceipts(apikey);
		 				render(userType,domain,connectedUsername,ecomBlogPlugin,receipts);  	 		    
		 			}else{ 	
		 				
		 				//check if the domain is available  				
		 				boolean hasDomain = Company.isDomainAvailable(domain);
		 				if(hasDomain){	//show domain not found page
		 					Apps.multiple_login(connected);
		 				}else{
		 					Apps.domain_not_found(domain);
		 				}
		 			}
				}
				
				Apps.page_not_found("");
	}
	
	public void sendInvoice(){
		renderJSON(""); 
	}
	
	public static void subscription(@Required String type,int status, String msg ,boolean change){ 
		ForceSSL.redirectToHttps();
		/*UserType userType = Security.connectedUserType();
    	if(userType!=null && UserType.isAgent(userType))
			Dashboard.index(Security.connectedDomain());
    	*/
    	String apikey = Security.connectedApikey();	
    	if(!validation.hasErrors() && apikey!=null){
    		String stripeSecretKey= DropifiTools.StripeSecretKey;
    		String stripePublickey = DropifiTools.StripePublisahblekey;
    		AccountType accountType = AccountType.valueOf(type);
    		//check if the customer has credit card information created in stripe
    		SubCustomer cus = isSubscribed(apikey);
    		/*SubCustomer cus = SubCustomer.findByAppikey(apikey);
    		
    		if(cus==null){
    			//the customer subscription data is not created. create and assign the customer to a free plan
	    		Company company = Company.findByApikey(apikey);
	    		cus = company.subscribeToPlan(SubscriptionType.MONTHLY.name(), "", "");
	    		change =true;
    		}*/
    		
    		SubPlan plan = SubPlan.findByAccountType(accountType);
    		
    		//send a trial message
    		if(plan==null){ 
    			status = 200;
				subscription_success(accountType.name(),msg,status);
    		}
    		
    		String connectedUsername = Security.connected();
    		String cardName = cus.getName();
    		double savedAmount = DropifiTools.roundTwoDecimals((plan.getAmount()*12) - plan.getYearlyAmount());
    		 
			List<String> years = DropifiTools.listOfYears(); 
			String asignPlan = DropifiTools.changePlanName(accountType.name());
			
       		if(!accountType.equals(AccountType.Free) && (cus==null || cus.hasCardToken(stripeSecretKey)==null || change==true)){ 
       			render(plan,savedAmount,status, msg,connectedUsername,years,cardName,asignPlan,stripePublickey); 
    		}else{ 
    			asignPlan = AccountType.Free.name();
    			if(accountType.equals(AccountType.Free) && change==true){
    				status = 200;
    				subscription_success(accountType.name(),msg,status); 
    			}
    			
    			String subtype = cus.getSubscriptionType()!=null?cus.getSubscriptionType().name():SubscriptionType.MONTHLY.name();
    			//automatically subscribe the customer to plan
	       		Map<String,Object> subcribed = subscribeToPlan(accountType.name(), subtype, cus.getCardToken(), cus.getCardType(), cus.getName(),null,true);
	       		if(subcribed!=null){
	       			status = (int)subcribed.get("status");
	       			if(status==200){ 
			       		msg ="";
			    		subscription_success(accountType.name(),msg,status);
	       			}
	       		}else{
	       			msg="<p>A problem occurred whilst subscribing you to "+accountType.name()+" Plan.</p>" +
	       					"<p>Ensure that the credit card details you provided is correct.</p>";
	       		}
    		}
       		
       		render(plan,savedAmount,status, msg,connectedUsername,years,cardName,asignPlan,stripePublickey); 
    	} 
    	
    	Apps.page_not_found("");
    }
	
	public static void subscription_success(String accountType, String msg,int status){
		ForceSSL.redirectToHttps(); 
		
		String apikey = Security.connectedApikey();
		if(apikey!=null){
			String stripeSecretKey = DropifiTools.StripeSecretKey;
			String stripePublickey = DropifiTools.StripePublisahblekey;
			SubCustomer cus = SubCustomer.findByAppikey(apikey); 
			String subType = "";
			CardSerializer card =null;
			double amount = 0.00;
			if(cus!=null){
				card = new CardSerializer(cus.hasCardToken(stripeSecretKey));
				SubPlan plan = SubPlan.findByAccountType(AccountType.valueOf(accountType));
				if(plan!=null){
					if(cus.getSubscriptionType().equals(SubscriptionType.MONTHLY))
						amount = plan.getAmount();
					else
						amount = plan.getYearlyAmount();
				}
				subType = cus.getSubscriptionType().name().replace("MONTHLY", "Monthly").replace("YEARLY", "Yearly");
			}else{
				status = 301;
			}
			
			String domain = Security.connectedDomain();
			String asignPlan = DropifiTools.changePlanName(accountType);
			
			render(accountType,card,msg,status,amount,subType,domain,asignPlan,stripePublickey);
		}
		
		Apps.page_not_found("");
	}
	
	public static void subscription_free(String accountType){
		ForceSSL.redirectToHttps();
		String apikey = Security.connectedApikey();
		if(apikey!=null){
			String asignPlan = DropifiTools.changePlanName(accountType);
			render(asignPlan,accountType);
		}
		Apps.page_not_found("");
	}
	
	public static void subscribeCustomer(@Required String accountType, @Required String paymentOption, @Required String stripeToken, String stripeCardType, String cardName,String creditcode){ 
		ForceSSL.redirectToHttps();
		int rstatus=501;
		try{ 	
			String apikey = Security.connectedApikey();
			//verify that the coupon the user entered is valid
			if(creditcode!=null&& !creditcode.trim().isEmpty()){
				Map<String,Object>result = SubCustomer.verifyCredit(apikey,DropifiTools.StripeSecretKey, accountType, creditcode);
				rstatus = (int) result.get("status");
				if(rstatus!=200){
					String msg = (String)result.get("msg")+"<br> If you do not have a coupon, leave the field blank";
					subscription(accountType,rstatus,msg,true); 
				}
			}
			
			Map<String,Object> subcribed = subscribeToPlan(accountType, paymentOption, stripeToken, stripeCardType, cardName,creditcode,true);
			if(subcribed!=null){
				int status =(int)subcribed.get("status");
				String msg = (String)subcribed.get("msg");
				if(status==200){
					//Apply the coupon to the user
					subscription_success(accountType,msg,status);
				}
				subscription(accountType,status,msg,true); 
			}
		}catch(Exception e){
			String msg ="We could not subscribe you to "+accountType+" plan. Ensure the credit card information provided is valid";
			subscription(accountType,rstatus,msg,true); 
		}
		 
	} 
	
	public static void cancel_account(){
		render(); 
	}
	
	public static void cancelSubscriptionPlan(@Valid CancelAccountSerializer ca){
		JsonObject json = new JsonObject();
		int status = 505;
		String msg = "";
		
		if(!validation.hasErrors()){
			String apikey= Security.connectedApikey();
			if(apikey!=null){ 
				Company company = Company.findByApikey(apikey);
				if(company!=null){
					status = company.cancelAccount(ca.cancelEmail, ca.cancelPassword, ca.cancelMessage);
					if(status==501)
						msg = "The email address or password you entered is not correct. Please enter the correct email or password.";
					else if(status==200){
						session.clear();
						Security.removeCookie("rememberme");
					}else{
						msg="Your account could not be canceled. Try again"; 
					} 
				}else{
					msg="Your account do not exist"; 
				}
			}else{
				msg="Your account do not exist";
			}
		}
		 
		json.addProperty("status", status);
		json.addProperty("msg", msg);
		renderJSON(json.toString());
	}
	
	public static void redeemCredit(@Required String creditcode){
		JsonObject json = new JsonObject();
		String msg = "";
		if(!validation.hasErrors()){
			String apikey =Security.connectedApikey();
			if(apikey!=null){
				SubCustomer customer = SubCustomer.findByAppikey(apikey);
				if(customer!=null && !customer.getCurrentPlan().name().equals(AccountType.Free.name())){
					StCustomer cus = customer.redeemCredit(DropifiTools.StripeSecretKey, creditcode);
					if(cus!=null){
						msg = cus.getError();
					}else{
						msg="Your credit could not be redeemed. Try again or contact support@dropifi.com for help";
					}
				}else{
					msg="Upgrade to any of the premium plan to redeem credit";
				}
			}
		} 
		json.addProperty("msg", msg);
		renderJSON(json.toString());
	}
	
	public static void verifyCredit(@Required String plan, @Required String creditcode){
		JsonObject json = new JsonObject();
		int status=501;
		SubCustomer cus = SubCustomer.findByAppikey(Security.connectedApikey());
		if(cus!=null){
			Map<String,Object> result = cus.verifyCredit(DropifiTools.StripeSecretKey,plan, creditcode);
			if(result!=null){
				status = (int) result.get("status");
				Gson gson = new Gson();
				json.add("result", gson.toJsonTree(result));
			} 
		} 
		json.addProperty("status", status);
		renderJSON(json.toString());
	}
	
	@Util
	public static Map<String,Object> subscribeToPlan(@Required String accountType, @Required String paymentOption, @Required String stripeToken, String stripeCardType, String cardName,String coupon, boolean isNotify){
		int status = 501;
		String apikey = Security.connectedApikey();
		String msg="";
		Map<String,Object> subMap = null;
		if(apikey!=null){
			subMap = new HashMap<String,Object>();
			if(!validation.hasErrors()){
				String email = Security.connected();
				String widgetType = Security.connectedPlugin();
 				String userType =Security.connectedUserType().name();
 				long companyId = Security.connectedCompanyId();
 				String domain = Security.connectedDomain();
 				//cardName = Security.connectedName();
 				
 				SubCustomer cus = Company.subscribeToPlan(apikey,companyId,domain,email,cardName,accountType, paymentOption, stripeToken, stripeCardType, userType, widgetType,coupon);
				if(cus!=null && cus.getSubscription()!=null){
					 status = 200; 
					 msg = cus.getError();
					 //send a message to the customer if the chosen plan is free - 
					 if(cus.getCurrentPlan().name().equalsIgnoreCase(AccountType.Free.name()) && isNotify){  
						  new MailSerializer(cus.getName(), cus.getEmail(),"","You have opted out of premium customer engagement ",
								  MailTemplate.getFreePlanNotification(cus.getName(), cus.getEmail(), cus.getCurrentPlan().name())).tellMailingActor();
					 }
				}else if(cus!=null){
					msg = cus.getError();
				}else{
					msg ="An error occurred whilst validating your credit card information. Ensure that the credit card is valid";
				}
			}else{
				msg="Some of the required fields are not specified";
			}
			
			subMap.put("status", status);
			subMap.put("msg", msg); 
			return subMap;
		}
 
		return null;
	}
	
	/***
	 * Subscribe the customer to free plan if the customer subscription data is not created or 
	 * has no credit card information created in stripe. 
	 * @param apikey
	 * @return
	 */
	@Util
	public static SubCustomer isSubscribed(String apikey){
		//
		SubCustomer cus = SubCustomer.findByAppikey(apikey); 
		if(cus==null){
			//the customer subscription data is not created. create and assign the customer to a free plan
    		Company company = Company.findByApikey(apikey);
    		cus = company.subscribeToPlan(SubscriptionType.MONTHLY.name(), "", ""); 
		}
		return cus;
	}
	
}
