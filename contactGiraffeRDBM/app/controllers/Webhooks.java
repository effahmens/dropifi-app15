package controllers;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.Company;
import models.addons.AddonDownGrade;
import models.addons.AddonSubscription;
import models.dsubscription.DownGradePlan;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubPaymentEvent;
import models.dsubscription.SubscriptionType;

import org.codehaus.jackson.JsonNode;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import api.wix.WixClient;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.Http.Header;
import play.mvc.With;
import query.inbox.InboxSelector;
import resources.AccountType;
import resources.DCON;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.MailTemplate;
import resources.MsgStatus;
import resources.UserType;
import resources.dstripe.StEvent;
import resources.pricing.WebhookEvent;
import view_serializers.CoProfileSerializer;
import view_serializers.MailSerializer;


@With(Compress.class)
public class Webhooks extends Controller{
	
	public static void pricing(){
		JsonObject json = new JsonObject();
		Gson gson = new Gson();
		json.addProperty("status", 200); 
		//play.Logger.log4j.info("Just Test Webhook 1");
		Map<String,String> map = params.allSimple();
		
		if(map!=null){ 
 
			String object = map.get("body");
			if(object!=null){
 
				try{
					JsonElement jele = gson.toJsonTree(object);
					JSONObject js = (JSONObject) new JSONParser().parse(jele.getAsString());
					String id = (String)js.get("id"); 
					String type = (String) js.get("type");
					
					StEvent event = new WebhookEvent(DropifiTools.StripeSecretKey);
					if(type.startsWith("invoice")){
						event.processInvoice(DropifiTools.StripeSecretKey,id, type,js);
					}else if(type.startsWith("charge")){
						event.processCharge(id, type,js);
					}
					
					//play.Logger.log4j.info("Id: "+id+" - type: "+type);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.info(e.getMessage(),e); 
				}
			}
			
		} 
		 
		response.status = play.mvc.Http.StatusCode.OK;
		renderJSON(json.toString());
	}
	
	public static void wixCallback(String instance){
		JsonObject json = new JsonObject();
		try{
			JsonNode node  = WixClient.getInstance(instance, DropifiTools.WixSecretkey); 
			
			if(node!=null){
				String instanceId = WixClient.getString(node.get("instanceId"));
				String vendorProductId = WixClient.getString(node.get("vendorProductId"));
				if(instanceId!=null){
					Company found = Company.findByEcomBlogKey(instanceId, EcomBlogType.Wix);
					 found.subscribeWixToPlan(vendorProductId,instanceId, EcomBlogType.Wix).getCurrentPlan();
				}
				
				json.addProperty("instanceId", instanceId);
				json.addProperty("vendorProductId", vendorProductId);
			}
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		
		response.status = play.mvc.Http.StatusCode.OK;
		json.addProperty("status",200);
		renderJSON(json.toString());
	}
	
	public static void wixCallbackEndPoint(String instance){
		JsonObject json = new JsonObject();
		try{
			
			String instanceId=null,eventType=null,signature,timestamp;
			
			for(String key :request.headers.keySet()){
				String value =request.headers.get(key).value().trim();
				switch(key){
				case "x-wix-timestamp":
					timestamp = value;
					break;
				case "x-wix-signature":
					signature = value;
					break;
				case "x-wix-instance-id":
					instanceId = value;
					break;
				case "x-wix-event-type":
					eventType = value;
					break;
				}
			}
			
			if(eventType!=null &&(eventType.equals("/billing/upgrade") || eventType.equals("/billing/cancel"))){
				String vendorProductId=null;
				if(eventType.equals("/billing/upgrade")){
					Map<String,String> map = params.allSimple(); 
					if(map!=null){
						JSONObject js = (JSONObject) new JSONParser().parse(new Gson().toJsonTree(map.get("body")).getAsString());  
						vendorProductId = (String)js.get("vendor-product-id"); 
					}
				} 
				
				if(instanceId!=null){ 
					Company found = Company.findByEcomBlogKey(instanceId, EcomBlogType.Wix);
					if(found!=null){
						found.subscribeWixToPlan(vendorProductId,instanceId, EcomBlogType.Wix).getCurrentPlan();
						
						//Notify Dropifi Admin of the successful upgrade
						String notifyMsg = "<p>This is an upgrade or cancel notification</p>"
								+ "<p>Event Type: "+eventType+"</p>"
										+ "<p>Vendor Product ID: "+vendorProductId+"</p>"
												+ "<p>Customer Email: "+found.getEmail()+"</P";
						MailSerializer notify = new MailSerializer("Philips","philips+invoice@dropifi.com","","Wix Payment From "+found.getEmail(),notifyMsg);								
						notify.tellMailingActor();
					}
				}
				
				json.addProperty("instanceId", instanceId);
				json.addProperty("vendorProductId", vendorProductId);
			}
			 
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		
		response.status = play.mvc.Http.StatusCode.OK;
		json.addProperty("status",200);
		renderJSON(json.toString());
	}

	public static void inboxUpdater(@Required String apikey,@Required long companyId,@Required String secretKey){
		JsonObject json = new JsonObject();
		int res_status = 501;
		try{
			if(!validation.hasErrors()){
				
				String foundApikey = Company.findApikey(apikey);
				if(foundApikey!=null && foundApikey.equals(apikey) && secretKey.equals(DropifiTools.InboxUpdater)){
					InboxSelector.findMessages(apikey,companyId, "", 1, 50,true,0,true);
					InboxSelector.findCounters(apikey,companyId,true);
					InboxSelector.findUnResolvedMessages(apikey,companyId,0,true);
					InboxSelector.findMessages(apikey,companyId,"", true,1, 50,true,0,true);
					InboxSelector.findMessages(apikey,companyId,"", false,1, 50,true,0,true);
					
					for(String sentiment : "Negative,Positive,Neutral".split(",")){
						InboxSelector.findSentimentMessages(apikey,companyId, "", sentiment,1, 50,true,0,true);
						InboxSelector.findSentimentMessages(apikey,companyId, "", sentiment,true,0, 50,true,0,true);
						InboxSelector.findSentimentMessages(apikey,companyId, "", sentiment,false,0, 50,true,0,true);
					}
					
					for(MsgStatus status : MsgStatus.values()){
						InboxSelector.findStatusMessages(apikey,companyId, "", status,1, 50,0,true);
						InboxSelector.findStatusMessages(apikey,companyId, "", status,true,1, 50,0,true);
						InboxSelector.findStatusMessages(apikey,companyId, "", status,false,1, 50,0,true);
					}
					
					for(String subject : InboxSelector.listOfMsgSubjects(apikey)){
						InboxSelector.findSubjectMessages(apikey,companyId, subject,0,true);
						InboxSelector.findSubjectMessages(apikey,companyId, subject,true,0,true);
						InboxSelector.findSubjectMessages(apikey,companyId, subject,false,0,true);
					}
				}
				res_status = 200;
			}
			json.addProperty("status", res_status);
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(),e);
		} 
		//play.Logger.log4j.error("Apikey: "+apikey+" , companyId: "+companyId+", secretKey: "+secretKey);
		renderJSON(json.toString());
	}
	
	/* 
	 public static void index(String arrComp){
		long total=0;
		JsonArray companies = new JsonArray();
		if(arrComp!=null && !arrComp.isEmpty()){
			JsonParser parser = new JsonParser();
			companies = parser.parse(arrComp).getAsJsonArray(); 
			total= companies!=null?companies.size():0;
		}
		render(total,companies);
	}
	
	public static void downgrade(){
		Long total = Company.count();
		JsonArray companies= new JsonArray();
		for(Long id=0l;id<total;id++){
			try{
				Map<String,Object> data= Company.findByEmailPassword(id, UserType.Admin);
				if(data==null)
					continue;
				Company company = (Company)data.get("company");
				if(company!=null){
					if(!company.getApikey().startsWith("delete")){
						SubCustomer found = SubCustomer.findByAppikey(company.getApikey());
						//play.Logger.log4j.info("Company found: "+ company.getEmail());
						
						if(found==null){
							//play.Logger.log4j.info("Not subscribed to a plan: "+ company.getEmail());
							found = company.subscribeToPlan(SubscriptionType.MONTHLY.name(), "", "");
							if(found!=null && found.getCurrentPlan().name().equals(AccountType.Free.name())){
								MailSerializer mail = new MailSerializer((String)data.get("username"),company.getEmail(),"","New pricing tiers go into effect today",MailTemplate.getBetaDownGradeNotification((String)data.get("username"),company.getEmail(), company.getAccountType().name()));							
								mail.tellMailingActor();
								
								JsonObject object = new JsonObject();
								object.addProperty("name",(String)data.get("username"));
								object.addProperty("email",company.getEmail());
								object.addProperty("accountType",company.getAccountType().name());
								object.addProperty("url",company.getProfile().getUrl());
								object.addProperty("created",company.getCreated().toString());
								companies.add(object); 
							}
						} 
					}else{
						//play.Logger.log4j.info("Company is deleted: "+ company.getEmail());
					}
				}
			}catch(Exception e){
				
			}
		}
		play.Logger.log4j.info("Total: "+ companies.size());
		index(companies.toString());
		
	}*/
	
	public static void downGradePlan(@Required String apikey,@Required String dwongradeKey){
		int status =501;
		if(!validation.hasErrors() && dwongradeKey.equals(DropifiTools.InboxUpdater)){
		 
			try{
				//lock the plan so that no other process will have access to it
				DownGradePlan plan = DownGradePlan.findByApikey(apikey);
				if(plan!=null){
					//play.Logger.log4j.info("Downgrade initiated: "+ plan.getApikey()); 
					if(plan.updateLock(true)){
						//determine if it is time to send notification
						try{ 
							Date time = new Date();
							WebhookEvent event = new WebhookEvent(DropifiTools.StripeSecretKey);
							if(plan.getCounter()<=2){
								if(time.after(plan.getNotifyDate())){		
									//set down grade date to 1 weeks
									DateTime datetime = new DateTime(time.getTime()).plusWeeks(2);
									plan.updateNotifyDate(new Timestamp(time.getTime()), new Timestamp(datetime.toDate().getTime()));			
									event.sendFailureNotification(SubPaymentEvent.findEvent(plan.getApikey(),plan.getTypeId()),true);
									status = 201;
								} 
							}else{
								//suspend the account
								if(plan.downGradePlan()){
									if(Company.downGradePlan(plan.getApikey())!=null){
										//send downgrade message 
										event.sendFailureNotification(SubPaymentEvent.findEvent(plan.getApikey(), plan.getTypeId()),false);
										//DownGradePlan.resetPlan(apikey,AccountType.Free.name());
										status =200;
									}
								}
							}
							//play.Logger.log4j.info("Downgrade finish");
						}catch(Exception e){
							play.Logger.log4j.info(e.getMessage(),e);
						}
						plan.updateLock(false);
					}/*else{
						play.Logger.log4j.info("Downgrade loc");
					} */
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}else{
			
		}
		
		JsonObject json = new JsonObject();
		json.addProperty("status", status);
		renderJSON(json.toString());
	}
	
	public static void addonDowngrade(@Required String apikey,@Required String platformKey, String title, @Required String downgradeKey){
		int status = 501;
		JsonObject json = new JsonObject();
		if(!validation.hasErrors() && downgradeKey.equals(DropifiTools.InboxUpdater)){
			
			if(AddonDownGrade.initiateDownGrade(platformKey, apikey)){
				try{
					CoProfileSerializer profile = Company.findInfoByApikey(apikey);
					if(profile!=null){ 
						AddonSubscription addon = AddonSubscription.findAddon(apikey, platformKey);
						
						if(addon!=null && addon.downgrade()!=null){ 
							
							Addons.resetWidget(apikey);
							
							//send addon downgrade notification 
							String subject = "Your Dropifi "+title +" trial period has end",
								msg=MailTemplate.getAddonDowngradeNotificationB(profile.getMainName(), profile.getEmail(), profile.getDomain(),title, platformKey, "");
							
							MailSerializer mail = new MailSerializer(profile.getMainName(),profile.getEmail(),"",subject,msg);								
							mail.tellMailingActor();
							
							//notify the dropifi team of the downgrade 
							if(play.Play.mode.isProd()){
								  new MailSerializer("Support","support@dropifi.com","","APPS : "+subject,msg).tellMailingActor(); 
							}else{
								 new MailSerializer(DropifiTools.mailName, DropifiTools.mailEmail,"","APPS: "+subject,msg).tellMailingActor();	 
							}
							
							status =200;
							json.addProperty("email",profile.getEmail());
						}
					} 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
					json.addProperty("error", e.getMessage());
					status =301;
				}
			}else{ 
				status =305;
			}
		} 
		
		json.addProperty("status", status);
		renderJSON(json.toString());
	}


}
