package controllers;

import models.review.ReviewApp;

import com.google.gson.JsonObject;

import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.With;
import resources.EcomBlogType;

@With(Compress.class)
public class ReviewApps extends Controller{
	
	public static void show(String domain){
		
		JsonObject json = new JsonObject();		
		int status = 501;
		
		String c_domain = Security.connectedDomain();
		if(c_domain !=null && c_domain.equals(domain)){
			
			String pluginType = Security.connectedPlugin();
			ReviewApp review = new ReviewApp(Security.connectedApikey(), pluginType);
			
			json.addProperty("showReview", review.showReview(Security.connectedCreated())); 	
			json.addProperty("pluginType", pluginType);
			json.addProperty("loginName", Security.connectedName());
			
			status = 200;
		}
		
		json.addProperty("status", status);
		renderJSON(json.toString());
	}
	
	public static void redirectToPage(@Required Boolean isReviewed){
		JsonObject json = new JsonObject();		
		int status = 501;
		if(!validation.hasErrors()){
			 String apikey = Security.connectedApikey();
			 status = ReviewApp.updateReview(apikey, isReviewed);
		}		 
		json.addProperty("status", status);
		renderJSON(json.toString());
	}
	
	
}
