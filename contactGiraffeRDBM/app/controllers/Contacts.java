package controllers;

import java.util.List;

import models.AccountUser;
import models.Company;
import models.Customer;
import models.DropifiMessage;
import models.MsgCounter;
import models.WidgetTracker;
import models.dsubscription.SubServiceName;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import query.analytics.CustSegSelector;
import query.analytics.MainAnalyticsSelector;
import query.inbox.Contact_Serializer;
import query.inbox.InboxSelector;
import query.inbox.MessageDetailSerializer;
import resources.CacheKey;
import resources.EcomBlogType;
import resources.MsgStatus;
import resources.RangeType;
import resources.UserType;
import resources.Cache.Cache_Analytics;
import resources.helper.ServiceSerializer;
@With(Compress.class)
public class Contacts extends Controller{
	
    @Before
    static void setArgs(){
    	String apikey = Security.connectedApikey();	
		String foundApikey = Company.findApikey(apikey);
		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0){  
			 renderArgs.put("page","contacts");
			 renderArgs.put("title","Agent - Contacts"); 
		}else{
			//clear the session
	    	session.clear();
	    	//response.removeCookie("rememberme");			
			try {
				Secure.login("");
			} catch (Throwable e) {
				Application.index(); 
			}
		} 	   
    }
	
	public static void index(String domain){
		ForceSSL.redirectToHttps();
		
		String apikey = Security.connectedApikey();	
		
		if(!validation.hasErrors() && apikey !=null){	
			String connected = Security.connectedDomain();
			domain =domain!=null? domain.trim():"";
			if(!connected.equalsIgnoreCase(domain)){			
				index(connected);
			}
			
				String[] letters = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
				String connectedUsername = Security.connected();
				String userType = Security.connectedUserType().name();
				
				String ecomBlogPlugin=Security.connectedPlugin();
				//check if the widget is installed on the site of the user
				boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin)); 
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
				render(userType,domain,letters,connectedUsername,ecomBlogPlugin,hasWidget,service); 	
 			/*}else{
 				//check if the domain is available 
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
		Apps.page_not_found("");
	}
	
	public static void fetch_all_contacts(@Required int start, @Required int end) {
		JsonObject json = new JsonObject();	
		  int status = 405;
		  if(!validation.hasErrors()){
			  	String apikey = Security.connectedApikey();
			  	Long companyId = Security.connectedCompanyId();//Company.findCompanyId(apikey);
				String userEmail = Security.connected();
				
				UserType userType = Security.connectedUserType();	
				
				if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
					String userMailboxes = "";//AccountUser.getQueryMailboxes(apikey, userEmail, userType);	
					
					Gson gson = new Gson();  
					json.add("contacts", InboxSelector.findContacts(companyId,userMailboxes, start, end,true)); 
					json.add("count", gson.toJsonTree(InboxSelector.findSentimentCounter(companyId,null))); 
					json.addProperty("sources","Widget Message"/* InboxSelector.sourceList(Security.connectedApikey(), Security.connected(), Security.connectedUserType())*/);
				    
				  status = 200;
				}else{ 
				  status = 700;
				  json.addProperty("error", "invalid account, login with the correct credentials");
			  }
			  
		  }else{
			  json.addProperty("error", "required fields are not specified");
		  }
		  
		  json.addProperty("status", status);
		  renderJSON(json.toString());		
	}
	
	public static void fetch_all_contacts_like(@Required String like,@Required String mode, String sentiment, @Required int start, @Required int end) {
		JsonObject json = new JsonObject();	
		int status = 405;
		if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();
		  	Long companyId =Security.connectedCompanyId();// Company.findCompanyId(apikey); 		 
			String userEmail = Security.connected();
			UserType userType = Security.connectedUserType();	
			Gson gson = new Gson();
			
			if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
				String userMailboxes = ""; //AccountUser.getQueryMailboxes(apikey, userEmail, userType);		
				 		 
				  if(mode.equalsIgnoreCase("all")){					  
					  json.add("contacts", InboxSelector.findContacts(companyId,userMailboxes,like,start, end,true)); 
					  json.add("count", gson.toJsonTree(InboxSelector.findSentimentLikeCounter(companyId,null,like))); 
				  }else if(mode.contains("true")|| mode.contains("false")){					  
					  
					boolean isCustomer = Boolean.parseBoolean(mode);
					json.add("contacts", InboxSelector.findContacts(companyId,userMailboxes,like,isCustomer,start, end,true));					
					json.add("count", gson.toJsonTree(InboxSelector.findSentimentLikeCounter(companyId, isCustomer,like)) );   			  
				  } 
				   
				  status = 200;
			  }else{ 
				  status = 700;
				  json.addProperty("error", "invalid account, login with the correct credentials");
			  }
			  
		  }else{
			  json.addProperty("error", "required fields are not specified");
		  }
		  
		  json.addProperty("status", status);
		  renderJSON(json.toString());		
	}
	
	public static void people_with_sentiment(@Required String mode, String source, @Required String sentiment, @Required String like, int start,   int end){
		JsonObject json = new JsonObject();	
		int status = 405;
		if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();
		  	Long companyId =Security.connectedCompanyId();// Company.findCompanyId(apikey);	 
			String userEmail = Security.connected();			 
			UserType userType = Security.connectedUserType();
			
			if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
				 	 		 
				  if(mode.equalsIgnoreCase("all")) {					  
					  json.add("contacts", InboxSelector.findContactsLikeWithSentiment(companyId,sentiment,like,end)); 
					  //json.add("count", gson.toJsonTree(InboxSelector.findSentimentCounter(companyId,null))); 
				  }else if(mode.equalsIgnoreCase("true") || mode.equalsIgnoreCase("false")){					  
					  boolean isCustomer = Boolean.parseBoolean(mode);
					  json.add("contacts", InboxSelector.findContactsLikeWithSentiment(companyId,sentiment,like,isCustomer,end));
					  //json.add("count", gson.toJsonTree(InboxSelector.findSentimentCounter(companyId, isCustomer)) ); 
				  }  				  
				 				 
				  status = 200;
			  }else{ 
				  status = 700;
				  json.addProperty("error", "invalid account, login with the correct credentials");
			  }
			  
		  }else{
			  json.addProperty("error", "required fields are not specified");
		  }
		  
		  json.addProperty("status", status); 
		  renderJSON(json.toString()); 
	}
	
	public static void sviewMessages_old(@Required String email, @Required String mailbox,@Required boolean panel, String sentiment, @Required int maxi){
		JsonObject json = new JsonObject();
		  Gson gson = new Gson();
		  int status = 405;
		  
		  if(!validation.hasErrors()){
			  	String apikey = Security.connectedApikey();
			  	Long companyId = Security.connectedCompanyId(); //Company.findCompanyId(apikey); 
				String userEmail = Security.connected();				 
				UserType userType = Security.connectedUserType();	
				
				if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
					String userMailboxes = "";//AccountUser.getQueryMailboxes(apikey, userEmail, userType);	
				  
				 Contact_Serializer contact = InboxSelector.findCustomerProfile(companyId, email);  
				 if(contact!=null){
					 json.add("contact", gson.toJsonTree(contact));	
					 JsonArray messages=null;
					 maxi =3;
					 if(mailbox.equalsIgnoreCase("all mailboxes")){
						 if(panel ==false){
							 messages = InboxSelector.findContactMessages(companyId,userMailboxes, email, maxi,true); 
						 }else{
							 messages = InboxSelector.findContactMessagesWithSentiment(companyId,userMailboxes, email,sentiment, maxi,true); 
						 }
					 }else{
						 if(panel ==false){
							 messages = InboxSelector.findContactMessages(companyId, email,mailbox, maxi); 
						 }else{
							 messages = InboxSelector.findContactMessagesWithSentiment(companyId, email,mailbox,sentiment, maxi);
						 }
					 }					
					 json.add("messages", messages);
					 
					 status=200; 
				 } 
			  }else{			  
				  //Redirect to login page
				  json.addProperty("error", "invalid account, login with the correct credentials");
			  }
			  
		  }else{
			  json.addProperty("error", "required fields are not specified");
		  }
		  
		  json.addProperty("status", status);
		  renderJSON(json.toString());
	}	
		
	public static void viewMessages(@Required String email){
	  JsonObject json = new JsonObject();
	  Gson gson = new Gson();   
	  int status = 405;
	  
	  if(!validation.hasErrors()){
		  	String apikey = Security.connectedApikey();
		  	Long companyId = Security.connectedCompanyId();//Company.findCompanyId(apikey); 
			String userEmail = Security.connected();				 
			UserType userType = Security.connectedUserType();	
			
			if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
			 		  
			 Contact_Serializer contact = InboxSelector.findCustomerProfile(companyId, email);  
			 if(contact!=null){
				json.add("contact", gson.toJsonTree(contact));
				
				//json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessagesFromContact(companyId, email, null, null));
				json.add("trdsenmsg", MainAnalyticsSelector.TrendingSentimentsAndMessages(companyId, email,30));
				json.add("sentiment", CustSegSelector.custMsgSentimentsFromContact(companyId, email, null, null));				
				json.addProperty("nmsgs", CustSegSelector.numberOfMessagesFromContact(companyId, email, null, null));
				json.addProperty("rmsgs", CustSegSelector.UnResolvedMessagesFromContact(companyId, email, null, null));
				json.addProperty("avgSentScore", CustSegSelector.AverageSentimentScoreFromContact(companyId, email, null, null)); 
				
				status=200; 
			 } 
		  }else{			  
			  //Redirect to login page
			  json.addProperty("error", "invalid account, login with the correct credentials");
		  }
		  
	  }else{
		  json.addProperty("error", "required fields are not specified");
	  }
	  
	  json.addProperty("status", status);
	  renderJSON(json.toString());
	}
	
	public static void search_contact(@Required String search) {
		  JsonObject json = new JsonObject();	
		  int status = 405;
		  if(!validation.hasErrors()){
			  	String apikey = Security.connectedApikey();
			  	Long companyId = Security.connectedCompanyId();//Company.findCompanyId(apikey);
				String userEmail = Security.connected();
				
				UserType userType = Security.connectedUserType();	
				
				if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
				    //String userMailboxes = "";//AccountUser.getQueryMailboxes(apikey, userEmail, userType);	
					
					Gson gson = new Gson();  
					json.add("contacts", InboxSelector.searchContacts(companyId, search));  
					json.add("count", gson.toJsonTree(InboxSelector.findSentimentCounter(companyId,null))); 
					json.addProperty("sources","Widget Message"/* InboxSelector.sourceList(Security.connectedApikey(), Security.connected(), Security.connectedUserType())*/);
				    
				  status = 200;
				}else{ 
				  status = 700;
				  json.addProperty("error", "invalid account, login with the correct credentials");
				} 
		  }else{
			  json.addProperty("error", "required fields are not specified");
		  }
		  
		  json.addProperty("status", status);
		  renderJSON(json.toString());
	}
	
	public static void search_email(){ 
		  JsonObject json = new JsonObject(); 
		  Gson gson = new Gson();
		  
		  Long companyId = Security.connectedCompanyId(); 
		  int status = 405;
		  if(companyId!=null){
			  List<String> searchTerm = Customer.getEmailList(companyId);		  
			  json.add("search", gson.toJsonTree(searchTerm));
			  status =200;
		  }
		  
		  json.addProperty("status", status);	 
		  renderJSON(json.toString()); 
	}
	
	// viewMessages With Cache
	/*public static void viewMessages(@Required String email)
	{
		String json = Cache_Analytics.InboxAnalyticsData(Security.connectedApikey(), RangeType.all, email, Security.connected(), Security.connectedUserType(), validation.hasErrors());
		renderJSON(json);
	}*/
	
}