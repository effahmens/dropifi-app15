package controllers;
 
import models.pluginintegration.Integration;
import com.google.gson.JsonObject; 
import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.With;
import resources.DropifiTools;
import resources.EcomBlogType;
@With(Compress.class)
public class CashieCommerce extends Controller{

	public static void signup(@Required String displayName,@Required String user_email,	@Required String user_password,
			@Required String user_re_password,@Required String user_domain,@Required  String site_url, String type, String s,String location, String secretkey,String phoneNumber){ 
		
		Integration  found = Integration.findByAuthCode(secretkey); 
		if(found!=null && found.isAuthorized()){
			Integrations.signup(displayName, user_email, user_password, user_re_password, user_domain, site_url, site_url, DropifiTools.generateCode(12), site_url,"json", s, location, EcomBlogType.CashieCommerce,phoneNumber);
		}else{
			Integrations.returnResponse(new JsonObject(), 203, "Not an authorized platform for widget installation");
		}
	}
	
	public static void login(@Required String temToken,@Required String userEmail,String IPAddress,String location,String secretkey){ 
		Integration  found = Integration.findByAuthCode(secretkey); 
		if(found!=null && found.isAuthorized()){
			Integrations.login(temToken, userEmail, IPAddress, location, EcomBlogType.CashieCommerce);
		}else{ 
			Integrations.returnResponse(new JsonObject(), 203, "Not an authorized platform for widget installation");
		}
	}
	
	public static void loginToken(@Required String login_email,@Required String login_password,@Required String site_url,String type, String s,String secretkey){
		Integration  found = Integration.findByAuthCode(secretkey); 
		if(found!=null && found.isAuthorized()){ 
			Integrations.loginToken(login_email, login_password, site_url,DropifiTools.generateCode(12), site_url, "json", s, EcomBlogType.CashieCommerce);
		}else{
			Integrations.returnResponse(new JsonObject(), 203, "Not an authorized platform for widget installation");
		}
	}
	
}
