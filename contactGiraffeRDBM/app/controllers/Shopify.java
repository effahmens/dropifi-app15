package controllers;

import static akka.actor.Actors.actorOf;

import java.net.URI;
import java.sql.Timestamp; 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import job.process.EventTrackActor;
import job.process.MailingActor;
import job.process.WidgetTrackerActor;
import models.Company;
import models.CompanyProfile;
import models.EcomBlogPlugin;
import akka.actor.ActorRef;

import com.shopify.api.APIAuthorization;
import com.shopify.api.client.ShopifyClient;
import com.shopify.api.credentials.Credential;
import com.shopify.api.credentials.ShopifyCredentialsStore;
import com.shopify.api.endpoints.AuthAPI;
import com.shopify.api.endpoints.ProductsService;
import com.shopify.api.endpoints.ScriptTagsService;
import com.shopify.api.endpoints.ShopsService;
import com.shopify.api.resources.Product;
import com.shopify.api.resources.ScriptTag;
import com.shopify.api.resources.Shop;
import com.shopify.connectors.ShopifyAPIConnect;

import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.*;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import resources.WidgetTrackerSerializer;
import view_serializers.MailSerializer;
@With(Compress.class)
public class Shopify extends Controller {
	
	public static void index(String shop,String new_useremail,boolean isEmail){
		if(flash.get("new_account")==null && flash.get("existing_account")==null)
			flash.put("new_account", true);
		
		String shop_s = session.get("shopify_shop");
		 
		if(shop_s == null || shop_s.trim().isEmpty()) {		
			Security.signup("");
		}else{
			shop = shop_s;
		}
		
		render(shop, new_useremail,isEmail);	 
	}
	
	public static void show_new(String shop,String email,boolean isEmail,String error){
		flash.put("new_account", true);
		flash.put("new_useremail", email); 
		if(error!=null) 
			flash.error(error);
		
		flash.keep();
		index(shop, email,isEmail);  
	}
	
	public static void show_existing(String shop,String email,boolean isEmail){
		flash.put("existing_account", true);
		index(shop, email,isEmail);
	} 
	
	public static void login_oauth2(String code){
		String accessToken = session.get("shopify_code"); 
		//play.Logger.log4j.info("Code: "+code);
		
		if(accessToken ==null){	  
			session.remove("shopify_code"); 
		}else{ 
			
		} 
	}
	
    public static void login(String shop,String t,String timestamp, String signature) { 
    	//remove all data from the session
    	String accessToken = session.get("shopify_code"); 
    	String email = "";
    	boolean isEmail = false;
    	
    	if(accessToken == null){	    
    		Credential cred = new Credential(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey,  shop);
			AuthAPI auth = new AuthAPI(new APIAuthorization(cred));				 
			URI url = auth.getAuthRequestURI();			 
			removeFromSession();			 
			
			session.put("shopify_code", shop); 
			//play.Logger.log4j.info("Request URL: "+ url.toString());
			//check if the url is secure
			redirect(url.toString()); 			
    	}else{
    		 
    		session.clear(); 
    		try{ 
    			 
				ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, shop,t,timestamp,signature);				 
				ShopifyClient client = new ShopifyClient(connect.getCredential());  
				
				//request for the shop info
				ShopsService myShop = client.constructService(ShopsService.class);
				
				if(myShop != null){ 
					
					Shop sh = null;
					
					try{
						sh = myShop.getShops(); 
						session.remove("shopify_code"); 
					}catch(Exception e) {
						//shop authentication failed. Repeat authentication
						play.Logger.log4j.info(e.getMessage(),e);  
						session.remove("shopify_code");						
						//login(shop, t, timestamp, signature);
						String msg = "<p>Could not access your store - "+shop+". <br>Ensure that your are installing Dropifi contact widget " +
								"from the Shopify App store.<br> <a href='http://apps.shopify.com/dropifi-contact-widget'>Click here to view the app on Shopify</a></p>";
						
						Apps.page_not_found(msg);
					}
					
					if(sh != null){			 
						
						addToSession(shop,t, timestamp, signature);						
									
						EcomBlogType ecomType = EcomBlogType.Shopify;
						Company company = Company.findByEcomBlog(shop, ecomType);
						//check if the user shopify account is created 
						if(company != null && company.getId()!=null){
							EcomBlogType pluginType = company.getEcomBlogPlugin().getEb_pluginType();

							//returning user
							EcomBlogPlugin plugin = new EcomBlogPlugin(company.getEmail(), sh.getMyshopifyDomain(), t, signature, EcomBlogType.Shopify);
							CompanyProfile profile = new CompanyProfile(sh.getShopOwner(), sh.getMyshopifyDomain(), sh.getTimezone(),sh.getPlanName(), sh.getCountry());					
							
							//make changes the account
							company.setProfile(profile); 
							company.setEcomBlogPlugin(plugin);
							company.setDomain(plugin.getDomain()); 
							company = company.save();  
							
							//redirect the owner to the inbox page
							if(company.getDomain().equalsIgnoreCase(plugin.getDomain())){
								//add the js script to shopify
								company.getEcomBlogPlugin().addShopifyScriptTag(timestamp);
								
								try{
									WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),sh.getMyshopifyDomain(),sh.getMyshopifyDomain());
									ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
									trackActor.tell(track);  
								}catch(Exception e){
									play.Logger.log4j.error(e.getMessage(), e);
								}
								
								//clear the seesion
								Shopify.removeFromSession(); 		
								session.clear(); 
								
								//set the session
								Security.setLoginSession(company,"","LOGIN","");
								//play.Logger.info("compare shop info" + sh.getMyshopifyDomain() +" : "+shop);
								PrestaShop.revertToFree(company.getApikey(),pluginType, plugin.getEb_pluginType());
								
								Dashboard.index(company.getDomain()); 
							}
							
							//redirect("/"+company.getDomain()+"/inbox/index");   
						} 
						
						//isEmail = Company.isEmailAvailable(sh.getEmail());
						
						email = sh.getEmail();
						//play.Logger.info("has shop info" + sh.getMyshopifyDomain()); 
						//ask the user to create a dropifi account  
					}else{
						//play.Logger.info("Access Token 1:  shop authentication failed. Repeat authentication");
						//shop authentication failed. Repeat authentication
						session.remove("shopify_code");						
						login(shop, t, timestamp, signature);
					}
				} 				
				
			}catch (Exception e){
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(),e);  
			}finally{    		 
				session.remove("shopify_code");
			}
		}
    	
        show_new(shop,email,isEmail,""); 
    }
    
    public static void createNewAccount(@Required String new_useremail, @Required String userpassword, @Required String confirmPassword,String location, String phoneNumber){    	    	    	
    	String shop 	 = session.get("shopify_shop"),
    		   t    	 = session.get("shopify_t"),
    	       timestamp = session.get("shopify_timestamp"),
    	       signature = session.get("shopify_signature");     	
    	
    	if(validation.hasErrors()){
    		//boolean isEmail = new_useremail!=""?Company.isEmailAvailable(new_useremail):false;
    		show_new(shop,new_useremail,true,"Oops, email address and password are required");
    	}else if(!userpassword.equals(confirmPassword)) {//check password match
    		    		 
    		//boolean isEmail = new_useremail!=""?Company.isEmailAvailable(new_useremail):false; 
    		show_new(shop,new_useremail,true,"Oops, password and re-passoword do not match");		 
    	}else if(Company.isEmailAvailable(new_useremail)){		 
    		show_new(shop,new_useremail,true,"Oops, the email  address "+new_useremail+" belongs to an existing account.");	 
    	} 
    	
    	//check if the account is not created					 		
		if(EcomBlogPlugin.hasEcomBlog(new_useremail,shop,EcomBlogType.Shopify)){
			//play.Logger.log4j.info("An account with the email address "+new_useremail+" and shop ("+shop+") belongs to an existing account."); 
    		//boolean isEmail = true;    		 
    		show_new(shop,new_useremail,true,"An account with the email address "+new_useremail+" and shop name ("+shop+") belongs to an existing account.");	
		}
    	
    	Shop sh = null;     	
		try {
			
			if(shop!=null){
				ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, shop,t,timestamp,signature);

				sh = connect.getShop();
				if(sh!=null){ 
					
					EcomBlogPlugin plugin = new EcomBlogPlugin(new_useremail, sh.getMyshopifyDomain(), t, signature, EcomBlogType.Shopify);
					CompanyProfile profile = new CompanyProfile(sh.getShopOwner(), sh.getMyshopifyDomain(), sh.getTimezone(),sh.getPlanName(), sh.getCountry(),(phoneNumber!=null && !phoneNumber.trim().isEmpty() ?phoneNumber:sh.getPhone()));
 
					if(Company.isDomainAvailable(plugin.getDomain())){
						show_new(shop,new_useremail,true,"Oops, your shop name "+plugin.getEb_name()+" is associated with a Dropifi account.");
					}
					
					//create a new account for the shop
					Company company = new Company(plugin, userpassword,profile); 
					company = company.createCompany();				
					if(company!=null && company.getId()!=null){
							
						//add the widget js file to the shop's script tag 	
			    		company.getEcomBlogPlugin().addShopifyScriptTag(timestamp);	 
			    					    		
			    		try{
			    			//send an initial message to the user			    			 
							company.sendWelcomeMsg(EcomBlogType.Shopify); 
							 
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						}
			    		
			    		try{
							WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),sh.getMyshopifyDomain(),sh.getMyshopifyDomain());
							ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
							trackActor.tell(track);  
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						}
			    		
			    		try{   					
							//send an email to the shop owner
				    	    MailSerializer mail = new MailSerializer(company.getProfile().getName(), company.getEmail(),"", "Welcome to Dropifi",company.getShopifySignUpMsg());								
							ActorRef mailActor = actorOf(MailingActor.class).start();   
							mailActor.tell(mail); 
							
			    	    }catch(Exception e){
			    	    	play.Logger.log4j.error(e.getMessage(), e);
			    	    } 
			    		
			    		Shopify.removeFromSession();	
			    		session.clear(); 			    		
			    		Security.setLoginSession(company,"","SIGNUP",location);  

			    		//redirect the owner to the widget customization page
						redirect("/"+company.getDomain()+"/admin/widgets");
					}else{					 
						show_new(shop,new_useremail,true,"Oops, the account can not be created.");
					}
				}else{
					login(shop,t,timestamp,signature);
				} 
			
			}else{
				//user login credentials not found. repeat shop authentication
				login(shop, t, timestamp, signature);
			}
			
		}catch (Exception e) {
			//TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(),e);
			flash.put("error", "An error occurred while installing dropifi contact widget for your store. Try installing it again");			
		}finally{			 
			//boolean isEmail = new_useremail!=""?Company.isEmailAvailable(new_useremail):false;			 
			show_new(shop,new_useremail,true,"");
		}
    }
    
    public static void createExistingAccount(@Required String user_email, @Required String password,String location){
    	 
    	String shop      = session.get("shopify_shop"),
	    	   t         = session.get("shopify_t"),
	    	   timestamp = session.get("shopify_timestamp"),
	    	   signature = session.get("shopify_signature");     	
    	
    	boolean isEmail = false;
    	flash.put("user_email", user_email);
    	
    	if(validation.hasErrors()){    		
    		flash.put("error", "user email address and password required");    		
    	}if(!validation.email(user_email).ok){
    		flash.put("error", "user email address required");
    	}else{
    		
	    	//find the user information
	    	 Map<String,Object> userdata = Company.findByEmailPassword(user_email,UserType.Admin);
	    	    	 
	    	if(userdata == null){
	    		flash.put("error", "Oops, there is no account associated with this email");
	    		isEmail = true;
	    	}else{	    		
	    		Object ph = userdata.get("password");	
	    		String pass = ph!=null?(String)ph:null;
	    		
	    		String hPassword = Codec.hexMD5(password);
	    		
	    		if(!pass.equals(hPassword) || pass==null){ 
	    			flash.put("error", "Oops, the password you entered is not correct");
		    		isEmail = true;
		    	}else{		    			
		    		Company company = (Company) userdata.get("company");
		    		
		    		try{
		    			
		    			if(shop!=null){
				    		ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, shop,t,timestamp,signature);
						    Shop sh = connect.getShop();
						    
							if(sh != null){ 	
								EcomBlogType pluginType = company.getEcomBlogPlugin().getEb_pluginType();

							 	EcomBlogPlugin plugin = new EcomBlogPlugin(company.getEmail(), sh.getMyshopifyDomain(), t, signature, EcomBlogType.Shopify);
								CompanyProfile profile = new CompanyProfile(sh.getShopOwner(), sh.getMyshopifyDomain(), sh.getTimezone(),sh.getPlanName(), sh.getCountry());
														
								try{ 
									
					    			//reset the EcomBlogPlugin
									company.setEcomBlogPlugin(plugin);									 
									company.setProfile(profile);
									//company.setDomain(plugin.getDomain()); 
									company.save();
									 
					    			//add the javascript file
						    		company.getEcomBlogPlugin().addShopifyScriptTag(timestamp);	
						    		
						    		try{
										WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),sh.getMyshopifyDomain(),sh.getMyshopifyDomain());
										ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
										trackActor.tell(track);  
									}catch(Exception e){
										play.Logger.log4j.error(e.getMessage(), e);
									} 
						    		
						    		//track user signup
							       /* try{
							    	    EventTrackSerializer event = new EventTrackSerializer(company.getProfile().getName(), company.getEmail(),"SIGNUP","", company.getEmail(),UserType.Admin.name(),true);
							    	    event.setWidgetType(EcomBlogType.Shopify.name());
							    	    event.setCreated(new Date(company.getCreated().getTime()).toString());
							    	    event.location = location;
										ActorRef eventActor = actorOf(EventTrackActor.class).start();   
										eventActor.tell(event); 
						    	    }catch(Exception e){ 
						    	    	play.Logger.log4j.info(e.getMessage(),e); 
						    	    } 
						    		*/
						    		
						    		Shopify.removeFromSession();						    		
						    		session.clear();
						    		
						    		company.loginSubscription();
						    		Security.setLoginSession(company,"","LOGIN",location);
						    		
						    		PrestaShop.revertToFree(company.getApikey(),pluginType,  plugin.getEb_pluginType());
						    		
						    		//redirect the owner to the widget customization page
									redirect("/"+company.getDomain()+"/admin/widgets"); 
		
					    		}catch(Exception e){    			
					    			//unable to create the account 			    			
					    			flash.put("error" , "An error occurred while adding dropifi contact widget to your store. Try creating the account again");     			
					    		} 	
								
							}else{		
								//play.Logger.log4j.error(sh + "1. getting the shop's information failed. repeat shop authentication");
								//getting the shop's information failed. repeat shop authentication
								login(shop,t,timestamp,signature);
							}
		    			}else{
		    				//play.Logger.log4j.error("2. getting the shop's information failed. repeat shop authentication");
		    				//getting the shop's information failed. repeat shop authentication
		    				login(shop,t,timestamp,signature);
		    			}
		    		}catch(Exception e){
		    			play.Logger.log4j.error(e.getMessage(),e);
		    		}
	    		}
	    	}    	
    	}
    	
    	flash.put("existing_account", true);
		flash.keep();		
		index(shop,user_email,isEmail);
    	
    }
    
    @Util
    protected static void addToSession(String shop,String t,String timestamp, String signature){
    	session.put("shopify_shop", shop);
    	session.put("shopify_t", t);
    	session.put("shopify_timestamp", timestamp);
    	session.put("shopify_signature", signature);
    }
    
    @Util
    protected static void removeFromSession(){
    	session.remove("shopify_shop");
    	session.remove("shopify_t");
    	session.remove("shopify_timestamp");
    	session.remove("shopify_signature");
    }
}
