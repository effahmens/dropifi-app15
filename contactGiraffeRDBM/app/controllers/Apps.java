package controllers;

import models.Company;
import models.notification.EmailNotification;
import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.With;

@With(Compress.class)
public class Apps extends Controller{
	
	public static void domain_not_found(String domain){
		renderArgs.put("page", "");
		render(domain);
	}
	
	public static void multiple_login(String domain){
		renderArgs.put("page", "");
		render(domain);
	}
	
	public static void page_not_found(String content){
		renderArgs.put("page", "");
		render(content);
	}
	
	public static void popout_not_found(String content){
		renderArgs.put("page", "");
		renderHtml(content);
	}
	
	public static void unsubscribe_notifications(@Required String notifyKey){
		renderArgs.put("page", "");
		if(validation.hasErrors()){ page_not_found(""); } 
		
		boolean hasUnsubscribed = EmailNotification.unSubscribeToNotification(notifyKey,false);
		render(notifyKey,hasUnsubscribed);
	}
	
	public static void subscribe_notifications(@Required String notifyKey){
		renderArgs.put("page", "");
		if(validation.hasErrors()){ page_not_found(""); }
		
		boolean hasUnsubscribed = EmailNotification.unSubscribeToNotification(notifyKey,true);
		render(notifyKey,hasUnsubscribed);
	}
}
