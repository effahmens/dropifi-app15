package controllers;

import static akka.actor.Actors.actorOf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

import job.process.EventTrackActor;
import job.process.MailingActor;
import job.process.WidgetTrackerActor;
import models.Company;
import models.CompanyProfile;
import models.EcomBlogPlugin;
import models.WidgetDefault;
import akka.actor.ActorRef;

import com.google.gson.JsonObject;

import flexjson.JSONSerializer;

import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.DCON;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import resources.WidgetTrackerSerializer;
import view_serializers.EcomParameter;
import view_serializers.MailSerializer;
@With(Compress.class)
public class Magento extends Controller{
	public static void index(){
		render(); 
	}
	
	public static void signup(@Required String displayName,@Required String user_email,	@Required String user_password,
			@Required String user_re_password,@Required String user_domain,@Required String hostUrl, @Required String requestUrl,
			@Required String accessToken,String site_url,String type, String s,String location,String phoneNumber){
		
		JsonObject json = new JsonObject();
		int status = 501;
		String msg= ""; 
		EcomParameter ecomParam = new EcomParameter();
		if(validation.hasErrors()){
			msg= "Some of the required fields are not specified. All the fields are required";
		}else if(!user_password.equalsIgnoreCase(user_re_password)){
			status= 406; msg = "Oops, password and re-passoword do not match"; 
		}else{
		
		//check if the user is installing the magento plugin from a local machine
		//boolean isLocal = DropifiTools.isFromLocalWP(requestUrl);
		
		//String foundToken = !isLocal? DropifiTools.wordpressAuth(requestUrl):"";
		//if(foundToken.compareTo(accessToken)==0)
		
			if(accessToken!=null){						
				user_domain = user_domain.trim().replace(" ", "").toLowerCase(); 
				//check if the email address is not associated with any account
				user_domain = Company.replaceSpecialChar(user_domain);
				if(!Company.isValidDomain(user_domain)){
					status=308; msg="Oops, your company name should not contain special characters such as !@#$%^&*()+=-[]\\\';,./{}|\":<>?~_ and spaces. Only letters and numbers are allowed. Eg retailtower"; 
				}else if(Company.isEmailAvailable(user_email)){
					status=305; msg="Oops, the email  address "+user_email+" belongs to an existing account"; 
				}else if(EcomBlogPlugin.hasEcomBlog(user_email,hostUrl,EcomBlogType.Magento)){
					status =307; msg = "Oops, the email  address "+user_email+" belongs to an existing account";  
				}else if(Company.isDomainAvailable(user_domain)|| Company.usedDomains().contains(user_domain)){
					status =309; msg ="Oops, the company name "+user_domain+" belongs to an existing account";
				}else{
				
					//use the credentials to create an account for the user
					EcomBlogPlugin plugin = new EcomBlogPlugin(user_email,hostUrl, accessToken, accessToken, EcomBlogType.Magento);
					CompanyProfile profile = new CompanyProfile(displayName, hostUrl, "","", "",phoneNumber);
					
					//create a new account for the shop
					Company company = new Company(plugin,user_domain,user_re_password,profile);
					company = company.createCompany();
					
					if(company!=null && company.getId()!=null){
						status =200;
						
						try{
			    			//send an initial message to the user				    			 
							company.sendWelcomeMsg(EcomBlogType.Magento); 							 
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						} 
						
						try{
							WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),site_url,site_url);
							ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
							trackActor.tell(track);  
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e); 
						}
						
						try{   					
							//send an email to the shop owner
				    	    MailSerializer mail = new MailSerializer(company.getProfile().getName(), company.getEmail(),"", "Welcome to Dropifi",company.getShopifySignUpMsg());								
							ActorRef mailActor = actorOf(MailingActor.class).start();
							mailActor.tell(mail);  
							
			    	    }catch(Exception e){
			    	    	play.Logger.log4j.error(e.getMessage(), e);
			    	    } 
						
						//add the public key to the json object to be returned to magento
						String publicKey = WidgetDefault.getPublicApikey(company.getApikey());
						msg="Installation of Dropifi Magento plugin on your website completed successfully";
						json.addProperty("publicKey", publicKey);
						json.addProperty("userEmail", user_email);
						
						//generate a temporarily login token							
						String temToken = DropifiTools.generateCode(12);
						company.getEcomBlogPlugin().setEb_secretKey(temToken);
						company.save(); 
						
						if(type!=null && type.equalsIgnoreCase("json")){ 
							ecomParam.setPublicKey(publicKey);
							ecomParam.setUserEmail(user_email);
							ecomParam.setTemToken(temToken);
						}
						
						//track user signup
				        try{
				    	    EventTrackSerializer event = new EventTrackSerializer(displayName,user_email,"SIGNUP","",user_email,UserType.Admin.name(),true);
				    	    event.setWidgetType(EcomBlogType.Magento.name());
				    	    event.setCreated(new Date(company.getCreated().getTime()).toString());
				    	    event.setUrl(company.getProfile().getUrl());
				    	    event.location=location;
				    	    event.setPhone(phoneNumber);
							ActorRef eventActor = actorOf(EventTrackActor.class).start();   
							eventActor.tell(event); 
			    	    }catch(Exception e){
			    	    	
			    	    } 
						
						json.addProperty("temToken", temToken);
					
					}else{
						status = 203;
						msg="Your account could not be created. The email address or company name you specified may not be valid.";
					} 					
				}				
			}else{
				//play.Logger.log4j.info("Authentication of the magento plugin failed. Ensure that you have the correct version of the plugin");		
				msg="Authentication of the magento plugin failed. Ensure that you have the correct version of the plugin";
			}			 
		} 
		
		if(type!=null && type.equalsIgnoreCase("json")){
			JSONSerializer fjson = new JSONSerializer();
			 
			ecomParam.setStatus(status);
			ecomParam.setMsg(msg);				 	
			renderJSON(s + '(' + fjson.serialize(ecomParam)+ ')');				 
		}else{
			returnResponse(json, status, msg);
		}
	}
	
	public static int updateEbSecretKey(String apikey,String eb_secretKey){
		Connection conn=null;
		try{
			conn = DCON.getDefaultConnection();
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Update Company c Set eb_secretKey = ?, eb_pluginType = ? Where apikey = ?"); 
				query.setString(1, eb_secretKey); 
				query.setInt(2, EcomBlogType.Wordpress.ordinal());
				query.setString(3, apikey); 
				return query.executeUpdate()>0?200:201;	
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(), e);
		}finally{
			try {
				if(conn!=null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(), e);
			}
		}
		return 505; 
	}
		
	public static void login(@Required String temToken,@Required String userEmail,String IPAddress,String location){ 
		 
		if(!validation.hasErrors()){
			 
			 //find the login credentials of the user
			try{
			Map<String,String> credentials = Company.authenticate(userEmail, temToken,EcomBlogType.Magento);
			 if(credentials != null){
				String credPassword = (String)credentials.get("hashPassword"); 
					
				if(credPassword!=null && temToken!=null && temToken.compareTo(credPassword) == 0){
					 
					Company.updateEbSecretKey(credentials.get("apikey"), "",EcomBlogType.Magento);  
					credentials.put("userEmail", userEmail); 
					credentials.put("IPAddress", IPAddress!=null?IPAddress:""); 
					credentials.put("location", location!=null?location:""); 
					Security.setLoginSession(credentials); 
					
					//redirect the owner to the widget customization page
					redirect("/"+credentials.get("domain")+"/admin/widgets"); 
				}  					 			 
			 }else{
				 play.Logger.log4j.info("C. Testing some few things"); 
			 } 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		try {
			session.clear();  
			Secure.login("");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}	 
		
	}
	
	public static void loginToken(@Required String login_email,@Required String accessKey,@Required String requestUrl,@Required String accessToken,String site_url,String type, String s){
		JsonObject json = new JsonObject(); 
		String msg="Required fields are not specified";
		int status = 501;
		
		EcomParameter ecomParam = new EcomParameter();
		if(validation.hasErrors()){
			 
			if(type!=null && type.equalsIgnoreCase("json")){ 
				JSONSerializer fjson = new JSONSerializer();
		 
				ecomParam.setStatus(status);
				ecomParam.setMsg(msg);				 	
				renderJSON(s + '(' + fjson.serialize(ecomParam)+ ')');			 
			}else{
				returnResponse(json, 501, "Required fields are not specified"); 
			}
			
		}
		
		//check if the user is installing the magento plugin from a local machine
		//boolean isLocal = DropifiTools.isFromLocalWP(requestUrl);
		
		//String foundToken = !isLocal ? DropifiTools.wordpressAuth(requestUrl):"";

		if(accessToken!=null){				
			if(type!=null && type.equalsIgnoreCase("json"))
				accessKey = Codec.hexMD5(accessKey);
			
			Map<String,String> credential = Company.authenticateBlog(login_email, accessKey); 			
			
			if(credential !=null ){ 
				//create a temp. access token which will be sent to magento for accessing the user data
				String temToken = DropifiTools.generateCode(12); 
				String apikey=credential.get("apikey"); 
				
				if(apikey!=null && Company.updateEbSecretKey(apikey, temToken,EcomBlogType.Magento)==200){ 
					json.addProperty("temToken", temToken);
					json.addProperty("publicKey", credential.get("publicKey")); 
					json.addProperty("userEmail", login_email);
					
					if(type!=null && type.equalsIgnoreCase("json")){
						ecomParam.setPublicKey(credential.get("publicKey"));
						ecomParam.setUserEmail(login_email);
						ecomParam.setTemToken(temToken);
					}
					
					status=200;
					msg = "Installation of Dropifi Conact Widget extension on your website completed successfully"; 
					 
					try{
						//play.Logger.log4j.info(site_url);
					    WidgetTrackerSerializer track = new WidgetTrackerSerializer(apikey,site_url,site_url); 
						ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
						trackActor.tell(track);
					}catch(Exception e){
						play.Logger.log4j.error(e.getMessage(), e);
					}
				}			
			}
			
			if(status!=200)
				msg="The email address or password you entered are not correct";
			
		}else{ 
			play.Logger.log4j.info("Authentication of the magento plugin failed. Ensure that you have the correct version of the plugin");		
			msg="Authentication of the magento plugin failed. Ensure that you have the correct version of the plugin";
		}
		
		
		if(type!=null && type.equalsIgnoreCase("json")){
			JSONSerializer fjson = new JSONSerializer();
			//fjson.serialize(widget)
			ecomParam.setStatus(status);
			ecomParam.setMsg(msg);
			renderJSON(s + '(' +  fjson.serialize(ecomParam) + ')');			 
		}else{
			returnResponse(json, status, msg);
		}
	}
	
	/**
	 * render a json object as a response to a request with default params {status:200,msg:'message'}
	 * @param json
	 * @param status
	 * @param msg
	 */
	public static void returnResponse(JsonObject json,int status,String msg){
		json.addProperty("status", status); 
		json.addProperty("msg", msg);		
		renderJSON(json.toString()); 
	} 
}
