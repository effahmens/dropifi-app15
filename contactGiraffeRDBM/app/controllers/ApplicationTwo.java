package controllers;

import java.util.List;
import java.util.Map;
import java.util.Set; 

import models.GeoLocation;
import models.dsubscription.SubPlan;
import models.dsubscription.SubService;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import resources.DropifiTools;
import resources.addons.SocialReviewClient;

@With(Compress.class)
public class ApplicationTwo extends Controller{

	@Before
	public static void onload(){
		ForceSSL.secureDevMode();
	}
	 
    public static void index(){  
    	renderArgs.put("page","index");  
    	render();
    }
    
    public static void features(){   
    	renderArgs.put("page","features"); 
    	render();
    }
    
    public static void pricing(){ 
    	renderArgs.put("page","pricing");
    	render();
    }
    
    public static void company(){
    	renderArgs.put("page","company"); 
    	render(); 	  	
    }	
    
    public static void social_review(){   
    	renderArgs.put("page","social_review"); 
    	render();
    }
    
    public static void integrations(){   
    	renderArgs.put("page","instegrations"); 
    	render();
    }
    
    public static void privacy_policy(){   
    	renderArgs.put("page","privacy_policy"); 
    	render();
    }
    
    public static void legal_terms(){   
    	renderArgs.put("page","legal_terms"); 
    	render();
    }
    
    public static void newsroom(){   
    	renderArgs.put("page","newsroom"); 
    	render();
    }
    
}
