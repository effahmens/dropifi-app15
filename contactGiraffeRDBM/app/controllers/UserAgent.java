package controllers;

import gmailoauth2grant.DGOauthGrant;

import java.util.List;

import models.Channel;
import models.Company;
import models.InboundMailbox;
import models.gmailauth.GmailCredentials;
import adminModels.MailboxConfig;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import resources.UserType;
import view_serializers.MbSerializer;
@With(Compress.class)
public class UserAgent  extends Controller{
	
	@Before
    static void setArgs(String domain){
		String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
 			/*UserType userTypes = Security.connectedUserType();
				if(UserType.isAgent(userTypes) ){
					//Dashboard.index(domain);
				}  
 			*/
 			
 			renderArgs.put("page","admin");
		     renderArgs.put("sub_page","mail"); 
		     renderArgs.put("title","Admin - Mail Management (Outgoing Mailboxes)");  			 		     
 		}else{ 			
 			
 			//clear the session
 	    	session.clear(); 
 	    	//response.removeCookie("rememberme");			
 			try {
 				Secure.login("");
 			} catch (Throwable e) {
 				Application.index();
 			}
 		}
			
      
    } 
	
	//displays an editable details  of an inbound mailbox
	public static void show(@Required String email, @Required String domain){  		
		ForceSSL.redirectToHttps();		
		String connected = Security.connectedDomain();			
		String apikey = Security.connectedApikey();
		
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){				
			List<String> mailservers = MailboxConfig.findAllMailserver();
			String btnsave =""; 
			InboundMailbox mailbox =null;
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			String gmailUrl = DGOauthGrant.authorization_url();
			
			if(validation.required(email).ok){ 
				//check if an email is specified
									
				if(email.equals("new")){
					 btnsave ="Save"; 
				}else if (validation.email(email).ok){
					//search for the connected company mailbox						 
					Channel channel = Channel.findByApikey(apikey);				
					mailbox = channel == null ? null:channel.findInboundMb(email);					
					btnsave ="Update"; 					
					if(mailbox.getMailserver().equals("Gmail")){
						GmailCredentials credential = GmailCredentials.retrieveCredential(apikey, email);
						if(credential!=null)
							gmailUrl =null;
					}
				} 			
			}else{ 
				email="new";
				btnsave ="Save"; 			 
			}
			
			render(btnsave, email,mailservers,domain,mailbox,connectedUsername,ecomBlogPlugin,userType,gmailUrl); 
		}
		
		Apps.popout_not_found("");
		 
	}
		
}
