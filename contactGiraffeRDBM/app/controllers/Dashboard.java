package controllers;
import models.Company;
import models.Widget;
import models.WidgetDefault;
import models.WidgetTracker;
import models.dsubscription.SubServiceName;
import models.notification.EmailNotification;
import play.Logger;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.AccountType;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.UserType;
import resources.helper.ServiceSerializer;
@With(Compress.class)
public class Dashboard extends Controller{
	
	@Before
	static void setArgs(){		
		String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
				renderArgs.put("page","admin");							 		     
 		}else{ 			
 			
 			//clear the session
 	    	session.clear(); 
 	    	//response.removeCookie("rememberme");			
 			try {
 				Secure.login("");
 			} catch (Throwable e) {
 				Application.index();
 			} 
 		}
	} 
	 	
	public static void index(String domain){   
		ForceSSL.redirectToHttps();
		
		renderArgs.put("title","Admin - Dashboard"); 
		renderArgs.put("sub_page","dashboard");
		String apikey = Security.connectedApikey();	 
		
		if(apikey !=null) {			
			//check if domain is a valid 		
			String connected = Security.connectedDomain(); 			
			domain =domain!=null? domain.trim():""; 			
			if(!connected.equalsIgnoreCase(domain)){			
				index(connected);
			}
			
			String connectedUsername = Security.connected();
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			
			//check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.getValue(ecomBlogPlugin));
			ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
			
			EmailNotification eNotify = EmailNotification.findByApikey(apikey);
			double mprice = DropifiTools.MINIMUMPRICE;
			render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget,service,eNotify,mprice);
		 			 
		} 
		
	} 
	
	public static void welcome( String domain,int status,String continueUrl){   
		ForceSSL.redirectToHttps();
		
		renderArgs.put("title","Admin - Widget Integration");
		renderArgs.put("sub_page","integration");
		UserType userTypes = Security.connectedUserType();
		if(UserType.isAgent(userTypes) ){
			redirect("/"+domain+"/dashboard/index");
			//Dashboard.index(domain); 
		} 
		
		String apikey = Security.connectedApikey();		
		if(apikey !=null) {
			//check if domain is a valid 		
			String connected = Security.connectedDomain(); 	  			
			domain =domain!=null? domain.trim():""; 			
			if(!connected.equalsIgnoreCase(domain)){			
				welcome(connected,status,continueUrl);
			}	
			
			String connectedUsername = Security.connected();
			String userType = userTypes.name();
			String ecomBlogPlugin = Security.connectedPlugin();
			
			//search for the user plugin code
			WidgetDefault wid = WidgetDefault.findByApiKey(apikey); 
			String publickey = wid.getPublicKey();
			
			Widget widget = new Widget();
			String plugin	= play.Play.mode.isDev()?widget.generateAsyncSnippetDev(publickey,ecomBlogPlugin):widget.generateAsyncSnippet(publickey,ecomBlogPlugin); 
			boolean hasWidget =true;
			
			boolean newAccount = Boolean.valueOf(session.get("newAccount")); 
			if(newAccount)
				session.remove("newAccount");  
			
			String cookieKey = CacheKey.genCookiekey(CacheKey.Secure,domain, "Plan");
			String plan = Security.getCookie(cookieKey,apikey);
			
			Security.removeCookie(cookieKey);
			plan=plan!=null?plan:AccountType.Free.name();
			 
			String message = params.get("message");
			render(userType,domain,connectedUsername,ecomBlogPlugin,plugin,message,status,hasWidget,continueUrl,publickey,newAccount,plan); 				
			/*}else{
				//check if the domain is available 
				boolean hasDomain = Company.isDomainAvailable(domain);
				if(hasDomain){	//show domain not found page
					Apps.multiple_login(connected);
				}else{
					Apps.domain_not_found(domain); 
				}
			}*/				 
		}
		 
    }
	
	public static void notification(String domain,int status,String continueUrl){   
		ForceSSL.redirectToHttps();
		renderArgs.put("title","Admin - Account Settings");
		renderArgs.put("sub_page","login_notification");
		UserType userTypes = Security.connectedUserType();
		if(UserType.isAgent(userTypes) ){
			redirect("/"+domain+"/dashboard/index");
		} 
		
		String apikey = Security.connectedApikey();		
		if(apikey !=null) {	
			//check if domain is a valid 		
			String connected = Security.connectedDomain(); 			
			domain =domain!=null? domain.trim():""; 			
			if(!connected.equalsIgnoreCase(domain)){			
				notification(connected,status,continueUrl);
			}
			
			render(); 
		}
	}

	public static void confirmWidget(){ 
		String apikey = Security.connectedApikey();	
		String domain =Security.connectedDomain();
		String continueUrl ="#";
		int status =501;
		String message=null;
		
		UserType userTypes = Security.connectedUserType();
		if(UserType.isAgent(userTypes) ){
			redirect("/"+domain+"/dashboard/index");
		}
		
		if(apikey !=null){
			 
			WidgetTracker track = WidgetTracker.findByApikey(apikey);
			if(track!=null && track.hasWidgetInstalled(EcomBlogType.valueOf(Security.connectedPlugin()))){  
				continueUrl = "/"+domain+"/admin/widgets/index/"; 
				message= "You have successfully installed the Dropifi contact widget. Click continue to customize the look and feel of your widget.";
				status=200;
			}else{
				message="The Dropifi contact widget is not installed on your site. Follow the instructions below to add our javascript to your website";
				status =404;
			}	 
		}else{
			message = "This account does not exist.";
		}		
		
		showIntegrationPage(apikey, status, domain, continueUrl, message, null);
	}
	
	public static void manage_account(String domain) {
	ForceSSL.redirectToHttps();
		
		renderArgs.put("title","Admin - Manage Account"); 
		renderArgs.put("sub_page","manage_account");
		String apikey = Security.connectedApikey();	 
		
		if(apikey !=null) {			
			//check if domain is a valid 		
			String connected = Security.connectedDomain(); 			
			domain =domain!=null? domain.trim():""; 			
			if(!connected.equalsIgnoreCase(domain)){			
				index(connected);
			}
			
			String connectedUsername = Security.connected();
			String userType = Security.connectedUserType().name();
			String ecomBlogPlugin = Security.connectedPlugin();
			
			//check if the widget is installed on the site of the user
			boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(),EcomBlogType.getValue(ecomBlogPlugin));
			ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
			 
			render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget,service);
		 			 
		} 
		
	}
	
	@Util
	public static void showIntegrationPage(String apikey,int status,String domain,String continueUrl,String message,String plan){
		renderArgs.put("title","Admin - Dashboard"); 
		renderArgs.put("sub_page","dashboard");
		
		String connectedUsername = Security.connected();
		String userType = Security.connectedUserType().name();
		String ecomBlogPlugin = Security.connectedPlugin();
		
		//check if the widget is installed on the site of the user
		boolean hasWidget = true;
		ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
		
		//search for the user publickey 
		String publickey = WidgetDefault.getPublicApikey(apikey); 
		
		Widget widget = new Widget();
		//String plugin	= play.Play.mode.isDev()?widget.generatePluginDev(publickey):widget.generatePlugin(publickey); //
		String plugin	= play.Play.mode.isDev()?widget.generateAsyncSnippetDev(publickey, "Dropifi"):widget.generateAsyncSnippet(publickey, "Dropifi");
		plan=plan!=null?plan:service.getAccountType();
		 
		renderTemplate("Dashboard/welcome.html",domain,status,continueUrl, message,userType,connectedUsername,userType,hasWidget,service,plugin,ecomBlogPlugin,plan,publickey); 
	}
	
}
