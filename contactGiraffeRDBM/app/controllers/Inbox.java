package controllers;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import net.sf.oval.constraint.Email;
import models.AccountUser;
import models.Company;
import models.Customer;
import models.DropifiMessage;
import models.DropifiResponse;
import models.InboundMailbox;
import models.MessageManager;
import models.MsgCounter;
import models.QuickResponse;
import models.WidgetTracker;
import models.dsubscription.SubServiceName;
import adminModels.DropifiMailbox;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject; 

import flexjson.JSONSerializer;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import query.inbox.Contact_Serializer;
import query.inbox.InboxSelector;
import query.inbox.MessageDetailSerializer;
import resources.CacheKey;
import resources.EcomBlogType;
import resources.MsgStatus;
import resources.UserType;   
import resources.helper.ServiceSerializer;
import view_serializers.SubjectSerializer;

@With(Compress.class)
public class Inbox extends Controller{
  @Util //@Before 
  static void setArgs(){
  	String apikey = Security.connectedApikey();
	String foundApikey = Company.findApikey(apikey);
	if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
		 renderArgs.put("page","agent");
		 renderArgs.put("title","Agent - Inbox");
	}else{ 
		//clear the session
    	session.clear();
    	//response.removeCookie("rememberme"); 
		try {
			Secure.login(""); 
		} catch (Throwable e) { 
			Application.index();
		}
	}
  }
 
  public static void index(String domain,String requestType, String scrollToId) { 
	ForceSSL.redirectToHttps();
	setArgs();
	
	String apikey = Security.connectedApikey();	
	
 	if(!validation.hasErrors() && apikey !=null){	
 		String connected = Security.connectedDomain();
 		domain =domain!=null? domain.trim():"";
 		if(!connected.equalsIgnoreCase(domain)){			
			index(connected,requestType,scrollToId);
		}
		
			 String connectedUsername = Security.connected();
			 String userType = Security.connectedUserType().name();
			 String ecomBlogPlugin = Security.connectedPlugin();
			 //check if the widget is installed on the site of the user
			 boolean hasWidget = WidgetTracker.hasWidgetMailboxInstalled(apikey,Security.connectedCompanyId(), EcomBlogType.getValue(ecomBlogPlugin));
			
			 List<SubjectSerializer> subjects = MessageManager.listOfMsgSubjectsSer(apikey);
			 ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget, SubServiceName.Branding);
			 render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget,requestType,subjects,service,scrollToId);
		/*}else{
			//check if the domain is available  
			boolean hasDomain = Company.isDomainAvailable(domain);
			if(hasDomain){
				//show domain not found page
				Apps.multiple_login(connected); 
			}else{
				Apps.domain_not_found(domain);
			}
		}*/
	} 
 	Apps.page_not_found("");
  }
  
  public static void compose(@Required String domain,Boolean isDraft,boolean isSent,String type,String msgId,Long messageId,String email,String subject ,String message,Long respId){
	ForceSSL.redirectToHttps();
	setArgs();
	  
	String connected = Security.connectedDomain(); 			
	domain =domain!=null? domain.trim():""; 			
	if(!connected.equalsIgnoreCase(domain)){			
		domain =connected;
	}
	String apikey = Security.connectedApikey(); 
	
	if(apikey!=null){				
		
		  String femail = email;
	  
		  UserType userTypes = Security.connectedUserType();
		  String userEmail = Security.connected();
		  String connectedUsername = Security.connected();
		  String userType = Security.connectedUserType().name();
		  String ecomBlogPlugin = Security.connectedPlugin();
		 
		  List<String> quickResponses = MessageManager.listOfQuickResponseTitles(apikey);
		  List<String> mailboxes =  AccountUser.getComposeMailboxes(apikey, userEmail, userTypes); 		
		  	 
		  //check the type of compose 
		  if(type !=null) {
			  
			  subject.trim();
			  String sub =subject.toUpperCase();
			  if(type.equalsIgnoreCase("reply")){ 
				  if(sub.startsWith("FWD:")){
					  sub = sub.replaceAll("FWD:", "");
					  subject = subject.replaceAll("FWD:", "");
				  }
				  subject = sub.startsWith("RE:")? subject:"RE: " + subject; 		   
			  }else if(type.equalsIgnoreCase("inbox_forward") ){ 
				  if(sub.startsWith("RE:")){
					  sub = sub.replaceAll("RE:", "");
					  subject = subject.replaceAll("FWD:", "");
				  }
				  subject =sub.startsWith("FWD:")?subject: "FWD: " + subject; 
				  email = "";  
			  }else if(type.equalsIgnoreCase("sent_forward") ){ 
				  //DropifiMessage.retrieveSubjectMessage(messageId, email, msgId);
				  if(sub.startsWith("RE:")){
					  sub = sub.replaceAll("RE:", "");
					  subject = subject.replaceAll("FWD:", "");
				  }
				  subject = sub.startsWith("FWD:")?subject: "FWD: " + subject; 	
				  email = " "; 
			  }
		  } 
		  
		  String fmb = "new";
		  if(mailboxes.size()>0)
			   fmb = mailboxes.get(0);
		  else
			  mailboxes =null;
		 String host =request.host; 
		  
		 messageId = messageId==null?0l:messageId;
	     render(mailboxes,quickResponses,type,subject,message,email,femail,messageId,msgId,host,domain, fmb,isDraft,respId,connectedUsername,userType,ecomBlogPlugin);	   
	}
	Apps.popout_not_found("");
  } 
  
  public static void mailbox_response_template(){
	   String apikey = Security.connectedApikey();
	   UserType userType = Security.connectedUserType();
	   String userEmail = Security.connected();
 
	   JsonObject json = new JsonObject();
	   Gson gson = new Gson();
	   if(apikey!=null){
		   json.add("mailboxes", gson.toJsonTree(AccountUser.getComposeMailboxes(apikey, userEmail, userType)));
		   json.add("quickResponses", gson.toJsonTree(MessageManager.listOfQuickResponseTitles(apikey)));
	   }
	   renderJSON(json.toString());
   }
  
  public static void fetch_foward_message(@Required String type, String msgId , Long id, @Required String email,Boolean isDraft,Long respId){
	  JsonObject json = new JsonObject();
	  int status = 403;
	  if(!validation.hasErrors()){	
		   String apikey = Security.connectedApikey();
		 
		   JsonObject msg =((type.equalsIgnoreCase("reply") || type.equalsIgnoreCase("inbox_forward")) &&(isDraft==null))?DropifiMessage.retrieveSubjectMessage(id,msgId, email):DropifiResponse.retrieveSubjectMessage(apikey, respId, msgId, email); 
		   if(msg!=null){ 
			   json.add("content", msg);
			   String header ="";
			   //create the header of the message
			   if(isDraft==null ||isDraft==false){
				   if(type.equalsIgnoreCase("inbox_forward")){
					   header = "<br>----- Fowarded Message -----";
					   header+="<table><tr><td>From: </td><td>"+email+"</td></tr>";
					   header+="<tr><td>Subject: </td><td>"+msg.get("subject")+"</td></tr></table>"; 
					   header+="<br>";
				   }else if(type.equalsIgnoreCase("reply")){
					   header = "<p>On "+msg.get("created")+", &lt;"+email+"&gt; wrote: </p>";
				   }
			   }else{
				   header = "<p>On "+msg.get("created")+", <"+msg.get("userEmail")+">wrote: </p>"; 
			   }
			   
			   json.addProperty("header", header);
			   status = 200;  
		   }
	   }
	  json.addProperty("status", status); 
	  renderJSON(json.toString());
  }
   
  public static void fetch_quick_response(@Required String title){
	  JsonObject json = new JsonObject();
	  int status = 404;
	  if(!validation.hasErrors()){
		  String apikey = Security.connectedApikey();
		  if(apikey != null) {
			  Gson gson = new Gson();
			  MessageManager msg = MessageManager.findByApikey(apikey);		
			  QuickResponse quickResponse = (msg == null ? null : msg.findQRByTitle(title));
			  json.add("quickResponse", gson.toJsonTree(quickResponse));
			  status = 200;
		  }else{
			  json.addProperty("error", "no quick response available");
		  }
	  }else{
		  json.addProperty("error", "no quick response available");
	  }
	  
	  json.addProperty("status", status);
	  renderJSON(json.toString());
  } 
  
  public static void save_send_message(Long respId, @Required String composeType, @Required String sender, @Required String recipient,
		  String cc, String bcc, String subject, @Required String message,Long messageId,String msgId,String type, Boolean isSent) { 
	  
	  JsonObject json = new JsonObject(); 
	  int status = 404; 
	  
	  if(!validation.hasErrors()){ 
			String apikey = Security.connectedApikey();
		  	Long companyId = Security.connectedCompanyId(); //Company.findCompanyId(apikey);		
		 
		  if(apikey !=null && companyId != null){			
			  DropifiResponse response;
			  if((respId == null || respId <= 0) || ( (respId != null && respId > 0) && (isSent!=null && isSent==true)  )  ){ 
				  
				  //create a new response for saving the message to be sent
				  response = new DropifiResponse();
				  Timestamp time = new Timestamp(new Date().getTime());
				  response.setCreated(time);					 
				  response.setSent(false); 
				  response.setCompanyId(companyId);  
				  response.setApikey(apikey);
			  }else{
				  //search for the response from the list of created responses
				  response = DropifiResponse.findByApikeyAndId(apikey, respId);  
			  }
			  response.setSender(sender); 
			  if(type!=null && type.equalsIgnoreCase("reply")){
				  response.setMessageId(messageId); 			  
				  response.setMsgId(msgId);		 
			  }else{ 
				  response.setMessageId(0l);			  
				  response.setMsgId("");		 
			  }
			  
			  response.setRecipient(DropifiResponse.recipient(recipient));	 
			  response.setSubject(subject);
			  response.setMessage(message);
			  response.setCcAddress(DropifiResponse.recipient(cc));
			  response.setBccAddress(DropifiResponse.recipient(bcc)); 			   
			  
			  //set the current connected user
			  String userEmail = Security.connected();
			 
			  if(composeType.equalsIgnoreCase("save")){
				  response.setSent(false); 
				  DropifiResponse rep = response.saveResponse(userEmail); 
				  if(rep != null){
					  json.addProperty("respId", rep.getId()); 
					  status = 200;
				  }else{  
					  status = 201;
				  } 				  
			  }else if(composeType.equalsIgnoreCase("send")){
				 
				  DropifiResponse rep = response.saveResponse(userEmail);  				  
				  if(rep != null){
					  json.addProperty("respId", rep.getId());			
					  status = response.sendMessage(recipient,cc,bcc,null); 
					  
				  } else {
					  json.addProperty("error", "unable to send message");
					  status = 203;
				  }			  			  
			  }else if(composeType.equalsIgnoreCase("discard")){
				  if(respId != null && respId !=0){
					status = response.delete() !=null? 200:201;
				  }				  
			  } 
			  		  
			  if(status==200) InboxSelector.deleteAllCaches(apikey,companyId,false);
			  json.add("sentMessages",InboxSelector.countSents(apikey,companyId));
		  }else{
			  status = 405;
			  json.addProperty("error", "some required fields are not specified"); 
		  }
	  }else{
		  status = 406;
		  json.addProperty("error", "some required fields are not specified");
	  }
	 
	  json.addProperty("status", status);
	  renderJSON(json.toString());
  }
    
  public static void fetch_all_messages(int start, int end,Integer timezone){
	  //play.Logger.log4j.info("------------------------------------------------"); 
	  play.Logger.log4j.info("Request Start: "+ new Date().toString()); 
	  JsonObject json = new JsonObject();
	  int status = 405;
	  if(!validation.hasErrors()) {
		String apikey = Security.connectedApikey();
		Long companyId = Security.connectedCompanyId(); 
		String userEmail = Security.connected(); 
		UserType userType = Security.connectedUserType();
		
		if(companyId != null && userEmail!=null && apikey!=null && userType!=null){ 
			  json.add("messages", InboxSelector.findMessages(apikey,companyId, "", start, end,true,timezone,false)); 
			  json.add("count", InboxSelector.findCounters(apikey, companyId,false));  
			  json.addProperty("sources", "");  
			  status = 200; 
		  } else{ 			  
			  json.addProperty("error", "invalid account, login with the correct credentials");
		  } 
	  }else{
		  json.addProperty("error", "required fields are not specified");
	  }
	  
	  json.addProperty("status", status);
	  play.Logger.log4j.info("Request End: "+ new Date().toString()); 
	  renderJSON(json.toString());
  }
  
  public static void fetch_by_source(String source,@Required String mode,@Required boolean isPanel,String panel,@Required String typeOfPanel,int start, int end,@Required String inboxPanel,Integer timezone){ 
	  JsonObject json = new JsonObject();	
	  int status = 405;
	  
	  if(!validation.hasErrors()){
	    String apikey = Security.connectedApikey();
		Long companyId = Security.connectedCompanyId();
		String userEmail = Security.connected();
		 
		UserType userType = Security.connectedUserType();		
		
		if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
			String userMailboxes = "", mailbox = "all mailboxes";
			mode = mode.toLowerCase(); 
			inboxPanel =inboxPanel.trim();
			
			source = "all mailboxes";
			if(inboxPanel.equalsIgnoreCase("ref_unresolved")){
				json.add("messages", InboxSelector.findUnResolvedMessages(apikey,companyId,timezone,false)); 
				status = 200;
				json.addProperty("status", status);
				renderJSON(json.toString());
			} 
			play.Logger.log4j.info(" Subject: "+panel);
			if(mode.equals("all") && source.equals(mailbox)){ //condition 1 
				if(isPanel ==false || panel.equalsIgnoreCase("inbox")){				
					json.add("messages", InboxSelector.findMessages(apikey,companyId,userMailboxes,start, end,true,timezone,false));					
				}else{
					if(validation.required(panel).ok){
						if(typeOfPanel.equalsIgnoreCase("sentiment")){  
							json.add("messages", InboxSelector.findSentimentMessages(apikey,companyId,userMailboxes,panel,start, end,true,timezone,false));								
						}else if(typeOfPanel.equalsIgnoreCase("status")){
							json.add("messages", InboxSelector.findStatusMessages(apikey,companyId,userMailboxes,MsgStatus.typeOfStatus(panel),start, end,timezone,false));	
						}else if(typeOfPanel.equalsIgnoreCase("subjects")){
							json.add("messages", InboxSelector.findSubjectMessages(apikey,companyId, panel,timezone,false)); 
						}					
					}
				} 
			}else if(mode.equals("all") && !source.equals(mailbox)){ //condition 2	will not 
				if(isPanel ==false || panel.equalsIgnoreCase("inbox")){		
					json.add("messages", InboxSelector.findMessages(apikey,companyId, source,start, end,true,timezone,false));						
				}else{
					if(validation.required(panel).ok){
						if(typeOfPanel.equalsIgnoreCase("sentiment")){
							json.add("messages", InboxSelector.findSentimentMessages(apikey,companyId,panel,source,start, end,true,timezone,false));		
						}else if(typeOfPanel.equalsIgnoreCase("status")){
							json.add("messages", InboxSelector.findStatusMessages(apikey,companyId,userMailboxes,MsgStatus.typeOfStatus(panel),start, end,timezone,false));	
						}else if(typeOfPanel.equalsIgnoreCase("subjects")){
							json.add("messages", InboxSelector.findSubjectMessages(apikey,companyId, panel,timezone,false));							
						}					
					}
				} 
			}else if(!mode.equals("all") && source.equals(mailbox)){ //condition 3 
				if(mode.contains("true")|| mode.contains("false")){
					boolean isCustomer = Boolean.parseBoolean(mode);
					if(isPanel ==false || panel.equalsIgnoreCase("inbox")){								
						json.add("messages", InboxSelector.findMessages(apikey,companyId,userMailboxes, isCustomer,start, end,true,timezone,false));									
					}else{
						if(validation.required(panel).ok){
							if(typeOfPanel.equalsIgnoreCase("sentiment")){
								json.add("messages", InboxSelector.findSentimentMessages(apikey,companyId,userMailboxes,panel,isCustomer,start, end,true,timezone,false));
							}else if(typeOfPanel.equalsIgnoreCase("status")){
								json.add("messages", InboxSelector.findStatusMessages(apikey,companyId,userMailboxes,MsgStatus.typeOfStatus(panel),isCustomer,start, end,timezone,false));	
							}else if(typeOfPanel.equalsIgnoreCase("subjects")){
								json.add("messages", InboxSelector.findSubjectMessages(apikey,companyId, panel,isCustomer,timezone,false));								
							}			
						}
					}										 				
				}	 
			}else if(!mode.equals("all") && !source.equals(mailbox)){ //condition 4: not all customers and source of message - will not 
				if(mode.contains("true")|| mode.contains("false")){ 
					boolean isCustomer = Boolean.parseBoolean(mode);
					if(isPanel ==false || panel.equalsIgnoreCase("inbox")){								
						json.add("messages", InboxSelector.findMessages(apikey,companyId,source, isCustomer,start, end,true,timezone,false));							
					}else{
						if(validation.required(panel).ok){
							if(typeOfPanel.equalsIgnoreCase("sentiment")){
								json.add("messages", InboxSelector.findSentimentMessages(apikey,companyId,panel,source,isCustomer,start, end,true,timezone,false));
							}else if(typeOfPanel.equalsIgnoreCase("status")){
								json.add("messages", InboxSelector.findStatusMessages(apikey,companyId,userMailboxes,MsgStatus.typeOfStatus(panel),isCustomer,start, end,timezone,false));	
							}else if(typeOfPanel.equalsIgnoreCase("subjects")){
								json.add("messages", InboxSelector.findSubjectMessages(apikey,companyId, panel,isCustomer,timezone,false));							
							}					
						}
					} 
				}	  
			} 
			
			json.add("count", InboxSelector.findCounters(apikey,companyId,false)); 
			status = 200; 
		}else{ 			  
			  json.addProperty("error", "invalid account, login with the correct credentials");
		} 
	  }else{
		  json.add("messages",new JsonArray());
		  json.addProperty("error", "required fields are not specified");
	  } 
	  json.addProperty("status", status);	 
	  renderJSON(json.toString()); 
  }
  
  public static void count_message(){
	  JsonObject json = new JsonObject();
	  Gson gson = new Gson();
	  int status = 405; 
	  
	  Long companyId = Security.connectedCompanyId();
	  if(companyId != null){
		 json.add("count", gson.toJsonTree(InboxSelector.findInboxCounters(companyId,true)));			
		 json.add("subjectCount", null); //InboxSelector.subjectCounter(companyId) 
		 status =200;
	  }
	  
	  json.addProperty("status", status);	 
	  renderJSON(json.toString()); 
  }
  
  public static void view_message(@Required Long id, @Required String msgId,@Required String email, 
		  @Required String msgStatus, String sentiment, String source, String mailbox,Integer timezone){
	  
	  JsonObject json = new JsonObject();
	  Gson gson = new Gson();
	  int status = 405;
	  
	  if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();
		  	Long companyId = Security.connectedCompanyId();		  	 
			String userEmail = Security.connected();			 
			UserType userType = Security.connectedUserType(); 	

			if(companyId != null && userEmail!=null && apikey!=null && userType!=null){
				
			  if(msgStatus.equalsIgnoreCase(MsgStatus.New.name())){
				  
				  int sta =  DropifiMessage.updateOpenStatus(id, msgId);
				  if(sta==200){
					  msgStatus = MsgStatus.Open.name();  
					  MsgCounter.incOpenCounter(companyId,source, sentiment);
					  InboxSelector.deleteAllCaches(apikey,companyId,false);
				  }   				  
			  }  
 
			  //search for the personal profile of the contact: social links, demographic
			 Contact_Serializer contact = InboxSelector.findCustomerProfile(companyId, email);  
			 if(contact!=null){
				 MessageDetailSerializer message = InboxSelector.findMessage(companyId, id, msgId, email,timezone);
				 Map<String,String>optionals = InboxSelector.getOptionals(companyId, msgId, email);
				 
				 if(optionals!=null){
					 try{
						 contact.setPhone(optionals.get("phone")); 
						 message.setPageUrl(optionals.get("pageUrl"));
						 
						 if(optionals.get("location")!=null){
							 //set the location of the contact when message was sent
							 contact.setLocation(optionals.get("location"));
						 }
					 }catch(Exception e){
						 play.Logger.log4j.info(e.getMessage(),e);
					 }
				 }
				 
				 //if(contact.getPhone()==null || contact.getPhone().isEmpty())
				 //contact.setPhone(InboxSelector.findPhone(id));
				 
				 status = 200; 		 
				 json.add("contact", gson.toJsonTree(contact));						
				 json.add("message", gson.toJsonTree(message));   
				
			    /*
			     * if(message !=null){
			 		} else {
					 status = 208;
				 	}	
				 */	

			 }  
			 //search for the users who replied to the message
			 //json.add("replyer", DropifiResponse.lastRepliedBy(apikey, msgId, id,userEmail)); 
			 
		  }else{			  
			  //Redirect to login page 
			  json.addProperty("error", "invalid account, login with the correct credentials");
		  } 
		  
	  }else{
		  json.addProperty("error", "required fields are not specified");
	  }
	  
	  json.addProperty("mstatus",msgStatus);
	  json.addProperty("status", status);
	  json.addProperty("domain", Security.connectedDomain());
	  renderJSON(json.toString());
  }
  
  public static void fetch_sent_messages(@Required boolean isSent,Integer timezone) {
	  String apikey = Security.connectedApikey();
	  JsonObject json = new JsonObject(); 	
	  int status = 404;
	  
	  if(apikey != null && !validation.hasErrors()){		  
		  //search for the users who replied to the message
		  String userEmail = Security.connected(); 		 
		  json.add("messages", DropifiResponse.findTaskSentMessages(apikey, userEmail,isSent,timezone)); 
		  json.add("count", InboxSelector.findCounters(apikey,Security.connectedCompanyId(),false)); 
		  status =200; 
	  }
	  json.addProperty("status", status);
	  renderJSON(json.toString());  
  }
  
  public static void view_sent_message(@Required Long id,String msgId, String email,boolean isSent,Integer timezone){ 
	  JsonObject json = new JsonObject();
	  Gson gson  = new Gson();
	  int status = 404; 
	  if(!validation.hasErrors()){
		  String apikey = Security.connectedApikey();
		  String userEmail = Security.connected();
		  long companyId = Security.connectedCompanyId();
		  if(apikey != null){
			  
			  json.add("message", DropifiResponse.findMessage(apikey,id,userEmail,isSent,timezone));
			  
			  //search for the personal profile of the contact: social links, demography 
			  
			  if(validation.required(email).ok){
				  Contact_Serializer contact = InboxSelector.findCustomerProfile(companyId, email); 
				  Map<String,String>optionals = InboxSelector.getOptionals(companyId, msgId, email);
					 
					 if(optionals!=null){
						 try{
							 
							 contact.setPhone(optionals.get("phone"));
							 if(optionals.get("location")!=null){
								 //set the location of the contact when message was sent
								 contact.setLocation(optionals.get("location"));
							 }
						 }catch(Exception e){
							 play.Logger.log4j.info(e.getMessage(),e);
						 }
					 }
				  json.add("contact", gson.toJsonTree(contact));				  
			  }
			
			  status = 200; 
		  }
	  }
	  
	  json.addProperty("status", status);
	  json.addProperty("mstatus", "Opened");  
	  json.addProperty("domain", Security.connectedDomain());
	  renderJSON(json.toString());
  }
  
  public static void view_reply_messages(@Required String msgId, String email,Integer timezone){ 
	  JsonObject json = new JsonObject();	 
	  int status = 404; 
	  if(!validation.hasErrors()){
		  String apikey = Security.connectedApikey(); 
		  String userEmail = Security.connected();
		  if(apikey != null){  			  
			  json.add("messages", DropifiResponse.findMessages(apikey, msgId,userEmail,timezone));
			  status = 200; 
		  }
	  }
	  
	  json.addProperty("status", status); 
	  json.addProperty("mstatus", "Opened");  
	  json.addProperty("domain", Security.connectedDomain());
	  renderJSON(json.toString()); 
  }
  
  public static void activateCustomer(@Required String email,@Required Boolean activated){
	  JsonObject json = new JsonObject();
		int status = 404; 
		
		if(!validation.hasErrors()){ 			
			//search for the connected company	
			//String apikey = Security.connectedApikey();
		  	Long companyId = Security.connectedCompanyId();		
			if(companyId != null){				 
				status = Customer.updateIsCustomer(companyId, email, activated)	;
				InboxSelector.deleteAllCaches(Security.connectedApikey(),companyId,false);
				json.addProperty("error", (status==200? "none" : "update was not successful") ); 				 
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}		
		json.addProperty("activated", activated);
		json.addProperty("status", status);	
		renderJSON(json.toString()); 
  }
  
  public static void mark_status(@Required long id, @Required String msgStatus,@Required String typeOfUpdate, @Required String source, @Required String msgId, @Required String mailbox){
	  
	  JsonObject json = new JsonObject();	  
	  
	  int status = 405;
	  
	  if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();
		  	Long companyId = Security.connectedCompanyId(); ;
		  	//Company.findCompanyId(apikey);
			String userEmail = Security.connected();
			 
			UserType userType = Security.connectedUserType();		
			
			if(companyId != null && userEmail!=null && apikey!=null && userType!=null){ 
			  if(typeOfUpdate.equalsIgnoreCase(MsgStatus.Pending.name())){
				  if(msgStatus.equalsIgnoreCase(MsgStatus.Open.name())){		  
					  status =  DropifiMessage.updatePendingStatus(id, msgId);
					  if(status ==200){
						  msgStatus = MsgStatus.Pending.name(); 
						  MsgCounter.incPendingCounter(companyId, source);
						  InboxSelector.deleteAllCaches(apikey,companyId,false);
					  } 
				  }
			  }else if(typeOfUpdate.equalsIgnoreCase(MsgStatus.Resolved.name())){ 
				  Boolean currentStatus = null;
				  if(msgStatus.equalsIgnoreCase(MsgStatus.Open.name()))
					  currentStatus = true;
				  else  if(msgStatus.equalsIgnoreCase(MsgStatus.Pending.name()))
					  currentStatus = false; 
				  
				  if(currentStatus != null){		
					 status =  DropifiMessage.updateResolvedStatus(id, msgId);
					  if(status ==200){
						  msgStatus = MsgStatus.Resolved.name(); 
						  MsgCounter.incResolvedCounter(companyId, source, currentStatus);		
						  InboxSelector.deleteAllCaches(apikey,companyId,false);
					  }						
				  }				 
			  }else{
				  status = 403;
			  } 
		  }
		  
	  }else{
		  json.addProperty("error", "required fields are not specified");
	  }
	  
	  json.addProperty("mstatus",msgStatus);
	  json.addProperty("status", status);
	  renderJSON(json.toString());
  }
	
  public static void count_sent_messages(Long messageId, String msgId){
	  String apikey = Security.connectedApikey();
	  JsonObject json = new JsonObject();	
	  int status = 404;
	  if(apikey != null){ 
		  json.add("sentMessages", InboxSelector.countSents(apikey,Security.connectedCompanyId(),msgId));     
		  status =200;
	  } 
	  json.addProperty("status", status);
	  renderJSON(json.toString()); 
  }
  
  public static void delete_message(@Required String typeOfPanel, @Required List<Long> messageIds){
	  play.Logger.info("Over here delete");
	  String apikey = Security.connectedApikey();
	  long companyId = Security.connectedCompanyId();
	  JsonObject json = new JsonObject(); 	
	  int status = 404;
	  
	  if(apikey!=null && !validation.hasErrors()){
		  typeOfPanel =typeOfPanel.trim().toLowerCase();
		 if(typeOfPanel.equals("ref_sent") || typeOfPanel.equals("ref_drafts")){
			 status =  DropifiResponse.deleteMessage(companyId, messageIds);			
		 }else{
			 status =  DropifiMessage.deleteMessage(companyId, messageIds); 
		 }
		 InboxSelector.deleteAllCaches(apikey,companyId,false);
	  }	   
	  json.addProperty("status", status); 
	  renderJSON(json.toString());
  } 
  
  public static void search_message(@Required String search, Integer timezone){
	  String apikey = Security.connectedApikey();
	  long companyId = Security.connectedCompanyId();
	  JsonObject json = new JsonObject(); 	
	  
	  int status = 404;
	  
	  if(apikey!=null && !validation.hasErrors()){
		  json.add("messages", InboxSelector.searchMessage(companyId, search,timezone)); 
		status = 200; 
	  }
	  
	  json.addProperty("status", status);
	  renderJSON(json.toString()); 
  }
  
  public static void search_words(Integer timezone){
	  JsonObject json = new JsonObject(); 
	  Gson gson = new Gson();
	  
	  Long companyId = Security.connectedCompanyId(); 
	  int status = 405;
	  if(companyId!=null){
		  List<String> searchTerm = Customer.getEmailNameList(companyId);		  
		  json.add("search", gson.toJsonTree(searchTerm));
		  status =200;
	  } 
	  
	  json.addProperty("status", status);	 
	  renderJSON(json.toString()); 
  }
  
  public static void search_email(){ 
	  JsonObject json = new JsonObject(); 
	  Gson gson = new Gson();
	  
	  Long companyId = Security.connectedCompanyId(); 
	  int status = 405;
	  
	  if(companyId!=null){
		  List<String> searchTerm = Customer.getEmailList(companyId);		  
		  json.add("search", gson.toJsonTree(searchTerm));
		  status =200;
	  }
	  
	  json.addProperty("status", status);	 
	  renderJSON(json.toString()); 
  }
 
  public static void fileUploadTest(File fileToUpload, String name, String id){
	  JsonObject json = new JsonObject();
	  try { 
		  String msg ="";
		  if(fileToUpload != null){			 
			  //json.put("file", fileToUpload.getName());
			  json.addProperty("error", "File Name: "+fileToUpload.length());
			  play.Logger.log4j.info("testing file upload: "+fileToUpload.getName()+" Additional: "+name);
			  msg ="file uploaded";
		  }else{
			  msg= "file not uploaded";	
			  json.addProperty("error", "No file was uploaded.");
		  }
		  json.addProperty("msg", msg);  
		  
		  String xml = org.json.XML.toString(json);
		  //play.Logger.log4j.info("JSON: "+json.toString());
		  //renderXml(xml);
		  
		  renderJSON(json.toString()); 
	  } catch (JSONException e){
			// TODO Auto-generated catch block
		  play.Logger.log4j.info("JSON: "+json.toString());
		  renderXml("<error>An error occurred</error>"); 
	  }
	  
	  //renderJSON(f + '(' + jsonser.serialize(v)+ ')');
   }
  
}