package controllers;

import static akka.actor.Actors.actorOf;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import job.process.EventTrackActor;
import job.process.LoginActor;
import akka.actor.ActorRef;
import models.AccountUser;
import models.Company;
import models.WidgetTracker;
import models.addons.AddonSubscription;
import play.Play;
import play.mvc.*;
import play.cache.CacheFor;
import play.data.validation.*;
import play.libs.*;
import play.utils.*;
import resources.Check;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import resources.addons.AddonStatus;
import view_serializers.LoginSerializer;

@With(Compress.class)
public class Login extends Controller {

	@Before
	public static void onload(){
		ForceSSL.secureDevMode();
	}
	
    @Before(unless={"login", "authenticate", "logout","signup","forgot_password","reset","reset_user_password","create_new_password"})
    static void checkAccess() throws Throwable{
        
    	//Authentication
        if(!session.contains("username")) {
            flash.put("url", "GET".equals(request.method) ? request.url : "/");  
            login("");
        } 
        
        //Checks
        Check check = getActionAnnotation(Check.class);
        if(check != null) {
            check(check);
        }
        check = getControllerInheritedAnnotation(Check.class);
        if(check != null) {
            check(check);
        }
    }
    
    private static void check(Check check) throws Throwable {
        for(String profile : check.value()) {
            boolean hasProfile = (Boolean)Security.invoke("check", profile);
            
            if(!hasProfile) {
                Security.invoke("onCheckFailed", profile);
            }
        }
    }
       
    // ~~~ Login 
    public static void login(String url,String... args)throws Throwable {
    	 
    	if(url==null)url="";
    	
		//check that the user is already login. if true direct user to the dashboard
		String apikey = Security.connectedApikey();  
        if(apikey != null && !apikey.trim().isEmpty()){        	
        	Security.invoke("onAuthenticated",url,"","");
        }
        
        String useremail = Security.getCookie("rememberme", Crypto.sign("rememberlogin"));  
        if(useremail!=null){
        	flash.put("useremail", useremail);
            flash.put("remember", true); 
            flash.keep();
        }else{
        	useremail="";
        	flash.put("remember", false); 
        } 
        String error=null;
	    if(args!=null){
	    	try{
	    		useremail = args.length>1?args[0]:null; 
	    	}catch(Exception e){
	    		play.Logger.log4j.info(e.getMessage(),e);
	    	}
	    	
	    	try{
	    		error = args.length>2?args[1]:null; 
	    	}catch(Exception e){
	    		play.Logger.log4j.info(e.getMessage(),e);
	    	}
	    }
	    
	    //flash.keep("url");
	    renderArgs.put("page", "login");  
	    render(url,useremail,error);
    } 
    
    /**
	 * This method is called during the authentication process. This is where you check if
	 * the user is allowed to log in into the system. This is the actual authentication process
	 * against a third party system (most of the time a DB).
	 * 
	 */	
    public static void authenticate(@Required @Email String useremail, @Required String password,boolean remember,String IPAddress,String url,String location) throws Throwable {
    	
        // Check tokens
        Integer allowed = 501;           
        String error=null;
        try {        	
        	String timezone = request.params.get("timezone");
        	
        	IPAddress =IPAddress!=null?IPAddress:"";
        	location =location!=null?location:"";
        	timezone =timezone!=null?timezone:"";
        	
        	allowed = (Integer)Security.invoke("authenticate", useremail,password,IPAddress,location,timezone);          
        } catch (Exception e ) {
        	play.Logger.log4j.info(e.getMessage(),e);
        }
        
        try{        
	        if(validation.hasErrors() || allowed!=200 ){
	            //flash.keep("url");
	            if(allowed ==201){ 
	            	error ="Oops, invalid account password";
	            	//renderArgs.put("error", "Oops, invalid account password"); 
	            }else if(allowed==203){
	            	error ="Oops, your login credentials have been deactiavted. Contact your dropifi account manager";
	            }else{
	            	error ="Oops, there is no account associated with this email"; 
	            }
	            
	            flash.error(error);
	            flash.keep();
	            
	            login(url,useremail); 
	        }
	        
	        // Remember if needed
	        if(remember) {
	        	Security.setCookie("rememberme", Crypto.sign("rememberlogin"), useremail, "15d"); 
	        }else{ 
	        	//or remove remember if not needed
	        	response.removeCookie("rememberme");
	        }
	        
	        if(url==null) url=""; 
	        
	        if(error==null)error="";
	        	flash.error(error);
	        //Redirect to the original URL (or /)
	        Security.invoke("onAuthenticated",url,error,"");
        
        }catch(Exception e){
        	play.Logger.log4j.info(e.getMessage(),e); 
        	if(error==null)error="";
        		flash.error(error);
        	
        	login(url,useremail);
        	//renderTemplate("Secure/login.html", url,useremail,error="");
        }
    } 
    
    public static void logout() throws Throwable{  
    	
       if(flash.get("secure.logout")!=null)
    	   flash.success(flash.get("secure.logout"));  
       else if(flash.get("secure.error")!=null)
    	   flash.error(flash.get("secure.error")); 
       
       if(Security.connectedPrestashop()!=null && Security.connectedPrestashop().equals("true"))
    	   PrestaShop.back_office();
       
       String socialUID = AddonSubscription.getUID(Security.connectedApikey(),DropifiTools.SOCIAL_REVIEW, AddonStatus.Installed);
       if(socialUID!=null){
    	   flash.put("uid",socialUID);
       }
       
       session.clear();
       redirect("/");
    }
 
    // ~~~ Utils 
    @Util
    static void redirectToOriginalURL() throws Throwable {
        Security.invoke("onAuthenticated","","","");
        
       /* String url = flash.get("url");
        if(url == null) {
            url = "/";
        }
        redirect(url);*/
    }

    public static class Security extends Controller {

        /**
         * @Deprecated
         * 
         * @param username
         * @param password
         * @return
         */
    	
        
        @Deprecated
        static boolean authentify(String username,String domain,String password) {
            throw new UnsupportedOperationException();
        } 

        /**
         * This method is called during the authentication process. This is where you check if
         * the user is allowed to log in into the system. This is the actual authentication process
         * against a third party system (most of the time a DB).
         *
         * @param username
         * @param password
         * @return true if the authentication process succeeded
         */
        static int authenticate(String useremail,String password,String IPAddress,String location,String timezone) {            
        	Map<String,String> credentials = Company.authenticate(useremail, password);
    		if(credentials != null){
    			try{
    				String hPassword = Codec.hexMD5(password); 	
    				String credPassword = credentials.get("hashPassword");
    				String userType = credentials.get("userType");
    				Boolean activated = Boolean.valueOf(credentials.get("activated"));
    				
    				if(userType!=null && userType.equals(UserType.Agent.name()) && activated!=null && activated==false){
    					 return 203; //account deactivated
    				}
    				
    				if(credPassword!=null && hPassword!=null && hPassword.compareTo(credPassword) == 0){
    					//Mark user as connected
    					credentials.put("userEmail", useremail);
    					credentials.put("IPAddress", IPAddress);
    					credentials.put("location", location);
    					Security.setLoginSession(credentials); 
    					return 200; 
    				}
    			}catch(Exception e){
    				play.Logger.log4j.info(e.getMessage(),e);
    			}
    			return 201; 			 
    		} 
    		return 501; 
        }
        
        static int reset(String email){
        	return -1;
        }

        /**
         * This method checks that a profile is allowed to view this page/method. This method is called prior
         * to the method's controller annotated with the @Check method. 
         *
         * @param profile
         * @return true if you are allowed to execute this controller method.
         */
        static boolean check(String profile) {
            return true;
        }

        /**
         * This method returns the current connected user email
         * @return
         */
        @Util
        static String connected() {
            return session.get("username");
        }
        
        @Util
        static void setConnected(String email) {
            session.put("username",email);
        }
        
        
        /**
         * This method returns the current connected user name
         * @return
         */
        @Util
        static String connectedName() { 
        	String name = session.get("loginName");
            return name!=null?name:"";
        }
        
        @Util
        static void setConnectedName(String loginName) { 
        	  session.put("loginName",loginName);
        }
        
        /**
         * Set a value to indicate that the user login through prestashop back office demo
         * @param prestashop
         */
        @Util
        static void setPrestashop(String prestashop) {
            session.put("prestashop",prestashop);
        }
        /**
         * This method return the apikey of the current connected company
         * @return
         */
        @Util
        static String connectedApikey(){
        	return session.get("apikey"); 
        }
        
        @Util
        static String connectedDomain(){
        	return session.get("domain");
        }
        
        @Util
        static UserType connectedUserType(){
        	String user = session.get("userType");
        	if(user !=null){
	        	if(user.equalsIgnoreCase(UserType.Admin.name()))
	        		return UserType.Admin;
	        	else
	        		return UserType.Agent;
        	}
        	return null;
        }
        
        @Util
        static Long connectedCompanyId(){
        	try{
        		return Long.parseLong(session.get("companyId"));
        	}catch(Exception e){
        		return null;
        	}
        }
        
        @Util
        static String connectedPlugin(){
        	return session.get("ecomBlogPlugin");
        }
        
        @Util
        static String connectedPrestashop(){
        	return session.get("prestashop");
        }
        
        /**
         * This method returns the date the user account was created
         * @return
         */
        @Util
        static Timestamp connectedCreated(){
        	try{
        		//Date date = new Date();         		
        		return new Timestamp(Long.valueOf(session.get("created")));
        	}catch(Exception e){        		 
        		return null;
        	}
        }
     
        @Util
        static void setCookie(String name,String privateKey, String value, String duration) {
           try{
        	// Setting cookie
        	if(privateKey==null || privateKey.length()<=15)
        		return;
        	
        	privateKey = (privateKey.length()==16)?privateKey: privateKey.substring(0, 16);
            response.setCookie(name, Crypto.encryptAES(value, privateKey), duration);
           }catch(Exception e){
        	   play.Logger.log4j.info(e.getMessage(),e);
           }
        }

        @Util
        static String getCookie(String key,String privateKey) {
            try{
        	// retrieving cookie by key
        	if(privateKey==null ||privateKey.length()<16)
        		return null;
        	
        	privateKey = (privateKey.length()==16)?privateKey: privateKey.substring(0, 16);
        	Http.Cookie ck = request.cookies.get(key); 
            String value = ck!=null?Crypto.decryptAES(ck.value, privateKey):null; 
             return value;
            }catch(Exception e){ 
            	play.Logger.log4j.info(e.getMessage(),e);
            }
            return null;
        }
        
        @Util
        static void removeCookie(String key) {
        	try{
        		request.cookies.remove(key);
        		response.cookies.remove(key);
        	}catch(Exception e){
        		play.Logger.log4j.info(e.getMessage(),e);
        	}
        }
   
        
        /**
         * Indicate if a user is currently connected
         * @return  true if the user is connected
         */
        static boolean isConnected() {
            return session.contains("username");
        }

        /**
         * This method is called after a successful authentication.
         * You need to override this method if you with to perform specific actions (eg. Record the time the user signed in)
         */
        static void onAuthenticated(String url,String error,String prestashop){ 
        	//perform checks on the user and provide the redirecting url 		 
    		String domain = Security.connectedDomain();
    		String apikey = Security.connectedApikey();
    		String useremail = Security.connected();
    		if(apikey !=null && !apikey.trim().isEmpty()){
    			
    			//update the user login date 
    			 try{
    		    	 LoginSerializer login = new LoginSerializer(Security.connected(), apikey)	;					
    				 ActorRef loginActor = actorOf(LoginActor.class).start();   
    				 loginActor.tell(login); 
    	    	    }catch(Exception e){
    	    	    	
    	    	    } 
    			 
    			 //Redirect the user to specific page within the app which the user want to access.
    			 //Eg. when a user click this link (www.dropifi.com/login?url=/dashboard/manage_account) 
    			 //to get to the login and succeed in logging in, he will redirect to the Account Settings page
    			 if(url!=null && !url.trim().isEmpty()){
    				 url = url.replace("|", "&");
    				 url = url.startsWith("/")?url:"/"+url; 
    				 redirect("/"+domain+url); 
    			 }
    			 
    			//redirect to Dashboard 
    			boolean newAccount = Boolean.valueOf(session.get("newAccount"));  
    			session.put("prestashop", prestashop); 
    			
    			//logout if the user is logging as prestashop demo and the account is not prestashop
    			if(prestashop!=null && prestashop.equals("true") && !Security.connectedPlugin().equals(EcomBlogType.PrestaShop.name())){
    				try {
    					flash.put("secure.error", "You have been logout because the account is not a prestshop account");
    					Secure.logout();
    				} catch (Throwable e) {
    					// TODO Auto-generated catch block
    					Apps.page_not_found("");
    					play.Logger.log4j.info(e.getMessage(),e);
    				}
    			}
    			
    			if( domain != null && !newAccount){ 
    				//check if the user has the widget code actively on their site
    				WidgetTracker track = WidgetTracker.findByApikey(apikey);
    				
    				if(track==null || !track.hasWidgetInstalled(EcomBlogType.valueOf(Security.connectedPlugin()))){
    					redirect("/"+domain+"/dashboard/welcome/"); 
    				} 
    				Dashboard.index(domain);
    				
    			}else if(domain != null && newAccount){
    				redirect("/"+domain+"/dashboard/welcome/");
    			} 	
    		}
    		
    		try {			 
    	        session.clear(); 
    	        renderTemplate("Secure/login.html", url,useremail,error);
    		} catch (Throwable e) {
    			// TODO Auto-generated catch block
    			Application.page("home");
    		}
        	
        }
        
        /**
         * Add the credentials to the session after a successful login and
         * send an event with the user details to mixpanel and google analytics to track the user login
         * @param credentials
         */
        static void setLoginSession(Map<String,String> credentials){ 
        	//redirect to the profile page 		    		
        	if(credentials!=null){ 
        		session.clear();  
    			// Mark user as connected				
    	        session.put("username", credentials.get("userEmail"));       
    	        session.put("domain",credentials.get("domain"));
    	        session.put("apikey",credentials.get("apikey")); 
    	        session.put("companyId",credentials.get("companyId")); 
    	        session.put("userType",credentials.get("userType"));	        
    	        session.put("created",credentials.get("created"));
    	        session.put("loginName", credentials.get("username"));   
    	        
    	        EcomBlogType ecomType = EcomBlogType.getValue(credentials.get("ecomBlogPlugin"));	       
    		    session.put("ecomBlogPlugin", ecomType.name());
    		   	      
    		    //track user login 
    	        try{
    	    	    EventTrackSerializer event = new EventTrackSerializer(credentials.get("username"),credentials.get("userEmail"),"LOGIN",
    	    	    		credentials.get("IPAddress"),credentials.get("userEmail"),credentials.get("userType"),true);
    	    	    
    	    	    event.setWidgetType(ecomType.name());
    	    	    event.setAccountType(credentials.get("accountType"));
    	    	    event.setUsername(credentials.get("domain"));
    	    	    event.setCreated(credentials.get("created"));
    	    	    event.setUrl(credentials.get("url"));
    	    	    event.location =credentials.get("location");
    	    	    event.setPhone(credentials.get("phone"));
    	    	    
    				ActorRef eventActor = actorOf(EventTrackActor.class).start();   
    				eventActor.tell(event); 
        	    }catch(Exception e){
        	    	play.Logger.log4j.error(e.getMessage(),e);
        	    }	 
    	        
    		    Company.clearSubscriptionFromCache(credentials.get("apikey"));

        	}
        }

         /**
         * This method is called before a user tries to sign off.
         * You need to override this method if you wish to perform specific actions (eg. Record the name of the user who signed off)
         */
        static void onDisconnect() {
        }

         /**
         * This method is called after a successful sign off.
         * You need to override this method if you wish to perform specific actions (eg. Record the time the user signed off)
         */
        static void onDisconnected() {
        }

        /**
         * This method is called if a check does not succeed. By default it shows the not allowed page (the controller forbidden method).
         * @param profile
         */
        static void onCheckFailed(String profile) {
            forbidden();
        }
         
        public static Object invoke(String m, Object... args) throws Throwable {
            try {
                return Java.invokeChildOrStatic(Security.class, m, args);        
            } catch(InvocationTargetException e) {
                throw e.getTargetException();
            }
        }

    }

}
