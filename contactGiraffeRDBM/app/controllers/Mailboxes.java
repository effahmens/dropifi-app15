package controllers; 
import gmailoauth2grant.DGOauthGrant;

import java.io.IOException;
import java.util.*; 

import javax.mail.MessagingException;

import adminModels.DropifiMailer;
import adminModels.MailboxConfig;

import com.google.gson.*;

import models.*;
import models.dsubscription.SubConsumeService;
import models.dsubscription.SubServiceName;
import models.dsubscription.SubServiceType;
import models.gmailauth.GmailCredentials;
import play.data.validation.*;
import play.mvc.*;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.UserType;
import resources.helper.ServiceSerializer;
import resources.helper.ServiceVolumeSerializer;
import view_serializers.MbSerializer; 
@With(Compress.class)
public class Mailboxes extends Controller{
	//adEmail: the email address used to create the account
	
	@Before
    static void setArgs(String domain){
		String apikey = Security.connectedApikey();	
 		String foundApikey = Company.findApikey(apikey);
 		if(apikey !=null && foundApikey!=null && apikey.compareTo(foundApikey) == 0) {
 			 renderArgs.put("page","admin");
		     renderArgs.put("sub_page","mail"); 
		     renderArgs.put("title","Admin - Mail Management (Inbound Mailboxes)"); 			 		     
 		}else{
 			//clear the session
 	    	session.clear(); 
 	    	//response.removeCookie("rememberme");			
 			try {
 				Secure.login("");
 			} catch (Throwable e) { 
 				Application.index();
 			}
 		} 
    } 
	
	//displays all the inbound mailbox
	public static void index(@Required String domain,String emailMailbox){ 	
		//TODO implemented security checks
		ForceSSL.redirectToHttps();
		
		UserType userTypes = Security.connectedUserType();
		if(UserType.isAgent(userTypes) ){	
			redirect("/"+domain+"/dashboard/index");
			//Dashboard.index(domain);
		} 
	
		String apikey = Security.connectedApikey();		
		 		
		if(!validation.hasErrors() && apikey !=null){			
			//check if domain is a valid
			
 			String connected = Security.connectedDomain(); 	
 			domain =domain!=null? domain.trim():"";
 			if(!connected.equalsIgnoreCase(domain)){			
 				index(connected,emailMailbox);
 			}
 				String connectedUsername = Security.connected(); 				
 				String ecomBlogPlugin = Security.connectedPlugin();
 				String userType = userTypes.name();
 				//check if the widget is installed on the site of the user
 				boolean hasWidget = true; //WidgetTracker.hasWidgetInstalled(apikey, EcomBlogType.getValue(ecomBlogPlugin));
 				ServiceSerializer service = CacheKey.getMMServiceSerializer(apikey,CacheKey.MonitoredMailboxes, SubServiceName.MonitoredMailbox);
 				
 				render(userType,domain,connectedUsername,ecomBlogPlugin,hasWidget,service,emailMailbox);
 			/*}else{ 				
 				//check if the domain is available 
 				
 				boolean hasDomain = Company.isDomainAvailable(domain);
 				if(hasDomain){	//show domain not found page
 					Apps.multiple_login(connected);
 				}else{
 					Apps.domain_not_found(domain);
 				}
 			}*/
		}
		
		Apps.page_not_found("");
	}
	
	public static void populate(){
		//TODO implemented security checks
			 
		JsonObject json = new JsonObject();
		Gson gson = new Gson();
		int status = 404;
		String apikey = Security.connectedApikey();
		
		//String domain =Security.connectedDomain();
		List mailboxes = null; 
		int size =0;
		if(apikey !=null){
			mailboxes = Channel.findAllInboundMb(apikey); 		
		
			//check the status of the mailbox: if size 0 then return 200 meaning mailbox(s) is/are available
			size = mailboxes.size();
			status = (mailboxes != null && size>0) ? 200:501;	
			
			//update the total volume consumed
			/*try{
				ServiceVolumeSerializer volSerializer = new ServiceVolumeSerializer(apikey, SubServiceName.MonitoredMailbox, SubServiceType.FixedVolume, size);
				//ActorRef widgetActor = actorOf(WidgetActor.class).start();
				//widgetActor.tell(volSerializer);
				SubConsumeService.updateConsumedVolume(volSerializer); 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);  
			}
			
			ServiceSerializer service = SubConsumeService.findServiceSerializer(apikey, SubServiceName.MonitoredMailbox);
			json.add("service", gson.toJsonTree(service));
			
			//cache the service
			CacheKey.cacheSubService(apikey,CacheKey.MonitoredMailboxes,service, DropifiTools.SUBSERVICE);
			*/
				
			json.add("mailboxes", MbSerializer.toJsonArray(mailboxes));
			json.addProperty("mmsize", CacheKey.getMonitoredMailboxes(apikey, Security.connectedCompanyId()));
		}
		
		json.addProperty("status", status);
		json.addProperty("size", size);
		
		renderJSON(json.toString());
	}
	
	//add a new outbound mailbox to the existing mailboxes
 	public static void create(MbSerializer mb){
 		
 		//TODO: 
 		/**
 		 * 
 		 * 
 		 * 
 		 * 
 		 * Not Implemented: add business rules to the creation of a mailbox 
 		 * 
 		 * 
 		 * 
 		 */

		//validate the mailbox data
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		
		//search for the connected company	
		String apikey = Security.connectedApikey();
		Long companyId = Company.findCompanyId(apikey);
		// && InboundMailbox.countMailbox(companyId)<2
		if(companyId != null){ 
			//add server info if selected server is not other 
			if(!mb.getMailserver().equalsIgnoreCase("other")){
				//search for the server setting of mail server
				MailboxConfig config = MailboxConfig.findByMailServer(mb.getMailserver()); //search for the mailserver configuration
				
				if(config != null){ 
					mb.setHostType(config.getHostType());
					mb.setOutHostType(config.getOutHostType());
					
					mb.setHost(config.getHost());
					mb.setOutHost (config.getOutHost()); 					
					
					mb.setPort(config.getPort());
					mb.setOutPort(config.getOutPort()); 				
				}else{
					//send mail server not found error message
					status = 250;	
					
					json.add("content", gson.toJsonTree(mb)); 
					json.addProperty("error", "there are no settings associated with this mail server");
					json.addProperty("status", status); 
					renderJSON(json.toString());
				}
			} else {
				//check that all required fields are met 
				 
			}			
			
		 	//check if the email is a valid one
			if(!mb.validateAll() && validation.required(mb.getEmail()).ok && validation.email(mb.getEmail()).ok){	 
					
					if(apikey != null){						
						Channel channel = Channel.findByApikey(apikey);
						
						if(channel != null){
							//Set all new created mailboxes as managed
							//mb.setMonitored(true);
							/*if(mb.getMonitored()!=null && mb.getMonitored()){
								ServiceSerializer service = CacheKey.getMMServiceSerializer(apikey,CacheKey.MonitoredMailboxes, SubServiceName.MonitoredMailbox);
								if(service==null || service.hasExceeded){
									mb.setMonitored(false); 
								}
							}
							*/
							
							status = channel.addInboundMb(mb.getInboundMailbox(), Security.connectedDomain(),false); 	
						 
							String error="";
							if(status==200)
								error ="update was successful" ;
							else if(status==205)
								error ="Authentication failed! The email or password specified may not be correct";
							else if(status==302)
								error = "duplication of mailbox! This mailbox has been created already";
							else if(status == 610)
								error ="could not connect to mailbox. Invalid inbound settings";
							else if(status == 611)
								error ="could not connect to mailbox. Invalid outbound settings";
							else if(status>607)
								error = "could not connect to mailbox. Invalid inbound and outbound settings";
							else
								error = "update was not successful";
							
							json.addProperty("error",error);
							
							//Assign this mailbox to the current user as a role if the user is an agent
							UserType userType = Security.connectedUserType();
							
							/*if(status == 200 && UserType.isAgent(userType)){
								String email = Security.connected();
								AccountUser user = AccountUser.findByAgent(email, apikey);
								if(user!=null){
									user.addRole(new AgentRole(mb.getEmail())); 
								} 
							}
							*/
							
							//if the user is an agent add as role
			    			if(status == 200 && Security.connectedUserType().equals(UserType.Agent)){
			    				AccountUser acc = AccountUser.findByAgent(Security.connected(), apikey);
			    				if(acc!=null){
			    					 acc.addAgentRole(mb.getEmail());
			    				}
			    			}
							
							//update the total volume consumed
							try{
								ServiceVolumeSerializer volSerializer = new ServiceVolumeSerializer(apikey, SubServiceName.MonitoredMailbox, SubServiceType.FixedVolume, InboundMailbox.countMonitoredMailbox(channel.getCompanyId()));
								SubConsumeService.updateConsumedVolume(volSerializer); 
							}catch(Exception e){
								play.Logger.log4j.info(e.getMessage(), e); 
							}
							
							CacheKey.delListOfRepFromCache(apikey,CacheKey.MonitoredMailboxes);
							
							
						}else{
							status = 407;
							json.addProperty("error", "account do not exist");
						}
						
					}else{
						status = 405;
						json.addProperty("error", "account do not exist");
					}
	 
			}else{
				json.addProperty("error", "the format of the email address is not correct."); 
			}
		}else{
			json.addProperty("error", "you can only create one mailbox"); 
			status = 211; 
		}
		 
		json.addProperty("status", status); 
		json.addProperty("monitored",mb.getMonitored());
		renderJSON(json.toString());		
	}
	
	//update an existing outbound mailbox	
	public static void update(@Required String email, MbSerializer mb){ 
		//NB: the object outbound is a json array containing the fields of the mailbox
		//required fields:username, email,password, hostType, host, port, activated
		//validate the mailbox data
		
		Gson gson = new Gson();				 
		JsonObject json = new JsonObject();
		int status= 404;
		String error="";
		if(!validation.hasErrors()){
				 		
			//add server info if selected server is not other 
			if(!mb.getMailserver().equalsIgnoreCase("other")){
				//search for the server setting of mail server
				MailboxConfig config = MailboxConfig.findByMailServer(mb.getMailserver());
				
				if(config != null){
					mb.setHostType(config.getHostType());
					mb.setOutHostType(config.getOutHostType());
					
					mb.setHost(config.getHost());
					mb.setOutHost(config.getOutHost()); 					
					
					mb.setPort(config.getPort());
					mb.setOutPort(config.getOutPort()); 
					
				}else{
					//send mail server not found error message
					status = 250;	
					
					json.add("content", gson.toJsonTree(mb)); 
					json.addProperty("error", "there are no settings associated with this mail server");
					json.addProperty("status", status); 
					renderJSON(json.toString()); 
				} 				
			}  			
			
		 	//check if the email is a valid one
			if(validation.required(mb.getEmail()).ok && validation.email(mb.getEmail()).ok){
				String apikey = Security.connectedApikey(); 
				if(apikey != null){				
					 				 				
					Channel channel = Channel.findByApikey(apikey); 
					if(channel != null){
						
						InboundMailbox mailbox = channel.findInboundMb(email); 
						
						/*if(mb.getMonitored()!=null  && mb.getMonitored()&& mailbox.getMonitored()!=null && !mailbox.getMonitored()){
							ServiceSerializer service = CacheKey.getMMServiceSerializer(apikey,CacheKey.MonitoredMailboxes, SubServiceName.MonitoredMailbox);
							if(service==null || service.hasExceeded){
								mb.setMonitored(false);
							}
						}*/
						
						InboundMailbox upMailbox = mb.getInboundMailbox();
						//update the changes to the mailbox
						status = channel.updateInboundMb(mailbox,upMailbox,Security.connectedDomain(),false);  
						  
						if(status==200){
							error ="update was successful";
							
							//if the user is an agent add as role
			    			if(Security.connectedUserType().equals(UserType.Agent)){
			    				AccountUser acc = AccountUser.findByAgent(Security.connected(), apikey);
			    				if(acc!=null){
			    					 acc.updateAgentRole(mailbox.getEmail(),upMailbox.getEmail());
			    				}
			    			}
						}
						else if(status==205)
							error ="Authentication failed! The email or password specified may not be correct";
						else if(status==302)
							error = "duplication of mailbox! This mailbox has been created already";
						else if(status == 610)
							error ="could not connect to mailbox. Invalid inbound settings";
						else if(status == 611)
							error ="could not connect to mailbox. Invalid outbound settings";
						else if(status>607)
							error = "could not connect to mailbox. Invalid inbound and outbound settings";
						else
							error = "update was not successful";
						
						//update the total volume consumed
						try{
							ServiceVolumeSerializer volSerializer = new ServiceVolumeSerializer(apikey, SubServiceName.MonitoredMailbox, SubServiceType.FixedVolume, InboundMailbox.countMonitoredMailbox(channel.getCompanyId()));
							SubConsumeService.updateConsumedVolume(volSerializer); 
						}catch(Exception e){
							play.Logger.log4j.info(e.getMessage(), e); 
						} 
						
						CacheKey.delListOfRepFromCache(apikey,CacheKey.MonitoredMailboxes);
					} 
					
				}else{//no apikey found
					status = 405;
					error ="acount do not exist";
				}
				
			}else{
				error= "invalid email";
			}
		
		}else{
			status = 401;
			error= "required fields are not specified";
		}
		
		json.addProperty("error",error);
		json.add("content", gson.toJsonTree(mb));
		json.addProperty("status", status); 
		
		renderJSON(json.toString());	
	}
	
	//delete an inbound mailbox from the list of mailboxes
	public static void delete(@Required String email){
		JsonObject json = new JsonObject();
		int status = 404;
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();
			
			if(apikey != null){				
				
				Channel channel = Channel.findByApikey(apikey);
				
				if(channel != null){ 	
					InboundMailbox mailbox = channel.findInboundMb(email);
					
					//update the changes to the mailbox
					status = channel.removeInboundMb(mailbox);
					json.addProperty("error", (status==200? "none" : "update was not successful") );
					
					//if the user is an agent add as role
	    			if(status == 200 && Security.connectedUserType().equals(UserType.Agent)){
	    				AccountUser acc = AccountUser.findByAgent(Security.connected(), apikey);
	    				if(acc!=null){
	    					 acc.removeRole(mailbox.getEmail());
	    				}
	    			}
					//update the total volume consumed
					try{
						ServiceVolumeSerializer volSerializer = new ServiceVolumeSerializer(apikey, SubServiceName.MonitoredMailbox, SubServiceType.FixedVolume, InboundMailbox.countMonitoredMailbox(channel.getCompanyId()));
						SubConsumeService.updateConsumedVolume(volSerializer); 
					}catch(Exception e){
						play.Logger.log4j.info(e.getMessage(), e); 
					}
					
					CacheKey.delListOfRepFromCache(apikey,CacheKey.MonitoredMailboxes);
				} 
			}
			
		}
		
		json.addProperty("status", status); 
		json.addProperty("email", email); 
		renderJSON(json.toString());
	}
	
	//displays an editable details  of an inbound mailbox
	public static void show(@Required String email, @Required String domain){		
		ForceSSL.redirectToHttps(); 
		String connected = Security.connectedDomain(); 			
		String apikey = Security.connectedApikey();
		
		if(apikey!=null && connected.equalsIgnoreCase(domain) && !validation.hasErrors()){				
			List<String> mailservers = MailboxConfig.findAllMailserver(); 
			//play.Logger.log4j.info(mailservers.size());
			String btnsave =""; 
			String connectedUsername = Security.connected(); 				
			String ecomBlogPlugin = Security.connectedPlugin();
			UserType userTypes = Security.connectedUserType();
			String userType = userTypes.name();
			String gmailUrl = DGOauthGrant.authorization_url();
			
			InboundMailbox mailbox = null;
			 
			if(validation.required(email).ok){ 
				//check if an email is specified
					
				if(email.equals("new")){
					 btnsave ="Save";   
				}else if (validation.email(email).ok){
					//search for the connected company //search for the mailbox
					Channel channel = Channel.findByApikey(apikey);				
					mailbox = channel == null ? null:channel.findInboundMb(email);					
					btnsave ="Update";
					if(mailbox.getMailserver().equals("Gmail")){
						GmailCredentials credential = GmailCredentials.retrieveCredential(apikey, email);
						if(credential!=null){
							gmailUrl =null;
						}
					}
				}		
			}else{ 
				email="new";
				btnsave ="Save"; 
				mailbox = new InboundMailbox();
			}	
			render(btnsave, email,mailbox,mailservers,domain,connectedUsername,ecomBlogPlugin,userType,gmailUrl); 
		}
		
		Apps.popout_not_found("");
		 
	}

	//enable or disable mailbox
	public static void activateMailbox(@Required String email, @Required Boolean activated){
		 
		JsonObject json = new JsonObject();
		int status = 404; 
		
		if(!validation.hasErrors()){
			
			//search for the connected company	
			String apikey = Security.connectedApikey();			
			
			if(apikey != null){
				Channel channel = Channel.findByApikey(apikey);
				if(channel != null){ 	
					InboundMailbox mailbox = channel.findInboundMb(email);					
					//update the changes to the mailbox
					status = mailbox.updateActivated(activated); 					
					json.addProperty("error", (status==200? "none" : "update was not successful") );
					CacheKey.delListOfRepFromCache(apikey,CacheKey.MonitoredMailboxes);
				} 
			}else{
				json.addProperty("error", "unknown user account");
			}
			
		}else{
			json.addProperty("error", "required fields are not specified");
		}
		
		json.addProperty("activated", activated);
		json.addProperty("status", status);	
		renderJSON(json.toString()); 
	}
	
	public static void checkMonitoredMailbox(@Required boolean monitored, String email){
		JsonObject json = new JsonObject();
		int status = 404; 
		if(!validation.hasErrors()){ 
			String apikey = Security.connectedApikey();		
			if(monitored && apikey!=null){
				InboundMailbox mailbox = InboundMailbox.findByApikeyAndEmail(apikey, email);
				ServiceSerializer service = CacheKey.getMMServiceSerializer(apikey,CacheKey.MonitoredMailboxes, SubServiceName.MonitoredMailbox);
				 
				if(mailbox!=null && mailbox.getMonitored()!=null && mailbox.getMonitored()){
					//mailbox is already monitored
				}else if(service==null || service.hasExceeded){
					monitored = false;
					status = 305;
				}else{
					status = 200; 
				}
				
				json.addProperty("consumedVolume", service.consumedVolume); 
				json.addProperty("accountType", service.getAccountType());
				json.addProperty("volume", service.getThreshold().getVolume());
			} 
		}else{
			monitored = false;
		} 
		
		json.addProperty("monitored", monitored);
		json.addProperty("status", status);	
		renderJSON(json.toString()); 
	}
	
	public static void orderMailboxes(@Required List<String> mailboxes){
		JsonObject json = new JsonObject(); 
		int status = 404; 
		
		if(!validation.hasErrors()){
			String apikey = Security.connectedApikey();	 
			if(apikey != null){
				status =  InboundMailbox.orderMailboxes(Security.connectedCompanyId(), Security.connectedDomain(), mailboxes);
				json.addProperty("error", (status==200?"none":"update was not successful") );
				
				//delete the list of mailboxes from the cache
				CacheKey.delListOfRepFromCache(apikey,CacheKey.MonitoredMailboxes);
			}
		}else{
			json.addProperty("error", "required fields are not specified");
		} 
		
		json.addProperty("status", status); 	
		renderJSON(json.toString());
	}
		
}
