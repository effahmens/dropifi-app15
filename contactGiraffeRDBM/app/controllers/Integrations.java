package controllers;

import static akka.actor.Actors.actorOf;

import java.util.Date;
import java.util.Map;

import job.process.EventTrackActor;
import job.process.MailingActor;
import job.process.WidgetTrackerActor;
import models.Company;
import models.CompanyProfile;
import models.EcomBlogPlugin;
import models.WidgetDefault;
import akka.actor.ActorRef;

import com.google.gson.JsonObject;

import flexjson.JSONSerializer;
import play.data.validation.Required;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.Util;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.UserType;
import resources.WidgetTrackerSerializer;
import view_serializers.EcomParameter;
import view_serializers.MailSerializer;

public class Integrations extends Controller {
	
	/**
	 * Install the widget by creating a new account
	 * @param displayName
	 * @param user_email
	 * @param user_password
	 * @param user_re_password
	 * @param user_domain
	 * @param hostUrl
	 * @param requestUrl
	 * @param accessToken
	 * @param site_url
	 * @param type
	 * @param s
	 * @param location
	 * @param ecomPluginType
	 */
	@Util
	protected static void signup(@Required String displayName,@Required String user_email,	@Required String user_password,
			@Required String user_re_password,@Required String user_domain,@Required String hostUrl, @Required String requestUrl,
			@Required String accessToken,String site_url, String type, String s,String location, EcomBlogType ecomPluginType, String phoneNumber){
		
		JsonObject json = new JsonObject();
		EcomParameter ecomParam = new EcomParameter();
		
		int status = 501;
		String msg= ""; 
		if(validation.hasErrors()){
			msg = "Some of the required fields are not specified. All the fields are required";
		}else if(!user_password.equals(user_re_password)){
			status = 406; msg = "Oops, password and re-passoword do not match"; 
		}else{
		 
			//String foundToken = !isLocal? DropifiTools.wordpressAuth(requestUrl):"";
			//(foundToken.compareTo(accessToken)==0)
			
			if(accessToken!=null){ 						
				user_domain = user_domain.trim().replace(" ", "").toLowerCase();
				//check if the email address is not associated with any account
				user_domain = Company.replaceSpecialChar(user_domain);
				if(!Company.isValidDomain(user_domain)){
					status = 308; msg= "Oops, your company name should not contain special characters such as !@#$%^&*()+=-[]\\\';,./{}|\":<>?~_ and spaces. Only letters and numbers are allowed. Eg retailtower"; 
				}else if(Company.isEmailAvailable(user_email)){
				 status = 305; msg = "Oops, the email  address "+user_email+" belongs to an existing account"; 
				}else if(EcomBlogPlugin.hasEcomBlog(user_email,hostUrl,ecomPluginType)){
					status =307; msg= "Oops, the email  address "+user_email+" belongs to an existing account";  
				}else if(Company.isDomainAvailable(user_domain)|| Company.usedDomains().contains(user_domain)){
					status =309; msg= "Oops, the company name "+user_domain+" belongs to an existing account"; 
				}else{ 
					//generate a temporarily login token							
					String temToken = DropifiTools.generateCode(12);

					//use the credentials to create an account for the user
					EcomBlogPlugin plugin = new EcomBlogPlugin(user_email,hostUrl, accessToken, temToken, ecomPluginType);
					CompanyProfile profile = new CompanyProfile(displayName, hostUrl, "","", "",phoneNumber);  
					
					//create a new account for the shop
					Company company = new Company(plugin,user_domain,user_re_password,profile);
					company = company.createCompany();
					
					if(company!=null && company.getId()!=null){
						status = 200;
						
						try{
			    			//send an initial message to the user				    			 
							company.sendWelcomeMsg(ecomPluginType); 								 
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e);
						} 
						
						try{
							WidgetTrackerSerializer track = new WidgetTrackerSerializer(company.getApikey(),site_url,site_url);
							ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
							trackActor.tell(track);  
						}catch(Exception e){
							play.Logger.log4j.error(e.getMessage(), e); 
						}
						
						try{   					
							//send an email to the shop owner
				    	    MailSerializer mail = new MailSerializer(company.getProfile().getName(), company.getEmail(),"", "Welcome to Dropifi",company.getShopifySignUpMsg());								
							ActorRef mailActor = actorOf(MailingActor.class).start();
							mailActor.tell(mail);
			    	    }catch(Exception e){
			    	    	play.Logger.log4j.error(e.getMessage(), e);
			    	    } 
						
						//add the public key to the json object to be returned to "+ecomPluginType.name();
						String publicKey = WidgetDefault.getPublicApikey(company.getApikey());
						msg="Installation of Dropifi "+ecomPluginType.name()+" plugin on your website completed successfully";
						json.addProperty("publicKey", publicKey);
						json.addProperty("userEmail", user_email); 
						 
						if(type!=null && type.equalsIgnoreCase("json")){ 
							ecomParam.setPublicKey(publicKey);
							ecomParam.setUserEmail(user_email);
							ecomParam.setTemToken(temToken);
						} 
						
						//track user signup
				        try{
				    	    EventTrackSerializer event = new EventTrackSerializer(displayName,user_email,"SIGNUP","",user_email,UserType.Admin.name(),true);
				    	    event.setWidgetType(ecomPluginType.name());
				    	    event.setCreated(new Date(company.getCreated().getTime()).toString());
				    	    event.setUrl(company.getProfile().getUrl());
				    	    event.location=location;
				    	    event.setPhone(phoneNumber);
							ActorRef eventActor = actorOf(EventTrackActor.class).start();   
							eventActor.tell(event); 
			    	    }catch(Exception e){} 
						
						json.addProperty("temToken", temToken);
						
						//check if the plugin type is prestashop
						if(company.getEcomBlogPlugin().getEb_pluginType().equals(EcomBlogType.PrestaShop)){
							company.subscribeEcomPluginToPaidPlan(company.getEcomBlogPlugin().getEb_pluginType());
						}

						//Company.updateEbSecretKey(company.getApikey(), temToken,ecomPluginType); 
					}else{
						status = 203;
						msg="Your account could not be created. The email address or company name you specified may not be valid.";
					} 					
				}			
			}else{
				//play.Logger.log4j.info("Authentication of the "+ecomPluginType.name().toLowerCase()+" plugin failed. Ensure that you have the correct version of the plugin");		
				msg="Authentication of the "+ecomPluginType.name().toLowerCase()+" plugin failed. Ensure that you have the correct version of the plugin";
			}		
		
		}
		
		if(type!=null && type.equalsIgnoreCase("json")){
			JSONSerializer fjson = new JSONSerializer();
			 
			ecomParam.setStatus(status);
			ecomParam.setMsg(msg);				 	
			renderJSON(s + '(' + fjson.serialize(ecomParam)+ ')');				 
		}else{
			returnResponse(json, status, msg);
		}
	}
	
	/**
	 * Login to dropifi dashboard to configure the widget
	 * @param temToken
	 * @param userEmail
	 * @param IPAddress
	 * @param location
	 * @param ecomPluginType
	 */
	@Util
	public static void login(@Required String temToken,@Required String userEmail,String IPAddress,String location,EcomBlogType ecomPluginType){ 
		
		if(!validation.hasErrors()){
			try{	 
				 //find the login credentials of the user
				 Map<String,String> credentials = Company.authenticate(userEmail, temToken,ecomPluginType);
				 if(credentials != null){
					String credPassword = (String)credentials.get("hashPassword"); 
						
					if(credPassword!=null && temToken!=null && temToken.compareTo(credPassword) == 0){
						 
						//Company.updateEbSecretKey(credentials.get("apikey"), "",ecomPluginType);
						credentials.put("userEmail", userEmail); 
						credentials.put("IPAddress", IPAddress!=null?IPAddress:""); 
						credentials.put("location", location!=null?location:"");
						Security.setLoginSession(credentials);
						
						//redirect the owner to the widget customization page
						redirect("/"+credentials.get("domain")+"/admin/widgets"); 
					}  					 			 
				 } 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		try {
			session.all().clear();  
			Secure.login("");
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}	 
		
	}
	
	/**
	 * Install widget by connecting to an existing account
	 * @param login_email
	 * @param accessKey
	 * @param requestUrl
	 * @param accessToken
	 * @param site_url
	 * @param type
	 * @param s
	 * @param ecomPluginType
	 */
	@Util
	public static void loginToken(@Required String login_email,@Required String accessKey,@Required String requestUrl,@Required String accessToken,String site_url,String type, String s,EcomBlogType ecomPluginType){
		JsonObject json = new JsonObject();
		String msg="Required fields are not specified";
		int status = 501;		 
		
		EcomParameter ecomParam = new EcomParameter();
		if(validation.hasErrors()){ 
			if(type!=null && type.equalsIgnoreCase("json")){ 
				JSONSerializer fjson = new JSONSerializer();
		 
				ecomParam.setStatus(status);
				ecomParam.setMsg(msg);				 	
				renderJSON(s + '(' + fjson.serialize(ecomParam)+ ')');			 
			}else{
				returnResponse(json, 501, "Required fields are not specified"); 
			} 
		}
		//String foundToken = !isLocal ? DropifiTools.wordpressAuth(requestUrl):"";
		
		if(accessToken!=null){				

			if(type!=null && type.equalsIgnoreCase("json"))
				accessKey = Codec.hexMD5(accessKey);
				
			Map<String,String> credential = Company.authenticateBlog(login_email, accessKey);			
			
			if(credential !=null ){ 
				//create a temp. access token which will be sent to "+ecomPluginType.name()+" for accessing the user data
				String temToken = DropifiTools.generateCode(12); 
				String apikey= credential.get("apikey"); 
				String pluginType =credential.get("pluginType");
				 				
				if(apikey!=null && Company.updateEbSecretKey(apikey, temToken,ecomPluginType)==200){ 
					
					json.addProperty("temToken", temToken);
					json.addProperty("publicKey", credential.get("publicKey")); 
					json.addProperty("userEmail", login_email);
					
					if(type!=null && type.equalsIgnoreCase("json")){ 
						ecomParam.setPublicKey(credential.get("publicKey"));
						ecomParam.setUserEmail(login_email);
						ecomParam.setTemToken(temToken);
					}
					
					status=200;
					msg = "Installation of Dropifi "+ecomPluginType.name()+" plugin on your website completed successfully"; 
					
					try{
						//play.Logger.log4j.info(site_url);
					    WidgetTrackerSerializer track = new WidgetTrackerSerializer(apikey,site_url,site_url); 
						ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
						trackActor.tell(track);
					}catch(Exception e){
						play.Logger.log4j.error(e.getMessage(), e);
					}
					
					
					if(ecomPluginType.equals(EcomBlogType.PrestaShop)){
						//make all prestashop account paid
						Company.subscribeEcomPluginToPaidPlan(apikey, ecomPluginType);
					}else if(pluginType.equals(EcomBlogType.PrestaShop.name())){
						//the previous account was prestashop so revert it to a free plan for other platforms
						Company.subscribeToFreePlan(apikey, ecomPluginType);
					}
				}			
			}
			
			if(status!=200)
				msg="The email address or password you entered are not correct";
			
		}else{ 
			//play.Logger.log4j.info("Authentication of the "+ecomPluginType.name()+" plugin failed. Ensure that you have the correct version of the plugin");		
			msg="Authentication of the "+ecomPluginType.name()+" plugin failed. Ensure that you have the correct version of the plugin";
		}
			 
		if(type!=null && type.equalsIgnoreCase("json")){
			JSONSerializer fjson = new JSONSerializer();
			//fjson.serialize(widget)
			ecomParam.setStatus(status);
			ecomParam.setMsg(msg); 
			renderJSON(s + '(' +  fjson.serialize(ecomParam) + ')');			 
		}else{
			returnResponse(json, status, msg);
		}
	}
	
	/**
	 * render a json object as a response to a request with default params {status:200,msg:'message'}
	 * @param json
	 * @param status
	 * @param msg
	 */
	@Util
	public static void returnResponse(JsonObject json,int status,String msg){
		json.addProperty("status", status); 
		json.addProperty("msg", msg);		
		renderJSON(json.toString()); 
	}
}
