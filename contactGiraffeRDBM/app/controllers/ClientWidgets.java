package controllers;
 
import static akka.actor.Actors.actorOf; 
import play.cache.Cache;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.libs.Images;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Util;
import play.mvc.With;
import play.mvc.results.RenderJson;
import play.utils.HTML;
import view_serializers.AttSerializer;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.FileType;
import resources.ImageUtil;
import resources.MsgSerializer;
import resources.SentimentSerializer;
import resources.WidgetRespSerializer;
import resources.WidgetTrackerSerializer; 
import resources.addons.AddonStatus;
import sun.misc.BASE64Decoder;
import view_serializers.CaptchaSerializer;
import view_serializers.ClientSerializer;
import view_serializers.ClientWidgetSerializer;
import view_serializers.MbSerializer;
import view_serializers.WCParameter;
import view_serializers.WidgetCallbackSerializer;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.*;
import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.ws.rs.core.MediaType;
import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;
import job.process.AttachmentActor;
import job.process.SentimentActor;
import job.process.WidgetActor;
import job.process.WidgetTrackerActor;
import models.Company;
import models.CompanyProfile;
import models.DropifiMessage;
import models.EcomBlogPlugin;
import models.FileDocument;
import models.GeoLocation;
import models.InboundMailbox;
import models.PreviewWidget;
import models.WiParameter;
import models.Widget;
import models.WidgetDefault;
import models.WixDefault;
import models.addons.AddonSubscription;
import adminModels.DropifiMailbox;
import adminModels.DropifiMailer;
import adminModels.MailingType;
import akka.actor.ActorRef;
import com.google.api.client.json.Json;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import flexjson.JSONSerializer;

@With(Compress.class)
public class ClientWidgets extends Controller{ 

	public static void load(){
		ForceSSL.redirectToHttps();
		String apikey =Security.connectedApikey();
		if(apikey!=null){ 
			String publicKey=null, plugin=null;
			EcomBlogPlugin ecom = Company.findEcomBlogPlugin(apikey);
			if(ecom!=null && ecom.getEb_pluginType().equals(EcomBlogType.Shopify)){
				publicKey = ecom.getEb_name(); 
				plugin = ecom.getEb_pluginType().name();
			}else{				 
				publicKey = WidgetDefault.getPublicApikey(apikey);
			}			 
			render(publicKey,plugin);
		}
		render();
	}
	
	public static void show(String apikey){
		JsonObject json = new JsonObject();
		int status = 404;
		
		if(!validation.hasErrors()){
			apikey = Security.connectedApikey();
			if( apikey !=null && !apikey.isEmpty()){
				String plugin = WidgetDefault.findHtmlCode(apikey);
				if(plugin != null){
					status =200; 				
					json.addProperty("plugin", plugin);
				}
			} 
		}
		json.addProperty("status", status);
		renderJSON(json.toString());
	}
	
	public static void unknown(){  
		render();
	}
	
	public static void show_widget_content(@Required String publicKey, @Required String buttonPosition,@Required String hostUrl,
			String pluginType,String requestUrl,String xdm_e){	
		
		String html = ""; 
		String widget_content=null;	
		String cacheKey ="";
		if(!validation.hasErrors()) {	
			
			//check if the pluginType is shopify or tictail
			if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){
				String shopName = publicKey; 
				EcomBlogType ptype = EcomBlogType.valueOf(pluginType); 
				//load data from cache 
				String sKey = CacheKey.getPluginKey(ptype,shopName);					
				publicKey = Cache.get(sKey,String.class);  
				
				if(publicKey==null){
					publicKey = Company.findPublicKey(shopName, ptype);
					Cache.set(sKey, publicKey);
				}
			}
				
			cacheKey = CacheKey.getWidgetContentKey(publicKey);
			widget_content = Cache.get(cacheKey, String.class); 
			
			/*
			if(widget_content!=null && !widget_content.contains("easyXDM.js")){ 
				widget_content = null;
			} 
			*/
			
			if(widget_content == null){
				html = WidgetDefault.findHtmlCodeByPKey(publicKey);
				widget_content = WidgetDefault.renderWidgetContent(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl);
				//update the widget cache key
				try{
					Cache.set(cacheKey, widget_content, "1440mn");
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		}
		
		if(requestUrl==null && xdm_e!=null){
			requestUrl = xdm_e;
		}
		String attachmentId = DropifiTools.generateCode(12);
		String serverURL=  DropifiTools.ServerURL;
		String strCaptcha =null;
		
		// System.out.println(CacheKey.getHasCaptcha(publicKey)+" Test");
		if(CacheKey.getHasCaptcha(publicKey)) {
			CaptchaSerializer captcha = CaptchaSerializer.getCaptch();
			setCaptcha(captcha.accessCode, captcha.code);
			strCaptcha = captcha.getJson();
		}
		
		render(widget_content,buttonPosition,pluginType,hostUrl,publicKey,requestUrl,attachmentId,serverURL,strCaptcha);
	}
	
	public static void show_widget_content_all(@Required String publicKey, @Required String buttonPosition,@Required String hostUrl,String pluginType,String requestUrl,String xdm_e){	
		 
		String html = ""; 
		String widget_content=null;	
		String cacheKey ="";
		if(!validation.hasErrors()){			
			
			//check if the pluginType is shopify or tictail
			if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){
				String shopName = publicKey; 
				EcomBlogType ptype = EcomBlogType.valueOf(pluginType); 
				//load data from cache 
				String sKey = CacheKey.getPluginKey(ptype,shopName);					
				publicKey = Cache.get(sKey,String.class);  
				
				if(publicKey==null){
					publicKey = Company.findPublicKey(shopName, ptype);
					Cache.set(sKey, publicKey);
				}
			}
				
			cacheKey = CacheKey.getWidgetContentKey(publicKey);
			widget_content = Cache.get(cacheKey, String.class); 
			
			/*
			if(widget_content!=null && !widget_content.contains("easyXDM.js")){ 
				widget_content = null;
			} 
			*/
			
			if(widget_content == null){   
				html = WidgetDefault.findHtmlCodeByPKey(publicKey);
				widget_content = WidgetDefault.renderWidgetContent(html, buttonPosition, pluginType, hostUrl, publicKey, requestUrl);

				//update the widget cache key
				try{
					Cache.set(cacheKey, widget_content, "1440mn");
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		}
		
		if(requestUrl==null && xdm_e!=null){
			requestUrl = xdm_e;
		}
		String attachmentId = DropifiTools.generateCode(12); 
		
		//implementation of captcha
		String strCaptcha =null;
		
		if(CacheKey.getHasCaptcha(publicKey)) {
			CaptchaSerializer captcha = CaptchaSerializer.getCaptch();
			setCaptcha(captcha.accessCode, captcha.code);
			strCaptcha = captcha.getJson();
		}
		
	    render(widget_content,buttonPosition,pluginType,hostUrl,publicKey,requestUrl,attachmentId,strCaptcha);
	    
	}
	
	public static void show_widget_content_preview(@Required String publicKey, @Required String buttonPosition,@Required String hostUrl,String pluginType,String requestUrl,String xdm_e){	
		
		String html =""; 
		String widget_content=null;	
		//String cacheKey ="";
		if(!validation.hasErrors()){ 
			
			//check if the pluginType is shopify or tictail
			if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){
				String shopName = publicKey;
				EcomBlogType ptype = EcomBlogType.valueOf(pluginType); 
				//load data from cache 
				//String sKey = CacheKey.getPluginKey(ptype,shopName);					
				//publicKey = Cache.get(sKey,String.class);  
				///if(publicKey==null){
				publicKey = Company.findPublicKey(shopName, ptype);
				//Cache.set(sKey, publicKey);
				//}
			}
				
			//cacheKey = CacheKey.getWidgetContentKey(publicKey);
			//widget_content = Cache.get(cacheKey, String.class); 
			
			if(widget_content == null){   
				PreviewWidget preWidget = PreviewWidget.findByApiKey(Security.connectedApikey());
				if(preWidget!=null){
					html = preWidget.getWidgetDefault(publicKey).getHtml(); 
					//html = WidgetDefault.findHtmlCodeByPKey(publicKey);
				
					widget_content = WidgetDefault.renderWidgetContent(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl);
				}
				//update the widget cache key
				try{
					//Cache.set(cacheKey, widget_content, "1440mn");
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		} 
		
		if(requestUrl==null && xdm_e!=null){
			requestUrl = xdm_e;
		}
		String attachmentId = DropifiTools.generateCode(12);
		String strCaptcha =null;
		
		if(PreviewWidget.findHasCaptch(publicKey)) {
			CaptchaSerializer captcha = CaptchaSerializer.getCaptch();
			setCaptcha(captcha.accessCode, captcha.code);
			strCaptcha = captcha.getJson();
		}
		render(widget_content,buttonPosition,pluginType,hostUrl,publicKey,requestUrl,attachmentId,strCaptcha);
	}
	
	public static void show_widget_content_popout(@Required String publicKey, @Required String buttonPosition,@Required String hostUrl,String pluginType,String requestUrl,String xdm_e, String popout,String bgcolor){
		String html =""; 
		String widget_content = null; 	
		String cacheKey ="";
		if(!validation.hasErrors()){ 
			
			//check if the pluginType is shopify or tictail
			if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){
				String shopName = publicKey; 
				EcomBlogType ptype = EcomBlogType.valueOf(pluginType); 
				//load data from cache 
				String sKey = CacheKey.getPluginKey(ptype,shopName);					
				publicKey = Cache.get(sKey,String.class);  
				
				if(publicKey==null){
					publicKey = Company.findPublicKey(shopName, ptype);
					Cache.set(sKey, publicKey);
				}
			}
			
			cacheKey = CacheKey.getWidgetContentKey(publicKey);
			widget_content = Cache.get(cacheKey, String.class); 
			
			if(widget_content!=null && widget_content.contains("easyXDM.js")){
				widget_content = null;
			}
			
			if(widget_content == null){   
				html = WidgetDefault.findHtmlCodeByPKey(publicKey);
				widget_content = WidgetDefault.renderWidgetPopoutContent(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor);
				
				//update the widget cache key
				try{
					Cache.set(cacheKey, widget_content, "1440mn");
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		}
		
		if(requestUrl==null && xdm_e!=null){
			requestUrl = xdm_e;
		}
		String attachmentId = DropifiTools.generateCode(12);
		String strCaptcha =null;
		
		if(CacheKey.getHasCaptcha(publicKey)) {
			CaptchaSerializer captcha = CaptchaSerializer.getCaptch();
			setCaptcha(captcha.accessCode, captcha.code);
			strCaptcha = captcha.getJson();
		}
		
		render(widget_content,requestUrl,bgcolor,attachmentId,strCaptcha);
	}
	
	public static void show_fileupload(String publicKey,String attTitle,String font,String attachmentId){
		if(attachmentId==null)
			attachmentId = DropifiTools.generateCode(12);
		
		render(publicKey,attTitle,font,attachmentId);
	}
	 
	/**
	 * On widget load on the client side: load the parameters for the widget button (color, position, text) : DEV MODE
	 * @param apikey
	 * @param w 
	 */
	public static void paremeters_test(@Required String apikey, @Required String hostUrl,String requestUrl, String pluginType, String w){  
			
		JSONSerializer json = new JSONSerializer(); 		
		WCParameter widget = null;
		ClientWidgetSerializer clientWidget = null; 
		String publicKey=apikey;
		try{
			if(!validation.hasErrors()) { 
				//check if the pluginType is shopify or tictail
				if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){   
					String shopName=publicKey;
					publicKey = Company.findPublicKey(shopName, EcomBlogType.valueOf(pluginType));  
				}

				if(clientWidget == null){
					PreviewWidget preWidget = PreviewWidget.findByApiKey(Security.connectedApikey());
					if(preWidget!=null){
						
						if(Security.connectedPlugin().equals(EcomBlogType.Wix.name())){
							WixDefault wix = WixDefault.findByApikey(preWidget.getApikey());
							if(wix!=null && wix.getWiParameter()!=null){
								preWidget.setWiParameter(wix.getWiParameter());
							}
							
						}
						WidgetDefault defaultWidget = preWidget.getWidgetDefault(publicKey);
						 
						if(defaultWidget.getWiParameter().getButtonFont()==null){
							defaultWidget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
						}
						
						clientWidget = new ClientWidgetSerializer(defaultWidget.getApikey(), defaultWidget.getPublicKey(), defaultWidget.getWiParameter(),defaultWidget.getSocialUID());	
					}
				}
			}
		}catch(Exception e){ 
			play.Logger.info(e.getMessage(), e);	
		}catch(Throwable t){
			play.Logger.log4j.info(t.getMessage(),t);
		}
			
		if(clientWidget != null){ 
			widget = clientWidget.getParam(); 
		}
		
		renderJSON(w + '(' + json.serialize(widget) + ')');
		 
	}
	
	public static void bk_paremeters_test(@Required String apikey, @Required String hostUrl,String requestUrl, String pluginType, String w){  
		
		JSONSerializer json = new JSONSerializer(); 		
		WCParameter widget = null;
		ClientWidgetSerializer clientWidget = null; 
		String publicKey=apikey;
		try{
			if(!validation.hasErrors()) {
				//check if the pluginType is shopify or tictail
				if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){   
					String shopName=publicKey;
					publicKey = Company.findPublicKey(shopName, EcomBlogType.valueOf(pluginType));  
				}

				if(clientWidget == null){
					WidgetDefault defaultWidget = WidgetDefault.findByPublicKey(publicKey);	
					if(defaultWidget.getWiParameter().getButtonFont()==null){
						defaultWidget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
					}
					clientWidget = new ClientWidgetSerializer(defaultWidget.getApikey(), defaultWidget.getPublicKey(), defaultWidget.getWiParameter());			
				} 
				
				/**
				//check if the user has install social review addon
				if(clientWidget!=null && clientWidget.getParam().getSocialUID()==null){ 
					String socialUID = AddonSubscription.getUID(clientWidget.getApikey(), DropifiTools.SOCIAL_REVIEW,AddonStatus.Installed,true);
					clientWidget.getParam().setSocialUID(socialUID); 
				}
				**/
			}
		}catch(Exception e){ 
			play.Logger.info(e.getMessage(), e);	
		}catch(Throwable t){
			play.Logger.log4j.info(t.getMessage(),t);
		}finally{
			
			if(clientWidget != null){ 
				widget = clientWidget.getParam(); 
			}
			
			renderJSON(w + '(' + json.serialize(widget) + ')');
		}
	}
	
	/**
	 * On widget load on the client side: load the parameters for the widget button (color, position, text) : PROD MODE
	 * @param apikey
	 * @param w 
	 */
	public static void widget_parameters(@Required String apikey, @Required String hostUrl,String requestUrl, String pluginType, String w){
		
		JSONSerializer json = new JSONSerializer();		
		WCParameter widget = null;
		ClientWidgetSerializer clientWidget = null; 
		String publicKey=apikey;
		try{
			if(!validation.hasErrors()) {  
				//check if the pluginType is shopify or tictail
				if(EcomBlogType.isNonePublicKeyPlugin(pluginType)){
					 
					EcomBlogType ptype = EcomBlogType.valueOf(pluginType); 
					//load data from cache 
					String sKey = CacheKey.getPluginKey(ptype,apikey);					
					publicKey = Cache.get(sKey,String.class);  
					
					if(publicKey == null){
						publicKey = Company.findPublicKey(apikey, ptype);
						Cache.set(sKey, publicKey);
					}
				}
					
				//load data from cache
				String cacheKey = CacheKey.getWidgetTabKey(publicKey);
				try{
					clientWidget =  Cache.get(cacheKey, ClientWidgetSerializer.class); 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
				
				if(clientWidget == null){
					WidgetDefault defaultWidget = WidgetDefault.findByPublicKey(publicKey);	
					if(defaultWidget.getWiParameter().getButtonFont()==null){
						defaultWidget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
					}
					clientWidget = new ClientWidgetSerializer(defaultWidget.getApikey(), defaultWidget.getPublicKey(), defaultWidget.getWiParameter() , defaultWidget.getSocialUID());
					CacheKey.set(cacheKey, clientWidget,"1440mn"); 
				} 
			}
		}catch(Exception e){ 
			play.Logger.info(e.getMessage(), e);	
		}catch(Throwable t){
			play.Logger.log4j.info(t.getMessage(),t);
		}finally{
			
			if(clientWidget != null){ 
				widget = clientWidget.getParam();
				if(widget!=null){
					try{
						WidgetTrackerSerializer track = new WidgetTrackerSerializer(clientWidget.getApikey(),hostUrl,requestUrl,publicKey,clientWidget.getParam());
						ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
						trackActor.tell(track);
					}catch(Exception e){
						play.Logger.log4j.info(e.getMessage(),e);
					}
					
				} 
			}
						
			renderJSON(w + '(' + json.serialize(widget) + ')');
		}
	}
	
	/**
	 * Used for receiving messages sent from the widget: IFRAME implementation  
	 */ 	
	public static void widget_iframe_sending_message(@Required String publicKey, ClientSerializer cs, String pluginType, GeoLocation location, boolean isPreview, File file){
		//json objects and serializes 
		JsonObject json = new JsonObject();
		int status = 404; 
		WidgetCallbackSerializer callback = new WidgetCallbackSerializer();  
		CaptchaSerializer captcha = CaptchaSerializer.getCaptch(); 
		if(!Validation.hasErrors()) {
			//Reset the captcha value to a different value 
			
			boolean hasCaptcha = (isPreview==true)?PreviewWidget.findHasCaptch(publicKey):CacheKey.getHasCaptcha(publicKey);
			//play.Logger.log4j.info("hasCaptcha "+ hasCaptcha+" "+cs.captchaResult+" "+publicKey);
			if(hasCaptcha){
				
				try {  
					//play.Logger.log4j.info("Testing : "+cs.captchaCode +" "+cs.captchaResult); 
					if(cs !=null && cs.captchaCode !=null && cs.captchaResult != null) {
						
						if( getCaptcha(cs.captchaCode) != cs.captchaResult) { 
							callback.setError("The answer you entered is not correct.");
							callback.setStatus(491);  
							json.add("callback", new Gson().toJsonTree(callback));
							json.add("Captcha", new Gson().toJsonTree(captcha)); 
							setCaptcha(captcha.accessCode, captcha.code);
							renderJSON(json.toString());
						}
					}else {
						callback.setError("You tried to send a message from an unauthorized source.");
						callback.setStatus(491);  
						json.add("callback", new Gson().toJsonTree(callback));
						json.add("Captcha", new Gson().toJsonTree(captcha));
						setCaptcha(captcha.accessCode, captcha.code);
						renderJSON(json.toString());
					} 
				}catch(Exception e) {
					callback.setError("The answer you entered is not correct.");
					callback.setStatus(491);  
					json.add("callback", new Gson().toJsonTree(callback));
					json.add("Captcha", new Gson().toJsonTree(captcha));
					setCaptcha(captcha.accessCode, captcha.code);
					renderJSON(json.toString());
				}
			}	
			
			try{			
				
				if(publicKey !=null && cs != null && cs.getEmail()!=null && !cs.getEmail().trim().isEmpty()){ 			
					
					if(validation.email(cs.getEmail()).ok){
						//find the connected widget using the publicKey
						Widget widget =  Widget.findByPublickey(publicKey); 
						
						if(widget!=null){
							
							//Serialize the message and submit it to AKKA actor to perform sentiment analysis
							WidgetRespSerializer resp = new WidgetRespSerializer(widget.getCompanyId(), widget.getApikey(),
									widget.getId(),	cs, widget.getDefaultEmail(),widget.getResponseSubject(),widget.getResponseMessage(),
									widget.getResponseActivated()); 									
							resp.setLocation(location);
							ActorRef widgetActor = actorOf(WidgetActor.class).start();  
							widgetActor.tell(resp);			
							status = 200;
							 
							callback.setMessage(widget.getDefaultMessage());  
						}else{
							status = 405;
							callback.setError("unknown widget settings, check your public in the widget on your site");  
						}  
					}else{
						status = 406;
						callback.setError("invalid email address format");
					} 
				}else{
					callback.setError("required fields not specified");
				}		 		
			}catch(Exception e){
				callback.setError("your message was not delivered, try sending it again");
			}
			
		}else {
			callback.setError("Required fields not specified.");  
		}
		
		//play.Logger.log4j.info(location +" of contact");
		setCaptcha(captcha.accessCode, captcha.code);
		callback.setStatus(status);  
		json.add("callback", new Gson().toJsonTree(callback));
		json.add("Captcha", new Gson().toJsonTree(captcha));
		renderJSON(json.toString());
	}
	
	@Util
	private static void widget_sending_message(@Required String publicKey, ClientSerializer cs, String pluginType, GeoLocation location,String w, String requestUrl){ 

		//json objects and serializes 
		//JsonObject json = new JsonObject();
		JSONSerializer json = new JSONSerializer();	
		WidgetCallbackSerializer callback = new WidgetCallbackSerializer();  
		int status = 404; 
		
		try{
			if(publicKey !=null && cs != null && cs.getEmail()!=null && !cs.getEmail().trim().isEmpty()){ 			
				
				if(validation.email(cs.getEmail()).ok){
					
					if(cs.getMessage()!=null && !cs.getMessage().trim().isEmpty()){
						//find the connected widget using the publicKey
						Widget widget =  Widget.findByPublickey(publicKey);
						if(widget!=null){
							
							try{
								WidgetTrackerSerializer track = new WidgetTrackerSerializer(widget.getApikey(),requestUrl,requestUrl,publicKey,widget.getWiParameter());
								ActorRef trackActor = actorOf(WidgetTrackerActor.class).start();  
								trackActor.tell(track);
							}catch(Exception e){
								play.Logger.log4j.info(e.getMessage(),e);
							}
							
							//Serialize the message and submit it to AKKA actor to perform sentiment analysis
							WidgetRespSerializer resp = new WidgetRespSerializer(widget.getCompanyId(), widget.getApikey(),
									widget.getId(),	cs, widget.getDefaultEmail(),widget.getResponseSubject(),widget.getResponseMessage(),
									widget.getResponseActivated()); 									
							resp.setLocation(location);
							ActorRef widgetActor = actorOf(WidgetActor.class).start();  
							widgetActor.tell(resp);			
							status = 200; 
							callback.setMessage(widget.getDefaultMessage());
							callback.setError("Message sent successfully");
						}else{
							status = 405;
							callback.setError("unknown widget settings, check your public in the widget on your site");  
						}  
					}else{
						status = 406;
						callback.setError("no message was type"); 
					}
				}else{
					status = 407;
					callback.setError("invalid email address format");
				} 
			}else{
				callback.setError("required fields not specified");
			}		 		
		}catch(Exception e){
			callback.setError("your message was not delivered, try sending it again");
		}
		//play.Logger.log4j.info(location +" of contact");
		callback.setStatus(status); 
		///Gson gson = new Gson();
		//json.add("callback", gson.toJsonTree(callback));
		//renderJSON(json.toString());
		renderJSON(w + '(' + json.serialize(callback) + ')');
	}
	
	public static void uploadWidgetFile(File uploadfile){
		String publicKey=null, attachmentId=null;
		JsonObject json = new JsonObject(); 
		int status = 501;
		try{ 			
			if(uploadfile!=null){
				//verify that the file extension is allowed
				if(!FileDocument.isBlockedAttachFile(uploadfile.getName())){
		    		//verify that the size of the file is allowed
					if(FileDocument.getFileSize(uploadfile).get("megabytes")<=25){
						String fileName = uploadfile.getName(); 
			    		String fileType = params.get("fileType");
			    		publicKey = params.get("publickey");
			    	    attachmentId = params.get("attachmentId"); 
			    		String sender = params.get("sender");
			    		
			    		Map<String,String> company = Company.findApikeyDomain(publicKey, true);
			    		if(company!=null){ 
			    			AttSerializer att = new AttSerializer(null, company.get("domain"), sender, null, fileName, fileType, uploadfile, null, null, attachmentId);
			    			att.apikey = company.get("apikey");
			    			att.uploadFileToRackspace();
			    		}  
					}else{
						status = 301;
					}
				}else{
					status = 305;
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
		json.addProperty("status", status); 
		renderHtml(json.toString());
	}
	
	public static void uploadScreenShot(String w){ 		
		//String publickey = request.params.get("publickey");
		//String screenfile = request.params.get("screenfile");
		//play.Logger.log4j.info(screenfile+"\n");  
		//play.Logger.log4j.info("public key: "+publickey);  
		JsonObject json = new JsonObject();
		json.addProperty("status", 201);
		json.addProperty("error", 305);
		
		renderJSON(json.toString());
		//renderJSON(w + '(' + json.toString() + ')');
		
	}
	
	@Util
	public static void setCaptcha(String key, String value) {
		session.put(key, value);
	}
	
	@Util
	public static  void setCaptcha(String key, int value) {
		session.put(key, value);
	}

	@Util
	public static int getCaptcha(String key) {
		return Integer.valueOf(session.get(key)); 
	}
	
}

