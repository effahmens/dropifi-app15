package controllers;

import controllers.Secure.Security;
import models.Company;
import play.data.validation.Email;
import play.data.validation.Required;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Util;
import play.mvc.With;
import resources.EcomBlogType;

@With(Compress.class)
public class PrestaShop extends Controller{

	public static void signup(@Required String displayName,@Required String user_email,	@Required String user_password,
			@Required String user_re_password,@Required String user_domain,@Required String hostUrl, @Required String requestUrl,
			@Required String accessToken,String site_url, String type, String s,String location,String phoneNumber){
		
		Integrations.signup(displayName, user_email, user_password, user_re_password, user_domain, hostUrl, requestUrl, accessToken, site_url, type, s, location, EcomBlogType.PrestaShop,phoneNumber);
	}
	
	public static void login(@Required String temToken,@Required String userEmail,String IPAddress,String location){ 
		Integrations.login(temToken, userEmail, IPAddress, location, EcomBlogType.PrestaShop);
	}
	
	public static void loginToken(@Required String login_email,@Required String accessKey,@Required String requestUrl,@Required String accessToken,String site_url,String type, String s){
		Integrations.loginToken(login_email, accessKey, requestUrl, accessToken, site_url, type, s, EcomBlogType.PrestaShop);
	}
	
	public static void front_office(){
		Boolean prestashop =true; 
		render(prestashop);
	}
	
	public static void back_office(){
		session.clear();
		Boolean prestashop =true;
		String useremail = Security.getCookie("rememberme", Crypto.sign("rememberlogin"));  
        if(useremail!=null){
        	flash.put("useremail", useremail);
            flash.put("remember", true); 
            flash.keep();
        }else{
        	useremail="";
        	flash.put("remember", false); 
        }   
     
		render(prestashop,useremail);
	}
	
	public static void back_login(@Required @Email String useremail, @Required String password,boolean remember,String IPAddress,String url,String location) throws Throwable{
		// Check tokens
        Integer allowed = 501;           
        String error=null;
        try {        	
        	String timezone = request.params.get("timezone");
        	
        	IPAddress =IPAddress!=null?IPAddress:"";
        	location =location!=null?location:"";
        	timezone =timezone!=null?timezone:"";
        	
        	allowed = (Integer)Security.invoke("authenticate", useremail,password,IPAddress,location,timezone);          
        } catch (Exception e ) {
        	play.Logger.log4j.info(e.getMessage(),e);
        }
        
        try{        
	        if(validation.hasErrors() || allowed!=200 ){
	            //flash.keep("url");
	            if(allowed ==201){ 
	            	error ="Oops, invalid account password: default demo password is 12345";
	            	//renderArgs.put("error", "Oops, invalid account password"); 
	            }else if(allowed==203){
	            	error ="Oops, your login credentials have been deactiavted. Contact your dropifi account manager";
	            }else{
	            	error ="Oops, there is no account associated with this email: default demo email is prestashop@dropifi.com";
	            	//renderArgs.put("error", "Oops, there is no account associated with this email"); 
	            }
	            
	            flash.error(error);
	            flash.keep();
	            
	            back_office(); 
	            //renderTemplate("Secure/login.html", url,useremail,error);
	        }
	        
	        // Remember if needed
	        if(remember) {
	        	Security.setCookie("rememberme", Crypto.sign("rememberlogin"), useremail, "15d");
	            //response.setCookie("rememberme", Crypto.sign(useremail) + "-" + useremail, "30d");
	        }else{ //or remove remember if not needed
	        	response.removeCookie("rememberme");
	        }
	        
	        if(url==null) url=""; 
	        
	        if(error==null)error="";
	        	flash.error(error);
	        //Redirect to the original URL (or /)
	        Security.invoke("onAuthenticated",url,error,"true");
        
        }catch(Exception e){
        	play.Logger.log4j.info(e.getMessage(),e); 
        	if(error==null)error="";
        		flash.error(error);
        	
        	back_office();
        	//renderTemplate("Secure/login.html", url,useremail,error="");
        }
	}
	
	/**
	 * if the previous account is prestashop, revert it to a free plan for other platforms
	 * @param apikey
	 * @param pluginType
	 */
	@Util
	protected static void revertToFree(String apikey,EcomBlogType previous,EcomBlogType pluginType){
		if(previous.equals(EcomBlogType.PrestaShop) || previous.equals(EcomBlogType.Tictail)){ 
			Company.subscribeToFreePlan(apikey,pluginType);
		}
	}
}
