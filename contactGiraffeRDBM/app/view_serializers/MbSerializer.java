package view_serializers;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

import models.InboundMailbox;
import adminModels.DropifiMailbox;
import adminModels.MailboxConfig;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

import java.util.*;

import play.data.validation.Required;
import play.data.validation.Validation;

public class MbSerializer implements Serializable {
	
	@SerializedName("id")
	private String id;
	
	@SerializedName("mailserver") 
	@Required
	private String mailserver;
	
	@SerializedName("username")
	@Required
	private String username;  
	
	@SerializedName("email")
	@Required
	private String email; 
	
	@SerializedName("password") 
	@Required
	private String password;
	
	//inbound setting
	@SerializedName("hostType")
	@Required
	private String hostType;	
	@SerializedName("host") 
	@Required
	private String host;		
	@SerializedName("port") 
	@Required
	private Integer port;	
	
	//outbound setting
	@SerializedName("outHostType") 
	@Required
	private String outHostType;
	@SerializedName("outHost") 
	@Required
	private String outHost;		
	@SerializedName("outPort") 
	@Required
	private Integer outPort;	
	
	@SerializedName("activated") 
	private Boolean activated;
	
	@SerializedName("created")
	private Timestamp created;
	@SerializedName("updated")
	private Timestamp updated;
	
	@SerializedName("monitored")
	private Boolean monitored; 
	@SerializedName("emailFilters")
	private String emailFilters;
	
	@SerializedName("authName")
	private String authName;
	/**
	 * use this constructor purposely for sending a json list to the view 
	 * @param mailbox
	 */
	public MbSerializer(InboundMailbox mailbox){
		
		if(mailbox !=null){
			Long id = mailbox.getId();
			this.setId(id!=null?id.toString():"");
			
			this.setEmail(mailbox.getEmail());
			this.setActivated(mailbox.getActivated());
			this.setMailserver(mailbox.getMailserver());
			this.setUsername(mailbox.getUsername());
			this.setCreated(mailbox.getCreated());
			this.setUpdated(mailbox.getUpdated()); 
			
			this.setMonitored(mailbox.getMonitored());
			this.setEmailFilters(mailbox.getEmailFilters());
			this.setAuthName(mailbox.getAuthName());
		}
	}
	
	public MbSerializer(InboundMailbox mailbox, boolean all){
		
		if(mailbox !=null){
			Long id = mailbox.getId();
			this.setId(id!=null?id.toString():"");			
			this.setActivated(mailbox.getActivated());
			
			//Credential
			this.setMailserver(mailbox.getMailserver());
			this.setUsername(mailbox.getUsername());
			this.setEmail(mailbox.getEmail());
			this.setPassword(mailbox.getPassword());
			
			//inbound mailbox
			this.setHostType(mailbox.getHostType());
			this.setHost(mailbox.getHost());
			this.setPort(mailbox.getPort());
			
			//outbound mailbox
			this.setOutHostType(mailbox.getOutHostType());
			this.setOutHost(mailbox.getOutHost());
			this.setOutPort(mailbox.getOutPort()); 
			
			//monitor mailbox
			this.setMonitored(mailbox.getMonitored());
			this.setEmailFilters(mailbox.getEmailFilters());
			
			this.setAuthName(mailbox.getAuthName());
		} 
	}
	
	public MbSerializer(String mailserver, String email,String username,String password,String hostType,String host,Integer port,
			String outHostType,String outHost,Integer outPort,String authName){
		
		this.setMailserver(mailserver);
		this.setEmail(email);
		this.setUsername(username);
		this.setPassword(password);
		this.setHostType(hostType);
		this.setHost(host);
		this.setPort(port);
		this.setOutHostType(outHostType );
		this.setOutHost(outHost);
		this.setOutPort(outPort);
		this.setAuthName(authName);
	}
	
	public MbSerializer(DropifiMailbox mailbox){
		
		if(mailbox !=null){ 
			this.setMailserver(mailbox.getMailserver());
			this.setEmail(mailbox.getEmail());
			this.setUsername(mailbox.getUsername()); 
			this.setPassword(mailbox.getPassword());
			this.setHostType(null);
			this.setHost(null);
			this.setPort(000);
			this.setOutHostType(mailbox.getHostType());
			this.setOutHost(mailbox.getHost());
			this.setOutPort(mailbox.getPort());
			this.setAuthName(mailbox.getAuthName());
		}
	}
	
	public MbSerializer(String email,String username){
		this.setEmail(email);
		this.setUsername(username);
	}
	
	public MbSerializer(String email,String username,String password,MailboxConfig config,boolean activated){
		this.setEmail(email);
		this.setUsername(username);
		this.setPassword(password);
		if(config != null){
			this.setMailserver(config.getMailserver());
			
			this.setHostType(config.getHostType());
			this.setOutHostType(config.getOutHostType());
			
			this.setHost(config.getHost());
			this.setOutHost (config.getOutHost()); 					
			
			this.setPort(config.getPort());
			this.setOutPort(config.getOutPort()); 				
		}
		this.setActivated(activated);
	}
	public MbSerializer(){}
	
	@Override
 	public String toString() {
		return "MbSerializer [mailserver=" + mailserver + ", username="
				+ username + ", email=" + email + ", password=" + password
				+ ", hostType=" + hostType + ", host=" + host + ", port="
				+ port + ", outHostType=" + outHostType + ", outHost="
				+ outHost + ", outPort=" + outPort + ", activated=" + activated
				+ "]";
	}
	 
	public String getMailserver() {
		return mailserver;
	}

	public void setMailserver(String mailserver) {
		this.mailserver = mailserver;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHostType() {
		return hostType;
	}

	public void setHostType(String hostType) {
		this.hostType =hostType!=null?hostType.toLowerCase(): hostType;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host!=null?host.toLowerCase():host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = isInteger(port) ? Integer.parseInt(port) : null;  
	}

	public void setPort(Integer port) {
		this.port = port;
	}
	
	public String getOutHostType() {
		return outHostType;
	}

	public void setOutHostType(String outHostType) {
		this.outHostType = outHostType!=null?outHostType.toLowerCase():outHostType;
	}

	public String getOutHost() {
		return outHost;
	}

	public void setOutHost(String outHost) {
		this.outHost = outHost!=null?outHost.toLowerCase():outHost; 
	}

	public Integer getOutPort() {
		return outPort;
	}

	public void setOutPort(Integer outPort) {
		this.outPort = outPort;
	}
	
	public void setOutPort(String outPort) {
		this.outPort = isInteger(outPort.trim()) ? Integer.parseInt(outPort.trim()) : null;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(String activated)	{
		this.activated = activated==null?false: Boolean.parseBoolean(activated);
	}

	public void setActivated(Boolean activated) {
		this.activated = activated==null?false:activated;
	}

	public InboundMailbox getInboundMailbox(){ 		
		try {
			InboundMailbox mailbox = new InboundMailbox(this.getMailserver(), this.getUsername(), this.getEmail(), this.getPassword(), 
					this.getHostType(), this.getHost(), this.getPort(),this.getOutHostType(),this.getOutHost(),this.getOutPort(), this.getActivated());
			mailbox.setMonitored(this.getMonitored());
			mailbox.setEmailFilters(this.getEmailFilters());
			return mailbox;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e); 
		}
		return null;
	}
	
	public boolean validate(){
		
		return false;
	}

	public static boolean isInteger(String str)  
	{  
	  try {  
	    int d = Integer.parseInt(str);  
	  }catch(NumberFormatException nfe){  
	    return false;  
	  }  
	  return true;  
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * convert a list of Mailbox to a serialize Mailbox list
	 * @param mailboxes
	 * @return
	 */
	public static List<MbSerializer> serializeMailbox(List<InboundMailbox> mailboxes){
		
		List<MbSerializer> serializeList = new ArrayList<MbSerializer>();
		ListIterator<InboundMailbox> mbs = mailboxes.listIterator();
		
		while(mbs.hasNext()){
			serializeList.add(new MbSerializer(mbs.next()));			
		}
		return serializeList;
	}
	
	public static JsonArray toJsonArray(List<InboundMailbox> mailboxes){
		JsonArray arrayJson = new JsonArray();
		if(mailboxes!=null){
			Gson gson = new Gson();
			ListIterator<MbSerializer> mblist = MbSerializer.serializeMailbox(mailboxes).listIterator();		
			
			while(mblist.hasNext()){
				arrayJson.add(gson.toJsonTree(mblist.next()));
			}
		}
		return arrayJson;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public boolean validateAll(){					
		return Validation.hasErrors();
	}

	public Boolean getMonitored() {
		return monitored;
	}

	public void setMonitored(Boolean monitored) {
		this.monitored = monitored;
	}

	public String getEmailFilters() {
		return emailFilters;
	}

	public void setEmailFilters(String emailFilters) {
		this.emailFilters = emailFilters;
	}

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}

}
