package view_serializers;

import java.io.Serializable;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

public class CacheJsonArray implements Serializable{	
	
	@SerializedName("jsonArray")
	private JsonArray jsonArray;
	
	public CacheJsonArray(JsonArray jsonArray){ 
		this.setJsonArray(jsonArray);
	}

	public JsonArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JsonArray jsonArray) {
		this.jsonArray = jsonArray;
	}
}
