package view_serializers;

import java.io.Serializable;

import models.WidgetControl;

import resources.WidgetControlType;
import resources.WidgetControlValidator;
import resources.WidgetField;

import com.google.gson.annotations.SerializedName;

public class ConSerializer implements Serializable{
	
	@SerializedName("condId")
	private String condId; 
	@SerializedName("title")
	private String title;
	@SerializedName("type")
	private String type;
	@SerializedName("validator")
	private String validator;
	@SerializedName("required")
	private Boolean required;
	@SerializedName("position")
	private Integer position;
	@SerializedName("activated")
	private Boolean activated;
	
	public ConSerializer(WidgetControl control){
		this.setCondId(control.getCondId().toString());
		this.setTitle(control.getTitle());
		this.setType(control.getType().toString());
		this.setValidator(control.getValidator().toString());
		this.setPosition(control.getPosition());
		this.setActivated(control.getActivated());
	}
	
	@Override
	public String toString() {
		return "ConSerializer [condId=" + condId + ", title=" + title
				+ ", type=" + type + ", validator=" + validator + ", required="
				+ required + ", position=" + position + ", activated="
				+ activated + "]";
	}

	public String getCondId() {
		return condId;
	}

	public void setCondId(String condId) {
		this.condId = condId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValidator() {
		return validator;
	}

	public void setValidator(String validator) {
		this.validator = validator;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	
	
	
}
