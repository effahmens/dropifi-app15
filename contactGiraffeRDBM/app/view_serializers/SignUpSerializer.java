package view_serializers;

import java.io.Serializable;

import play.data.validation.Required;

import com.google.gson.annotations.SerializedName;

public class SignUpSerializer implements Serializable {

	@SerializedName("username")
	private String username;
	
	@SerializedName("email")
	private String email;
	
	@SerializedName("password")
	private String password;
	
	@SerializedName("confirmPassword")
	private String confirmPassword;
	
	@SerializedName("domain")
	private String domain;

	public SignUpSerializer(String username, String email, String password,
			String confirmPassword, String domain) {
		 
		this.setUsername(username);
		this.setEmail(email);
		this.setPassword(password);
		this.setConfirmPassword(confirmPassword);
		this.setDomain(domain);
	}
	
		 
	@Override
	public String toString() {
		return "SignUpSerializer [username=" + username + ", email=" + email
				+ ", password=" + password + ", confirmPassword="
				+ confirmPassword + ", domain=" + domain + "]";
	}



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password==null?null:password.trim();
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String comfirmPassword) {
		this.confirmPassword = comfirmPassword==null?null:comfirmPassword.trim();
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	
}
