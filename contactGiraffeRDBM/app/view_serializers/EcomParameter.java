package view_serializers;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class EcomParameter implements Serializable {
	@SerializedName("status")
	private int status;
	@SerializedName("msg")
	private String msg;
	@SerializedName("temToken")
	private String temToken;
	@SerializedName("publicKey")
	private String publicKey;
	@SerializedName("userEmail")
	private String userEmail;
	
	public EcomParameter(int status, String msg, String temToken,
			String publicKey, String userEmail) {
		 
		this.status = status;
		this.msg = msg;
		this.temToken = temToken;
		this.publicKey = publicKey;
		this.userEmail = userEmail;
	}

	public EcomParameter(){
		this(501,"","","","");
	}
	
	@Override
	public String toString() {
		return "EcomParameter [status=" + status + ", msg=" + msg
				+ ", temToken=" + temToken + ", publicKey=" + publicKey
				+ ", userEmail=" + userEmail + "]";
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTemToken() {
		return temToken;
	}

	public void setTemToken(String temToken) {
		this.temToken = temToken;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	} 
	
	
	
}
