package view_serializers;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class UserRoleSerializer implements Serializable {
	
	@SerializedName("id")
	private Integer id;
	@SerializedName("name")
	private String name;
	@SerializedName("activated")
	private Boolean activated;
	
	public UserRoleSerializer(Integer id, String name, Boolean activated) {		 
		this.setId(id);
		this.setName(name);
		this.setActivated(activated);
	}
	
	public UserRoleSerializer(Integer id, String name) {		 
		this.setId(id);
		this.setName(name);
		this.setActivated(false);
	}
	
	@Override
	public String toString() {
		return "UserRole [id=" + id + ", name=" + name + ", activated="
				+ activated + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id; 
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name; 
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}
	
	
	
}
