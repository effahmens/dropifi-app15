package view_serializers;

import com.google.gson.annotations.SerializedName;
import models.WiParameter;
import models.WidgetDefault;

public class WCParameter extends WiParameter{
	
	@SerializedName("html")
	private String html;
	@SerializedName("socialUID")
	private String socialUID; 
	
	public WCParameter (WidgetDefault widget) {
		this(widget.getWiParameter());
	}
	
	public WCParameter (WiParameter wi) {
		if(wi!=null){
			this.setActivated(wi.getActivated());
			this.setButtonColor(wi.getButtonColor());
			this.setButtonText(wi.getButtonText());
			this.setButtonPercent(wi.getButtonPercent());
			this.setButtonPosition(wi.getButtonPosition());
			this.setButtonTextColor(wi.getButtonTextColor());
			this.setButtonFont(wi.getButtonFont());
			//this.setNormalText(wi.getNormalText());
			this.setImageText(wi.getImageText());
		}
	}

	@Override
	public String toString() {
		return "WCParameter [activated=" + this.getActivated() + ",html=" + html + ", buttonColor=" + this.getButtonColor()
				+ ", buttonText=" + this.getButtonText() + ", buttonTextColor="
				+ this.getButtonTextColor() + ", buttonPosition=" + this.getButtonPosition()
				+ ", buttonFont=" + this.getButtonFont()
				+ ", buttonPercent=" + this.getButtonPercent() + ", iframeHtml="
				+ html + ", normalText=" + this.getNormalText() + ", imageText="
				+ this.getImageText()+ ", socialUID=" + this.getSocialUID()+"]";
	}
	
	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getSocialUID() {
		return socialUID;
	}

	public void setSocialUID(String socialUID) {
		this.socialUID = socialUID;
	}
 
	
}
