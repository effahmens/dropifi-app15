package view_serializers;
 
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import models.InboundMailbox;
import models.QuickResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

public class QrSerializer implements Serializable {
	
	@SerializedName("id")
	private String id;
	@SerializedName("title")
	private String title; 
	@SerializedName("subject")
	private String subject;	
	@SerializedName("message")
	private String message;
	@SerializedName("activated")
	private Boolean activated;
	@SerializedName("permission")
	private Integer permission;
	@SerializedName("created")
	private Timestamp created;
	@SerializedName("updated")
	private Timestamp updated;
	
	public QrSerializer(QuickResponse quickResponse){
		this.setTitle(quickResponse.getTitle());
		this.setSubject(quickResponse.getSubject());
		this.setMessage(quickResponse.getMessage());
		this.setActivated(quickResponse.getActivated());
		this.setCreated(quickResponse.getCreated());
		this.setUpdated(quickResponse.getUpdated());
	}
	
	public QrSerializer(){
		
	}
	
	 	
	@Override
	public String toString() {
		return "QrSerializer [id=" + id + ", title=" + title + ", subject="
				+ subject + ", message=" + message + ", activated=" + activated
				+ ", permission=" + permission + ", created=" + created
				+ ", updated=" + updated + "]";
	}

	//
	/**
	 * convert a list of Mailbox to a serialize Mailbox list
	 * @param quickResponses
	 * @return
	 */
	public static List<QrSerializer> serializeQuickResponse(List<QuickResponse> quickResponses){
		
		List<QrSerializer> serializeList = new ArrayList<QrSerializer>();
		ListIterator<QuickResponse> qrs = quickResponses.listIterator();
		int n=1;
		while(qrs.hasNext()){
			QrSerializer qser = new QrSerializer(qrs.next());
			qser.setId(String.valueOf(n++)); 
			serializeList.add(qser);			
		}
		return serializeList;
	}
	
	public static JsonArray toJsonArray(List<QuickResponse> quickResponses){
		Gson gson = new Gson();
		ListIterator<QrSerializer> mblist = QrSerializer.serializeQuickResponse(quickResponses).listIterator();
		JsonArray arrayJson = new JsonArray();
		
		while(mblist.hasNext()){
			arrayJson.add(gson.toJsonTree(mblist.next())); 
		}
		
		return arrayJson;
	}
	
	//---------------------------------------------------------------------------------
	//getters and setters
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public Integer getPermission() {
		return permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}
	
	public QuickResponse getQuickResponse(){
		return new QuickResponse(title, subject, message, activated,permission);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	
}
