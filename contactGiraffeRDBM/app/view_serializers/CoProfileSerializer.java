package view_serializers;

import java.awt.Color;
import java.io.Serializable;

import resources.DropifiTools;

import com.google.gson.annotations.SerializedName;

public class CoProfileSerializer implements Serializable {
	
	@SerializedName("apikey")
	private String apikey;
	@SerializedName("companyId")
	private Long companyId;
	@SerializedName("name")
	private String name;
	@SerializedName("url")
	private String url;
	@SerializedName("username")
	private String username;
	@SerializedName("domain")
	private String domain;
	@SerializedName("email")
	private String email;
	@SerializedName("color")
	private String color;
	@SerializedName("textColor")
	private String textColor;
	@SerializedName("logoUrl") 
	private String logoUrl;
	@SerializedName("accountType") 
	private String accountType;
	@SerializedName("responseColor") 
	private String responseColor; 
	
	public CoProfileSerializer(String name, String url, String username,String domain, String email, String color,String responseColor) {
		super();
		this.setName(name);
		this.setUrl(url);
		this.setUsername(username); 
		this.setDomain(domain);
		this.setEmail(email);
		this.setColor(color);
		this.setResponseColor(responseColor);
	}
	
	public CoProfileSerializer(String apikey, long companyId){
		this.apikey = apikey;
		this.companyId = companyId;
	}
	
	public CoProfileSerializer(){} 

	
	@Override
	public String toString() {
		return "CoProfileSerializer [apikey=" + apikey + ", companyId="
				+ companyId + ", name=" + name + ", url=" + url + ", username="
				+ username + ", domain=" + domain + ", email=" + email
				+ ", color=" + color + ", textColor=" + textColor
				+ ", logoUrl=" + logoUrl + ", accountType=" + accountType
				+ ", responseColor=" + responseColor + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name!=null?name:"";
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url!=null?url:"";
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain!=null?domain:"";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username!=null?username:"";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email!=null?email:"";
	}
	
	/**
	 * provides a name which aid in providing a way for a customer to identify a company
	 * @return
	 */
	public String getMainName(){
		return !this.getUsername().isEmpty()?this.getUsername():(!this.getName().isEmpty()? this.getName() : "" );
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor =textColor; 
	}

	public void setTextColor() {
		this.textColor = DropifiTools.isBrighten(this.getResponseColor())?"#000000":"#ffffff";
	}
	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getResponseColor() {
		return responseColor;
	}

	public void setResponseColor(String responseColor) { 
		this.responseColor = (responseColor!=null && !responseColor.trim().isEmpty())?responseColor:this.getColor(); 
		this.setTextColor();
	}
	
	public void setResponseColor(Object responseColor) {
		if(responseColor!=null){
			 this.setResponseColor((String)responseColor);
		}else{
			this.responseColor = this.getColor();
			this.setTextColor();
		}
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	} 
	
}
