package view_serializers;

import java.io.Serializable;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class CaptchaSerializer implements Serializable{
	//@SerializedName("valueOne")
	public int valueOne;
	//@SerializedName("valueTwo")
	public int valueTwo;
	//@SerializedName("operand")
	public String operand; 
	public int code;
	
	//@SerializedName("accessCode")
	public String accessCode;
	
	public String query;
	 
	
	@Override
	public String toString() {
		return "CaptchaSerializer [valueOne = " + valueOne + ", valueTwo="
				+ valueTwo + ", operand=" + operand + ", code=" + code
				+ ", accessCode=" + accessCode + ", query=" + query + "]";
	}

	public CaptchaSerializer captcha(int length) {
		this.valueOne =(int) generateRandomNumbers(length);
		this.valueTwo = (int) generateRandomNumbers(length);
		int operandValue = 0; //(int)(Math.random()*2);
		
		if(operandValue==0) {
			this.code = valueOne + valueTwo; 
			this.operand="+"; 
		}else {
			this.code = valueOne - valueTwo; 
			this.operand="-";
		}
		
		this.accessCode =String.valueOf(generateRandomNumbers(12));
		this.query = this.valueOne+" "+this.operand+" "+this.valueTwo +" = ";
		return this;
	}
	
	public static CaptchaSerializer getCaptch() { 
		return new CaptchaSerializer().captcha(1);
	}
	
	public String getJson() { 
		return new Gson().toJson(this);
	}
	
	public static CaptchaSerializer getCaptch(int length) {
		return new CaptchaSerializer().captcha(length);
	}
	
	public long generateRandomNumbers(int length) {
		String nums = ""; 
        Random randomGenerator = new Random(); 
        while(true) {
        	if(nums.length()>=length) 
        		break;
        	
            nums += randomGenerator.nextInt(100); 
        }
        nums=nums.substring(0, length);
        return Long.valueOf(nums.trim());
	}
}
