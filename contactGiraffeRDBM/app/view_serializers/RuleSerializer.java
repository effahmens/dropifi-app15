package view_serializers;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import play.cache.Cache;

import models.InboundMailbox;
import models.MsgRule;
import models.QuickResponse;
import models.RuleAction;
import models.RuleCondition;
import models.WidgetRule;
import resources.CacheKey;
import resources.DropifiTools;
import resources.RuleGroup;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

import controllers.Security;
import flexjson.JSONSerializer;

public class RuleSerializer implements Serializable {
	
	@SerializedName("id")
	private int id;
	@SerializedName("name")
	private String name;
	@SerializedName(value="type")
	private String type;
	@SerializedName("activated")
	private boolean activated;
	@SerializedName("ruleGroup")	
	private RuleGroup ruleGroup;
	@SerializedName(value="ruleConditions")
	private List<RuleCondition> ruleConditions; 
	@SerializedName(value="ruleActions")
	private List<RuleAction> ruleActions;
	@SerializedName(value="created")
	private Timestamp created;
	@SerializedName(value="updated")
	private Timestamp updated;
	
	public RuleSerializer(MsgRule msgrule){
		this.setName(msgrule.getName());
		this.setRuleGroup(msgrule.getRuleGroup());
		this.setActivated(msgrule.getActivated());
		this.setRuleActions((List)msgrule.ruleActions);
		this.setRuleConditions((List)msgrule.ruleConditions); 
		this.setCreated(msgrule.getCreated());
		this.setUpdated(msgrule.getUpdated());		
	}
	
	public RuleSerializer(WidgetRule msgrule){ 
		this.setName(msgrule.getName());
		this.setRuleGroup(msgrule.getRuleGroup());
		this.setActivated(msgrule.getActivated());
		this.setRuleActions((List)msgrule.ruleActions);
		this.setRuleConditions((List)msgrule.ruleConditions); 
		this.setCreated(msgrule.getCreated());
		this.setUpdated(msgrule.getUpdated());		
	}

	public static List<RuleSerializer> serializeRule(List<MsgRule> msgRules){
		
		List<RuleSerializer> serializeList = new ArrayList<RuleSerializer>();
		ListIterator<MsgRule> qrs = msgRules.listIterator(); 
		int n=1;
		
		while(qrs.hasNext()){
			RuleSerializer qser = new RuleSerializer(qrs.next());
			qser.setId(n++); 
			serializeList.add(qser);			
		}
		return serializeList;
	}
	
	public static List<RuleSerializer> serializeWidgetRule(List<WidgetRule> msgRules){
		
		List<RuleSerializer> serializeList = new LinkedList<RuleSerializer>();
		ListIterator<WidgetRule> qrs = msgRules.listIterator(); 
		int n=1;
		
		while(qrs.hasNext()){
			RuleSerializer qser = new RuleSerializer(qrs.next());
			qser.setId(n++); 
			serializeList.add(qser);			
		}
		return serializeList;
	}

	public static JsonArray toJsonArray(List<MsgRule> msgrules){
		JsonArray arrayJson = new JsonArray();
		if(msgrules!=null){
			Gson gson = new Gson();
			ListIterator<RuleSerializer> mblist = RuleSerializer.serializeRule(msgrules).listIterator();			
			while(mblist.hasNext()){
				arrayJson.add(gson.toJsonTree(mblist.next())); 
			}
		}
		return arrayJson;
	}
	
	//check if the data exist in the cache then fetch else fetch from the database
	public static JsonArray toWidgetJsonArray(long companyId){			 
	   List<WidgetRule>msgrules = WidgetRule.findSortedRules(companyId);		   	   			
	   return toWidgetJsonArray(msgrules);
	}
	
	public static JsonArray toWidgetJsonArray(Connection conn,long companyId){
		List<WidgetRule>msgrules = WidgetRule.findAll(conn,companyId);  
		return toWidgetJsonArray(msgrules) ;
	}
	
	public static JsonArray toWidgetJsonArray( List<WidgetRule>msgrules){
		JsonArray arrayJson = new JsonArray();  		
		ListIterator<RuleSerializer> mblist = RuleSerializer.serializeWidgetRule(msgrules).listIterator();
	    Gson gson = new Gson();
	    while(mblist.hasNext()){	
		   arrayJson.add(gson.toJsonTree(mblist.next())); 
	    } 
	    return arrayJson; 
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public RuleGroup getRuleGroup() {
		return ruleGroup;
	}

	public void setRuleGroup(RuleGroup ruleGroup) {
		this.ruleGroup = ruleGroup;
	}

	public List<RuleCondition> getRuleConditions() {
		return ruleConditions;
	}

	public void setRuleConditions(List<RuleCondition> ruleConditions) {
		this.ruleConditions = new LinkedList<RuleCondition>();
		for(RuleCondition cond :ruleConditions)
			this.ruleConditions.add(cond);
	}

	public List<RuleAction> getRuleActions() {
		return ruleActions;
	}

	public void setRuleActions(List<RuleAction> ruleActions) {
		this.ruleActions = new LinkedList<RuleAction>();
		for(RuleAction action:ruleActions)
			this.ruleActions.add(action);
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

}
