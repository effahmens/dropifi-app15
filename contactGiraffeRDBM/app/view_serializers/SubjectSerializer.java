package view_serializers;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class SubjectSerializer implements Serializable{

	@SerializedName("id")
	private int id;
	@SerializedName("subTitle")
	private String subTitle;
	@SerializedName("subject")
	private String subject;
	@SerializedName("numOfMsg")
	private long numOfMsg;
	
	public SubjectSerializer(int id, String subTitle, String subject,
			long numOfMsg) {
		super();
		this.id = id;
		this.subTitle = subTitle;
		this.subject = subject;
		this.numOfMsg = numOfMsg;
	}
	
	public SubjectSerializer(){}
	
	@Override
	public String toString() {
		return "SubjectSerializer [id=" + id + ", subTitle=" + subTitle
				+ ", subject=" + subject + ", numOfMsg=" + numOfMsg + "]";
	}
 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public long getNumOfMsg() {
		return numOfMsg;
	}

	public void setNumOfMsg(long numOfMsg) {
		this.numOfMsg = numOfMsg;
	}
	
	
	
}
