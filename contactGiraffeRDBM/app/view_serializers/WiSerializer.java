package view_serializers;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class WiSerializer implements Serializable {
	
	@SerializedName("name")
	private String name;
	@SerializedName("email")
	private String email;
	@SerializedName("subject")
	private String subject;
	@SerializedName("phone")
	private String phone;
	@SerializedName("message")
	private String message;
	@SerializedName("attachment")
	private String attachment;
	
	@SerializedName("widgetheader")
	private String widgetheader;
	@SerializedName("buttonColor")
	private String buttonColor;
	@SerializedName("buttonText")
	private String buttonText;
	@SerializedName("buttonFont")
	private String buttonFont;
	@SerializedName("buttonPosition")
	private String buttonPosition;
	@SerializedName("buttonPercent")
	private Integer buttonPercent;
	@SerializedName("buttonTextColor")
	private String buttonTextColor;
	
	@SerializedName("defaultMessage")
	private String defaultMessage;
	@SerializedName("defaultEmail")
	private String defaultEmail;
	@SerializedName("plugin")
	private String plugin;
	
	@SerializedName("responseSubject")
	private String responseSubject;
	@SerializedName("responseMessage")
	private String responseMessage;
	@SerializedName("responseActivated")
	private Boolean responseActivated;
	@SerializedName("responseColor")
	private String responseColor;

	@SerializedName("sendButtonText")
	private String sendButtonText;	
	@SerializedName("sendButtonColor")
	private String sendButtonColor;
	
	@SerializedName("messageTemplate")
	private String messageTemplate; 
	@SerializedName("domain")
	private String domain;
	
	@SerializedName("actname")
	private boolean actname;
	@SerializedName("actphone")
	private boolean actphone;
	@SerializedName("actsubject")
	private boolean actsubject;
	@SerializedName("actattachment")
	private boolean actattachment;
	
	@SerializedName("hasCaptcha")
	private boolean hasCaptcha;
	@SerializedName("captchaValue")
	private String captchaValue;
	
	public WiSerializer(String name, String email, String subject,
			String phone, String message, String attachment,
			String widgetheader, String buttonColor, String buttonText,
			String buttonPosition,Integer buttonPercent, String defaultMessage, String defaultEmail,
			String plugin,String sendButtonText,String sendButtonColor,String buttonTextColor,String buttonFont,
			boolean actname,boolean actphone,boolean actsubject,boolean actattachment,boolean hasCaptcha,String captchaValue) {
		 
		this.name = name;
		this.email = email;
		this.subject = subject;
		this.phone = phone;
		this.message = message;
		this.attachment = attachment;
		this.widgetheader = widgetheader;
		this.buttonColor = buttonColor;
		this.buttonText = buttonText;
		this.buttonPosition = buttonPosition;
		this.defaultMessage = defaultMessage;
		this.defaultEmail = defaultEmail;
		this.plugin = plugin;
		this.setButtonPercent(buttonPercent);
		this.setSendButtonText(sendButtonText);
		this.setSendButtonColor(sendButtonColor);
		this.setButtonTextColor(buttonTextColor);
		this.setButtonFont(buttonFont);
		this.actname = actname;
		this.actphone =actphone;
		this.actsubject=actsubject;
		this.actattachment=actattachment;
		this.hasCaptcha =hasCaptcha;
		this.captchaValue =captchaValue;
	} 

	@Override
	public String toString() {
		return "WiSerializer [name=" + name + ", email=" + email + ", subject="
				+ subject + ", phone=" + phone + ", message=" + message
				+ ", attachment=" + attachment + ", widgetheader="
				+ widgetheader + ", buttonColor=" + buttonColor
				+ ", buttonText=" + buttonText + ", buttonFont=" + buttonFont
				+ ", buttonPosition=" + buttonPosition + ", buttonPercent="
				+ buttonPercent + ", buttonTextColor=" + buttonTextColor
				+ ", defaultMessage=" + defaultMessage + ", defaultEmail="
				+ defaultEmail + ", plugin=" + plugin + ", responseSubject="
				+ responseSubject + ", responseMessage=" + responseMessage
				+ ", responseActivated=" + responseActivated
				+ ", responseColor=" + responseColor + ", sendButtonText="
				+ sendButtonText + ", sendButtonColor=" + sendButtonColor
				+ ", messageTemplate=" + messageTemplate + ", domain=" + domain
				+ ", actname=" + actname + ", actphone=" + actphone
				+ ", actsubject=" + actsubject + ", actattachment="
				+ actattachment + ", hasCaptcha=" + hasCaptcha + ", captchaValue=" + captchaValue+ "]";
	} 

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getWidgetheader() {
		return widgetheader;
	}
	public void setWidgetheader(String widgetheader) {
		this.widgetheader = widgetheader;
	}
	public String getButtonColor() {
		return buttonColor;
	}
	public void setButtonColor(String buttonColor) {
		this.buttonColor = buttonColor;
	}
	public String getButtonText() {
		return buttonText;
	}
	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}
	public String getButtonPosition() {
		return buttonPosition;
	}
	public void setButtonPosition(String buttonPosition) {
		this.buttonPosition = buttonPosition;
	}
	public String getDefaultMessage() {
		return defaultMessage;
	}
	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}
	public String getDefaultEmail() {
		return defaultEmail;
	}
	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}
	public String getPlugin() {
		return plugin;
	}
	public void setPlugin(String plugin) {
		this.plugin = plugin;
	}
	public String getResponseSubject() {
		return responseSubject;
	}
	public void setResponseSubject(String responseSubject) {
		this.responseSubject = responseSubject;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Boolean getResponseActivated() {
		return responseActivated;
	}
	public void setResponseActivated(Boolean responseActivated) {
		this.responseActivated = responseActivated;
	}

	public Integer getButtonPercent() {
		return buttonPercent;
	}

	public void setButtonPercent(Integer buttonPercent) {
		this.buttonPercent = buttonPercent;
	}

	public String getSendButtonText() {
		return sendButtonText;
	}

	public void setSendButtonText(String sendButtonText) {
		this.sendButtonText = sendButtonText;
	}

	public String getSendButtonColor() {
		return sendButtonColor;
	}

	public void setSendButtonColor(String sendButtonColor) {
		this.sendButtonColor = sendButtonColor; 
	} 

	public String getButtonTextColor() {
		return buttonTextColor;
	} 

	public void setButtonTextColor(String buttonTextColor) {
		this.buttonTextColor = buttonTextColor;
	}


	public String getDomain() {
		return domain;
	}


	public void setDomain(String domain) {
		this.domain = domain;
	} 

	public String getButtonFont() {
		return buttonFont;
	} 

	public void setButtonFont(String buttonFont) {
		this.buttonFont = buttonFont;
	} 
	
	public String getResponseColor() {
		return responseColor;
	} 

	public void setResponseColor(String responseColor) {
		this.responseColor = responseColor;
	} 

	public String getMessageTemplate() {
		return messageTemplate;
	} 
	
	public void setMessageTemplate(String messageTemplate) {
		this.messageTemplate = messageTemplate;
	}


	public boolean isActname() {
		return actname;
	}


	public void setActname(boolean actname) {
		this.actname = actname;
	}


	public boolean isActphone() {
		return actphone;
	}


	public void setActphone(boolean actphone) {
		this.actphone = actphone;
	}


	public boolean isActsubject() {
		return actsubject;
	}


	public void setActsubject(boolean actsubject) {
		this.actsubject = actsubject;
	}


	public boolean isActattachment() {
		return actattachment;
	}


	public void setActattachment(boolean actattachment) {
		this.actattachment = actattachment;
	}


	public boolean isHasCaptcha() {
		return hasCaptcha;
	}


	public void setHasCaptcha(boolean hasCaptcha) {
		this.hasCaptcha = hasCaptcha;
	}

	public String getCaptchaValue() {
		return captchaValue;
	}

	public void setCaptchaValue(String captchaValue) {
		this.captchaValue = captchaValue;
	}


}
