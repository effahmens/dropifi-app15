package view_serializers;

import java.io.Serializable;
import java.sql.Timestamp;

import models.WMsgRecipient;

import com.google.gson.annotations.SerializedName;

public class RecipientSerializer implements Serializable{
    
	@SerializedName("id")
	private long id;
	@SerializedName("username")
	private String username;
	@SerializedName("email")
	private String email;
	@SerializedName("activated")
	private boolean activated; 
	@SerializedName("created")
	private Timestamp created;
	@SerializedName("updated")
	private Timestamp updated;
	@SerializedName("isMailboxForward")
	private Boolean isMailboxForward;
	
	public RecipientSerializer(Long id ,WMsgRecipient recipient){
		this.setId(id);
		this.setEmail(recipient.getEmail());
		this.setUsername(recipient.getUsername());
		this.setActivated(recipient.isActivated());
		this.setCreated(recipient.getCreated());
		this.setUpdated(recipient.getUpdated());
		this.setIsMailboxForward(recipient.getIsMailboxForward());
	}
	
	public RecipientSerializer(){}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setId(int id) {
		this.id = id; 
	}
	
	public void setId(String id) {
		this.id = Long.parseLong(id); 
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	
	public Timestamp getCreated() {
		return created;
	}
	

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public Boolean getIsMailboxForward() {
		return isMailboxForward;
	}

	public void setIsMailboxForward(Boolean isMailboxForward) {
		this.isMailboxForward = isMailboxForward;
	}
	
	
}
