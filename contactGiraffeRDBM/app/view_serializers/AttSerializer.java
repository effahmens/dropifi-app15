package view_serializers;

import java.io.*;
import java.net.URI;
import java.net.URL;

import play.db.jpa.Blob;
import resources.AWSS3;
import resources.DropifiTools;
import resources.FileType;
import models.FileDocument;
import api.rackspace.RackspaceClient;

import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.google.gson.annotations.SerializedName;
public class AttSerializer implements Serializable{
	
	public Long id;	
	public String domain; //main folder
	public String email; // folder2: folder of main folder
	public String msgId; //folder3: sub folder of folder2 
	public String fileName; //original filename
	public String fileType;  
	public File file;
	public InputStream input;
	public String prefix;
	public String comments;
	public String attachmentId;
	public String apikey;
	private Blob fileBlob;
	
	public AttSerializer(Long id, String domain, String email, String msgId, String fileName, String fileType, File file, String prefix,String comments,String attachmentId) { 
		this(id,domain,email,msgId,fileName,fileType,file,null,prefix,comments,attachmentId);
	}
	
	public AttSerializer(Long id, String domain, String email, String msgId, String fileName, String attachmentId) { 
		this(id,domain,email,msgId,fileName,null,null,null,null,attachmentId,attachmentId);
	}
	
	public AttSerializer(Long id, String domain, String email, String msgId, String fileName, String fileType, InputStream input,String attachmentId) {
		this(id,domain,email,msgId,fileName,fileType,null,input,null,attachmentId,attachmentId); 
	}
 
	public AttSerializer(Long id, String domain, String email, String msgId, String fileName, String fileType, File file, InputStream input,
			String prefix, String comments, String attachmentId) {
		super();
		this.id = id;
		this.domain = domain;
		this.email = email;
		this.msgId = msgId;
		this.fileName = fileName;
		this.fileType = fileType;
		this.file = file;
		this.input = input;
		this.prefix = prefix;
		this.comments = comments;
		this.attachmentId = attachmentId;
	}

	@Override
	public String toString() {
		return "AttSerializer [id=" + id + ", domain=" + domain + ", email="
				+ email + ", msgId=" + msgId + ", fileName=" + fileName
				+ ", fileType=" + fileType + ", file=" + file + ", input="
				+ input + ", prefix=" + prefix + ", comments=" + comments
				+ ", attachmentId=" + attachmentId + "]";
	}

	public String folderName(){ 
		return this.domain+"/"+this.email.replace("@", "_")+"/"+ this.attachmentId; 
	}	
	
	public String key(){ 
		return this.domain+"/"+this.email.replace("@", "_")+"/"+ this.attachmentId+"/"+this.fileName;
	} 
	
	/***
	 * 
	 * Rackspace
	 */
		
	public void uploadFileToRackspace(String BUCKETNAME){
		RackspaceClient.putPublicFile((BUCKETNAME+File.separator+this.folderName()),this.file,this.fileName);
		FileDocument doc = new FileDocument(this.apikey, this.fileName, FileType.WidgetAttachment,this.attachmentId); 
		doc.saveFileDocument();
	} 
	
	public void uploadFileToRackspace(){
		uploadFileToRackspace(DropifiTools.AWSUserFileBUCKETNAME);
	}
	
	/***
	 * AWS S3
	 ***/
	
	public void uploadFileToS3(String BUCKETNAME){  
		if(AWSS3.putFile(BUCKETNAME, this.folderName(), this.fileName, this.file) != null){
			FileDocument doc = new FileDocument(this.apikey, this.fileName, FileType.WidgetAttachment,this.attachmentId); 
			doc.saveFileDocument();
		}
	}
	
	public void uploadStreamToS3(String BUCKETNAME) throws IOException{
		if(AWSS3.putFile(BUCKETNAME, this.key(), this.input, this.fileType)!=null){
			FileDocument doc = new FileDocument(this.apikey, this.fileName, FileType.MonitoredMailbox,this.attachmentId); 
			doc.saveFileDocument();
		}
	}
	
	public void uploadFileToS3(){
		uploadFileToS3(DropifiTools.AWSUserFileBUCKETNAME);
	}
	
	public Blob getFileFromS3(String BUCKETNAME){
		return AWSS3.getBlob(BUCKETNAME, this.folderName(), this.fileName);
	}
	
	public Blob getFileFromS3(){
		return getFileFromS3(DropifiTools.AWSUserFileBUCKETNAME);
	}
	
	public Blob getFileFromCloud(){
		return RackspaceClient.getBlob(DropifiTools.AWSUserFileBUCKETNAME, this.folderName(), this.fileName); 
	}
	
	@Deprecated
	public boolean hasAttachedFile(){
		URI url= RackspaceClient.getURL(DropifiTools.AWSUserFileBUCKETNAME, this.folderName(), this.fileName);
		if(url!=null)
			return true;
		
		Blob blob = getFileFromCloud(); 
		if(blob!=null)
			return true;
		
		return false;
	}
	
	public boolean hasAttachedBlobFile(){ 
		Blob blob = getFileFromCloud();  
		if(blob!=null) {
			this.fileBlob = blob;
			return true;
		}
		return false;
	}
	
	public URI getRackspaceURI(){
		return  RackspaceClient.getURL(DropifiTools.AWSUserFileBUCKETNAME, this.folderName(), this.fileName);
	}
	public Blob getFileBlob() {
		return fileBlob;
	}

	public void setFileBlob(Blob fileBlob) {
		this.fileBlob = fileBlob;
	}
}
