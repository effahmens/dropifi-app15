package view_serializers;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class WidgetCallbackSerializer implements Serializable{
	
	@SerializedName("status")
	private Integer status;
	@SerializedName("error")
	private String error;
	@SerializedName("message")
	private String message;
	
	public WidgetCallbackSerializer(Integer status, String error, String message) {
		super();
		this.setStatus(status);
		this.setError(error);
		this.setMessage(message);
	}
	public WidgetCallbackSerializer(){
		
	}
	
	@Override
	public String toString() {
		return "WidgetCallbackSerializer [status=" + status + ", error="
				+ error + ", message=" + message + "]";
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
