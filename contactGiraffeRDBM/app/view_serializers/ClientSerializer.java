package view_serializers;

import java.io.Serializable;
import com.google.gson.annotations.SerializedName;

/**
 * Used as the object for receiving messages submitted by a user through the widget
 * @author philips
 *
 */
public class ClientSerializer implements Serializable {
	
	@SerializedName("name")
	private String name;
	@SerializedName("email")
	private String email;
	@SerializedName("phone")
	private String phone;
	@SerializedName("subject")
	private String subject;
	@SerializedName("message")
	private String message;
	@SerializedName("pageUrl")
	private String pageUrl;
	@SerializedName("attachmentId")
	private String attachmentId;
	@SerializedName("captchaCode")
	public String captchaCode;
	@SerializedName("captchaResult")
	public Integer captchaResult;
	
	public ClientSerializer(String name, String email, String phone,String subject, String message) {		
		this(name,email,phone,subject,message,null);
	}
	
	public ClientSerializer(String name, String email, String phone,String subject, String message,String attachmentId) {		
		this.setName(name);
		this.setEmail(email);
		this.setPhone(phone);
		this.setSubject(subject);
		this.setMessage(message); 
		this.setAttachmentId(attachmentId);
	}
	
	public ClientSerializer(){}
	
	@Override
	public String toString() {
		return "ClientSerializer [name=" + name + ", email=" + email
				+ ", phone=" + phone + ", subject=" + subject + ", message="
				+ message + ", pageUrl=" + pageUrl + ", attachmentId="
				+ attachmentId + ", captchaCode=" + captchaCode
				+ ", captchaResult=" + captchaResult + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone; 
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		if(subject==null)
			this.subject = "(no subject)";
		
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	} 
	
	
}
