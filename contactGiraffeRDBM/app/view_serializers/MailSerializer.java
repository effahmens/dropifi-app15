package view_serializers;

import static akka.actor.Actors.actorOf;

import java.io.Serializable;

import job.process.MailingActor;

import com.google.gson.annotations.SerializedName;

import adminModels.DropifiMailbox;
import akka.actor.ActorRef;
import api.tictail.TictailStore;

public class MailSerializer extends ClientSerializer{

	@SerializedName("mailbox")
	private  DropifiMailbox mailbox;
	
	
	public MailSerializer(String name, String email, String phone,String subject, String message) { 
		super(name, email, phone, subject, message);
		// TODO Auto-generated constructor stub
		this.resetEmail(email);
	}
 	
	public MailSerializer(){
		super();
	}

	public void resetEmail(String email){
		if(!email.contains(TictailStore.SEPARATOR))
			this.setEmail(email);
		else
			this.setEmail(email.split(TictailStore.SEPARATOR)[1]);
	}
	
	public DropifiMailbox getMailbox() {
		return mailbox;
	}
	

	public void setMailbox(DropifiMailbox mailbox) {
		this.mailbox = mailbox;
	}
	
	/**
	 * send an email using the mailing actor
	 */
	public void tellMailingActor(){
		try{
			ActorRef notifyActor = actorOf(MailingActor.class).start();
			notifyActor.tell(this);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}


}
