package view_serializers;

import java.io.Serializable;

import models.WiParameter;

import com.google.gson.annotations.SerializedName;

import view_serializers.WCParameter;

public class ClientWidgetSerializer implements Serializable {

	@SerializedName("apikey")
	private String apikey;
	@SerializedName("publickey")
	private String publickey;
	@SerializedName("param")
	private WCParameter param;
	
	public ClientWidgetSerializer(String apikey, String publickey,WiParameter param) {
		this.apikey = apikey;
		this.publickey = publickey;
		this.param = new WCParameter(param);
		this.param.setHtml("");
	}
	
	public ClientWidgetSerializer(String apikey, String publickey,WiParameter param,String socialUID) {
		this(apikey,publickey,param);
		this.param.setSocialUID(socialUID);
	}

	@Override
	public String toString() {
		return "ClientWidgetSerializer [apikey=" + apikey + ", publickey="
				+ publickey + ", param=" + param + "]";
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getPublickey() {
		return publickey;
	}

	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}

	public WCParameter getParam() {
		return param;
	}

	public void setParam(WCParameter param) {
		this.param = param;
	}
	 
}
