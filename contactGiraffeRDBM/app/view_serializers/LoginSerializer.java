package view_serializers;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class LoginSerializer implements Serializable{
	@SerializedName("email")
	private String email;
	
	@SerializedName("apikey")
	private String apikey;

	public LoginSerializer(String email, String apikey) {
		 
		this.setEmail(email);
		this.setApikey(apikey);
	}

	@Override
	public String toString() {
		return "LoginSerializer [email=" + email + ", apikey=" + apikey + "]";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	
	
}
