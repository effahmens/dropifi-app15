package query.inbox;

import java.io.Serializable;

import javax.persistence.Column;

import models.MsgCounter;

import com.google.gson.annotations.SerializedName;

public class CounterSerializer implements Serializable{
	
	@SerializedName("inbox")
	private long inbox;
	@SerializedName("sent")
	private long sent;
	@SerializedName("draft")
	private long draft;
	@SerializedName("newMsg")
	private long newMsg;
	@SerializedName("openMsg")
	private long openMsg;
	@SerializedName("pendingMsg")
	private long pendingMsg;
	@SerializedName("resolvedMsg")
	private long resolvedMsg;
	@SerializedName("positive")
	private long positive;
	@SerializedName("neutral")
	private long neutral;
	@SerializedName("negative")
	private long negative; 
	
	public CounterSerializer(long inbox, long sent, long draft, long newMsg,
			long openMsg, long pendingMsg, long resolvedMsg, long positive,
			long neutral, long negative) {
		 
		this.setInbox(inbox);
		this.setSent(sent);
		this.setDraft(draft);
		this.setNewMsg(newMsg);
		this.setOpenMsg(openMsg);
		this.setPendingMsg(pendingMsg);
		this.setResolvedMsg(resolvedMsg);
		this.setPositive(positive);
		this.setNeutral(neutral);
		this.setNegative(negative);
	}
	public CounterSerializer(MsgCounter count){
		this(count.getInbox(),count.getSent(),count.getDraft(),count.getNewMsg(),count.getOpenMsg(),count.getPendingMsg(), 
				count.getResolvedMsg(),count.getPositive(), count.getNeutral(),count.getNegative());
	}
	
	public CounterSerializer(){}

	@Override
	public String toString() {
		return "CounterSerializer [inbox=" + inbox + ", sent=" + sent
				+ ", draft=" + draft + ", newMsg=" + newMsg + ", openMsg="
				+ openMsg + ", pendingMsg=" + pendingMsg + ", resolvedMsg="
				+ resolvedMsg + ", positive=" + positive + ", neutral="
				+ neutral + ", negative=" + negative + "]";
	}

	public long getInbox() {
		return inbox;
	}

	public void setInbox(long inbox) {
		this.inbox = inbox;
	}

	public long getSent() {
		return sent;
	}

	public void setSent(long sent) {
		this.sent = sent;
	}

	public long getDraft() {
		return draft;
	}

	public void setDraft(long draft) {
		this.draft = draft;
	}

	public long getNewMsg() {
		return newMsg;
	}

	public void setNewMsg(long newMsg) {
		this.newMsg = newMsg;
	}

	public long getOpenMsg() {
		return openMsg;
	}

	public void setOpenMsg(long openMsg) {
		this.openMsg = openMsg;
	}

	public long getPendingMsg() {
		return pendingMsg;
	}

	public void setPendingMsg(long pendingMsg) {
		this.pendingMsg = pendingMsg;
	}

	public long getResolvedMsg() {
		return resolvedMsg;
	}

	public void setResolvedMsg(long resolvedMsg) {
		this.resolvedMsg = resolvedMsg;
	}

	public long getPositive() {
		return positive;
	}

	public void setPositive(long positive) {
		this.positive = positive;
	}

	public long getNeutral() {
		return neutral;
	}

	public void setNeutral(long neutral) {
		this.neutral = neutral;
	}

	public long getNegative() {
		return negative;
	}

	public void setNegative(long negative) {
		this.negative = negative;
	}
	
	 
}
