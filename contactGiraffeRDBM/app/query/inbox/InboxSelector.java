package query.inbox;

/**
 * All queries are serialize as JSONObject or JSONArray
 */

import static akka.actor.Actors.actorOf;
import helper.redis.Redis; 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.joda.time.DateTime;

import job.ConcurrentTask.MessagePullTask;
import job.process.InboxUpdateActor;
import job.process.WidgetActor;
import play.cache.Cache;
import play.db.jpa.JPA;
import play.db.jpa.JPQL;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.libs.ws.WSAsync;
import resources.AccountType;
import resources.CacheKey;
import resources.DCON;
import resources.DropifiTools;
import resources.InboxNavigator;
import resources.MsgStatus;
import resources.UserType;
import view_serializers.CoProfileSerializer;
import models.AccountUser;
import models.AgentRole;
import models.Company;
import models.CustSocialProfile;
import models.Customer;
import models.CustomerProfile;
import models.DropifiMessage;
import models.DropifiResponse;
import models.InboundMailbox;
import models.MessageManager;
import models.MsgCounter;
import models.MsgSearchKeyword;
import models.MsgSubject;
import akka.actor.ActorRef;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

public class InboxSelector {
	
	//find all messages 
	public static JsonArray findMessages(String apikey,Long companyId, String mailboxes, int  start, int end, boolean isAll,Integer timezone, boolean flag){ 
		JsonArray json = new JsonArray();
		if(companyId!=null){ 
			String jsonStr = null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, "AllMessages");
			if(!flag){
				//play.Logger.log4j.info("RD Start LoadTime: " + new Date().toString()); 
				jsonStr =  Redis.get(cacheKey);
				//play.Logger.log4j.info("RD End LoadTime: " + new Date().toString()); 
			}
			
			if(jsonStr == null || jsonStr.isEmpty()){
				//play.Logger.log4j.info("DB Start LoadTime: "+ new Date().toString()); 
				List<Object[]>  resultList =
						DropifiMessage.find("SELECT distinct d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m " +
								"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (d.companyId =?) and (d.isArchived=false) " +
								"ORDER BY d.id DESC ", companyId).fetch();
				
				//play.Logger.log4j.info("DB End LoadTime: " + new Date().toString()); 
				json = getTaskMessages(resultList,timezone);
				//play.Logger.log4j.info("PullTask End LoadTime: "+ new Date().toString() +" Timezone: "+ timezone);
			    //play.Logger.log4j.info("Loaded from Database: "+json.size());
				Redis.set(cacheKey, json);
			}else{
				//play.Logger.log4j.info("JS Start LoadTime: "+ new Date().toString() +" Timezone: "+ timezone);
				json = getJsonArray(jsonStr);
				//play.Logger.log4j.info("JS End LoadTime: "+ new Date().toString() +" Timezone: "+ timezone);
				//play.Logger.log4j.info("Loaded from Redis");
			}  
		}
		return json;
	}
	
	//find all archived messages
	public static JsonArray findArchivedMessages(Long companyId, String mailboxes, int  start, int end, boolean isAll,Integer timezone){ 
		if(companyId!=null){ 			
			List<Object[]> resultList =  
					DropifiMessage.find("SELECT distinct d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m " +
							"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (d.companyId =?) and (d.isArchived=true) " +
							"ORDER BY d.id DESC ", companyId).fetch(); 
			 			 
			return getTaskMessages(resultList,timezone);   
		}
		return new JsonArray();
	}
		
	//find all messages from a source (mailbox or widget)
	public static JsonArray findMessages(Long companyId, String source, int start, int end,Integer timezone){ 
		if(companyId!=null){
			
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT distinct d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject,  " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
							"WHERE (c.id = d.customerId) and (m.messageId = d.id) and (d.companyId =?) and (d.isArchived=false) " +
							"ORDER BY d.id DESC", companyId).fetch();
			
			return getTaskMessages(resultList,timezone);	
		}
		return new JsonArray();
	}

	//find all messages in source and either is a customer or not
	public static JsonArray findMessages(Long companyId, String source, boolean isCustomer, int start, int end,Integer timezone){ 
		 
		if(companyId!=null){
			
			List<Object[]> resultList =
					DropifiMessage.find("SELECT distinct d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
							"WHERE (c.id = d.customerId) and (m.messageId = d.id) and (c.isActive =?) and (d.companyId =?) and (d.isArchived=false) " +
							"ORDER BY d.id DESC ",isCustomer,companyId).fetch();
			
			return getTaskMessages(resultList,timezone);  		
		}
		return new JsonArray();
	}
	
	//find all messages and either is a customer or not
	public static JsonArray findMessages(String apikey,Long companyId,String mailboxes, boolean isCustomer, int start, int end, boolean isAll,Integer timezone,boolean flag){ 
		JsonArray json = new JsonArray();
		if(companyId!=null){
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, "CustomerMessages"+isCustomer);
			if(!flag){ 
				jsonStr =  Redis.get(cacheKey);
			}
			if(jsonStr == null || jsonStr.isEmpty()){ 
				List<Object[]> resultList = 
						DropifiMessage.find("SELECT distinct d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
								"WHERE (c.id = d.customerId) and (m.messageId = d.id) and (c.isActive =?) and (d.companyId =?) and (d.isArchived=false) " +
								"ORDER BY d.id DESC",isCustomer,companyId).fetch();
			
				json = getTaskMessages(resultList,timezone);
				Redis.set(cacheKey, json);
			}else{
				json = getJsonArray(jsonStr);
			} 		
		}
		return json;
	}

	//-------------------------------------------------------------------------------------------------------------------
	//sentiment messages
	
	//find all messages with sentiment
	public static JsonArray findSentimentMessages(String apikey, Long companyId,String mailboxes,String sentiment, int start, int end,boolean isAll,Integer timezone, boolean flag){ 
		JsonArray json = new JsonArray();
		if(companyId!=null){
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, sentiment+"SentiMessages");
			if(!flag){ 
				jsonStr =  Redis.get(cacheKey);  
			}
			
			if(jsonStr == null || jsonStr.isEmpty()){ 
				List<Object[]> resultList = DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject,  " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
							"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (m.articleSentiment =?) and (d.companyId = ?) and (d.isArchived=false) " + 							
							"ORDER BY d.id DESC",sentiment, companyId).fetch(); 
				 
				json = getTaskMessages(resultList,timezone);
				Redis.set(cacheKey, json);
			}else{
				json = getJsonArray(jsonStr);
			} 
		}
		return json;
	}
	
	//find all message in a source with sentiment
	public static JsonArray findSentimentMessages(Long companyId,String sentiment,String source, int start, int end,Integer timezone){ 
		 
		if(companyId!=null){
			
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true)  ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
							"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (m.articleSentiment =?) and (d.companyId =?) and (d.isArchived=false) " +
							"ORDER BY d.id DESC",sentiment, companyId).fetch();
			 
			return getTaskMessages(resultList,timezone);
		
		}
		return new JsonArray();
	}

	//
	public static JsonArray findSentimentMessages(Long companyId,String sentiment,String source,boolean isCustomer, int start, int end,Integer timezone){  
		
		if(companyId!=null){
			
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m " +
							"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (c.isActive = ?)  and (m.articleSentiment =?)  and (d.companyId =?) and (d.isArchived=false) " +
							"ORDER BY d.id DESC",isCustomer,sentiment, companyId).fetch();
			 
			return getTaskMessages(resultList,timezone); 
		}
		return new JsonArray();
	}
 
	public static JsonArray findSentimentMessages(String apikey,Long companyId,String mailboxes,String sentiment,boolean isCustomer, int start, int end,boolean isAll,Integer timezone,boolean  flag){ 
		JsonArray json = new JsonArray(); 
		if(companyId!=null){ 
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, sentiment+"SentiCustMessages"+isCustomer);
			if(!flag){ 
				jsonStr =  Redis.get(cacheKey);
			}
			if(jsonStr == null || jsonStr.isEmpty()){ 
				List<Object[]> resultList = 
						DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m " +
								"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (c.isActive = ?) and (m.articleSentiment =?) and (d.companyId =?) and (d.isArchived=false) " +
								"ORDER BY d.id DESC"
								,isCustomer,sentiment, companyId).fetch();
			 
				json = getTaskMessages(resultList,timezone);
				Redis.set(cacheKey, json);
			}else{
				json = getJsonArray(jsonStr);
			}  
		} 
		return json;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//status messages
	public static JsonArray findUnResolvedMessages(String apikey,Long companyId,Integer timezone,boolean flag){ 
		JsonArray json = new JsonArray();
		if(companyId!=null){
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, "UnResolvedMessages");
			if(!flag){ 
				jsonStr =  Redis.get(cacheKey);  
			}
			
			if(jsonStr == null || jsonStr.isEmpty()){ 
				List<Object[]> resultList = 
						DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
								"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (d.status <> ?) and (d.companyId =?)  and (d.isArchived=false) " +
								"ORDER BY d.id DESC",MsgStatus.Resolved, companyId).fetch();
				
				json = getTaskMessages(resultList,timezone);
				Redis.set(cacheKey, json);
			}else{
				json = getJsonArray(jsonStr);
			}
		}
		return json;
	}
	
	public static JsonArray findStatusMessages(String apikey,Long companyId,String mailboxes,MsgStatus status, int start, int end,Integer timezone, boolean flag){ 
		JsonArray json = new JsonArray(); 
		if(companyId!=null){
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, status.name()+"StatusMessages");
			if(!flag){ 
				jsonStr =  Redis.get(cacheKey);  
			}
			if(jsonStr == null || jsonStr.isEmpty()){ 
				List<Object[]> resultList = 
						DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
								"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (d.status =?) and (d.companyId =?) and (d.isArchived=false) " +
								"ORDER BY d.id DESC",status, companyId).fetch();
				
				json = getTaskMessages(resultList,timezone);
				Redis.set(cacheKey, json);
			}else{
				json = getJsonArray(jsonStr);
			}
		
		}
		return json;
	}
	
	public static JsonArray findStatusMessages(Long companyId,MsgStatus status,String source, int start, int end,Integer timezone){ 
		 
		if(companyId!=null){
			
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
							"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (d.status =?) and (d.companyId =?) and (d.isArchived=false) " +
							"ORDER BY d.id DESC", status, companyId).fetch();
			
			return getTaskMessages(resultList,timezone);
		
		}
		return new JsonArray();
	}
	
	public static JsonArray findStatusMessages(Long companyId,MsgStatus status,String source,boolean isCustomer, int start, int end,Integer timezone){ 
		 
		if(companyId!=null){
			
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m " +
							"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (c.isActive = ?) and (d.status =?)  and (d.companyId =?) and (d.isArchived=false) " +
							"ORDER BY d.id DESC", isCustomer, status, companyId).fetch();
			
			return getTaskMessages(resultList,timezone);
		
		}
		return new JsonArray();
	}
	
	public static JsonArray findStatusMessages(String apikey,Long companyId,String mailboxes,MsgStatus status,boolean isCustomer, int start, int end,Integer timezone,boolean flag){ 
	
		JsonArray json = new JsonArray(); 
		if(companyId!=null ){ 
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, status.name()+"StatusCustMessages"+isCustomer);
			if(!flag){
				jsonStr = Redis.get(cacheKey);
			}
			
			if(jsonStr == null || jsonStr.isEmpty()){
				List<Object[]> resultList = 
						DropifiMessage.find("SELECT distinct d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m " +
								"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (c.isActive = ?) and (d.status =?) and (d.companyId =?) and (d.isArchived=false) " +
								"ORDER BY d.id DESC", isCustomer,status, companyId).fetch();
			
				json = getTaskMessages(resultList,timezone);
				//play.Logger.log4j.info("Testing our thing: status "+status+" : "+ resultList.size());
				Redis.set(cacheKey, json);
			}else{
				json = getJsonArray(jsonStr);
			} 
		}
		return json;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	//subject messages
	public static JsonArray findSubjectMessages(String apikey,Long companyId,String subject,Integer timezone,boolean flag){ 
		JsonArray json = new JsonArray(); 
		if(companyId!=null){
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, subject+"SubjectMessages");
			if(!flag){ 
				jsonStr =  Redis.get(cacheKey);  
			}
			if(jsonStr == null || jsonStr.isEmpty()){
				subject = subject.replace('_', ' ');
				List<Object[]> resultList = 
						DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
								"WHERE (c.id = d.customerId ) and (m.messageId = d.id )  and (d.subject  = ?) and (d.companyId =?) and (d.isArchived=false) " +
								"ORDER BY d.id DESC ",  subject, companyId).fetch(); 
		 
				json = getTaskMessages(resultList,timezone);
				Redis.set(cacheKey, json);
			}else{ 
				json = getJsonArray(jsonStr);
			} 
		}
		return json;
	}
	
	public static JsonArray findSubjectMessages(String apikey,Long companyId, String subject, boolean isCustomer,Integer timezone,boolean flag){
		JsonArray json = new JsonArray(); 
		if( companyId !=null ){
			String jsonStr =null;
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, subject+"SubjectCustMessages"+isCustomer);
			if(!flag){ 
				jsonStr =  Redis.get(cacheKey);
			}
			if(jsonStr == null || jsonStr.isEmpty()){  
				subject = subject.replace('_', ' '); 
				List<Object[]> resultList =  
						DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
								"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
								"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
								"FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
								"WHERE (c.id = d.customerId ) and (m.messageId = d.id )  and (c.isActive = ?) and (d.subject  = ?) and (d.companyId = ? ) and (d.isArchived=false) " +
								"ORDER BY d.id DESC ", isCustomer, subject, companyId).fetch(); 
			
				json = getTaskMessages(resultList,timezone);
				Redis.set(cacheKey, json);
			}else{
				json = getJsonArray(jsonStr);
			} 		
		}
		return json;
	} 
			
	//------------------------------------------------------------------------------------------------------------------
	private static JsonArray getMessages(List<Object[]> resultList,Integer timezone) {
		JsonArray arrayJson = new JsonArray();
		if(resultList !=null){
			int n=0;
		 	for(Object[] fields : resultList) {
		 		arrayJson.add(getMessage(fields, n++,timezone));  
			} 
		} 
		return arrayJson;
	}
	
	private static JsonArray getTaskMessages(List<Object[]>resultList,Integer timezone){
		int size = resultList.size();
		MessagePullTask pull = new MessagePullTask(resultList, 0, size,timezone);
		pull.THRESHOLD = size/4;
		
		ForkJoinPool process = new ForkJoinPool();
		JsonArray j = process.invoke(pull); 
		process.shutdown();
		if(j.size()<1)j=null;
		return j;
	}
	
	/**
	 * The message returned is formatted with the new and open message html content. 
	 * The size of the json object grows bigger as the number of messages increases
	 * @param fields
	 * @param index
	 * @param timezone
	 * @return
	 */
	public static JsonElement getMessage( Object[] fields, int index,Integer timezone) {		 
		JsonObject json = new JsonObject();
		int i=0; 
		MessageSerializer msg = new MessageSerializer(
						(Long)fields[i++],  
						(String)fields[i++],  
						(String)fields[i++],  
						(MsgStatus)fields[i++],    
						(String)fields[i++], 
						(String)fields[i++], 						
						//(String)fields[i++],  //textBody
						(String)fields[i++],  
						(String)fields[i++],  
						(String)fields[i++],  
						(String)fields[i++],  
						(Timestamp)fields[i++],
						fields[i]!=null?(long)fields[i]:0,
						timezone
		);
		
		json.addProperty("id", msg.getId());
		json.addProperty("content", msg.getMessage(index)); 
		return json;		 
	}
	
	/**
	 * The message returned is unformatted without new and open html content
	 * The size of the json is comparatively smaller than the formatted getMessage 
	 * @param fields
	 * @param timezone
	 * @return
	 */
	public static JsonElement getMessage( Object[] fields,Integer timezone){		 
		Gson gson = new Gson();
		int i=0; 
		MessageSerializer msg = new MessageSerializer(
						(Long)fields[i++],  
						(String)fields[i++],  
						(String)fields[i++],  
						(MsgStatus)fields[i++],    
						(String)fields[i++], 
						(String)fields[i++], 						
						//(String)fields[i++],  //textBody
						(String)fields[i++],  
						(String)fields[i++],  
						(String)fields[i++],  
						(String)fields[i++],  
						(Timestamp)fields[i++],
						fields[i]!=null?(long)fields[i]:0,
						timezone
		); 
		msg.setTextBody(null);
		return gson.toJsonTree(msg);		 
	}
	
	//------------------------------------------------------------------------------------------------------------------
	//counters
	@Deprecated
	public static CounterSerializer findCounters(Long companyId,String mailboxes, boolean isAll){
		List<Object[]> resultList = MsgCounter.find("SELECT sum(m.inbox) , sum(m.sent) , sum(m.draft) , sum(m.newMsg) , sum(m.openMsg) , sum(m.pendingMsg) , " +
				"sum(m.resolvedMsg), sum(m.positive) , sum(m.neutral) , sum(m.negative) FROM MsgCounter m  WHERE m.companyId =?", companyId).query.setMaxResults(1).getResultList();
		
		return setCounters(resultList);
	}
	
	public static JsonObject findCounters(String apikey, long companyId,boolean flag){
		JsonObject json = new JsonObject();
		
		String jsonStr =null;
		String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Inbox, "InboxCounter");
		if(!flag){ 
			jsonStr =  Redis.get(cacheKey); 
		}
		
		if(jsonStr == null || jsonStr.isEmpty()){
			json.addProperty("inbox", DropifiMessage.count("byCompanyIdAndIsArchived", companyId,false));
			//json.addProperty("sent", DropifiResponse.count("byCompanyIdAndIsSentAndIsArchived", companyId,true,false));
			json.addProperty("draft", DropifiResponse.count("byCompanyIdAndIsSentAndIsArchived", companyId,false,false));
			
			Redis.set(cacheKey, json);
		}else{
			json = getJsonObject(jsonStr);
		}
		return json;
	}
	
	public static JsonArray sourceList(String apikey, String userEmail,UserType userType ) {
		JsonArray jsonArray = new JsonArray();
		Gson gson = new Gson();
		List<String> sources = AccountUser.getAssignedMailboxes(apikey, userEmail, userType);
		 
		for(String source:sources){
			jsonArray.add(gson.toJsonTree(source)); 
		}
		return jsonArray; 		 
	}
	
	public static CounterSerializer findCounters(Long companyId,String sourceOfMsg){
		MsgCounter counter = MsgCounter.find("SELECT m FROM MsgCounter m WHERE m.companyId =? ", companyId).first(); 
		if(counter !=null)
			return new CounterSerializer(counter);
		return null; 
	} 
	
	private static CounterSerializer setCounters(List<Object[]> resultList){
		if(resultList !=null){			
			for(Object[] fields : resultList) {
				int i=0;
				if(fields[0]!=null){
				CounterSerializer counter = new CounterSerializer(
						(Long)fields[i++],
						(Long)fields[i++], 
						(Long)fields[i++], 
						(Long)fields[i++], 
						(Long)fields[i++], 
						(Long)fields[i++], 
						(Long)fields[i++], 
						(Long)fields[i++], 
						(Long)fields[i++],
						(Long)fields[i++]
						);
				return counter;
				}
			}
		}
		return null;
	}
	
	private static ResultSet queryInboxCounters(Connection conn, long companyId) throws SQLException{ 		
		PreparedStatement query = conn.prepareStatement("select distinct "+  
					//"(select count(d.id) from DropifiMessage d ,MsgSentiment m where m.messageId=d.id and d.isArchived = r.isArchived and d.companyId = r.companyId) as inbox, "+ 
					"(select count(d.status) from DropifiMessage d ,MsgSentiment m where m.messageId=d.id and d.isArchived = r.isArchived and d.companyId = r.companyId and d.status =?) as new, "+ 
					"(select count(d.status) from DropifiMessage d ,MsgSentiment m where m.messageId=d.id and d.isArchived = r.isArchived and d.companyId = r.companyId and d.status =?) as open, "+
					"(select count(d.status) from DropifiMessage d ,MsgSentiment m where m.messageId=d.id and d.isArchived = r.isArchived and d.companyId = r.companyId and d.status =?) as pending, "+
					"(select count(d.status) from DropifiMessage d ,MsgSentiment m where m.messageId=d.id and d.isArchived = r.isArchived and d.companyId = r.companyId and d.status =?) as resolved, "+
					"(select count(d.status) from DropifiMessage d,MsgSentiment m where m.messageId=d.id and m.companyId = d.companyId and d.isArchived = r.isArchived and d.companyId = r.companyId and m.articleSentiment ='Positive' and d.status =?) as positive, "+
					"(select count(d.status) from DropifiMessage d,MsgSentiment m where m.messageId=d.id and m.companyId = d.companyId and d.isArchived = r.isArchived and d.companyId = r.companyId and m.articleSentiment ='Negative' and d.status =?) as negative, "+
					"(select count(d.status) from DropifiMessage d,MsgSentiment m where m.messageId=d.id and m.companyId = d.companyId and d.isArchived = r.isArchived and d.companyId = r.companyId and m.articleSentiment ='Neutral' and d.status =?) as neutral "+
					",(select count(s.isSent) from DropifiResponse s where s.isSent = true and s.companyId = r.companyId and s.isArchived=r.isArchived) as sent "+
					",(select count(s.isSent) from DropifiResponse s where s.isSent = false and s.companyId = r.companyId and s.isArchived=r.isArchived) as draft "+
					"from DropifiMessage r where r.companyId = ? and r.isArchived=?"); 
			int i =1;
			query.setInt(i++, MsgStatus.New.ordinal());
			query.setInt(i++, MsgStatus.Open.ordinal());
			query.setInt(i++, MsgStatus.Pending.ordinal());
			query.setInt(i++, MsgStatus.Resolved.ordinal());
			query.setInt(i++, MsgStatus.New.ordinal());
			query.setInt(i++, MsgStatus.New.ordinal());
			query.setInt(i++, MsgStatus.New.ordinal());
			query.setLong(i++, companyId);
			query.setBoolean(i++, false);
			
			return query.executeQuery();			
		
	}
	
	public static JsonObject findInboxCounters(long companyId){
		JsonObject json = new JsonObject();	
		Connection conn = DCON.getDefaultConnection();	
		try{
			ResultSet result = queryInboxCounters(conn,companyId);
		
 
			while(result.next()){ 				 
				json.addProperty("inbox", result.getLong("new"));
				json.addProperty("new", result.getLong("new")); 
				json.addProperty("open", result.getLong("open"));
				json.addProperty("pending", result.getLong("pending")); 
				json.addProperty("resolved", result.getLong("resolved")); 
				json.addProperty("positive", result.getLong("positive"));
				json.addProperty("negative", result.getLong("negative")); 
				json.addProperty("neutral", result.getLong("neutral"));  
				json.addProperty("sent", result.getLong("sent")); 
				json.addProperty("draft", result.getLong("draft"));  
				break;
			}
		}catch(SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(), e);
		} finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}
		return json; 
	}
	
	public static CounterSerializer findInboxCounters(long companyId,boolean type){
		CounterSerializer counter = new CounterSerializer(); 
		Connection conn = DCON.getDefaultConnection();	
		
		try {
			
		ResultSet result = queryInboxCounters(conn,companyId);
		
		
			while(result.next()){				
				counter.setInbox(result.getLong("new"));
				counter.setNewMsg(result.getLong("new")); 
				counter.setOpenMsg(result.getLong("open"));
				counter.setPendingMsg(result.getLong("pending")); 
				counter.setResolvedMsg(result.getLong("resolved")); 
				counter.setPositive(result.getLong("positive"));
				counter.setNegative(result.getLong("negative")); 
				counter.setNeutral(result.getLong("neutral"));  
				counter.setSent(result.getLong("sent"));
				counter.setDraft(result.getLong("draft")); 
				break; 
			}
		}catch(SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(), e);
		} finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}
		return counter; 
	}
	
	
	//------------------------------------------------------------------------------------------------------------------
	public static Contact_Serializer findCustomerProfile(Long companyId,String email){		 
		List<Object[]>	resultList = CustomerProfile.find("select p, c.isActive, c.phone from CustomerProfile p, Customer c " +
					"where (p.email = c.email) and (c.email = ?1) and (c.companyId = ?2)",
					email, companyId).query.setMaxResults(1).getResultList(); 
		
		Contact_Serializer customer = new Contact_Serializer(); 
		
		if(resultList !=null){
			
			for(Object[] fields : resultList) {
				int i=0;		
				
				CustomerProfile profile = (CustomerProfile)fields[i++]; 
				customer = new Contact_Serializer(profile);  
								
				//retrieve the social profile of the customer
				List<CustSocialProfile> socialList  = profile.getSocialProfile();				
				List<SocialSerializer> socials = new ArrayList<SocialSerializer>();
				
				if(socialList!=null){
					for(CustSocialProfile social: socialList){
						SocialSerializer soc =new SocialSerializer(social.getSoicalId(), social.getType(), social.getUrl());
						Integer fo = social.getFollowers();
						Integer fol = social.getFollowing();
						soc.setFollowers(fo==null?0:fo);
						soc.setFollowing(fol ==null?0:fol); 
						socials.add(soc);						
					}
				}
				
				customer.setSocials(socials);
				
				//add customer status
				customer.setIsCustomer((Boolean)fields[i++]); 	
				
				//add customer phone 
				Object phone = fields[i++];				 
				if(phone!=null){					
					customer.setPhone((String) phone); 
				}
				
				return customer;
			}
		}		
		
		return customer;
	}
	
	public static Contact_Serializer findCustomerProfile(Connection conn,Long companyId,String email){
		
		Contact_Serializer customer = new Contact_Serializer(); 
		
		try{
		CustomerProfile profile = CustomerProfile.getCustomerProfile(conn, email);
		
		if(profile!=null){
			//search for the profile of the customer
			customer = new Contact_Serializer(profile); 
			
			//search for the social contacts of the customer
			List<CustSocialProfile> socialList = CustSocialProfile.findByCusProfileId(conn,profile.getEmail());
			List<SocialSerializer> socials = new ArrayList<SocialSerializer>();
			
			if(socialList != null) {
				for(CustSocialProfile social: socialList){
					SocialSerializer soc = new SocialSerializer(social.getSoicalId(), social.getType(), social.getUrl());
					Integer fo = social.getFollowers();
					Integer fol = social.getFollowing();
					soc.setFollowers(fo==null?0:fo);
					soc.setFollowing(fol ==null?0:fol);
					socials.add(soc);						
				}
			}
			customer.setSocials(socials);
		}	
		}catch(Exception e){}
		return customer;
	}
	
	public static MessageDetailSerializer findMessage(Long companyId, Long id, String msgId, String email,Integer timezone){

		if(companyId!=null && id!=null && msgId!=null && email!=null){
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT distinct  d.id , d.msgId , d.sourceOfMsg, d.status, d.subject,d.textBody, d.htmlBody, d.created , c.email , " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
							"WHERE (c.id = d.customerId) and (m.messageId = d.id ) and (d.id =?) and (d.email=?) and (d.companyId =?) "
							,id,email, companyId).query.getResultList();
			
			
			if(resultList !=null){
				MessageDetailSerializer message = new MessageDetailSerializer();
				for(Object[] fields : resultList) {
					int i =0;
					
					message.setId((Long)fields[i++]);
					message.setMsgId((String)fields[i++]);
					message.setSource((String)fields[i++]);
					message.setStatus((MsgStatus)fields[i++]);
					message.setSubject((String)fields[i++]);
					message.setTextBody((String)fields[i++]); 
					message.setHtmlBody((String)fields[i++]);
					message.setCreated((Timestamp)fields[i++],timezone); 
					message.setEmail((String)fields[i++]);
					message.setSentiment((String)fields[i++]);
					message.setIntense((String)fields[i++]);
					message.setEmotion((String)fields[i++]); 
					message.setAttachments(DropifiMessage.retrieveAttachments(id, msgId,message.getEmail()));
					return message;
				}
			}
		}
		return null;
	}
	
	public static String findPhone(long messageId){
		Connection conn =null; 
		
		try{
			 conn = DCON.getDefaultConnection();			
			 PreparedStatement opt  = conn.prepareStatement("Select msg_opt as phone from DropifiMessage_optionals where (DropifiMessage_id =?) and (optionals_KEY=?) ");
			 opt.setLong(1, messageId);
			 opt.setString(2, "phone"); 
			 
			 ResultSet result = opt.executeQuery();
			 while(result.next()){
				 return result.getString("phone");
			 }
		}catch(Exception e){
			
		}finally{
			try{
				if(conn!=null)
				conn.close();
				}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}				 
		}
		return "";
	}
	
	public static Map<String, String> getOptionals(long companyId,String msgId,String email){
		Connection conn =null;
		try{
			conn = DCON.getDefaultConnection();
			PreparedStatement query  = conn.prepareStatement("Select d.optionals_KEY as optKey, d.msg_opt as optValue from DropifiMessage_optionals d, DropifiMessage m where d.DropifiMessage_id=m.id and m.companyId=? and m.msgId=? and m.email =?");
			query.setLong(1, companyId);
			query.setString(2, msgId);
			query.setString(3, email);
			ResultSet result = query.executeQuery();
			//,companyId,msgId,email
			Map<String, String> data = new HashMap<String, String>();
			
			while(result.next()){
				data.put( result.getString("optKey"), result.getString("optValue")); 
			}
			return data; 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}finally{
			try {
				if(conn!=null && !conn.isClosed()) 
					conn.close();
			} catch (SQLException e) {play.Logger.log4j.info(e.getMessage(),e); }
		}
		return null; 
	}
	
	
	/** 
	 * -------------------------------------------CONTACTS--------------------------------------------------------------------
	 */
	
	//All contacts u
	public static JsonArray findContacts(Long companyId,String mailboxes, int start, int end,boolean isAll) {
		List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
				//"(SELECT sum (s.numOfMsg) FROM CustomerMsgSource s WHERE s.companyId = c.companyId and (s.sourceOfMsg in ("+mailboxes+")) and s.email = c.email) " +
				"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.companyId = ?) ORDER BY p.fullName ", companyId).fetch();
		
		return  getContacts(resultList); 
	}
	
	public static List<Map<String, String>> findContacts(Long companyId) {
		List<Object[]> resultList = CustomerProfile.find("SELECT distinct  p.fullName, c.email, p.locationGeneral, c.phone, p.gender, p.age, p.image " +
				//"(SELECT sum (s.numOfMsg) FROM CustomerMsgSource s WHERE s.companyId = c.companyId and (s.sourceOfMsg in ("+mailboxes+")) and s.email = c.email) " +
				"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.companyId = ?) ORDER BY p.fullName ", companyId).fetch();
		
		List<Map<String, String>> maps = new ArrayList<Map<String,String>>(); 
		for(Object[] fields : resultList) {
			Map<String, String> map = new HashMap<String, String>();  
			
			map.put("Name",String.valueOf(fields[0]));
			map.put("Email", String.valueOf(fields[1])); 
			map.put("Country",String.valueOf(fields[2])); 
			map.put("Phone",String.valueOf(fields[3]));  
			map.put("Gender",String.valueOf(fields[4])); 
			map.put("Age",String.valueOf(fields[5]));  
			map.put("Picture", String.valueOf(fields[6]));
			
			maps.add(map);
		}
		return  maps;
	}
	
	//all contacts and source
	public static JsonArray findSourceContacts(Long companyId,String sourceOfMsg, int start, int end) {
		List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
				//"(SELECT sum(s.numOfMsg) FROM CustomerMsgSource s  WHERE s.companyId = t.companyId and s.email = t.email and s.sourceOfMsg = t.sourceOfMsg ) " +
				"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.companyId = ?) ORDER BY p.fullName ",companyId ).fetch();
		 return  getContacts(resultList); 
	}
	
	//customer or prospect only
	public static JsonArray findContacts(Long companyId,String mailboxes, boolean isCustomer, int start, int end,boolean isAll) { 
		List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
				//"(SELECT sum(s.numOfMsg) FROM CustomerMsgSource s WHERE (s.email = t.email) and s.companyId= c.companyId and (s.sourceOfMsg in ("+mailboxes+")) and s.email = c.email) " +
				"FROM CustomerProfile p , Customer c WHERE  (p.email = c.email) and (c.isActive=?) and (c.companyId = ?) ORDER BY p.fullName ",isCustomer, companyId).fetch(); 
		 return  getContacts(resultList);
	}
	
	//customer or prospect only 
	public static JsonArray findSourceContacts(Long companyId,boolean isCustomer,String sourceOfMsg, int start, int end) { 
		List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
				//"(SELECT sum(s.numOfMsg) FROM CustomerMsgSource s  WHERE s.companyId=c.companyId and s.email = t.email and  s.sourceOfMsg = t.sourceOfMsg ) " +
				"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.isActive=?) and (c.companyId = ?) ORDER BY p.fullName " ,isCustomer, companyId).fetch(); 
		 return  getContacts(resultList);
	}
	
	//all contacts like (A, B ... Y, Z) u
	public static JsonArray findContacts(Long companyId,String mailboxes,String like,int start, int end,boolean isAll) { 
		
		 if(!like.equalsIgnoreCase("All")){ 	
			 like = likeValue(like);
			 List<Object[]> resultList =CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
						//" (SELECT sum(s.numOfMsg) FROM CustomerMsgSource s  WHERE s.companyId=c.companyId and (s.sourceOfMsg in ("+mailboxes+")) and s.email = t.email) " + 
						"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.companyId = ?) and ("+like+") ORDER BY p.fullName " 
						,companyId).fetch();  
			 return  getContacts(resultList);
		 }else{
			 return findContacts(companyId,mailboxes,start,end,isAll);
		 }		
	}
	
	//all contacts like (A, B ... Y, Z) and source
	public static JsonArray findSourceContacts(Long companyId,String like,String sourceOfMsg,int start, int end) { 
		if(!like.equalsIgnoreCase("All")){	
		//like = (like+"%").toLowerCase();
		like = likeValue(like);  
		List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image" +
				//"(SELECT sum(s.numOfMsg) FROM CustomerMsgSource s  WHERE s.companyId=c.companyId and s.email = t.email and s.sourceOfMsg = t.sourceOfMsg ) " +
				"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.companyId = ?) and ("+like+") ORDER BY p.fullName " 
				 ,companyId).fetch(); 
		 	return  getContacts(resultList);
		}else{
			return findSourceContacts(companyId,sourceOfMsg,start,end); 
		}
	}
	
	//customer or prospect like (A, B ... Y, Z) u
	public static JsonArray findContacts(Long companyId,String mailboxes,String like, boolean isCustomer,int start, int end,boolean isAll) {
		if(!like.equalsIgnoreCase("All")){	
			//like = (like+"%").toLowerCase();
			like = likeValue(like);
			List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
					//"(SELECT sum(s.numOfMsg) FROM CustomerMsgSource s  WHERE s.companyId = c.companyId and (s.sourceOfMsg in ("+mailboxes+")) and s.email = t.email) " +
					"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.isActive=?) and (c.companyId = ?) and ("+like+" ) ORDER BY p.fullName"
					,isCustomer,companyId).fetch();
			 return  getContacts(resultList);
		}else{
			return findContacts(companyId,mailboxes, isCustomer, start, end,isAll);
		}
	}
	
	//customer or prospect like (A, B ... Y, Z) and sentiment
	public static JsonArray findSourceContacts(Long companyId,String like,String sourceOfMsg, boolean isCustomer,int start, int end) { 
		if(!like.equalsIgnoreCase("All")){	 
			//like = (like+"%").toLowerCase();
			like = likeValue(like); 
			List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
					//"(SELECT sum(s.numOfMsg) FROM CustomerMsgSource s  WHERE s.companyId=c.companyId and s.email = c.email and s.sourceOfMsg = t.sourceOfMsg ) " +
					" FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.isActive=?)  and (c.companyId = ?) and ("+like+")  ORDER BY p.fullName "
					 ,isCustomer,companyId,like).fetch(end); 
			 return  getContacts(resultList);
		}else{
			return findSourceContacts(companyId,isCustomer,sourceOfMsg,  start, end);
		}
	} 
	
	private static String likeValue(String like){
		String qlike="";
		if(!like.equals("0-9")){
			qlike = "LOWER(p.fullName) like '"+(like+"%").toLowerCase()+"'";  
		}else{
			char ch[] = {'0','1','2','3','4','5','6','7','8','9','?','.','-','<','>','(',')','[',']','{','}','/','\\','|','@','#','$','^','&','*','+','=','!','~','`',';',':','"',' '}; 
			
			for(char c:ch){
				qlike+="p.fullName LIKE '"+String.valueOf(c)+"%' OR "; 
			}		 
			qlike+="p.fullName LIKE ',%'";
		}
		return qlike; 
	}
	
	//------------------------------------Sentiment
	
	//all new messages contacts with sentiment	u
	public static JsonArray findContactsWithSentiment(Long companyId, String sentiment, int end, boolean isAll){ 
		 		
		List<Object[]>resultList = CustomerProfile.find("SELECT distinct c.id, c.email, p.fullName, c.isActive, p.image " +	
				//" (SELECT count(s.articleSentiment) FROM MsgSentiment s WHERE (s.email = t.email) and (s.articleSentiment = m.articleSentiment) " +
				//" and (s.sourceofMsg in ("+mailboxes+")) and (s.sourceofMsg = t.sourceOfMsg) and (s.companyId = m.companyId) ) "+				
				"FROM CustomerProfile p , Customer c , MsgSentiment m  WHERE (p.email = c.email) and (m.email=c.email) and (m.companyId= c.companyId) and (m.isArchived=false) and (m.articleSentiment = ? ) and (c.companyId = ?)" +
				"ORDER BY p.fullName",sentiment,companyId).fetch();    
		
		 return  getContacts(resultList); 
	}
	
	//all new messages contacts with sentiment	and source
	public static JsonArray findContactsLikeWithSentiment(Long companyId, String sentiment,String like,int end){  
		if(!like.equalsIgnoreCase("All")){	 
			like = likeValue(like);
			List<Object[]>resultList = CustomerProfile.find("SELECT distinct c.id, c.email, p.fullName, c.isActive, p.image " +
					" FROM CustomerProfile p , Customer c , MsgSentiment m WHERE (p.email = c.email) and (m.email=c.email) and (m.companyId = c.companyId) and (m.isArchived=false) and (m.articleSentiment = ?) " +
					" and (c.companyId = ?) and ("+like+") ORDER BY p.fullName ", sentiment, companyId).fetch(); 
			return  getContacts(resultList);
		}else{
			return findContactsWithSentiment(companyId,sentiment,end,true);
		}
	}
	
	//all new messages contacts with sentiment u
	public static JsonArray findContactsWithSentiment(Long companyId,String sentiment,boolean isCustomer, int end, boolean isAll){
		
		List<Object[]>resultList = CustomerProfile.find("SELECT distinct c.id, c.email, p.fullName, c.isActive, p.image " +	
				//" (SELECT count(s.articleSentiment) FROM MsgSentiment s WHERE (s.email = t.email) and (s.articleSentiment = m.articleSentiment) " +
				//" and (s.sourceofMsg in ("+mailboxes+")) and (s.companyId = m.companyId) )  "+				
				" FROM CustomerProfile p , Customer c , MsgSentiment m WHERE  (p.email = c.email) and (m.email=c.email) and (m.companyId= c.companyId) and (m.isArchived=false) and (c.isActive = ?) and (m.articleSentiment = ?) and (c.companyId = ?)" +
				" ORDER BY p.fullName ", isCustomer, sentiment, companyId).fetch();  
		 
		 return  getContacts(resultList); 
	}
	
	//all new messages contacts with sentiment u
	public static JsonArray findContactsLikeWithSentiment(Long companyId, String sentiment,String like,boolean isCustomer, int end) {
		if(!like.equalsIgnoreCase("All")){	 
		like = likeValue(like);
		
		List<Object[]>resultList = CustomerProfile.find("SELECT distinct c.id, c.email, p.fullName, c.isActive, p.image " +
				"FROM CustomerProfile p , Customer c , MsgSentiment m WHERE (p.email = c.email)  and (m.email = c.email) " +
				"and (m.companyId = c.companyId) and (m.isArchived=false) and (c.isActive = ?) and (m.articleSentiment = ?) " +
				"and (c.companyId = ?) and ("+like+") ORDER BY p.fullName", isCustomer, sentiment, companyId).fetch();
		 
		 return  getContacts(resultList); 
		}else{
			return findContactsWithSentiment(companyId,sentiment,isCustomer,end,true);
		}
	}
	
	//---------------------------------------------------------------------------------------------------------------------------------------
	//all contact messages
	
	public static JsonArray findContactMessages(Long companyId,String mailboxes, String email, int maxi, boolean isAll){ 
		if(companyId!=null & email!=null && !email.trim().isEmpty()){
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT d.id , d.msgId , d.sourceOfMsg, d.status, d.subject, d.htmlBody, d.created , c.email , " +
							" m.articleSentiment , m.intenseSentence , m.dominantEmotion FROM DropifiMessage d , Customer c , MsgSentiment m  " + 
							" WHERE (c.id = d.customerId) and (m.messageId = d.id ) and (d.isArchived=false)  and (d.email = ?) and (d.companyId = ?) " +
							" ORDER BY d.created DESC "  ,email, companyId).fetch(maxi); 
			
			return  getContactMessages(resultList);
		}
		return null;		
	}
	
	//all contact messages with sentiment
	public static JsonArray findContactMessagesWithSentiment(Long companyId, String mailboxes, String email, String sentiment, int maxi,boolean isAll){ 
		if(companyId!=null & email!=null && !email.trim().isEmpty()){
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT d.id , d.msgId , d.sourceOfMsg, d.status, d.subject, d.htmlBody, d.created , c.email , " +
							" m.articleSentiment , m.intenseSentence , m.dominantEmotion FROM DropifiMessage d , Customer c , MsgSentiment m " + 
							" WHERE (c.id = d.customerId) and (m.messageId = d.id ) and (d.isArchived=false) and (m.articleSentiment=?) and (d.email=?) and (d.companyId =?) " +
							" ORDER BY d.created DESC "  ,sentiment,email, companyId).fetch(maxi); 
			
			return  getContactMessages(resultList);
		}
		return null;		
	}
	
	//all contact messages and source
	public static JsonArray findContactMessages(Long companyId, String email,String sourceOfMsg, int maxi){ 
		if(companyId!=null & email!=null && !email.trim().isEmpty()){
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT d.id , d.msgId , d.sourceOfMsg, d.status, d.subject, d.htmlBody, d.created , c.email , " +
							" m.articleSentiment , m.intenseSentence , m.dominantEmotion FROM DropifiMessage d , Customer c , MsgSentiment m , CustomerMsgSource t  " + 
							" WHERE (c.id = d.customerId) and (m.messageId = d.id ) and (d.isArchived=false) and (t.email =c.email)  and (t.companyId = c.companyId) " +
							" and (t.sourceOfMsg =?) and (d.email=?) and (d.companyId =?) " +
							" ORDER BY d.created DESC "  ,email, companyId).fetch(maxi); 
			
			return  getContactMessages(resultList);
		}
		return null;		
	}
	
	//all contact messages and source
	public static JsonArray findContactMessagesWithSentiment(Long companyId, String email,String sourceOfMsg,String sentiment, int maxi){ 
		if(companyId!=null & email!=null && !email.trim().isEmpty()){
			List<Object[]> resultList = 
					DropifiMessage.find("SELECT d.id , d.msgId , d.sourceOfMsg, d.status, d.subject, d.htmlBody, d.created , c.email , " +
							" m.articleSentiment , m.intenseSentence , m.dominantEmotion FROM DropifiMessage d , Customer c , MsgSentiment m , CustomerMsgSource t  " + 
							" WHERE (c.id = d.customerId) and (m.messageId = d.id ) and (d.isArchived=false) and (t.email = c.email) and (t.sourceOfMsg = d.sourceOfMsg) and (t.companyId = c.companyId) " +
							" and (m.articleSentiment =?) and (t.sourceOfMsg =?)  and (d.email=?) and (d.companyId =?) " +
							" ORDER BY d.created DESC " ,sentiment,sourceOfMsg,email, companyId).fetch(maxi); 
			
			return  getContactMessages(resultList);
		}
		return null;		
	} 
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------
	//count of contacts with sentiments
	
	public static SentimentCounter findSentimentCounter(long companyId, Boolean isCustomer){
		if(isCustomer!=null)
			return countCustomerSenti(companyId,  isCustomer); 
		else
			return countContactSenti(companyId); 	 
	}
	
	public static SentimentCounter findSentimentLikeCounter(long companyId, Boolean isCustomer,String like){
		if(isCustomer!=null)
			return countCustomerSentiLike(companyId,  isCustomer,like); 
		else
			return countContactSentiLike(companyId,like);  	 
	}
	
	public static SentimentCounter countContactSenti(long companyId){
		Connection conn = DCON.getDefaultConnection();
		SentimentCounter counter = new SentimentCounter(0,0,0); 
		try {
			PreparedStatement query = conn.prepareStatement("select distinct "+
					 "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m "+
				      "where  m.articleSentiment='Positive' and m.companyId = c.id and (m.isArchived=false)) as positive, "+
				     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m "+
				      "where  m.articleSentiment='Negative' and m.companyId = c.id and (m.isArchived=false)) as negative , "+
				     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m "+
				     "where  m.articleSentiment='Neutral' and m.companyId = c.id and (m.isArchived=false)) as neutral "+  
					 "from Company c where c.id = ?"); 
			query.setLong(1, companyId);
			ResultSet result = query.executeQuery();
			
			while(result.next()){
				counter.setPositive(result.getLong("positive"));
				counter.setNegative(result.getLong("negative"));
				counter.setNeutral(result.getLong("neutral"));
				return counter;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.info(e.getMessage(), e);
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block				 
			}
		}
		return counter;
	}
	
	public static SentimentCounter countCustomerSenti(long companyId, boolean isCustomer){
		Connection conn = DCON.getDefaultConnection();
		SentimentCounter counter = new SentimentCounter(0,0,0); 
		try {
			PreparedStatement query = conn.prepareStatement("select distinct "+
					 "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m, Customer u "+
				      "where m.articleSentiment='Positive' and m.companyId = u.companyId "+
				      "and m.email = u.email and u.companyId = c.id and u.isActive = ?  and (m.isArchived=false)) as positive, "+
				     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m , Customer u "+
				      "where  m.articleSentiment='Negative' and m.companyId = u.companyId "+
				      "and m.email = u.email and u.companyId = c.id and u.isActive = ?  and (m.isArchived=false)) as negative , "+
				     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m , Customer u "+
				     "where  m.articleSentiment='Neutral' and m.companyId = u.companyId "+
				      "and m.email = u.email and u.companyId = c.id and u.isActive = ?  and (m.isArchived=false)) as neutral "+
					 "from Company c where c.id = ?"); 
			query.setBoolean(1, isCustomer);
			query.setBoolean(2, isCustomer);
			query.setBoolean(3, isCustomer);
			query.setLong(4, companyId);
			ResultSet result = query.executeQuery();
			
			while(result.next()){				 
				counter.setPositive(result.getLong("positive"));
				counter.setNegative(result.getLong("negative"));
				counter.setNeutral(result.getLong("neutral"));
				return counter;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(), e);
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block				 
			}
		}
		return counter;
	}

	public static SentimentCounter countContactSentiLike(long companyId,String like){
		if(!like.equalsIgnoreCase("All")){	 
			Connection conn = DCON.getDefaultConnection();
			like=likeValue(like);
			SentimentCounter counter = new SentimentCounter(0,0,0); 
			try {
				PreparedStatement query = conn.prepareStatement("select distinct "+
						 "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m, Customer u, CustomerProfile p "+
					      "where m.articleSentiment='Positive' and m.companyId = u.companyId  "+
					      "and m.email = u.email and u.companyId = c.id and u.email= p.email and ("+like+")  and (m.isArchived=false)) as positive, "+
					     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m , Customer u , CustomerProfile p "+
					      "where  m.articleSentiment='Negative' and m.companyId = u.companyId "+
					      "and m.email = u.email and u.companyId = c.id and u.email=p.email and ("+like+")  and (m.isArchived=false)) as negative , "+
					     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m , Customer u , CustomerProfile p "+
					     "where  m.articleSentiment='Neutral' and m.companyId = u.companyId "+
					      "and m.email = u.email and u.companyId = c.id and u.email=p.email and ("+like+")  and (m.isArchived=false)) as neutral "+ 
						 "from Company c where c.id = ?"); 
				 
				query.setLong(1, companyId); 
				ResultSet result = query.executeQuery();  
				
				while(result.next()){				 
					counter.setPositive(result.getLong("positive"));
					counter.setNegative(result.getLong("negative"));
					counter.setNeutral(result.getLong("neutral"));
					return counter;
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}finally{
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block				 
				}
			}
			return counter; 
		}else{
			return countContactSenti(companyId);
		}
	}
	
	public static SentimentCounter countCustomerSentiLike(long companyId, boolean isCustomer,String like) {
		if(!like.equalsIgnoreCase("All")){	 
			Connection conn = DCON.getDefaultConnection();
			SentimentCounter counter = new SentimentCounter(0,0,0);
			like=likeValue(like); 
			try {
				PreparedStatement query = conn.prepareStatement("select distinct "+
						 "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m, Customer u , CustomerProfile p "+
					      "where m.articleSentiment='Positive' and m.companyId = u.companyId "+
					      "and m.email = u.email and u.companyId = c.id and u.isActive = ? and u.email=p.email and ("+like+")  and (m.isArchived=false)) as positive, "+
					     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m , Customer u , CustomerProfile p "+
					      "where  m.articleSentiment='Negative' and m.companyId = u.companyId "+
					      "and m.email = u.email and u.companyId = c.id and u.isActive = ? and u.email=p.email and ("+like+")and u.email=p.email and ("+like+")  and (m.isArchived=false)) as negative , "+
					     "(select count(distinct m.companyId, m.email, m.articleSentiment) from MsgSentiment m , Customer u , CustomerProfile p "+
					     "where  m.articleSentiment='Neutral' and m.companyId = u.companyId "+
					      "and m.email = u.email and u.companyId = c.id and u.isActive = ? and u.email=p.email and ("+like+")  and (m.isArchived=false)) as neutral "+
						 "from Company c where c.id = ?");
				query.setBoolean(1, isCustomer); 
				query.setBoolean(2, isCustomer);
				query.setBoolean(3, isCustomer);
				query.setLong(4, companyId); 
				ResultSet result = query.executeQuery(); 
				
				while(result.next()){				 
					counter.setPositive(result.getLong("positive"));
					counter.setNegative(result.getLong("negative")); 
					counter.setNeutral(result.getLong("neutral"));
					return counter;
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}finally{
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block 				 
				}
			}
			return counter;
		}else{
			return countCustomerSenti(companyId,isCustomer);
		}
	}
	
	public static JsonArray subjectCounter(Long companyId){
		Connection conn = DCON.getDefaultConnection();	
		
		JsonArray jsonArray = new JsonArray();
		try {
			String subjects = MessageManager.queryMsgSubjects(companyId);
			if(subjects!=null){
				PreparedStatement query = conn.prepareStatement("select distinct d.subject as subject, " +
						"(select count(w.subject) from DropifiMessage w, MsgSentiment m where m.messageId= d.id and (w.subject = d.subject)  and (w.companyId = d.companyId) and (w.isArchived=false) ) as numOfMsg " +
						"from DropifiMessage d where (d.subject in ("+subjects+") ) and d.isArchived=false and d.companyId = ? ");
				 
				query.setLong(1, companyId);
				ResultSet result = query.executeQuery();
				
				jsonArray = setSubjectCounter(result);
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.info(e.getMessage(), e);			 
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block 
				play.Logger.info(e.getMessage(), e);
			}
		}		
		return jsonArray;
	}
	
	public static JsonArray subjectCounter(Long companyId,boolean isCustomer) {
		Connection conn = DCON.getDefaultConnection();  
		JsonArray jsonArray = new JsonArray();
		try {
			
			String subjects = MessageManager.queryMsgSubjects(companyId);
			
			PreparedStatement query = conn.prepareStatement("select distinct d.subject,(select count(w.subject) from DropifiMessage w , Customer u " +
					"where (u.companyId = w.companyId) and (u.isActive = c.isActive) and (w.isArchived=false) and(w.sourceOfMsg = d.sourceOfMsg) and (w.subject = d.subject) and  " +
					"(w.companyId = d.companyId) ) as numOfMsg from DropifiMessage d, Customer c where (c.companyId = d.companyId)  and (c.isActive = ?) " +
					"and d.sourceOfMsg = 'Widget Message' and (d.subject in ("+subjects+")  and (d.isArchived=false)) and (d.companyId = ?) "); 
			
			//query.setInt(1, MsgStatus.New.ordinal()); 
			query.setBoolean(1, isCustomer);
			query.setLong(2, companyId);
			ResultSet result = query.executeQuery();
			play.Logger.info("here: "+ subjects);
			jsonArray = setSubjectCounter(result);
			
		}catch (SQLException e){
			// TODO Auto-generated catch block
			play.Logger.info(e.getMessage(), e);
		}finally{
			try { 
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block 
				play.Logger.info(e.getMessage(), e);
			}
		}		
		return jsonArray;
	}
		
	private static JsonArray setSubjectCounter(ResultSet result){		
		JsonArray jsonArray = new JsonArray();
		try {
			int id =0;
			while(result.next()){
				JsonObject json = new JsonObject();
				String subject = result.getString("subject");
				String subTitle = subject;
				if(subject != null)
					subject = subject.trim().replace(' ', '_');
				
				json.addProperty("id",id++);
				json.addProperty("subTitle",subTitle);
				json.addProperty("subject",subject);
				json.addProperty("numOfMsg",result.getLong("numOfMsg"));
				jsonArray.add(json); 
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.info(e.getMessage(), e);
		}
		return jsonArray;
	}
	
	//------------------------------------------------------------------------------------------------------------------
	//
	private static JsonArray getContacts(List<Object[]> resultList){
		JsonArray jsonArray = new JsonArray();
		JsonArray noNameArray = new JsonArray(); //create list of all contact without name
		if(resultList !=null){
			
			Gson gson = new Gson();
		 	for(Object[] fields : resultList) {
		 		int i=0; 
		 		Contact_Serializer contact = new Contact_Serializer( 
		 				(long)fields[i++],
						(String)fields[i++],
						(String)fields[i++],
						(boolean)fields[i++],
						(String)fields[i++] 
				);
		 		
		 	 
	 			//int n = i++;
	 			long numOfMsg = 0;//fields[n] !=null?(long)fields[n]:0;
		 		contact.setNumOfMsg(numOfMsg);		 	 
		 		
		 		if(contact.getFullName()!=null && !contact.getFullName().trim().isEmpty())
		 			jsonArray.add(gson.toJsonTree(contact));
		 		else
		 			noNameArray.add(gson.toJsonTree(contact));
			}
		 	if(noNameArray.size()>0) //add the contacts without name to the contacts with name
		 		jsonArray.addAll(noNameArray); 
		} 
		return jsonArray;
	}
	
	public static JsonArray getContactMessages(List<Object[]> resultList){
		JsonArray jsonArray = new JsonArray();
		if(resultList !=null){			
			Gson gson = new Gson();
			MessageDetailSerializer message;
			for(Object[] fields : resultList) {
				int i =0;
				message = new MessageDetailSerializer();
				message.setId((Long)fields[i++]);
				message.setMsgId((String)fields[i++]);
				message.setSource((String)fields[i++]);
				message.setStatus((MsgStatus)fields[i++]);
				message.setSubject((String)fields[i++]);
				message.setHtmlBody((String)fields[i++]);
				message.setCreated((Timestamp)fields[i++]);
				message.setEmail((String)fields[i++]);
				message.setSentiment((String)fields[i++]);
				message.setIntense((String)fields[i++]);
				message.setEmotion((String)fields[i++]);
				
				jsonArray.add(gson.toJsonTree(message));				 
			} 
		}
		
		return jsonArray;
	}

	
	//------------------------------------------------------------------------------------------------------------------
	//sent and drafts
	
	//count messages sent and drafted
	public static JsonObject countSents(String apikey, long companyId){ 
		JsonObject json = new JsonObject(); 
		json.addProperty("sent", DropifiResponse.count("byCompanyIdAndIsSentAndIsArchived", companyId,true,false));
		json.addProperty("draft", DropifiResponse.count("byCompanyIdAndIsSentAndIsArchived", companyId,false,false)); 
		return json;
	}
	
	public static JsonObject countSents(String apikey, long companyId,String msgId){ 
		JsonObject json = new JsonObject();
		json.addProperty("sent", DropifiResponse.count("byCompanyIdAndIsSentAndIsArchived", companyId,true,false));
		json.addProperty("draft", DropifiResponse.count("byCompanyIdAndIsSentAndIsArchived", companyId,false,false));
		json.addProperty("replies", DropifiResponse.count("byCompanyIdAndIsSentAndIsArchivedAndMsgId", companyId,true,false,msgId));
		return json;
	}
	
	@Deprecated
	public static JsonObject countSentsOLd(String apikey){ 
		JsonObject json = new JsonObject(); 
		
		Connection conn = DCON.getDefaultConnection();			
		PreparedStatement query;
		try {
			query = conn.prepareStatement("select distinct "+
					"(select count(s.isSent) from DropifiResponse s where s.isSent = true and s.apikey = r.apikey and (s.isArchived=r.isArchived)) as sent, "+ 
					"(select count(d.isSent) from DropifiResponse d where d.isSent = false and d.apikey = r.apikey and (d.isArchived=r.isArchived)) as draft "+
					"from DropifiResponse r where r.apikey = ? and (r.isArchived=false)");
		
			query.setString(1, apikey);		
			ResultSet result = query.executeQuery();
			 
			while(result.next()){
				Long sent = result.getLong("sent");
				Long draft = result.getLong("draft");
				
				json.addProperty("sent", sent);
				json.addProperty("draft", draft);
				return json;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(), e);
		} finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}		
		
		return json;
	}
	
	@Deprecated
	public static JsonObject countSents(String apikey,String msgId){ 
		JsonObject json = new JsonObject(); 
		
		Connection conn = DCON.getDefaultConnection();			
		PreparedStatement query;
		try {
			query = conn.prepareStatement("select distinct "+ 
					"(select count(s.isSent) from DropifiResponse s where s.isSent = true and s.apikey = r.apikey and s.isArchived =r.isArchived) as sent, "+ 
					"(select count(d.isSent) from DropifiResponse d where d.isSent = false and d.apikey = r.apikey and d.isArchived =r.isArchived) as draft, "+
					"(select count(d.isSent) from DropifiResponse d where d.isSent = true and d.apikey = r.apikey and d.msgId =? and d.isArchived =r.isArchived) as replies "+
					"from DropifiResponse r where r.apikey = ? and r.isArchived =false");
 
			query.setString(1, msgId);
			query.setString(2, apikey);	
			ResultSet result = query.executeQuery();
			 
			while(result.next()){
				json.addProperty("sent", result.getLong("sent")); 
				json.addProperty("draft", result.getLong("draft"));
				json.addProperty("replies", result.getLong("replies")); 
				return json;
			}
			
		}catch(SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(), e);
		} finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}		
		
		return json; 
	}
 
	//------------------------------------------------------------------------------------------------------------------
	//Search for messages
	public static JsonArray searchMessage(Long companyId, String search,Integer timezone){
		if(companyId!=null){		
			search ="%"+search+"%";
			
			List<Object[]> resultList =  
					DropifiMessage.find("SELECT distinct d.id , d.msgId , d.sourceOfMsg, d.status, c.email , c.fullname , d.subject, " +
							"m.articleSentiment , m.intenseSentence , m.dominantEmotion , d.created, " +
							"(select count(r) from DropifiResponse r where (r.msgId = d.msgId) and (r.mainRecipient =d.email) and (r.isSent=true) ) as responses " +
							"FROM DropifiMessage d , Customer c , MsgSentiment m " +
							"WHERE (c.id = d.customerId ) and (m.messageId = d.id ) and (d.companyId =:companyId) and (d.isArchived=false) and " + 
							"(c.email LIKE :search OR c.fullname LIKE :search OR d.subject LIKE :search OR d.textBody LIKE :search OR m.intenseSentence LIKE :search OR m.dominantEmotion LIKE :search OR c.phone LIKE :search ) " + 
							"ORDER BY d.id DESC ").setParameter("companyId", companyId).setParameter("search", search).fetch();
			 			 
			return getTaskMessages(resultList,timezone);    
		}
		return new JsonArray();
	 }
	
	//Search for contacts
	public static JsonArray searchContacts(Long companyId, String search) {
		search ="%"+search+"%";  
		List<Object[]> resultList = CustomerProfile.find("SELECT distinct c.id,c.email, p.fullName, c.isActive, p.image " +
				"FROM CustomerProfile p , Customer c WHERE (p.email = c.email) and (c.companyId = :companyId) and " +
				"(c.email LIKE :search OR c.fullname LIKE :search OR p.gender LIKE :search OR p.jobCompany LIKE :search OR p.jobTitle LIKE :search OR p.bio LIKE :search OR p.locationGeneral LIKE :search) " +
				"ORDER BY p.fullName ").setParameter("companyId", companyId).setParameter("search", search).fetch(); 		
		return  getContacts(resultList); 
	}
	
	//delete from cache and redis
	public static void deleteAllCaches(CoProfileSerializer profile, boolean flag){
		deleteAllCaches(profile.getApikey(), profile.getCompanyId(),flag); 
	}
	public static void deleteAllCaches(String apikey, long companyId,boolean flag){ 
		Connection conn =null;
		try{
			conn = DCON.getDefaultConnection();
			
			Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, "AllMessages"));
			Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, "UnResolvedMessages"));
			Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, "InboxCounter"));
			Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, "CustomerMessages"+true));
			Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, "CustomerMessages"+false));
			
			for(String sentiment : "Positive,Negative,Neutral".split(",")){
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, sentiment+"SentiMessages"));
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, sentiment+"SentiCustMessages"+true));
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, sentiment+"SentiCustMessages"+false));
			} 
			
			for(MsgStatus status : MsgStatus.values()){
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox,status.name()+"StatusMessages"));
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox,status.name()+"StatusCustMessages"+true));
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox,status.name()+"StatusCustMessages"+false)); 
			}
			
			for(String subject : InboxSelector.listOfMsgSubjects(conn,apikey,companyId)){
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, subject+"SubjectMessages"));
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, subject+"SubjectCustMessages"+true));
				Redis.delete(CacheKey.genCacheKey(apikey, CacheKey.Inbox, subject+"SubjectCustMessages"+false)); 
			}
			
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}finally{
			try { 
				if(conn!=null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		try{
			CoProfileSerializer profile = new CoProfileSerializer(apikey, companyId);
			ActorRef inboxActor = actorOf(InboxUpdateActor.class).start(); 
			inboxActor.tell(profile);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static JsonArray getJsonArray(String jsonStr){
		try{ return new JsonParser().parse(jsonStr).getAsJsonArray();
		}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}
		return new JsonArray();
	}
	
	public static JsonObject getJsonObject(String jsonStr){
		try{ return new JsonParser().parse(jsonStr).getAsJsonObject();
		}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}
		return new JsonObject();
	}
	
	public static List<String> listOfMsgSubjects(Connection conn, String apikey, long companyId){
		String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Subjects, "listOfMsgSubjects");
		List<String> subjects = Cache.get(cacheKey, List.class);
		if(subjects==null){
			 try {
				subjects = MessageManager.listOfMsgSubjects(conn, companyId);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
			CacheKey.set(cacheKey, subjects);
		}
		return subjects; 
	}
	
	public static List<String> listOfMsgSubjects( String apikey){
		String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Subjects, "listOfMsgSubjects");
		List<String> subjects = Cache.get(cacheKey, List.class);
		if(subjects==null){ 
			subjects = MessageManager.listOfMsgSubjects(apikey); 
			CacheKey.set(cacheKey, subjects);
		}
		return subjects;
	}
}
