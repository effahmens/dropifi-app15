package query.inbox;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class SocialSerializer implements Serializable{
	
	@SerializedName("id")
	private String id;
	@SerializedName("name")
	private String name;
	@SerializedName("url")
	private String url;
	private int followers;
	private int following;
	
 	public SocialSerializer(String id, String name,String url) {
		this.setId(id);
		this.setName(name);
		this.setUrl(url);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getFollowers() {
		return followers;
	}

	public void setFollowers(int followers) {
		this.followers = followers;
	}

	public int getFollowing() {
		return following;
	}

	public void setFollowing(int following) {
		this.following = following;
	}
	
	
	
}
