package query.inbox;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class SentimentCounter implements Serializable{

	@SerializedName("negative")
	private long negative;
	@SerializedName("positive")
	private long positive;
	@SerializedName("neutral")
	private long neutral;
	
	public SentimentCounter(long negative, long positive, long neutral){
		this.setNegative(negative);
		this.setPositive(positive);
		this.setNeutral(neutral); 
	}
	
	public SentimentCounter(){}

	public long getNegative() {
		return negative;
	}

	public void setNegative(long negative) {
		this.negative = negative;
	}

	public long getPositive() {
		return positive;
	}

	public void setPositive(long positive) {
		this.positive = positive;
	}

	public long getNeutral() {
		return neutral;
	}

	public void setNeutral(long neutral) {
		this.neutral = neutral;
	}
	
	
}
