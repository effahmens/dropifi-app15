package query.inbox;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import org.joda.time.DateTime;

import com.google.gson.annotations.SerializedName;

import resources.MsgStatus;
import view_serializers.AttSerializer;

public class MessageDetailSerializer implements Serializable{
	
	@SerializedName("id")
	private Long id;
	@SerializedName("msgId")
	private String msgId;
	@SerializedName("email")
	private String email;
	@SerializedName("status")
	private MsgStatus status;
	@SerializedName("sentiment")
	private String sentiment;
	@SerializedName("emotion")
	private String emotion;
	@SerializedName("source")
	private String source;
	@SerializedName("subject")
	private String subject;
	@SerializedName("intense")
	private String intense;
	@SerializedName("htmlBody")
	private String htmlBody;
	@SerializedName("textBody")
	private String textBody; 
	@SerializedName("pageUrl")
	private String pageUrl;
	@SerializedName("created")
	private Timestamp created;
	@SerializedName("attachments")
	private List<AttSerializer>attachments;
	
	public MessageDetailSerializer (Long id, String msgId,String email,MsgStatus status, String sentiment,
			String source, String subject, String intense, String htmlBody,String emotion,Timestamp created) {
		 
		this.setId(id);
		this.setMsgId(msgId);
		this.setEmail(email);
		this.setStatus(status);
		this.setSentiment(sentiment);
		this.setEmotion(emotion);
		this.setSource(source);
		this.setSubject(subject);
		this.setIntense(intense);
		this.setHtmlBody(htmlBody);
		this.setCreated(created);  		
	}
	
	public MessageDetailSerializer (){} 
	
 
	@Override
	public String toString() {
		return "MessageDetailSerializer [id=" + id + ", msgId=" + msgId
				+ ", email=" + email + ", status=" + status + ", sentiment="
				+ sentiment + ", emotion=" + emotion + ", source=" + source
				+ ", subject=" + subject + ", intense=" + intense
				+ ", htmlBody=" + htmlBody + ", textBody=" + textBody
				+ ", pageUrl=" + pageUrl + ", created=" + created + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public MsgStatus getStatus() {
		return status;
	}

	public void setStatus(MsgStatus status) {
		this.status = status;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getIntense() {
		return intense;
	}

	public void setIntense(String intense) {
		this.intense = intense;
	}

	public String getHtmlBody() {
		return htmlBody;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	public void setCreated(Timestamp created,Integer timezone){
		try{
			DateTime zone = new DateTime(created.getTime()); 
			if(timezone!=null){
				zone = zone.plusHours(timezone); 
			}
			 
			this.setCreated(new Timestamp(zone.toDate().getTime()));
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmotion() {
		return emotion;
	}

	public void setEmotion(String emotion) {
		if(emotion!=null){
			
			if(emotion.contains("_")){
				emotion = emotion.replaceAll("_", " and ");
			}
		}
		this.emotion = emotion;
	}

	public String getTextBody() {
		return textBody;
	}

	public void setTextBody(String textBody) {
		this.textBody = textBody;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public List<AttSerializer> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<AttSerializer> attachments) {
		this.attachments = attachments;
	}
	
	
	
}
