package query.inbox;

import java.io.Serializable;
import java.util.*;

import models.CustomerProfile;

import com.google.gson.annotations.SerializedName;

public class Contact_Serializer implements Serializable{
	@SerializedName("id")
	private Long id;
	@SerializedName("image")
	private String image;
	@SerializedName("email")
	private String email;
	@SerializedName("phone")
	private String phone; 
	@SerializedName("fullName")
	private String fullName;
	@SerializedName("gender")
	private String gender;
	@SerializedName("age")
	private Integer age;
	@SerializedName("location")
	private String location;
	@SerializedName("company")
	private String company; 
	@SerializedName("title")
	private String title;
	@SerializedName("isCustomer")
	private boolean isCustomer;
	private String bio;
	@SerializedName("socials")
	private List<SocialSerializer> socials;
	@SerializedName("numOfMsg")
	private long numOfMsg;
	
	public Contact_Serializer(String image, String email, String fullName,
			String gender, Integer age, String location, String company,
			String title, boolean isCustomer,String bio, List<SocialSerializer> socials) {
		 
		this.setImage(image);
		this.setEmail(email);
		this.setFullName(fullName);
		this.setGender(gender);
		this.setAge(age);
		this.setLocation(location);
		this.setCompany(company);
		this.setTitle(title);
		this.setIsCustomer(isCustomer);
		this.setBio(bio);
		this.setSocials(socials);	
	}
	
	public Contact_Serializer(long id ,String email, String fullName,boolean isCustomer, String image){		
		this.setId(id);
		this.setEmail(email);
		this.setFullName(fullName);
		this.setIsCustomer(isCustomer);
		this.setImage(image); 
	}
	
	public Contact_Serializer(CustomerProfile profile){
		this.setId(profile.getId());
		this.setImage(profile.getImage());
		this.setEmail(profile.getEmail());
		this.setFullName(profile.getFullName());
		this.setGender(profile.getGender());
		this.setLocation(profile.getLocationGeneral());			 
		this.setAge(profile.getAge());
		this.setCompany(profile.getJobCompany());
		this.setTitle(profile.getJobTitle());		 
		this.setBio(profile.getBio());
	}
	
	public Contact_Serializer(){}
	
	@Override
	public String toString() {
		return "Contact_Serializer [id=" + id + ", image=" + image + ", email="
				+ email + ", phone=" + phone + ", fullName=" + fullName
				+ ", gender=" + gender + ", age=" + age + ", location="
				+ location + ", company=" + company + ", title=" + title
				+ ", isCustomer=" + isCustomer + ", bio=" + bio + ", socials="
				+ socials + ", numOfMsg=" + numOfMsg + "]";
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean getIsCustomer() {
		return isCustomer;
	}

	public void setIsCustomer(boolean isCustomer) {
		this.isCustomer = isCustomer;
	}

	public List<SocialSerializer> getSocials() {
		return socials;
	}

	public void setSocials(List<SocialSerializer> socials) {
		this.socials = socials;
	}
	
	public String getBio() {
		return bio;
	}
	
	public void setBio(String bio) {
		this.bio = bio;
	}

	
	public Long getId() {
		return id;
	}
	

	public void setId(Long id) {
		this.id = id;
	}

	public long getNumOfMsg() {
		return numOfMsg;
	}

	public void setNumOfMsg(long numOfMsg) {
		this.numOfMsg = numOfMsg;
	}
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
