package query.inbox;

import java.io.Serializable;
import java.sql.Timestamp;

import org.joda.time.DateTime;

import com.google.gson.annotations.SerializedName;

import resources.DropifiTools;

public class SentMsg_Serializer implements Serializable{
	
	@SerializedName("id")
	private long id;
	@SerializedName("messageId")
	private long messageId;
	@SerializedName("msgId")
	private String msgId;	 
	@SerializedName("to") 
	private String to;	 
	@SerializedName("subject")
	private String subject;	
	@SerializedName("message")
	private String message;
	@SerializedName("created") 
	private long created;	
	@SerializedName("userEmail")
	private String userEmail; 
	@SerializedName("type")
	private String type;
	@SerializedName("isSent")
	private boolean isSent;
	@SerializedName("status")
	private String status;
	
	public SentMsg_Serializer(long id, long messageId, String msgId, String to, String subject, String message, Timestamp created,
			String userEmail, String type, boolean isSent, String status) {
		super();
		this.setId(id);
		this.setMessageId(messageId);  
		this.setMsgId(msgId);
		this.setTo(to);
		this.setSubject(subject);
		this.setMessage(message);
		this.setCreated(created);
		this.setUserEmail(userEmail);
		this.setType(type);
		this.setIsSent(isSent);
		this.setStatus(status);
	}
	
	public SentMsg_Serializer(Object[] fields,String userEmail,boolean isSent,Integer timezone){
		this.setSentMessage(fields, userEmail, isSent,timezone);
	}

	public void setSentMessage(Object[] fields,String userEmail,boolean isSent,Integer timezone){ 
	 	int i = 0;
		this.setId((Long)fields[i++]);
		Object  messageId = fields[i++];
		Object  msgId = fields[i++];
		this.setMessageId(messageId!=null?(Long)messageId:-1);
		this.setMsgId(msgId!=null?(String)msgId:"");		 
		 
		this.setTo((String)fields[i++]);
		 
		this.setSubject((String)fields[i++]);			
		//this.setMessage((String)fields[i++]);
		
		Timestamp time = (Timestamp)fields[i++];
		this.setCreated(time);

		String user = (String)fields[i++];	 
		this.setUserEmail( user.trim());
		this.setType("SentMessage");
		this.setIsSent(isSent);
		this.setStatus("Opened"); 
		 
	} 
		
	@Override
	public String toString() {
		return "SentMsg_Serializer [id=" + id + ", messageId=" + messageId
				+ ", msgId=" + msgId + ", to=" + to + ", subject=" + subject
				+ ", message=" + message + ", created=" + created
				+ ", userEmail=" + userEmail + ", type=" + type + ", isSent="
				+ isSent + ", status=" + status + "]";
	}
	
	//==========================================================================================================
	 
	public String getSentMessage(int index){  		
		return "<div class='mail_item mail_read' id ='mail_id_"+this.getId()+"_color' ref_index='"+index+"'>"+sentMessage(index);
	}
	
	public String sentMessage(int index) {  
	 		
		String subject = this.getSubject()!=null? this.getSubject() : "..";  
		String recipient =  this.getTo() !=null?this.getTo() : "..";	
		String userEmail =  (this.getIsSent()==true?"Replied By : ":"Drafted By : ")+this.getUserEmail();
		 
		String msg = "<input type='checkbox' id = 'chk_mail_id_" + this.getId() + "' value='"+this.getId()+"' class='msgArchives' name='msgArchives' />"+  
		"<div class='mail_content'  ref_index = '"+index+"'  id ='mail_id_" + this.getId() + "' ref_id='" + this.getMsgId()+ "' ref_to='"+ this.getTo() +"' ref_isSent='"+this.getIsSent()+
		"' onclick ='javascript:viewSentMessage(this.id)'><p class='hide_overflow_text'><span class='mail_to'>"+recipient+
		"</span> <span class='mail_time' style='float:right;'>"+ this.getCreated() +"</span></p> <p class='dominant_subject hide_overflow_text'>"+subject+"</p>"+
		"<p class='hide_overflow_text' style='font-weight:normal;'>"+userEmail+"</p></div></div>";
		
		return msg; 
	} 

	public long getId() { 
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}
	
	public void setCreated(Timestamp created) {
		this.setCreated(created.getTime());
	}
	
	public void setCreated(Timestamp created,Integer timezone){
		try{
			DateTime zone = new DateTime(created.getTime());
			if(timezone!=null){
				zone = zone.plusHours(timezone);
			}
			
			this.setCreated(new Timestamp(zone.toDate().getTime()));
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean getIsSent() {
		return isSent;
	}

	public void setIsSent(boolean isSent) {
		this.isSent = isSent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
}
