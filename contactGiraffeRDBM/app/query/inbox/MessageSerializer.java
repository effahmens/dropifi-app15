package query.inbox;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.joda.time.DateTime;

import com.google.gson.annotations.SerializedName;
import com.twitter.util.Time;

import resources.DropifiTools;
import resources.MsgStatus;

public class MessageSerializer implements Serializable{
	
	@SerializedName("id")
	private Long id;	
	@SerializedName("msgId")
	private String msgId;
	
	@SerializedName("email")
	private String email;
	@SerializedName("fullname")
	private String fullname;
	@SerializedName("subject")
	private String subject;
	@SerializedName("source")
	private String source;
	@SerializedName("textBody")
	private String textBody;	
	
	@SerializedName("status")
	private MsgStatus status;
	
	@SerializedName("sentiment")
	private String sentiment;
	@SerializedName("intense")
	private String intense;
	@SerializedName("emotion")
	private String emotion;
	
	@SerializedName("created")
	private long created;
	@SerializedName("type")
	private String type;
	@SerializedName("responses")
	private long responses;
	
	public MessageSerializer(Long id, String msgId, String source,MsgStatus status,
			String email,String fullName, String subject,  String textBody, String sentiment, 
			String intense,	String emotion,Timestamp created, long responses,Integer timezone) {
		 
		this.setId(id);
		this.setMsgId(msgId);
		this.setSource(source);
		this.setEmail(email);
		this.setFullname(fullName);
		this.setSubject(subject);		
		this.setStatus(status);
		this.setSentiment(sentiment);
		this.setIntense(intense);
		this.setEmotion(emotion);
		this.setCreated(created);
		this.setTextBody(textBody);
		this.setType("InboxMessage");
		this.setResponses(responses);
	}
	
	public MessageSerializer(Long id, String msgId, String source,MsgStatus status,
			String email,String fullName, String subject, String sentiment, 
			String intense,	String emotion,Timestamp created, long responses,Integer timezone) {
		this(id,msgId,source,status,email,fullName,subject,null,sentiment,intense,emotion,created,responses,timezone);
	}

	@Override
	public String toString() {
		return "thisSerializer [id=" + id + ", msgId=" + msgId + ", email="
				+ email + ", fullname=" + fullname + ", subject=" + subject
				+ ", source=" + source + ", textBody=" + textBody + ", status="
				+ status + ", sentiment=" + sentiment + ", intense=" + intense
				+ ", emotion=" + emotion + ", created=" + created + "]";
	} 
	
	public String getMessage(int index){
		if(this.status.equals(MsgStatus.New)){
			return newMessage(index);
		}else{
			return openMessage(index);
		}
	}
	
	public String newMessage(int index){
		return "<div class='mail_item' id ='mail_id_"+this.id+"_color' ref_index='"+index+"'>"+userMessage(index);
	}

	public String openMessage(int index){ 
		 return "<div class='mail_item mail_read' id ='mail_id_"+this.getId()+"_color' ref_index='"+index+"'>"+userMessage(index);
	} 
	
	public String userMessage(int index){
		return "<input type='checkbox' id = 'chk_mail_id_" + this.getId() + "' value='"+this.getId()+"' class='msgArchives' name='msgArchives' />"+ 
				"<div class='mail_content'  ref_index = '"+index+"' id ='mail_id_" + this.getId() + "' ref_id='" + this.getMsgId()+ "' ref_email='"+ this.getEmail() +
				"' ref_status='"+ this.getStatus() +"' ref_sentiment='"+this.getSentiment()+"' ref_source='"+ this.getSource() +"' onclick ='javascript:viewMessage(this.id)'>"+    
				"<p class='hide_overflow_text'><span class='mail_fullname'>"+(this.getFullname()!=null?this.getFullname():this.getEmail())+"</span> <span class='mail_time' style='float:right;'>"+ this.getCreated()+"</span></p>"+
				"<p class='dominant_subject hide_overflow_text'>"+this.getSubject()+"</p>"+
				"<p class='hide_overflow_text' style='font-weight:normal;'><span>"+sentimentEmotion()+"</span>"+this.getReply()+"</p>"+  
				"</div></div>";
	}
	
	public String getReply(){
		if(this.getResponses()>0){
			String r = this.getResponses()>1?" Replies ":" Reply "; 
			return"<span id= 'mail_id_"+this.getId()+"_reply' class='replyButton' replies='"+this.getResponses()+"' style='float:right'>"+this.getResponses()+r+"</span>";
		}
		return "<span id='mail_id_"+this.getId()+"_reply' class='' replies='"+this.getResponses()+"' style='float:right'></span>";
	}
	
	public String sentimentEmotion(){  
		String sent_image, sent_class; 
		
		switch(this.getSentiment()){  
			case "Positive":
				sent_image = "high_pos.png";
				sent_class = "pos_sentiment";
				break;
			case "Negative":
				sent_image = "high_neg.png";
				sent_class = "neg_sentiment"; 
				break;
			case "Neutral":
				sent_image = "neutral.png";
				sent_class = "neu_sentiment";
				break;
			default:
				sent_image = "";
				sent_class = "no_sentiment";   
				break; 
		}  
		
		String sent = "<img id='sentiment_image_message' src='https://s3.amazonaws.com/dropifi/images/features/"+sent_image+"' width='17' height='15' />"+ 
				"<span id='sentiment_message' class='"+sent_class+"'>"+this.getEmotion()+"</span>";
		return sent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String msgEmail) {
		this.email = msgEmail;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String msgFullname) {
		this.fullname = msgFullname;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String msgSubject) {
		this.subject = msgSubject;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String msgSource) {
		this.source = msgSource;
	}

	public MsgStatus getStatus() {
		return status;
	}

	public void setStatus(MsgStatus msgStatus) {
		this.status = msgStatus;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String msgSentiment) {
		this.sentiment = msgSentiment;;
	}

	public String getIntense() {
		return intense;
	}

	public void setIntense(String msgIntense) {
		this.intense = msgIntense;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created.getTime();
	}
	
	public void setCreated(Timestamp created,Integer timezone){
		try{
			DateTime zone = new DateTime(created.getTime());
			if(timezone!=null){
				zone = zone.plusHours(timezone);
			} 
			this.setCreated(new Timestamp(zone.toDate().getTime()));
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}

	public String getEmotion() {
		return emotion;
	}
	
	public void setEmotion(String emotion) {
		if(emotion!=null){
			
			if(emotion.contains("_")){
				emotion = emotion.replaceAll("_", " and ");
			}
		}
		this.emotion = emotion;
	}

	public String getTextBody() {
		return textBody;
	}

	public void setTextBody(String textBody) {
		this.textBody = textBody;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getResponses() {
		return responses;
	}

	public void setResponses(long responses) {
		this.responses = responses;
	}
	
		
}
