package query.analytics;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class CustSentSerialiser implements Serializable {
	
	@SerializedName("emotion")
	private String emotion;	

	@SerializedName("sentiment")
	private String sentiment;
	
	@SerializedName("number")
	private Long number;
	
	@SerializedName("score")
	private Double score;

	/**
	 * @param emotion
	 * @param number
	 */
	public CustSentSerialiser(String emotion, Long number) {
		this.emotion = emotion;
		this.number = number;
	}
	
	
	/**
	 * Default constructor
	 */
	public CustSentSerialiser() {
	}



	/**
	 * @return the emotion
	 */
	public String getEmotion() {
		return emotion;
	}

	/**
	 * @param emotion the emotion to set
	 */
	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}

	/**
	 * @return the sentiment
	 */
	public String getSentiment() {
		return sentiment;
	}

	/**
	 * @param sentiment the sentiment to set
	 */
	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	/**
	 * @return the number
	 */
	public Long getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(Long number) {
		this.number = number;
	}
	
	/**
	 * @return the score
	 */
	public Double getScore() {
		return score;
	}


	/**
	 * @param score the score to set
	 */
	public void setScore(Double score) {
		this.score = score;
	}

}
