package query.analytics;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import resources.MsgStatus;
import resources.SentimentType;

import models.Company;
import models.CustSocialProfile;
import models.CustomerProfile;
import models.DropifiMessage;
import models.InboundMailbox;
import models.MsgKeyword;
import models.MsgSentiment;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

public class CustSegSelector {
	
	
	//************************************************************************************************************
	// Age range selectors
	public static JsonArray cusAgeRangeNo(Long companyId, int noOfDays)
	{
		JsonArray arrayJson = new JsonArray();	
		DateTime dt = DateTime.now().minusDays(noOfDays);
		Timestamp endDate = new Timestamp(dt.getMillis());		
		
		if(companyId != null && noOfDays > 0)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.ageRange,(select count(a.ageRange) from  CustomerProfile a, Customer u where "+
					"(a.ageRange = p.ageRange ) and (a.email = u.email) and (u.companyId = ?1) and " +
					"(date(u.updated) >= ?2)) as MyCount from CustomerProfile p, Customer d where  "+
					"(p.email = d.email) And (d.companyId = ?1)	And  (date(d.created) >= ?2)", companyId,endDate).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setAgeRange((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray cusAgeRangeNo(Long companyId, Timestamp from)
	{
		JsonArray arrayJson = new JsonArray();				
		
		if(companyId != null && from != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select p.ageRange, count(p.ageRange) as MyCount from CustomerProfile p, Customer d where  (p.email = d.email) " +
					"And (d.companyId = ?1) and (date(d.created) = ?2) group by p.ageRange ", companyId,from).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setAgeRange((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray cusAgeRangeNo(Long companyId, Timestamp from, Timestamp to)
	{
		JsonArray arrayJson = new JsonArray();				
		
		if(companyId != null && from != null && to != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select p.ageRange, count(p.ageRange) as MyCount from CustomerProfile p, Customer d " +
					"where  (p.email = d.email) And (d.companyId = ?1) and (date(d.created) between ?2 and ?3) group by p.ageRange ", companyId,from, to).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setAgeRange((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	
	
	public static JsonArray cusAgeRangeNo(Long companyId, Timestamp from, int top){
		JsonArray arrayJson = new JsonArray();				
		
		if(companyId != null && from != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.ageRange,(select count(a.ageRange) from  CustomerProfile a, Customer u where "+
					"(a.ageRange = p.ageRange ) and (a.email = u.email) and (u.companyId = ?1) and (date(u.updated) = ?2)) as MyCount from CustomerProfile p, Customer d where  "+
					"(p.email = d.email) And (d.companyId = ?1)	And  (date(d.updated) = ?2)", companyId, from).fetch(top);
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setAgeRange((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray cusAgeRangeNo(Long companyId){
		JsonArray arrayJson = new JsonArray();
		
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select p.ageRange, count(p.ageRange) as MyCount from CustomerProfile p, Customer d where  (p.email = d.email) " +
					"And (d.companyId = ?1) group by p.ageRange", companyId).fetch();
						 
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setAgeRange((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	
	//************************************************************************************************************
	// Location selectors
	public static JsonArray custLocationToJsonArray(Long companyId, int noOfDays, int top)
	{
		JsonArray arrayJson = new JsonArray();		
		DateTime dt = DateTime.now().minusDays(noOfDays);
		Timestamp endDate = new Timestamp(dt.getMillis());
		
		if(companyId != null)
		{
			/*List<Object[]> resList = CustomerProfile.find(
					"select distinct p.locationGeneral,(select count(a.locationGeneral)	from CustomerProfile a, Customer b " +
					"where (a.locationGeneral=p.locationGeneral) and (a.email = b.email) And (b.companyId = ?1) And (date(b.updated) >= ?2)) as MyCount " +
					"from CustomerProfile p, Customer c	where (p.email = c.email) And (c.companyId = ?1) And (date(c.created) >= ?2) "+
					"order by MyCount desc ", companyId,endDate).fetch(top);
			*/ 
			List<Object[]> resList = CustomerProfile.find("select p.locationGeneral, count(p.locationGeneral) as MyCount " +
					"from CustomerProfile p, Customer c	where (p.email = c.email) And (c.companyId = ?1) And (date(c.created) >= ?2) " +
					"group by p.locationGeneral order by MyCount desc ", companyId,endDate).fetch(top);
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray custLocationToJsonArray(Long companyId, Timestamp from, int top)
	{
		JsonArray arrayJson = new JsonArray();		
		if(companyId != null && from != null)
		{
			/*List<Object[]> resList = CustomerProfile.find(
					"select distinct p.locationGeneral, (select count(a.locationGeneral) from CustomerProfile a, Customer b " +
					"where (a.locationGeneral=p.locationGeneral) and (a.email = b.email) And (b.companyId = ?1) And (date(b.created) = ?2)) as MyCount " +
					"from CustomerProfile p, Customer c	where (p.email = c.email) And (c.companyId = ?1) And (date(c.created) = ?2) "+
					"order by MyCount desc ", companyId, from).fetch(top);*/
			List<Object[]> resList = CustomerProfile.find("select p.locationGeneral, count(p.locationGeneral) as MyCount " +
					"from CustomerProfile p, Customer c	where (p.email = c.email) And (c.companyId = ?1) And (date(c.created) = ?2) " +
					"group by p.locationGeneral order by MyCount desc ", companyId, from).fetch(top);
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray custLocationToJsonArray(Long companyId, Timestamp from, Timestamp to, int top)
	{
		JsonArray arrayJson = new JsonArray();				
		
		if(companyId != null && from != null && to != null)
		{
			/*List<Object[]> resList = CustomerProfile.find(
					"select distinct p.locationGeneral,(select count(a.locationGeneral)	from CustomerProfile a, Customer b" +
			"	where (a.locationGeneral=p.locationGeneral) and (a.email = b.email) And (b.companyId = ?1) And (date(b.updated) between ?2 and ?3)) as MyCount" +
			"	from CustomerProfile p, Customer c	where (p.email = c.email) And (c.companyId = ?1) And (date(c.updated) between ?2 and ?3)"+
					"order by MyCount desc ", companyId, from, to).fetch(top);*/
			List<Object[]> resList = CustomerProfile.find(
					"select p.locationGeneral, count(p.locationGeneral) as MyCount " +
					"from CustomerProfile p, Customer c	where (p.email = c.email) And (c.companyId = ?1) And (date(c.created) between ?2 and ?3) " +
					"group by p.locationGeneral order by MyCount desc ", companyId, from, to).fetch(top);
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}

	
	
	public static JsonArray custLocationToJsonArray(Long companyId, Timestamp from, Timestamp to){
		JsonArray arrayJson = new JsonArray();				
		
		if(companyId != null && from != null && to != null)
		{
			List<Object[]> resList = CustomerProfile.find("select p.locationGeneral, count(p.locationGeneral) as MyCount 	" +
					"from CustomerProfile p, Customer c where (p.email = c.email) And (c.companyId = ?1) And (date(c.created) between ?2 and ?3) " +
					"group by p.locationGeneral order by MyCount desc ", companyId, from, to).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray custLocationToJsonArray(Long companyId, Timestamp from)
	{
		JsonArray arrayJson = new JsonArray();				
		
		if(companyId != null && from != null)
		{
			List<Object[]> resList = CustomerProfile.find("select p.locationGeneral, count(p.locationGeneral) as MyCount 	" +
					"from CustomerProfile p, Customer c where (p.email = c.email) And (c.companyId = ?1) And (date(c.created) = ?2) " +
					"group by p.locationGeneral order by MyCount desc ", companyId, from).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray custLocationToJsonArray(Long companyId, int noOfDays)
	{
		JsonArray arrayJson = new JsonArray();		
		DateTime dt = DateTime.now().minusDays(noOfDays);
		Timestamp endDate = new Timestamp(dt.getMillis());
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.locationGeneral,(select count(a.locationGeneral)	from CustomerProfile a, Customer b" +
			"	where (a.locationGeneral=p.locationGeneral) and (a.email = b.email) And (b.companyId = ?1) And (date(b.updated) >= ?2)) as MyCount" +
			"	from CustomerProfile p, Customer c	where (p.email = c.email) And (c.companyId = ?1) And (date(c.updated) >= ?2) "+
					"order by MyCount desc ", companyId,endDate).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray custLocationToJsonArray(Long companyId)
	{
		JsonArray arrayJson = new JsonArray();		
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select p.locationGeneral, count(p.locationGeneral) as MyCount from CustomerProfile p, Customer c " +
					"where (p.email = c.email) And (c.companyId = ?1) group by p.locationGeneral order by MyCount desc ", companyId).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	public static JsonArray custLocationToJsonArray_Top(Long companyId, int top)
	{
		JsonArray arrayJson = new JsonArray();		
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select p.locationGeneral, count(p.locationGeneral) as MyCount from CustomerProfile p, Customer c " +
					"where (p.email = c.email) And (c.companyId = ?1) group by p.locationGeneral order by MyCount desc ", companyId).fetch(top);
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setLocation((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	//**************************************************************************************************************
	// Gender selectors	
	public static JsonArray custGenderToJsonArray(Long companyId, int noOfDays)
	{
		JsonArray arrayJson = new JsonArray();			
		DateTime dt = DateTime.now().minusDays(noOfDays);
		Timestamp endDate = new Timestamp(dt.getMillis());
				
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
				"select distinct p.gender ,(select count(b.gender) from CustomerProfile b, Customer c "+
				"where (b.gender = p.gender) and (b.email=c.email) and (c.companyId=?1) and (date(c.updated) >= ?2)) as mcount "+
				"from CustomerProfile p, Customer d where (p.email=d.email) And (d.companyId=?1) And (date(d.created) > ?2)", companyId,endDate).fetch();
		
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setGender((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
						
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray custGenderToJsonArray(Long companyId, Timestamp from)
	{
		JsonArray arrayJson = new JsonArray();
				
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
				"select p.gender , count(p.gender)  as mcount from CustomerProfile p, Customer d " +
				"where (p.email=d.email) And (d.companyId = ?1) and  (date(d.created) = ?2) " +
				"group by p.gender", companyId,from).fetch();
		
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setGender((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
						
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	public static JsonArray custGenderToJsonArray(Long companyId, Timestamp from, Timestamp to){
		JsonArray arrayJson = new JsonArray();			
			
		if(companyId != null && from != null && to != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select p.gender , count(p.gender) from CustomerProfile p, Customer d " +
					"where (p.email=d.email) And (d.companyId = ?1) and  (date(d.created) between ?2 and ?3) " +
					"group by p.gender", companyId, from, to).fetch();
		
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setGender((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
						
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	
	/*public static JsonArray custGenderToJsonArray(Long companyId, Timestamp from){
		JsonArray arrayJson = new JsonArray();				
		if(companyId != null && from != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.gender ,(select count(b.gender) from CustomerProfile b, Customer c "+
					"where (b.gender = p.gender) and (b.email=c.email) and (c.companyId=?1) and (date(d.updated) = ?2)) as mcount "+
					"from CustomerProfile p, Customer d where (p.email=d.email) And (d.companyId=?1) And (date(d.updated) = ?2)", companyId, from).fetch();
		
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setGender((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
						
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}*/
	public static JsonArray custGenderToJsonArray(Long companyId){
		JsonArray arrayJson = new JsonArray();
		
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select p.gender , count(p.gender)  as mcount from CustomerProfile p, Customer d " +
					"where (p.email=d.email) and (d.companyId = ?1) " +
					"group by p.gender", companyId).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setGender((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
						
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
				
	//***************************************************************************************************************
	// Marital status selector 	
	
	public static JsonArray custMaritalStatusToJsonArray(Long companyId, Timestamp from, Timestamp to){
		JsonArray arrayJson = new JsonArray();				
				
		if(companyId != null && from != null && to != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.maritalStatus , (select count(b.maritalStatus) from CustomerProfile b where "+
					"(b.maritalStatus = p.maritalStatus)) from CustomerProfile p, Customer d where "+
					"(p.email = d.email) And (d.companyId = ?1) And (d.updated between ?2 and ?3)", companyId,from, to).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setMaritalStatus((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	public static JsonArray custMaritalStatusToJsonArray(Long companyId, Timestamp from){
		JsonArray arrayJson = new JsonArray();				
				
		if(companyId != null && from != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.maritalStatus , (select count(b.maritalStatus) from CustomerProfile b where "+
					"(b.maritalStatus = p.maritalStatus)) from CustomerProfile p, Customer d where "+
					"(p.email = d.email) And (d.companyId = ?1) And (date(d.updated) = ?2)", companyId,from).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setMaritalStatus((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	public static JsonArray custMaritalStatusToJsonArray(Long companyId, int noOfDays, int top){
		JsonArray arrayJson = new JsonArray();		
		DateTime dt = DateTime.now().minusDays(noOfDays);
		Timestamp endDate = new Timestamp(dt.getMillis());
		
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.maritalStatus , (select count(b.maritalStatus) from CustomerProfile b where "+
					"(b.maritalStatus = p.maritalStatus)) from CustomerProfile p, Customer d where "+
					"(p.email = d.email) And (d.companyId = ?1) And (d.updated > ?2)", companyId,endDate).fetch(top);
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setMaritalStatus((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	public static JsonArray custMaritalStatusToJsonArray(Long companyId, int noOfDays){
		JsonArray arrayJson = new JsonArray();	
		DateTime dt = DateTime.now().minusDays(noOfDays);
		Timestamp endDate = new Timestamp(dt.getMillis());
		
		if(companyId != null)
		{
			List<Object[]> resList = CustomerProfile.find(
					"select distinct p.maritalStatus , (select count(b.maritalStatus) from CustomerProfile b where "+
					"(b.maritalStatus = p.maritalStatus)) from CustomerProfile p, Customer d where "+
					"(p.email = d.email) And (d.companyId = ?1) And (d.updated > ?2)", companyId,endDate).fetch();
			
			if(resList != null){
				Gson gson = new Gson();
				for(Object[] obj : resList){
					int i = 0;
					CustSegSerialiser cssr = new CustSegSerialiser();
					cssr.setMaritalStatus((String)obj[i++]);
					cssr.setNoOfCust((Long)obj[i++]);
					
					arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	
	 public static JsonArray custMaritalStatusToJsonArray(Long companyId){
		 JsonArray arrayJson = new JsonArray();		
		 if(companyId != null)
		 {
			 List<Object[]> resList = CustomerProfile.find(
						"select distinct p.maritalStatus , (select count(b.maritalStatus) from CustomerProfile b where "+
						"(b.maritalStatus = p.maritalStatus)) from CustomerProfile p, Customer d where "+
						"(p.email = d.email) And (d.companyId = ?1) And (d.updated > ?2)", companyId).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setMaritalStatus((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
			 }
		}	
		return arrayJson;
	}
	
	//*****************************************************************************************************************
	 // Social Network selector
	 public static JsonArray custSocialNetworksToJsonArray(Long companyId, int noOfDays, int top){
			
		 JsonArray arrayJson = new JsonArray();			
		 DateTime dt = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dt.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = CustSocialProfile.find(
					 "select  s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, " +
					 "Customer  c where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) And " +
					 "(date(c.created) >= ?2) group by s.type order by MyCount desc", companyId, endDate).fetch(top);
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}

	 public static JsonArray custSocialNetworksToJsonArray(Long companyId,  Timestamp from, int top){
			
		 JsonArray arrayJson = new JsonArray();
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = CustSocialProfile.find(
					 "select  s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, " +
					 "Customer  c where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) And " +
					 "(date(c.created) = ?2) group by s.type order by MyCount desc", companyId, from).fetch(top);
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}

	 public static JsonArray custSocialNetworksToJsonArray(Long companyId, Timestamp from, Timestamp to, int top){
			
		 JsonArray arrayJson = new JsonArray();	
		 		 
		 if(companyId != null && from != null && to != null)
		 {
			 List<Object[]> resList = CustSocialProfile.find(
					 "select  s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, Customer  c " +
					 "where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) And (date(c.created) between ?2 and ?3) " +
					 "group by s.type order by MyCount desc", companyId, from, to).fetch(top);
			 
			if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	 
	 
	 
	 public static JsonArray custSocialNetworksToJsonArray(Long companyId){
		 JsonArray arrayJson = new JsonArray();	
		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = CustSocialProfile.find(
					 "select s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, Customer  c " +
					 "where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) group by s.type " +
					 "order by MyCount desc  ", companyId).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}	 
	 
	 public static JsonArray custSocialNetworksToJsonArray_Top(Long companyId, int top){
		 JsonArray arrayJson = new JsonArray();	
		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = CustSocialProfile.find(
					 "select s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, Customer  c " +
					 "where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) group by s.type " +
					 "order by MyCount desc  ", companyId).fetch(top);
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	 
	 
	 public static JsonArray custSocialNetworksToJsonArray(Long companyId, int noOfDays)
	 {			
		 JsonArray arrayJson = new JsonArray();			
		 DateTime dt = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dt.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 CustomerProfile.find("select s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, Customer  c " +
							 "where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) and (date(c.created) >= ?2) " +
							 "group by s.type order by MyCount desc  ", companyId, endDate).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}	 	 
	 public static JsonArray custSocialNetworksToJsonArray(Long companyId, Timestamp from, Timestamp to)
	 {					
		 JsonArray arrayJson = new JsonArray();	
		 		 
		 if(companyId != null && from != null && to != null)
		 {
			 List<Object[]> resList = CustomerProfile.find(
					 "select s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, Customer  c " +
					 "where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) and (date(c.created) between ?2 and ?3) " +
					 "group by s.type order by MyCount desc", companyId, from,to).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
	 public static JsonArray custSocialNetworksToJsonArray(Long companyId, Timestamp from)
	 {			
		 JsonArray arrayJson = new JsonArray();	
		 		 
		 if(companyId != null && from != null)
		 {
			 List<Object[]> resList = 
					 CustomerProfile.find(
							 "select s.type , count(s.type) as MyCount from CustSocialProfile s, CustomerProfile  p, Customer  c " +
							 "where (s.email = p.email) And (p.email = c.email) And (c.companyId = ?1) and (date(c.created) = ?2) " +
							 "group by s.type order by MyCount desc  ", companyId, from).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson();
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSegSerialiser cssr = new CustSegSerialiser();
					 cssr.setSocialNetwork((String)obj[i++]);
					 cssr.setNoOfCust((Long)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				}
			}
		}	
		return arrayJson;
	}
		
	 /************************************************************************************************************/
	 //Query selectors for trending sentiments
	
	 private static void addCustSent(String type, Long number, JsonArray jsa, Gson gs, int num)
	 {
		 CustSentSerialiser cs = new CustSentSerialiser();
		 switch(num){
		 case 0:
			 cs.setSentiment(type);
			 cs.setNumber(number);
			 jsa.add(gs.toJsonTree(cs));
			 break;
		 case 1:
			 cs.setEmotion(type);
			 cs.setNumber(number);
			 jsa.add(gs.toJsonTree(cs));
			 break;
		 }		
	 }
	 
	
	 public static Double sentimentPercentage(Double number, Double total){
		 Double result = (100.0 / total) * number;	 
		 return result;
	}

	 public static JsonArray custMsgSentiments(Long companyId)
	 {
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
						"select distinct s.articleSentiment, count(s.articleSentiment)," +
					"(select count(a.articleSentiment) from MsgSentiment a where (a.companyId=?1) " +
					"and (a.articleSentiment <>'NoSentiment')) from MsgSentiment s where (s.companyId=?1) And  " +
					"(s.articleSentiment <> 'NoSentiment') group by s.articleSentiment", companyId).fetch();	
			 
			
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean posi = false, neg = false, neu = false;
				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 CustSentSerialiser cssr = new CustSentSerialiser();					 
					 cssr.setSentiment((String)obj[i++]);					 
					 cssr.setScore(sentimentPercentage(Double.parseDouble(String.valueOf(obj[i++])), Double.parseDouble(String.valueOf(obj[i++]))));
					 					 
					 if(cssr.getSentiment().equalsIgnoreCase("positive"))
						 posi = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("negative"))
						 neg = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("neutral"))
						 neu = true;
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 if(!posi){
					 addCustSent("Positive",0L, arrayJson,gson,0);
				 }
				 if(!neg){
					 addCustSent("Negative",0L, arrayJson,gson,0);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,0);
				 }
			}
		}	
		return arrayJson;
	 }
	
	 public static JsonArray custMsgSentiments(Long companyId, int noOfDays)
	 {
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
					"select distinct s.articleSentiment, count(s.articleSentiment)," +
					"(select count(a.articleSentiment) from MsgSentiment a where (a.companyId=?1) " +
					"and (date(a.createdDate) > ?2) and (a.articleSentiment <>'NoSentiment')) " +
					"from MsgSentiment s where (s.companyId=?1) And (date(s.createdDate) > ?2) and " +
					"(s.articleSentiment <> 'NoSentiment') group by s.articleSentiment", companyId,endDate).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean posi = false, neg = false, neu = false;
				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 CustSentSerialiser cssr = new CustSentSerialiser();					 
					 cssr.setSentiment((String)obj[i++]);					 
					 cssr.setScore(sentimentPercentage(Double.parseDouble(String.valueOf(obj[i++])), Double.parseDouble(String.valueOf(obj[i++]))));
					 					 
					 if(cssr.getSentiment().equalsIgnoreCase("positive"))
						 posi = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("negative"))
						 neg = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("neutral"))
						 neu = true;
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 if(!posi){
					 addCustSent("Positive",0L, arrayJson,gson,0);
				 }
				 if(!neg){
					 addCustSent("Negative",0L, arrayJson,gson,0);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,0);
				 }
			}
		}	
		return arrayJson;
	 } 
	 
	 public static JsonArray custMsgSentiments(Long companyId, Timestamp from)
	 {
		 JsonArray arrayJson = new JsonArray();	
		 		 
		 if(companyId != null && from != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
						"select distinct s.articleSentiment, count(s.articleSentiment)," +
						"(select count(a.articleSentiment) from MsgSentiment a where (a.companyId=?1) " +
						"and (date(a.createdDate) = ?2) and (a.articleSentiment <>'NoSentiment')) " +
						"from MsgSentiment s where (s.companyId=?1) And (date(s.createdDate) = ?2) and " +
						"(s.articleSentiment <> 'NoSentiment') group by s.articleSentiment", companyId,from).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean posi = false, neg = false, neu = false;
				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 CustSentSerialiser cssr = new CustSentSerialiser();					 
					 cssr.setSentiment((String)obj[i++]);					 
					 cssr.setScore(sentimentPercentage(Double.parseDouble(String.valueOf(obj[i++])), Double.parseDouble(String.valueOf(obj[i++]))));
					 					 
					 if(cssr.getSentiment().equalsIgnoreCase("positive"))
						 posi = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("negative"))
						 neg = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("neutral"))
						 neu = true;
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 if(!posi){
					 addCustSent("Positive",0L, arrayJson,gson,0);
				 }
				 if(!neg){
					 addCustSent("Negative",0L, arrayJson,gson,0);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,0);
				 }
			}
		}	
		return arrayJson;
	 }
	 
	 public static JsonArray custMsgSentiments(Long companyId, Timestamp from, Timestamp to)
	 {
		 JsonArray arrayJson = new JsonArray();	
		 		 
		 if(companyId != null && from != null && to != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
						"select distinct s.articleSentiment, count(s.articleSentiment)," +
						"(select count(a.articleSentiment) from MsgSentiment a where (a.companyId=?1) " +
						"and (date(a.createdDate) between ?2 and ?3) and (a.articleSentiment <>'NoSentiment')) " +
						"from MsgSentiment s where (s.companyId=?1) And (date(s.createdDate) between ?2 and ?3) and " +
						"(s.articleSentiment <> 'NoSentiment') group by s.articleSentiment", companyId,from,to).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean posi = false, neg = false, neu = false;
				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 CustSentSerialiser cssr = new CustSentSerialiser();					 
					 cssr.setSentiment((String)obj[i++]);					 
					 cssr.setScore(sentimentPercentage(Double.parseDouble(String.valueOf(obj[i++])), Double.parseDouble(String.valueOf(obj[i++]))));
					 					 
					 if(cssr.getSentiment().equalsIgnoreCase("positive"))
						 posi = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("negative"))
						 neg = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("neutral"))
						 neu = true;
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 if(!posi){
					 addCustSent("Positive",0L, arrayJson,gson,0);
				 }
				 if(!neg){
					 addCustSent("Negative",0L, arrayJson,gson,0);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,0);
				 }
			}
		}	
		return arrayJson;
	 }
	 
	 
	 
	 public static JsonArray custMsgEmotions(Long companyId)
	 {
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
					 "select distinct s.dominantEmotion, count(s.dominantEmotion) from MsgSentiment s"+
					 " where (s.dominantEmotion <> 'NoSentiment') and (s.companyId=?1) group by s.dominantEmotion", companyId).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean af = false, ee = false, neu = false, ae = false, cg = false, sg = false,al = false, fu = false, hs = false;
				 String[] emo = {"affection_friendliness","enjoyment_elation","amusement_excitement","contentment_gratitude","sadness_grief","anger_loathing","fear_uneasiness","humiliation_shame","neutral"};
				 
				 String[] capemo = {"Affection/Friendliness","Enjoyment/Elation","Amusement/Excitement","Contentment/Gratitude","Sadness/Grief","Anger/Loathing","Fear/Uneasiness","Humiliation/Shame","Neutral"};
				 
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSentSerialiser cssr = new CustSentSerialiser();
					 cssr.setEmotion((String)obj[i++]);
					 cssr.setNumber((Long)obj[i++]);
					 
					 for(int j=0;j<emo.length;j++){
						 if(cssr.getEmotion().equalsIgnoreCase(emo[j]))
						 {
							 cssr.setEmotion(capemo[j]);							 
							 break;
						 }
					 }
					 					 
					 if(cssr.getEmotion().equalsIgnoreCase("Affection/Friendliness"))
						 af = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Enjoyment/Elation"))
						 ee = true;					 
					 else if(cssr.getEmotion().equalsIgnoreCase("Amusement/Excitement"))
						 ae = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Contentment/Gratitude"))
						 cg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Sadness/Grief"))
						 sg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Anger/Loathing"))
						 al = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Fear/Uneasiness"))
						 fu = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Humiliation/Shame"))
						 hs = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Neutral"))
						 neu = true;
					 
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 
				 if(!af){
					 addCustSent("Affection/Friendliness",0L, arrayJson,gson,1);
				 }
				 if(!ee){
					 addCustSent("Enjoyment/Elation",0L, arrayJson,gson,1);
				 }
				 if(!ae){
					 addCustSent("Amusement/Excitement",0L, arrayJson,gson,1);
				 }
				 if(!cg){
					 addCustSent("Contentment/Gratitude",0L, arrayJson,gson,1);
				 }
				 if(!sg){
					 addCustSent("Sadness/Grief",0L, arrayJson,gson,1);
				 }
				 if(!al){
					 addCustSent("Anger/Loathing",0L, arrayJson,gson,1);
				 }
				 if(!fu){
					 addCustSent("Fear/Uneasiness",0L, arrayJson,gson,1);
				 }
				 if(!hs){
					 addCustSent("Humiliation/Shame",0L, arrayJson,gson,1);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,1);
				 }
			}
		}	
		return arrayJson;
	 }
	 
	 public static JsonArray custMsgEmotions(Long companyId, Timestamp from, Timestamp to)
	 {
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
					 "select distinct s.dominantEmotion, count(s.dominantEmotion) from MsgSentiment s"+
					 " where (s.dominantEmotion <> 'NoSentiment') and (s.companyId=?1) and (date(s.createdDate) between ?2 and ?2) group by s.dominantEmotion", companyId, from, to).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean af = false, ee = false, neu = false, ae = false, cg = false, sg = false,al = false, fu = false, hs = false;
				 String[] emo = {"affection_friendliness","enjoyment_elation","amusement_excitement","contentment_gratitude","sadness_grief","anger_loathing","fear_uneasiness","humiliation_shame","neutral"};
				 
				 String[] capemo = {"Affection/Friendliness","Enjoyment/Elation","Amusement/Excitement","Contentment/Gratitude","Sadness/Grief","Anger/Loathing","Fear/Uneasiness","Humiliation/Shame","Neutral"};
				 
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSentSerialiser cssr = new CustSentSerialiser();
					 cssr.setEmotion((String)obj[i++]);
					 cssr.setNumber((Long)obj[i++]);
					 
					 for(int j=0;j<emo.length;j++){
						 if(cssr.getEmotion().equalsIgnoreCase(emo[j]))
						 {
							 cssr.setEmotion(capemo[j]);							 
							 break;
						 }
					 }
					 					 
					 if(cssr.getEmotion().equalsIgnoreCase("Affection/Friendliness"))
						 af = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Enjoyment/Elation"))
						 ee = true;					 
					 else if(cssr.getEmotion().equalsIgnoreCase("Amusement/Excitement"))
						 ae = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Contentment/Gratitude"))
						 cg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Sadness/Grief"))
						 sg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Anger/Loathing"))
						 al = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Fear/Uneasiness"))
						 fu = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Humiliation/Shame"))
						 hs = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Neutral"))
						 neu = true;
					 
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 
				 if(!af){
					 addCustSent("Affection/Friendliness",0L, arrayJson,gson,1);
				 }
				 if(!ee){
					 addCustSent("Enjoyment/Elation",0L, arrayJson,gson,1);
				 }
				 if(!ae){
					 addCustSent("Amusement/Excitement",0L, arrayJson,gson,1);
				 }
				 if(!cg){
					 addCustSent("Contentment/Gratitude",0L, arrayJson,gson,1);
				 }
				 if(!sg){
					 addCustSent("Sadness/Grief",0L, arrayJson,gson,1);
				 }
				 if(!al){
					 addCustSent("Anger/Loathing",0L, arrayJson,gson,1);
				 }
				 if(!fu){
					 addCustSent("Fear/Uneasiness",0L, arrayJson,gson,1);
				 }
				 if(!hs){
					 addCustSent("Humiliation/Shame",0L, arrayJson,gson,1);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,1);
				 }
			}
		}	
		return arrayJson;
	 }

	 public static JsonArray custMsgEmotions(Long companyId, int noOfDays)
	 {
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
					 "select distinct s.dominantEmotion, count(s.dominantEmotion) from MsgSentiment s"+
					 " where (s.dominantEmotion <> 'NoSentiment') and (s.companyId=?1) And (date(s.createdDate) > ?2) group by s.dominantEmotion", companyId,endDate).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean af = false, ee = false, neu = false, ae = false, cg = false, sg = false,al = false, fu = false, hs = false;
				 String[] emo = {"affection_friendliness","enjoyment_elation","amusement_excitement","contentment_gratitude","sadness_grief","anger_loathing","fear_uneasiness","humiliation_shame","neutral"};
				 
				 String[] capemo = {"Affection/Friendliness","Enjoyment/Elation","Amusement/Excitement","Contentment/Gratitude","Sadness/Grief","Anger/Loathing","Fear/Uneasiness","Humiliation/Shame","Neutral"};
				 
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSentSerialiser cssr = new CustSentSerialiser();
					 cssr.setEmotion((String)obj[i++]);
					 cssr.setNumber((Long)obj[i++]);
					 
					 for(int j=0;j<emo.length;j++){
						 if(cssr.getEmotion().equalsIgnoreCase(emo[j]))
						 {
							 cssr.setEmotion(capemo[j]);							 
							 break;
						 }
					 }
					 
					 if(cssr.getEmotion().equalsIgnoreCase("Affection/Friendliness"))
						 af = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Enjoyment/Elation"))
						 ee = true;					 
					 else if(cssr.getEmotion().equalsIgnoreCase("Amusement/Excitement"))
						 ae = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Contentment/Gratitude"))
						 cg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Sadness/Grief"))
						 sg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Anger/Loathing"))
						 al = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Fear/Uneasiness"))
						 fu = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Humiliation/Shame"))
						 hs = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Neutral"))
						 neu = true;
					 
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 
				 if(!af){
					 addCustSent("Affection/Friendliness",0L, arrayJson,gson,1);
				 }
				 if(!ee){
					 addCustSent("Enjoyment/Elation",0L, arrayJson,gson,1);
				 }
				 if(!ae){
					 addCustSent("Amusement/Excitement",0L, arrayJson,gson,1);
				 }
				 if(!cg){
					 addCustSent("Contentment/Gratitude",0L, arrayJson,gson,1);
				 }
				 if(!sg){
					 addCustSent("Sadness/Grief",0L, arrayJson,gson,1);
				 }
				 if(!al){
					 addCustSent("Anger/Loathing",0L, arrayJson,gson,1);
				 }
				 if(!fu){
					 addCustSent("Fear/Uneasiness",0L, arrayJson,gson,1);
				 }
				 if(!hs){
					 addCustSent("Humiliation/Shame",0L, arrayJson,gson,1);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,1);
				 }
			}
		}	
		return arrayJson;
	 }
	 
	 
	 // Sentiment Scores
	 
	 public static Double custMsgSentimentScore(Long companyId, int noOfDays){
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 CustSentSerialiser cssr = new CustSentSerialiser();
		 
		 if(companyId != null)
		 {
			 Object resList = MsgSentiment.find("select sum(s.score) from MsgSentiment s"+
					 									" where (s.companyId=?1) And (date(s.createdDate) > ?2)", companyId,endDate).first();
			 	
			 if(resList != null){
				 cssr.setScore((Double)resList);				 
			}
		}	
		return cssr.getScore();
	 }
	 
	 public static Double AverageSentimentScore(Long companyId){		 
		 CustSentSerialiser cssr = new CustSentSerialiser();		 
		 if(companyId != null)
		 {
			 Object resList = MsgSentiment.find("select round( avg(s.score), 5) from MsgSentiment s where (s.companyId = ?1)", companyId).first();
			 if(resList != null){
				 cssr.setScore((Double)resList);
			}
		 }	
		 return cssr.getScore();
	 }
	 public static Double AverageSentimentScore(Long companyId, Timestamp from, Timestamp to){		 
		 CustSentSerialiser cssr = new CustSentSerialiser();		 
		 if(companyId != null)
		 {
			 Object resList = MsgSentiment.find("select round( avg(s.score), 5) from MsgSentiment s " +
			 		"where (s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3)", companyId, from, to).first();
			 if(resList != null){
				 cssr.setScore((Double)resList);
			}
		 }	
		 return cssr.getScore();
	 }
	 
	 
	 public static Long NumberOfMailBoxes(Long companyId){
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 if(companyId != null){
			 Object res = InboundMailbox.find("select count(i.email) from InboundMailbox i " +
			 		"where (i.activated is true) and (i.ishidden is false) and (i.companyId = ?1)", companyId).first();
			 if(res != null){
				 kval.setLnum((Long)res);
			 }
		 }
		 return kval.getLnum();
	 }
	 
	
	 /************************************************************************************************************/
	// Troubling Issues
	 public static JsonArray compTroublingIssuesImp(Long companyId,int days, int top){
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(days);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgKeyword.find(
					"select k.keyword, count(k.keyword) as mct from MsgKeyword k where (k.companyId = ?1) And " +
					"(k.sentiment = ?2) and (date(k.createdDate) >= ?3) group by k.keyword order by mct desc",
					companyId,SentimentType.Negative, endDate).fetch(top);
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray compTroublingIssues(Long companyId, Timestamp from, int top){
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgKeyword.find(
							 "select distinct k.keyword, "+
							 "(select count(a.keyword) from MsgKeyword a where (a.keyword = k.keyword) and (a.sentiment = ?2) and " +
							 "(a.companyId = ?1) and (date(a.createdDate) = ?3)) as MyCount from MsgKeyword k where " +
							 "(k.companyId=?1) And (k.sentiment = ?2) and (date(k.createdDate) = ?3) " +
							 "order by MyCount desc", companyId,SentimentType.Negative, from).fetch(top);
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray compTroublingIssues(Long companyId, Timestamp from , Timestamp to, int top){
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgKeyword.find(
							 "select distinct k.keyword, "+
							 "(select count(a.keyword) from MsgKeyword a where (a.keyword = k.keyword) and (a.sentiment = ?2) and " +
							 "(a.companyId = ?1) and (date(a.createdDate) between ?3 and ?4)) as MyCount from MsgKeyword k where " +
							 "(k.companyId=?1) And (k.sentiment = ?2) and (date(k.createdDate) between ?3 and ?4) " +
							 "order by MyCount desc", companyId,SentimentType.Negative, from, to).fetch(top);
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray compTroublingIssues(Long companyId, int noOfDays){
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 MsgKeyword.find(
							 "select distinct k.keyword, "+
							 "(select count(a.keyword) from MsgKeyword a where (a.companyId=k.companyId) and "+
							 "(a.keyword = k.keyword) and (a.sentiment = k.sentiment) and (date(a.createdDate) > ?3)) as MyCount"+
							 " from MsgKeyword k where (k.companyId=?1) And (k.sentiment=?2) And (date(k.createdDate) > ?3) "+
							 " order by MyCount desc ", companyId,SentimentType.Negative,endDate).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 
	 public static JsonArray compTroublingIssues(Long companyId, int noOfDays, int top){
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList =  MsgKeyword.find(
				"select distinct k.keyword, (select count(m.keyword) from MsgKeyword m where (m.keyword=k.keyword) and  " +
				"(m.companyId=?1) and (m.sentiment=?2) and (date(m.createdDate) >= ?3)) as mcnt from  MsgKeyword k where " +
				"(k.companyId=?1) and (k.sentiment=?2) and (date(k.createdDate) >= ?3)", 
				companyId,SentimentType.Negative,endDate).fetch(top);
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	
	 
	 public static JsonArray compKeywords(Long companyId, int noOfDays, int top)
	 {
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 MsgKeyword.find(
							 "select distinct k.keyword, count(k.keyword) as MyCount from MsgKeyword k "+
							 " where (k.companyId=?1) and  (date(k.createdDate) > ?2) group by k.keyword order by MyCount desc ", companyId,endDate).fetch(top);
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray compKeywords(Long companyId, int noOfDays)
	 {
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 MsgKeyword.find(
							 "select distinct k.keyword, count(k.keyword) as MyCount from MsgKeyword k "+
							 " where (k.companyId=?1) and  (date(k.createdDate) > ?2) group by k.keyword order by MyCount desc ", companyId,endDate).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray compKeywords(Long companyId)
	 {
		 JsonArray arrayJson = new JsonArray();
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 MsgKeyword.find(
							 "select distinct k.keyword, count(k.keyword) as MyCount from MsgKeyword k "+
							 " where (k.companyId=?1) group by k.keyword order by MyCount desc ", companyId).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 KeyValueSerialiser kvs = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kvs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 /************************************************************************************************************/
	 // Mailbox Statistics
	 public static JsonArray mailBoxMessagesStatistics(Long companyId, int noOfDays){
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = DropifiMessage.find(
					 "select distinct m.sourceOfMsg," +
					 "(select count(d) from DropifiMessage d  where (d.companyId = m.companyId) and (d.status = ?3) and " +
					 "(d.sourceOfMsg=m.sourceOfMsg) and (date(d.created) >= ?2)) as NewMsg, " +
					 "(select count(a) from DropifiMessage a where (a.companyId = m.companyId) and (a.status <> ?4) and " +
					 "(a.sourceOfMsg=m.sourceOfMsg) and (date(a.created) >= ?2) ) as Unresd, " +
					 "(select count(b) from DropifiMessage b where (b.companyId=m.companyId) and (b.status = ?4) and " +
					 "(b.sourceOfMsg=m.sourceOfMsg) and (date(b.created) >= ?2) ) as Resld, " +
					 "count(m) as total from DropifiMessage m where (m.companyId=?1)  and (date(m.created) >= ?2) " +
					 "group by m.sourceOfMsg", companyId, endDate, MsgStatus.New, MsgStatus.Resolved).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbs = new MailBoxStatSerialiser((String)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++]);				 
					 arrayJson.add(gson.toJsonTree(mbs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray mailBoxMessagesStatistics(Long companyId, Timestamp from){
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = DropifiMessage.find(
					 "select distinct m.sourceOfMsg," +
					 "(select count(d) from DropifiMessage d  where (d.companyId = m.companyId) and (d.status = ?3) and " +
					 "(d.sourceOfMsg=m.sourceOfMsg) and (date(d.created) = ?2)) as NewMsg, " +
					 "(select count(a) from DropifiMessage a where (a.companyId = m.companyId) and (a.status <> ?4) and " +
					 "(a.sourceOfMsg=m.sourceOfMsg) and (date(a.created) = ?2) ) as Unresd, " +
					 "(select count(b) from DropifiMessage b where (b.companyId=m.companyId) and (b.status = ?4) and " +
					 "(b.sourceOfMsg=m.sourceOfMsg) and (date(b.created) = ?2) ) as Resld, " +
					 "count(m) as total from DropifiMessage m where (m.companyId=?1)  and (date(m.created) = ?2) " +
					 "group by m.sourceOfMsg", companyId, from, MsgStatus.New, MsgStatus.Resolved).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbs = new MailBoxStatSerialiser((String)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++]);				 
					 arrayJson.add(gson.toJsonTree(mbs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray mailBoxMessagesStatistics(Long companyId, Timestamp from, Timestamp to){
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = DropifiMessage.find(
					 "select distinct m.sourceOfMsg," +
					 "(select count(d) from DropifiMessage d  where (d.companyId = m.companyId) and (d.status = ?4) and " +
					 "(d.sourceOfMsg=m.sourceOfMsg) and (date(d.created) between ?2 and ?3)) as NewMsg, " +
					 "(select count(a) from DropifiMessage a where (a.companyId = m.companyId) and (a.status <> ?5) and " +
					 "(a.sourceOfMsg=m.sourceOfMsg) and (date(a.created) between ?2 and ?3) ) as Unresd, " +
					 "(select count(b) from DropifiMessage b where (b.companyId=m.companyId) and (b.status = ?5) and " +
					 "(b.sourceOfMsg=m.sourceOfMsg) and (date(b.created) between ?2 and ?3)) as Resld, " +
					 "count(m) as total from DropifiMessage m where (m.companyId=?1)  and (date(m.created) between ?2 and ?3) " +
					 "group by m.sourceOfMsg", companyId, from, to, MsgStatus.New, MsgStatus.Resolved).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbs = new MailBoxStatSerialiser((String)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++]);				 
					 arrayJson.add(gson.toJsonTree(mbs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 
	 
	 // Mailbox trending totals for line chart
	 public static JsonArray trendingMailBoxMessage(Long companyId, int noOfDays)
	 {
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp timestamp = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = DropifiMessage.find(
					 "select m.sourceOfMsg, m.created from  DropifiMessage m where (m.companyId=?1) And (date(m.created) >= ?2) ",
					 companyId, timestamp).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbs = new MailBoxStatSerialiser();
					 mbs.setMailbox((String)obj[i++]);
					 mbs.setTimestamp((Timestamp)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(mbs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray trendingMailBoxMessage(Long companyId, Timestamp from){
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = DropifiMessage.find(
					 "select m.sourceOfMsg, m.created from  DropifiMessage m where (m.companyId=?1) And (date(m.created) = ?2) ",
					 companyId, from).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbs = new MailBoxStatSerialiser();
					 mbs.setMailbox((String)obj[i++]);
					 mbs.setTimestamp((Timestamp)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(mbs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray trendingMailBoxMessage(Long companyId, Timestamp from, Timestamp to){
		 JsonArray arrayJson = new JsonArray();		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = DropifiMessage.find(
					 "select m.sourceOfMsg, m.created from  DropifiMessage m where (m.companyId=?1) And (date(m.created) between ?2 and ?3) ",
					 companyId, from, to).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbs = new MailBoxStatSerialiser();
					 mbs.setMailbox((String)obj[i++]);
					 mbs.setTimestamp((Timestamp)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(mbs));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 // Mailbox totals for pie chart
	 /*public static JsonArray MailBoxMessageForPieChart(Long companyId, int noOfDays){
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp timestamp = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = DropifiMessage.find(
					 "select m.sourceOfMsg, m.created from  DropifiMessage m where (m.companyId=?1) And (date(m.created) >= ?2) ",
					 companyId, timestamp).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbs = new MailBoxStatSerialiser();
					 mbs.setMailbox((String)obj[i++]);
					 mbs.setTimestamp((Timestamp)obj[i++]);
					 
					 arrayJson.add(gson.toJsonTree(mbs));
				 }						 	
			}
		}
		return arrayJson;
	 }*/
	 
	 
	 
	 
	 public static JsonArray mailBoxStatistics(Long companyId, int noOfDays){
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 DropifiMessage.find("select distinct d.sourceOfMsg, count(d), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg ) and (s.articleSentiment='Negative')), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='Positive')), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='Neutral')) "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='NoSentiment')) "+
			 "from DropifiMessage d where (d.companyId=?1) And (date(d.created) > ?2) group by d.sourceOfMsg order by count(d) desc", companyId, endDate).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbss = new MailBoxStatSerialiser((String)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++]);				 
					 arrayJson.add(gson.toJsonTree(mbss));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray mailBoxStatistics(Long companyId, int noOfDays, int top){
		 JsonArray arrayJson = new JsonArray();
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 DropifiMessage.find("select distinct d.sourceOfMsg, count(d), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg ) and (s.articleSentiment='Negative')), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='Positive')), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='Neutral')) "+
			 "from DropifiMessage d where (d.companyId=?1) And (date(d.created) >= ?2) group by d.sourceOfMsg order by count(d) desc", companyId, endDate).fetch(top);
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbss = new MailBoxStatSerialiser((String)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++]);				 
					 arrayJson.add(gson.toJsonTree(mbss));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 public static JsonArray mailBoxStatistics(Long companyId, Timestamp from, Timestamp to){
		 JsonArray arrayJson = new JsonArray();		 
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = 
					 DropifiMessage.find("select distinct d.sourceOfMsg, count(d), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg ) and (s.articleSentiment='Negative')), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='Positive')), "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='Neutral')) "+
			 "(select count(s.articleSentiment) from MsgSentiment s where (s.sourceofMsg = d.sourceOfMsg) and (s.articleSentiment='NoSentiment')) "+
			 "from DropifiMessage d where (d.companyId=?1) And (date(d.created) between ?2 and ?3) group by d.sourceOfMsg order by count(d) desc", companyId, from, to).fetch();
			 			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 for(Object[] obj : resList){
					 int i = 0;
					 MailBoxStatSerialiser mbss = new MailBoxStatSerialiser((String)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++], (Long)obj[i++]);				 
					 arrayJson.add(gson.toJsonTree(mbss));
				 }						 	
			}
		}
		return arrayJson;
	 }
	 
	 /************************************************************************************************************/
	 // Number of messages from a contact
	
	 
	 
	 
	 
	 /***********************************************************************************************************/
	 // Number of new messages
	 public static Long numberOfNewMessages(Long companyId, int noOfDays){
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and (m.isArchived=false) and " +
					 "(m.companyId = ?1) and (m.status = ?2) and (date(m.updated) >= ?3)", companyId,MsgStatus.New, endDate).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 public static Long numberOfNewMessages(Long companyId, Timestamp from){
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status = ?2) and (date(m.updated) = ?3)", companyId,MsgStatus.New, from).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 public static Long numberOfNewMessages(Long companyId, Timestamp from, Timestamp to){
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status = ?2) and (date(m.updated) between ?3 and ?4)", companyId,MsgStatus.New, from, to).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 
	 // Unresolved messages
	 public static Long UnResolvedMessages(Long companyId, int noOfDays){
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status is not ?2) and (date(m.updated) >= ?3)", companyId,MsgStatus.Resolved, endDate).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 public static Long UnResolvedMessages(Long companyId, Timestamp from){
		 KeyValueSerialiser kval = new KeyValueSerialiser();		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status <> ?2) and (date(m.updated) = ?3)", companyId,MsgStatus.Resolved, from).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 public static Long UnResolvedMessages(Long companyId, Timestamp from , Timestamp to){
		 KeyValueSerialiser kval = new KeyValueSerialiser();		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m where m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status <> ?2) and (date(m.updated) between ?3 and ?4)", companyId,MsgStatus.Resolved, from, to).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 // Resolved messages
	 public static Long ResolvedMessages(Long companyId, int noOfDays){
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status = ?2) and (date(m.updated) >= ?3)", companyId,MsgStatus.Resolved, endDate).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 public static Long ResolvedMessages(Long companyId, Timestamp from){
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status = ?2) and (date(m.updated) = ?3)", companyId,MsgStatus.Resolved, from).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 public static Long ResolvedMessages(Long companyId, Timestamp from, Timestamp to){
		 KeyValueSerialiser kval = new KeyValueSerialiser();		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(m) from DropifiMessage m , MsgSentiment s where (s.messageId = m.id ) and m.isArchived=false and " +
					 "(m.companyId = ?1) and (m.status = ?2) and (date(m.updated) between ?3 and ?4)", companyId,MsgStatus.Resolved, from, to).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum(); 
	 }
	 
	 
	 
	 public static Long numberOfCustomers(Long companyId, int noOfDays){
		 DateTime dateTime = DateTime.now().minusDays(noOfDays);
		 Timestamp endDate = new Timestamp(dateTime.getMillis());
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = DropifiMessage.find(
					 "select count(c) as NumOfCustomers from Customer c where  (c.companyId = ?1) and (date(c.updated) >= ?2)", companyId, endDate).first();
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 /*-----------------------------------------------------------------------------------------------------------------------*/
	 // Individual
	 // Customer's contact sentiment on all messages sent		 
	 public static JsonArray custMsgSentimentsFromContact(Long companyId, String contactEmail,  Timestamp from, Timestamp to)
	 {
		 JsonArray arrayJson = new JsonArray();
		 		 		 
		 if(companyId != null && contactEmail != null)
		 {
			 List<Object[]> resList = null;
			 
			 if(from != null && to == null)
			 {
				 resList = MsgSentiment.find("select s.articleSentiment, count(s.articleSentiment), (select count(a.articleSentiment) from MsgSentiment a where " +
						 "(a.companyId = ?1) and (a.email = ?2) and (date(a.createdDate) = ?3) and (a.articleSentiment <> 'NoSentiment')) from MsgSentiment s " +
						 "where (s.companyId = ?1) and (s.email = ?2) and (date(s.createdDate) = ?3) and (s.articleSentiment <> 'NoSentiment') " +
						 "group by s.articleSentiment", companyId, contactEmail, from).fetch(); 
			 }
			 else if(from != null && to != null)
			 {
				 resList = MsgSentiment.find("select s.articleSentiment, count(s.articleSentiment), (select count(a.articleSentiment) from MsgSentiment a where " +
						 "(a.companyId = ?1) and (a.email = ?2) and (date(a.createdDate) between ?3 and ?4) and (a.articleSentiment <> 'NoSentiment')) from MsgSentiment s " +
						 "where (s.companyId = ?1) and (s.email = ?2) and (date(s.createdDate) between ?3 and ?4) and (s.articleSentiment <> 'NoSentiment') " +
						 "group by s.articleSentiment", companyId, contactEmail, from, to).fetch(); 
			 }
			 else if(from == null && to == null)
			 {
				 resList = MsgSentiment.find("select s.articleSentiment, count(s.articleSentiment), (select count(a.articleSentiment) from MsgSentiment a where " +
						 "(a.companyId = ?1) and (a.email = ?2) and (a.articleSentiment <> 'NoSentiment')) from MsgSentiment s " +
						 "where (s.companyId = ?1) and (s.email = ?2) and (s.articleSentiment <> 'NoSentiment') " +
						 "group by s.articleSentiment", companyId, contactEmail).fetch(); 
			 } 
			 
			 
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean posi = false, neg = false, neu = false;
				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 CustSentSerialiser cssr = new CustSentSerialiser();					 
					 cssr.setSentiment((String)obj[i++]);					 
					 cssr.setScore(sentimentPercentage(Double.parseDouble(String.valueOf(obj[i++])), Double.parseDouble(String.valueOf(obj[i++]))));
					 					 
					 if(cssr.getSentiment().equalsIgnoreCase("positive"))
						 posi = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("negative"))
						 neg = true;
					 else if(cssr.getSentiment().equalsIgnoreCase("neutral"))
						 neu = true;
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 if(!posi){
					 addCustSent("Positive",0L, arrayJson,gson,0);
				 }
				 if(!neg){
					 addCustSent("Negative",0L, arrayJson,gson,0);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,0);
				 }
			}
		}	
		return arrayJson;
	 } 
	 
	 // Average sentiment score of messages from a contact
	 public static Double AverageSentimentScoreFromContact(Long companyId, String contactEmail, Timestamp from, Timestamp to)
	 {		 
		 CustSentSerialiser cssr = new CustSentSerialiser();
		 
		 if(companyId != null && contactEmail != null)
		 {
			 Object resList = null;
			 if(from != null && to == null)
			 {
				 resList = MsgSentiment.find("select round( avg(s.score), 5) from MsgSentiment s where (s.companyId = ?1) and (s.email = ?2) and " +
				 		"(date(s.createdDate) = ?3)", companyId, contactEmail, from).first();
			 }
			 else if(from != null && to != null)
			 {
				 resList = MsgSentiment.find("select round( avg(s.score), 5) from MsgSentiment s where (s.companyId = ?1) and (s.email = ?2) and " +
				 		"(date(s.createdDate) between ?3 and ?4)", companyId, contactEmail, from, to).first();
			 }
			 else if(from == null && to == null)
			 {
				 resList = MsgSentiment.find("select round( avg(s.score), 5) from MsgSentiment s where (s.companyId = ?1) and (s.email = ?2)", companyId, contactEmail).first();
			 }			 
			 
			 if(resList != null){
				 cssr.setScore((Double)resList);
			}
		 }	
		 return cssr.getScore();
	 }
	 
	 // Number of messages from  a contact
	 public static Long numberOfMessagesFromContact(Long companyId, String contactEmail,  Timestamp from, Timestamp to)
	 {
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null && contactEmail != null)
		 {
			 Object resList = null;
			 
			 if(from != null && to == null)
			 {
				 resList = DropifiMessage.find("select count(m) from DropifiMessage m where (m.companyId = ?1) and (m.email = ?2) " +
				 		"and (date(m.updated) = ?3)", companyId, contactEmail, from).first();
			 }
			 else if(from != null && to != null)
			 {
				 resList = DropifiMessage.find("select count(m) from DropifiMessage m where (m.companyId = ?1) and (m.email = ?2) " +
				 		"and (date(m.updated) between ?3 and ?4)", companyId, contactEmail, from, to).first();
			 }
			 else if(from == null && to == null)
			 {
				 resList = DropifiMessage.find("select count(m) from DropifiMessage m where (m.companyId = ?1) and (m.email = ?2) ", 
						 companyId, contactEmail).first();
			 }
			 
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
	 
	 // Number of unresolved messages from a contact
	 public static Long UnResolvedMessagesFromContact(Long companyId, String contactEmail,  Timestamp from, Timestamp to)
	 {
		 KeyValueSerialiser kval = new KeyValueSerialiser();
		 
		 if(companyId != null){
			 Object resList = null;
			 
			 if(from != null && to == null)
			 {
				 resList = DropifiMessage.find("select count(m) from DropifiMessage m where (m.companyId = ?1) and (m.email = ?2) and (m.status is not ?3) " +
				 		"and (date(m.updated) = ?4)", companyId, contactEmail, MsgStatus.Resolved, from).first();
			 }
			 else if(from != null && to != null)
			 {
				 resList = DropifiMessage.find("select count(m) from DropifiMessage m where (m.companyId = ?1) and (m.email = ?2) and (m.status is not ?3) " +
				 		"and (date(m.updated) between ?4 and ?5)", companyId, contactEmail, MsgStatus.Resolved, from, to).first();
			 }
			 else if(from == null && to == null)
			 {
				 resList = DropifiMessage.find("select count(m) from DropifiMessage m where (m.companyId = ?1) and (m.email = ?2) and (m.status is not ?3) "
						 , companyId, contactEmail, MsgStatus.Resolved).first();
			 }
			 
			 if(resList != null){
			 	kval.setLnum((Long)resList);				 
			 }
		 }
		 return kval.getLnum();
	 }
 
	 // Trending Emotion of a contact's messages
	 public static JsonArray custMsgEmotionsFromContact(Long companyId, String contactEmail,  Timestamp from, Timestamp to)
	 {
		 JsonArray arrayJson = new JsonArray();
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgSentiment.find("select s.dominantEmotion, count(s.dominantEmotion) " +
			 		"from MsgSentiment s  where (s.dominantEmotion <> 'NoSentiment') and (s.companyId = ?1) " +
			 		"and (s.email = ?2) group by s.dominantEmotion", companyId, contactEmail).fetch();
			 	
			 if(resList != null){
				 Gson gson = new Gson(); 
				 boolean af = false, ee = false, neu = false, ae = false, cg = false, sg = false,al = false, fu = false, hs = false;
				 String[] emo = {"affection_friendliness","enjoyment_elation","amusement_excitement","contentment_gratitude","sadness_grief","anger_loathing","fear_uneasiness","humiliation_shame","neutral"};
				 
				 String[] capemo = {"Affection/Friendliness","Enjoyment/Elation","Amusement/Excitement","Contentment/Gratitude","Sadness/Grief","Anger/Loathing","Fear/Uneasiness","Humiliation/Shame","Neutral"};
				 
				 for(Object[] obj : resList){
					 int i = 0;
					 CustSentSerialiser cssr = new CustSentSerialiser();
					 cssr.setEmotion((String)obj[i++]);
					 cssr.setNumber((Long)obj[i++]);
					 
					 for(int j=0;j<emo.length;j++){
						 if(cssr.getEmotion().equalsIgnoreCase(emo[j]))
						 {
							 cssr.setEmotion(capemo[j]);							 
							 break;
						 }
					 }
					 
					 if(cssr.getEmotion().equalsIgnoreCase("Affection/Friendliness"))
						 af = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Enjoyment/Elation"))
						 ee = true;					 
					 else if(cssr.getEmotion().equalsIgnoreCase("Amusement/Excitement"))
						 ae = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Contentment/Gratitude"))
						 cg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Sadness/Grief"))
						 sg = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Anger/Loathing"))
						 al = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Fear/Uneasiness"))
						 fu = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Humiliation/Shame"))
						 hs = true;
					 else if(cssr.getEmotion().equalsIgnoreCase("Neutral"))
						 neu = true;
					 
					 
					 arrayJson.add(gson.toJsonTree(cssr));
				 }
				 
				 if(!af){
					 addCustSent("Affection/Friendliness",0L, arrayJson,gson,1);
				 }
				 if(!ee){
					 addCustSent("Enjoyment/Elation",0L, arrayJson,gson,1);
				 }
				 if(!ae){
					 addCustSent("Amusement/Excitement",0L, arrayJson,gson,1);
				 }
				 if(!cg){
					 addCustSent("Contentment/Gratitude",0L, arrayJson,gson,1);
				 }
				 if(!sg){
					 addCustSent("Sadness/Grief",0L, arrayJson,gson,1);
				 }
				 if(!al){
					 addCustSent("Anger/Loathing",0L, arrayJson,gson,1);
				 }
				 if(!fu){
					 addCustSent("Fear/Uneasiness",0L, arrayJson,gson,1);
				 }
				 if(!hs){
					 addCustSent("Humiliation/Shame",0L, arrayJson,gson,1);
				 }
				 if(!neu){
					 addCustSent("Neutral",0L, arrayJson,gson,1);
				 }
			}
		}	
		return arrayJson;
	 }
}
