package query.analytics;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import query.inbox.MessageSerializer;
import resources.MsgStatus;

import models.Company;
import models.DropifiMessage;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

public class CustSegSerialiser implements Serializable {
	
	@SerializedName("ageRange")
	private String ageRange;
	
	@SerializedName("gender")
	private String gender;
	
	@SerializedName("socialNetwork")
	private String socialNetwork;
	
	@SerializedName("maritalStatus")
	private String maritalStatus;
	
	@SerializedName("location")
	private String location;
	
	@SerializedName("noOfCust") 
	private Long noOfCust;
	
	@SerializedName("emotions")
	private String emotions;
	
	@SerializedName("sentiment")
	private String sentiment;
	
	
	//--------------------------------------------------------------------------------------------
	// Constructor for the class
	public CustSegSerialiser(){}
	
	public CustSegSerialiser(String ageRange, String gender,
			String socialNetwork, String maritalStatus, String location, Long noOfCust) {
		super();
		this.ageRange = ageRange;
		this.gender = gender;
		this.socialNetwork = socialNetwork;
		this.maritalStatus = maritalStatus;
		this.location = location;
		this.noOfCust = noOfCust;
	}
	
	//----------------------------------------------------------------------------------------------
	// getters and setter for the entity fields
	
	public String getAgeRange() {
		return ageRange;
	}
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getSocialNetwork() {
		return socialNetwork;
	}
	public void setSocialNetwork(String socialNetwork) {
		this.socialNetwork = socialNetwork;
	}
	
	public Long getNoOfCust() {
		return noOfCust;
	}
	public void setNoOfCust(Long noOfCust) {
		this.noOfCust = noOfCust;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEmotions() {
		return emotions;
	}

	public void setEmotions(String emotions) {
		this.emotions = emotions;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

}
