package query.analytics;

import java.io.Serializable;
import java.sql.Timestamp;

import com.google.gson.annotations.SerializedName;

public class KeyValueSerialiser implements Serializable {

	@SerializedName("type")
	private String type;
	
	@SerializedName("lnum")
	private Long lnum;
	
	@SerializedName("dnum")
	private Double dnum;
	
	@SerializedName("inum")
	private Integer inum;
	
	@SerializedName("timestamp")
	private Timestamp timestamp;

	/**
	 * @param type
	 * @param inum
	 */
	public KeyValueSerialiser(String type, Integer inum) {
		this.type = type;
		this.inum = inum;
	}

	/**
	 * @param type
	 * @param dnum
	 */
	public KeyValueSerialiser(String type, Double dnum) {
		this.type = type;
		this.dnum = dnum;
	}

	/**
	 * @param type
	 * @param lnum
	 */
	public KeyValueSerialiser(String type, Long lnum) {
		this.type = type;
		this.lnum = lnum;
	}
	
	/**
	 * @param type
	 * @param timestamp
	 */
	public KeyValueSerialiser(String type, Timestamp timestamp) {
		this.type = type;
		this.timestamp = timestamp;
	}
	
	/**
	 * @param type
	 * @param timestamp
	 * @param inum
	 */
	public KeyValueSerialiser(String type, Timestamp timestamp, Long lnum) {
		this.type = type;
		this.timestamp = timestamp;
		this.lnum = lnum;
	}

	public KeyValueSerialiser(){}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the lnum
	 */
	public Long getLnum() {
		return lnum;
	}

	/**
	 * @param lnum the lnum to set
	 */
	public void setLnum(Long lnum) {
		this.lnum = lnum;
	}

	/**
	 * @return the dnum
	 */
	public Double getDnum() {
		return dnum;
	}

	/**
	 * @param dnum the dnum to set
	 */
	public void setDnum(Double dnum) {
		this.dnum = dnum;
	}

	/**
	 * @return the inum
	 */
	public Integer getInum() {
		return inum;
	}

	/**
	 * @param inum the inum to set
	 */
	public void setInum(Integer inum) {
		this.inum = inum;
	}

	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
}
