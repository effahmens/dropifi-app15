package query.analytics;

import java.io.Serializable;
import java.sql.Timestamp;

import com.google.gson.annotations.SerializedName;

public class MailBoxStatSerialiser implements Serializable{
	
	@SerializedName("mailbox")
	private String mailbox;
	
	@SerializedName("timestamp")
	private Timestamp timestamp;
	
	@SerializedName("totalMsg")
	private Long totalMsg;
	
	@SerializedName("negaSent")
	private Long negaSent;
	
	@SerializedName("posiSent")
	private Long posiSent;
	
	@SerializedName("neutSent")
	private Long neutSent;
	
	@SerializedName("noSent")
	private Long noSent;
	
	@SerializedName("resolved")
	private Long resolved;
	
	@SerializedName("unresolved")
	private Long unresolved;
	
	@SerializedName("newmsg")
	private Long newmsg;
	
	
	
	/**
	 * @param mailbox
	 * @param newmsg
	 * @param resolved
	 * @param unresolved
	 * @param totalMsg
	 */
	public MailBoxStatSerialiser(String mailbox, Long newmsg, Long resolved,
			Long unresolved, Long totalMsg) {
		this.mailbox = mailbox;
		this.newmsg = newmsg;
		this.resolved = resolved;
		this.unresolved = unresolved;
		this.totalMsg = totalMsg;
	}
	

	/**
	 * @param mailbox
	 * @param totalMsg
	 * @param negaSent
	 * @param posiSent
	 * @param neutSent
	 */
	public MailBoxStatSerialiser(String mailbox, Long totalMsg, Long negaSent,
			Long posiSent, Long neutSent, Long noSent) {
		this.mailbox = mailbox;
		this.totalMsg = totalMsg;
		this.negaSent = negaSent;
		this.posiSent = posiSent;
		this.neutSent = neutSent;
		this.noSent = noSent;
	}	

	public MailBoxStatSerialiser() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the mailbox
	 */
	public String getMailbox() {
		return mailbox;
	}

	/**
	 * @param mailbox the mailbox to set
	 */
	public void setMailbox(String mailbox) {
		this.mailbox = mailbox;
	}

	/**
	 * @return the totalMsg
	 */
	public Long getTotalMsg() {
		return totalMsg;
	}

	/**
	 * @param totalMsg the totalMsg to set
	 */
	public void setTotalMsg(Long totalMsg) {
		this.totalMsg = totalMsg;
	}

	/**
	 * @return the negaSent
	 */
	public Long getNegaSent() {
		return negaSent;
	}

	/**
	 * @param negaSent the negaSent to set
	 */
	public void setNegaSent(Long negaSent) {
		this.negaSent = negaSent;
	}

	/**
	 * @return the posiSent
	 */
	public Long getPosiSent() {
		return posiSent;
	}

	/**
	 * @param posiSent the posiSent to set
	 */
	public void setPosiSent(Long posiSent) {
		this.posiSent = posiSent;
	}

	/**
	 * @return the neutSent
	 */
	public Long getNeutSent() {
		return neutSent;
	}

	/**
	 * @param neutSent the neutSent to set
	 */
	public void setNeutSent(Long neutSent) {
		this.neutSent = neutSent;
	}

	/**
	 * @return the noSent
	 */
	public Long getNoSent() {
		return noSent;
	}

	/**
	 * @param noSent the noSent to set
	 */
	public void setNoSent(Long noSent) {
		this.noSent = noSent;
	}

	/**
	 * @return the resolved
	 */
	public Long getResolved() {
		return resolved;
	}

	/**
	 * @param resolved the resolved to set
	 */
	public void setResolved(Long resolved) {
		this.resolved = resolved;
	}

	/**
	 * @return the unresolved
	 */
	public Long getUnresolved() {
		return unresolved;
	}

	/**
	 * @param unresolved the unresolved to set
	 */
	public void setUnresolved(Long unresolved) {
		this.unresolved = unresolved;
	}

	/**
	 * @return the newmsg
	 */
	public Long getNewmsg() {
		return newmsg;
	}

	/**
	 * @param newmsg the newmsg to set
	 */
	public void setNewmsg(Long newmsg) {
		this.newmsg = newmsg;
	}


	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}


	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	

}
