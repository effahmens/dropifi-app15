package query.analytics;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.joda.time.DateTime;

import play.db.jpa.JPA;

import resources.MsgStatus;
import resources.SentimentType;

import models.Company;
import models.CustSocialProfile;
import models.CustomerProfile;
import models.DropifiMessage;
import models.MessageManager;
import models.MsgKeyword;
import models.MsgSentiment;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import controllers.*;

public class MainAnalyticsSelector {
	
	/********************************************************************/
	// Customer Profile Analytics
	
	public static JsonArray custProfileAnalytics(Long companyId, Timestamp dfrom, Timestamp dto, String str1, String str2)
	{		
		JsonArray arrayJson = new JsonArray();
		try
		{		
			if(companyId != null)
			{		
				if(str1.equalsIgnoreCase("ageRange") && str2.equalsIgnoreCase("0"))
				{
					if((dfrom == null) && (dto == null))
					{
						arrayJson = CustSegSelector.cusAgeRangeNo(companyId);
					}
					else if((dfrom != null) && (dto == null))
					{
						arrayJson = CustSegSelector.cusAgeRangeNo(companyId, dfrom);
					}
					else if((dfrom == null) && (dto != null))
					{
						arrayJson = CustSegSelector.cusAgeRangeNo(companyId, dto);
					}
					else if((dfrom != null) && (dto != null))
					{
						arrayJson = CustSegSelector.cusAgeRangeNo(companyId, dfrom, dto);
					}
				}
				else if((str1.equalsIgnoreCase("ageRange") && str2.equalsIgnoreCase("location"))||
						(str1.equalsIgnoreCase("location") && str2.equalsIgnoreCase("ageRange")))
				{
					//play.Logger.log4j.info("age range and location");
					if((dfrom == null) && (dto == null))
					{	
						//play.Logger.log4j.info("age range and location");
					
					List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, p.locationGeneral, count(p.locationGeneral) as MyCount from CustomerProfile p, Customer c " +
							"where (p.email = c.email) And (c.companyId = ?1) group by p.ageRange, p.locationGeneral", companyId).fetch();
						
						if(resList != null){
							add_ageRang_location(arrayJson,resList);
						}		
					}
					else if((dfrom != null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, p.locationGeneral, count(p.locationGeneral) as MyCount from CustomerProfile p, Customer c " +
							"where (p.email = c.email) And (c.companyId = ?1) and (date(c.created) = ?2) group by p.ageRange, p.locationGeneral ", companyId, dfrom).fetch();
											
						if(resList != null){
							add_ageRang_location(arrayJson,resList);
						}
					}
					else if((dfrom == null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, p.locationGeneral, count(p.locationGeneral) as MyCount from CustomerProfile p, Customer c " +
							"where (p.email = c.email) And (c.companyId = ?1) and (date(c.created) = ?2) group by p.ageRange, p.locationGeneral ", companyId, dto).fetch();
											
						if(resList != null){
							add_ageRang_location(arrayJson,resList);
						}
					}
					else if((dfrom != null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, p.locationGeneral, count(p.locationGeneral) as MyCount from CustomerProfile p, Customer c " +
							"where (p.email = c.email) And (c.companyId = ?1) and (date(c.created) between ?2 and ?3) group by p.ageRange, p.locationGeneral ", companyId, dfrom, dto).fetch();
						
						if(resList != null){
							add_ageRang_location(arrayJson,resList);
						}
					}
				}
				else if((str1.equalsIgnoreCase("ageRange") && str2.equalsIgnoreCase("gender")) || 
						(str1.equalsIgnoreCase("gender") && str2.equalsIgnoreCase("ageRange")))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId= ?1) group by p.ageRange, p.gender",  companyId).fetch();
						
						if(resList != null){
							add_ageRang_gender(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId= ?1) and (date(d.created) = ?2) group by p.ageRange, p.gender",  companyId,dfrom).fetch();
						if(resList != null){
							add_ageRang_gender(arrayJson,resList);	
						}
					}
					else if((dfrom == null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId= ?1) and (date(d.created) = ?2) group by p.ageRange, p.gender",  companyId,dto).fetch();
						if(resList != null){
							add_ageRang_gender(arrayJson,resList);	
						}
					}
					else if((dfrom != null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId = ?1) and (date(d.created) between ?2 and ?3) group by p.ageRange, p.gender",  companyId, dfrom, dto).fetch();
						if(resList != null){
							add_ageRang_gender(arrayJson,resList);
						}
					}
				}
				else if((str1.equalsIgnoreCase("ageRange") && str2.equalsIgnoreCase("social")) || (str1.equalsIgnoreCase("social") && str2.equalsIgnoreCase("ageRange")))
				{
					if((dfrom == null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, s.type, count(s.type)  as MyCount from CustSocialProfile s, CustomerProfile p,  Customer c " +
							"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) group by p.ageRange, s.type",  companyId).fetch();
						
						if(resList != null){
							add_ageRang_social(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, s.type, count(s.type)  as MyCount from CustSocialProfile s, CustomerProfile p,  Customer c " +
							"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(c.created) = ?2) " +
							"group by p.ageRange, s.type",  companyId, dfrom).fetch();
												
						if(resList != null){
							add_ageRang_social(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, s.type, count(s.type)  as MyCount from CustSocialProfile s, CustomerProfile p,  Customer c " +
							"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(c.created) = ?2) " +
							"group by p.ageRange, s.type",  companyId, dto).fetch();
												
						if(resList != null){
							add_ageRang_social(arrayJson,resList);						
						}
					}				
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
							"select p.ageRange, s.type, count(s.type)  as MyCount from CustSocialProfile s, CustomerProfile p,  Customer c " +
							"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(c.created) between ?2 and ?3) " +
							"group by p.ageRange, s.type",  companyId, dfrom, dto).fetch();
												
						if(resList != null){
							add_ageRang_social(arrayJson,resList);						
						}
					}
					
					
				}
				else if(str1.equalsIgnoreCase("ageRange") && str2.equalsIgnoreCase("emotions"))
				{
					if((dfrom == null) && (dto == null)){
						
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) " +
								"group by p.ageRange, s.dominantEmotion",  companyId).fetch();
						
						if(resList != null){
							add_ageRang_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.ageRange, s.dominantEmotion",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_ageRang_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.ageRange, s.dominantEmotion",  companyId, dto).fetch();
						
						if(resList != null){
							add_ageRang_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.ageRange, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) " +
								"group by p.ageRange, s.dominantEmotion",  companyId, dfrom, dto).fetch();
												
						if(resList != null){
							add_ageRang_emotions(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("ageRange") && str2.equalsIgnoreCase("sentiment"))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, s.articleSentiment, count(s.articleSentiment)  as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) " +
								"group by p.ageRange, s.articleSentiment",  companyId).fetch();		
						
						if(resList != null){
							add_ageRang_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, s.articleSentiment, count(s.articleSentiment)  as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.ageRange, s.articleSentiment",  companyId,dfrom).fetch();
												
						if(resList != null){
							add_ageRang_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, s.articleSentiment, count(s.articleSentiment)  as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.ageRange, s.articleSentiment",  companyId,dto).fetch();
												
						if(resList != null){
							add_ageRang_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, s.articleSentiment, count(s.articleSentiment)  as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and  (p.email=s.email) and  (s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) " +
								"group by p.ageRange, s.articleSentiment",  companyId,dfrom, dto).fetch();
												
						if(resList != null){
							add_ageRang_sentiment(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("ageRange") && str2.equalsIgnoreCase("messages"))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) group by p.ageRange",  companyId).fetch();		
						
						if(resList != null){
							add_ageRang_messages(arrayJson, resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) = ?2) group by p.ageRange ",  companyId, dfrom).fetch();
																	
						if(resList != null){
							add_ageRang_messages(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) = ?2) group by p.ageRange ",  companyId, dfrom).fetch();
																	
						if(resList != null){
							add_ageRang_messages(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.ageRange, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) between ?2 and ?3) group by p.ageRange ",  companyId, dfrom, dto).fetch();
																	
						if(resList != null){
							add_ageRang_messages(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("location") && str2.equalsIgnoreCase("0"))
				{
					if((dfrom == null) && (dto == null))
					{
						//arrayJson = CustSegSelector.custLocationToJsonArray(companyId);
						arrayJson = CustSegSelector.custLocationToJsonArray_Top(companyId, 10);
					}
					else if((dfrom != null) && (dto == null))
					{
						arrayJson = CustSegSelector.custLocationToJsonArray(companyId, dfrom);
					}
					else if((dfrom != null) && (dto != null))
					{
						arrayJson = CustSegSelector.custLocationToJsonArray(companyId, dfrom, dto);
					}
				}
				else if((str1.equalsIgnoreCase("location") && str2.equalsIgnoreCase("gender")) || 
						(str1.equalsIgnoreCase("gender") && str2.equalsIgnoreCase("location")))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId= ?1) group by p.locationGeneral, p.gender order by mcount desc",  companyId).fetch();
						
						if(resList != null){
							add_location_gender(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId= ?1) and (date(d.created) = ?2) group by p.locationGeneral, p.gender order by mcount desc",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_location_gender(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId= ?1) and (date(d.created) = ?2) group by p.locationGeneral, p.gender order by mcount desc",  companyId, dto).fetch();
						
						if(resList != null){
							add_location_gender(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, p.gender, count(p.gender) as mcount from CustomerProfile p, Customer d " +
								"where (p.email=d.email) And (d.companyId= ?1) and (date(d.created) between ?2 and ?3) group by p.locationGeneral, p.gender order by mcount desc",  companyId, dfrom, dto).fetch();
						
						if(resList != null){
							add_location_gender(arrayJson,resList);						
						}
					}
				}
				else if((str1.equalsIgnoreCase("location") && str2.equalsIgnoreCase("social")) || 
						(str1.equalsIgnoreCase("social") && str2.equalsIgnoreCase("location")))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.type, count(s.type) as MyCount from CustSocialProfile s, CustomerProfile p , Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) group by p.locationGeneral, s.type ",  companyId).fetch();
						
						if(resList != null){
							add_location_social(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.type, count(s.type) as MyCount from CustSocialProfile s, CustomerProfile p , Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1)  and (date(c.created) = ?2) group by p.locationGeneral, s.type ",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_location_social(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.type, count(s.type) as MyCount from CustSocialProfile s, CustomerProfile p , Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1)  and (date(c.created) = ?2) group by p.locationGeneral, s.type ",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_location_social(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.type, count(s.type) as MyCount from CustSocialProfile s, CustomerProfile p , Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1)  and (date(c.created) between ?2 and ?3) group by p.locationGeneral, s.type ",  companyId, dfrom, dto).fetch();
						
						if(resList != null){
							add_location_social(arrayJson,resList);						
						}
					}
				}
				else if((str1.equalsIgnoreCase("location") && str2.equalsIgnoreCase("emotions")))
				{
					if((dfrom == null) && (dto == null)){
						
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) " +
								"group by p.locationGeneral, s.dominantEmotion ",  companyId).fetch();
						
						if(resList != null){
							add_location_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.locationGeneral, s.dominantEmotion",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_location_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.locationGeneral, s.dominantEmotion",  companyId, dto).fetch();
						
						if(resList != null){
							add_location_emotions(arrayJson,resList);						
						}
					}					
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.locationGeneral, s.dominantEmotion, count(s.dominantEmotion) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) " +
								"group by p.locationGeneral, s.dominantEmotion",  companyId, dfrom, dto).fetch();
												
						if(resList != null){
							add_location_emotions(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("location") && str2.equalsIgnoreCase("sentiment"))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral, s.articleSentiment, count(s.articleSentiment) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) group by p.locationGeneral, s.articleSentiment",  companyId).fetch();		
						
						if(resList != null){
							add_location_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral, s.articleSentiment, count(s.articleSentiment) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.locationGeneral, s.articleSentiment ",  companyId,dfrom).fetch();
												
						if(resList != null){
							add_location_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral, s.articleSentiment, count(s.articleSentiment) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.locationGeneral, s.articleSentiment ",  companyId,dto).fetch();
												
						if(resList != null){
							add_location_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral, s.articleSentiment, count(s.articleSentiment) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) " +
								"group by p.locationGeneral, s.articleSentiment",  companyId,dfrom, dto).fetch();
												
						if(resList != null){
							add_location_sentiment(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("location") && str2.equalsIgnoreCase("messages"))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral,  count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) group by p.locationGeneral order by mycount desc",  companyId).fetch();		
						
						if(resList != null){
							add_location_messages(arrayJson, resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral,  count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) = ?2) group by p.locationGeneral order by mycount desc",  companyId, dfrom).fetch();
																	
						if(resList != null){
							add_location_messages(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral,  count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) = ?2) group by p.locationGeneral order by mycount desc",  companyId, dto).fetch();
																	
						if(resList != null){
							add_location_messages(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.locationGeneral,  count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) between ?2 and ?3) group by p.locationGeneral order by mycount desc",  companyId, dfrom, dto).fetch();
																	
						if(resList != null){
							add_location_messages(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("gender") && str2.equalsIgnoreCase("0"))
				{
					if((dfrom == null) && (dto == null))
					{
						arrayJson = CustSegSelector.custGenderToJsonArray(companyId);
					}
					else if((dfrom != null) && (dto == null))
					{
						arrayJson = CustSegSelector.custGenderToJsonArray(companyId, dfrom);
					}
					else if((dfrom != null) && (dto != null))
					{
						arrayJson = CustSegSelector.custGenderToJsonArray(companyId, dfrom, dto);
					}
				}
				else if((str1.equalsIgnoreCase("gender") && str2.equalsIgnoreCase("social")) || 
						(str1.equalsIgnoreCase("social") && str2.equalsIgnoreCase("gender")))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.type, count(s.type) as MyCount from CustSocialProfile s, CustomerProfile p, Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) group by p.gender, s.type",  companyId).fetch();
						
						if(resList != null){
							add_gender_social(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.type, count(s.type) as MyCount from CustSocialProfile s, CustomerProfile p, Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(c.created) = ?2) group by p.gender, s.type",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_gender_social(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.type, count(s.type) as MyCount from CustSocialProfile s, CustomerProfile p, Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(c.created) = ?2) group by p.gender, s.type",  companyId, dto).fetch();
						
						if(resList != null){
							add_gender_social(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null))
					{
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.type, count(s.type) as MyCount from CustomerProfile p, CustSocialProfile s, Customer c " +
								"where (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(c.created) between ?2 and ?3) group by p.gender, s.type",  companyId, dfrom, dto).fetch();
						
						if(resList != null){
							add_gender_social(arrayJson,resList);						
						}
					}
				}
				else if((str1.equalsIgnoreCase("gender") && str2.equalsIgnoreCase("emotions")))
				{
					if((dfrom == null) && (dto == null)){
						
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.dominantEmotion,  count(s.dominantEmotion)  as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) " +
								"group by p.gender, s.dominantEmotion",  companyId).fetch();
						
						if(resList != null){
							add_gender_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.dominantEmotion,  count(s.dominantEmotion)  as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.gender, s.dominantEmotion",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_gender_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.dominantEmotion,  count(s.dominantEmotion)  as MyCount from  CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.gender, s.dominantEmotion",  companyId, dto).fetch();
						
						if(resList != null){
							add_gender_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(
								"select p.gender, s.dominantEmotion,  count(s.dominantEmotion)  as MyCount from  CustomerProfile p, MsgSentiment s " +
								"where (s.dominantEmotion <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) " +
								"group by p.gender, s.dominantEmotion",  companyId, dfrom, dto).fetch();
												
						if(resList != null){
							add_gender_emotions(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("gender") && str2.equalsIgnoreCase("sentiment"))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, s.articleSentiment, count(s.articleSentiment) as MyCount from CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) " +
								"group by p.gender, s.articleSentiment",  companyId).fetch();		
						
						if(resList != null){
							add_gender_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, s.articleSentiment, count(s.articleSentiment) as MyCount from  CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.gender, s.articleSentiment",  companyId,dfrom).fetch();
												
						if(resList != null){
							add_gender_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, s.articleSentiment, count(s.articleSentiment) as MyCount from CustomerProfile p, MsgSentiment s  " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) = ?2) " +
								"group by p.gender, s.articleSentiment",  companyId,dfrom).fetch();
												
						if(resList != null){
							add_gender_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, s.articleSentiment, count(s.articleSentiment) as MyCount from  CustomerProfile p, MsgSentiment s " +
								"where (s.articleSentiment <> 'NoSentiment') and (p.email=s.email) and (s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) " +
								"group by p.gender, s.articleSentiment",  companyId,dfrom, dto).fetch();
												
						if(resList != null){
							add_gender_sentiment(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("gender") && str2.equalsIgnoreCase("messages"))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) group by p.gender",  companyId).fetch();		
						
						if(resList != null){
							add_gender_messages(arrayJson, resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) = ?2) group by p.gender",  companyId, dfrom).fetch();
																	
						if(resList != null){
							add_gender_messages(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) = ?2) group by p.gender",  companyId, dto).fetch();
																	
						if(resList != null){
							add_gender_messages(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select p.gender, count(d.textBody) as mycount from CustomerProfile p, DropifiMessage d " +
								"where (p.email = d.email) and (d.companyId = ?1) and (date(d.created) between ?2 and ?3) group by p.gender",  companyId, dfrom, dto).fetch();
																	
						if(resList != null){
							add_gender_messages(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("social") && str2.equalsIgnoreCase("0"))
				{
					if((dfrom == null) && (dto == null))
					{
						//arrayJson = CustSegSelector.custSocialNetworksToJsonArray(companyId);
						arrayJson = CustSegSelector.custSocialNetworksToJsonArray_Top(companyId, 10);
					}
					else if((dfrom != null) && (dto == null))
					{
						arrayJson = CustSegSelector.custSocialNetworksToJsonArray(companyId, dfrom);
					}
					else if((dfrom == null) && (dto != null))
					{
						arrayJson = CustSegSelector.custSocialNetworksToJsonArray(companyId, dto);
					}
					else if((dfrom != null) && (dto != null))
					{
						arrayJson = CustSegSelector.custSocialNetworksToJsonArray(companyId, dfrom, dto);
					}
				}
				else if((str1.equalsIgnoreCase("social") && str2.equalsIgnoreCase("emotions")))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.dominantEmotion, count(m.dominantEmotion)  as mycount from CustSocialProfile s, MsgSentiment m " +
								"where (m.dominantEmotion <> 'NoSentiment' ) and (s.email=m.email)  and (m.companyId = ?1) " +
								"group by s.type, m.dominantEmotion",  companyId).fetch();
						
						if(resList != null){
							add_social_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.dominantEmotion, count(m.dominantEmotion)  as mycount from CustSocialProfile s, MsgSentiment m " +
								"where (m.dominantEmotion <> 'NoSentiment' ) and (s.email=m.email) and (m.companyId = ?1) and (date(m.createdDate) = ?2) " +
								"group by s.type, m.dominantEmotion",  companyId, dfrom).fetch();
											
						if(resList != null){
							add_social_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.dominantEmotion, count(m.dominantEmotion)  as mycount from CustSocialProfile s, MsgSentiment m " +
								"where (m.dominantEmotion <> 'NoSentiment' ) and (s.email=m.email) and (m.companyId = ?1) and (date(m.createdDate) = ?2) " +
								"group by s.type, m.dominantEmotion",  companyId, dto).fetch();
						
						if(resList != null){
							add_social_emotions(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.dominantEmotion, count(m.dominantEmotion)  as mycount from CustSocialProfile s, MsgSentiment m " +
								"where (m.dominantEmotion <> 'NoSentiment' ) and (s.email=m.email) and (m.companyId = ?1) and (date(m.createdDate) between ?2 and ?3) " +
								"group by s.type, m.dominantEmotion",  companyId, dfrom, dto).fetch();
												
						if(resList != null){
							add_social_emotions(arrayJson,resList);						
						}
					}
				}
				else if((str1.equalsIgnoreCase("social") && str2.equalsIgnoreCase("sentiment")))
				{
					if((dfrom == null) && (dto == null))
					{					
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.articleSentiment, count(m.articleSentiment)  as mcount " +
								"from CustSocialProfile s, MsgSentiment m " +
								"where (m.articleSentiment <> 'NoSentiment' ) and (s.email=m.email) and (m.companyId = ?1) " +
								"group by s.type, m.articleSentiment",  companyId).fetch();
						
						if(resList != null){
							add_social_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.articleSentiment, count(m.articleSentiment)  as mcount " +
								"from CustSocialProfile s, MsgSentiment m " +
								"where (m.articleSentiment <> 'NoSentiment' ) and (s.email=m.email) and (m.companyId = ?1) and (date(m.createdDate) = ?2)   " +
								"group by s.type, m.articleSentiment",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_social_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.articleSentiment, count(m.articleSentiment)  as mcount from CustSocialProfile s, MsgSentiment m " +
								"where (m.articleSentiment <> 'NoSentiment' ) and (s.email=m.email)  and (m.companyId = ?1) and (date(m.createdDate) = ?2) " +
								"group by s.type, m.articleSentiment",  companyId, dfrom).fetch();
						
						if(resList != null){
							add_social_sentiment(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustSocialProfile.find(
								"select s.type , m.articleSentiment, count(m.articleSentiment)  as mcount from CustSocialProfile s, MsgSentiment m " +
								"where (m.articleSentiment <> 'NoSentiment' ) and (s.email=m.email) and (m.companyId = ?1) and (date(m.createdDate) between ?2 and ?3) " +
								"group by s.type, m.articleSentiment",  companyId, dfrom, dto).fetch();
												
						if(resList != null){
							add_social_sentiment(arrayJson,resList);						
						}
					}
				}
				else if(str1.equalsIgnoreCase("social") && str2.equalsIgnoreCase("messages"))
				{
					if((dfrom == null) && (dto == null))
					{
						List<Object[]> resList = CustomerProfile.find(							
								"select s.type, count(d.textBody)  as mcount from CustSocialProfile s, DropifiMessage d " +
								"where  (s.email=d.email) and (d.companyId = ?1) group by s.type order by mcount desc",  companyId).fetch();		
						
						if(resList != null){
							add_social_messages(arrayJson, resList);						
						}
					}
					else if((dfrom != null) && (dto == null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select s.type, count(d.textBody)  as mcount from CustSocialProfile s, DropifiMessage d " +
								"where  (s.email=d.email) and (d.companyId = ?1) and (date(d.created) = ?2) " +
								"group by s.type order by mcount desc",  companyId, dfrom).fetch();
																	
						if(resList != null){
							add_social_messages(arrayJson,resList);						
						}
					}
					else if((dfrom == null) && (dto != null)){
						
						List<Object[]> resList = CustomerProfile.find(							
								"select s.type, count(d.textBody)  as mcount from CustSocialProfile s, DropifiMessage d " +
								"where  (s.email=d.email) and (d.companyId = ?1) and (date(d.created) = ?2) " +
								"group by s.type order by mcount desc",  companyId, dto).fetch();
																	
						if(resList != null){
							add_social_messages(arrayJson,resList);						
						}
					}
					else if((dfrom != null) && (dto != null)){
						List<Object[]> resList = CustomerProfile.find(							
								"select s.type, count(d.textBody)  as mcount from CustSocialProfile s, DropifiMessage d " +
								"where  (s.email=d.email) and (d.companyId = ?1) and (date(d.created) between ?2 and ?3) " +
								"group by s.type order by mcount desc",  companyId, dfrom, dto).fetch();
																	
						if(resList != null){
							add_social_messages(arrayJson,resList);						
						}
					}
				}				
			}		
		}
		catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		
		return arrayJson;
	}
	
	private static void add_ageRang_location(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setAgeRange((String)obj[i++]);
			cssr.setLocation((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_ageRang_gender(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setAgeRange((String)obj[i++]);
			cssr.setGender((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_ageRang_social(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setAgeRange((String)obj[i++]);
			cssr.setSocialNetwork((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_ageRang_emotions(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setAgeRange((String)obj[i++]);
			cssr.setEmotions((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_ageRang_sentiment(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setAgeRange((String)obj[i++]);
			cssr.setSentiment((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_ageRang_messages(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			KeyValueSerialiser kval = new KeyValueSerialiser();
			kval.setType((String)obj[i++]);
			kval.setLnum((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(kval));
		}
	}
	

	private static void add_location_gender(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setLocation((String)obj[i++]);
			cssr.setGender((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_location_social(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setLocation((String)obj[i++]);
			cssr.setSocialNetwork((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_location_emotions(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setLocation((String)obj[i++]);
			cssr.setEmotions((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_location_sentiment(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setLocation((String)obj[i++]);
			cssr.setSentiment((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_location_messages(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setLocation((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	

	private static void add_gender_social(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setGender((String)obj[i++]);
			cssr.setSocialNetwork((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_gender_emotions(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setGender((String)obj[i++]);
			cssr.setEmotions((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_gender_sentiment(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setGender((String)obj[i++]);
			cssr.setSentiment((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_gender_messages(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setGender((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	
	
	private static void add_social_emotions(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setSocialNetwork((String)obj[i++]);
			cssr.setEmotions((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	private static void add_social_sentiment(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setSocialNetwork((String)obj[i++]);
			cssr.setSentiment((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	
	private static void add_social_messages(JsonArray jsa, List<Object[]> result)
	{
		Gson gson = new Gson();
		for(Object[] obj : result){
			int i = 0;
			CustSegSerialiser cssr = new CustSegSerialiser();
			cssr.setSocialNetwork((String)obj[i++]);
			cssr.setNoOfCust((Long)obj[i++]);
			
			jsa.add(gson.toJsonTree(cssr));
		}
	}
	
	
	
	/****************************************************************************/
	// Sentiment Analysis
	// Trending emotions
	public static JsonArray TrendingEmotions(Long companyId){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = MsgSentiment.find("select distinct s.dominantEmotion, (select count(e.dominantEmotion) " +
					"from MsgSentiment e where (e.dominantEmotion=s.dominantEmotion) and (e.companyId = ?1) and " +
					"(e.dominantEmotion <> 'NoSentiment')) as Mct from MsgSentiment s where (s.companyId = ?1) and " +
					"(s.dominantEmotion <> 'NoSentiment') order by Mct desc", companyId).fetch();
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			}
		}
		return json;
	}
	
	public static JsonArray TrendingEmotions(Long companyId, int days, int top){
		JsonArray json = new JsonArray();
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		 
		
		if(companyId != null)
		{			
			List<Object[]> res = MsgSentiment.find(
					"select s.dominantEmotion, count(s.dominantEmotion) as Mct " +
					"from MsgSentiment s where (s.companyId = ?1) and (s.dominantEmotion <> 'NoSentiment') " +
					"and (date(s.createdDate) >= ?2) group by s.dominantEmotion order by Mct desc", companyId, endDate).fetch(top);
			
			/*List<Object[]> res = MsgSentiment.find("select distinct s.dominantEmotion, (select count(e.dominantEmotion) " +
					"from MsgSentiment e where (e.dominantEmotion=s.dominantEmotion) and (e.companyId = ?1) and " +
					"(e.dominantEmotion <> 'NoSentiment')) as Mct from MsgSentiment s where (s.companyId = ?1) and " +
					"(s.dominantEmotion <> 'NoSentiment') order by Mct desc", companyId).fetch();*/
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			}
		}
		return json;
	}
	
	public static JsonArray TrendingEmotions(Long companyId, Timestamp from, int top){
		JsonArray json = new JsonArray();
		if(companyId != null)
		{
			List<Object[]> res = MsgSentiment.find(
					"select s.dominantEmotion, count(s.dominantEmotion) as Mct " +
					"from MsgSentiment s where (s.companyId = ?1) and (s.dominantEmotion <> 'NoSentiment') " +
					"and (date(s.createdDate) = ?2) group by s.dominantEmotion order by Mct desc", companyId, from).fetch(top);
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			}
		}
		return json;
	}
	
	public static JsonArray TrendingEmotions(Long companyId, Timestamp from ,Timestamp to, int top){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = MsgSentiment.find(
					"select s.dominantEmotion, count(s.dominantEmotion) as Mct " +
					"from MsgSentiment s where (s.companyId = ?1) and (s.dominantEmotion <> 'NoSentiment') " +
					"and (date(s.createdDate) between ?2 and ?3) group by s.dominantEmotion order by Mct desc", companyId, from, to).fetch(top);
			
			/*List<Object[]> res = MsgSentiment.find(
					"select s.dominantEmotion , s.createdDate from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
					"(s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) order by time(s.createdDate)", companyId, from, to).fetch();
			
			List<Object[]> res = MsgSentiment.find("select distinct s.dominantEmotion, (select count(e.dominantEmotion) " +
					"from MsgSentiment e where (e.dominantEmotion=s.dominantEmotion) and (e.companyId = ?1) and " +
					"(e.dominantEmotion <> 'NoSentiment') and (date(e.createdDate) between ?2 and ?3)) as Mct from MsgSentiment s where (s.companyId = ?1) and " +
					"(s.dominantEmotion <> 'NoSentiment') and (date(s.createdDate) between ?2 and ?3) order by Mct desc", companyId, from, to).fetch();*/
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			}
		}
		return json;
	}
	
	// Trending Keywords and Messages
	public static JsonArray TrendingKeywordAndMessages(Long companyId){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = MsgSentiment.find(
					"select distinct k.keyword, ( select count(e.keyword) from MsgKeyword e where (e.keyword = k.keyword) " +
					"and (e.companyId = ?1)) as Mct from MsgKeyword k where " +
					"(k.companyId = ?1) order by Mct desc", companyId).fetch();
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			} 
		}
		return json;
	}
	
	public static JsonArray TrendingKeywordAndMessages(Long companyId, int days){
		JsonArray json = new JsonArray();
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		
		if(companyId != null){
			List<Object[]> res = MsgSentiment.find(
					"select k.keyword, k.createdDate from MsgKeyword k where (k.companyId = ?1) and " +
					"(date(k.createdDate) >= ?2)", companyId, endDate).fetch();
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			}
		}
		return json;
	}
	
	public static JsonArray TrendingKeywordAndMessages(Long companyId, Timestamp from){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = MsgSentiment.find(
					"select k.keyword, k.createdDate from MsgKeyword k where (k.companyId = ?1) and " +
					"(date(k.createdDate) = ?2)", companyId, from).fetch();
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			}
		}
		return json;
	}
	
	public static JsonArray TrendingKeywordAndMessages(Long companyId, Timestamp from ,Timestamp to){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = MsgSentiment.find(
					"select k.keyword, k.createdDate from MsgKeyword k where (k.companyId = ?1) and " +
					"(date(k.createdDate) between ?2 and ?3)", companyId, from , to).fetch();
			
			Gson gson = new Gson();
			for(Object[] obj : res){
				int i = 0;
				KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);				
				json.add(gson.toJsonTree(kval));
			}
		}
		return json;
	}
	
	// Trending Top Location and Messages
	public static JsonArray TrendingTopLocationAndMessages(Long companyId){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select distinct p.locationGeneral, ( select count(d) from DropifiMessage d, CustomerProfile a, Customer b " +
					"where (d.email = a.email) and (a.email=b.email) and  (b.companyId= ?1) and (a.locationGeneral = p.locationGeneral) " +
					") as mct from CustomerProfile p, Customer c where (p.email=c.email) and (c.companyId= ?1) order by mct", companyId).fetch();
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingTopLocationAndMessages(Long companyId,  int days){
		JsonArray json = new JsonArray();
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select  p.locationGeneral, d.created from CustomerProfile p, DropifiMessage d  where  " +
					"(p.email=d.email) and (d.companyId= ?1) and (date(d.created) >= ?2)", companyId, endDate).fetch();
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingTopLocationAndMessages(Long companyId,  Timestamp from){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select  p.locationGeneral, d.created from CustomerProfile p, DropifiMessage d  where  " +
					"(p.email=d.email) and (d.companyId= ?1) and (date(d.created) = ?2)", companyId, from).fetch();
						
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingTopLocationAndMessages(Long companyId,  Timestamp from ,Timestamp to){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select  p.locationGeneral, d.created from CustomerProfile p, DropifiMessage d  where  " +
					"(p.email=d.email) and (d.companyId= ?1) and (date(d.created) between ?2 and ?3)", companyId, from,to).fetch();
						
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	
	// Trending Sentiments and Messages
	
	// Trending Sentiments and Messages
	public static JsonArray TrendingSentimentsAndMessages(Long companyId, int days)
	{
		JsonArray arrayJson = new JsonArray();	
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		 		 
		 if(companyId != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
						"select  s.articleSentiment, s.createdDate from MsgSentiment s " +
						"where  (s.articleSentiment <> 'NoSentiment') and (s.companyId = ?1) " +
						"and (date(s.createdDate) >= ?2) ", companyId,endDate).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson(); 				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++],(Timestamp)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kval));
				 }
			}
		}	
		return arrayJson;
	 }
	
	public static JsonArray TrendingSentimentsAndMessages(Long companyId, Timestamp from)
	{
		 JsonArray arrayJson = new JsonArray();	
		 		 
		 if(companyId != null && from != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
						"select  s.articleSentiment, s.createdDate from MsgSentiment s " +
						"where  (s.articleSentiment <> 'NoSentiment') and (s.companyId = ?1) " +
						"and (date(s.createdDate) = ?2) ", companyId,from).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson(); 				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++],(Timestamp)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kval));
				 }
			}
		}	
		return arrayJson;
	 }
	
	public static JsonArray TrendingSentimentsAndMessages(Long companyId, Timestamp from, Timestamp to)
	{
		 JsonArray arrayJson = new JsonArray();	
		 		 
		 if(companyId != null && from != null)
		 {
			 List<Object[]> resList = MsgSentiment.find(
						"select  s.articleSentiment, s.createdDate from MsgSentiment s " +
						"where  (s.articleSentiment <> 'NoSentiment') and (s.companyId = ?1) " +
						"and (date(s.createdDate) between ?2 and ?3) ", companyId,from, to).fetch();
			 
			 if(resList != null){
				 Gson gson = new Gson(); 				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++],(Timestamp)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kval));
				 }
			}
		}	
		return arrayJson;
	 }
	
	
	// Trending Age and Messages
	public static JsonArray TrendingAgeAndMessages(Long companyId){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select distinct p.ageRange,( select count(*) from DropifiMessage d, CustomerProfile a, Customer b " +
					"where  (a.ageRange= p.ageRange) and (d.email = a.email) and (a.email=b.email) and  (b.companyId= ?1) " +
					") as mct from CustomerProfile p, Customer c where (p.email=c.email) and (c.companyId= ?1)", companyId).fetch();
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	
	public static JsonArray TrendingAgeAndMessages(Long companyId, Timestamp from){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select p.ageRange, d.created from CustomerProfile p, DropifiMessage d " +
					"where (p.email=d.email) and (d.companyId=?1) and (date(d.created) = ?2) " +
					"order by time(d.created)", companyId, from).fetch();
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;		
	}
	
	public static JsonArray TrendingAgeAndMessages(Long companyId, int days){
		JsonArray json = new JsonArray();
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select p.ageRange, d.created from CustomerProfile p, DropifiMessage d " +
					"where (p.email=d.email) and (d.companyId=?1) and (date(d.created) >= ?2) " +
					"order by time(d.created)", companyId, endDate).fetch();
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;		
	}
	
	public static JsonArray TrendingAgeAndMessages(Long companyId, Timestamp from, Timestamp to){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select p.ageRange, d.created from CustomerProfile p, DropifiMessage d " +
					"where (p.email=d.email) and (d.companyId=?1) and (date(d.created) between ?2 and ?3) " +
					"order by time(d.created)", companyId, from, to).fetch();
			
			/*List<Object[]> res = CustomerProfile.find(
					"select distinct p.ageRange,( select count(*) from DropifiMessage d, CustomerProfile a, Customer b " +
					"where  (a.ageRange= p.ageRange) and (d.email = a.email) and (a.email=b.email) and  (b.companyId= ?1) and (date(b.updated) between ?2 and ?3) " +
					") as mct from CustomerProfile p, Customer c where (p.email=c.email) and (c.companyId= ?1) and (date(c.updated) between ?2 and ?3)", companyId,from,to).fetch();
			*/
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;		
	}
	
	// Trending Social Network and Messages
	public static JsonArray TrendingSocialsAndMessages(Long companyId){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(							
					"select distinct s.type,(select count(a) from DropifiMessage a, CustSocialProfile b, CustomerProfile d, Customer  e " +
					"where (b.type=s.type) and (a.email=b.email) and (b.email=d.email) and (d.email=e.email) and (e.companyId=?1) " +
					") as mcount from CustSocialProfile s, CustomerProfile p, Customer c where  (s.email=p.email) and (p.email=c.email) " +
					"and (c.companyId = ?1) order by mcount",  companyId).fetch();	
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	
	public static JsonArray TrendingSocialsAndMessages(Long companyId, int days){
		JsonArray json = new JsonArray();
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(							
					"select s.type, d.created from CustSocialProfile s, DropifiMessage d " +
					"where (s.email=d.email) and (d.companyId = ?1) and (date(d.created) >= ?2) " +
					"order by time(d.created)",  companyId, endDate).fetch();	
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingSocialsAndMessages(Long companyId, Timestamp from){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(							
					"select s.type, d.created from CustSocialProfile s, DropifiMessage d " +
					"where (s.email=d.email) and (d.companyId = ?1) and (date(d.created) = ?2) " +
					"order by time(d.created)",  companyId, from).fetch();	
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	
	public static JsonArray TrendingSocialsAndMessages(Long companyId, Timestamp from, Timestamp to){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(							
					"select s.type, d.created from CustSocialProfile s, DropifiMessage d " +
					"where (s.email=d.email) and (d.companyId = ?1) and (date(d.created) between ?2 and ?3) " +
					"order by time(d.created)",  companyId, from,to).fetch();	
			/*List<Object[]> res = CustomerProfile.find(							
					"select distinct s.type,(select count(a) from DropifiMessage a, CustSocialProfile b, CustomerProfile d, Customer  e " +
					"where (b.type=s.type) and (a.email=b.email) and (b.email=d.email) and (d.email=e.email) and (e.companyId=?1)  and (date(e.created) between ?2 and ?3)" +
					") as mcount from CustSocialProfile s, CustomerProfile p, Customer c where  (s.email=p.email) and (p.email=c.email) " +
					"and (c.companyId = ?1)  and (date(c.created) between ?2 and ?3) order by mcount",  companyId, from, to).fetch();	*/
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				} 
			}
		}
		return json;
	}
	
	public static JsonArray DefaultSocialAndMessage(Long companyId, int noOfDays){
		JsonArray json = new JsonArray();
		DateTime dt = DateTime.now().minusDays(noOfDays);
		Timestamp endDate = new Timestamp(dt.getMillis());
		
		List<Object[]> resList = CustSocialProfile.find(	
			"select s.type , m.articleSentiment, count(m.articleSentiment)  as mcount " +
			"from CustSocialProfile s, MsgSentiment m " +
			"where (m.articleSentiment <> 'NoSentiment' ) and (s.email=m.email) and (m.companyId = ?1) and (date(m.createdDate) >= ?2)   " +
			"group by s.type, m.articleSentiment",  companyId, endDate).fetch();
		
		/*"select s.type , m.articleSentiment, count(m.articleSentiment) as Mycount " +
		"from CustSocialProfile s, MsgSentiment m, CustomerProfile p, Customer c " +
		"where (m.articleSentiment <> 'NoSentiment' ) and (m.email=s.email) and " +
		"(s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(m.createdDate) >= ?2) " +
		"group by m.articleSentiment, s.type " +
		"order by s.type, m.articleSentiment",  companyId, endDate).fetch();*/
		
		
		
		/*List<Object[]> resList = CustSocialProfile.find(
				"select distinct s.type , m.articleSentiment,(select count(a.articleSentiment) from MsgSentiment a, CustSocialProfile b, " +
				"CustomerProfile d, Customer e  where  (a.articleSentiment <> 'NoSentiment') and  (a.articleSentiment = m.articleSentiment) " +
				"and (b.type = s.type) and (a.email=b.email) and (b.email=d.email) and (d.email=e.email) and (e.companyId=?1)) as mcount " +
				"from CustSocialProfile s, MsgSentiment m, CustomerProfile p, Customer c where (m.articleSentiment <> 'NoSentiment' ) " +
				"and (m.email=s.email) and (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1)",  companyId).fetch();*/
		
		if(resList != null){
			add_social_sentiment(json,resList);						
		}
		return json;
	}
	/*public static JsonArray DefaultSocialAndMessage(Long companyId){
		JsonArray json = new JsonArray();
		List<Object[]> resList = CustSocialProfile.find(
				"select s.type , m.articleSentiment, count(m.articleSentiment) as Mycount " +
				"from CustSocialProfile s, MsgSentiment m, CustomerProfile p, Customer c " +
				"where (m.articleSentiment <> 'NoSentiment' ) and (m.email=s.email) and " +
				"(s.email=p.email) and (p.email=c.email) and (c.companyId = ?1) and (date(m.createdDate) >= '2012-08-28')" +
				"group by m.articleSentiment, s.type " +
				"order by s.type, m.articleSentiment",  companyId).fetch();
		
		List<Object[]> resList = CustSocialProfile.find(
				"select distinct s.type , m.articleSentiment,(select count(a.articleSentiment) from MsgSentiment a, CustSocialProfile b, " +
				"CustomerProfile d, Customer e  where  (a.articleSentiment <> 'NoSentiment') and  (a.articleSentiment = m.articleSentiment) " +
				"and (b.type = s.type) and (a.email=b.email) and (b.email=d.email) and (d.email=e.email) and (e.companyId=?1)) as mcount " +
				"from CustSocialProfile s, MsgSentiment m, CustomerProfile p, Customer c where (m.articleSentiment <> 'NoSentiment' ) " +
				"and (m.email=s.email) and (s.email=p.email) and (p.email=c.email) and (c.companyId = ?1)",  companyId).fetch();
		
		if(resList != null){
			add_social_sentiment(json,resList);						
		}
		return json;
	}*/
	
	
	
	// Trending Emotions and Messages
	public static JsonArray TrendingEmotionsAndMessages(Long companyId){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(							
					"select distinct s.dominantEmotion, count(s.dominantEmotion) as mct from MsgSentiment s, Customer c " +
					"where  (s.dominantEmotion <> 'NoSentiment') and (s.email=c.email) and (c.companyId = ?1) group by s.dominantEmotion " +
					"order by mct desc",  companyId).fetch();	
			
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Long)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingEmotionsAndMessages(Long companyId, int days){
		JsonArray json = new JsonArray();
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select s.dominantEmotion , s.createdDate from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
					"(s.companyId = ?1) and (date(s.createdDate) >= ?2) order by time(s.createdDate)", companyId, endDate).fetch();		
						
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingEmotionsAndMessages(Long companyId, Timestamp from){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select s.dominantEmotion , s.createdDate from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
					"(s.companyId = ?1) and (date(s.createdDate) = ?2) order by time(s.createdDate)", companyId, from).fetch();		
						
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingEmotionsAndMessages(Long companyId, Timestamp from, Timestamp to){
		JsonArray json = new JsonArray();
		if(companyId != null){
			List<Object[]> res = CustomerProfile.find(
					"select s.dominantEmotion , s.createdDate from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
					"(s.companyId = ?1) and (date(s.createdDate) between ?2 and ?3) order by time(s.createdDate)", companyId, from,to).fetch();	
						
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
		
	// Widget subject trending
	public static JsonArray TrendingWidgetSubjectsAndMessages_New(Long companyId, int days, Timestamp from, Timestamp to)
	{
		JsonArray json = new JsonArray();
		if(companyId != null)
		{
			List<Object[]> res = null;
			String widgetSubjects = MessageManager.queryMsgSubjects(companyId);
			
			if(days == 0 && from == null && to == null)
			{
				res = CustomerProfile.find("select distinct d.subject, count(d) as mct from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) group by d.subject", companyId).fetch();				
			}
			else if(days != 0 && from == null && to == null)
			{
				DateTime dateTime = DateTime.now().minusDays(days);
				Timestamp endDate = new Timestamp(dateTime.getMillis());
				
				res = CustomerProfile.find("select  d.subject, d.created from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) and (date(d.created) >= ?2) ",  companyId, endDate).fetch();
			}
			else if(days == 0 && from != null && to == null)
			{
				res = CustomerProfile.find("select  d.subject, d.created from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) and (date(d.created) = ?2) ",  companyId, from).fetch();	
			}
			else if(days == 0 && from == null && to != null)
			{
				res = CustomerProfile.find("select  d.subject, d.created from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) and (date(d.created) = ?2) ",  companyId, to).fetch();
			}
			else if(days == 0 && from != null && to != null)
			{
				res = CustomerProfile.find(							
						"select  d.subject, d.created from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) and (date(d.created) between ?2 and ?3) ",  companyId, from, to).fetch();
			}
			
			
			if(res != null)
			{
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		
		return json;
	}
	
	public static JsonArray TrendingWidgetSubjectsAndMessages(Long companyId){
		JsonArray json = new JsonArray();
		if(companyId != null){
			//Retrive list of widget subject set by the user.
			String widgetSubjects = MessageManager.queryMsgSubjects(companyId);
			
			if(widgetSubjects != null)
			{
				List<Object[]> res = CustomerProfile.find(							
						"select distinct d.subject, count(d) as mct from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) group by d.subject", companyId).fetch();	
				
				if(res != null){
					Gson gson = new Gson();
					for(Object[] obj:res){
						int i = 0;
						//play.Logger.log4j.info( "hurry!!!!!: "  + (String)obj[i++]);
						KeyValueSerialiser kval = new KeyValueSerialiser((String.valueOf(obj[i++])), (Long)obj[i++]);
						json.add(gson.toJsonTree(kval));
					}
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingWidgetSubjectsAndMessages(Long companyId, Timestamp from){
		JsonArray json = new JsonArray();
		
		if(companyId != null){
			//Retrive list of widget subject set by the user.
			String widgetSubjects = MessageManager.queryMsgSubjects(companyId);
			
			if(widgetSubjects != null)
			{
				List<Object[]> res = CustomerProfile.find(							
						"select  d.subject, d.created from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) and (date(d.created) = ?2) ",  companyId, from).fetch();	
				
				if(res != null){
					Gson gson = new Gson();
					for(Object[] obj:res){
						int i = 0;
						KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
						json.add(gson.toJsonTree(kval));
					}
				}
			}
		}
		return json;
	}
	public static JsonArray TrendingWidgetSubjectsAndMessages(Long companyId, int days){
		JsonArray json = new JsonArray();
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
				
		if(companyId != null){
			//Retrive list of widget subject set by the user.
			String widgetSubjects = MessageManager.queryMsgSubjects(companyId);
			
			if(widgetSubjects != null)
			{
				List<Object[]> res = CustomerProfile.find(							
						"select  d.subject, d.created from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) and (date(d.created) >= ?2) ",  companyId, endDate).fetch();	
				
				if(res != null){
					Gson gson = new Gson();
					for(Object[] obj:res){
						int i = 0;
						KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
						json.add(gson.toJsonTree(kval));
					}
				}
			}
		}
		return json;
	}
	
//	public static JsonArray TrendingWidgetSubjectsAndMessages(Long companyId, int days){
//		JsonArray json = new JsonArray();
//		DateTime dateTime = DateTime.now().minusDays(days);
//		Timestamp endDate = new Timestamp(dateTime.getMillis());
//		
//		if(companyId != null){
//			List<Object[]> res = CustomerProfile.find(							
//					"select  d.subject, d.created from DropifiMessage d where (d.sourceOfMsg='Widget Message') " +
//					"and (d.companyId= ?1) and (date(d.created) >= ?2) ",  companyId, endDate).fetch();	
//			
//			if(res != null){
//				Gson gson = new Gson();
//				for(Object[] obj:res){
//					int i = 0;
//					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
//					json.add(gson.toJsonTree(kval));
//				}
//			}
//		}
//		return json;
//	}
	
	public static JsonArray TrendingWidgetSubjectsAndMessages(Long companyId, Timestamp from, Timestamp to){
		JsonArray json = new JsonArray();
		if(companyId != null){
			//Retrive list of widget subject set by the user.
			String widgetSubjects = MessageManager.queryMsgSubjects(companyId);
			
			if(widgetSubjects != null)
			{
				List<Object[]> res = CustomerProfile.find(							
						"select  d.subject, d.created from DropifiMessage d where (d.subject in ("+ widgetSubjects +")) " +
						"and (d.companyId= ?1) and (date(d.created) between ?2 and ?3) ",  companyId, from,to).fetch();
				
				if(res != null){
					Gson gson = new Gson();
					for(Object[] obj:res){
						int i = 0;
						KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
						json.add(gson.toJsonTree(kval));
					}
				}
			}
		}
		return json;
	}
	
	
	
	// Resolution Rates
	public static Double ResolutionRate(Long companyId){
		KeyValueSerialiser kval = new KeyValueSerialiser();
		if(companyId != null){			
			Object[] res = DropifiMessage.find(
					"select distinct (select count(d) from DropifiMessage d where  " +
					"(d.companyId=a.companyId) and (d.status= ?2)) as resolved, " +
					"(select count(m) from DropifiMessage m where (m.companyId = a.companyId)) as total " +
					"from DropifiMessage a where (a.companyId= ?1)", companyId, MsgStatus.Resolved ).first();
			int i = 0;
			kval.setDnum(0.0);			
			if(res != null){
				kval.setDnum(CustSegSelector.sentimentPercentage(Double.parseDouble(String.valueOf(res[i++])), Double.parseDouble(String.valueOf(res[i++]))));	
			}
			
		}
		return kval.getDnum();
	}
	
	public static Double ResolutionRate(Long companyId, int days)
	{
		DateTime dateTime = DateTime.now().minusDays(days);
		Timestamp endDate = new Timestamp(dateTime.getMillis());
		KeyValueSerialiser kval = new KeyValueSerialiser();
		
		if(companyId != null){			
			Object[] res = DropifiMessage.find(
					"select distinct (select count(d) from DropifiMessage d where  " +
					"(d.companyId=a.companyId) and (d.status= ?2)) as resolved, " +
					"(select count(m) from DropifiMessage m where (m.companyId = a.companyId)) as total " +
					"from DropifiMessage a where (a.companyId= ?1) and (date(a.created) >= ?3)", companyId, MsgStatus.Resolved,endDate ).first();
			int i = 0;
			kval.setDnum(0.0);			
			if(res != null){
				kval.setDnum(CustSegSelector.sentimentPercentage(Double.parseDouble(String.valueOf(res[i++])), Double.parseDouble(String.valueOf(res[i++]))));	
			}
			
		}
		return kval.getDnum();
	}
	
	public static Double ResolutionRate(Long companyId, Timestamp from){
		KeyValueSerialiser kval = new KeyValueSerialiser();
		if(companyId != null){			
			Object[] res = DropifiMessage.find(
					"select distinct (select count(d) from DropifiMessage d where  " +
					"(d.companyId=a.companyId) and (d.status= ?2)) as resolved, " +
					"(select count(m) from DropifiMessage m where (m.companyId = a.companyId)) as total " +
					"from DropifiMessage a where (a.companyId= ?1)  and (date(a.created) = ?3)", companyId, MsgStatus.Resolved, from).first();
			int i = 0;
			kval.setDnum(0.0);			
			if(res != null){
				kval.setDnum(CustSegSelector.sentimentPercentage(Double.parseDouble(String.valueOf(res[i++])), Double.parseDouble(String.valueOf(res[i++]))));	
			}
			
		}
		return kval.getDnum();
	}
	
	public static Double ResolutionRate(Long companyId, Timestamp from, Timestamp to){
		KeyValueSerialiser kval = new KeyValueSerialiser();
		if(companyId != null){			
			Object[] res = DropifiMessage.find(
					"select distinct (select count(d) from DropifiMessage d where  " +
					"(d.companyId=a.companyId) and (d.status = ?2)) as resolved, " +
					"(select count(m) from DropifiMessage m where (m.companyId = a.companyId)) as total " +
					"from DropifiMessage a where (a.companyId = ?1)  and (date(a.created) between ?3 and ?4)", companyId, MsgStatus.Resolved, from, to).first();
			int i = 0;
			kval.setDnum(0.0);			
			if(res != null){
				kval.setDnum(CustSegSelector.sentimentPercentage(Double.parseDouble(String.valueOf(res[i++])), Double.parseDouble(String.valueOf(res[i++]))));	
			}
			
		}
		return kval.getDnum();		
	}
	
	
	
	//MESSAGES
	public static Long TotalMessageInMailboxes(Long companyId, String mailbox, Timestamp from)
	{
		KeyValueSerialiser kval = new KeyValueSerialiser();
		if(companyId != null){			
			Object[] res = DropifiMessage.find(
					"select count(m) from DropifiMessage m where (m.companyId = ?1) " +
					"and m.sourceOfMsg in ("+ mailbox +")", companyId, from).first();
			int i = 0;
			kval.setLnum(0L);			
			if(res != null){
				kval.setLnum((Long)res[i++]);	
			}
			
		}
		return kval.getLnum();
	}
	
	/************************************************************************************************/
	// Trending issues for a contact
	// Trending emotions on messages from a contact
	public static JsonArray TrendingEmotionsAndMessagesFromContact(Long companyId, String contactEmail,  Timestamp from, Timestamp to)
	{
		JsonArray json = new JsonArray();
		
		if(companyId != null && contactEmail != null)
		{
			List<Object[]> res = null;
			if(from != null && to == null)
			{
				res = CustomerProfile.find("select s.dominantEmotion , s.createdDate from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
						"(s.companyId = ?1) and (s.email = ?2) and (date(s.createdDate) = ?3) order by time(s.createdDate)", companyId, contactEmail, from).fetch();
			}
			else if(from != null && to != null)
			{
				res = CustomerProfile.find("select s.dominantEmotion , s.createdDate from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
						"(s.companyId = ?1) and (s.email = ?2) and (date(s.createdDate) between ?3 and ?4) order by time(s.createdDate)", companyId, contactEmail, from, to).fetch();
			}
			else if(from == null && to == null)
			{
				/*res = CustomerProfile.find("select s.dominantEmotion , date(s.createdDate), count(date(s.createdDate)) from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
						"(s.companyId = ?1) and (s.email = ?2) group by s.dominantEmotion, date(s.createdDate) order by date(s.createdDate)", companyId, contactEmail).fetch();*/
				res = CustomerProfile.find("select s.dominantEmotion , s.createdDate from MsgSentiment s where  (s.dominantEmotion <> 'NoSentiment') and " +
						"(s.companyId = ?1) and (s.email = ?2) order by time(s.createdDate)", companyId, contactEmail).fetch();
			}					
						
			if(res != null){
				Gson gson = new Gson();
				for(Object[] obj:res){
					int i = 0;
					//KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++], (Long)obj[i++]);
					KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++], (Timestamp)obj[i++]);
					json.add(gson.toJsonTree(kval));
				}
			}
		}
		return json;
	}
	
	public static JsonArray TrendingSentimentsAndMessages(Long companyId,String contactEmail, Timestamp from, Timestamp to)
	{
		 JsonArray arrayJson = new JsonArray();	
		 if(companyId != null && contactEmail != null)
		 {
			 List<Object[]> resList =null;		
			 
			 if(to != null && from != null)
			 {
				  resList = MsgSentiment.find(
						"select  s.articleSentiment, s.createdDate from MsgSentiment s " +
						"where  (s.articleSentiment <> 'NoSentiment') and (s.companyId = ?1) " +
						"and (date(s.createdDate) between ?2 and ?3) and s.email=?4 order by time(s.createdDate)", companyId,from, to,contactEmail).fetch();
			}
			else if(from == null && to == null)	
			{
				 resList = MsgSentiment.find(
							"select  s.articleSentiment, s.createdDate from MsgSentiment s " +
							"where  (s.articleSentiment <> 'NoSentiment') and (s.companyId = ?1) " +
							"and s.email=?2 order by time(s.createdDate)", companyId,contactEmail).fetch();
			}
			
			 if(resList != null){
				 Gson gson = new Gson(); 				 				 
				 for(Object[] obj : resList){
					 int i = 0;					 
					 KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++],(Timestamp)obj[i++]);					 
					 arrayJson.add(gson.toJsonTree(kval));
				 }
			}
		 }
		return arrayJson;
	 }
	
	// Trending Sentiments and Messages
		public static JsonArray TrendingSentimentsAndMessages(Long companyId,String contactEmail, int days)
		{
			JsonArray arrayJson = new JsonArray();	
			DateTime dateTime = DateTime.now().minusDays(days);
			Timestamp endDate = new Timestamp(dateTime.getMillis());
			 		 
			 if(companyId != null)
			 {
				 List<Object[]> resList = MsgSentiment.find(
							"select  s.articleSentiment, s.createdDate from MsgSentiment s " +
							"where  (s.articleSentiment <> 'NoSentiment') and (s.companyId = ?1) " +
							"and (date(s.createdDate) >= ?2) and s.email=?3 order by time(s.createdDate)", companyId,endDate,contactEmail).fetch();
				 
				 if(resList != null){
					 Gson gson = new Gson(); 				 				 
					 for(Object[] obj : resList){
						 int i = 0;					 
						 KeyValueSerialiser kval = new KeyValueSerialiser((String)obj[i++],(Timestamp)obj[i++]);					 
						 arrayJson.add(gson.toJsonTree(kval));
					 }
				}
			}	
			return arrayJson;
		 }
	
}
