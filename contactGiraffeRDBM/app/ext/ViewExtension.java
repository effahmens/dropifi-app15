/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ext;

/**
 *
 * @author david osei
 * 
 */

public class ViewExtension extends play.templates.JavaExtensions {
    
    public static String isSelect(String page, String current){ 
    	if (page.equals(current)){
            return "selected";
        }else{
            return "";
        }
    }
    
    public static String isHover(String page, String current){
	    if (page.equals(current)){
	        return "_hover.png";
	    }else{
	        return ".png";
	    } 
    }
    
    public static String isAnalytic(String page, String current){
	    if (page.equals(current)){
	        return "analytics_selected";
	    }else{
	        return "";
	    } 
    }
    
    public static String isSubSelect(String page, String current){
        if (page.equals(current)){
            return "sub_head_selected";
        }else{
            return "";
        } 
    }
    
}
