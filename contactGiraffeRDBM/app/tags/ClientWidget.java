package tags;  
import groovy.lang.Closure; 
import play.templates.*; 
import play.templates.GroovyTemplate.ExecutableTemplate;
import java.util.*; 
import java.io.PrintWriter;  

import models.Widget;
import models.WidgetControl;
import models.WidgetDefault;

@FastTags.Namespace("clientWidget") 
public class ClientWidget extends play.templates.FastTags {  
	
	public static void _show (Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		
		String apikeyValue =args.get("apikey").toString(); 
		
		if(!apikeyValue.equalsIgnoreCase("null")){
			 	
				//select the company widget profile using the apikey
				
				//Widget widget = Widget.findByApiKey(apikeyValue); 
				WidgetDefault widget = WidgetDefault.findByPublicKey(apikeyValue);
				if(widget !=null){
					out.println(widget.getHtml());  
				}else{					
					//there is know account associated with this apikey, send a message to the user
					//notifying of the error
					out.println("<h1>REFRESH THE PAGE</h1>");					
				}
			
		}else{
				out.print("<h1>Hi, "+" we don't know you.</h1>");			
		}
	} 
	
}
