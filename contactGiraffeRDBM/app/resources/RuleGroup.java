package resources;

public enum RuleGroup {
	ALL,
	ANY;
	
	public static boolean isAll(String value){
		return value.equalsIgnoreCase(RuleGroup.ALL.name());
	}
	
	public static boolean isAny(String value){
		return value.equalsIgnoreCase(RuleGroup.ANY.name());
	}
	
	public static RuleGroup getType(String value){
		if(value.equalsIgnoreCase(RuleGroup.ANY.name())){
			return RuleGroup.ANY;
		}else if(value.equalsIgnoreCase(RuleGroup.ALL.name())){
			return RuleGroup.ALL;
		} 
		return null; 
	}
	
	public static RuleGroup getType(int value){
		if(value == RuleGroup.ANY.ordinal()){
			return RuleGroup.ANY;
		}else if(value==RuleGroup.ALL.ordinal()){
			return RuleGroup.ALL;
		} 
		return null; 
	}
	
}
