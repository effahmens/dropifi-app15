package resources;

public enum MsgStatus {	
	New,
	Open,
	Pending,
	Resolved;
	
	public static MsgStatus typeOfStatus(String status){
		if(MsgStatus.New.name().equalsIgnoreCase(status))
			return MsgStatus.New;
		else if(MsgStatus.Open.name().equalsIgnoreCase(status))
			return MsgStatus.Open;
		if(MsgStatus.Pending.name().equalsIgnoreCase(status))
			return MsgStatus.Pending;
		else if(MsgStatus.Resolved.name().equalsIgnoreCase(status))
			return MsgStatus.Resolved;		 
		
		return null;
	}
}
