package resources;

import static akka.actor.Actors.actorOf;

import java.util.Date;

import job.process.EventTrackActor;

import org.apache.http.HttpResponse;

import resources.addons.AddonStatus;
import resources.addons.SocialPlatformType;
import resources.addons.SocialReviewClient;
import resources.dstripe.StCustomer;
import models.Company;
import models.addons.AddonDownGrade;
import models.addons.AddonPlatform;
import models.addons.AddonSubscription;
import models.dsubscription.SubCustomer;
import akka.actor.ActorRef;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException; 
import com.stripe.model.Customer;
import com.stripe.model.Subscription; 

import controllers.Addons;

public class AddonHelper {
	/***
	 * Install Social Review by enforcing subscription. In this case the customer has to
	 * provide debit/credit card details in case it does not exist.
	 * @param apikey
	 * @param platformKey
	 * @param title
	 * @param token
	 * @param coupon
	 * @return
	 */
	@Deprecated
	public static int installSocialReviewAddon(String apikey,String platformKey,String title,String token,String coupon){
		SubCustomer customer = SubCustomer.findByAppikey(apikey);
		if(customer!=null && customer.getCustomerId()!=null && !customer.getCustomerId().trim().isEmpty()){
			//confirm that the user is created in stripe
			Customer cus =  new  StCustomer(DropifiTools.StripeSecretKey).retrieveCustomer(customer.getCustomerId());
			if(cus!=null && cus.getId().equals(customer.getCustomerId())){
				//play.Logger.log4j.info("Testing over");
				AddonPlatform platform =null;
				if(platformKey!=null && !platformKey.isEmpty())
				  platform = AddonPlatform.findByPlatformKey(platformKey);
				else if(title!=null && !title.isEmpty())
				  platform = AddonPlatform.findByTitle(title);
				
				if(platform!=null){ 
					//connect to kudobuzz and create a user account
					Company company = Company.findByApikey(apikey);
					
					if(company!=null){
						//check if the user addon subscription is created
						AddonSubscription addon = AddonSubscription.findAddon(apikey, platformKey);
						SocialReviewClient client = null;
						
						if(addon==null || addon.getSecretKey()==null){
							 client = SocialReviewClient.getSocialUID(company.getEmail());
						}else{
							 client = SocialReviewClient.getSocialUID(addon.getSecretKey(),company.getEmail());
						} 
						
						 String url = company.getEcomBlogPlugin().getEb_pluginType().equals(EcomBlogType.Shopify)?company.getEcomBlogPlugin().getEb_name():company.getProfile().getUrl();
						 String shop_id =company.getEcomBlogPlugin().getEb_pluginType().equals(EcomBlogType.Shopify)? company.getEcomBlogPlugin().getEb_apikey():null;
						 
						 /**
						  * Temporal provision for resolving create account url;
						  * url +=url+"/dropifistatic";
						  **/
						 url +=url+"/dropifistatic_"+new Date().getTime();
						 if(client==null || client.getSuccess()==0){
							 client = SocialReviewClient.createAccount(company.getEmail(), "", url,
									 SocialPlatformType.valueOf(company.getEcomBlogPlugin().getEb_pluginType().name()).ordinal() ,
									 "", "",shop_id, company.getDomain());
							 
							// play.Logger.log4j.info("Create client: ["+client.toString()+"]");
						 }else{
							 //play.Logger.log4j.info("Get client: ["+client.toString()+"]");
							 SocialReviewClient uclient = client.updateSocialAccount(client.getUid(), company.getEmail(), "", url, SocialPlatformType.valueOf(company.getEcomBlogPlugin().getEb_pluginType().name()).ordinal() , company.getDomain()); 
							  
							 if(uclient!=null && uclient.getSuccess()==1)
								 client = uclient;
							 
							// play.Logger.log4j.info("Update client: ["+client.toString()+"]");
						 } 
						 
						//check if the user account has been created on dropifi
						if(client!=null && client.getSuccess()==1){
							
							//subscribe the user to the selected addon plan
							Subscription sub  = null;
							if(addon==null || (addon.getSubscriptionId()==null||addon.getSubscriptionId().trim().isEmpty()))
								 sub = customer.subscribeToAddon(DropifiTools.StripeSecretKey, platform.getSubscriptionPlan(),coupon);
							else
								sub = customer.updateAddonSubscription(DropifiTools.StripeSecretKey, addon.getSubscriptionId(), platform.getSubscriptionPlan());

							if(sub!=null){		 
								addon = new AddonSubscription(platform.getPlatformKey(),customer.getApikey(), client.getUid(), client.getUser_id(),client.getAccount_id(), customer.getCustomerId(), sub.getId());
								addon = addon.createAddon();
								
								if(addon!=null && addon.getId()!=null){
									//play.Logger.log4j.info("Addon created or updated");   
									 EventTrackSerializer.trackValueActor(company.getEmail(),"SocialReview",addon.getStatus().name()); 
									return 200;
								}
							}else{return 501;}
						}
						return 201;
					}
				 
				}else{//addon platform not created
					return 301;
				} 
			} 
		}
		//customer account is not created 
		return 501;
	} 
	/**
	 * Install a Social Review APP without enforcing subscription to a plan.
	 * AddonDowngrade is added
	 * @param apikey
	 * @param platformKey
	 * @param title
	 * @param token
	 * @param coupon
	 * @return
	 */
	public static int installSocialReview(String apikey,String platformKey,String title,String token,String coupon){
		
		//play.Logger.log4j.info("Testing over");
		AddonPlatform platform =null;
		if(platformKey!=null && !platformKey.isEmpty())
		  platform = AddonPlatform.findByPlatformKey(platformKey);
		else if(title!=null && !title.isEmpty())
		  platform = AddonPlatform.findByTitle(title);
		
		if(platform!=null){ 
			//connect to kudobuzz and create a user account
			Company company = Company.findByApikey(apikey);
			
			if(company!=null){
				//check if the user addon subscription is created
				AddonSubscription addon = AddonSubscription.findAddon(apikey, platformKey);
				SocialReviewClient client = null;
				
				if(addon==null || addon.getSecretKey()==null){
					 client = SocialReviewClient.getSocialUID(company.getEmail());
				}else{
					 client = SocialReviewClient.getSocialUID(addon.getSecretKey(),company.getEmail());
				} 
				
				 String url = company.getEcomBlogPlugin().getEb_pluginType().equals(EcomBlogType.Shopify)?company.getEcomBlogPlugin().getEb_name():company.getProfile().getUrl();
				 String shop_id =company.getEcomBlogPlugin().getEb_pluginType().equals(EcomBlogType.Shopify)? company.getEcomBlogPlugin().getEb_apikey():null;
				 
				 /**
				  * Temporal provision for resolving create account url;
				  * url +=url+"/dropifistatic";
				  **/
				 url +=url+"/dropifistatic_"+new Date().getTime();
				 if(client==null || client.getSuccess()==0){
					 client = SocialReviewClient.createAccount(company.getEmail(), "", url,
							 SocialPlatformType.valueOf(company.getEcomBlogPlugin().getEb_pluginType().name()).ordinal() ,
							 "", "",shop_id, company.getDomain());
					 
					// play.Logger.log4j.info("Create client: ["+client.toString()+"]");
				 }else{
					 //play.Logger.log4j.info("Get client: ["+client.toString()+"]");
					 SocialReviewClient uclient = client.updateSocialAccount(client.getUid(), company.getEmail(), "", url, SocialPlatformType.valueOf(company.getEcomBlogPlugin().getEb_pluginType().name()).ordinal() , company.getDomain()); 
					  
					 if(uclient!=null && uclient.getSuccess()==1)
						 client = uclient; 
					 
					// play.Logger.log4j.info("Update client: ["+client.toString()+"]"); 
				} 
				 
				//check if the user account has been created on dropifi
				if(client!=null && client.getSuccess()==1){
					SubCustomer customer = SubCustomer.findByAppikey(apikey);
					if(customer!=null && customer.getCustomerId()!=null && !customer.getCustomerId().trim().isEmpty()){
						//confirm that the user is created in stripe
						Customer cus =  new  StCustomer(DropifiTools.StripeSecretKey).retrieveCustomer(customer.getCustomerId());
						if(cus==null || cus.getId().equals(customer.getCustomerId())){
							customer =null;
						}
					}
					
					//subscribe the user to the selected addon plan if the user has debit/credit card in stripe
					Subscription sub  = null;
					String customerId=null,subId=null;
					AddonStatus addonStatus = AddonStatus.Downgraded;
					if(customer!=null && (addon==null || (addon.getSubscriptionId()==null||addon.getSubscriptionId().trim().isEmpty())))
						 sub = customer.subscribeToAddon(DropifiTools.StripeSecretKey, platform.getSubscriptionPlan(),coupon);
					else if(customer!=null)
						sub = customer.updateAddonSubscription(DropifiTools.StripeSecretKey, addon.getSubscriptionId(), platform.getSubscriptionPlan());

					if(sub!=null){		
						subId = sub.getId();
						customerId =customer.getCustomerId();
						addonStatus = AddonStatus.Installed;
					}
					
					addon = new AddonSubscription(platform.getPlatformKey(),apikey, client.getUid(), client.getUser_id(),client.getAccount_id(),customerId, subId).createAddon(); 
					
					if(addon!=null && addon.getId()!=null){
						new AddonDownGrade(addon.getPlatformKey(), addon.getApikey()).createDownGrade(30,addonStatus); 
						//play.Logger.log4j.info("Addon created or updated");   
						EventTrackSerializer.trackValueActor(company.getEmail(),"SocialReview",addon.getStatus().name()); 
						return 200;
					}
				}
				return 201;
			}
		 
		}
		//addon platform not created
		return 301; 
		
		//customer account is not created 
		//return 501;
	} 
	
	public static void updateEmail(String apikey,String email){
		try{
			//check if the user has Social Review install and change the email;
			AddonSubscription addonSub = AddonSubscription.findAddonByTitle(apikey,DropifiTools.SOCIAL_REVIEW,AddonStatus.Installed);
			if(addonSub!=null){
				//play.Logger.log4j.info("uid: "+addonSub.getUid()+", email="+email);
				SocialReviewClient.updateEmail(addonSub.getUid(), email);
				//play.Logger.log4j.info("Updated: "+client);
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		} 
	}
	
	public static void deleteSocialAccount(String apikey,String email){
		try{
			//check if the user has social review install and delete the account from kudobuzz;
			AddonSubscription addonSub = AddonSubscription.findAddonByTitle(apikey,DropifiTools.SOCIAL_REVIEW,AddonStatus.Installed);
			if(addonSub!=null){
				SocialReviewClient.deleteSocialAccount(addonSub.getUid(), email);
				addonSub.delete();
				//reset the widget to remove it from website
				Addons.resetWidget(addonSub.getApikey());
				//play.Logger.log4j.info("Deleted: "+client);
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		} 
	}
	
}
