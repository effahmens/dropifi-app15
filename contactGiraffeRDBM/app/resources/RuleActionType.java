package resources;

public enum RuleActionType {
	send_automatic_response_to_sender,
	forward_to_other_email_address,
	mark_as_customer,
	stop_processing
}
