/**
 * A serialize class for sending messages to sentimentActor (AKKA actor for handling concurrent processing) 
 */

package resources;

import java.io.Serializable; 

import resources.sentiment.SentimentBaseSerializer;
import resources.sentiment.lymbix.*;
public class SentimentSerializer implements Serializable{	
	 
	private Long msgId;
	private String email;
	private Long companyId;
	private  Long sourceId;
	private String sourceOfMsg;
	private String article;	
	private ArticleInfo articleInfo;
	private SentimentBaseSerializer sentiment;
	
	public SentimentSerializer(Long msgId,String email, Long companyId, Long sourceId,String sourceofMsg, String article){ 		 
		
		this.setMsgId(msgId);
		this.setEmail(email);
		this.setCompanyId(companyId); 
		this.setSourceId(sourceId);		
		this.setSourceOfMsg(sourceofMsg);
		this.setArticle(article); 
	}
	
	public SentimentSerializer(Long msgId,String email, Long companyId, Long sourceId,String sourceofMsg, String article,ArticleInfo articleInfo){ 		 
		
		this.setMsgId(msgId);
		this.setEmail(email);
		this.setCompanyId(companyId); 
		this.setSourceId(sourceId);		
		this.setSourceOfMsg(sourceofMsg);
		this.setArticle(article);
		this.setArticleInfo(articleInfo);
	}
	
public SentimentSerializer(Long msgId,String email, Long companyId, Long sourceId,String sourceofMsg, String article,SentimentBaseSerializer sentiment){ 		 
		
		this.setMsgId(msgId);
		this.setEmail(email);
		this.setCompanyId(companyId); 
		this.setSourceId(sourceId);		
		this.setSourceOfMsg(sourceofMsg);
		this.setArticle(article);
		this.setSentiment(sentiment); 
	}
	 
	@Override
	public String toString() {
		return "SentimentSerializer [msgId=" + msgId + ", email=" + email
				+ ", companyId=" + companyId + ", sourceId=" + sourceId
				+ ", sourceOfMsg=" + sourceOfMsg + ", article=" + article
				+ ", articleInfo=" + articleInfo + ", sentiment=" + sentiment
				+ "]";
	}

	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	
	public String getSourceOfMsg() {
		return sourceOfMsg;
	}
	

	public void setSourceOfMsg(String sourceOfMsg) {
		this.sourceOfMsg = sourceOfMsg;
	}

	
	public Long getSourceId() {
		return sourceId;
	}
	

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public ArticleInfo getArticleInfo() {
		return articleInfo;
	}

	public void setArticleInfo(ArticleInfo articleInfo) {
		this.articleInfo = articleInfo;
	}

	public SentimentBaseSerializer getSentiment() {
		return sentiment;
	}

	public void setSentiment(SentimentBaseSerializer sentiment) {
		this.sentiment = sentiment;
	}
	
}
