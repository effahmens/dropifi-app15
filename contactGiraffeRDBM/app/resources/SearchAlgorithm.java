package resources;

public class SearchAlgorithm {
	
	
	 public int ternarySearch(int [] items, int search, int s_index, int l_index)  {
		    //if the start index of the array is greater than the last index return -1
		    //-1 means record was not found
            if (s_index > l_index)  
                   return -1;         
            else{
            	   //devide the array into three parts by taking the two mid points
                   int mid_one = (s_index*2+l_index) / 3;
                   int mid_two = (s_index+l_index*2) / 3;
                   
                   //determine the location of the 'search item' by comparing the value at each
                   //index position with the search item
                   if (items[s_index] == search)                 
                          return s_index;                  
                   else if (items[l_index] == search)                   
                          return l_index;                   
                   else if (items[mid_one] == search)                   
                          return mid_one;                   
                   else if (items[mid_two] == search)                   
                          return mid_two;
                   //if any of the conditions above do not hold, search for the item within the
                   //three divided parts
                   else if (search < items[mid_one])                   
                          return ternarySearch(items, search, s_index, mid_one-1);                   
                   else if (search < items[mid_two])                   
                          return ternarySearch(items, search, mid_one+1, mid_two-1);                   
                   else                   
                          return ternarySearch(items, search, mid_two+1, l_index);                   
            }
     }
	 
	 public int modifiedBinarySearch(int [] items, int search, int s_index, int l_index){
		
		 if(s_index>l_index) 
			 return -1;
		 
		 System.out.println("out");
		  int mid = (s_index+l_index) / 2;
		  
		  //compute the nearest location of the search item
		 //  int bound1 = Math.abs(search-items[mid-1]);
		 // int bound2 = Math.abs(search-items[mid]);
		  
		  if (items[s_index] == search)                 
              return s_index;                  
		  else if (items[l_index] == search)                   
             return l_index;                   
		  else if (items[mid] == search)                   
             return mid;           	 
      	 else if (search< items[mid])
      		 return modifiedBinarySearch(items,search, s_index, mid-1);
    	 else
    		 return modifiedBinarySearch(items,search, mid+1, l_index);
		 
	 }
	 
}
