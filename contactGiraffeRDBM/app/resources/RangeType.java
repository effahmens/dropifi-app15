package resources;

public enum RangeType {	
	specific,
	range,
	thirtydays,
	nodate,
	all
}
