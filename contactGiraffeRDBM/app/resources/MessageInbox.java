
package resources;

import java.io.Serializable;
import java.util.*;

 
public class MessageInbox implements Serializable{
	
	 public String from;
	 public String to;
	 public String subject;
	 public String body;
	 public Date receivedDate;
	 
	public MessageInbox(String from, String to, String subject, String body,
			Date receivedDate) {
		super();
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.body = body;
		this.receivedDate = receivedDate;
	}

	@Override
	public String toString() {
		return "MessageInbox [from=" + from + ", to=" + to + ", subject="
				+ subject + ", body=" + body + ", receivedDate=" + receivedDate
				+ "]";
	}
 
	 
}