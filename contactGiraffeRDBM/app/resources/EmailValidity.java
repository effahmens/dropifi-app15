package resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Hashtable;

import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class EmailValidity { 
	
	private String senderEmail;
	
	private String emailToCheck;
	
	private String[] MXS;
	
	private int portNumber;
	
	private Attribute attribute;
	
	public EmailValidity(){}
	
	public EmailValidity(String emailToCheck){
		this.setEmailToCheck(emailToCheck);
		this.setSenderEmail(emailToCheck);
		this.setPortNumber(25);
	}
	 
	
	public boolean isEmailInCorrectFormat(){ 
		boolean flag = true;
		if(!this.getEmailToCheck().matches("[^@]+@([-\\p{Alnum}]+\\.)*\\p{Alnum}+"))	
			flag = false;
		return flag;
	}
	
	public int CheckDomain(){ 
		int status = 501;
		try
		{ 				
			String[] temp = this.getEmailToCheck().split("@"); 
			String user = temp[0], hostname = temp[1];
			
			Hashtable env = new Hashtable();
			env.put("java.naming.factory.initial", "com.sun.jndi.dns.DnsContextFactory");		
			
			DirContext ictx = new InitialDirContext(env);
			Attributes attrs = ictx.getAttributes(hostname, new String[] {"MX"});
			this.setAttribute(attrs.get("MX"));
			if(this.getAttribute().size() > 0){
				status = this.fillMailExchangeArray();				
			}
		}
		catch(NamingException ne){
			//System.out.println(ne.getMessage());
			play.Logger.log4j.info(ne.getMessage(),ne);
			
		}	
		return status;
	}
	
	public int fillMailExchangeArray() 
	{
		int status = 501;		
		try
		{
			MXS = new String[this.getAttribute().size()];
			NamingEnumeration neu = this.getAttribute().getAll();
			
			int n = 0;
			while(neu.hasMore()){
				MXS[n++] = ((neu.next().toString()).split(" "))[1];
			}	
			Arrays.sort(MXS); 
			status = 201;
		}
		catch(NamingException ne){
			//System.out.println(ne.getMessage());
			play.Logger.log4j.info(ne.getMessage(),ne);
		}			
		return status;
	}
	
	public int isEmailValid()
	{ 	
		int status = this.CheckDomain();
		if(status == 201)
		{ 
			Socket socket=null;;
			try
			{				    
			    if(getMXS().length > 0)
			    {			    	
			    	
					BufferedReader dataInput;
				    PrintStream dataOutput;
				    String mailServer;
				    String response = "";
				    status =501;
				    for(String MX : getMXS())
				    {
				    	mailServer = new String(MX);
				    	socket = new Socket(mailServer, getPortNumber()); 
				    	dataInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				    	dataOutput = new PrintStream(socket.getOutputStream());
				    	
				    	response = dataInput.readLine();				    	
					   // System.out.println("connected: " + response);
					    				    
					    if(response != null && response.split(" ")[0].trim().equals("220"))
					    {
					    	//dataOutput.println("HELO");
						    response = dataInput.readLine();//.split(" ")[0].trim();
						 //   System.out.println("response: " + response);
						    
						    if(response!=null && response.split(" ")[0].trim().equals("250"))
						    {					    
							    //dataOutput.println("mail from:<" + getEmailToCheck() + ">");
							    response = dataInput.readLine();
						//	    System.out.println(response);
							    
							    if(response.split(" ")[0].trim().equals("250"))
							    {
								    dataOutput.println("rcpt to:<" + getEmailToCheck() + ">");
								    response = dataInput.readLine();
							//	    System.out.println(response);
								    
								    if(response.split(" ")[0].trim().equals("250")) 
								    {
								 					    
									    dataOutput.println("Quit"); 
									    response = dataInput.readLine();
								//	    System.out.println(response);
									    
									    dataOutput.flush();							    
									    
									    status = 200;									    
									    break;
								    }else{
								    	 status =203;
								    }
							    }
							    socket.close();
						    }
					    }
					    socket.close();
				    }
			    }			   
			}catch(IOException ioe){
				status = 501;
				 try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.info(e.getMessage(), e);
				}
				//System.out.println(ioe.getMessage());
				return status;
			}
		}	     
	    return status;
	}
	
	
	/**
	 * @return the senderEmail
	 */
	public String getSenderEmail() {
		return senderEmail;
	}

	/**
	 * @param senderEmail the senderEmail to set
	 */
	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	/**
	 * @return the emailToCheck
	 */
	public String getEmailToCheck() {
		return emailToCheck;
	}

	/**
	 * @param emailToCheck the emailToCheck to set
	 */
	public void setEmailToCheck(String emailToCheck) {
		this.emailToCheck = emailToCheck;
	}

	/**
	 * @return the portNumber
	 */
	public int getPortNumber() {
		return portNumber;
	}

	/**
	 * @param portNumber the portNumber to set 
	 */
	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}

	/**
	 * @return the mXS
	 */
	public String[] getMXS() {
		return MXS;
	}

	/**
	 * @return the attribute
	 */
	public Attribute getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}
	
	

}
