package resources;

import java.sql.Connection;
import java.sql.SQLException;

import play.cache.Cache;

import adminModels.ApikeyType;
import adminModels.SystemApikey;

/**
 * constant fields for holding different API keys. e.g. fullContact, facebook, twitter, lymbix, stripe
 * @author phillips
 *
 */
public class APIKEY {
	
	public static  String fullContact ="43ad8a7026c810f6"; 	//"da99e637274020fa"; //"f82629cf99ca01ad"; //43ad8a7026c810f6
	public static  String lymbix = "0eb0f01a39f035419bbf319436138ed4d47e71db"; //"c89717db91145a608b8e833bb8b275a40f2e61e2"; //"91f9c689a9a4773ee0bb007395e947292287ecca";//
	public static  String stripe;
	
	public static int setApikeys(Connection conn) throws SQLException{
		int status = 404; 
		String fContact = getApikey(conn, CacheKey.getFullcontactKey(), ApikeyType.FullContact); 
		String lym = getApikey(conn, CacheKey.getLymbixKey(), ApikeyType.Lymbix); 
		String stri = getApikey(conn, CacheKey.getStripeKey(), ApikeyType.Stripe); 
	
		if(fContact !=null && !fContact.trim().isEmpty()){
			 fullContact = fContact;
			 status = 200;
		}else{
			status = 203;
		}
				
		if(lym !=null && !lym.trim().isEmpty()){
			lymbix = lym;
			 status = 200;
		}else{
			status +=1;
		}	
		
		if(stri !=null && !stri.trim().isEmpty()){
			stripe = stri;
			 status = 200;
		}else{
			status +=1;
		}	
		
		return status;
	}
	
	public static int setFullContactApikey(Connection conn) throws SQLException{
		String fContact = getApikey(conn, CacheKey.getFullcontactKey(), ApikeyType.FullContact);
		if(fContact !=null && !fContact.trim().isEmpty()){
			 fullContact = fContact;
			return 200;
		}else{
			return 501;
		}
	}
	
	public static int setLymbixApikey(Connection conn) throws SQLException{
		String lym = getApikey(conn, CacheKey.getLymbixKey(), ApikeyType.Lymbix);
		if(lym !=null && !lym.trim().isEmpty()){
			lymbix = lym; 
			return 200;
		}else{
			return 501;
		}	
	}
	
	public static int setStripeApikey(Connection conn) throws SQLException{
		String stri = getApikey(conn, CacheKey.getStripeKey(), ApikeyType.Stripe);
		if(stri !=null && !stri.trim().isEmpty()){
			stripe = stri; 
			return 200;
		}else{
			return 501;
		}	
	}	
	
	private static String getApikey(Connection conn,String cacheKey, ApikeyType apikeyType) throws SQLException{
		//save the apikey in the cache for future querying
		String apikey = Cache.get(cacheKey, String.class);
		if(apikey == null) {	
			apikey = SystemApikey.findApikey(conn, apikeyType.name()); 
			Cache.set(cacheKey, apikey);
		}else{
			play.Logger.log4j.info("Apikey from cache");
		}
		return apikey;
	}
	
	public static void setStripeApikey(String stripeApikey){
		 stripe = stripeApikey;
	}	
	
	public static void setLymbixApikey(String lymbixApiKey) throws SQLException{
		 lymbix = lymbixApiKey;
	}
	
	public static void setFullContactApikey(String lymbixApiKey) throws SQLException{
		 lymbix = lymbixApiKey;
	}
}
