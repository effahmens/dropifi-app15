package resources.Cache;

import java.io.Serializable;
import java.sql.Timestamp;

import models.Company;
import query.analytics.CustSegSelector;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import query.analytics.MainAnalyticsSelector;
import query.inbox.Contact_Serializer;
import query.inbox.InboxSelector;
import resources.CacheKey;
import resources.RangeType;
import resources.UserType;

import play.cache.Cache;
import play.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import controllers.Security;


public class Cache_Analytics {
	public static String AnalyticDate(RangeType rtype, String startDate, String endDate)
	{
		if(rtype == RangeType.thirtydays)
		{
			DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/yyyy");
			DateTime fdate = DateTime.now().minusDays(30);
			DateTime today = DateTime.now();
			
			String fd = format.print(fdate);
			String td = format.print(today);		
			
			return "_" + fd +"_"+ td;			 
		}
		else if(rtype == RangeType.specific)
		{
			return (startDate != "") ? "_" + startDate : (endDate != "" ? "_" + endDate : "");
		}
		else if(rtype == RangeType.range)
		{			
			if(startDate != "" && endDate != "")
				return "_" + startDate + "_" + endDate;
		}		
		return ""; 
	}
	
	public static boolean isToday(String startDate, String endDate)
	{
		boolean flag = false;
		DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/yyyy");
		String nw = format.print(DateTime.now());
		
		String sd = "";
		String ed = "";
		
		if(startDate != "" && endDate != "")
		{
			sd = format.print(DateTime.parse(startDate));
			ed = format.print(DateTime.parse(endDate));				
			flag = (nw == sd) ? true : ((nw == ed) ? true : false);			 
		}
		else if(startDate != "" && endDate == "")
		{
			sd = format.print(DateTime.parse(startDate));
			flag = (nw == ed) ? true : false;
		}
		else if(startDate == "" && endDate != "")
		{
			ed = format.print(DateTime.parse(endDate));
			flag = (nw == ed) ? true : false;
		}
		
		return flag;
	}
	
	//Dashboard analytic data
	public static String DashboardAnalyticsData(String apikey, RangeType rtype) 
	{
		String key = CacheKey.genCacheKey(apikey, CacheKey.Dashboard_Analytics, AnalyticDate(rtype, "", ""));
		String value = (String)Cache.get(key);
		
		if(value == null)
		{			
			JsonObject json = new JsonObject();
			try{			
				Long companyId = Company.findCompanyId(apikey);
				int status = 405;
				
				if(companyId != null)
				{
					int no = 30;
					json.add("location", CustSegSelector.custLocationToJsonArray(companyId, no, 3));
					json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId, no,5));
					json.add("sentiment", CustSegSelector.custMsgSentiments(companyId,no));
					json.add("emotion", CustSegSelector.custMsgEmotions(companyId,no));
					json.add("tIssue", CustSegSelector.compTroublingIssues(companyId, no, 3));
					json.addProperty("nmsgs", CustSegSelector.numberOfNewMessages(companyId, no));
					json.addProperty("rmsgs", CustSegSelector.UnResolvedMessages(companyId, no));
					json.addProperty("ncust", CustSegSelector.numberOfCustomers(companyId, no));
					json.addProperty("mailbox", CustSegSelector.NumberOfMailBoxes(companyId));
					json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, no));
					status = 200;
				}			
				json.addProperty("status", status);
				value = json.toString();
				Cache.set(key, value, "1mn");
			}
			catch(Exception e){
				play.Logger.log4j.error(e.getMessage(), e);			
			}			
		}
		return value.toString();
	}

	// Messages analytic data
	public static String MessageAnalyticsData(String apikey, RangeType rtype, int days, String startDate, String endDate)
	{
		String key;
		String value;
		if(rtype == RangeType.thirtydays)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Messsage_Analytics, AnalyticDate(rtype, "", ""));
			value = (String)Cache.get(key);
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try{
					
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					if(companyId != null)
					{
						json.addProperty("newmsgs", CustSegSelector.numberOfNewMessages(companyId, days));
						json.addProperty("nonresolvedmsgs", CustSegSelector.UnResolvedMessages(companyId, days));
						json.addProperty("resolvedmsgs", CustSegSelector.ResolvedMessages(companyId, days));
						json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, days));
						json.add("trnMbox", CustSegSelector.trendingMailBoxMessage(companyId, days));			
						
						status = 200;
					}	
					json.addProperty("status", status);
					value = json.toString();
					
					if(value.toString().length() > 0)
						Cache.set(key, value, "1mn");
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);					
				}		
			}
			return value.toString();
		}
		else if(rtype == RangeType.specific)
		{
			String specDate = (startDate != "") ? startDate : (endDate != "" ? endDate : "");
			key = CacheKey.genCacheKey(apikey, CacheKey.Messsage_Analytics, AnalyticDate(rtype, specDate, ""));
			value = (String)Cache.get(key); 		
			
			if(value == null)
			{
				JsonObject json = new JsonObject();			
				
				try{					
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					Timestamp fts = null;		
					if(!specDate.equals("")){			
						fts = new Timestamp((DateTime.parse(specDate)).getMillis());
					}		
					
					if(companyId != null){
						json.addProperty("newmsgs", CustSegSelector.numberOfNewMessages(companyId, fts));
						json.addProperty("nonresolvedmsgs", CustSegSelector.UnResolvedMessages(companyId, fts));
						json.addProperty("resolvedmsgs", CustSegSelector.ResolvedMessages(companyId, fts));
						json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, fts));
						json.add("trnMbox", CustSegSelector.trendingMailBoxMessage(companyId, fts));
						
						status = 200;
					}
					
					json.addProperty("status", status);					
					value = json.toString();
					
					if(isToday(specDate, "") && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
					
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}					
			}
			return value.toString();
		}
		else if(rtype == RangeType.range)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Messsage_Analytics, AnalyticDate(rtype, startDate, endDate));
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try
				{			
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					Timestamp fts = null;
					Timestamp tts = null;
					
					if(!startDate.equals("")  &&  endDate.equals("")){			
						fts = new Timestamp((DateTime.parse(startDate)).getMillis());
						tts = null;
					}
					else if(!startDate.equals("") && !endDate.equals("")){
						fts = new Timestamp((DateTime.parse(startDate)).getMillis());
						tts = new Timestamp((DateTime.parse(endDate)).getMillis());
					}		
					
					if(companyId != null){
						json.addProperty("newmsgs", CustSegSelector.numberOfNewMessages(companyId, fts, tts));
						json.addProperty("nonresolvedmsgs", CustSegSelector.UnResolvedMessages(companyId, fts, tts));
						json.addProperty("resolvedmsgs", CustSegSelector.ResolvedMessages(companyId, fts, tts));
						json.addProperty("resolutionrate", MainAnalyticsSelector.ResolutionRate(companyId, fts, tts));
						json.add("trnMbox", CustSegSelector.trendingMailBoxMessage(companyId, fts, tts));
						
						status = 200;
					}
					
					json.addProperty("status", status);	
					value = json.toString();
					
					if(isToday(startDate, endDate) && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}				
			}
			return value.toString();
		}
		return "";
	}
	
	public static String MessageAnalyticsData_Extra(String apikey, RangeType rtype, int days, String startDate, String endDate)
	{
		String key;
		String value;
		if(rtype == RangeType.thirtydays)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Messsage_Analytics, "_extra", AnalyticDate(rtype, "", ""));
			value = (String)Cache.get(key);
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try
				{					
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					if(companyId != null)
					{
						json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, days));
						json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, days));
						json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, days));
						
						json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, days));
						json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, days));		
						
						status = 200;
					}	
					json.addProperty("status", status);
					value = json.toString();
					
					if(value.toString().length() > 0)
						Cache.set(key, value, "1mn");
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);					
				}		
			}
			return value.toString();
		}
		else if(rtype == RangeType.specific)
		{
			String specDate = (startDate != "") ? startDate : (endDate != "" ? endDate : "");
			key = CacheKey.genCacheKey(apikey, CacheKey.Messsage_Analytics, "_extra", AnalyticDate(rtype, specDate, ""));
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();					
				try
				{					
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;	
					
					if(companyId != null)
					{
						Timestamp fts = new Timestamp((DateTime.parse(specDate)).getMillis());
						
						json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, fts));
						json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, fts));
						json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, fts));
						
						json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, fts));
						json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, fts));
						
						status = 200;
					}
					
					json.addProperty("status", status);					
					value = json.toString();
					
					if(isToday(specDate, "") && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
					
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}					
			}
			return value.toString();
		}
		else if(rtype == RangeType.range)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Messsage_Analytics, "_extra", AnalyticDate(rtype, startDate, endDate));
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try
				{			
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;		
					
					if(companyId != null)
					{			
						Timestamp fts = new Timestamp((DateTime.parse(startDate)).getMillis());
						Timestamp tts = new Timestamp((DateTime.parse(endDate)).getMillis());
						
						json.add("trdkwdmsg", MainAnalyticsSelector.TrendingKeywordAndMessages(companyId, fts, tts));
						json.add("trdsubmsg", MainAnalyticsSelector.TrendingWidgetSubjectsAndMessages(companyId, fts, tts));
						json.add("trdagemsg", MainAnalyticsSelector.TrendingAgeAndMessages(companyId, fts, tts));
						
						json.add("trdlocmsg", MainAnalyticsSelector.TrendingTopLocationAndMessages(companyId, fts, tts));
						json.add("trdsocmsg", MainAnalyticsSelector.TrendingSocialsAndMessages(companyId, fts, tts));
						
						status = 200;
					}
					
					json.addProperty("status", status);	
					value = json.toString();
					
					if(isToday(startDate, endDate) && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);
				}				
			}
			return value.toString();
		}		
		return "";
	}
	
	// Contacts analytic data
	public static String ContactAnalyticsData_Pie(String apikey, RangeType rtype, int days, int top, String startDate, String endDate)
	{
		String key;
		String value;
		if(rtype == RangeType.thirtydays)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Contact_Analytics, AnalyticDate(rtype, "", ""));
			value = (String)Cache.get(key);
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try{					
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					if(companyId != null)
					{			
						json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId, days,10));
						json.add("location", CustSegSelector.custLocationToJsonArray(companyId, days, 10));
						json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId,days));
						json.add("gender", CustSegSelector.custGenderToJsonArray(companyId,days));
						status = 200;
					}	
					json.addProperty("status", status);
					value = json.toString();
					
					if(value.toString().length() > 0)
						Cache.set(key, value, "1mn");
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);					
				}		
			}
			return value.toString();
		}
		else if(rtype == RangeType.specific)
		{
			String specDate = (startDate != "") ? startDate : (endDate != "" ? endDate : "");
			key = CacheKey.genCacheKey(apikey, CacheKey.Contact_Analytics, AnalyticDate(rtype, specDate, ""));
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();					
				try
				{	
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					Timestamp fts = null;		
					if(!specDate.equals("")){			
						fts = new Timestamp((DateTime.parse(specDate)).getMillis());
					}
							
					if(companyId != null)
					{			
						json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId, fts ,top));
						json.add("location", CustSegSelector.custLocationToJsonArray(companyId, fts, top));
						json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId,fts));
						json.add("gender", CustSegSelector.custGenderToJsonArray(companyId,fts));
						
						status = 200;
					}	
					json.addProperty("status", status);					
					value = json.toString();
					
					if(isToday(specDate, "") && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
					
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}					
			}
			return value.toString();
		}
		else if(rtype == RangeType.range)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Contact_Analytics, AnalyticDate(rtype, startDate, endDate));
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try
				{			
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					Timestamp fts = null;
					Timestamp tts = null;
					
					if(startDate != null && !startDate.equals("") && endDate != null && !endDate.equals("")){
						fts = new Timestamp((DateTime.parse(startDate)).getMillis());
						tts = new Timestamp((DateTime.parse(endDate)).getMillis());
					}
							
					if(companyId != null)
					{			
						json.add("socialnetwork", CustSegSelector.custSocialNetworksToJsonArray(companyId,fts, tts,top));
						json.add("location", CustSegSelector.custLocationToJsonArray(companyId,fts,tts,top));
						json.add("ageRange", CustSegSelector.cusAgeRangeNo(companyId,fts,tts));
						json.add("gender", CustSegSelector.custGenderToJsonArray(companyId,fts,tts));			
						status = 200;
					}	
					json.addProperty("status", status);			
					value = json.toString();
					
					if(isToday(startDate, endDate) && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}				
			}
			return value.toString();
		}
		return "";
	}

	public static String CustomerAnalyticsData(String apikey, RangeType rtype, String startDate, String endDate, String par1, String par2)
	{
		String key = null;
		String value = null;
		
		if(rtype == RangeType.thirtydays)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, "", ""));
			value = (String)Cache.get(key);
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try{					
					Long companyId = Company.findCompanyId(apikey);
					int status = 405;
					
					if(companyId != null){					
						json.add("socialsentiment", MainAnalyticsSelector.DefaultSocialAndMessage(companyId, 30));
						status = 200;
					}	
					json.addProperty("status", status);
					value = json.toString();
					
					if(value.toString().length() > 0)
						Cache.set(key, value, "1mn");
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);					
				}		
			}
			return value.toString();
		}
		else if(rtype == RangeType.specific)
		{
			String specDate = (startDate != "") ? startDate : (endDate != "" ? endDate : "");
			
			if(par1 != "0" && par2 == "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, specDate, ""), par1);
			}
			else if(par1 == "0" && par2 != "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, specDate, ""), par2);
			}
			else if(par1 != "0" && par2 != "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, specDate, ""), par1, par2);
			}						
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();					
				try
				{	
					Long companyId = Company.findCompanyId(apikey);			
					int status = 405;
																	
					if(companyId != null){
						Timestamp fts = new Timestamp((DateTime.parse(specDate)).getMillis());
						json.add("mcustanalytic", MainAnalyticsSelector.custProfileAnalytics(companyId, fts, null, par1, par2));			
						status = 200;			
					}
					
					json.addProperty("status", status);				
					value = json.toString();
					
					if(isToday(specDate, "") && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
					
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}					
			}
			return value.toString();
		}
		else if(rtype == RangeType.range)
		{			
			if(par1 != "0" && par2 == "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, startDate, endDate), par1);
			}
			else if(par1 == "0" && par2 != "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, startDate, endDate), par2);
			}
			else if(par1 != "0" && par2 != "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, startDate, endDate), par1, par2);
			}						
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();					
				try
				{	
					Long companyId = Company.findCompanyId(apikey);			
					int status = 405;
												
					if(companyId != null){	
						Timestamp fts = new Timestamp((DateTime.parse(startDate)).getMillis());
						Timestamp tts = new Timestamp((DateTime.parse(endDate)).getMillis());
						json.add("mcustanalytic", MainAnalyticsSelector.custProfileAnalytics(companyId, fts, tts, par1, par2));			
						status = 200;			
					}
					
					json.addProperty("status", status);				
					value = json.toString();
					
					if(isToday(startDate, endDate) && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
					
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}					
			}
			return value.toString();
		}
		else if(rtype == RangeType.nodate)
		{			
			if(par1 != "0" && par2 == "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, "", ""), par1);
			}
			else if(par1 == "0" && par2 != "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, "", ""), par2);
			}
			else if(par1 != "0" && par2 != "0")
			{
				key = CacheKey.genCacheKey(apikey, CacheKey.Customer_Analytics, AnalyticDate(rtype, "", ""), par1, par2);
			}						
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();					
				try
				{	
					Long companyId = Company.findCompanyId(apikey);			
					int status = 405;
												
					if(companyId != null){
						json.add("mcustanalytic", MainAnalyticsSelector.custProfileAnalytics(companyId, null, null, par1, par2));			
						status = 200;			
					}
					
					json.addProperty("status", status);				
					value = json.toString();
					
					if(isToday(startDate, endDate) && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
					
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}					
			}
			return value.toString();
		}
		return "";
	}

	// Sentiment analytics data
	public static String SentimentAnalyticsData(String apikey, RangeType rtype, int days, int top, String startDate, String endDate)
	{
		String key;
		String value;
		if(rtype == RangeType.thirtydays)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Sentiment_Analytics, AnalyticDate(rtype, "", ""));
			value = (String)Cache.get(key);
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try
				{					
					Long companyId = Company.findCompanyId(apikey);					
					int status = 405;	
					
					if(companyId != null)
					{
						json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessages(companyId, days));
						json.add("trdsenmsg", MainAnalyticsSelector.TrendingSentimentsAndMessages(companyId, days));			
						
						status = 200;
					}
					json.addProperty("status", status);		
					value = json.toString();
					
					if(value.toString().length() > 0)
						Cache.set(key, value, "1mn");
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);					
				}		
			}
			return value.toString();
		}
		else if(rtype == RangeType.specific)
		{
			String specDate = (startDate != "") ? startDate : (endDate != "" ? endDate : "");
			key = CacheKey.genCacheKey(apikey, CacheKey.Sentiment_Analytics, AnalyticDate(rtype, specDate, ""));
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();					
				try
				{					
					Long companyId = Company.findCompanyId(apikey);					
					int status = 405;			
					
					if(companyId != null)
					{
						Timestamp fts = new Timestamp((DateTime.parse(specDate)).getMillis());
						json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessages(companyId, fts));
						json.add("trdsenmsg", MainAnalyticsSelector.TrendingSentimentsAndMessages(companyId, fts));
						
						status = 200;
					}
					json.addProperty("status", status);					
					value = json.toString();
					
					if(isToday(specDate, "") && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}					
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);	
				}					
			}
			return value.toString();
		}
		else if(rtype == RangeType.range)
		{
			key = CacheKey.genCacheKey(apikey, CacheKey.Sentiment_Analytics, AnalyticDate(rtype, startDate, endDate));
			value = (String)Cache.get(key);
			
			if(value == null)
			{
				JsonObject json = new JsonObject();
				try
				{			
					Long companyId = Company.findCompanyId(apikey);
					
					int status = 405;
					
					if(companyId != null)
					{
						Timestamp fts = new Timestamp((DateTime.parse(startDate)).getMillis());
						Timestamp tts = new Timestamp((DateTime.parse(endDate)).getMillis());
						
						json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessages(companyId, fts, tts));
						json.add("trdsenmsg", MainAnalyticsSelector.TrendingSentimentsAndMessages(companyId, fts, tts));
						
						status = 200;
					}
					json.addProperty("status", status);	
					value = json.toString();
					
					if(isToday(startDate, endDate) && value.toString().length() > 0)
					{
						Cache.set(key, value, "1mn");
					}
					else if(value.toString().length() > 0)
					{
						Cache.set(key, value);
					}
				}
				catch(Exception e){ 
					play.Logger.log4j.error(e.getMessage(), e);
				}				
			}
			return value.toString();
		}		
		return "";
	}
	
	// Inbox analytic data
	public static String InboxAnalyticsData(String apikey, RangeType rtype, String email, String userEmail, UserType userType, boolean hasErrors)
	{
		String key = CacheKey.genCacheKey(apikey, CacheKey.Inbox_Analytics, email, AnalyticDate(rtype, "", ""));
		String value = (String)Cache.get(key);	
		
		if(value == null)
		{			
			JsonObject json = new JsonObject();
			Gson gson = new Gson(); 
			try
			{
				int status = 405;
				if(!hasErrors)
				{
					Long companyId = Company.findCompanyId(apikey); 
					
					if(companyId != null && userEmail!=null && apikey!=null && userType!=null)
					{
						Contact_Serializer contact = InboxSelector.findCustomerProfile(companyId, email);
						if(contact!=null)
						{
							json.add("contact", gson.toJsonTree(contact));	
							
							json.add("trdemomsg", MainAnalyticsSelector.TrendingEmotionsAndMessagesFromContact(companyId, email, null, null));
							json.add("sentiment", CustSegSelector.custMsgSentimentsFromContact(companyId, email, null, null));				
							json.addProperty("nmsgs", CustSegSelector.numberOfMessagesFromContact(companyId, email, null, null));
							json.addProperty("rmsgs", CustSegSelector.UnResolvedMessagesFromContact(companyId, email, null, null));
							json.addProperty("avgSentScore", CustSegSelector.AverageSentimentScoreFromContact(companyId, email, null, null)); 
							
							status=200;
						} 
					}
					else
					{
						//Redirect to login page
						json.addProperty("error", "invalid account, login with the correct credentials");
					}
				}
				else
				{
				  json.addProperty("error", "required fields are not specified");
				}
			  
				json.addProperty("status", status);
				value = json.toString();
				if(value.length() > 0)
					Cache.set(key, value, "1mn");
			}
			catch(Exception e){
				play.Logger.log4j.error(e.getMessage(), e);			
			}			
		}
		return value.toString();
	}

	
}
