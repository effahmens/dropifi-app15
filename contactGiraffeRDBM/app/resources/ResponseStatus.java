package resources;

public enum ResponseStatus {
	Draft,
	Reply,
	Forward,
	Normal;
	
	public static ResponseStatus typeOfStatus(String status){
		if(ResponseStatus.Draft.name().equalsIgnoreCase(status))
			return ResponseStatus.Draft; 
		else if(ResponseStatus.Reply.name().equalsIgnoreCase(status))
			return ResponseStatus.Reply;
		if(ResponseStatus.Forward.name().equalsIgnoreCase(status))
			return ResponseStatus.Forward;
		else if(ResponseStatus.Normal.name().equalsIgnoreCase(status))
			return ResponseStatus.Normal;		 
		
		return null;
	}
}
