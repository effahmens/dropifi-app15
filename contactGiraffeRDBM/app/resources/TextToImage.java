package resources;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException; 
import java.util.Date;

import javax.imageio.ImageIO;

import api.rackspace.RackspaceClient;

/**
 * Convert a text to an image and upload the image to AWS S3
 * @author Philips
 *
 */
public class TextToImage {
	
	private String BUCKETNAME;	
	private String fileName;
	private String outFileName;
	private String text;
	private String foldName; 
	private Color  textColor;
	private String textFont;
	private String buttonPosition;
	
	public TextToImage(String fileName,String text,String folderName, String color){
		this(fileName,text,folderName,color,null);
	}
	
	public TextToImage(String fileName,String text,String folderName, String color,String buttonPosition){
		this.BUCKETNAME = play.Play.mode.isProd()?DropifiTools.AWSWidgetTabBucketProd :DropifiTools.AWSWidgetTabBucketDev;
		this.setFileName(fileName);		 
		this.setText(text);
		this.setFoldName(folderName); 
		this.setTextColor(color);
		this.setButtonPosition(buttonPosition);
	}
	
	private void textToTransparentImage(){
		try{
		  final File in = new File(this.getFileName());
	      final BufferedImage source = ImageIO.read(in);  

	      final int color = source.getRGB(0, 0);

	      final Image imageWithTransparency = makeColorTransparent(source, new Color(color));

	      final BufferedImage transparentImage = imageToBufferedImage(imageWithTransparency);

	      final File out = new File(this.getOutFileName());
	      ImageIO.write(transparentImage, "PNG", out);	       
	      
		}catch(IOException e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public String transform(){
		
        String sample = play.Play.mode.isProd()?"prodsample":"sample";
        
        //create the main folder to keep all the tabText created;
        this.makeFolder(play.Play.applicationPath.getAbsolutePath()+"\\awss3\\"+sample);
        
        //create a folder to keep the temporarily file Object
		String tempFolderName = play.Play.applicationPath.getAbsolutePath()+"\\awss3\\"+sample+"\\"+this.getFoldName();
		this.makeFolder(tempFolderName);
		
		//String tempFileName = tempFolderName+"\\"+this.getFileName();
        File newFile = new File(tempFolderName+"\\"+this.getFileName());
        
         
        //create the font you wish to use. Default font family is Trebuchet MS
        if(this.getTextFont()==null || this.getTextFont().trim().isEmpty()){
        	this.setTextFont(DropifiTools.defaultFont);
        } 
        
        Font font = new Font(this.getTextFont(), Font.BOLD,14); 
        
        //create the FontRenderContext object which helps us to measure the text
        //AffineTransform atf = new AffineTransform();
        FontRenderContext frc = new FontRenderContext(null, true, true);
         
        //get the height and width of the text
        Rectangle2D bounds = font.getStringBounds(this.getText(), frc);
        //int w = (int)bounds.getWidth()+5;
        //int h = (int) bounds.getHeight()+5 ;
        
        //create a BufferedImage object 
        BufferedImage bi = new BufferedImage(((int)bounds.getWidth()+5),((int) bounds.getHeight()+5),BufferedImage.TYPE_INT_ARGB); 
 
        Graphics2D g = bi.createGraphics();
        g.clearRect(0, 0, 0, 0);
        
        //set color and other parameters
       g.setBackground(new Color(0,0,0,0));
       //g.fillRect(0, 0, w, h);
        
       g.setColor(this.getTextColor()); 
       g.setFont(font);      
      
       //smoothen the edges of the text
       g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
       g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
       
       g.drawString(this.getText(), (int)bounds.getX(), (int) -bounds.getY());
       g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OUT, 0.0f)); 
             
       //releasing resources
       g.dispose();  
      
        //creating the file 
       try {
			ImageIO.write(bi, "PNG", newFile);  
			
			//rotate the image to display the text vertical only if the button position is right or left
			if(this.getButtonPosition()==null || this.getButtonPosition().equalsIgnoreCase("right") || this.getButtonPosition().equalsIgnoreCase("left")){
				bi = rotate90ToLeft(bi);		
				ImageIO.write(bi, "PNG", newFile);
			} 
			
			AWSS3.putPublicFile(BUCKETNAME, this.getFoldName(), newFile,this.getFileName());	
			
			//RackspaceClient client = new RackspaceClient(DropifiTools.RackspaceUsername, DropifiTools.RackspaceApikey);
			//client.uploadObject(BUCKETNAME+File.separator+ this.getFoldName(), newFile, this.getFileName());
			
			//RackspaceClient.putPublicFile(BUCKETNAME+File.separator+ this.getFoldName(), newFile, this.getFileName()); 
			//play.Logger.log4j.info("successfully created image and uploaded to s3"+this.getFileName());
			try{
				newFile.delete();
				File fo = new File(tempFolderName);
				if(fo.exists()){
					fo.delete();
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e); 
			}
			
			return this.generateImageUrl(); //this.generateRackspaceImageUrl(client.getSSLCDN(BUCKETNAME));
       } catch (IOException e) {
		// TODO Auto-generated catch block
		play.Logger.log4j.info(e.getMessage(),e); 
       }   
       return null;
 }
	
	public BufferedImage rotate90ToLeft( BufferedImage inputImage ){
		//The most of code is same as before
        int width = inputImage.getWidth();
        int height = inputImage.getHeight();
        BufferedImage returnImage = new BufferedImage( height, width , inputImage.getType()  );
		//We have to change the width and height because when you rotate the image by 90 degree, the
		//width is height and height is width :)

        for( int x = 0; x < width; x++ ) {
            for( int y = 0; y < height; y++ ) {
	            returnImage.setRGB(y, width - x - 1, inputImage.getRGB( x, y  )  ); 
	            //Again check the Picture for better understanding
            }
        }
        return returnImage;

	}
	
	public BufferedImage rotate180( BufferedImage inputImage ) {
		//We use BufferedImage because it’s provide methods for pixel manipulation
        int width = inputImage.getWidth(); //the Width of the original image
        int height = inputImage.getHeight();//the Height of the original image

        BufferedImage returnImage = new BufferedImage( width, height, inputImage.getType()  );
		//we created new BufferedImage, which we will return in the end of the program
		//it set up it to the same width and height as in original image
		// inputImage.getType() return the type of image ( if it is in RBG, ARGB, etc. )

        for( int x = 0; x < width; x++ ) {
            for( int y = 0; y < height; y++ ) {
                    returnImage.setRGB( width - x - 1, height - y - 1, inputImage.getRGB( x, y  )  );
            }
        }
		//so we used two loops for getting information from the whole inputImage
		//then we use method setRGB by whitch we sort the pixel of the return image
		//the first two parametres is the X and Y location of the pixel in returnImage and the last one is the //source pixel on the inputImage
		//why we put width – x – 1 and height –y – 1 is hard to explain for me, but when you rotate image by //180degree the pixel with location [0, 0] will be in [ width, height ]. The -1 is for not to go out of
		//Array size ( remember you always start from 0 so the last index is lower by 1 in the width or height
		//I enclose Picture for better imagination  ... hope it help you
		        return returnImage;
		//and the last return the rotated image

	}
	
	public BufferedImage rotate90ToRight( BufferedImage inputImage ){
        int width = inputImage.getWidth();
        int height = inputImage.getHeight();
        BufferedImage returnImage = new BufferedImage( height, width , inputImage.getType()  );

        for( int x = 0; x < width; x++ ) {
            for( int y = 0; y < height; y++ ) {
                returnImage.setRGB( height - y - 1, x, inputImage.getRGB( x, y  )  );
                //Again check the Picture for better understanding
            }
        }
        return returnImage;
	}
	
	 public BufferedImage flipImage(BufferedImage myImage, Graphics2D g) {
		   // Image myImage = new ImageIcon("yourImage.jpg").getImage();
		    BufferedImage bufferedImage = new BufferedImage(myImage.getWidth(null), myImage.getHeight(null), BufferedImage.TYPE_INT_RGB);
		    Graphics2D g2d = g;

		    Graphics gb = bufferedImage.getGraphics();
		    gb.drawImage(myImage, 0, 0, null);
		    gb.dispose();

		    AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		    tx.translate(-myImage.getWidth(null), 0);
		    AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		    bufferedImage = op.filter(bufferedImage, null);
		    g2d.clearRect(0, 0, 0, 0); 
		    g2d.setBackground(new Color(0,0,0,0));
		    g2d.drawImage(myImage, 10, 10, null);
		    g2d.drawImage(bufferedImage, null, 300, 10); 
		    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OUT, 0.0f)); 
		    g2d.dispose();
		    return bufferedImage;
		  }
 
	/**
	    * Make provided image transparent wherever color matches the provided color.
	    *
	    * @param im BufferedImage whose color will be made transparent.
	    * @param color Color in provided image which will be made transparent.
	    * @return Image with transparency applied.
	    */
	   public  Image makeColorTransparent(final BufferedImage im, final Color color)
	   {
	      final ImageFilter filter = new RGBImageFilter()
	      {
	         // the color we are looking for (white)... Alpha bits are set to opaque
	         public int markerRGB = color.getRGB() | 0xFFFFFFFF;

	         public final int filterRGB(final int x, final int y, final int rgb)
	         {
	            if ((rgb | 0xFF000000) == markerRGB)
	            {
	               // Mark the alpha bits as zero - transparent
	               return 0x00FFFFFF & rgb;
	            }
	            else
	            {
	               // nothing to do
	               return rgb; 
	            }
	         }
	      };

	      final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
	      return Toolkit.getDefaultToolkit().createImage(ip);
	   }
	
	   /**
	    * Convert Image to BufferedImage.
	    *
	    * @param image Image to be converted to BufferedImage.
	    * @return BufferedImage corresponding to provided Image.
	    */
	   private  BufferedImage imageToBufferedImage(final Image image)
	   {
	      final BufferedImage bufferedImage =
	         new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
	      final Graphics2D g2 = bufferedImage.createGraphics();
	      g2.drawImage(image, 0, 0, null);
	      g2.dispose();
	      return bufferedImage;
	    } 

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOutFileName() {
		return outFileName;
	}

	public void setOutFileName(String outFileName) {
		this.outFileName = outFileName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = " "+text.trim()+" ";
	}

	public String getFoldName() {
		return foldName;
	}

	public void setFoldName(String foldName) {
		this.foldName = foldName;
	}

	public Color getTextColor() {
		return textColor;
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}
	
	public void setTextColor(String backgroundColor){		
		this.textColor = DropifiTools.isBrighten(backgroundColor)?Color.BLACK:Color.WHITE;
	}
	
	private String generateImageUrl(){
		//Date date = new Date(); 
		//return "https://s3.amazonaws.com/"+BUCKETNAME+"/"+this.getFoldName()+"/tab_text.png?v="+date.getTime();
		if(this.getFileName()==null)
			this.setFileName("tab_text");
		
		return "https://s3-us-west-2.amazonaws.com/"+BUCKETNAME+"/"+this.getFoldName()+"/"+this.getFileName()+"?v="+new Date().getTime();
	}
	
	private String generateRackspaceImageUrl(String cdnContainer){ 
		if(this.getFileName()==null)
			this.setFileName("tab_text");
		
		return cdnContainer+"/"+this.getFoldName()+"/"+this.getFileName()+"?v="+new Date().getTime();
	}
	
	private void makeFolder(String folderName){
		File theDir = new File(folderName);

		  // if the directory does not exist, create it
		  if (!theDir.exists()){
		    boolean result = theDir.mkdir();  
		    if(result){    
		       
		     }

		  }
	}

	public String getTextFont() {
		return textFont;
	}

	public void setTextFont(String font) {
		this.textFont = font;
	}

	public String getButtonPosition() {
		return buttonPosition;
	}

	public void setButtonPosition(String buttonPosition) {
		this.buttonPosition = buttonPosition;
	}
	
 
}
