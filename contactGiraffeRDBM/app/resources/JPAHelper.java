package resources;

import javax.persistence.EntityManager;
import play.db.jpa.JPA;

public class JPAHelper {

	/**
	 * Set the the value for JPA.local if it is null;
	 */
	public static void reset(){
		try{
			 if (JPA.local.get() == null) {
	             EntityManager em = JPA.newEntityManager();
	             final JPA jpa = new JPA();
	             jpa.entityManager = em;
	             JPA.local.set(jpa);
	         }

	        //JPA.em().getTransaction().begin();
	        //... DO YOUR STUFF HERE ...
			//JPA.em().getTransaction().commit();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static void begin(){
		try{JPA.em().getTransaction().begin();}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static void commit(){
		try{JPA.em().getTransaction().commit();}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
}
