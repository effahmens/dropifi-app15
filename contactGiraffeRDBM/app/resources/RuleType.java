package resources;

public enum RuleType {	
	Filter,
	Inbound,	
	Message,
	Notification,	
	Widget;
	
	public static RuleType valueOf(int value){
		
		switch(value){
			case 0:
				return RuleType.Filter;
			case 1:
				return RuleType.Inbound;
			case 2:
				return RuleType.Message;
			case 3:
				return RuleType.Notification;
			case 4:
				return RuleType.Widget;	
			default:
				return null;
		}
	}
}
