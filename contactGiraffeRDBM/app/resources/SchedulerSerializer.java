package resources;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class SchedulerSerializer  implements Serializable{
	
	@SerializedName("id")
	public long id;
	@SerializedName("email")
	public String email;
	
	public SchedulerSerializer(long id, String email) { 
		this.id = id;
		this.email = email;
	}

	public SchedulerSerializer() { 
	}
	
	
}
