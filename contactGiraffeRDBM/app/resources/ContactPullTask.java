package resources;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.RecursiveAction;

import job.process.ContactActor;
import static akka.actor.Actors.actorOf;
import akka.actor.ActorRef;
import akka.actor.Actors;
import api.fullcontact.FullContactException;

import models.Customer;
import models.CustomerProfile;
import models.InboundMailbox;
import models.failed.FullContactFailed;

/**
 * A concurrent process for pulling demographic,geographic, social profile and photos of customers
 * @author philips
 *
 */
public class ContactPullTask  extends RecursiveAction{
	
	private  int low;
	private  int high;
	private static final int THRESHOLD = 1;	//for dynamic value, the value should always be read from a database  	
	private List<CustomerProfile> customers;
	
	public ContactPullTask(List<CustomerProfile> customers, int low, int high){
		this.low= low;
		this.high = high;
		this.customers = customers;
	}
	
	@Override
	protected void compute(){
		//TODO Auto-generated method stub
		
		if((high-low)<=THRESHOLD){
			if(low>high) return ;
			
			for(int i=low;i<high;i++){				
				//search for the full contact profile of customer using fullContact API	
								
				try {
					CustomerProfile customer = customers.get(i);
					if(customer.getEmail().equalsIgnoreCase(DropifiTools.introEmail)){
						//default email of dropifi. do not automatically update
					}else{
						CustomerProfile.addFullProfile(customer.getId(), customer.getEmail(),null);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					play.Logger.info("Addd full profile: "+ e.getMessage(),e);
				} 			 				
			}
			
		}else{ 
			
			//divide the task
			int mid =(high+low)>>>1;
			ContactPullTask left = new ContactPullTask(customers, low, mid);
			ContactPullTask right = new ContactPullTask(customers, mid, high); 
			invokeAll(left,right);
		}
		
	}

}
