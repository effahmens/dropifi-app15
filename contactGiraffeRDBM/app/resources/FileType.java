package resources;

public enum FileType {
	AutoReplyLogo,
	WidgetLogo,
	WidgetTab,
	WidgetAttachment,
	MonitoredMailbox
}
