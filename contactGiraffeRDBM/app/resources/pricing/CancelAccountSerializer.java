package resources.pricing;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

import play.data.validation.Required;

public class CancelAccountSerializer implements Serializable{
	@SerializedName("cancelEmail")
	@Required public String cancelEmail;
	@SerializedName("cancelPassword")
	@Required public String cancelPassword;
	@SerializedName("cancelMessage")
	@Required public String cancelMessage;
}
