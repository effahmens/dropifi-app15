package resources.pricing;
import static akka.actor.Actors.actorOf;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import job.process.EventTrackActor;
import job.process.MailingActor;
import models.Company;
import models.dsubscription.DownGradePlan;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubPaymentEvent;

import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import play.data.validation.Required;
import play.data.validation.Valid;
import play.data.validation.Validation;
import resources.DCON;
import resources.DropifiTools;
import resources.EventTrackSerializer;
import resources.MailTemplate;
import resources.dstripe.StCharge;
import resources.dstripe.StEvent;
import resources.dstripe.StInvoice;
import resources.dstripe.StSubscription;
import view_serializers.MailSerializer;
import akka.actor.ActorRef;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Coupon;
import com.stripe.model.Customer;
import com.stripe.model.Event;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceItem;
import com.stripe.model.InvoiceItemCollection;
import com.stripe.model.InvoiceLineItem;
import com.stripe.model.InvoiceLineItemCollection;
import com.stripe.model.Plan;
import com.stripe.model.Subscription;
import com.sun.xml.internal.ws.server.AbstractWebServiceContext;

public class WebhookEvent implements StEvent {
	
	private String stripeKey;
	
	public WebhookEvent(String stripeKey){
		Stripe.apiKey = stripeKey; 
		this.stripeKey = stripeKey;
	} 
	
	private InvoiceLineItem getInvoiceItem(Invoice in,String type){
		InvoiceLineItem foundItem = null; 
	    try{
	    	if(in!=null){
				//send an invoice statement to the customer
			    InvoiceLineItemCollection items = in.getLines();
			    for(InvoiceLineItem item : items.getData()){  
			    	
			    	if(item.getType().equalsIgnoreCase(type)){
				    	foundItem = item;
				    	break;
			    	}
			    }
	    	}
	    }catch(Exception e){
	    	play.Logger.log4j.info(e.getMessage(),e);
	    }
	    return foundItem;
	}
	
	public Invoice processInvoice(@Required String secretKey,@Required String eventId,@Required String eventType,@Valid JSONObject object){
		try {
			//if(!Validation.hasErrors()){
			
				JSONObject data = (JSONObject)((JSONObject)object.get("data")).get("object");
				String inid = (String)data.get("id"); 
				Invoice in = StInvoice.retrieve(inid,DropifiTools.StripeSecretKey);
				if(in!=null){ 
					//play.Logger.log4j.info("Customer ID: "+ in.getCustomer()); 
					SubCustomer cu = SubCustomer.findByCustomerId(in.getCustomer());
															
					if(cu !=null){
						Subscription sub = new StSubscription(DropifiTools.StripeSecretKey).retrieve(cu.getCustomerId(), in.getSubscription()); 

						//check if this particular invoice was sent to customer
						SubPaymentEvent found = SubPaymentEvent.findEvent(eventId, inid, eventType); 
						Date time =null;
						try{
							time = new Date(in.getDate() * 1000);
						}catch(Exception e){
							time = new Date();
							//play.Logger.log4j.info(e.getMessage(),e); 
						}
						
						String plan=null;
			
						if(eventType.equalsIgnoreCase("invoice.payment_succeeded")){ 
							 
							//check if the invoice is sent/mailed to the customer
							found = SubPaymentEvent.findEvent(cu.getApikey()); //check if the initial invoice has been sent to customer
							
							//replace true with this. found == null  
							if(found == null){ 
								//play.Logger.log4j.info("Testing invoice delivery"); 
								Charge ch = StCharge.retrieve(in.getCharge(),secretKey);
								Card card = ch!=null? ch.getCard():cu.hasCardToken(secretKey); 
								double planAmount = Double.valueOf(DropifiTools.converToDecimal(in.getAmountDue(),"#.00"));
								
								if(sub!=null){
									plan = sub.getPlan().getName();
									//play.Logger.log4j.info("From subscription"+ sub.getPlan().getName()+" : "+ sub.getPlan().getAmount());
								}else{
									InvoiceLineItem item = getInvoiceItem(in,"subscription"); 
									plan = item.getPlan().getName();
									//play.Logger.log4j.info("From Line item"+item.getId());
								}
								
								plan = plan.replaceAll("MONTHLY", "Monthly").replaceAll("YEARLY", "Yearly"); 
								
								String receiptid = inid.replace("in_", "DPI_");
								 
								String msg = MailTemplate.getPaymentReceipt(receiptid, card.getName(),cu.getEmail(),("$"+planAmount),plan,time,card, sub.getPlan().getStatementDescription()); 
								//send a receipt to the customer
								MailSerializer mail = new MailSerializer(card.getName(),cu.getEmail(),"","Dropifi Premium Plan Receipt",msg);								
								mail.tellMailingActor();
								
								if(found == null){
									found = new SubPaymentEvent(cu.getApikey(), cu.getEmail(), cu.getName(), eventId, eventType, inid, cu.getCustomerId(), object.toJSONString());
									found.createPayment(); 
								}
							
								//test notification to dropifi:   
								String notifyMsg ="<div style='padding-bottom:10px;border-bottom:1px solid black;'>Payment</div>"+msg;
								MailSerializer notify = new MailSerializer("Philips","philips+invoice@dropifi.com","","Payment receipt sent to "+cu.getEmail(),notifyMsg);								
								notify.tellMailingActor();
								
								EventTrackSerializer event = new EventTrackSerializer(cu.getEmail(), "REVENUE", planAmount);
								event.tellEventTrackerActor();
								
								//play.Logger.log4j.info("invoice found is succeeded: "+plan);
							}else{ 
								//save this invoice if it has not being created before
								found = findEvent(inid, eventType);
								if(found == null){
									found = new SubPaymentEvent(cu.getApikey(), cu.getEmail(), cu.getName(), eventId, eventType, inid, cu.getCustomerId(), object.toJSONString());
									found.createPayment();
								}
							} 
						
							resetRestPlan(cu); 
							
							//upgrade the user to the a plan in case he is not on that plan
							try{ 
								String cuplan = sub.getPlan().getId();
								if(cuplan.startsWith("F"))
									cuplan="Free";
								else if(cuplan.startsWith("S"))
									cuplan="Starter";
								else if(cuplan.startsWith("L"))
									cuplan = "Lite";
								else if(cuplan.startsWith("B"))
									cuplan = "Business";
								
								if(cu.getCurrentPlan().name().startsWith("Free") && plan!=null && !plan.startsWith("Free") && !cuplan.equals(cu.getCurrentPlan().name())){ 
									Company.systemGradePlan(cu.getApikey(), cuplan);
								}
							}catch(Exception e){
								play.Logger.log4j.info(e.getMessage(),e);
							}
							
						}else if(eventType.equalsIgnoreCase("invoice.payment_failed")){
							//send failure notification to customer
							found = SubPaymentEvent.findEvent(eventId, inid, eventType);
							if(found == null){
								found = new SubPaymentEvent(cu.getApikey(), cu.getEmail(), cu.getName(), eventId, eventType, inid, cu.getCustomerId(), object.toJSONString());
								found.createPayment();
							}
							
							if(!sub.getPlan().getId().startsWith("addon")){
								Company.intiateDownGrade(cu.getApikey(), inid);
								startDowngrade(cu.getApikey());
							}else{
								
							} 
							//play.Logger.log4j.info("invoice found is failed");
						} 
						return in;
					}else{
						//play.Logger.log4j.info("charge not found");
					}
				}else{
					//play.Logger.log4j.info("invoice not found");
				} 
			//}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		} 
		
		return null;
	}
	
	public void sendFailureNotification(SubPaymentEvent failed, boolean isDowngrade) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException{
		Charge ch=null;
		SubCustomer cu=null;
		Invoice in =null;  
		if(failed.getEventType().equalsIgnoreCase("invoice.payment_failed")){
			in = StInvoice.retrieve(failed.getTypeId(),DropifiTools.StripeSecretKey);
			ch = StCharge.retrieve(in.getCharge(),DropifiTools.StripeSecretKey);
		}else if(failed.getEventType().equalsIgnoreCase("charge.failed")){
			ch = StCharge.retrieve(failed.getTypeId(),DropifiTools.StripeSecretKey); 
			if(ch!=null){
				try{
				in = StInvoice.retrieve(ch.getInvoice(),DropifiTools.StripeSecretKey);
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		} 
		
		if(in!=null || ch!=null){  
			cu = SubCustomer.findByCustomerId(ch.getCustomer()); 
			if(cu!=null){
				
				//send an invoice statement to the customer
				Card card = ch!=null? ch.getCard():cu.hasCardToken(DropifiTools.StripeSecretKey); 
				String amount = "$"+DropifiTools.converToDecimal(ch.getAmount(),"#.00");
				Date date = null;
				try{
					date = new Date(ch.getCreated() * 1000);
				}catch(Exception e){
					date = new Date();
					//play.Logger.log4j.info(e.getMessage(),e);
				} 

				String msg = "",subject = "",plan = DropifiTools.changePlanName(resources.AccountType.valueOf(cu.getCurrentPlan().name())) +" "+ cu.getSubscriptionType().name();
				
				InvoiceLineItem item = getInvoiceItem(in,"subscription");
				
				if(item!=null)
					plan = item.getPlan().getName();
					
				plan = plan.replaceAll("MONTHLY", "Monthly").replaceAll("YEARLY", "Yearly"); 
				
				if(plan.startsWith("Free"))
					return;
				
				if(isDowngrade){
					msg = MailTemplate.getCardFailureNotification(card.getName(),cu.getEmail(),amount, plan,date,card,ch.getFailureMessage()); 
					subject = "Your credit card for Dropifi premium customer engagement failed";
				}else{
					msg = MailTemplate.getDowngradeNotification(card.getName(),cu.getEmail(),amount, plan,date,card,ch.getFailureMessage());
					subject = "You’re losing your Dropifi premium customer engagement";
				}
				
				//send a receipt to the customer
				MailSerializer mail = new MailSerializer(card.getName(),cu.getEmail(),"",subject,msg);								
				mail.tellMailingActor();
				
				//notify the dropifi team of the downgrade 
				if(play.Play.mode.isProd()){  
					mail = new MailSerializer("Support","support@dropifi.com","","User Card Failure: "+subject,msg);								
					mail.tellMailingActor();
				}else{
					mail = new MailSerializer(DropifiTools.mailName,DropifiTools.mailEmail,"","User Card Failure: "+subject,msg);								
					mail.tellMailingActor();
				} 
			}
		} 
	}
	
	public Charge processCharge(@Required String eventId,@Required String eventType,@Valid JSONObject object){
		try { 
			if(!Validation.hasErrors()){ 
				JSONObject data = (JSONObject)((JSONObject)object.get("data")).get("object"); 
				String chid = (String)data.get("id");
				
				Charge ch = StCharge.retrieve(chid,DropifiTools.StripeSecretKey);
				SubCustomer cu = SubCustomer.findByCustomerId(ch.getCustomer());
				 
				if(ch!=null && cu!=null){
					SubPaymentEvent found = SubPaymentEvent.findEvent(eventId, chid, eventType);
					if(found == null){
						found = new SubPaymentEvent(cu.getApikey(), cu.getEmail(), cu.getName(), eventId, eventType, chid, cu.getCustomerId(), object.toJSONString());
						found.createPayment();
					}
					 
					if(eventType.equalsIgnoreCase("charge.succeeded")){
						resetRestPlan(cu);
					} else if(eventType.equalsIgnoreCase("charge.failed")){
						 
						//send failure notification to customer
						startDowngrade(cu.getApikey());
						//play.Logger.log4j.info("charge found is failed: "+ cu.getApikey());
					}
				} 
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e); 
		}
		
		return null;
	}
	
	public List<Map<String,Object>> getPaymentReceipts(String apikey){
		List<SubPaymentEvent> events = SubPaymentEvent.find("Select p From SubPaymentEvent p Where p.apikey=? and p.eventType=? Order By p.id DESC", apikey,"invoice.payment_succeeded").fetch();
		SubCustomer cu = SubCustomer.findByAppikey(apikey); 
		//List<String> receipts = new LinkedList<String>();
		List<Map<String,Object>> maplist = new LinkedList<Map<String,Object>>();
		for(final SubPaymentEvent event : events){ 
			
			//event.get
			JSONParser par = new JSONParser();
			try {
				JSONObject object = (JSONObject) par.parse(event.getStoredObject());
				JSONObject data = (JSONObject)((JSONObject)object.get("data")).get("object");
				String inid = (String)data.get("id"); 
				Invoice in = Invoice.retrieve(inid);
				if(in!=null){ 
					final InvoiceLineItem item = getInvoiceItem(in, "subscription");
					final Date time = new Date(in.getDate() * 1000);
					final Date beginDate = new Date(in.getPeriodStart() * 1000);
					final Date endDate = new Date(in.getPeriodEnd() * 1000);
					Charge ch = StCharge.retrieve(in.getCharge(),this.stripeKey);
					 
					final Card card = ch!=null? ch.getCard():cu.hasCardToken(this.stripeKey);  
					String amt =DropifiTools.converToDecimal(in.getAmountDue(),"#.00");
					final String amount = amt.equals(".00")?"0.00":amt;
					final String plan = item.getPlan().getName().replaceAll("MONTHLY", "Monthly").replaceAll("YEARLY", "Yearly");
					final String planAmount = DropifiTools.converToDecimal(item.getPlan().getAmount(),"#.00");
					final double discount = processCoupon(in, Double.valueOf(amount));
					 					 					 
					Map<String,Object> map = new HashMap<String,Object>(){{
						put("typeId",event.getTypeId());
						put("created",time.toString());
						put("beginDate",beginDate);
						put("endDate",endDate);
						put("amount",amount);
						put("planAmount",planAmount);
						put("plan",plan);
						put("discount",discount);
						put("name",card.getName());
						put("last4",card.getLast4());
						put("description",item.getDescription());
						 
					}};
					maplist.add(map);
				}
			}catch (ParseException | AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		return maplist;
	}
	
	public int sendInvoice(String typeId, String apikey){ 
		return 501;
	}
	
	private void resetRestPlan(SubCustomer cu){
		DownGradePlan.resetPlan(cu.getApikey(), cu.getCurrentPlan().name());  
	}
	
	private void startDowngrade(String apikey){
		try {
			DownGradePlan plan = DownGradePlan.findDowngradeAccount(apikey);
			if(plan!=null && !plan.isLocked() && !plan.isSuspended()){
				//lock the plan so that no other process will have access to it
				
				if(plan.updateLock(true)){
					//determine if it is time to send notification
					try{
						Date time = new Date();
						WebhookEvent event = new WebhookEvent(DropifiTools.StripeSecretKey);
						if(plan.getCounter()<=3){
							if(time.after(plan.getNotifyDate())){		
								//set down grade date to 1 weeks
								DateTime datetime = new DateTime(time.getTime()).plusWeeks(1);
								plan.updateNotifyDate(new Timestamp(time.getTime()), new Timestamp(datetime.toDate().getTime()));					
								event.sendFailureNotification(SubPaymentEvent.findEvent(plan.getApikey(), plan.getTypeId()),true);
							} 
						}else{
							//suspend the account
							if(plan.downGradePlan()){
								if(Company.downGradePlan(plan.getApikey())!=null){ 
									//send downgrade message 
									event.sendFailureNotification(SubPaymentEvent.findEvent(plan.getApikey(), plan.getTypeId()),false);
								}
							}
						}
					}catch(Exception e){
						play.Logger.log4j.info(e.getMessage(),e);
					}finally{ 
						plan.updateLock(false);
					}
				}
			}
			
		}catch(Exception e) {
			// TODO Auto-generated catch block
			play.Logger.info("Addd full profile: " + e.getMessage(),e);
		} 
	}
	
	private double processCoupon(Invoice in, double amount){
		double redAmt = 0.00; 
		 
		try{
			Coupon coupon = in.getDiscount().getCoupon(); 
			if(coupon.getAmountOff()!=null){
				redAmt  = Double.valueOf(DropifiTools.converToDecimal(coupon.getAmountOff(),"#.00"));
			}else{ 
				redAmt = amount * (coupon.getPercentOff()/100);
			}
		}catch(Exception e){
			redAmt= 0.00;
		}
		
		return redAmt;
	}
	
	public static SubPaymentEvent findEvent(String typeId,String eventType){
		return SubPaymentEvent.find("Select p from SubPaymentEvent p where p.typeId=? and p.eventType=?",typeId, eventType).first();
	}
	
}
