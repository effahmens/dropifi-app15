package resources;

import java.sql.Connection;

import models.InboundMailbox;
import models.addons.AddonDownGrade;
import models.dsubscription.SubConsumeService;
import models.dsubscription.SubService;
import models.dsubscription.SubServiceName;
import adminModels.ApikeyType;
import play.cache.Cache;
import resources.com.AccountType;
import resources.helper.ServiceSerializer;

public enum CacheKey {
	ClientWidgets, 
	Contact,
	Contact_Analytics,
	Customer_Analytics,
	Dashboard,
	Dashboard_Analytics,
	Inbox,
	Inbox_Analytics,
	Keywords,
	Mailboxes,
	Messsage_Analytics,
	QuickResponse,
	Recipients,
	Secure,
	Sentiment_Analytics,
	Shopify,
	Subjects,
	Widget,
	Widget_Rules,
	Agent,
	Analytics,
	Sentiment,
	Wix,
	MonitoredMailboxes,
	WidgetTracker,
	Tictail,
	Addon;
	
	public static String genCacheKey(String apikey,CacheKey cachekey,String... identifier){
		String key = apikey+"_"+cachekey.name(); 
		for(String id:identifier){
			key +="_"+id;
		} 
		return key;
	}
	
	public static String genCookiekey(CacheKey cachekey,String... identifier){
		String key =cachekey.name(); 
		for(String id:identifier){
			key +="_"+id;
		} 
		return key;
	}
	
	public static String getWidgetContentKey(String publicKey) {
		return publicKey+"_"+CacheKey.ClientWidgets.name();
	}
	
	/**
	 * Generate a key for finding the publicKey of a shopify shop using the .myshopify url assigned to the shop
	 * @param shop
	 * @return
	 */
	public static String getShopifyKey(String shop){
		return Shopify.name()+"_"+shop;
	}
	
	/**
	 * Generate a key for finding the publicKey of a Tictail store using the subdomain assigned to the store
	 * @param shop
	 * @return
	 */
	public static String getTictailyKey(String shop){
		return Tictail.name()+"_"+shop;
	}
	
	/**
	 * Generate a key for finding the publicKey of a Tictail store using the subdomain assigned to the store
	 * @param shop
	 * @return
	 */
	public static String getPluginKey(EcomBlogType pluginType,String shop){
		return pluginType.name()+"_"+shop;
	}
	
	/**
	 * Generate a key for finding the publicKey of a wix site using the instanceId assigned to the site
	 * @param instanceId
	 * @return
	 */
	public static String getWixKey(String instanceId){
		return Wix.name()+"_"+instanceId;
	}
	
	/**
	 * Generate a key used for finding the widget tab setting 
	 * @param publicKey
	 * @return
	 */
	public static String getWidgetTabKey(String publicKey) {
		return publicKey+"_"+CacheKey.ClientWidgets.name()+"_WidgetTab";
	}
	
	/**
	 * Generate a key used for finding the widget tab setting 
	 * @param publicKey
	 * @return
	 */
	public static String getWidgetTabKey(String publicKey, String pluginType) {
		return publicKey+"_"+CacheKey.ClientWidgets.name()+"_WidgetTab_"+pluginType;
	}
	
	public static String getMonitoredMailboxKey(String apikey){
		return apikey+"_"+CacheKey.MonitoredMailboxes+"_Total_";
	}
	
	public static String getLymbixKey(){
		return "Apikey_"+ApikeyType.Lymbix.name();
	}
	
	public static String getFullcontactKey(){
		return "Apikey_"+ApikeyType.FullContact.name();
	}
	
	public static String getStripeKey(){
		return "Apikey_"+ApikeyType.Stripe.name();
	}
	
	public static String getTextToImagekey(String apikey){
		return "TextToImage_"+apikey;
	}
	
	/**
	 * Delete the associated value of the key
	 * @param cacheKey
	 */
	public static void deleteCache(String cacheKey){
		try{
			Cache.delete(cacheKey);
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(),e);
		}
	}
	
	
	/**
	 * delete the list of services stored in the cache memory (memcache)
	 * @param apikey
	 */
	public static void delListOfRepFromCache(String apikey,CacheKey cacheKeyType){
		try{
			//String cacheKey = CacheKey.genCacheKey(apikey,cacheKeyType, DropifiTools.POPULATE);
			//String cacheServiceKey = CacheKey.genCacheKey(apikey, cacheKeyType, DropifiTools.SUBSERVICE);
			
			CacheKey.deleteCache(CacheKey.genCacheKey(apikey, cacheKeyType, DropifiTools.SUBSERVICE));
			CacheKey.deleteCache(CacheKey.genCacheKey(apikey,cacheKeyType, DropifiTools.POPULATE)); 
			
			//premium services
			//String pcacheKey = CacheKey.genCacheKey(apikey,cacheKeyType, DropifiTools.POPULATE,"Premium");
			//String pcacheServiceKey =CacheKey.genCacheKey(apikey, cacheKeyType,DropifiTools.SUBSERVICE, "Premium");
			
			CacheKey.deleteCache(CacheKey.genCacheKey(apikey, cacheKeyType,DropifiTools.SUBSERVICE, "Premium"));
			CacheKey.deleteCache(CacheKey.genCacheKey(apikey,cacheKeyType, DropifiTools.POPULATE,"Premium"));
			
			
			if(cacheKeyType.equals(CacheKey.Widget)){
				//String cacheMailKey = CacheKey.genCacheKey(apikey, CacheKey.Widget, "MailBranding");
				CacheKey.deleteCache(CacheKey.genCacheKey(apikey, CacheKey.Widget, "MailBranding"));
			}else if(cacheKeyType.equals(CacheKey.MonitoredMailboxes)){
				//String mmCacheKey = CacheKey.getMonitoredMailboxKey(apikey);
				CacheKey.deleteCache(CacheKey.getMonitoredMailboxKey(apikey));
			}else if(cacheKeyType.equals(CacheKey.Sentiment)) { 
				CacheKey.deleteCache(CacheKey.genCacheKey(apikey,cacheKeyType, "MailDelivered"));
			}
			
			
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e); 
		}
	}
	
	public static void delServiceFromCache(String apikey,CacheKey cacheKeyType){
		try{ 
			String cacheServiceKey = CacheKey.genCacheKey(apikey, cacheKeyType, DropifiTools.SUBSERVICE); 
			CacheKey.deleteCache(cacheServiceKey);
			if(cacheKeyType.equals(CacheKey.Widget)){
				//String cacheMailKey = CacheKey.genCacheKey(apikey, CacheKey.Widget, "MailBranding");
				CacheKey.deleteCache(CacheKey.genCacheKey(apikey,cacheKeyType, "MailDelivered")); 
			}else if(cacheKeyType.equals(CacheKey.MonitoredMailboxes)){
				//String mmCacheKey = CacheKey.getMonitoredMailboxKey(apikey);
				CacheKey.deleteCache(CacheKey.getMonitoredMailboxKey(apikey));
			}else if(cacheKeyType.equals(CacheKey.Sentiment)) { 
				CacheKey.deleteCache(CacheKey.genCacheKey(apikey,cacheKeyType, "MailDelivered"));
			} 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static void delAddonFromCache(String apikey){
		CacheKey.deleteCache(CacheKey.genCacheKey(apikey, Addon,"HasTrialExpired"));
	}
	
	public static Boolean hasAddonExpired(String apikey, String platformKey){
		String cacheKey =CacheKey.genCacheKey(apikey, Addon,"HasTrialExpired");
		Boolean hasExpired = Cache.get(cacheKey,Boolean.class);
		if(hasExpired == null){
			hasExpired = AddonDownGrade.hasTrialExpired(platformKey, apikey);
			CacheKey.set(cacheKey, hasExpired);
		}
		return hasExpired;
	}
	
	public static boolean getHasCaptcha(String publicKey) {
		try {
			String cacheKey = CacheKey.genCacheKey(publicKey, Widget,"HasCaptcha");
			Boolean cap = Cache.get(cacheKey, Boolean.class);
			if(cap==null) {
				cap = models.Widget.findHasCaptcha(publicKey);
				if(cap==null) cap=false;
				
				CacheKey.set(cacheKey, cap);  
			}
			return cap;
		}catch(Exception e) {
			play.Logger.log4j.info(e.getMessage(),e);
		} 
		return false;
	}
	
	/**
	 * save the details of the subscription service to cache
	 * @param apikey
	 * @param service
	 * @param SUBSERVICE
	 */
	public static void cacheSubService(String apikey,CacheKey cacheKeyType, ServiceSerializer service, String name){
		String cacheKey = CacheKey.genCacheKey(apikey, cacheKeyType, DropifiTools.SUBSERVICE);
		cacheSubService(cacheKey,service);
	}
	
	/**
	 * save the details of the subscription  service to cache
	 * @param cacheKey
	 * @param service
	 */
	public static void cacheSubService(String cacheKey,ServiceSerializer service){
		try{
			Cache.set(cacheKey, service);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static ServiceSerializer getServiceSerializer(String apikey,CacheKey cacheKeyType,SubServiceName serviceName){
		String cacheKey = CacheKey.genCacheKey(apikey, cacheKeyType, DropifiTools.SUBSERVICE);
		ServiceSerializer service = Cache.get(cacheKey, ServiceSerializer.class);
		if(service==null){
			service = SubConsumeService.findServiceSerializer(apikey,serviceName);
			cacheSubService(cacheKey, service);
		}
		return service;
	}
	
	public static ServiceSerializer getPremiumServiceSerializer(String apikey,CacheKey cacheKeyType,SubServiceName serviceName){
		
		try{
			String cacheKey = CacheKey.genCacheKey(apikey, cacheKeyType,DropifiTools.SUBSERVICE, "Premium");
			ServiceSerializer service = Cache.get(cacheKey, ServiceSerializer.class);
			if(service==null){
				SubConsumeService consumeService = SubConsumeService.findSubConsumeService(apikey, serviceName); 
				if(consumeService!=null && consumeService.getThreshold()!=null){
					service = new ServiceSerializer(consumeService);
				}else{
					service = createTempServiceSerializer(apikey, serviceName, AccountType.Free);
				}
				CacheKey.cacheSubService(cacheKey, service);
			} 
			return service;
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	/***
	 * Temporary implementation of this method. Will be removed
	 */
	private static ServiceSerializer createTempServiceSerializer(String apikey,SubServiceName serviceName, AccountType accountType){ 
		SubService subService = SubService.findSubService(accountType, serviceName );
		SubConsumeService found = new SubConsumeService(accountType, apikey, subService);
		found.setThreshold(found.getSubServiceId(), serviceName);
		return new ServiceSerializer(found, 0l);
	}
	
	/**
	 * Monitored Mailbox ServiceSerializer
	 * @param apikey
	 * @param cacheKeyType
	 * @param serviceName
	 * @return
	 */
	public static ServiceSerializer getMMServiceSerializer(String apikey,CacheKey cacheKeyType,SubServiceName serviceName){
		String cacheKey = CacheKey.genCacheKey(apikey, cacheKeyType, DropifiTools.SUBSERVICE);
		ServiceSerializer service = Cache.get(cacheKey, ServiceSerializer.class);
		if(service==null){
			service = SubConsumeService.findMMServiceSerializer(apikey,serviceName);
			cacheSubService(cacheKey, service);
		}
		return service;
	}
	
	public static ServiceSerializer getServiceSerializer(Connection conn,String apikey,CacheKey cacheKeyType,SubServiceName serviceName){
		String cacheKey = CacheKey.genCacheKey(apikey, cacheKeyType, DropifiTools.SUBSERVICE);
		ServiceSerializer service = Cache.get(cacheKey, ServiceSerializer.class);
		if(service==null){
			service = SubConsumeService.findServiceSerializer(conn,apikey,serviceName);
			cacheSubService(cacheKey, service);
		}
		return service;
	}
	
	public static ServiceSerializer getServiceSerializer(Connection conn,String apikey,CacheKey cacheKeyType,SubServiceName serviceName,String identifier){
		String cacheKey = CacheKey.genCacheKey(apikey, cacheKeyType, identifier);
		ServiceSerializer service = Cache.get(cacheKey, ServiceSerializer.class);
		if(service==null){
			service = SubConsumeService.findServiceSerializer(conn,apikey,serviceName);
			cacheSubService(cacheKey, service);
		}
		return service;
	}
	
	/***
	 * Temporary implementation of this method. Will be removed
	 */
	public static ServiceSerializer findServiceSerializerBrand(String apikey, SubServiceName serviceName){
		
		try{
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Widget, "MailBranding");
			ServiceSerializer service = Cache.get(cacheKey, ServiceSerializer.class);
			if(service==null){
				SubConsumeService consumeService = SubConsumeService.findSubConsumeService(apikey, serviceName); 
				if(consumeService!=null && consumeService.getThreshold()!=null){
					service = new ServiceSerializer(consumeService);
					CacheKey.cacheSubService(cacheKey, service);
				}
			}
			return service;
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	/***
	 * Temporary implementation of this method. Will be removed
	 */
	public static ServiceSerializer findServiceSerializerBrand(Connection conn,String apikey, SubServiceName serviceName){	
		try {
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Widget, "MailBranding");
			ServiceSerializer service = Cache.get(cacheKey, ServiceSerializer.class);
			if(service==null){
				SubConsumeService consumeService = SubConsumeService.findSubConsumeService(conn,apikey, serviceName);
				if(consumeService!=null && consumeService.getThreshold()!=null){
					service = new ServiceSerializer(consumeService);
					CacheKey.cacheSubService(cacheKey, service);
				} 
			}
			return service;
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	
	public static long getMonitoredMailboxes(String apikey,long companyId){
		String cacheKey =  getMonitoredMailboxKey(apikey);
		Long total = Cache.get(cacheKey, Long.class);
		if(total==null){
			total = InboundMailbox.countMonitoredMailbox(companyId);
			set(cacheKey,total);
		}
		return total;
	}
	
	public static void set(String cacheKey,Object object, String duration){
		try{ 
			duration=duration!=null?duration:"1440mn";
			Cache.set(cacheKey, object,duration);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		 
	}
	
	public static void set(String cacheKey,Object object){
		try{ 
			Cache.set(cacheKey, object);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		 
	}
	
	public static void delete(String cacheKey){
		try{
			Cache.delete(cacheKey);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		 
	} 
}
