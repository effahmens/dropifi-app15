package resources;

public enum RuleConditionType {
   from,
   subject,
   entire_message,
   sender_is_a_customer,
   sender_is_a_prospect,
   contact_name, 
   contact_email_address, 
   message_sentiment,
   message_emotion,
   location_country,
   location_state,
   location_city,
   page_url
}
