package resources;
 

public enum NotificationType {
	/** Weekly Summary emails */
	WEEKLY_SUMMARY,
	
	/** Dropifi Leaderboard emails */
	MONTHLY_LEADERBOARD_MSG,
	
	/** New Message Event Triggered Email */
	NEW_MESSAGE_TRIGGER,
	NEWMSG_NEGATIVE_SENTI_TRIGGER,
	UNRESOLVED_MESSAGE_TRIGGER;
	
	public static NotificationType valueOf(int value){
		switch(value){
			case 0:
				return WEEKLY_SUMMARY;
			case 1:
				return MONTHLY_LEADERBOARD_MSG;
			case 2:
				return NEW_MESSAGE_TRIGGER;
			case 3:
				return NEWMSG_NEGATIVE_SENTI_TRIGGER;
			case 4:
				return UNRESOLVED_MESSAGE_TRIGGER;
			default:
				return null;
		}
		 
	}
}
