package resources;

import static akka.actor.Actors.actorOf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import job.process.EventTrackActor;

import org.json.JSONObject;

import akka.actor.ActorRef;

import com.google.gson.JsonObject;
import com.mixpanel.mixpanelapi.*;

import api.mixpanel.mixpanelapi.MessageBuilderHelper;
import play.libs.WS;



public class EventTrackSerializer implements Serializable { 
		
	private String name;
	private String identity;
	private String event;
	private String IP;
	private String email;
	private String username;
	private String userType;
	private String widgetType;
	private String created;
	private boolean isPeople;
	private String url;
	private String accountType;
	private double amount;
	public String cancelReason;
	public String location;
	public String browser;
	private String phone;
	
	private String key;
	private String value;
	
	public EventTrackSerializer(String name, String identity, String event,String IP,String email,String userType, boolean isPeople) {		 
		this.setName(name);
		this.setIdentity(identity);
		this.setEvent(event);
		this.setPeople(isPeople);
		this.setIP(IP);
		this.setEmail(email);
		this.setUserType(userType);
	}
	
	public EventTrackSerializer(String event,String IP,boolean isPeople){
		this(null,null,event,IP,null,null,isPeople);
	}
	
	public EventTrackSerializer(String email,String event, boolean isPeople, String accountType,String userType,String widgetType){
		this.setIdentity(email);
		this.setEmail(email);
		this.setEvent(event);
		this.setAccountType(accountType);
		this.setUserType(userType);
		this.setWidgetType(widgetType);
	}
	
	public EventTrackSerializer(String email,String event,String cancelReason){
		this(email,event,true,null,null,null);
		this.cancelReason=cancelReason;
	}
	
	public EventTrackSerializer(String email,String event,double amount){
		this(email,event,true,null,null,null);
		this.setAmount(amount);
	}
	
	public EventTrackSerializer(String email,String event,String name,String location,String browser){
		this.setIdentity(email);
		this.email=email;
		this.name=name;
		this.event=event;
		this.location =location;
		this.browser=browser;
	}
	
	public EventTrackSerializer(String email,String event,String key,String value){
		this.setIdentity(email);
		this.email = email;
		this.event = event;
		this.key =key;
		this.value=value;
	}
	
	public EventTrackSerializer(){}
	//===================================================================================
	//events
 
	public void trackSignup(String token){
		if(token!=null){
			MixpanelAPI mMixpanel  = new MixpanelAPI();
			MessageBuilderHelper messageBuilder = new MessageBuilderHelper(token,"0");
					
			ClientDelivery delivery = new ClientDelivery();  
			delivery.addMessage(this.setProperties(messageBuilder));
			delivery.addMessage(this.addEvent(messageBuilder, "SIGNUP_SUCCESSFUL"));
			
			try {
				mMixpanel.deliver(delivery,false);	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(),e);
			}
		}
	}
	
	public void trackLogin(String token){
		if(token!=null){
			MixpanelAPI mMixpanel  = new MixpanelAPI();
			MessageBuilderHelper messageBuilder = new MessageBuilderHelper(token,"0");
		 
			//set increment value
			Map<String,Long> loginMap = new HashMap<String,Long>();
			loginMap.put("LoginCounter", 1L);
			
			JSONObject loginMessage = messageBuilder.increment(this.getIdentity(), loginMap);
			
			ClientDelivery delivery = new ClientDelivery();
			delivery.addMessage(this.setProperties(messageBuilder)); 
			delivery.addMessage(loginMessage);
			delivery.addMessage(this.addEvent(messageBuilder, "LOGIN_SUCCESSFUL")); 
			
			try{
				mMixpanel.deliver(delivery,false);				 
			} catch (IOException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
	}
	
	public static void trackValueActor(String email, String key,String value){
		try{
			ActorRef eventActor = actorOf(EventTrackActor.class).start();   
			eventActor.tell(new EventTrackSerializer(email,"KEYVALUE",key,value)); 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public void trackValue(String token){
		trackValue(token, this.getKey(), this.getValue());
	}
	
	public void trackValue(String token,String key,String value){
		Map<String, String> namePropsMap = new HashMap<String, String>(); 
		namePropsMap.put(key,value); 
		this.trackProfileUpates(token, namePropsMap);
	}
	
	public void trackSubscription(String token){ 
		trackValue(token, "AccountType", this.getAccountType());
	}
	
	public void trackCacncelAccount(String token){ 
		trackValue(token, "CancelReason",this.cancelReason);
	}
	
	public void trackUrl(String token){ 
		trackValue(token, "URL",this.getUrl());
	}
	
	public void trackUrl(String token,String email,String url){
		this.setEmail(email);
		this.setIdentity(email);
		this.setUrl(url);
		this.trackUrl(token);
	}
	
	public void trackTotalMessages(String token,String email,long messages){ 
		trackTotalMessages(token,email,messages, "Total Messages");
	}
	
	public void trackTotalMessages(String token,String email,long messages, String sourceId){
		this.setEmail(email);
		this.setIdentity(email);
		Map<String, String> namePropsMap = new HashMap<String, String>();
		namePropsMap.put(sourceId,String.valueOf(messages));
		if(sourceId.equals("Total Messages")) 
			namePropsMap.put("LastWidgetMsgDate",new Date().toString());
		
		this.trackProfileUpates(token, namePropsMap);
	}
	
	public void trackCharge(String token){
		this.setIdentity(this.getEmail());
		MessageBuilder messageBuilder = new MessageBuilder(token);

		// Track a charge of $29.99 for the user identified by 13793
		JSONObject update = messageBuilder.trackCharge(this.getIdentity(), this.getAmount(), null);

		//Send the update to mixpanel
		MixpanelAPI mixpanel = new MixpanelAPI();
		try {
			mixpanel.sendMessage(update);
		} catch (MixpanelMessageException | IOException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(),e); 
		}
		 
	}
	private JSONObject setProperties(MessageBuilderHelper messageBuilder){
		Map<String, String> namePropsMap = new HashMap<String, String>();
		String date = new Date().toString();
		
        namePropsMap.put("$name", this.getName());
        namePropsMap.put("$email",this.getEmail());
        namePropsMap.put("$username", this.getUsername());
        namePropsMap.put("UserType", this.getUserType());
        namePropsMap.put("WidgetType", this.getWidgetType());
        namePropsMap.put("URL", this.getUrl());
        namePropsMap.put("$created",date);
        namePropsMap.put("UserCreated", this.getCreated()); 
        namePropsMap.put("AccountType", this.getAccountType());
        namePropsMap.put("Phone", this.getPhone());
        
        //message
        JSONObject properties = new JSONObject(namePropsMap);		 
		return messageBuilder.set(this.getIdentity(), properties);
	}
	
	private JSONObject addEvent(MessageBuilderHelper messageBuilder, String event){
		Map<String, String> eventPropsMap = new HashMap<String, String>();
		eventPropsMap.put("UserEmail",this.getEmail());		 
		eventPropsMap.put("UserType", this.getUserType());
		eventPropsMap.put("WidgetType", this.getWidgetType());
		
		JSONObject eventProperties = new JSONObject(eventPropsMap);
		return  messageBuilder.event(this.getIdentity(),event, eventProperties); 
	}
	
	/**
	 * Track the user activity
	 * @param token
	 * @param namePropsMap
	 */
	private void track(String token,Map<String, String> namePropsMap,boolean hasEvent){
		if(token!=null){
			MixpanelAPI mMixpanel  = new MixpanelAPI();
			MessageBuilderHelper messageBuilder = new MessageBuilderHelper(token,"0");

			//message
	        JSONObject properties = new JSONObject(namePropsMap);	

			ClientDelivery delivery = new ClientDelivery();  
			delivery.addMessage(messageBuilder.set(this.getIdentity(), properties));
			
			if(hasEvent) 
				delivery.addMessage(this.addEvent(messageBuilder, this.getEvent()));
			
			try {
				mMixpanel.deliver(delivery,false);  				 
			} catch (IOException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
	}
	
	private void trackProfileUpates(String token,Map<String, String> namePropsMap){	 
		MessageBuilderHelper messageBuilder = new MessageBuilderHelper(token,"0");
		JSONObject update = messageBuilder.set(this.getIdentity(), new JSONObject(namePropsMap));

		// Send the update to mixpanel
		MixpanelAPI mixpanel = new MixpanelAPI();
		/*ClientDelivery delivery = new ClientDelivery();
		delivery.addMessage(update);*/
		try {
			mixpanel.sendMessage(update); 
			//mixpanel.deliver(delivery, true); 
		} catch (MixpanelMessageException | IOException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	/**
	 * track an event or update user property to mixpanel using the event tracker actor
	 */
	public void tellEventTrackerActor(){
		try{
			ActorRef notifyActor = actorOf(EventTrackActor.class).start();
			notifyActor.tell(this);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e); 
		}
	}

	@Override
	public String toString() {
		return "EventTrackSerializer [name=" + name + ", identity=" + identity
				+ ", event=" + event + ", IP=" + IP + ", email=" + email
				+ ", username=" + username + ", userType=" + userType
				+ ", widgetType=" + widgetType + ", created=" + created
				+ ", isPeople=" + isPeople + ", url=" + url + ", accountType="
				+ accountType + ", amount=" + amount + ", cancelReason="
				+ cancelReason + ", location=" + location + ", phone=" + phone
				+ "]";
	}

	//===================================================================================
	//getters and setters
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name!=null?name.trim():"";
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public boolean isPeople() { 
		return isPeople;
	}

	public void setPeople(boolean isPeople) {
		this.isPeople = isPeople;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username!=null?username.trim():"";
	}

	public String getWidgetType() {
		return widgetType;
	}

	public void setWidgetType(String widgetType) {
		this.widgetType = widgetType;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
