package resources;

public enum InboxNavigator {
	Current,
	Previous,
	Next;
	
	public static boolean isCurrent(String value){
		return value.equalsIgnoreCase(Current.name());
	}
	
	public static boolean isPrevious(String value){
		return value.equalsIgnoreCase(Previous.name());
	}
	
	public static boolean isNext(String value){
		return value.equalsIgnoreCase(Next.name());
	}
}
