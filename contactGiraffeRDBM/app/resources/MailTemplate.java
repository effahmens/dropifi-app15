package resources;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List; 
import java.util.Map;

import models.WidgetDefault;
import models.dsubscription.SubServiceName;
import models.dsubscription.SubServiceType;

import org.joda.time.DateTime;

import com.stripe.model.Card;

import query.inbox.Contact_Serializer;
import query.inbox.InboxSelector;
import query.inbox.SocialSerializer;
import resources.helper.ServiceSerializer;
import view_serializers.CoProfileSerializer;

public class MailTemplate{ 
	private static String filehost ="http://a0d8c7134da53657f161-5786ae10fda515b5172c9037d69789d3.r15.cf5.rackcdn.com";
	private static String footer(){
		DateTime date = new DateTime();
		return "<div style='text-align:center; color:#999; font-size:12px; margin-top:5px;'>Turn your site visitors into loyal visitors. " +
		  		"<a href='http://www.dropifi.com/signup' target='_blank'>SignUp For Your Free Account Now!</a>" +
		  		"<br> &#169; "+date.now().getYear()+", Dropifi.com </div>" ;
	} 
	
	private static String customizedFooter(){
		DateTime date = new DateTime(); 
		return "<div style='font-size:13px;'><p style='margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;color:#999'><a style='color:#999' href='http://www.dropifi.com/signup' target='_blank'>" +
				"Turn your site visitors into loyal visitors.<br/>SignUp For Your Free Account Now!<br> &#169; "+date.now().getYear()+", Dropifi.com </a></p></div>";
	}
	
	private static String upgradeReminder(Connection conn,String apikey,String domain,String email){
		ServiceSerializer service = CacheKey.findServiceSerializerBrand(conn,apikey,  SubServiceName.Branding); 
		if(service==null || service.hasExceeded){ 
			return upgradeReminder(conn,apikey, domain,email, "Want to send auto-responses in your own branded templates? Upgrade now!");
		}
		return "";
	}
	
	private static String upgradeReminder(Connection conn,String apikey,String domain,String email,String message){
		ServiceSerializer service = CacheKey.findServiceSerializerBrand(conn,apikey,  SubServiceName.Branding); 
		if(service==null || service.hasExceeded){	
			return "<div style='margin-top:5px;'><a style=' text-align:center;background-color:#5aa9d3; text-decoration:none;color:#FFFFFF;padding:15px;display:block; font-size:14px;' href='https://www.dropifi.com/login?url=/admin/pricing/index&args="+email+"'>"+message+"</a></div>";	
		}
		return "";
	}

	public static String autoResponse(Connection conn,CoProfileSerializer compProfile,String email,String fullName,String subject, String message){
		//check if the
		ServiceSerializer service = CacheKey.findServiceSerializerBrand(conn,compProfile.getApikey(),  SubServiceName.Branding);  
		
		if(service!=null && !service.hasExceeded)
			return customizedAutoResponseTwo(compProfile, email, fullName, subject, message);
	 	else
			return defaultAutoResponseTwo(compProfile, email, fullName, subject, message); 
	}
	
	public static String autoResponse(CoProfileSerializer compProfile,String email,String fullName,String subject, String message){
		//check if the
		ServiceSerializer service = CacheKey.findServiceSerializerBrand(compProfile.getApikey(),  SubServiceName.Branding);  
		
		if(service!=null && !service.hasExceeded)
			return customizedAutoResponseTwo(compProfile, email, fullName, subject, message);
	 	else
	 		return defaultAutoResponseTwo(compProfile, email, fullName, subject, message); 
	}
	
	/**
	 * Used for sending automatic reply to users without branding access
	 * @param compProfile
	 * @param email
	 * @param fullName
	 * @param subject
	 * @param message
	 * @return
	 */
	public static String defaultAutoResponse(CoProfileSerializer compProfile,String email,String fullName,String subject, String message){
		String name = DropifiTools.convertEmailToName(email, fullName);
		
		//check for the company name
		//String cName = ""; 
		String location = "";
		if(compProfile!=null){
			String url = compProfile.getUrl().trim().toLowerCase();
			url = url.startsWith("http")?url:"http://"+url;
			//cName = compProfile.getMainName();
			location="<tr><td>From:</td><td>"+compProfile.getMainName()+"</td></tr>" 
					+"<tr><td>Website: </td><td><a target='_blank' href='"+url+"'>"+ url+"</a></td></tr>";   
		}
		 
		String autoResponse =
				"<!DOCTYPE html >"+
				"<html>"+
				"<head>"+
				"<meta charset='UTF-8' >"+
				"<title>Dropifi.com</title>"+
				"</head>"+ 
					"<body style='background:none repeat scroll 0 0 #EDEDED; color:#232525; font: 13px/1.5 Helvetica,Arial,sans-serif;' >"+
					"<div style='background:none repeat scroll 0 0 #EDEDED; padding-top:10px;padding-bottom:10px;width:100%;height:100%;font: 13px/1.5 Helvetica,Arial,sans-serif;'>"+
			  		"<div style='width:60%;height:auto;margin-top:10px; margin-left:auto; margin-right:auto;'>"+
			  		"<div style='width:100%; background:none repeat scroll 0 0 #5aa9d3; padding:5px 26px; overflow:auto;'>"+	//subject is temporary disabled 
			  		"<div style='float:left; width:60%;padding-top:20px; font-weight:500;color:#fff;font-size:1.8em;'>"+""+
			  		"</div> <div style='width:35%;text-align:right;float:right !important;'>" +
			  		"<img src='"+filehost+"/images/logo.png' width='125' height='70' alt='Dropifi'/>" +
			  		"</div>"+
			  		"</div>"+
			  		"<div style='border:1px solid #ccc; background:none repeat scroll 0 0 #FFFFFF; width:100%;min-height:300px;padding:10px 25px;'>"+
			  
			  		"<div style='font-weight:500; font-size:1.3em; margin-top:10px;'>Hi " + name + ",</div>" +
			  		"<div style='min-height:150px; margin-top:20px; font-size:1.1em;'>"+ 
			  		"<p style='margin-bottom:5px;'>"+message+".</p>" +	  		 
			  		"</div>"+ 
			  
			  		"<div style='border-top:1px solid #efefef; margin-top:20px;'>"+
			  		"<p><table>"+location+"</table>"	+		  		
			  						  		 
			  		"</div>"+ 
			  		"</div>"+ 			  
						footer()+ 
			  	"</div>"+
		        "</div>"+
				"</body>"+
				"</html>";  
				 
		return autoResponse;
	} 
	
	//TODO: Not implemented - customization of mailing template	
	public static String customizedAutoResponse(CoProfileSerializer compProfile,String email,String fullName,String subject, String message){
		String name = DropifiTools.convertEmailToName(email, fullName);
		
		//check for the company name
		String cName = "";
		String location = ""; 
		if(compProfile!=null){
			String url = compProfile.getUrl().trim().toLowerCase();
			url = url.startsWith("http")?url:"http://"+url;
			cName = compProfile.getMainName();
			location ="<tr><td>From:</td><td>"+compProfile.getMainName()+"</td></tr>"  
					 +"<tr><td>Website: </td><td><a target='_blank' href='"+url+"'>"+ url+"</a></td></tr>"; 
		}
		
		String autoResponse =
				"<!DOCTYPE html >"+
				"<html>"+
				"<head>"+
				"<meta charset='UTF-8' >"+
				"<title>Dropifi.com</title>"+
				"</head>"+ 
					"<body style='background:none repeat scroll 0 0 #EDEDED; color:#232525 ;font: 13px/1.5 Helvetica,Arial,sans-serif;' >" +
					"<div style='background:none repeat scroll 0 0 #EDEDED; padding-top:10px;padding-bottom:10px;width:100%;height:100%;font: 13px/1.5 Helvetica,Arial,sans-serif;'>"+
			  		"<div style='width:60%;height:auto;margin-top:10px; margin-left:auto; margin-right:auto;'>"+
			  		"<div style='width:100%; background: none repeat scroll 0 0 "+compProfile.getResponseColor()+"; padding:5px 26px; overflow:auto;min-height:40px;'>"+	//subject is temporary disabled 
			  		"<div style='float:left; width:60%;padding-top:20px; font-weight:500;color:#fff;font-size:1.8em;'>"+""+
			  		"</div> <div style='float:right;text-align:right;'>" +
			  		"<img src='"+compProfile.getLogoUrl()+"' alt='"+cName+"' style='width:80px;height:80px;border:none;color:"+compProfile.getTextColor()+"; font-size:28px'/>" +
			  		"</div>"+
			  		"</div>"+ 
			  		"<div  style='border:1px solid #ccc; background: none repeat scroll 0 0 #FFFFFF; width:100%;min-height:300px;padding:10px 25px;'>"+
			  		"<div  style='font-weight:500; font-size:1.3em; margin-top:10px;'>Hi " + name + ",</div>" +
			  		"<div style='min-height:150px; margin-top:20px; font-size:1.1em;'>"+
			  		"<p style='margin-bottom:5px;'>"+message+".</p>" +  		 
			  		"</div>"+
			  
			  		"<div style='border-top:1px solid #efefef; margin-top:20px;'>"+
			  		"<p><table>"+location+"</table>"+		  		
			  						  		 
			  		"</div>"+ 
			  		"</div>"+			  
						footer()+
			  	"</div>"+
		   
				"<div></body>"+
				"</html>";   
				//C/o Meltwater News, 50 Fremont Street,Suite 200, San Francisco, California 94105
		return autoResponse;
	}
	
	/**
	 * Used for sending automatic reply to users without branding access
	 * @param compProfile
	 * @param email
	 * @param fullName
	 * @param subject
	 * @param message
	 * @return
	 */
	public static String defaultAutoResponseTwo(CoProfileSerializer compProfile,String email,String fullName,String subject, String message){
		String name = DropifiTools.convertEmailToName(email, fullName);
		
		//check for the company name
		String cName = "";
		String location = ""; 
		String url="#";
		if(compProfile!=null){
			url = compProfile.getUrl().trim().toLowerCase();
			url = url.startsWith("http")?url:"http://"+url;
			cName = compProfile.getMainName();
			location ="<tr><td>From:</td><td>"+compProfile.getMainName()+"</td></tr>"  
					 +"<tr><td>Website: </td><td><a target='_blank' href='"+url+"'>"+ url+"</a></td></tr>"; 
		}
		
		String autoResponse ="<!DOCTYPE html>"+ 
				"<html>"+
				"<head>"+
				"</head>"+

				"<body>"+
				"<div style='background:#e6e6e6;color:#4d4b48;font-family:Helvetica,Arial,Verdana,sans-serif;font-size:14px;line-height:18px;margin-top:20px;margin-right:20px;margin-bottom:20px'>"+
				"  <table width='500' style='margin:0 auto;max-width:584px'>"+
				"    <tbody><tr>"+
				"      <td>"+
				"        <table width='500' style='background:none repeat scroll 0 0 #5aa9d3;border:1px solid #a8adad;padding-top:50px;margin-top:20px'>"+
				"          <tbody><tr>"+
				"            <td align='right'><a  style='color:#fff;text-decoration:none;min-height:68px;width:222px' href='https://www.dropifi.com'><img src='"+filehost+"/images/logo.png' width='130' height='70' alt='Dropifi' style='border:none;background-repeat:no-repeat;color:#fff;font-size:28px' /></a></td>"+
				"          </tr>"+
				"        </tbody></table>"+
				"        <table width='500px' cellpadding='24' style='background:#fff;border:1px solid #a8adad;border-top:none'>"+
				"  <tbody><tr>"+
				"    <td>"+
				"     	<div style='min-height:110px;padding:20px'>"+
				"	      <p style='margin-top:0;margin-bottom:12px'>Hi "+name+",</p>"+
					
				"	      <p style='margin-top:0;margin-bottom:12px'>"+message+"</p>"+
				 
				"	      </div><div style='padding:20px;margin-top:40px;border-top:1px solid #efefef;'><p style='margin-top:0;margin-bottom:12px'>From: "+cName+"</p>"+
					      
				"	      <p style='margin-top:0;margin-bottom:12px'>Website: <a href='"+url+"'>"+url+"</a></p></div>"+
				"	     "+
				"    </td>"+
				"  </tr>"+
				"</tbody></table>"+
				"        <table width='500' cellpadding='20' style='background:url("+filehost+"/images/mailtemplate/mail-drop-shadow.png) 50% -2px transparent scroll no-repeat;color:#747778;font-size:11px;line-height:15px;margin-bottom:20px;text-align:center'>"+
				"          <tbody><tr>"+
				"            <td>"+
								customizedFooter()+
				"            </td>"+
				"          </tr>"+
				"        </tbody></table>"+
				"      </td>"+
				"    </tr>"+
				"  </tbody></table><div class='yj6qo'></div><div class='adL'>"+
				"</div></div>"+
				"</body>"+
				"</html>";
		
		return autoResponse;
	}
	
	public static String customizedAutoResponseTwo(CoProfileSerializer compProfile,String email,String fullName,String subject, String message){
		String name = DropifiTools.convertEmailToName(email, fullName);
		
		//check for the company name
		String cName = "";
		String location = ""; 
		String url="#";
		if(compProfile!=null){
			url = compProfile.getUrl().trim().toLowerCase();
			url = url.startsWith("http")?url:"http://"+url;
			cName = compProfile.getMainName();
			location ="<tr><td>From:</td><td>"+compProfile.getMainName()+"</td></tr>"  
					 +"<tr><td>Website: </td><td><a target='_blank' href='"+url+"'>"+ url+"</a></td></tr>"; 
		}
		
		String autoResponse ="<!DOCTYPE html>"+
				"<html>"+
				"<head>"+
				"</head>"+

				"<body>"+
				"<div style='background:#e6e6e6;color:#4d4b48;font-family:Helvetica,Arial,Verdana,sans-serif;font-size:14px;line-height:18px;margin-top:20px;margin-right:20px;margin-bottom:20px'>"+
				"  <table width='500' style='margin:0 auto;max-width:584px'>"+
				"    <tbody><tr>"+
				"      <td>"+
				"        <table width='500' style='background:"+compProfile.getResponseColor()+";border:1px solid #a8adad;padding-top:5px;margin-top:20px'>"+
				"          <tbody><tr>"+
				"            <td align='right'><a  style='color:"+compProfile.getTextColor()+";text-decoration:none;' href='"+url+"'><img src='"+compProfile.getLogoUrl()+"' alt='"+cName+"' style='width:80px;height:80px;border:none;background-repeat:no-repeat;color:"+compProfile.getTextColor()+"; font-size:28px' /></a></td>"+
				"          </tr>"+
				"        </tbody></table>"+
				"        <table width='500px' cellpadding='24' style='background:#fff;border:1px solid #a8adad;border-top:none'>"+
				"  <tbody><tr>"+
				"    <td>"+
				"     	<div style='min-height:110px;padding:20px'>"+
				"	      <p style='margin-top:0;margin-bottom:12px'>Hi "+name+",</p>"+
					
				"	      <p style='margin-top:0;margin-bottom:12px'>"+message+"</p>"+
				 
				"	      </div><div style='padding:20px;margin-top:40px;border-top:1px solid #efefef;'><p style='margin-top:0;margin-bottom:12px'>From: "+cName+"</p>"+
					      
				"	      <p style='margin-top:0;margin-bottom:12px'>Website: <a href='"+url+"'>"+url+"</a></p></div>"+
				"	     "+
				"    </td>"+
				"  </tr>"+
				"</tbody></table>"+
				"        <table width='500' cellpadding='20' style='background:url("+filehost+"/images/mailtemplate/mail-drop-shadow.png) 50% -2px transparent scroll no-repeat;color:#747778;font-size:11px;line-height:15px;margin-bottom:20px;text-align:center'>"+
				"          <tbody><tr>"+
				"            <td>"+
								//customizedFooter()+ 
				"            </td>"+
				"          </tr>"+
				"        </tbody></table>"+
				"      </td>"+
				"    </tr>"+
				"  </tbody></table><div class='yj6qo'></div><div class='adL'>"+
				"</div></div>"+
				"</body>"+
				"</html>";
		
		return autoResponse;
	}

 	public static String developerCode(String recipientEmail, String senderFullName, String senderEmail,String message, String widgetCode) {
		
		String name = senderEmail!=null ? senderEmail:""; 
		
		if(senderFullName !=null && !senderFullName.trim().isEmpty()){
			name = senderFullName;
		}
		
		String developerCode = 
		
				"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"+
				"<html xmlns='http://www.w3.org/1999/xhtml'>"+
				"<head>"+
				"<meta http-equiv='Content-Type' content='text/html; charset='UTF-8' />"+
				"<title>Dropifi.com</title>"+
				"</head>"+

				"<body background='"+filehost+"/images/bg-body.png' text='#232525' style='font: 13px/1.5 Helvetica,Arial,sans-serif;' >"+

		  			"<div id='main' style='width:80%;height:auto;margin-top:10px; margin-left:auto; margin-right:auto;'>"+
		  			"<div style='width:100%; background-color:#5aa9d3; padding:5px 26px; overflow:auto;'>"+
		  			"<div style='float:left; width:60%;padding-top:20px; font-weight:500;color:#fff;font-size:1.8em;'>Dropifi widget code from "+name+"</div> <div style='float:right;width:35%;text-align:right;'><img src='"+filehost+"/images/logo.png' width='125' height='70'/></div>"+
		  			"</div>"+
		  			"<div id='content' style='border:1px solid #ccc;background-color:#fff;width:100%;min-height:300px;padding:10px 25px;'>"+
		  
		  			//"<div  style='font-weight:500; font-size:1.3em; margin-top:10px;'>Hi </div>"+ 
		  			"<div style='min-height:150px; margin-top:20px; font-size:1.1em;'>"+
		  			"<p style='margin-bottom:5px;'>"+message+"</p>"+
		  					  
		  			"<p style='margin-bottom:5px; border:1px solid #C00; width:90%; margin:0 auto; padding:10px;'>"+widgetCode+"</p>"+
		  			"</div>"+
		  
		  			//"<div style='border-top:1px solid #efefef; margin-top:20px;'>"+
		  			//"<p>The Dropifi Team <span style='margin-left:20px'>(team@dropifi.com)<span></p>"+		  			 
		  			//"</div>"+
		  			"</div>"+
		  
					footer()+
		   
				"</body>"+
				"</html>";

		return developerCode;
	}
	
	//TODO: Not implemented - customization of mailing template	
	@Deprecated
 	public static String forwardedOld(RuleParameters param, String recipientName, String recipientEmail, String optional){
		Contact_Serializer customer;
		String forwarded ="";
		
		try {
			customer = InboxSelector.findCustomerProfile(DCON.getDefaultConnection(),param.companyId, param.sender); 			
		
		String name = recipientEmail;
		if(recipientName!=null && !recipientName.trim().isEmpty()){
			name = recipientName;
		}
		
		String contactName = param.contactName;
		
		String sentiColor ="inherit";
		if(param.sentiment!=null){
			String sent = param.sentiment.trim().toLowerCase();
			switch(sent){
				case "positive":
					sentiColor = "green";
					break;
				case "negative":
					sentiColor = "red";
					break;
				case "neutral":
					sentiColor="#666"; 
					break;
			}
		}
		
		String phone = "";
		if(param.phone!=null && !param.phone.isEmpty()){
			phone = "<tr><td><span>Phone:</span></td> <td><span style='margin-left:15px; color:#666;'>"+param.phone+ "</span></td> </tr>"; 
		} 
		
		forwarded = 
				"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"+
				"<html xmlns='http://www.w3.org/1999/xhtml'>"+
				"<head>"+
				"<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />"+
				"<title>Dropifi.com</title>"+
				"</head>"+  

				"<body background='"+filehost+"/images/bg-body.png' text='#232525' style='font: 13px/1.5 Helvetica,Arial,sans-serif;' >"+

					  "<div id='main' style='width:80%;height:auto;margin-top:10px; margin-left:auto; margin-right:auto;'>"+
					  "<div style='width:100%; background-color:#5aa9d3; padding:5px 26px; overflow:auto;'>"+
					  "<div style='float:left; width:60%;padding-top:20px; font-weight:500;color:#fff;font-size:1.8em;'>Forwarded Message</div> <div style='float:right;width:35%;text-align:right;'><img src='"+filehost+"/images/logo.png' width='125' height='70'/></div>"+
					  "</div>"+
					  "<div id='content' style='border:1px solid #ccc;background-color:#fff;width:100%;min-height:300px;padding:10px 25px;'>"+
					  
					  "<div  style='font-weight:500; font-size:1.3em; margin-top:10px;'>Hi "+name+"</div>"+
					  "<div style='min-height:150px; margin-top:20px; font-size:1.1em;'>"+
					  "<p style='margin-bottom:5px;'>You received a message from "+ contactName +(param.created!=null?(" on "+ param.created):" ")+", the details :</p>"+
					  
					  "<p style='margin-bottom:10px;'><strong>"+(customer.getFullName()!=null?customer.getFullName():" ")+" ("+customer.getEmail()+") Profile</strong></p>"+
					  
					  "<p style='margin-bottom:5px;'>"+
					  "<div style='overflow:auto; background-color:#FDFFFA;'><div style='width:120px; height:120px; background-color:#ccc; float:left;'> " +
					  (customer.getImage() != null ? "<img style='width:120px; height:120px; display:inline' src ='"+customer.getImage()+"'/>":"")+ 
					  "</div>"+
					  
					  "<div style='width:auto; margin-left:5px; float:left;'>"+
		  
					 "<table>"+
					  "<tr><td><span>Gender:</span></td> <td><span style='margin-left:15px; color:#666;'> "+ 
					 ((customer.getGender()!=null) ? customer.getGender(): " ")+
					 ((customer.getAge()!=null && customer.getAge()!=0)? " ("+customer.getAge()+" years)":" (--)" )+"</span></td></tr>"+
					  "<tr><td><span>Location:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
					  ((customer.getLocation()!=null)?customer.getLocation():"--" )+ "</span></td> </tr>"+
					  phone+
					  "<tr><td><span>Company:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
					  ((customer.getCompany()!=null)?customer.getCompany():"--" )+"</span></td> </tr>"+
					  "<tr><td><span>Title:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
					  ((customer.getTitle()!=null)?customer.getTitle():"--" )+"</span></td> </tr>"+
					  "<tr><td><span>Biography:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
					  ((customer.getBio()!=null)?customer.getBio():"--" )+"</span></td> </tr>"+ 
					  "<tr><td><span>Social Link:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
					  socialList(customer.getSocials())+"</span></td> </tr>"+
					  "</table>"+
					  
					  "</div>"+
					  "</div>"+
					  "</p>"+
					  
					  "<p style='margin-bottom:10px;margin-top:10px;'><strong>"+(customer.getFullName()!=null?customer.getFullName():customer.getEmail())+" Message</strong></p>"+
					  "<div>"+
					  "<table>"+
					  "<tr><td><span style='color:#0070BB'>Subject:</span></td> <td><span style='margin-left:15px;'>"+param.subject+"</span></td></tr>"+
					  "<tr><td><span style='color:#0070BB'>Sentiment:</span></td> <td><span style='margin-left:15px; color:"+sentiColor+"'>"+(param.sentiment!=null?param.sentiment:" ")+"</span></td></tr>"+
					  "<tr><td><span style='color:#0070BB'>Emotion:</span></td> <td><span style='margin-left:15px;color:"+sentiColor+"'>"+(param.emotion!=null?param.emotion.replace('_', ' '):" ")+"</span></td></tr>"+
					  "<tr><td><span style='color:#0070BB'>Intense Sentence:</span></td> <td><span style='margin-left:15px;'>"+(param.intense!=null?param.intense:"")+"</span></td></tr>"+
					  "<tr><td><span style='color:#0070BB'>Message:</span></td> <td><span style='margin-left:15px;'>"+param.message+"</span></td></tr>"+
					  "</table>"+
					  
					  "</div>"+
					  	optional +
					  "</div>"+
					  
					  "<div style='border-top:1px solid #efefef; margin-top:20px;'>"+
					  "<p>The Dropifi Team <span style='margin-left:20px'>(team@dropifi.com)<span></p>"+					 
					  "</div>"+
					  "</div>"+
					  
					  "<div style='text-align:center; color:#999; font-size:12px; margin-top:5px;'>Dropifi Limited </div>"+
					  "</div>"+
		   
				"</body>"+
				"</html>";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(),e);
		}
		return forwarded;
	}
	
	public static String reRouted(Connection  conn,RuleParameters param){
		String conditions = param.condition!=null? param.condition.replace('_', ' '):"";
		String optional = "<p style='margin-bottom:5px;'>Reason for redirect: <strong>"+(conditions)+"</strong></p>";
		return forwarded(conn,param, optional); 
	}
	
	public static String socialList(List<SocialSerializer> socials){ 
		StringBuilder socialProfile = new StringBuilder();
		
		if(socials == null)
			return socialProfile.toString();
		
		for(SocialSerializer val : socials){
			String photo = val.getName() !=null ? val.getName()+".png":"";				
			String social = val.getName()+"\r\nfollowers : "+val.getFollowers()+"\r\nfollowing : "+val.getFollowing();
			socialProfile.append("<a target='_blank' href='"+val.getUrl()+"'style='padding-right:5px;' class= 'contact_social_link' title ='"+social+"'><img style = 'display:inline' src='"+filehost+"/images/social/"+photo+"'/></a>");
		}
		return socialProfile.toString();
	}

	@Deprecated
	public static String deforwarded(Connection  conn,RuleParameters param, String recipientName, String recipientEmail, String optional){ 
		
		String forwarded =""; 
	
		try {			
			Contact_Serializer customer = param.getContactProfile();		
		 
		String name = recipientEmail;
		if(recipientName!=null && !recipientName.trim().isEmpty()){
			name = recipientName;
		}
 	
		String sentiColor ="inherit";
		if(param.sentiment!=null){
			String sent = param.sentiment.trim().toLowerCase();
			switch(sent){
				case "positive":
					sentiColor = "green";
					break;
				case "negative":
					sentiColor = "red";
					break;
				case "neutral":
					sentiColor="#666"; 
					break;
			}
		} 
		
		String phone = "";
		if(param.phone!=null && !param.phone.isEmpty()){
			phone = "<tr><td><span>Phone:</span></td> <td><span style='margin-left:15px; color:#666;'>"+param.phone+ "</span></td> </tr>"; 
		}
	
		forwarded +=  
					"<!DOCTYPE html >"+
					"<html>"+
					"<head>"+
					"<meta  charset='UTF-8' >"+
					"<title>Dropifi.com</title>"+
					"</head>"+			
					"<body background='"+filehost+"/images/bg-body.png' text='#232525' style='font: 13px/1.5 Helvetica,Arial,sans-serif;' >"+
				
					"<div id='main' style='width:90%;height:auto;margin-top:10px; margin-left:auto; margin-right:auto;'>"+  				  
					"<!--Header start-->"+
					"<div style='width:100%; background-color:#5aa9d3; padding:5px 26px; overflow:auto;'>"+
					"<div style='float:left; width:60%;padding-top:20px; font-weight:500;color:#fff;font-size:1.8em;'  title='subject of message'>"+//+param.subject+"<br/>"+
					"<span style=' font-style: italic;font-size:16px'  title='Summary of message'>"+param.intense+"</span></div>"+  
					"<div style='float:right;width:35%;text-align:right;'><img src='"+filehost+"/images/logo.png' width='125' height='70'/></div>"+
					"</div>"+
					"<!--Header end-->"+ 
				 
				    "<!--Content start-->"+ 
				    "<div id='content' style='border:1px solid #ccc;background-color:#fff;width:100%;min-height:300px;padding:10px 25px; overflow:auto;'> "+
				    
				      "<div style='float:left;width:60%;'>"+ 
				      "<div  style='font-weight:600; margin-top:10px;'>From: "+param.contactName+" ("+customer.getEmail()+")</div>"+
				      
				       "<div>"+
		              "<table><tr>"+ 
		              "<td><strong style='color:#0070BB'>Sentiment:</strong> <span style='margin-left:5px; color:"+sentiColor+"'>"+(param.sentiment!=null?param.sentiment:" ")+"</span></td>"+
		              "<td><strong style='color:#0070BB;margin-left:15px;'>Emotion:</strong>    <span style='margin-left:5px; color:"+sentiColor+"'>"+(param.emotion!=null?param.emotion.replace('_', ' '):" ")+"</span></td></tr>";
		
		if(param.pageUrl!=null){
			forwarded +=  "<tr><td colspan='2'><strong style='color:#0070BB'>Sent From:</strong><span style='margin-left:5px;'>"+param.pageUrl+"</span></td></tr>";
		}
		
		forwarded +=  
				  "</table>"+  
	               
	              "</div>"+
			      
			      "<div style='min-height:150px; margin-top:20px; font-size:1.1em;'>"+
			          "<div style='margin-bottom:5px;'><fieldset style='min-height:300px;border-color: 1px #CCCCCC solid'><legend>Message</legend><p style='font-size:14px'>"+param.message+"</p></fieldset></div>"+  
			      "</div>"+
			      
			      "</div>"+
			       
			      "<div style='float:right; width:35%; border-left:1px solid #efefef; padding-left:8px;'>"+
			      
			      "<p style='margin-bottom:5px;'><strong>"+param.contactName+"'s Profile</strong></p>"+ 
			      
			      "<p style='margin-bottom:5px;'>"+
			              "<div style='overflow:auto; background-color:#FDFFFA;'>"+
			              "<div style='width:188px; height:165px; background-color:#ccc;padding:0px;margin:0px;'>"+
			              (customer.getImage() != null ? "<img style='width:188px;min-height:165px;height:165px !important;max-height:165px;padding:0px; display:inline' src ='"+customer.getImage()+"'/>":" ")+
			              "</div>"+
			              
			                      "<div style='width:auto; margin-left:5px; margin-top:10px;'>"+ 
			                      
									"<table>"+
									"<tr><td><span>Gender:</span></td> <td><span style='margin-left:15px; color:#666;'> "+ 
									((customer.getGender()!=null) ? customer.getGender(): " ")+
									((customer.getAge()!=null && customer.getAge()!=0)? " ("+customer.getAge()+" years)":" (--)" )+"</span></td></tr>"+
									"<tr><td><span>Location:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
									((customer.getLocation()!=null)?customer.getLocation():"--" )+ "</span></td> </tr>"+
									phone +
									"<tr><td><span>Company:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
									((customer.getCompany()!=null)?customer.getCompany():"--" )+"</span></td> </tr>"+
									"<tr><td><span>Title:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
									((customer.getTitle()!=null)?customer.getTitle():"--" )+"</span></td> </tr>"+
									"<tr><td><span>Bio:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
									((customer.getBio()!=null)?customer.getBio():"--" )+"</span></td> </tr>"+ 
									"<tr><td><span>Social:</span></td> <td><span style='margin-left:15px; color:#666;'>"+
									socialList(customer.getSocials())+"</span></td> </tr>"+
									"</table>" +
			                          
			                      "</div>" + 
			              "</div>"+
			          "</p>"+
			      
			      "</div>"+		      
   
		
			  "</div>"+
			      "<!--Content end-->"+
			  
			  	  "<!--Footer start-->"+
			  	   upgradeReminder(conn,param.apikey,param.domain,param.compProfile.getEmail())+
			      "<div style='text-align:center; color:#999; font-size:12px; margin-top:5px;'>Dropifi Limited</div>"+
			      "<!--Footer end-->"+
			  
			  "</div>"+
			   
			"</body>"+
			"</html>";	
		              
		} catch (Exception e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(),e);
		}
		return forwarded;
	}

	public static String forwarded(Connection conn,RuleParameters param, String optional){
		
		if(param.compProfile!=null && param.compProfile.getAccountType()!=null && param.compProfile.getAccountType().equals(AccountType.Free.name())){
			ServiceSerializer service = CacheKey.getServiceSerializer(conn, param.apikey, CacheKey.Sentiment, SubServiceName.Sentiment, "MailDelivered");
	 
			if(service!=null && service.hasExceedLimit(SubServiceType.FixedVolume, param.countMsg)){
				String priceUrl=DropifiTools.ServerURL+"/login?url=/admin/pricing/index";
				param.message ="<div style=''><p>There is 1 new message waiting for your reply. You are on our free plan and have exceeded the allowed number of messages delivered through this notification. </p>"
						+ "<p>To receive complete messages and reply directly to your customers from here, "
						+ "<a href='"+priceUrl+"' style='text-decoration:underline; font-size:14px;font-family:Arial,sans-serif;color:#dd4b39;font-weight:bold' target='target'>Upgrade to any of our paid plans (Start at $"+DropifiTools.MINIMUMPRICE+")</a></p>"
						+ "</div>";
			}
			
		}
		
		if(param.messageTemplate!=null && (param.messageTemplate.equals("PlainText") || param.messageTemplate.equals("Plain Text")))
			return param.message;
		
		ServiceSerializer service = CacheKey.findServiceSerializerBrand(conn,param.apikey,  SubServiceName.Branding);
		
		if(param.countMsg >=10 && (service == null || service.hasExceeded)){
			return defaultForwarded(conn, param, optional);
		}else{
			return premiumForwarded(conn, param, optional);
		}
	}
	
	public static String defaultForwarded(Connection conn,RuleParameters param,String optional){
		String forwarded="";
		try{ 
			Contact_Serializer customer = param.getContactProfile();
			String viewMsgUrl=DropifiTools.ServerURL+"/login?url=/inbox/index&scrollToId="+param.msgId, viewContUrl=viewMsgUrl, sentiColor ="inherit", sentiUrl="",customerBio="", customerImg = (customer.getImage()!=null && !customer.getImage().isEmpty())?customer.getImage():""+filehost+"/images/default_customer.png";
			String priceUrl=DropifiTools.ServerURL+"/login?url=/admin/pricing/index";
			if(param.sentiment!=null){
				String sent = param.sentiment.trim().toLowerCase();
				switch(sent){
					case "positive":
						sentiColor = "#167616";
						sentiUrl=""+filehost+"/images/emoticons/positive.png";
						break;
					case "negative":
						sentiColor = "#FE4949";
						sentiUrl=""+filehost+"/images/emoticons/negative.png";
						break;
					case "neutral":
						sentiColor="#AEAEAE";
						sentiUrl=""+filehost+"/images/emoticons/neutral.png";
						break;
				}
			}
			
			if(customer.getBio()!=null && !customer.getBio().isEmpty() && !customer.getBio().equals("N/A")){
				customerBio="<tr><td width='100%'><span style='color:#666;font-family:Helvetica,Arial,sans-serif'>"+customer.getBio()+"</span></td></tr>";
			}else if(customer.getTitle()!=null && !customer.getTitle().isEmpty() && !customer.getTitle().equals("N/A")){
				customerBio="<tr><td width='100%'><span style='color:#666;font-family:Helvetica,Arial,sans-serif'>"+customer.getTitle()+"</span></td></tr>";
			}
			
			forwarded ="<!DOCTYPE html>"+
			"<html><head></head><body>"+
	
			"<div><div class='adM'>"+
			"  </div><table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#DFDFDF' style='font-family:Arial;border:solid 1px #dfdfdf;border-radius:0px'><tbody><tr><td width='98%' valign='top' align='center'><table width='545' cellspacing='0' cellpadding='0' border='0' bgcolor='#DFDFDF' style='font-family:Arial;border:solid 1px none;border-radius:0px;border:none'><tbody><tr><td width='98%' valign='top' align='left'>"+
			  
			"  <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial;background:#5AA9D3'>"+
			"    <tbody><tr>"+
			"      <td>"+
			"        <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table>"+
			"      </td>"+
			"    </tr>"+
			"    <tr>"+
			"      <td align='right' style='padding-right:15px;'>"+
			"        <a target='_blank' title='Dropifi' href='http://www.dropifi.com'>"+
			"        <img width='100' height='50' border='0' alt='' src='"+filehost+"/images/dropifi_logo_home.png'>"+
			"        </a>"+
			"      </td>"+
			"		    </tr>"+
			"		    <tr>"+
			"		      <td>"+
			"		        <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table>"+
			"		      </td>"+
			"		    </tr>"+
			"		  </tbody></table>"+
			  
			"		  <table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#333333' style='font-family:Arial;border:solid 1px #ffffff;border-radius:0px;border:none;background:#333'><tbody><tr><td width='98%' valign='top' align='left'>"+
			"		    <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		      <tbody><tr>"+
			"		        <td>"+
			"		          <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:1px;font-size:10px;line-height:1px'>&nbsp;</div></td></tr></tbody></table>"+
			"		        </td>"+
			"		      </tr>"+
			"		    </tbody></table>"+
			"		  </td></tr></tbody></table>"+
			"		  <table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#FFFFFF' style='font-family:Arial;border:solid 1px #ffffff;border-radius:0px;border:none'><tbody><tr><td width='98%' valign='top' align=''>"+
			"		    <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		      <tbody><tr><td height='20' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:20px;font-size:20px;line-height:20px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='60' height='60'>"+
			"		            <a target='_blank' href='"+viewContUrl+"'><img width='60' height='60' border='0' src='"+customerImg+"'></a>"+
			"		        </td>"+
			"		        <td width='10'><table width='10' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%'>"+
			"		          <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		            <tbody><tr>"+
			"		              <td width='100%'><a target='_blank' style='text-decoration:none' href='"+viewContUrl+"'><span style='color:#333;font-size:18px;font-family:Helvetica,Arial,sans-serif;font-weight:bold;text-transform: capitalize;'>"+param.contactName+"</a></td>"+
			"		            </tr>"+
							customerBio +
			"		            <tr><td width='100%'><span style='color:#666;font-family:Helvetica,Arial,sans-serif'><a href='"+priceUrl+"' style='text-decoration:underline; font-size:12px;font-family:Arial,sans-serif;color:#dd4b39;' target='target'>Upgrade to <strong>Pro Plan</strong> View Customer Demographic and Social Profile</a></span></td></tr>"+
			//"		            <tr><td width='100%'><span style='background-color:#f1f1f1;font-size:12px;color:#666'>Sent from</span><a style='font-size:12px;font-family:Helvetica,Arial,sans-serif;padding-left:5px;' href='"+param.pageUrl+"' target='_blank'>"+param.pageUrl+"</a></td></tr>"+
			"		          </tbody></table>"+
			"		        </td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			      
			"		      <tr><td height='10' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='font-size: 20px; min-height: 10px; line-height: 10px;'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' colspan='3'><div style='min-height:1px;font-size:1px;line-height:1px;border-top:1px solid #e1e1e1'>&nbsp;</div></td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr>"+
			"		      	<td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      	<td width='100%' colspan='3'><table width='100%' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td align='right'><div style='min-height:20px;line-height:20px;font-size:12px;padding-top:5px;padding-bottom:10px;'><a href='"+viewMsgUrl+"' target='_blank'><img width='20px' height='20px' src='"+sentiUrl+"' alt='"+
						     (param.sentiment!=null &&!param.sentiment.trim().isEmpty()&& param.sentiment!="NoSentiment"?param.sentiment:" ")+"' ><span style='float:right; color:"+sentiColor+";text-transform: capitalize;padding-left:3px;'>"+(param.sentiment!=null && !param.sentiment.trim().isEmpty() &&param.sentiment!="NoSentiment"?param.sentiment+" Sentiment":"")+"</span></a></div></td></tr></tbody></table></td>"+
			"		      </tr>  " +
			
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' style='font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#666' colspan='3'> "+
								param.message +
			"		        </td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr><td height='25' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:25px;font-size:25px;line-height:25px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' colspan='3'>"+
			"		          <table cellspacing='0' cellpadding='0' border='0' align=''><tbody><tr><td valign='middle' height='30' bgcolor='#5AA9D3' background='' align='center' style='border-radius: 3px; background-color: rgb(90, 169, 211); border: 1px solid rgb(90, 169, 211);'><div style='padding-right:13px;padding-left:13px'><a target='_blank' style='text-decoration:none' href='"+viewMsgUrl+"'><span style='font-size:13px;font-family:Arial;font-weight:bold;color:#ffffff;white-space:nowrap;display:block'>"+
			"		            Reply to <span class='il' style='text-transform: capitalize;'>"+param.contactName+"</span>"+
			"		          </span></a></div></td></tr></tbody></table>"+
			"		        </td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr><td height='15'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:15px;font-size:15px;line-height:15px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' style='font-size: 12px; color: rgb(153, 153, 153);' colspan='3'>";
			
			if(param.pageUrl!=null && !param.pageUrl.equals("null") && !param.pageUrl.trim().isEmpty())
				forwarded +="<span style='background-color:#f1f1f1;font-size:12px;color:#666'>Sent from</span><a style='font-size:12px;font-family:Arial,sans-serif;color:#999999;padding-left:5px;' href='"+param.pageUrl+"' target='_blank'>"+param.pageUrl+"</a> ";
			
			forwarded += "</td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr><td height='20' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:20px;font-size:20px;line-height:20px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		    </tbody></table>"+
			"		  </td></tr></tbody></table><div class='im'>"+
			"		  <table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#FFFFFF' style='font-family:Arial;border:solid 1px #ffffff;border-radius:0px;background-color:#dfdfdf;border:none'><tbody><tr><td width='98%' valign='top' align=''>"+
			"		    <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		      <tbody><tr>"+
			"		        <td>"+
	 
			"		<table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		  <tbody><tr><td><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		  <tr>"+
			"		    <td align='center' style='font-size:11px;font-family:Arial,sans-serif;color:#999999'>             You are receiving Dropifi message emails. <a target='_blank' href='"+DropifiTools.ServerURL+"/login?url=/admin/widgets/index' style='color:#006699;text-decoration:underline'>Unsubscribe</a>."+
			"		        <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table><a style='color:#006699;text-decoration:underline; text-align: center;' href='http://www.dropifi.com' target='_blank'>Dropifi Limited</a></td>"+
			"		  </tr>"+
			"		  <tr><td><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		</tbody></table>"+
	
			"		        </td>"+
			"		      </tr>"+
			"		    </tbody></table>"+
			"		  </td></tr></tbody></table>"+
			"		</div></td></tr></tbody></table></td></tr></tbody></table>"+ 
			
			"		</div>"+ 
			"		</body></html>";
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
 		return forwarded;
	}
	
	public static String premiumForwarded(Connection conn,RuleParameters param, String optional){
		String forwarded="";
		try{ 
			Contact_Serializer customer = param.getContactProfile();
			String viewMsgUrl=DropifiTools.ServerURL+"/login?url=/inbox/index&scrollToId="+param.msgId, viewContUrl=viewMsgUrl, sentiColor ="inherit", sentiUrl="",customerTitle="", 
					customerImg = (customer.getImage()!=null && !customer.getImage().isEmpty())?customer.getImage():""+filehost+"/images/default_customer.png";
			if(param.sentiment!=null){
				String sent = param.sentiment.trim().toLowerCase();
				switch(sent){
					case "positive":
						sentiColor = "#167616";
						sentiUrl=""+filehost+"/images/emoticons/positive.png";
						break;
					case "negative":
						sentiColor = "#FE4949";
						sentiUrl=""+filehost+"/images/emoticons/negative.png";
						break;
					case "neutral":
						sentiColor="#AEAEAE";
						sentiUrl=""+filehost+"/images/emoticons/neutral.png";
						break;
				}
			}
			String[] demog = new String[5]; 
			if(customer.getBio()!=null && !customer.getBio().isEmpty() && !customer.getBio().equals("N/A")){
				demog[0]="<tr><td width='100%'><span title='Biography' style='color:#666;font-family:Helvetica,Arial,sans-serif'>"+customer.getBio()+"</span></td></tr>";
			}else{
				demog[0]="";
			}
			
			if(customer.getGender()!=null && !customer.getGender().isEmpty() && !customer.getGender().equals("N/A")){
				demog[1]="<tr><td width='100%'><span title='Gender (Age)' style='color:#666;font-family:Helvetica,Arial,sans-serif'>"+customer.getGender()+(customer.getAge()!=null&& customer.getAge()>0?" ("+customer.getAge()+" years)":"") +"</span></td></tr>";
			}else if(customer.getAge()!=null && customer.getAge()>0){
				demog[1]="<tr><td width='100%'><span title='Gender (Age)' style='color:#666;font-family:Helvetica,Arial,sans-serif'>"+customer.getAge()+" years </span></td></tr>";
			}else{
				demog[1]="";
			}
			
			if(customer.getLocation()!=null && !customer.getLocation().isEmpty() && !customer.getLocation().equals("N/A")){
				demog[2]="<tr><td width='100%'><span title='Current Location of customer' style='color:#666;font-family:Helvetica,Arial,sans-serif'>"+customer.getLocation()+"</span></td></tr>";
			}else{
				demog[2]="";
			}
			
			if(param.phone!=null && !param.phone.isEmpty() && !param.phone.equals("N/A")){
				demog[3]="<tr><td width='100%'><span title='Phone' style='color:#666;font-family:Helvetica,Arial,sans-serif'>Tel: "+param.phone+"</span></td></tr>";
			}else{
				demog[3]="";
			}
			
			if(customer.getSocials()!=null && customer.getSocials().size()>0){
				demog[4]="<tr><td width='100%'><span title='Social media links of customer' style='color:#666;font-family:Helvetica,Arial,sans-serif'>"+socialList(customer.getSocials())+"</span></td></tr>";
			}else{
				demog[4]="";
			}
			
			/*
			if(param.pageUrl!=null && !param.pageUrl.isEmpty()){
				demog[5]="<tr><td width='100%'><span style='background-color:#f1f1f1;font-size:12px;color:#666'>Sent from</span><a style='font-size:12px;font-family:Helvetica,Arial,sans-serif;padding-left:5px;' href='"+param.pageUrl+"' target='_blank'>"+param.pageUrl+"</a></td></tr>"; 
			}else{ 
				demog[5] ="";
			}
			*/
			
			if(customer.getTitle()!=null && !customer.getTitle().isEmpty() && !customer.getTitle().equals("N/A")){
				customerTitle=" ("+customer.getTitle()+")";
			}
			
			forwarded = "<!DOCTYPE html>"+
			"<html><head></head><body>"+
	
			"<div><div class='adM'>"+
			"  </div><table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#DFDFDF' style='font-family:Arial;border:solid 1px #dfdfdf;border-radius:0px'><tbody><tr><td width='98%' valign='top' align='center'><table width='545' cellspacing='0' cellpadding='0' border='0' bgcolor='#DFDFDF' style='font-family:Arial;border:solid 1px none;border-radius:0px;border:none'><tbody><tr><td width='98%' valign='top' align='left'>"+
			  
			"  <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial;background:#5AA9D3'>"+
			"    <tbody><tr>"+
			"      <td>"+
			"        <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table>"+
			"      </td>"+
			"    </tr>"+
			"    <tr>"+
			"      <td align='right' style='padding-right:15px;'>"+
			"        <a target='_blank' title='Dropifi' href='http://www.dropifi.com'>"+
			"        <img width='100' height='50' border='0' alt='' src='"+filehost+"/images/dropifi_logo_home.png'>"+
			"        </a>"+
			"      </td>"+
			"		    </tr>"+
			"		    <tr>"+
			"		      <td>"+
			"		        <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table>"+
			"		      </td>"+
			"		    </tr>"+
			"		  </tbody></table>"+
			  
			"		  <table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#333333' style='font-family:Arial;border:solid 1px #ffffff;border-radius:0px;border:none;background:#333'><tbody><tr><td width='98%' valign='top' align='left'>"+
			"		    <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		      <tbody><tr>"+
			"		        <td>"+
			"		          <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:1px;font-size:10px;line-height:1px'>&nbsp;</div></td></tr></tbody></table>"+
			"		        </td>"+
			"		      </tr>"+
			"		    </tbody></table>"+
			"		  </td></tr></tbody></table>"+
			"		  <table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#FFFFFF' style='font-family:Arial;border:solid 1px #ffffff;border-radius:0px;border:none'><tbody><tr><td width='98%' valign='top' align=''>"+
			"		    <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		      <tbody><tr><td height='20' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:20px;font-size:20px;line-height:20px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='60' height='60'>"+
			"		            <a target='_blank' href='"+viewContUrl+"'><img width='60' height='60' border='0' src='"+customerImg+"'></a>"+
			"		        </td>"+
			"		        <td width='10'><table width='10' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%'>"+
			"		          <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		            <tbody><tr>"+
			"		              <td width='100%'><a target='_blank' style='text-decoration:none' href='"+viewContUrl+"'><span style='color:#333;font-size:18px;font-family:Helvetica,Arial,sans-serif;font-weight:bold;text-transform: capitalize;'>"+param.contactName+customerTitle+"</a></td>"+
			"		            </tr>";
			for(String demo : demog){ forwarded +=demo; }
			forwarded+=  "		          </tbody></table>"+
			"		        </td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			      
			"		      <tr><td height='10' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='font-size: 20px; min-height: 10px; line-height: 10px;'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' colspan='3'><div style='min-height:1px;font-size:1px;line-height:1px;border-top:1px solid #e1e1e1'>&nbsp;</div></td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr>"+
			"		      	<td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      	<td width='100%' colspan='3'><table width='100%' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td align='right'><div style='min-height:20px;line-height:20px;font-size:12px;padding-top:5px;padding-bottom:10px;'><a href='"+viewMsgUrl+"' target='_blank'><img width='20px' height='20px' src='"+sentiUrl+"' alt='"+
							(param.sentiment!=null && !param.sentiment.trim().isEmpty() && param.sentiment!="NoSentiment"?param.sentiment:" ")+"' ><span style='float:right; color:"+sentiColor+";text-transform: capitalize;padding-left:3px;'>"+(param.sentiment!=null && !param.sentiment.trim().isEmpty() && param.sentiment!="NoSentiment"?param.sentiment+" Sentiment":"")+"</span></a></div></td></tr></tbody></table></td>"+
			"		      </tr>  " +
			
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' style='font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#666' colspan='3'> "+
								param.message +
			"		        </td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr><td height='25' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:25px;font-size:25px;line-height:25px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' colspan='3'>"+
			"		          <table cellspacing='0' cellpadding='0' border='0' align=''><tbody><tr><td valign='middle' height='30' bgcolor='#5AA9D3' background='' align='center' style='border-radius: 3px; background-color: rgb(90, 169, 211); border: 1px solid rgb(90, 169, 211);'><div style='padding-right:13px;padding-left:13px'><a target='_blank' style='text-decoration:none' href='"+viewMsgUrl+"'><span style='font-size:13px;font-family:Arial;font-weight:bold;color:#ffffff;white-space:nowrap;display:block'>"+
			"		            Reply to <span class='il' style='text-transform: capitalize;'>"+param.contactName+"</span>"+
			"		          </span></a></div></td></tr></tbody></table>"+
			"		        </td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr><td height='15'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:15px;font-size:15px;line-height:15px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		      <tr>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		        <td width='100%' style='font-size: 12px; color: rgb(153, 153, 153);' colspan='3'>"+
			"		          <span style='background-color:#f1f1f1;font-size:12px;color:#666'>Sent from</span><a style='font-size:12px;font-family:Arial,sans-serif;color:#999999;padding-left:5px;' href='"+param.pageUrl+"' target='_blank'>"+param.pageUrl+"</a>" +
			"				</td>"+
			"		        <td width='20'><table width='20' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:0px;font-size:0px;line-height:0px'>&nbsp;</div></td></tr></tbody></table></td>"+
			"		      </tr>"+
			"		      <tr><td height='20' colspan='5'><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:20px;font-size:20px;line-height:20px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		    </tbody></table>"+
			"		  </td></tr></tbody></table><div class='im'>"+
			"		  <table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#FFFFFF' style='font-family:Arial;border:solid 1px #ffffff;border-radius:0px;background-color:#dfdfdf;border:none'><tbody><tr><td width='98%' valign='top' align=''>"+
			"		    <table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		      <tbody><tr>"+
			"		        <td>"+
	 
			"		<table width='100%' cellspacing='0' cellpadding='0' border='0' style='font-family:Arial'>"+
			"		  <tbody><tr><td><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		  <tr>"+
			"		    <td align='center' style='font-size:11px;font-family:Arial,sans-serif;color:#999999'>             You are receiving Dropifi message emails. <a target='_blank' href='"+DropifiTools.ServerURL+"/login?url=/admin/widgets/index' style='color:#006699;text-decoration:underline'>Unsubscribe</a>."+
			"		        <table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table><a style='color:#006699;text-decoration:underline; text-align: center;' href='http://www.dropifi.com' target='_blank'>Dropifi Limited</a></td>"+
			"		  </tr>"+
			"		  <tr><td><table width='1' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td><div style='min-height:10px;font-size:10px;line-height:10px'>&nbsp;</div></td></tr></tbody></table></td></tr>"+
			"		</tbody></table>"+
	
			"		        </td>"+
			"		      </tr>"+
			"		    </tbody></table>"+
			"		  </td></tr></tbody></table>"+
			"		</div></td></tr></tbody></table></td></tr></tbody></table>"+ 
			
			"		</div>"+ 
			"		</body></html>";
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
 		return forwarded;
	}
	
	public static String getSignUpMsgOLD(String username){
		 
		String msg =  
		
		"<div><p>Hi "+username+",</p>"+
		"<p>You just created a brand new account on the most amazing service for online customer engagement.<br/>"+
		"We hope you enjoy pleasing your customers the best way!</p>"+
		"<p>You can log into your account at http://dropifi.com/login , customize your widget and place the code in the header tag of your website."+ 
		"You are then ready to turn your contacts into loyal customers!</p>"+
		"<p>Need assistance? Be sure to reference our website <a href='http://www.dropifi.com'>http://www.dropifi.com</a> " +
		"or drop us an email to <a href='mailto:team@dropifi.com'>team@dropifi.com</a>  for all the assistance you need.</p>"+ 
		"<p>You can follow us on the social space for interesting bits and stay up to date with us.</p>"+
		"<p><a href='http://www.facebook.com/dropifi'>Facebook</a>|" +
		"<a href='http://www.twitter.com/dropifi'>Twitter</a>|" +
		"<a href='http://www.linkedin.com/company/2530969?trk=NUS_CMPY_TWIT'>LinkedIn</a>|" +
		"<a href='https://plus.google.com/b/116175324270114664152/116175324270114664152/posts/p/pub'>Google Plus</a></p>" + 
		"<p>Regards,</p><p>Dropifi Team.</p></div>";   		

		return msg; 
	}
	
	public static String getSignUpMsg(String username){
		 
		String msg ="<!DOCTYPE html >"+
				"<html>"+
				"<head>"+
				"<meta  charset=UTF-8' >"+
				"<title>Welcome to Dropifi</title></head><body><div>"+ 		
				"<div><p>Hi "+username+",</p>"+
				"<p>Congratulations on signing up with Dropifi - our award winning online customer engagement tool, designed to support your company's outreach with potential clients.</p>"+
				"<p>You can access your account at <a href='http://www.dropifi.com/login'>http://www.dropifi.com/login</a></p>"+ 
				"<p>Please follow our easy step-by-step instructions after you login to quickly turn your website into another sales opportunity!</p>"+		 
				"<p>Please email us at <a href='mailto:team@dropifi.com'>team@dropifi.com</a> with any questions, and we'll get back to you! </p>" +
				"<p>Thanks again for joining Dropifi!</p>" +
				"<p>Sincerely,<br/>"+
				"Kamil Nabong<br/>"+
				"Business Dev't/Co-founder<br/>"+ 
				"Dropifi Limited</p>"+ 
				"</div></body></html>";   			

		return msg;
	}
	
	public static String getShopifySignUpMsgOLD(String username){
		 
		String msg = "<!DOCTYPE html >"+
				"<html>"+
				"<head>"+
				"<meta  charset=UTF-8' >"+
				"<title>Welcome to Dropifi</title></head><body><div>"+ 		
				"<p>Hi "+username+", </p>"+
				"<p>You just created a brand new account on the most amazing service for online customer engagement.<br/>"+
				"We hope you enjoy pleasing your customers the best way!</p>"+
				"<p>Need assistance? Be sure to reference our website <a href='http://www.dropifi.com'>http://www.dropifi.com</a> " +
				"or drop us an email to <a href='mailto:team@dropifi.com'>team@dropifi.com</a> for all the assistance you need.</p>"+ 
				"<p>You can follow us on the social space for interesting bits and stay up to date with us.</p>"+
				"<p><a href='http://www.facebook.com/dropifi'>Facebook</a>|" +
				"<a href='http://www.twitter.com/dropifi'>Twitter</a>|" +
				"<a href='http://www.linkedin.com/company/2530969?trk=NUS_CMPY_TWIT'>LinkedIn</a>|" +
				"<a href='https://plus.google.com/b/116175324270114664152/116175324270114664152/posts/p/pub'>Google Plus</a></p>" + 
				"<p>Sincerely,<br/>"+
				"Kamil Nabong<br/>"+
				"Business Dev't/Co-founder<br/>"+ 
				"Dropifi Limited</p>"+ 
				"</div></body></html>";   		

		return msg; 
	}
	
	public static String getShopifySignUpMsg(String username){
		 
		String msg = "<!DOCTYPE html >"+
				"<html>"+
				"<head>"+
				"<meta  charset=UTF-8' >"+
				"<title>Welcome to Dropifi</title></head><body><div>"+ 				
				"<p>Hi "+username+",</p>"+
				"<p>Congratulations on signing up with Dropifi - our award winning online customer engagement tool, designed to support your company's outreach with potential clients.</p>"+
				"<p>You can access your account at <a href='http://www.dropifi.com/login'>http://www.dropifi.com/login</a></p>"+ 
				"<p>Please follow our easy step-by-step instructions after you login to quickly turn your website into another sales opportunity!</p>"+		 
				"<p>Please email us at <a href='mailto:team@dropifi.com'>team@dropifi.com</a> with any questions, and we'll get back to you! </p>" +
				"<p>Thanks again for joining Dropifi!</p>" +
				"<p>Sincerely,<br/>"+
				"Kamil Nabong<br/>"+
				"Business Dev't/Co-founder<br/>"+ 
				"Dropifi Limited</p>"+ 
				"</div></body></html>"; 		

		return msg;
	}
	
	public static String getWelcomeMessage(String username,String domain){
		String msg ="<!DOCTYPE html >"+
				"<html>"+
				"<head>"+
				"<meta  charset=UTF-8' >"+
				"<title>Let's get you off to a good start</title></head><body><div>"+ 
				"<p>Hi "+username+",</p>"+
				"<p>It's time to simplify your customer support, make business decisions with more insight and wow your customers. " +
				"Let’s get you off to a great start!</p>" + 
				"<p><ol>"+
				"<li>Check your site to make sure the widget is displayed.</li>"+// If not, copy and place the <a href='/"+domain+"/admin/widgets'>widget code</a> immediately after the &#60;head&#62; tag of your website HTML.</li>"+
				"<li>Go-to \"Mail Management\" under the \"Admin\" panel to customize the look and feel of your widget</li>"+
				"<li>Also remember to configure your business rules and add customer support staff. Dropifi business rules define how incoming messages are handled.</li></ol></p>"+
				"<p>If at any point you hit a stumbling block, just reference our website (<a href='https://www.dropifi.com'>Dropifi.com</a>) or drop us an email to <a href='mailto:support@dropifi.com'>support@dropifi.com</a> .<br/>"+
				"We're excited to help you more effectively interact with and understand your customers.<br/>"+
				"We'll also be happy to hear your feedback.</p>"+
				"<p>Sincerely,<br/>"+
				"Kamil Nabong<br/>"+
				"Business Dev't/Co-founder<br/>"+ 
				"Dropifi Limited</p>"+ 
				"</div></body></html>"; 
		
		return msg; 
	}
	
	public static String getEcomBlogWelcomeMessage(String username){
		String msg = "<!DOCTYPE html >"+
				"<html>"+
				"<head>"+
				"<meta charset=UTF-8' >"+
				"<title>Let's get you off to a good start</title></head><body><div>"+ 
				"<p>Hi "+username+",</p>"+
				"<p>It's time to simplify your customer support, make business decisions with more insight and wow your customers. " +
				"Let's get you off to a great start!</p>" +
				"<p><ol>"+
				"<li>Check your site to make sure the widget is displayed.</li>"+
				"<li>Go-to \"Mail Management\" under the \"Admin\" panel to customize the look and feel of your widget</li>"+
				"<li>Also remember to configure your business rules and add customer support staff. Dropifi business rules define how incoming messages are handled.</li></ol></p>"+
				"<p>If at any point you hit a stumbling block, just reference our website (<a href='https://www.dropifi.com'>Dropifi.com</a>) or drop us an email to <a href='mailto:support@dropifi.com'>support@dropifi.com</a>.<br/>"+
				"We're excited to help you more effectively interact with and understand your customers.<br/>"+
				"We'll also be happy to hear your feedback.</p>"+
				"<p>Sincerely,<br/>"+
				"Kamil Nabong<br/>"+
				"Business Dev't/Co-founder<br/>"+ 
				"Dropifi Limited</p>"+
				"</div></body></html>";
		
		return msg;
	}
	
	public static String getPaymentReceipt(String invoiceNumber,String name,String email,String amount,String plan,Date date,Card card,String reason){
		
		String content ="<p>Dear "+name+",</p>"+
		"<p>Your subscription has been successfully activated without any hitch. Below is your invoice</p>"+
		"<p><strong>Dropifi User Email:</strong>&nbsp;"+email+"<br>" +
		"<strong>Date:</strong>&nbsp;"+date.toString()+"<br>"+
        	"<strong>Invoice Number:</strong>&nbsp;"+invoiceNumber+"<br>"+
		  "<strong>Credit Card Type:</strong>&nbsp;"+card.getType()+"<br>"+
		  "<strong>Last 4 Digits of Credit Card:</strong>&nbsp;"+card.getLast4()+"<br>"+
		  "<strong>Credit Card Expiration Date:</strong>&nbsp;"+card.getExpMonth()+"/"+card.getExpYear()+"<br>"+
		  "<strong>Amount Due:</strong>&nbsp;"+amount+"</p>"+
		"<table width='100%' cellspacing='0px' cellpadding='0px' border='1'>"+
		  "<tbody><tr>"+
		    "<th scope='col'>Subscription Plan</th>"+
		    "<th scope='col'>Purchase Amount</th>"+
		    "</tr>"+
		  "<tr>"+
		    "<td><p align='center'>Dropifi "+newPlanName(plan)+"</p></td>"+
		    "<td><p align='center'>"+amount+"</p></td>"+
		    "</tr>"+
		  "</tbody></table>"+
		"<p><strong>About this invoice/receipt</strong></p>"+
		"<ul>"+
		  "<li>You won't receive a paper invoice/receipt; print this email if you need a physical copy.</li>"+
		  "<li>Dropifi is a non-EU provder with no VAT registration. VAT was not applied to this invoice.</li>"+
		  "<li>If a refund is required, it will be issued in the purchase currency for the amount of the original charge.</li>"+
		  "<li>Have any questions about this invoice/receipt? <a target='_blank' href='http://www.dropifi.com'>Contact us</a>.</li>"+
		  "</ul>"+
		"<p>&nbsp;</p>";
		
		 
		return getSubMailTemplate("DROPIFI INVOICE/RECEIPT", new Date(), content,DropifiTools.ServerURL+"/login?args="+email,"Login to Dropifi Now!");
	}
	
	public static String getPaymentSummary(String name,String email,String amount,String plan,Date date,Card card,String reason){
		
		String content =
				"<p><strong>Dropifi User Email:</strong>&nbsp;"+email+"<br>" +
				  "<strong>Date:</strong>&nbsp;"+date.toString()+"<br>"+
		          //"<strong>Invoice Number:</strong>&nbsp;"+invoiceNumber+"<br>"+
				  "<strong>Credit Card Type:</strong>&nbsp;"+card.getType()+"<br>"+
				  "<strong>Last 4 Digits of Credit Card:</strong>&nbsp;"+card.getLast4()+"<br>"+
				  //"<strong>Credit Card Expiration Date:</strong>&nbsp;"+card.getExpMonth()+"/"+card.getExpYear()+"<br>"+
				  "<strong>Amount Due:</strong>&nbsp;$"+amount+"</p>"+
				"<table width='100%' cellspacing='0px' cellpadding='0px' border='1'>"+
				  "<tbody><tr>"+
				    "<th scope='col'>Subscription Plan</th>"+
				    "<th scope='col'>Purchase Amount</th>"+
				    "</tr>"+
				  "<tr>"+
				    "<td><p align='center'>Dropifi "+newPlanName(plan)+"</p></td>"+
				    "<td><p align='center'>"+amount+"</p></td>"+
				    "</tr>"+
				  "</tbody></table>";
		return content;
	}
	
	public static String getCardFailureNotification(String name,String email,String amount,String plan,Date date,Card card,String reason){
		String content = "<p>Dear "+name+",</p>"+
					"<p>You are receiving this email because  you have subscribed for Dropifi’s premium customer engagement service, and the  credit card we have on file did not accept the monthly subscription charge.</p>"+
					"<p>Here is the information we have on  file for your account:</p>"+
					"<p><strong>Dropifi User Email:</strong>&nbsp;"+email+"<br>"+
					  "<strong>Subscription Plan:</strong>&nbsp; Dropifi "+newPlanName(plan)+"<br>"+
					  "<strong>Credit Card Type:</strong>&nbsp;"+card.getType()+"<br>"+
					  "<strong>Last 4 Digits of Credit Card:</strong>&nbsp;"+card.getLast4()+"<br>"+
					  "<strong>Credit Card Expiration Date:</strong>&nbsp;"+card.getExpMonth()+"/"+card.getExpYear()+"<br>"+
					  "<strong>Amount Due:</strong>&nbsp;"+amount+"</p>"+
					"<p>Please&nbsp;log  in to your account today&nbsp;to update your credit  card information. If this is your first credit card failure notice, you have a grace period of uninterrupted use of your account while you fix the billing issue. If this is your second or third failure in a row, you'll have to update your  credit card information to regain full account functionality.</p>"+
					"<p><a href='"+DropifiTools.ServerURL+"/login?url=/admin/pricing/index&args="+email+"' target='_blank'>Click this link to update your credit card information</a></p>"+
					"<p>Whatever the reason for your credit card declining, we want to get you back online! </p>"+
					"<p>We appreciate your prompt attention to this. Be assured that we are working hard to make your use of Dropifi as  enjoyable and productive as possible. As always, thanks you for your business.</p>"+
					"<p>Best Regards,</p>"+
					"<p>Dropifi Billing</p>";
		
		return getSubMailTemplate("CREDIT CARD FAILURE NOTIFICATION", new Date(), content,DropifiTools.ServerURL+"/login?url=/admin/pricing/index&args="+email,"Login to Dropifi Now!");
	}
	
	public static String getDowngradeNotification(String name,String email,String amount,String plan,Date date,Card card,String reason){
		String content = "<p>Hi "+name+",</p>"+ 
						 "<p> All attempts to charge your credit card we have on file have failed and your account has been downgraded.</p>"+
				
						"<p>Meanwhile, you can still engage your customers seamlessly; you'll however have limitations on personalization, team collaboration and automation features and business insight.</p>"+
				
						"<p>But don’t worry! We've gone ahead and saved all your activity for you. Once your subscription is re-activated, the complete functionality will be restored.</p>"+ 
						"<a href='"+DropifiTools.ServerURL+"/login?url=/admin/pricing/index?args="+email+"' target='_blank'>Reactivate Subscription Now!</a>"+ 
						"<p>If you have any questions or need a hand setting up things, just reply to this email and our team of experts will help you out.</p>"+
						"<p>Here is the information we have on  file for your account:</p>"+
						"<p><strong>Dropifi User Email:</strong>&nbsp;"+email+"<br>"+
						  "<strong>Subscription Plan:</strong>&nbsp; Dropifi "+newPlanName(plan)+"<br>"+
						  "<strong>Credit Card Type:</strong>&nbsp;"+card.getType()+"<br>"+
						  "<strong>Last 4 Digits of Credit Card:</strong>&nbsp;"+card.getLast4()+"<br>"+
						  "<strong>Credit Card Expiration Date:</strong>&nbsp;"+card.getExpMonth()+"/"+card.getExpYear()+"<br>"+
						  "<strong>Amount Due:</strong>&nbsp;"+amount+"</p>"+
						"<p>Thanks!</p>"+ 
						"<p>Dropifi Billing</p>";
 
		return getSubMailTemplate("DROPIFI PREMIUM ACCOUNT DOWNGRADE NOTIFICATION", new Date(), content,DropifiTools.ServerURL+"/login?url=/admin/pricing/index&args="+email,"Login to Dropifi Now!");
	}
	
	public static String getAddonDowngradeNotification(String name,String email,String domain,String addonTitle,String platformKey,String msg){
		
		String url = (DropifiTools.ServerURL+"/login?url=/addons/subscription?title="+addonTitle+"&platformKey="+platformKey+"&msg="+msg+"&domain="+domain),
		content = "<p>Hi "+name+",</p>"+ 
						 "<p>Your free trial of Dropifi "+addonTitle+" has expired and your account is deactiavted. That means it's time to add your Debit/Credit information!</p>"+
				 
						"<p>But don’t worry! We've gone ahead and saved all your activity for you. Once your subscription is re-activated, the complete functionality will be restored.</p>"+ 
						"<a href='"+url+"' target='_blank'>Reactivate "+addonTitle+" Now!</a>"+ 
						"<p>If you have any questions or need a hand setting up things, just reply to this email and our team of experts will help you out.</p>"+
						/*"<p>Here is the information we have on  file for your account:</p>"+
						"<p><strong>Dropifi User Email:</strong>&nbsp;"+email+"<br>"+
						  "<strong>Subscription Plan:</strong>&nbsp; Dropifi "+newPlanName(plan)+"<br>"+
						  "<strong>Credit Card Type:</strong>&nbsp;"+card.getType()+"<br>"+
						  "<strong>Last 4 Digits of Credit Card:</strong>&nbsp;"+card.getLast4()+"<br>"+
						  "<strong>Credit Card Expiration Date:</strong>&nbsp;"+card.getExpMonth()+"/"+card.getExpYear()+"<br>"+
						  "<strong>Amount Due:</strong>&nbsp;"+amount+
						"</p>"+*/
						"<p>Thanks!</p>"+ 
						"<p>Dropifi Billing</p>";
 
		return getSubMailTemplate("DROPIFI "+addonTitle.toUpperCase()+" DOWNGRADE NOTIFICATION", new Date(), content,url,"Login to Dropifi Now!");
	}
	
	public static String getAddonDowngradeNotificationB(String name,String email,String domain,String addonTitle,String platformKey,String msg){
		if(addonTitle==null)addonTitle="";
		String url = (DropifiTools.ServerURL+"/login?url=/addons/subscription?title="+addonTitle+"&platformKey="+platformKey+"&msg="+msg+"&domain="+domain),
		content = "<p>Hi "+name+",</p>"+ 
						 "<p>Your free trial of Dropifi "+addonTitle+" has end. Did you get a chance to put it to good use?</p>"+
				 
						"<p>If you'd like to keep going, great! You can go ahead and <a href='"+url+"' target='_blank'>upgrade now</a>. If you don't plan to renew, we'd love to know what we could be doing better.</p>"+ 
						"<p>Yes, I like to keep using "+addonTitle+". <a href='"+url+"' target='_blank'>Upgrade Now!</a></p>"+ 
						"<p>Need an extra week or so? Just reply now and ask!</p>"+
						"<p>As always, if you've got any feedback for us, or any questions at all, please get in touch by simply replying to this email.</p>"+
						"<p>Thanks!</p>"+ 
						"<p>Dropifi Billing</p>";
 
		return getSubMailTemplate("DROPIFI "+addonTitle.toUpperCase()+" DOWNGRADE NOTIFICATION", new Date(), content,url,"Login to Dropifi Now!");
	}
	
	public static String getBetaDownGradeNotification(String name,String email,String plan){
		String content= "<p>Hi "+name+",</p><p>"+
						"</p><p>This is just a quick heads-up that Dropifi's new pricing tiers have gone into effect! Please note that, from today on, only premium tiers will include the following features:</p>"+
						"<ol>"+
						"	<li>Additional agents</li>"+
						"	<li>Advanced reporting and analytics</li>"+
						"	<li>File Attachments</li>"+
						"	<li>Custom Branding</li>"+
						"	<li>Custom Auto-responders</li>"+
						"</ol>"+
						"<p>As one of our valued beta users, we're offering you a 30% discount on all price tiers. Use this code to redeem your discount: <span style='font-size: 16px; font-weight: bold;'>DROPIFIBETA30</span></p>"+
						"<a style='font-size: 16px; font-weight: bold;' href='"+DropifiTools.ServerURL+"/login?url=/admin/pricing/index&args="+email+"' target='_blank'>Get 30% Off Now</a>"+
						"<p>Thanks for being a valued Dropifi user</p>"+
						"<p>Dropifi Team</p>"; 
		return getSubMailTemplate("",new Date(),content,DropifiTools.ServerURL+"/login?url=/admin/pricing/index&args="+email,"Upgrade to Dropifi Premium Now!");
	}

	public static String getFreePlanNotification(String name, String email,String plan){
		String content ="<p>Hi "+name+",</p>"+ 
					    "<p>You have downgraded your Dropifi account to the Free forever version. </p>"+  
						"<p>Meanwhile, you can still engage your customers seamlessly; you’ll however have limitations on personalization, team collaboration and automation features and business insight.</p>"+  
						"<p>But don't worry! We've gone ahead and saved all your activity for you. Once your subscription is re-activated, the complete functionality will be restored.</p>"+  
						"<a href='"+DropifiTools.ServerURL+"/login?url=/admin/pricing/index&args="+email+"' target='_blank'>Reactivate Subscription Now!</a>"+  
						"<p>If you have any questions or need a hand setting up things, just reply to this email and our team of experts will help you out.</p>"+  
						"<p>Thanks!</p>"+  
						"<p>Dropifi Billing</p>";
 
		return getSubMailTemplate("", new Date(), content,DropifiTools.ServerURL+"/login?url=/admin/pricing/index&args="+email,"Login to Dropifi Now!");
	}
	
	private static String newPlanName(String plan){
		if(plan.equals("Lite"))
			plan = "Business";
		else if(plan.equals("Business"))
			plan="Pro";
		
		return plan;
	}
	
	public static String getCancelNotification(String name, String email){
		String content= "<p>Hi "+name+",</p>"+
				"<p>This is a confirmation that your Dropifi account has been cancelled and all your information deleted.</p>"+
				"<p>We're very interested in your thoughts on why you chose to cancel. If you have not already given us feedback, help us to improve by taking this survey.</p>"+
				"<p><a href='http://www.surveymonkey.com/s/DB2GLD6'>Click here to take survey</a>. Thanks for giving us your candid feedback!. </p>"+
				"<p>We hope to hear from you.</p>"+ 
				"<p>Team Dropifi</p>";

		return getSubMailTemplate("DROPIFI ACCOUNT CANCELED", new Date(),content,DropifiTools.ServerURL, "Login to Dropifi Now!");
	}
	
 	private static String getSubMailTemplate(String title, Date date, String content,String loginUrl,String loginName){
		String msg ="<!Doctype html> <html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'></head><body>"+
				"<div>"+
					"<div>"+
						"<div align='center'>"+ 

						"<table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#ffffff' style='background-color:#ffffff'>"+
						"<tbody><tr>"+
						"<td valign='top' align='center' colspan='1' rowspan='1' style='padding:14px 14px 14px 14px'>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:transparent'>"+
						"<tbody><tr>"+
						"<td width='100%' valign='top' align='center' colspan='1' rowspan='1'>"+
						"<table width='1' cellspacing='0' cellpadding='0' border='0' style='width:640px'>"+
						"<tbody><tr>"+

						"<td width='100%' valign='top' align='center' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px'>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td height='1' align='center' colspan='1' rowspan='1' style='padding-bottom:14px;height:1px;line-height:1px'><img width='5' vspace='0' hspace='0' height='1' border='0' src='' alt='' style='display:block'></td>"+
						"</tr>"+
						"</tbody></table>"+

						"</td>"+

						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+

						"<table width='1' cellspacing='0' cellpadding='0' border='0' style='width:640px'>"+
						"<tbody><tr>"+
						"<td valign='top' bgcolor='#dfdfdf' align='center' colspan='1' rowspan='1' style='background-color:#dfdfdf;padding:1px 1px 1px 1px'>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#ffffff' style='background-color:#ffffff'>"+
						"<tbody><tr>"+
						"<td valign='top' align='center' colspan='1' rowspan='1'>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+ 

						"<td width='100%' valign='top' align='center' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px'>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#FFFFFF' style='background-color:#ffffff'><tbody><tr><td valign='center' align='left' colspan='1' rowspan='1' style='color:#76b5be;font-family:Arial,Helvetica,sans-serif;font-size:10pt;padding:32px 32px 32px 32px'>"+
						"<div><a target='_blank' shape='rect' href='#'><img width='106' height='60' border='0' align='left' src='"+filehost+"/images/dropifi_blue.png' alt='Dropifi Logo'  style='display:block'></a></div>"+
						"</td><td width='100%' valign='center' align='right' colspan='1' rowspan='1' style='color:#76b5be;font-family:Arial,Helvetica,sans-serif;font-size:10pt;padding:32px 32px 32px 0px'>"+
						"<div><b><i>"+date.toString()+"</i></b></div>"+
						"</td></tr></tbody></table>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+

						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+

						"</td>"+

						"</tr>"+
						"</tbody></table>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+

						"<td width='100%' valign='top' align='center' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px'>"+

						"<a ></a><table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#5AA9D3' style='background-color:#5aa9d3'><tbody>"+
						"<tr><td valign='top' align='left' colspan='1' rowspan='1' style='color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:10pt;padding:8px 32px 18px 32px'>&nbsp;</td></tr></tbody></table>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td height='1' bgcolor='#d3ddde' align='center' colspan='1' rowspan='1' style='padding-bottom:0px;background-color:#d3ddde;height:1px;line-height:1px'><img width='5' vspace='0' hspace='0' height='1' border='0' src='' alt='' style='display:block'></td>"+
						"</tr>"+
						"</tbody></table></td></tr></tbody></table></td></tr></tbody></table><br>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+

						"<td width='100%' valign='top' align='center' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px'>"+
						"<a ></a><table width='100%' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td valign='top' align='left' colspan='1' rowspan='1' style='padding:8px 32px 9px;font-size:10pt;font-family:Arial,Helvetica,sans-serif;color:rgb(69,69,69)'>"+
						"<div><br>"+
						  "<div style='color:#ee5624;font-family:Arial,Helvetica,sans-serif;font-size:14pt'><b>"+title+"</b></div>"+
						
							content+
						
						"</div>"+ 
						
						"<br></td></tr></tbody></table>"+ 
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' align='left' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' height='1' bgcolor='#d3ddde' align='center' colspan='1' rowspan='1' style='background-color:#d3ddde;padding-bottom:0px;height:1px;line-height:1px'><img width='5' vspace='0' hspace='0' height='1' border='0' src='' alt='' style='display:block'></td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+
						"<a ></a><table width='100%' cellspacing='0' cellpadding='0' border='0'><tbody><tr><td valign='top' align='left' colspan='1' rowspan='1' style='color:#454545;font-family:Arial,Helvetica,sans-serif;font-size:10pt;padding:8px 32px 9px 32px'>&nbsp;</td></tr></tbody></table><table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' align='left' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' height='1' bgcolor='#d3ddde' align='center' colspan='1' rowspan='1' style='background-color:#d3ddde;padding-bottom:0px;height:1px;line-height:1px'><img width='5' vspace='0' hspace='0' height='1' border='0' src='' alt='' style='display:block'></td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table><a ></a>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' align='left' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' height='1' bgcolor='#d3ddde' align='center' colspan='1' rowspan='1' style='background-color:#d3ddde;padding-bottom:0px;height:1px;line-height:1px'>&nbsp;</td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table><table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' align='left' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' height='1' bgcolor='#d3ddde' align='center' colspan='1' rowspan='1' style='background-color:#d3ddde;padding-bottom:0px;height:1px;line-height:1px'><img width='5' vspace='0' hspace='0' height='1' border='0' src='' alt='' style='display:block'></td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table><table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' align='left' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' height='1' bgcolor='#d3ddde' align='center' colspan='1' rowspan='1' style='background-color:#d3ddde;padding-bottom:0px;height:1px;line-height:1px'><img width='5' vspace='0' hspace='0' height='1' border='0' src='' alt='' style='display:block'></td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table></td>"+ 
						
						"</tr>"+
						"</tbody></table>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+

						"<td width='100%' valign='middle' align='center' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px'>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#292B2F' style='background-color:#292b2f'><tbody><tr><td width='100%' valign='top' align='left' colspan='1' rowspan='1' style='color:#ffffff;font-family:Arial,Helvetica,sans-serif;font-size:10pt;padding:25px 25px 25px 50px'>"+
						"<div><br><span style='font-size:10pt'>Dropifi turns your site visitors into loyal customers.</span><br> <br></div>"+
						"</td><td valign='center' align='left' colspan='1' rowspan='1' style='color:#ffffff;font-family:Arial,Helvetica,sans-serif;font-size:10pt;padding:25px 50px 25px 25px'>"+
						"<div>"+
						"<table width='1' cellspacing='0' cellpadding='0' border='0' align='right'><tbody><tr><td valign='top' align='left' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;color:#454545;font-family:Arial,Helvetica,sans-serif;font-size:10pt'>"+
						"<table width='1' cellspacing='0' cellpadding='0' border='0' style='width:220px;margin:0px 0px 0px 0px'><tbody><tr><td valign='top' height='1' align='left' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px;height:1px;line-height:1px'>"+
						"<div><img width='1' vspace='0' hspace='0' height='5' border='0' src=''></div>"+
						"</td></tr></tbody></table>"+
						"</td></tr><tr><td valign='top' bgcolor='#ee5624' align='center' colspan='1' rowspan='1' style='background-color:#ee5624;border:1px solid #ffffff;border-color:#ffffff;padding:10px 10px 10px 10px;color:#ffffff;font-family:Arial,Helvetica,sans-serif;font-size:12pt'>"+
						"<div><b><i><a target='_blank' shape='rect' href='"+loginUrl+"' style='color:rgb(255,255,255);text-decoration:underline' >"+loginName+"</a></i></b></div>"+
						"</td></tr></tbody></table>"+
						"</div>"+
						"</td></tr></tbody></table>"+
						"<table width='100%' cellspacing='0' cellpadding='0' border='0' bgcolor='#f3f2f4' style='background-color:#f3f2f4;display:table'><tbody><tr><td valign='top' align='center' colspan='1' rowspan='1' style='color:#625467;font-family:Arial,Helvetica,sans-serif;font-size:8pt;padding:18px 32px 14px 32px'>"+
						"<div><b><i>STAY CONNECTED TO DROPIFI</i></b><br> <br> <a target='_blank' href='http://www.facebook.com/dropifi' shape='rect'><img width='22' height='22' border='0' title='Like us on Facebook' src='"+filehost+"/images/social/facebook.png' alt='Like us on Facebook'></a> &nbsp;&nbsp;<a target='_blank' href='http://www.twitter.com/dropifi' shape='rect'><img width='22' height='22' border='0' title='Follow us on Twitter' src='"+filehost+"/images/social/twitter.png' alt='Follow us on Twitter'></a> &nbsp;&nbsp;<a target='_blank' href='http://www.linkedin.com/company/2530969?trk=NUS_CMPY_TWIT' shape='rect'><img width='22' height='22' border='0' title='View our profile on LinkedIn' src='"+filehost+"/images/social/linkedin.png' alt='View our profile on LinkedIn'></a>&nbsp; &nbsp;&nbsp;<br></div>"+					
						"</td></tr></tbody></table>"+

						"</td>"+

						"</tr>"+
						"</tbody></table>"+

						"</td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:transparent'>"+
						"<tbody><tr>"+
						"<td width='100%' valign='top' align='center' colspan='1' rowspan='1'>"+
						"<table width='1' cellspacing='0' cellpadding='0' border='0' style='width:640px'>"+
						"<tbody><tr>"+

						"<td width='100%' valign='top' align='center' colspan='1' rowspan='1' style='padding:0px 0px 0px 0px'>"+

						"<table width='100%' cellspacing='0' cellpadding='0' border='0'>"+
						"<tbody><tr>"+
						"<td valign='top' align='center' colspan='1' rowspan='1' style='padding:0px 8px 0px 8px;color:#777777;font-family:Arial,Helvetica,sans-serif;font-size:8pt'><div></div>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+
						"</td>"+
						"</tr>"+
						"</tbody></table>"+
						"</div>"+
						"<div align='center' style='background-color:#ffffff'>"+
							"<table cellspacing='0' cellpadding='0' border='0' style='text-align:left'>"+
								"<tbody>"+
									"<tr>"+
										"<td align='center' colspan='1' rowspan='1'>"+
											"<div>"+
											"<table width='619' cellspacing='0' cellpadding='0' border='0' style='font-family:tahoma,sans-serif;font-size:11px;color:#2f2f2f;background-color:#ffffff'>"+
											"<tbody><tr>"+
											"<td colspan='1' rowspan='1'><font size='1' face='tahoma,sans-serif' color='#000000' style='font-family:tahoma,sans-serif;font-size:11px;color:#2f2f2f'>"+
											"<div></div>"+
											"</font></td>"+
											"</tr>"+
											"</tbody></table>"+
											"<div align='left' style='font-family:tahoma,sans-serif;font-size:12px;background-color:rgb(255,255,255);padding-top:12px'><font size='1' face='tahoma,sans-serif' color='#000000' style='font-family:tahoma,sans-serif;font-size:12px'>Dropifi LLC</font><font size='1' face='tahoma,sans-serif' color='#000000' style='font-family:tahoma,sans-serif;font-size:12px'><span style='color:rgb(186,186,186)'>| </span>444 Castro St. #1200<span style='color:rgb(186,186,186)'>&nbsp;| </span>Mountain View<span style='color:rgb(186,186,186)'>&nbsp;| </span>California<span style='color:rgb(186,186,186)'>| </span>94041<span style='color:rgb(186,186,186)'>| </span>United States</font></div>"+
											"</div>"+
										"</td>"+
									"</tr>"+
								"</tbody>"+
							"</table>"+
						"</div>"+
					"</div>"+
				"</div>"+
			"</body></html>";
		return msg;
	}

}
