package resources;

import javax.persistence.Column; 
import models.WiParameter; 
import com.google.gson.annotations.SerializedName;

public class WidgetTrackerSerializer {
	
	@SerializedName("apikey")
	private String apikey;
	@SerializedName("publicKey")
	private String publicKey;
	@SerializedName("host")
	private String host;
	@SerializedName("domain")
	private String domain; 
	
	@SerializedName("hasImageText")
	private Boolean hasImageText;
	@SerializedName("buttonText")
	private String buttonText;
	@SerializedName("buttonColor")
	private String buttonColor;
	@SerializedName("buttonFont")
	private String buttonFont;
	
	public WidgetTrackerSerializer(String apikey, String host, String domain,boolean hasImageText,String buttonText,
			String buttonColor,String publicKey,String buttonFont) { 
		this.setApikey(apikey); 
		this.setHost(host);
		this.setDomain(domain); 
		this.setHasImageText(hasImageText);
		this.setButtonText(buttonText);
		this.setButtonColor(buttonColor);
		this.setPublicKey(publicKey);
		this.setButtonFont(buttonFont);
	}
	
	public WidgetTrackerSerializer(String apikey, String host, String domain,String publicKey, WiParameter params){ 
		this(apikey,host,domain,false,params.getButtonText(),params.getButtonColor(),publicKey,params.getButtonFont());
		boolean hasImageText = ((params.getImageText()!=null) && (params.getImageText().length()>10));
		this.setHasImageText(hasImageText);
	}
	
	public WidgetTrackerSerializer(String apikey, String host, String domain){ 
		this.setApikey(apikey);
		this.setHost(host);
		this.setDomain(domain);
		this.setHasImageText(false);
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public boolean hasImageText() {
		return hasImageText;
	}

	public void setHasImageText(boolean hasImageText) {
		this.hasImageText = hasImageText;
	}

	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	public String getButtonColor() {
		return buttonColor;
	}

	public void setButtonColor(String buttonColor) {
		this.buttonColor = buttonColor;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getButtonFont() {
		return buttonFont;
	}

	public void setButtonFont(String buttonFont) {
		this.buttonFont = buttonFont;
	}
 
	
}
