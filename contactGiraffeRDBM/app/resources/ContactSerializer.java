package resources;
import java.io.Serializable;

 

import java.util.*;
import api.fullcontact.*;

public class ContactSerializer implements Serializable {
	
	private String apiKey;
	 
	private FullContact fullContact;
	private Long profileId;
	private String email; 	
	private FullContactEntity entity;
	
	public ContactSerializer(String apiKey, Long profileId,String email){
		this.apiKey = apiKey;
		this.email = email;
		this.setProfileId(profileId); 		
	}
	
	@Override
	public String toString() {
		return "ContactSerializer [profileId=" + profileId + ", email=" + email
				+ ", entity=" + entity + "]";
	}

	/**
	 * get all the contact profile of the user
	 * @throws FullContactException 
	 */
	
	public void pullContactEntity() throws FullContactException,NullPointerException{
		if(this.email == null || this.email.trim().isEmpty())
			throw new NullPointerException("email addres of user is required");
		
		this.fullContact = new FullContact(this.apiKey);
		this.setEntity(fullContact.getPersonInformation(this.email)); 	
	}
		
	public String getApiKey() {
		return apiKey;
	}
	
	public String getEmail(){
		return this.email;
	}

	public FullContactEntity getEntity() {
		return entity;
	}

	private void setEntity(FullContactEntity entity) {
		this.entity = entity;
	}

	
	public Long getProfileId() {
		return profileId;
	}
	

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}
}