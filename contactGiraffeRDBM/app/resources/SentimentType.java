package resources;

public enum SentimentType {
	Positive,
	Negative,
	Neutral,
	NoSentiment
}
