package resources;

import java.io.Serializable;

public class WixSerializer implements Serializable{
	
	public String apikey;
	public String pluginType;
	public String instanceId;
	public String vendorProductId;
	
	public WixSerializer(String apikey, String pluginType, String instanceId,
			String vendorProductId) {
		super();
		this.apikey = apikey;
		this.pluginType = pluginType;
		this.instanceId = instanceId;
		this.vendorProductId = vendorProductId;
	}
	
}
