package resources;

public enum UserType {
	Admin,
	Agent;
	
	public static boolean isAdmin(UserType userType){
		return userType.compareTo(UserType.Admin)==0;
	}	
	
	public static boolean isAgent(UserType userType){
		return userType.compareTo(UserType.Agent)==0;
	}	
}
