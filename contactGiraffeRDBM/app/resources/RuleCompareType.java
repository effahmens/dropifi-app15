package resources;

public enum RuleCompareType{
	 //general comparison
	 contains,
     does_not_contain, 
     begins_with, 
     ends_with, 
     is_equal_to, 
     is_not_equal_to,  
     //customer prospect comparison
     True,
     False, 	  
     //sentiment comparison
	 positive,
	 negative,
	 neutral, 
	 //emotion comparison
	 affection_friendliness,
	 amusement_excitment,	 
	 contentment_gratitude,
	 enjoyment_elation,
	 anger_loathing,
	 fear_uneasiness,
	 humailiation_shame,	 
	 sadness_grief;
}
