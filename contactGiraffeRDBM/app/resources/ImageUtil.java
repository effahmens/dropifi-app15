package resources;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.awt.image.BufferedImage; 

import javax.imageio.ImageIO;  

import com.ning.http.util.Base64;
public class ImageUtil {
	/**
     * Decode string to image
     * @param imageString The string to decode
     * @return decoded image
     */
    public static BufferedImage decodeToImage(String imageString) { 
    	
        BufferedImage image = null;
        byte[] imageByte; 
        try {
            //BASE64Decoder decoder = new BASE64Decoder();
            imageByte = Base64.decode(imageString); //decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis); 
            play.Logger.log4j.info("Created InputStream: "+ bis+" : "+ image);
            bis.close();
        } catch (Exception e) {
        	play.Logger.log4j.info(e.getMessage(),e);
        } 
        return image;
    }

    /**
     * Encode image to string
     * @param image The image to encode
     * @param type jpeg, bmp, ...
     * @return encoded string
     */
    public static String encodeToString(BufferedImage image, String type) {
    	String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray(); 
            //BASE64Encoder encoder = new BASE64Encoder();
            imageString = Base64.encode(imageBytes);//encoder.encode(imageBytes);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    } 
}
