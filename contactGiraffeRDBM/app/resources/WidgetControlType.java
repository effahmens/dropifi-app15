package resources;

public enum WidgetControlType {
	
	Attachment,
	Checkbox,
	Email,
	Password,
	Number,
	Radio,
	Select,
	Text,
	TextArea
	
}
