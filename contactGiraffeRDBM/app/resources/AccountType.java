package resources;

public enum AccountType {
	Free,
	Starter,
	Lite,
	Business,
	Enterprise;
	
	public static AccountType valueOf(int value){
		switch(value){
			case 0: 
				return Free;
			case 1:
				return Starter;
			case 2:
				return Lite;
			case 3:
				return Business;
			case 4:
				return Enterprise;
			default:
				return null;
		}
	}
}
