package resources;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

import models.*;
import org.bson.types.ObjectId;

import adminModels.DropifiMailbox;

import play.db.jpa.Blob;
import view_serializers.AttSerializer;
import view_serializers.ClientSerializer;
import view_serializers.MbSerializer;

 
public class MsgSerializer implements Serializable{
	private String domain; 
	private String msgId;
	private  Long companyId;
	private  Long inboundMb_id;
	private  String email=null;
	private  String fullname=null;
	private  String subject=null;
	private  String textBody=null;
	private  String htmlBody=null;
	private  Timestamp receivedDate;
	private  Timestamp sentDate;
	private	 Long sourceId; 
	private  String sourceOfMsg;
	private  int numOfMsg;
	public   static MbSerializer mailbox;
	private  String apikey;
	
	private Map<String, String>optionals;
	
	private List<AttSerializer> attachments;
	private String phone;
	
  	public MsgSerializer(String email, String fullname, String subject,
			String textBody, String htmlBody,String phone, Timestamp dateReceived, Timestamp dateSent, 
			Long companyId,Long sourceId,String sourceOfMsg) {
		 
		this.setEmail(email);
		this.setFullname(fullname);
		this.setSubject(subject);
		this.setTextBody(textBody);
		this.setHtmlBody(htmlBody);
		this.setPhone(phone);
		this.setReceivedDate(dateReceived);
		this.setSentDate(dateSent);
		this.companyId = companyId;
		this.sourceId =sourceId;
		this.setSourceOfMsg(sourceOfMsg);
		this.attachments = new ArrayList<AttSerializer>(); 
	}
	
	public MsgSerializer(Long companyId,Long inboundMb_Id){
		this.companyId = companyId;
		this.inboundMb_id = inboundMb_Id;
		this.attachments = new ArrayList<AttSerializer>();
	}
	
	public MsgSerializer(Long companyId,Long inboundMb_Id,long sourceId,String sourceOfMsg){
		this.companyId = companyId;
		this.inboundMb_id = inboundMb_Id;
		this.attachments = new ArrayList<AttSerializer>();
		this.setSourceId(sourceId);
		this.setSourceOfMsg(sourceOfMsg);
	}
	
	public MsgSerializer(ClientSerializer cs, long companyId,Timestamp dateReceived, Timestamp dateSent, long sourceId,String sourceMsg){
		this(cs.getEmail(),cs.getName(),cs.getSubject(),cs.getMessage(),cs.getMessage(),cs.getPhone(), dateReceived,dateSent,
				companyId,sourceId,sourceMsg);
  
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject =subject;
	}

	public String getTextBody() {
		return textBody;
	}

	public void setTextBody(String textBody) {
		this.textBody = DropifiTools.removeHTML(textBody); 
	}

	public String getHtmlBody() {
		return htmlBody;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody; 
	}

	public Timestamp getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Timestamp dateReceived) {
		this.receivedDate = dateReceived;
	}

	public Timestamp getSentDate() {
		return sentDate;
	}

	public void setSentDate(Timestamp dateSent) {
		this.sentDate = dateSent;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(Long SourceId) {
		this.sourceId = SourceId;
	}
	
	public String getSourceOfMsg() {
		return sourceOfMsg;
	}

	public void setSourceOfMsg(String sourceOfMsg) {
		this.sourceOfMsg = sourceOfMsg;
	}
	
	public List<AttSerializer> getAttachments() {
		return attachments;
	}
	
	public void setAttachments(List<AttSerializer> attachments) {
		this.attachments = attachments;
	}
	
	public void addAttachment(String fileName,String prefix,String fileType,File file,String comments,String attachmentId){			 
		this.attachments.add(new AttSerializer(null,this.getDomain(), this.getEmail(),this.getMsgId(),fileName, fileType, file,prefix,comments,attachmentId)); 
	}
	
	public void addAttachment(String fileName,String fileType,InputStream input,String attachmentId){			 
		this.attachments.add(new AttSerializer(null,this.getDomain(), this.getEmail(),this.getMsgId(),fileName, fileType, input,attachmentId)); 
	}
	
	public Long getInboundMb_id() {
		return inboundMb_id;
	}

	public void setInboundMb_id(Long inboundMb_id) {
		this.inboundMb_id = inboundMb_id;
	}

	public int getNumOfMsg() {
		return numOfMsg;
	}

	public void setNumOfMsg(int numOfMsg) {
		this.numOfMsg = numOfMsg;
	}
	
	public Map<String, String> getOptionals() {
		return optionals;
	}
	
	public void setOptionals(Map<String, String> optionals) {
		this.optionals = optionals;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	 
	public static void setMailbox(MbSerializer mb){
		mailbox = mb;
	}

	
	public String getPhone() {
		return phone;
	}
	

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
}
