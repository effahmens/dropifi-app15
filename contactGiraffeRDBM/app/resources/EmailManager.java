package resources;

import static akka.actor.Actors.actorOf;
import gmailoauth2grant.DGOauthGrant;
import gmailoauth2grant.DGOauthMail;

import javax.imageio.stream.FileImageOutputStream;
import javax.mail.*;
import javax.mail.event.*; 
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage.RecipientType;

import models.Company;
import models.DropifiMessage;
import models.InboundMailbox;
import models.LastPull;
import models.gmailauth.GmailCredentials;

import org.bson.types.ObjectId;

import com.google.api.client.auth.oauth2.Credential;

import play.libs.MimeTypes;
import adminModels.DropifiMailbox;
import adminModels.MailingType;
import akka.actor.ActorRef;
import akka.remote.MessageSerializer;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import job.process.MessageActor;

 
public class EmailManager{	
	
	private Store store;	 
	private InboundMailbox mailbox;
	private boolean isConnected;
	private GmailCredentials gcred;
	String apikey;
	
	public EmailManager(InboundMailbox mailbox) throws MessagingException, IOException{
		this.initialize(mailbox);			
	}
	
	public EmailManager(InboundMailbox mailbox,Connection conn) throws MessagingException, IOException{
		this.initialize(mailbox,conn);			
	}
	
	public EmailManager(){
		//initialize all component of the class
		//this.initialize();
	}
	
	private void initialize(InboundMailbox mailbox) throws MessagingException, IOException{
		//Instantiate instance variables	 
		this.mailbox = mailbox;
		this.apikey = Company.findApikey(mailbox.getCompanyId());
		 
		//check if the mailbox is a gmail oauth2: if true connect to the IMAP store using gmail oauth2
		this.gcred = null;
		if(mailbox.getMailserver().equalsIgnoreCase("Gmail")){
			
			try{
				this.gcred = GmailCredentials.retrieveCredential(this.apikey, mailbox.getEmail());
				if(this.gcred!=null){
					if(GmailCredentials.isExpired(this.gcred) || this.gcred.getAccessToken()==null){
						Credential credential = DGOauthGrant.authorization_refresh(this.gcred.getRefreshToken()); 
						this.gcred = GmailCredentials.updateCredentials(this.gcred, credential,this.gcred.getAccessCode()); 
					}
				} 
				
				if(this.gcred!=null && this.gcred.getAccessToken()!=null){
					this.store = DGOauthMail.getStore(this.gcred);
				}else{
					this.gcred = null;
					this.store = null;
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		if(this.gcred==null || this.store==null){
			
			// Setup properties
			Properties props = System.getProperties();	
			props.put("mail.imaps.ssl.checkserveridentity", "false");
			props.put("mail.imaps.ssl.trust", "*");
			props.put("mail.imap.ssl.checkserveridentity", "false");
			props.put("mail.imap.ssl.trust", "*");
			
			props.put("mail.pop3.ssl.checkserveridentity", "false");
			props.put("mail.pop3.ssl.trust","*");
			
			//getting the session for accessing email
			Session session = Session.getDefaultInstance(props);
			this.store = session.getStore(this.mailbox.getHostType());	  
		}
	}
	
	private void initialize(InboundMailbox mailbox,Connection conn) throws MessagingException, IOException{
		 
		//Instantiate instance variables	 
		this.mailbox = mailbox;
		this.apikey = Company.findApikey(conn,mailbox.getCompanyId());
		 
		//check if the mailbox is a gmail oauth2: if true connect to the IMAP store using gmail oauth2
		this.gcred = null;
		if(mailbox.getMailserver().equalsIgnoreCase("Gmail")){
			
			try{
				this.gcred = GmailCredentials.retrieveCredential(conn,this.apikey, mailbox.getEmail());
				if(this.gcred!=null){
					if(GmailCredentials.isExpired(this.gcred) || this.gcred.getAccessToken()==null){
						if(this.gcred.getRefreshToken()!=null){
							Credential credential = DGOauthGrant.authorization_refresh(this.gcred.getRefreshToken()); 
							this.gcred = GmailCredentials.updateCredentials(conn,this.gcred, credential,this.gcred.getAccessCode());
						}
					}
				} 
				
				if(this.gcred!=null && this.gcred.getAccessToken()!=null){
					this.store = DGOauthMail.getStore(this.gcred);
				}else{
					this.gcred = null;
					this.store = null;
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		if(this.gcred==null || this.store==null){
			
			// Setup properties
			Properties props = System.getProperties();	
			props.put("mail.imaps.ssl.checkserveridentity", "false");
			props.put("mail.imaps.ssl.trust", "*");
			props.put("mail.imap.ssl.checkserveridentity", "false");
			props.put("mail.imap.ssl.trust", "*");
			
			props.put("mail.pop3.ssl.checkserveridentity", "false");
			props.put("mail.pop3.ssl.trust","*");
			
			//getting the session for accessing email
			Session session = Session.getDefaultInstance(props);
			this.store = session.getStore(this.mailbox.getHostType());	
		
			//listening to store connections
			/*this.store.addConnectionListener(new ConnectionListener() {
				
				//when connection to a mailing server is opened, pull the new messages into
				//it's associate dropifi inbox
				
				@Override
				public void opened(ConnectionEvent arg0) {
					//System.out.println("Connection opened"); 
					// TODO retrieve the current message from the user inbox, take the metrics on them
					//and save them to the database
					
					//TOBE Implemented 				 
					try {
						pullMessageFromInbox();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						play.Logger.log4j.info(e.getMessage(),e);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						play.Logger.log4j.info(e.getMessage(),e);
					} 				 		 
				}
				
				@Override
				public void disconnected(ConnectionEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void closed(ConnectionEvent arg0) {
					// TODO Auto-generated method stub
					//System.out.println("Connection closed");
				}
			
			});
					
			//listening to store notifications
			this.store.addStoreListener(new StoreListener() {
				
				@Override
				public void notification(StoreEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		*/
		}
	}
	
	public Store connect() throws Exception {
		return connect(null);
	}
	//establish a connection to the mailing server to start pulling messages from the inbox.
	public Store connect(Connection conn) throws Exception {			
		//connect to store
		if(this.gcred!=null && this.gcred.getAccessToken()!=null){
			if(!this.store.isConnected()){ 
				this.store = DGOauthMail.getStore(this.gcred);
			}
			if(conn==null)
				this.pullMessageFromInbox();
			else
				this.pullMessageFromInbox(conn);
		}else{
			String username = this.mailbox.getEmail();
			if(this.mailbox.getAuthName()!=null && !this.mailbox.getAuthName().isEmpty())
				username = this.mailbox.getAuthName();
			
			try{
				this.store.connect(this.mailbox.getHost(),this.mailbox.getPort(),username, this.mailbox.getPassword());
			}catch(Exception e){
				//Connection to the mailbox failed. send an email to username to update his Dropifi settings
				//play.Logger.log4j.info(this.mailbox.getCompanyId()+". "+ this.mailbox.getEmail()+" ==> "+e.getMessage());
			}
			
			if(this.store.isConnected()){ 
				if(conn==null)
					this.pullMessageFromInbox();
				else
					this.pullMessageFromInbox(conn);
			}
		}
		return store;
	}
	
	public Folder getFolder() throws MessagingException{
		if(store.isConnected())
			return store.getFolder("inbox");
		return null;
	}
	
	public void unLockMailbox(Connection conn) throws SQLException, Exception{
		if(conn==null)
			   this.mailbox.unLockMailbox();
			else
				InboundMailbox.unLockMailbox(conn, this.mailbox);
	}
	public void pullMessageFromInbox() throws Exception{
		pullMessageFromInbox(null);
	}
	
	public void pullMessageFromInbox(Connection conn) throws Exception{	
		
		if(this.mailbox == null)
			return; 
		
		try {
			
			if(this.mailbox.checkLockState() == false){ //updated
			
				int lockMailbox = conn==null?this.mailbox.lockMailbox():InboundMailbox.lockMailbox(conn, this.mailbox);
				//connecting to store 
				if(lockMailbox==200){ //updated
					
					//instantiate a folder which will be used to pull messages from the inbox
					//initialize the folder which contains the messages
					Folder folder = this.store.getFolder("inbox");		
					 			
					//check if the inbox folder exist
					if(folder.exists()){
						
						//reading the inbox messages
						folder.open(Folder.READ_ONLY);		
						
						//retrieve all the inbox folder messages
						Message[] msg = folder.getMessages();
					
						int msgLength = msg.length;
						
						
						//check the last message		
						int startpull = -400;
						
						try{
							//this prevent pulling of email messages using this mailbox by another thread
							//play.Logger.log4j.info(" Status of Mailbox: there "+ this.mailbox.getIslock()); 
							startpull = conn==null? LastPull.getLast(this.mailbox, msg):LastPull.getLast(conn, this.mailbox, msg);	 //updated				
							//play.Logger.log4j.info(" Status of Mailbox: here "+ startpull); 
							
							//-400 means no new message or there is no user with the specified key			
							if(startpull != -400){ 
								//play.Logger.log4j.info("Found company: " + mailbox.getCompanyId());
								/**
								 * process the messages and save them to the database
								 */
								
								//compute the number of messages to be pulled 
								int numOfMsg = msgLength - startpull;	

								try{  
									//DCON.MAILER_DROPIFI_COM = "team@dropifi.com";  
									//DropifiMailbox.findEmailByIdentifier(conn, MailingType.Notification); 
									//publish the message to akka message handler
									
									for(int i = numOfMsg; i<msgLength; i++){ 											
										try{
		
											if(i<0)
												continue;
											
											//get the message at index i 
											Message retMsg = msg[i];  
											
											if(retMsg == null) 
												continue;
											
											//create an instance of MsgSerializer for receiving the incoming messages								 
											MsgSerializer msgSend = new MsgSerializer(mailbox.getCompanyId(), mailbox.getId(), mailbox.getId(), mailbox.getEmail());
											msgSend.setApikey(this.apikey);
											msgSend.setDomain(mailbox.getDomain());
																						
											//find the sender of the email
											Address[] addfrom = retMsg.getFrom();	
											
											boolean hasEmail = false;
											//retrieve the fullname and sender of email
											for(int j=0; j< addfrom.length; j++){
												//retrieve the email and name of sender
												String[] nameEMail =  nameAndEmail(addfrom[j].toString()); 
												if(hasEmail == false){
													//get the sender name and email address	
													msgSend.setFullname(nameEMail[0]); 
													msgSend.setEmail(nameEMail[1]);											
												}
												//play.Logger.log4j.info("From: " + nameEMail[1]);										
											}
											
											//retrieve the subject of the message
											msgSend.setSubject(retMsg.getSubject());								 
											
											//retrieve the received and sent date of the message
											Timestamp drec  = retMsg.getReceivedDate()!=null?new Timestamp(retMsg.getReceivedDate().getTime()):new Timestamp(new Date().getTime());
											msgSend.setReceivedDate(drec);
											 
											Timestamp dsent  = retMsg.getSentDate()!=null?new Timestamp(retMsg.getSentDate().getTime()):msgSend.getReceivedDate();
											msgSend.setSentDate(dsent);					 				 		
											
											/*
											//if the sender of the message is the default dropifi mailer. mark message as read
											//NB: if true, message was sent from the widget
											if(msgSend.getEmail().equalsIgnoreCase(DCON.MAILER_DROPIFI_COM)){ 
												 //LastPull.updateLastPull(msgSend.getInboundMb_id(), msgSend.getReceivedDate(), msgSend.getSentDate(), (i+1)); 
												 continue; 									 
											} 
											*/
											
											//retrieve the body and any other attached files on the message	
											try{ 	
												//generate a key for the message
												String msgId = DropifiMessage.genMsgId(mailbox.getCompanyId(), msgSend.getEmail()); 
												msgSend.setMsgId(msgId);
												
												try{
													Object content = retMsg.getContent();
											        if (content instanceof Multipart) {				          
											           handleMultipart(msgSend, (Multipart)content);						          
											        } else {				        	
											           handlePart(msgSend,retMsg);						          
											        }
												}catch(Exception e){
													play.Logger.log4j.info("Error Retrieve message: "+ e.getMessage(),e);
												}
										        							
										        //set the type of message				      
										        msgSend.setSourceId(mailbox.getId() ); 
										        msgSend.setSourceOfMsg(mailbox.getEmail()); 
										       
										        //index of the message received
												msgSend.setNumOfMsg(i+1); 
												
												//set the message as the last email message received by the company	      
											  if(conn==null)
												  LastPull.updateLastPull(this.mailbox.getId(), msgSend.getReceivedDate(), msgSend.getSentDate(),msgSend.getNumOfMsg());
											  else
												  LastPull.updateLastPull(conn,this.mailbox.getId(), msgSend.getReceivedDate(), msgSend.getSentDate(),msgSend.getNumOfMsg());
											 
											  ActorRef pullMessage = actorOf(MessageActor.class).start();
											  pullMessage.tell(msgSend);
											}catch(Exception e){
												//releases the lock on the mailbox for other thread or process to use
												//InboundMailbox.unLockMailbox(conn,mailbox);									
												play.Logger.log4j.info(e.getMessage(),e);
												//pullMessage.stop();	
												continue;
											}
											
											//this.mailbox.saveLastPull(retMsg.getReceivedDate(),retMsg.getSentDate(), msgLength); 
											//play.Logger.log4j.info("---------------------------------- end message ---------------------------------");
										 
										} catch(Exception e) {
											// TODO Auto-generated catch block 
											//releases the lock on the mailbox for other thread or process to use
											play.Logger.log4j.info("Error  message pull: 1: " + e.getMessage(),e);
											//InboundMailbox.unLockMailbox(conn,mailbox);	
											continue;
										} 
									
										
									}//end of for loop
									//
								   this.unLockMailbox(conn); 
								   this.store.close();
								   
								  //pullMessage.stop();						 
								 //no new message(s)						
								}catch(Exception e){
									play.Logger.log4j.info("Error message: 1"+e.getMessage(),e); 
								}
								
								//end of pulling (status != -400)
							}							
							 		 		
							//this.unLockMailbox(conn); 
							//this.store.close();
						}catch (Exception e) {  
							try {
								play.Logger.log4j.info("LastPull is null: "+e.getMessage(),e); 
							    //this.unLockMailbox(conn); 
								//this.store.close();
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								play.Logger.log4j.info(e1.getMessage(),e); 
							}				 
						}  				
							
					}/* else {
						 this.unLockMailbox(conn); 
					}*/
					//folder do not exist					
				}
			}
			//end of store try
		}catch (MessagingException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info("Error message :"+e.getMessage(),e);
			try {
				
				this.store.close();
			} catch (MessagingException e1) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}finally{ 
			try{
				this.unLockMailbox(conn); 
				this.store.close(); 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			} 
		} 		
		
	}
	
	private String[] nameAndEmail(String address){
		String[] addr = new String[2];
		
		if(address.contains("<")){
			addr = address.toString().split("<"); 		
			addr[0] = addr[0].replace('\"', ' ').trim();
			addr[1] = addr[1].replace('>', ' ').trim().toLowerCase(); 
		}else{
			addr[0]= "";
			addr[1]= address.trim().toLowerCase();
		}
		
		return addr;
	}
	
	/**
	 * retrieve the body and attached files of a sent email
	 * @param message
	 * @param multipart
	 * @throws MessagingException
	 * @throws IOException
	 */
	public void handleMultipart(MsgSerializer message,Multipart multipart) throws MessagingException, IOException {	    
		for(int i=0;i <multipart.getCount();  i++) {
	      handlePart(message,multipart.getBodyPart(i));
	    }
   }
	
	/**
	 * retrieve the message body and attached files of a received email
	 * @param message
	 * @param part
	 * @throws MessagingException
	 * @throws IOException
	 */
	public void handlePart(MsgSerializer message, Part part) throws MessagingException, IOException {
		
	    String disposition = part.getDisposition();
	    String contentType = part.getContentType();
	    String[] conType = contentType.split(";");
	    
	   // play.Logger.log4j.info("Type "  + contentType); 
	    
	    if (disposition == null) { //check if multipart is an attachment or body
	      
		      //When just body	     
		      //Check if plain
		      if (conType[0].equalsIgnoreCase("text/plain")) {
		    	 String content = (String) part.getContent(); 		    	 
		    	 //add retrieve message to list
		    	 message.setTextBody(content);
		    	 
		    	 if(message.getHtmlBody() == null || message.getHtmlBody().trim().isEmpty())
		    		 message.setHtmlBody(content);		    	 
		      } else { 		    	  
			    	//read multipart or text/html content
			      // play.Logger.info("Other Type: " + contentType);
			        Object otherbody = part.getContent();
			       
			        if(otherbody instanceof Multipart){
				      Multipart mime =(Multipart) otherbody; 				      
				      handleMultipart(message, mime); 				      
				       
			        } else if(otherbody instanceof InputStream){
			        	 
			        	//addFileToS3(message,part); 
			        //	play.Logger.log4j.info("Input Stream message");
			        }else {		
			        	
			        	String content = part.getContent().toString().trim(); 
			        	
			        	if(!content.isEmpty() && conType[0].equalsIgnoreCase("text/plain")){
				        	if(message.getTextBody() == null)
				        		message.setTextBody(content);
				        	
				        	if(message.getHtmlBody() == null || message.getHtmlBody().trim().isEmpty())
				        		message.setHtmlBody(content);
				        	
				       }else if(!content.isEmpty() && conType[0].equalsIgnoreCase("text/html")) {
				    	   if(message.getTextBody() == null)
				        		message.setTextBody(HTMLUtils.Html2Text(content));				        	
				    	   
				    		   message.setHtmlBody(content);
				       }			        	
			        } 			        
		     }
	      
		//IMPLEMENTED
	    //download attached files
	    } else if (disposition.equalsIgnoreCase(Part.ATTACHMENT)) {
	    	
	    	//add file attachment to message
	    	//message.addAttachment(part.getFileName(),conType[0],part.getInputStream());
	    	addFileToS3(message,part); 
	    } else if (disposition.equalsIgnoreCase(Part.INLINE)) {
	    	//System.out.println("Inline: " + contentType);	
	    	
	    	String inline = part.getContent().toString().trim();
	    	String  inType = contentType.split(";")[0];	    	
	    	
	    	if(inType.equalsIgnoreCase("text/plain")){
	    		message.setTextBody(inline);
	    		
	    		if(message.getHtmlBody() == null || message.getHtmlBody().trim().isEmpty())
	    			message.setHtmlBody(inline);
	    		
	    	}else if(inType.equalsIgnoreCase("text/html")&&inline != ""){
	    		if(message.getTextBody()==null)
	    			message.setTextBody(HTMLUtils.Html2Text(inline));	    		
	    		
	    			message.setHtmlBody(inline);	    		
	    	}else{ 	    	   
	    		 	    		
	    		//add file attachment to message
	    		//message.addAttachment(part.getFileName(),inType,part.getInputStream());
		    	addFileToS3(message,part); 
	    	} 
	    	//System.out.println("Inline: " + inline);	
	    	  
	    } else { //Should never happen
	    	//play.Logger.log4j.info("Other: " + disposition);
	    }
	    	   
	 }
		 
	public Map<String,Object> saveFile(String subFolder,String filename, InputStream input) throws IOException {
	    if (filename == null) {
	      filename = File.createTempFile("xx", ".out").getName();
	    }
	    
	    String destination =  play.Play.applicationPath.getAbsolutePath()+"/company/"+subFolder+"/";
	    
	    //Do no overwrite existing file
	    File file = new File(destination+filename);
	    
	    //String prefix= "";
	    if(file.exists()){		   
		    for (int i=0; file.exists(); i++){
		      file = new File(destination+ i+filename); 
		      //prefix = String.valueOf(i);
		    }
	    }  
	    
	    FileOutputStream fos = new FileOutputStream(file);
	    BufferedOutputStream bos = new BufferedOutputStream(fos);	
	    BufferedInputStream bis = new BufferedInputStream(input); 	    
	    
	    int aByte;
	    while ((aByte = bis.read()) != -1) {
	      bos.write(aByte);
	    }
	     
	    bos.flush();
	    bos.close();
	    bis.close();
	    
	    Map<String,Object> files = new HashMap<String, Object>();
	   // files.put("prefix", prefix);
	    
	    files.put("file", file);
	    
	    return files;
	}
		
	private void addFileToS3(MsgSerializer message, Part part) {
		try{  
			//create the file
	    	//Map<String,Object> files = saveFile(this.mailbox.getDomain(),part.getFileName(), part.getInputStream()); 
	    	//String prefix = (String) files.get("prefix");
	    	//File file = (File) files.get("file");
	    	
	    	//add file attachment to message
	    	/*String contentType =part.getContentType();
	    	if(part.getContentType().contains(";"))
	    	 contentType = contentType.split(";")[0];  
	    	 
	    	message.addAttachment(part.getFileName(),contentType,part.getInputStream(),DropifiTools.generateCode(12));	
	    	
	    	*/ 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	} 
	 	
	public void sendMessage() throws AddressException, MessagingException, UnsupportedEncodingException{
		Properties props = System.getProperties();
		
		//create a session
		Session session = Session.getDefaultInstance(props,null);
		
		//create a mime-message
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("efpam2010@gmail.com"));
		
		//create recipients
		Address[] address = {
			new InternetAddress("philips@dropifi.com","Philips Effah"),
			new InternetAddress("kamil@dropifi.com","Kamil Nabong"),
			new InternetAddress("david@dropifi.com","David Osei")
		};
		
		
		msg.addRecipients(javax.mail.Message.RecipientType.TO,address);
		msg.setSubject("This message was sent for testing purposes");
		msg.setContent("<h1>I love <strong>java</strong> because it is easy to use</h1>","text/html");
		msg.saveChanges();
		
		Transport transport = session.getTransport("smtps");
		transport.connect("smtp.gmail.com", 465, "efpam2010@gmail.com", "nanasa88");
		System.out.println("Connection successful....................");
		transport.sendMessage(msg, msg.getAllRecipients());
		System.out.println("Message sent successfully....................");
		transport.close();				
	}
	
	public PasswordAuthentication getPasswordAuthenticator(String username,String password){
		return new PasswordAuthentication(username, password);
	}

	public boolean isConnected() {
		return isConnected;
	}

	private void setIsConnected(boolean isconnected) {
		this.isConnected = isconnected;
	}
		
}
