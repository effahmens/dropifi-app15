package resources;

import java.io.Serializable;
import java.util.Map;

import models.CompanyProfile;
import models.GeoLocation;

import com.google.gson.annotations.SerializedName;
import view_serializers.ClientSerializer;
import view_serializers.CoProfileSerializer;
import view_serializers.MbSerializer;

public class WidgetRespSerializer  implements Serializable{
	
	@SerializedName("companyId")
	private Long companyId;
	@SerializedName("apikey")
	private String apikey;
	private Long widgetId;
	@SerializedName("clientSerializer")	
	private ClientSerializer clientSerializer;
	private String defaultEmail;
	private String defaultSubject;
	private String defaultMessage;
	private Boolean responseActivated;
	private MbSerializer dropifiMailbox;
	private CoProfileSerializer profile; 
	private String domain; 
	@SerializedName("location")
	private GeoLocation location;
	
	public WidgetRespSerializer(Long companyId, String apikey, Long widgetId,
			ClientSerializer clientSerializer,String defaultEMail,String defaultSubject, 
			String defaultMessage,Boolean responseActivated ){
		 
		this.setCompanyId(companyId); 
		this.setApikey(apikey);
		this.setClientSerializer(clientSerializer);
		this.setDefaultEmail(defaultEMail);
		this.setDefaultSubject(defaultSubject);
		this.setDefaultMessage(defaultMessage);
		this.setResponseActivated(responseActivated);
		this.setWidgetId(widgetId); 		 
	} 	
	
	@Override
	public String toString() {
		return "WidgetRespSerializer [companyId=" + companyId + ", apikey="
				+ apikey + ", widgetId=" + widgetId + ", clientSerializer="
				+ clientSerializer + ", defaultEmail=" + defaultEmail
				+ ", responseActivated=" + responseActivated + ", profile="
				+ profile + "]";
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public ClientSerializer getClientSerializer() {
		return clientSerializer;
	}

	public void setClientSerializer(ClientSerializer clientSerializer) {
		this.clientSerializer = clientSerializer;
	}

	public String getDefaultEmail() {
		return defaultEmail;
	}

	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	public Boolean getResponseActivated() {
		return responseActivated;
	}

	public void setResponseActivated(Boolean responseActivated) {
		this.responseActivated = responseActivated;
	}

	public Long getWidgetId() {
		return widgetId;
	}

	public void setWidgetId(Long widgetId) {
		this.widgetId = widgetId;
	}

	public CoProfileSerializer getProfile() {
		return profile;
	}

	public void setProfile(CoProfileSerializer profile) {
		this.profile = profile;
	}
	
	public String getDefaultSubject() {
		return defaultSubject;
	}

	public void setDefaultSubject(String defaultSubject) {
		this.defaultSubject = defaultSubject;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}
 
	public MbSerializer getDropifiMailbox() {
		return dropifiMailbox;
	}

	public void setDropifiMailbox(MbSerializer dropifiMailbox) {
		this.dropifiMailbox = dropifiMailbox;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public GeoLocation getLocation() {
		return location;
	}

	public void setLocation(GeoLocation location) {
		this.location = location;
	}
	
	
 	
}
