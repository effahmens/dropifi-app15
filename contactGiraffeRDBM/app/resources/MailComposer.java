package resources;

import java.util.List;

public class MailComposer {
	
	public static String html(String message, List<String> cssfiles, List<String> jsfiles){
		
		StringBuilder compose = new StringBuilder();
		compose.append("<!DOCTYPE html PUBLIC -//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		compose.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		compose.append("<head>");
		
		if(cssfiles !=null){
			//add css files
			for(String csfile: cssfiles){
				compose.append("<link rel='stylesheet' media='screen' href='"+csfile+"' / >");
			}
		}
		
		//add jquery as default js script
		//compose.append("<script type='text/javascript' src='/public/client/javascripts/jquery-1.7.2.min.js'></script>");
		
		if(jsfiles != null){
			//add js files		
			for(String jsfile: jsfiles){ 
				compose.append("<script type='text/javascript' src='"+jsfile+"'></script>");
			}
		}
		
		compose.append("</head><body>");		 
		compose.append(message); 
		compose.append("</body></html>");
		
		return compose.toString();
	}
}
