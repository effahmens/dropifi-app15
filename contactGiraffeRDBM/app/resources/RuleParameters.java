package resources;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import api.tictail.TictailStore;

import com.sun.xml.internal.ws.api.model.SEIModel;

import play.db.jpa.Blob;
import query.inbox.Contact_Serializer;
import view_serializers.AttSerializer;
import view_serializers.CoProfileSerializer;
import view_serializers.MbSerializer;  
import models.DropifiMessage;
import models.GeoLocation;
import resources.sentiment.SentimentBaseSerializer;
import resources.sentiment.lymbix.*;

public class RuleParameters implements Serializable{
	public Long companyId;
	public String sentiment;
	public String emotion;
	public String sender;
	public String msgId;
	public String subject;
	public String message; 
	public String intense;
	public String contactName;
	public String isCustomer;
	public String phone;
	public String domain;
	public String pageUrl;
	public Timestamp created;
	public String condition;
	public MbSerializer mailbox;
	public MbSerializer companyMailbox; 
	public String apikey;
	public boolean isResponseSent;
	public boolean hasSendAutoResponse; 
	public CoProfileSerializer compProfile;
	public String country;
	public String state;
	public String city; 
	private Contact_Serializer contactProfile;
	public Map<String,String> autoReplyTo;
	private AttSerializer attachedFile; 
	public String messageTemplate;
	public long countMsg;
	private String mgCampaignId;
	
	public RuleParameters(MsgSerializer msg, SentimentBaseSerializer sentiiment,MbSerializer mailbox,String apikey,GeoLocation location){
		 
		if(msg!=null){
			this.sender = msg.getEmail();
			this.subject = msg.getSubject();
			this.message = msg.getHtmlBody();
			this.phone = msg.getPhone();
			this.domain = msg.getDomain();	
			this.setContactName(msg.getFullname());
			this.msgId = msg.getMsgId();
		}else{
			this.sender="";
			this.subject="";
			this.message=""; 
			this.phone ="";
			this.domain ="";	
			this.setContactName("");
		}
		
		if(sentiiment!=null){
			this.sentiment = sentiiment.getSentiment();
			this.emotion = sentiiment.getEmotion();
			this.intense = sentiiment.getIntense();		
		}else{
			this.sentiment="";
			this.emotion="";
			this.intense="";
		}
		
		if(location!=null){
			this.country = location.getCountryName();
			this.state = location.getRegionName();
			this.city=location.getCity();
		}else{
			this.country="";
			this.state="";
			this.city="";
		}
		
		this.mailbox = mailbox;
		this.apikey = apikey;
		this.hasSendAutoResponse = false; 
		this.autoReplyTo = new HashMap<String, String>();
	}
	
	public RuleParameters(MsgSerializer msg, SentimentBaseSerializer sentiiment,MbSerializer mailbox){
		this(msg,sentiiment,mailbox,null,null);
	}
	
	public RuleParameters(){
		
	}

	public String getContactName() {
		return contactName;
	}
	
	private void setContactName(String contactName) {
		this.contactName = DropifiTools.convertEmailToName(this.sender, contactName);
	}

	public Contact_Serializer getContactProfile() {
		return contactProfile;
	}

	public void setContactProfile(Contact_Serializer contactProfile) {
		this.contactProfile = contactProfile; 	 
		this.setContactName(this.getContactProfile()!=null? this.getContactProfile().getFullName():null);
	}
	
	public void setAutoReplyTo(String email,String fullname){
		this.autoReplyTo.put("email", TictailStore.resetEmail(email));
		this.autoReplyTo.put("fullname", fullname);
	}

	public AttSerializer getAttachedFile() {
		return attachedFile;
	}

	public void setAttachedFile(AttSerializer attachedFile) {
		this.attachedFile = attachedFile;
	}

	public String getMgCampaignId() {
		return mgCampaignId;
	}

	public void setMgCampaignId(String mgCampaignId) {
		this.mgCampaignId = mgCampaignId;
	}
  

}
