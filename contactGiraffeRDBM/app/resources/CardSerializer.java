package resources;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.stripe.model.Card;

public class CardSerializer implements Serializable{
	
	@SerializedName("name")
	public String name;
	@SerializedName("last4")
	public String last4;
	@SerializedName("exp_month")
	public Integer exp_month;
	@SerializedName("exp_year")
	public Integer exp_year;
	@SerializedName("cvc_check")
	public String cvc_check;
	@SerializedName("type")
	public String type;
	
	public CardSerializer(Card card){
		if(card!=null){
			this.name = card.getName();
			this.last4 =card.getLast4();
			this.exp_month = card.getExpMonth();
			this.exp_year = card.getExpYear();
			this.cvc_check = card.getCvcCheck();
			this.type = card.getType();
		}
	}
}
