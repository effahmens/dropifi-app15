package resources;

import models.EcomBlogPlugin;

public enum EcomBlogType {
	Blogger,
	Magento,
	Shopify,
	Tumblr,
	Volusion,
	Wordpress,
	Dropifi,
	Wix,
	PrestaShop,
	CashieCommerce,
	Tictail;
 
	public String generateEmail(String email){
		 return this.name().toLowerCase() +"."+ email;
	}
	
	public static boolean isEcomBogType(String pluginType){
		if(pluginType==null || pluginType.trim().isEmpty() || pluginType.trim().equalsIgnoreCase("null")) 
			return false;
		
		try{
			pluginType = pluginType.trim();
			 
			EcomBlogType type = EcomBlogType.valueOf(pluginType); 
			switch(type.ordinal()){
				case 0: case 1: case 2: case 3: case 4: case 5:  case 7: case 8: case 9: case 10:
					return true;
				default: return false;				 
			}	
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			return false;
		} 
	}
	
	/**
	 * The plugin does not use publickey to load the widget on websites
	 * @param pluginType
	 * @return
	 */
	public static boolean isNonePublicKeyPlugin(String pluginType){
		if(pluginType==null || pluginType.trim().isEmpty() || pluginType.trim().equalsIgnoreCase("null")) 
			return false;
		
		try{
			pluginType = pluginType.trim(); 
			EcomBlogType type =  EcomBlogType.valueOf(pluginType);
			switch(type.ordinal()){
				case 2: case 10:
					return true;
				default: return false;				 
			}	
		}catch(Exception e){ 
			//play.Logger.log4j.info(e.getMessage(),e); 
		} 
		return false;
	}
	
	public static boolean isEcomBogType(EcomBlogType pluginType){
		 if(pluginType==null) return false;
		 return isEcomBogType(pluginType.name());
	} 
	
	public static EcomBlogType getValue(String pluginType){
		 return isEcomBogType(pluginType)?EcomBlogType.valueOf(pluginType):Dropifi;
	}
 
}
