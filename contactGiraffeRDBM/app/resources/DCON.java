package resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import play.Play;
import play.db.DB;

import javax.sql.*;

public class DCON { 
	
	private static Connection client;
	
	private String url, user, password;
	public static String  MAILER_DROPIFI_COM = "team@dropifi.com";
	
	//private Connection conn;	
 	public DCON(String url, String user, String password){
		this.url = url;
		this.user = user;
		this.password = password;
	}
 
	public static Connection getDefaultConnection(){		  
		try {			 
			Map<String,String> map = DropifiTools.getConnConfig();
			
			Class.forName(map.get("driver"),true,Play.classloader).newInstance();
			Connection con = DriverManager.getConnection(map.get("url"),map.get("user"), map.get("pass")); 
			return con; 
		}catch (Exception e) {
			// TODO Auto-generated catch block 	
			play.Logger.log4j.info(e.getMessage(), e); 
		} 
		return null;
	}
	 
	
	/**
	 * This is a synchronize connection. Do not close the connection anytime it is called
	 * @return
	 * @throws SQLException
	 */
	public static Connection GetConnectionClient() {
		try {
			if(DCON.client == null || DCON.client.isClosed()){
				DCON.client = getDefaultConnection();
			} 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(), e); 
		}
		return DCON.client;
	}
	
	public static Connection getDatasourceConnection(){
		 Map<String,String> map = DropifiTools.getConnConfig();
		 // Setting up the DataSource object
	      com.mysql.jdbc.jdbc2.optional.MysqlDataSource ds   = new com.mysql.jdbc.jdbc2.optional.MysqlDataSource();
	      ds.setServerName(map.get("server"));
	      ds.setPortNumber(Integer.valueOf(map.get("port")));
	      ds.setDatabaseName(map.get("database"));
	      ds.setUser(map.get("user"));
	      ds.setPassword(map.get("pass"));
	      
	      try {
			return ds.getConnection(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(), e); 
		}
	      return null;
	}
	
}
