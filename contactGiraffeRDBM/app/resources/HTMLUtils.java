package resources;
import java.io.IOException;
import java.io.FileReader;
import java.io.Reader;
import java.io.BufferedReader;
import org.jsoup.Jsoup;

public class HTMLUtils {
	private HTMLUtils() {}
	
	public static String extractText(Reader reader) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    BufferedReader br = new BufferedReader(reader);
	    String line;
	    while ( (line=br.readLine()) != null) {
	      sb.append(line);
	    }
	    String textOnly = Jsoup.parse(sb.toString()).text();
	    return textOnly;
	}
	
	public static String Html2TextRegx(String html){
		return html.toString().replaceAll("\\<.*?>","");
	}
	
	public static String Html2Text(String html){
		 String textOnly = Jsoup.parse(html.toString()).text();
		 return textOnly;
	}
}
