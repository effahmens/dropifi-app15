package resources;

import java.awt.Color;
import java.io.File;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import models.Widget;

import org.joda.time.DateTime;
import org.json.JSONObject;

import play.Play;
import play.libs.Codec;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class DropifiTools {

	public static String toDateString(Timestamp time){
		DateTime date = new DateTime(time.getTime());
		if(date.getDayOfMonth()== DateTime.now().getDayOfMonth()){
			int hour = date.getHourOfDay()==0?12:date.getHourOfDay();
			hour = hour>=12?(hour-12):hour;
			String AM_PM = date.getHourOfDay()>=12?"PM":"AM"; 
			
			return hour+ " : "+date.getMinuteOfHour()+" "+AM_PM;	
		} else{ 
			return date.getDayOfMonth()+File.separator+date.getMonthOfYear()+File.separator+date.getYear() + " ";
		}
	}
	
	public static JsonArray toJsonArray(String json){
		JsonParser parser = new JsonParser();
		JsonElement tradeElement = parser.parse(json);
		return tradeElement.getAsJsonArray();
	}
	
	public static Map<String, Object>createCacheMap(String cacheKey, String type,long companyId){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("key", cacheKey);
		map.put("type", "LPOP1");
		map.put("companyId",companyId);
		return map;
	}
	
	public static String reportAdminLogin(String siteurl, String loginType,String macAddress){ 
		return "<p>hi system admin at dropifi</P> " +
				"<p>This is to inform you that someone succeeded logging into "+
				siteurl+" with Mac-Address :"+macAddress+ " at "+new Date().toString();
	}
	
	public static String generateCode(int size){
		Timestamp time = new Timestamp(new Date().getTime()); 
		String code = Codec.hexSHA1(String.valueOf(time));
		
		if(code.length()>size){
			code = code.substring(0, size); 
		}
		return code;
	}
	
	public static HttpResponse restWS(String requestUrl){
		WSRequest rpxRequest = WS.url(requestUrl);
		HttpResponse res = null;
		try{ 
			res = rpxRequest.get();
			//return //(res!=null && res.success())?res:null;
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(),e); 
		}	
		
		return res;
	}
	
	public static String wordpressAuth(String requestUrl){ 
		WSRequest rpxRequest = WS.url(requestUrl);
		rpxRequest.setParameter("dropifi_token", "check");
		rpxRequest.setParameter("dropifi_apikey", "checkTest"); 		
		HttpResponse res = null;
		try{
			res = rpxRequest.get();
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(),e); 
		}		
		String foundToken ="";
		if( res!=null && res.success()){
			//request not successful	
			try{
				String resStr = res.getString();
				
				if(resStr.contains("{\"success")){
					int strIndex = resStr.indexOf("{\"success");
					//int endIndex = resStr.indexOf('}');				
					resStr = resStr.substring(strIndex).trim(); 
					
					//JsonParser jsonParser = new JsonParser();					 
					JSONObject auth = new JSONObject(resStr);
					 
					//Verify that the requestUrl accesstoken is valid;
					foundToken = auth.getString("accessToken");	
				}
			}catch(Exception e){
				play.Logger.log4j.error(e.getMessage(),e);
			}
		}
		
		return foundToken;
	}
 
	public static boolean isFromLocalWP(String requestUrl){
		requestUrl = requestUrl.toLowerCase();
		if(requestUrl.contains("localhost")){
			return true;
		}else if(requestUrl.contains("127.0.0.1")){
			return true; 
		}
		return false;
	}
	
	
	public static String convertEmailToName(String sender,String contactName){
		if((contactName == null || contactName.isEmpty()) && sender!=null && !sender.isEmpty() && sender.contains("@")){
			int e_index = sender.indexOf("@");
			contactName = sender.substring(0, e_index);
			contactName = contactName.replace('.', ' ').replace('_', ' ');
		} 
		return contactName; 		
	}
	
	/**
	 * convert a list of integer, long, string values to a comma separated string
	 * @param params
	 * @return
	 */
	public static String toSQLInParam(List<Long> values){
		String commaSeparated ="";
		int n=0;
		int size = values.size();
		for(long value : values){
			commaSeparated+= ""+value+""; 
			if(++n!=size){
				commaSeparated+=",";
			}
		}		
		return commaSeparated; 
	}
	
	public static String removeHTML(String htmlString) {
          // Remove HTML tag from java String 
		if(htmlString!=null){
	        String noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");
	
	        // Remove Carriage return from java String
	        noHTMLString = noHTMLString.replaceAll("\r", "<br/>");
	
	        // Remove New line from java string and replace html break
	        noHTMLString = noHTMLString.replaceAll("\n", " ");
	        noHTMLString = noHTMLString.replaceAll("\'", "&#39;");
	        noHTMLString = noHTMLString.replaceAll("\"", "&quot;");
	        return noHTMLString;
		}
		return "";
    }
	
	public static String testWSRequest(String ip){
		WSRequest rpxRequest = WS.url("http://api.hostip.info/get_html.php");
		rpxRequest.setParameter("ip",ip);
		
		HttpResponse res = rpxRequest.get(); 
		return res.getString();
	}
	
	public static String decreaseColor(String fontColor){
		if(fontColor!=null){
			String rawFontColor = fontColor.substring(1,fontColor.length());
	
			// convert hex string to int
			int rgb = Integer.parseInt(rawFontColor, 16);
	
			Color originalColour = new Color(rgb);
	
		    float hsbVals[] = Color.RGBtoHSB( originalColour.getRed(), originalColour.getGreen(), originalColour.getBlue(), null );
		    
		    Color shadow = Color.getHSBColor( hsbVals[0], hsbVals[1],hsbVals[2] );
		    return Integer.toHexString(shadow.getRGB() - ((int) (0.018642*shadow.getRGB()) ) ).replaceFirst("ff","#");
		}
		return "";
	}
	
	public static boolean isBrighten(String fontColor){
		//String fontColor = "#0cf356";
		if(fontColor==null)
			fontColor = "#0cf356";
		
		// remove hash character from string
		String rawFontColor = fontColor.substring(1,fontColor.length());

		// convert hex string to int
		int rgb = Integer.parseInt(rawFontColor, 16);

		Color c = new Color(rgb);

		float[] hsb = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), null);

		float brightness = hsb[2];
		//play.Logger.log4j.info(brightness + " brightness"); 
		if (brightness < 0.97) {
		   // use a bright background			 
			return false;
		} else {
		   // use a dark background			 
			return true;
		}
	}
	
	public static String[] splitName(String name){ 
		String[] names = name.split(" ", 2);
		if(names.length>=2){
			return names;
		}else{
			String[] temp  = {names[0] , " "};
			return temp;
		}
	}
	
	public static double roundTwoDecimals(double d) {
		return roundToDecimals(d, "#.##");
	}
	
	public static double roundToDecimals(double d, String format) {
	    DecimalFormat twoDForm = new DecimalFormat(format);
	    return Double.valueOf(twoDForm.format(d));
	}
	
	public static String converToDecimal(double x,String format){
		DecimalFormat df = new DecimalFormat(format); // Set your desired format here.
		return df.format(x/100.00); 
	}
	
	public static Map<String,String> getConnConfig(){
		Map<String,String> map = new HashMap<String,String>(){{
			put("url",    Play.configuration.getProperty("db.url"));
			put("driver", Play.configuration.getProperty("db.driver"));
			put("user",   Play.configuration.getProperty("db.user"));
			put("pass",   Play.configuration.getProperty("db.pass"));
			put("server", Play.configuration.getProperty("db.server"));
			put("database",   Play.configuration.getProperty("db.database"));
			put("port",   Play.configuration.getProperty("db.port"));
		}};
		return map;
	}
	
	public static String changePlanName(AccountType accountType){ 
        return changePlanName(accountType.name());
	}
	
	public static String changePlanName(String accountType){
        String asignPlan = accountType;  
        if(accountType.equals(AccountType.Lite.name()))
                asignPlan = "Business";
        else if(accountType.equals(AccountType.Business.name()))
                asignPlan ="Pro";

        return asignPlan;
	}

	public static List<String> listOfYears(){
		int year = new DateTime().getYearOfEra();
		List<String> years = new LinkedList<String>();  
		for(int i=0;i<21;i++){
			years.add(String.valueOf((year+i)));
		}  
		return years;
	}
	
	public static int generateRandomNumbers(int length) {
		String nums = "";

        Random randomGenerator = new Random(); 
        while(true) {
        	if(nums.length()>=length) 
        		break;
        	
            nums += randomGenerator.nextInt(10000); 
        }
        return Integer.valueOf(nums.substring(0, length));
	}
	
	/*public static byte[] decode(char[] in)  
    {  
        int iLen = in.length;  
        if (iLen % 4 != 0)  
            throw new IllegalArgumentException(  
            "Length of Base64 encoded input string is not a multiple of 4.");  
        while (iLen > 0 && in[iLen - 1] == '=')  
            iLen--;  
        int oLen = (iLen * 3) / 4;  
        byte[] out = new byte[oLen];  
        int ip = 0;  
        int op = 0;  
        while (ip < iLen)  
        {  
            int i0 = in[ip++];  
            int i1 = in[ip++];  
            int i2 = ip < iLen ? in[ip++] : 'A';  
            int i3 = ip < iLen ? in[ip++] : 'A';  
            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127)  
                throw new IllegalArgumentException(  
                "Illegal character in Base64 encoded data.");  
            int b0 = map2[i0];  
            int b1 = map2[i1];  
            int b2 = map2[i2];  
            int b3 = map2[i3];  
            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0)  
                throw new IllegalArgumentException(  
                "Illegal character in Base64 encoded data.");  
            int o0 = (b0 << 2) | (b1 >>> 4);  
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);  
            int o2 = ((b2 & 3) << 6) | b3;  
            out[op++] = (byte) o0;  
            if (op < oLen)  
                out[op++] = (byte) o1;  
            if (op < oLen)  
                out[op++] = (byte) o2;  
        }  
        return out;  
    }
	*/
	
	public static double MINIMUMPRICE =3.99;
	public static final String defaultFont = "SansSerif";
	public static final String POPULATE = "Populate";
	public static final String SUBSERVICE = "SubService";
	public static final String PREMIUM = "Premium";
	public static final String WIDGET_MESSAGE = Widget.class.getSimpleName() + " Message";
	public static final String ANALYTICS = "Analytics";
	public static final String WIDGET_TRACKER = "WidgetInstalled";
	public static final String SOCIAL_REVIEW = "Social Review";
	public static final String adminName = Play.configuration.getProperty("dropifi.admin.username"); 
	public static final String adminPassword = Play.configuration.getProperty("dropifi.admin.password");
	
	public static final String introName = Play.configuration.getProperty("dropifi.intromail.username"); 
	public static final String introEmail = Play.configuration.getProperty("dropifi.intromail.email");
	
	public static final String mailName = Play.configuration.getProperty("dropifi.mail.username");
	public static final String mailEmail = Play.configuration.getProperty("dropifi.mail.email");
	public static final String mailPassword = Play.configuration.getProperty("dropifi.mail.password");
	
	public static final String noreplyUserName = Play.configuration.getProperty("dropifi.noreply.username");
	public static final String noreplyEmail = Play.configuration.getProperty("dropifi.noreply.email");
	public static final String noreplyPassowrd = Play.configuration.getProperty("dropifi.noreply.password");
	
	public static final String teamUserName = Play.configuration.getProperty("dropifi.team.username");
	public static final String teamEmail = Play.configuration.getProperty("dropifi.team.email");
	public static final String teamPassowrd = Play.configuration.getProperty("dropifi.team.password");
	
	public static final String shopifiApikey = play.Play.mode.isDev()? Play.configuration.getProperty("dropifi.shopifyDev.apikey"):Play.configuration.getProperty("dropifi.shopify.apikey");
	public static final String shopifiSecretKey =play.Play.mode.isDev()? Play.configuration.getProperty("dropifi.shopifyDev.secretkey"):Play.configuration.getProperty("dropifi.shopify.secretkey");
	
	public static final String AWSAccessKey =  Play.configuration.getProperty("aws.access.key"); 
	public static final String AWSSecretKey = Play.configuration.getProperty("aws.secret.key"); 
	
	public static final String RackspaceUsername =  Play.configuration.getProperty("rackspace.username"); 
	public static final String RackspaceApikey = Play.configuration.getProperty("rackspace.apikey"); 
	
	public static final String StripeSecretKey = play.Play.mode.isDev()?Play.configuration.getProperty("stripe.dev.secret.key") : Play.configuration.getProperty("stripe.secret.key");  
	public static final String StripePublisahblekey =play.Play.mode.isDev()?Play.configuration.getProperty("stripe.dev.publishable.key") : Play.configuration.getProperty("stripe.publishable.key");
	public static final String StripeAdditionalId = Play.configuration.getProperty("stripe.additionalId.key");
	
	public static final String AWSWidgetTabBucketProd = play.Play.configuration.getProperty("aws.bucket.widgettab.prod");
	public static final String AWSWidgetTabBucketDev = play.Play.configuration.getProperty("aws.bucket.widgettab.dev");
	
	public static final String AWSUserFileBUCKETNAME = play.Play.mode.isDev()?"dropifi_user_files_dev":"dropifi_user_files";
	public static final String AWSUserLogoBUCKETNAME = play.Play.mode.isDev()?"dropifi_user_logo_dev":"dropifi_user_logo";
	
	public static final String MixpanelDevKey = play.Play.configuration.getProperty("dropifi.mixpanel.DevAPI");
	public static final String MixpanelProdKey = play.Play.configuration.getProperty("dropifi.mixpanel.ProdAPI");
	public static final String MIXPANEL_TOKEN = (play.Play.mode.isDev())?DropifiTools.MixpanelDevKey:DropifiTools.MixpanelProdKey;
	
	public static final String WixApikey = play.Play.mode.isDev()?play.Play.configuration.getProperty("wix.dev.apikey"):play.Play.configuration.getProperty("wix.apikey");
	public static final String WixSecretkey = play.Play.mode.isDev()?play.Play.configuration.getProperty("wix.dev.secretkey"):play.Play.configuration.getProperty("wix.secretkey");
	public static final String WixVendorProductId = play.Play.configuration.getProperty("wix.vendorProductId");
	
	public static final String TictailClientId =   play.Play.mode.isDev()?Play.configuration.getProperty("tictail.dev.clientId"):Play.configuration.getProperty("tictail.clientId"); 
	public static final String TictailClientSecret= play.Play.mode.isDev()?Play.configuration.getProperty("tictail.dev.clientSecret"):Play.configuration.getProperty("tictail.clientSecret");

	
	public static final String MailchimpApikey = play.Play.mode.isDev()?play.Play.configuration.getProperty("mailchimp.dev.apikey"):play.Play.configuration.getProperty("mailchimp.prod.apikey");
	public static final String MailchimpListId = play.Play.mode.isDev()?play.Play.configuration.getProperty("mailchimp.dev.listId"):play.Play.configuration.getProperty("mailchimp.prod.listId");
	public static final String MailchimpNewsListId = play.Play.mode.isDev()?play.Play.configuration.getProperty("mailchimp.dev.newsletterListId"):play.Play.configuration.getProperty("mailchimp.prod.newsletterListId");
	
	public static final String LymbixApiKey =  Play.configuration.getProperty("lymbix.apikey"); 
	public static final String LymbixSecretKey = Play.configuration.getProperty("lymbix.secretkey");
	
	public static final String RepustateApiKey =  Play.configuration.getProperty("repustate.apikey"); 
	
	public static final String AlchemyApiKey =  Play.configuration.getProperty("alchemy.apikey"); 
	
	public static final String TextalyticsApiKey =  Play.configuration.getProperty("textalytics.apikey"); 
	
	public static final String FullcontactApiKey =  Play.configuration.getProperty("fullcontact.apikey"); 
	public static final String FullcontactSecretKey = Play.configuration.getProperty("fullcontact.secretkey");
	
	public static final String InboxUpdater = Play.configuration.getProperty("dropifi.inboxupdate");
	
	public static final String ServerURL = play.Play.mode.isDev()?play.Play.configuration.getProperty("dropifi.dev.url"):play.Play.configuration.getProperty("dropifi.prod.url");
	
	public static final int MaxmindUserId = Integer.valueOf(Play.configuration.getProperty("maxmind.userid")); 
	public static final String MaxmindLicenseKey = Play.configuration.getProperty("maxmind.license_key");
	/**
	 * Absolute location to a json file which contains dropifi extra configuration settings which is not found in the application.conf file
	 */
	public static final String SystemConfigFile = Play.getFile("").getAbsolutePath() + File.separator + "conf"+ File.separator + "json" + File.separator + "system_config.json";

	public static int getEmailCheckTime(String mailServer){
		try{ return Integer.valueOf(Play.configuration.getProperty("emailCheckTime."+mailServer.replace(" ", "_")));
		}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);return 15;}
	}
	
	
}
