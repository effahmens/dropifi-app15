package resources;
import org.apache.commons.lang.StringEscapeUtils; 
import org.bouncycastle.i18n.filter.SQLFilter;

import play.utils.HTML;

import java.util.HashMap;

/**
 * Removes all illegal SQL characters which may harm the database
 * @author phillips
 *
 */
public class SQLInjector {
	
	public static String escape(String value){
		//String escapedValue = StringEscapeUtils.escapeHtml(value);
		String escapedValue = StringEscapeUtils.escapeSql(value);
		return escapedValue; 
	}
	
	public static String escapeValue(String value){
		SQLFilter filter= new SQLFilter();
		return filter.doFilter(value);
	}
	

}
