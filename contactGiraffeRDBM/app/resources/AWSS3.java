package resources;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;


import play.db.jpa.Blob; 


public class AWSS3 {
	
	private static String AWS_ACCESS_KEY=  DropifiTools.AWSAccessKey;
	private static String AWS_SECRET_KEY = DropifiTools.AWSSecretKey; 

	public static AmazonS3 GetS3Client(){ 		
		//AmazonS3 s3Client = new AmazonS3Client( new BasicAWSCredentials( AWS_ACCESS_KEY, AWS_SECRET_KEY) ); 
		 		 
		AmazonS3 s3Client = new AmazonS3Client(new AWSCredentials() { 
			@Override 
			public String getAWSSecretKey() {
				// TODO Auto-generated method stub
				return AWS_SECRET_KEY;
			}
			
			@Override
			public String getAWSAccessKeyId() {
				// TODO Auto-generated method stub
				return AWS_ACCESS_KEY;
			}
		});
		
		return s3Client; 		 
	}
	
	public static Bucket createBucket(String name){ 		
		//check if bucket name does not exist
		Bucket bucket = GetS3Client().createBucket(name);
		return bucket;
	}
	
	public static PutObjectResult createFolder(String bucketName, String folderName,File file){
		String key = folderName+File.separator;		
		PutObjectRequest request = new PutObjectRequest(bucketName, key, file);
		 
		AmazonS3 client = GetS3Client();
		return client.putObject(request);		
		 
	}
	
	public static PutObjectResult putFile(String bucketName, String folderName,File file){
		String key = folderName+File.separator+file.getName();		
		return putFileByKey(bucketName,key,file);	 
	}
	
	public static PutObjectResult putPublicFile(String bucketName, String folderName,File file,String fileName){
		String key = folderName+File.separator+fileName;	//file.getName(); 	
		return putPublicFileByKey(bucketName,key,file);	 
	}
	
	public static PutObjectResult putFile(String bucketName, String folderName,String fileName,File file){
		String key = folderName+File.separator+fileName;		
		return putFileByKey(bucketName,key,file);		 
	}
	
	public static PutObjectResult putFileByKey(String bucketName, String key,File file){		 
		PutObjectRequest request = new PutObjectRequest(bucketName, key, file); 	
		AmazonS3 client = GetS3Client();
		return client.putObject(request);		 
	}
	
	public static PutObjectResult putPublicFileByKey(String bucketName, String key, File file){		 
		PutObjectRequest request = new PutObjectRequest(bucketName, key, file).withCannedAcl(CannedAccessControlList.PublicRead); 
		AmazonS3 client = GetS3Client(); 
		return client.putObject(request);		 
	}
	public static PutObjectResult putFile(String bucketName, String folderName,String filname,InputStream input,String contentType) throws IOException{
		String key = folderName+File.separator+filname;		 
		return putFile(bucketName, key, input,contentType);	 
	}
	
	public static PutObjectResult putFile(String bucketName, String key,InputStream input,String contentType) throws IOException{		 
		ObjectMetadata object = new ObjectMetadata();
		object.setContentType(contentType);
		object.setContentLength(input.available());
		PutObjectRequest request = new PutObjectRequest(bucketName,key,input,object);
		 
		AmazonS3 client = GetS3Client();
		PutObjectResult r = client.putObject(request); 		 	
		return r;
	}
	
	public static S3Object getS3Object(String bucketName,String folderName, String filename){
		String key = folderName+File.separator+filename;		 
		return getS3Object(bucketName, key);
	}
	
	public static S3Object getS3Object(String bucketName,String key){		 
		S3Object object = GetS3Client().getObject(new GetObjectRequest(bucketName, key));
		return object;
	}
	
	public static Blob getBlob(S3Object object){
		Blob blob = new Blob();
		blob.set(object.getObjectContent(), object.getObjectMetadata().getContentType());
		return blob;
	}
	
	public static Blob getBlob(String bucketName,String folderName, String filename){
		S3Object object = getS3Object(bucketName, folderName, filename);
		return getBlob(object);
	}
	
	public static void deleteFile(String bucketName, String folderName, String fileName){		 
		String key = folderName+File.separator+fileName;
		deleteFile(bucketName, key);
	}
	
	public static void deleteFile(String bucketName, String key){
		AmazonS3 client = GetS3Client(); 		 
		client.deleteObject(new DeleteObjectRequest(bucketName, key));
	}
	
	public static  List<S3ObjectSummary> getFileList(String bucketName, String folderName){
		 ListObjectsRequest request = new ListObjectsRequest();
		 request.setBucketName(bucketName);
		 request.setMarker(folderName);
		 ObjectListing  objectList = GetS3Client().listObjects(request);		 
		 return objectList.getObjectSummaries();
	}
	
	public static List<S3ObjectSummary> getFileList(String bucketName){
		 ListObjectsRequest request = new ListObjectsRequest();
		 request.setBucketName(bucketName);		 
		 ObjectListing  objectList = GetS3Client().listObjects(request);		 
		 return objectList.getObjectSummaries();
	}
	
	public static URL makePublic(String bucketName,String folderName, String fileName){
		String key = folderName+File.separator+fileName;		 
		return makePublic( bucketName, key); 
	}
	
	public static URL makePublic(String bucketName,String key){
		AmazonS3 client = GetS3Client();
		return client.generatePresignedUrl(new GeneratePresignedUrlRequest(bucketName, key)); 
	}
	
	public static CopyObjectResult CopyFile(String sourceBucketName, String sourceKey, String destinationBucketName,String destinationKey){
		CopyObjectRequest request = new CopyObjectRequest(sourceBucketName, sourceKey, destinationBucketName, destinationKey);
		return GetS3Client().copyObject(request);		
	} 
	
	public static void renameFile(String sourceBucketName, String sourceKey, String destinationBucketName,String destinationKey){
		CopyObjectRequest request = new CopyObjectRequest(sourceBucketName, sourceKey, destinationBucketName, destinationKey).withCannedAccessControlList(CannedAccessControlList.PublicRead);
		GetS3Client().copyObject(request);		
		
		//Delete the original
		DeleteObjectRequest deleteRequest = new DeleteObjectRequest(sourceBucketName,sourceKey);		       
		GetS3Client().deleteObject(deleteRequest); 
	}
	
}
