package resources;

import java.io.Serializable;

import models.WixDefault;

public class WixDefaultSerializer implements Serializable{
	public String instanceId;
	public String userId;
	public String compId;
	public String origCompId;
	
	public WixDefaultSerializer(String instanceId, String userId, String compId, String origCompId) {
		super();
		this.instanceId = instanceId;
		this.userId = userId;
		this.compId = compId;
		this.origCompId = origCompId;
	}
	
	public WixDefaultSerializer(WixDefault wix){
		this(wix.getInstanceId(),wix.getUserId(),wix.getCompId(),wix.getOrigCompId());
	}
}
