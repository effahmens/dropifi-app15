package com.shopify.connectors;

import java.sql.Timestamp;
import java.util.HashMap;

import com.shopify.api.APIAuthorization;
import com.shopify.api.client.ShopifyClient;
import com.shopify.api.credentials.Credential;
import com.shopify.api.endpoints.ShopsService;
import com.shopify.api.resources.Shop;

public class ShopifyAPIConnect {
	
	private HashMap<String, String> responseParams;
	private APIAuthorization auth;
	public HashMap<String, String> getResponseParams() {
		return responseParams;
	}

	public void setResponseParams(HashMap<String, String> responseParams) {
		this.responseParams = responseParams;
	}

	public APIAuthorization getAuth() {
		return auth;
	}

	public void setAuth(APIAuthorization auth) {
		this.auth = auth;
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	private Credential credential;

	public ShopifyAPIConnect(String apiKey, String sharedSecret,String  shopName,String t,String timestamp,String signature) throws Exception{
		this.setUp(apiKey, sharedSecret, shopName, t, timestamp, signature);
	}
	
	protected void setUp(String apiKey, String sharedSecret,String  shopName,String t,String timestamp,String signature) throws Exception {
		 			
		responseParams = new HashMap<String, String>(); 
		responseParams.put("shop", shopName);
		responseParams.put("t", t);
		responseParams.put("timestamp", timestamp);
		responseParams.put("signature", signature); 
		
        this.credential = new Credential(apiKey, sharedSecret, shopName, null); 		
		auth = new APIAuthorization(credential);		
		auth.computeAPIPassword(responseParams);
	}
	
	public boolean isValidShopifyResponse(){
		return auth.isValidShopifyResponse(responseParams);
	}
	
	/**
	 * Get the configuration of the shop account 
	 * @return
	 */
	public Shop getShop() {
		ShopifyClient client = new ShopifyClient(this.getCredential());		
		//request for the shop info
		ShopsService myShop = client.constructService(ShopsService.class);
		
		if(myShop != null){			
			try{
				return myShop.getShops();				 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
				return null;
			}			
		}
		return null;
	}

}
