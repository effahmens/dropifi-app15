package com.shopify.api.resources;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
* This code has been machine generated by processing the single entry
* fixtures found from the Shopify API Documentation
*/
 
@JsonIgnoreProperties(ignoreUnknown=true)
public class CustomerGroup extends MGCustomerGroup {

}