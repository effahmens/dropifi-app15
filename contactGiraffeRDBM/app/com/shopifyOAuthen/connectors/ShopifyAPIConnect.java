package com.shopifyOAuthen.connectors;

import java.net.URI;
import java.sql.Timestamp;
import java.util.HashMap;

import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import resources.DropifiTools;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.shopifyOAuthen.api.APIAuthorization;
import com.shopifyOAuthen.api.client.ShopifyClient;
import com.shopifyOAuthen.api.credentials.Credential;
import com.shopifyOAuthen.api.endpoints.AuthAPI;
import com.shopifyOAuthen.api.endpoints.ShopsService;
import com.shopifyOAuthen.api.resources.Shop;

public class ShopifyAPIConnect {
	
	private HashMap<String, String> responseParams;
	private APIAuthorization auth;
	public HashMap<String, String> getResponseParams() {
		return responseParams;
	}

	public void setResponseParams(HashMap<String, String> responseParams) {
		this.responseParams = responseParams;
	}

	public APIAuthorization getAuth() {
		return auth;
	}

	public void setAuth(APIAuthorization auth) {
		this.auth = auth;
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	private Credential credential;

	public ShopifyAPIConnect(String apiKey, String sharedSecret,String  shopName,String code,String timestamp,String signature) throws Exception{
		this.setUp(apiKey, sharedSecret, shopName, code, timestamp, signature);
	}
		
	protected void setUp(String apiKey, String sharedSecret,String  shopName,String code,String timestamp,String signature) throws Exception {
		 			
		responseParams = new HashMap<String, String>(); 
		responseParams.put("shop", shopName);
		responseParams.put("code", code); 
		responseParams.put("timestamp", timestamp);
		responseParams.put("signature", signature); 
		
        this.credential = new Credential(apiKey, sharedSecret, shopName, null); 		
		auth = new APIAuthorization(credential); 
		auth.computeAPIPassword(responseParams); 
	}
	
	public boolean isValidShopifyResponse(){
		return auth.isValidShopifyResponse(responseParams);
	}
	
	/**
	 * Get the configuration of the shop account 
	 * @return
	 */
	public Shop getShop(){	
		//request for the shop info
		ShopsService myShop = GetClient().constructService(ShopsService.class); 
		if(myShop != null){			
			try{
				return myShop.getShops();				 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
				return null;
			}			
		}
		return null;
	}
	
	public ShopifyClient GetClient(){
		return new ShopifyClient(this.getCredential(),responseParams.get("code"));
	}

}
