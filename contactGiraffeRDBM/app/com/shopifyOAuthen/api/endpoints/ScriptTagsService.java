/**
*
*
**/
// Generated On: 2011-09-01T02:13:38-04:00
package com.shopifyOAuthen.api.endpoints;

import java.util.LinkedHashMap;
import java.util.List;

import org.codegist.crest.annotate.ContextPath;
import org.codegist.crest.annotate.Destination;
import org.codegist.crest.annotate.EndPoint;
import org.codegist.crest.annotate.HttpMethod;
import org.codegist.crest.annotate.Name;
import org.codegist.crest.annotate.ResponseHandler;
import org.codegist.crest.annotate.Path;

import static org.codegist.crest.HttpMethod.POST;
import static org.codegist.crest.HttpMethod.PUT;
import static org.codegist.crest.HttpMethod.DELETE;
import static org.codegist.crest.config.Destination.BODY;

import com.shopifyOAuthen.api.handlers.ShopifyResponseHandler;
import com.shopifyOAuthen.api.resources.ScriptTag;
 
@EndPoint("")
@ContextPath("/admin/script_tags")
@ResponseHandler(ShopifyResponseHandler.class)
public interface ScriptTagsService extends BaseShopifyService {

    // GET
    @Path(".json")
    List<ScriptTag> getScriptTags();

    @Path(".json?{0}")
    List<ScriptTag> getScriptTags(String queryParams);
    
    @Path(".json?src={0}")
    List<LinkedHashMap> getScriptTagsBySrc(String src);

    
    @Path("/{0}.json")
    ScriptTag getScriptTag(int id);

    @Path("/{0}.json?{1}") 
    ScriptTag getScriptTag(int id, String queryParams);

    @Path("/count.json")
    int getCount();

    @Path("/count.json?{0}")
    int getCount(String queryParams);

    // POST
    @Path(".json")
    @HttpMethod(POST)    
    ScriptTag createScriptTag(@Destination(BODY) @Name("script_tag") ScriptTag script_tag);

    // PUT
    @Path("/{0}.json")
    @HttpMethod(PUT)
    ScriptTag updateScriptTag(int id, @Destination(BODY) @Name("script_tag") ScriptTag script_tag); 

    // DELETE
    @Path("/{0}.json") 
    @HttpMethod(DELETE) 
    void deleteScriptTag(int id); 
}
