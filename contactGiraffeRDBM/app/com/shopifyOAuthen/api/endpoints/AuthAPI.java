package com.shopifyOAuthen.api.endpoints;

import java.net.URI;
import java.util.HashMap;
import java.util.List;

import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;

import com.shopifyOAuthen.api.APIAuthorization; 
import com.shopifyOAuthen.api.resources.ShopifyResource;

public class AuthAPI extends API {
	private static final String AUTH_API_ENDPOINT = "oauth/authorize";
	private static final String ACCESS_TOEKN_ENDPOINT = "oauth/access_token";
	
	public AuthAPI(APIAuthorization auth) {
		super(auth); 
		setContentTypeExtension(APIResponseContent.NONE);
	}

	@Override
	public ShopifyResource findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ShopifyResource> findByQuery(HashMap<String, String> queryParams) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ShopifyResource> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String getAPIEndpoint() {
		return AUTH_API_ENDPOINT;
	}
	
	public String getACCESSTOEKNENDPOINT() {
		return ACCESS_TOEKN_ENDPOINT;
	}
	
	public URI getAuthRequestURI() {
		HashMap<String, String> params = new HashMap<String, String>(){{
			put("client_id", getApiAuth().getCredential().getApiKey());
			put("scope", "write_script_tags,write_products,write_customers");
		}};
		return constructURI(AUTH_API_ENDPOINT,params); 
	}
	
	public URI getAccessTokenURI(final String code) {
		HashMap<String, String> params = new HashMap<String, String>(){{
			put("client_id", getApiAuth().getCredential().getApiKey());
			put("client_secret", getApiAuth().getCredential().getSharedSecret());
			put("code", code);
		}};
		return constructURI(ACCESS_TOEKN_ENDPOINT,params);  
	}
	
	public String getAccessToken(final String code){
		WSRequest request = WS.url(getAccessTokenURI(code).toString());
		
		HttpResponse response = request.post();
		return response.getJson().getAsJsonObject().get("access_token").getAsString();
	}

}
