package com.shopifyOAuthen.api.interceptors;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
 

public class ShopifyRequestInterceptor implements HttpRequestInterceptor
{
	private ObjectMapper mapper;
	private String ACCESS_TOKEN="";
	public ShopifyRequestInterceptor()
	{
		mapper = new ObjectMapper();
	}
	
	public ShopifyRequestInterceptor(String ACCESS_TOKEN)
	{ 
		mapper = new ObjectMapper();
		this.ACCESS_TOKEN =ACCESS_TOKEN;
	}
 
	@Override
	public void process(HttpRequest request, HttpContext context)
			throws HttpException, IOException
	{
		if (request instanceof HttpEntityEnclosingRequest) {
			HttpEntityEnclosingRequest entity_request = (HttpEntityEnclosingRequest)request;
			HttpEntity entity = entity_request.getEntity();
			if (URLEncodedUtils.isEncoded(entity)) {
				JsonParser parse = new JsonParser();
				JsonNode rootNode = mapper.createObjectNode();
				JsonObject json = new JsonObject();
				 
				for (NameValuePair field: URLEncodedUtils.parse(entity)) { 					
					json.add(field.getName(),parse.parse(field.getValue()));
					((ObjectNode) rootNode).put(field.getName(), mapper.valueToTree(field.getValue()));					
				}  				
				
				entity = new StringEntity(json.toString());
				((StringEntity) entity).setContentType("application/json");				 
				entity_request.setEntity(entity);
				entity_request.setHeader(entity.getContentType());
				entity_request.setHeader("Content-Length", Long.toString(entity.getContentLength()));
			}
		}
		request.addHeader("X-Shopify-Access-Token", ACCESS_TOKEN);
	}

}
