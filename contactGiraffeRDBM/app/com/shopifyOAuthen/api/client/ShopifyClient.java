package com.shopifyOAuthen.api.client; 

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.codegist.crest.CRest;
import org.codegist.crest.CRestBuilder;
import org.codegist.crest.HttpClientRestService;

import com.apache.commons.codec.binary.Base64;
import com.shopifyOAuthen.api.APIAuthorization;
import com.shopifyOAuthen.api.credentials.Credential;
import com.shopifyOAuthen.api.endpoints.BaseShopifyService;
import com.shopifyOAuthen.api.endpoints.EndpointImpl;
import com.shopifyOAuthen.api.resources.Shop;
import com.shopifyOAuthen.api.resources.ShopifyResource;
import com.shopifyOAuthen.api.resources.json.ShopifyRequestWriter;
import com.shopifyOAuthen.api.resources.json.ShopifyResponseReader;


public class ShopifyClient {
	private CRest crestClient;
	private APIAuthorization auth;
	private Credential creds;
	private ShopifyResponseReader reader = new ShopifyResponseReader();
	private ShopifyRequestWriter writer = new ShopifyRequestWriter();
	private String ACCESS_TOKEN = ""; 

	public ShopifyClient(Credential creds) {
		this.creds = creds;
		this.auth = new APIAuthorization(this.creds);
		crestClient = new CRestBuilder()
						.expectsJson()
						.setRestService(constructClientRestService())
						.overrideDefaultConfigWith(constructConfiguration())
						.build();
	} 
	
	public ShopifyClient(Credential creds,String ACCESS_TOKEN) {
		this.ACCESS_TOKEN = ACCESS_TOKEN;
		this.creds = creds;
		this.auth = new APIAuthorization(this.creds);  
		crestClient = new CRestBuilder() 
						.expectsJson()
						.setRestService(constructClientRestService()) 
						.overrideDefaultConfigWith(constructConfiguration())
						.build();
	} 

	private HashMap<String, String> constructConfiguration(){
		return new HashMap<String, String>(){{
			put("service.end-point", getEndpoint());
		}};
	}
	
	public String getEndpoint() {
		return "https://"+creds.getShopName()+(creds.getShopName().contains(".myshopify.com")?"":".myshopify.com");
	}
	
	private HttpClientRestService constructClientRestService() {
		return new HttpClientRestService(auth.getAuthorizedClient(this.ACCESS_TOKEN ));
	}
	
	public <T extends BaseShopifyService> T constructService(Class<T> interfaze){	 
		if(interfaze.isInterface()) {
			return constructInterface(interfaze); 
		} else {
			return constructEndpointImpl(interfaze);
		}
	}
	
	public <T extends BaseShopifyService> T constructInterface(Class<T> interfaze){
		return crestClient.build(interfaze);
	}
	
	public <T extends BaseShopifyService> T constructEndpointImpl(Class<T> clazz) {
		try {
			EndpointImpl instance = (EndpointImpl)clazz.newInstance();
			instance.setEndpoint(getEndpoint());
			instance.setHttpClient(auth.getAuthorizedClient(this.ACCESS_TOKEN));
			
			T service = constructService((Class<T>)instance.getServiceClass());
			instance.setServiceInterface(service);
			
			return (T) instance;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public <T extends ShopifyResource> List<T> handleResponse(InputStream in, Class<T> resource){
		return reader.read(new InputStreamReader(in), resource);
	}
	
	public <T extends ShopifyResource> String convertToJson(T object) throws IOException {
		StringWriter w = new StringWriter();
		writer.write(w, object);
		return w.toString();
	}

	
	

}
