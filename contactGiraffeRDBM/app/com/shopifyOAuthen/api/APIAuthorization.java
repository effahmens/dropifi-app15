package com.shopifyOAuthen.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.ParseException;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

import com.apache.commons.codec.binary.Hex;
import com.shopifyOAuthen.api.endpoints.AuthAPI;
import com.shopifyOAuthen.api.interceptors.ShopifyPostRequestInterceptor;
import com.shopifyOAuthen.api.interceptors.ShopifyRequestInterceptor;
import com.shopifyOAuthen.api.interceptors.ShopifyResponseInterceptor;
import com.shopifyOAuthen.api.credentials.ShopifyCredentialsStore;
import com.shopifyOAuthen.api.credentials.Credential; 

public class APIAuthorization {
	private static final String SIGNATURE = "signature";
	private static final String TOKEN = "code"; 
	 
	private Credential credential;
	private ShopifyCredentialsStore credentialsStore = null;
	private HttpClient client;
	
	public APIAuthorization(Credential credential) {
		this.credential = credential;
	}
	
	public APIAuthorization(ShopifyCredentialsStore store, String shop) throws Exception {
		this(store.loadCredential(shop));
		this.credentialsStore = store;
	}
	
	public boolean isValidShopifyResponse(HashMap<String, String> responseParameters) {
		String signature = responseParameters.remove(SIGNATURE);
		
		String preDigest = generatePreDigest(responseParameters);
		return signature.equals(toMD5Hexdigest(preDigest));
	}
	
	String generatePreDigest(HashMap<String, String> responseParameters) {
		ArrayList<String> sortedKeys = new ArrayList<String>(responseParameters.keySet());
		Collections.sort(sortedKeys);
		
		StringBuilder preDigest = new StringBuilder(credential.getSharedSecret());
		for(String key : sortedKeys) {
			preDigest.append(key);
			preDigest.append("=");
			preDigest.append(responseParameters.get(key));
		}
		return preDigest.toString();
	}
	
	public boolean computeAPIPassword(HashMap<String, String> responseParameters) throws Exception {
		if(isValidShopifyResponse(responseParameters)) {
			StringBuilder builder = new StringBuilder();
			builder.append(credential.getSharedSecret());
			builder.append(responseParameters.get(TOKEN));
			credential.setPassword(toMD5Hexdigest(builder.toString()));
			if (credentialsStore != null) {
				credentialsStore.saveCredential(credential);
			}
			return true;
		}
		return false;
	}
	
	public String toMD5Hexdigest(String message) {
		if(message != null) {
			try {
				MessageDigest digest = MessageDigest.getInstance("MD5");
				byte[] digestBytes = digest.digest(message.getBytes());
				
				return Hex.encodeHexString(digestBytes);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		return "";
	}
	
	public URI generateAuthRequest() {
		AuthAPI apiAuthRequest = new AuthAPI(this);
		HashMap<String, String> params = new HashMap<String, String>(){{
			put("client_id", credential.getApiKey());
		}};
		URI authEndpoint = apiAuthRequest.constructURI(params);
		return authEndpoint;
	}
	
	public HttpClient getAuthorizedClient(String Access_token) {
		return getAuthorizedClient(credential.getShopName() +(credential.getShopName().contains(".myshopify.com")?"":".myshopify.com"), 443,Access_token);
	}
	
	public HttpClient getAuthorizedClient(String hostName, int port, final String Access_token) {
		if(this.client == null) {	 
			DefaultHttpClient client = new DefaultHttpClient(); 
			client.addRequestInterceptor(new ShopifyRequestInterceptor(Access_token));
			client.addResponseInterceptor(new ShopifyResponseInterceptor()); 
			AuthScope scope = new AuthScope(hostName, port);  
			
			UsernamePasswordCredentials creds = new UsernamePasswordCredentials(credential.getApiKey(), credential.getPassword());
			client.getCredentialsProvider().setCredentials(scope, creds); 	
			this.client = client; 			 
		}
		 
		return this.client; 
	}
	
	public URLConnection testAuth() throws MalformedURLException{
		URL url = new URL("");
		URLConnection client = new URLConnection(url) {
			
			@Override
			public void connect() throws IOException {
				// TODO Auto-generated method stub
				 
			}
		};
		
		return client;
	}
	
	public boolean isAuthorized() { return credential.getPassword() != null; }

	public Credential getCredential() {
		return credential;
	}
	
	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
	public ShopifyCredentialsStore getCredentailsStore() {
		return credentialsStore;
	}
	
	public void setCredentialStore(ShopifyCredentialsStore credentialsStore) {
		this.credentialsStore = credentialsStore;
	}
}
