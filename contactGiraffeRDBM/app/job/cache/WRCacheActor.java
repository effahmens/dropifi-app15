package job.cache;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonArray;

import play.cache.Cache;
import resources.CacheKey;
import resources.DCON;
import view_serializers.RuleSerializer;

import akka.actor.UntypedActor;

/**
 * Widget Rule Cache Actor: asynchronous caching of widget rules created by user
 */
public class WRCacheActor extends UntypedActor{

	@Override
	public void onReceive(Object object) throws Exception{
		//TODO Auto-generated method stub 
		
	    if(object instanceof Map<?,?>) {
			 HashMap<String, Object> map = (HashMap<String, Object>)object;			 
			 String type =(String) map.get("type");
			 long companyId = (long)map.get("companyId");
			 String cacheKey = (String) map.get("key");
			 Connection conn = null;	 
			 try{
				 conn = DCON.getDefaultConnection();
				 switch(type){
					 case "LPOP1":
						 JsonArray jsonArray = RuleSerializer.toWidgetJsonArray(conn,companyId);
						 Cache.set(cacheKey,jsonArray.toString()); 
						 play.Logger.log4j.info("In here out there");
						 break;
				 }
			 }catch(Exception e){
				 
			 }finally{
				 if(conn!= null)
					 conn.close();
			 }
		 }
	    
	    if(this !=null && this.getContext()!=null)
    		 this.getContext().stop();
	} 

}
