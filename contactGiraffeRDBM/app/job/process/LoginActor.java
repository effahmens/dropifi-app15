package job.process;

import models.AccountUser;
import resources.WidgetRespSerializer;
import view_serializers.LoginSerializer;
import akka.actor.UntypedActor;

public class LoginActor extends UntypedActor{

	@Override
	public void onReceive(Object object) throws Exception {
		// TODO Auto-generated method stub
		if(object instanceof LoginSerializer){ 
			LoginSerializer login = (LoginSerializer)object;
			AccountUser.updateLoginDate(login.getEmail(), login.getApikey()); 
		}
		
		if(this !=null && this.getContext()!=null)
	 		 this.getContext().stop();
	}

	
}
