package job.process.Scheduler;

import java.util.concurrent.TimeUnit;  

import javax.ws.rs.ApplicationPath;

import job.process.InboundActor;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import resources.InboundSerializer;
import akka.actor.Actor;
import akka.actor.ActorRef; 
import akka.actor.UntypedActorFactory;
import akka.util.FiniteDuration;
import akka.actor.Scheduler;
import static akka.actor.Actors.actorOf;

//@OnApplicationStart
public class MailboxScheduler extends Job{ 
	@Override
	public void doJob(){  
		//play.Logger.log4j.info("Initial scheduler started");
		Scheduler.schedule(actorOf(InboundActor.class).start(), new InboundSerializer(), 0,10, TimeUnit.SECONDS);
	}
}
