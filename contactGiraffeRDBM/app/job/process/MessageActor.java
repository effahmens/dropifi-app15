package job.process;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import query.inbox.InboxSelector;
import resources.APIKEY;
import view_serializers.AttSerializer;
import resources.AccountType;
import resources.DCON;
import resources.DropifiTools;
import resources.EventTrackSerializer;
import resources.JPAHelper;
import resources.MsgSerializer;
import resources.RuleParameters;
import resources.SentimentSerializer;
import view_serializers.CoProfileSerializer;
import view_serializers.MbSerializer;
import models.*;
import adminModels.DropifiMailbox;
import adminModels.DropifiMailer;
import adminModels.MailingType;
import akka.actor.ActorRef;
import akka.actor.Actors;
import akka.actor.UntypedActor;
import api.sentiment.SentimentClient;
import resources.sentiment.SentimentBaseSerializer;
import resources.sentiment.SentinmentAPIType;
import resources.sentiment.lymbix.*;
import static akka.actor.Actors.actorOf; 

public class MessageActor extends UntypedActor { 	
	
	@Override
	public void onReceive(Object object) throws Exception { 
		
		/**
		 * 
		 * 
		 * Add business rules
		 * 
		 * 
		 */
		
		// TODO Auto-generated method stub
			
		if(object instanceof MsgSerializer) {	
			MsgSerializer msg = (MsgSerializer)object;
			Connection conn=null; 
	        try {
	        	conn = DCON.getDefaultConnection(); 
	        	/**
				  * Perform Rules on the widget 
				  */	 
	        	//Make a post request using any of the api source to determine the sentiment of the text
				SentimentClient sentiClient = new SentimentClient(DropifiTools.RepustateApiKey, SentinmentAPIType.Repustate, DropifiTools.removeHTML(msg.getTextBody()), "en");	 
				SentimentBaseSerializer sentiment =null;
				try{
					//sentiment = sentiClient.analyze();
				}catch(Exception e){
					 play.Logger.log4j.info(e.getMessage(),e);
				}
				
				//set the parameters to be used in comparing the rules
				DropifiMailbox mailbox = DropifiMailbox.findByIdentifier(conn, MailingType.Notification); 					
				MbSerializer mb = new MbSerializer(mailbox);
				
				RuleParameters params = new RuleParameters(msg, sentiment,mb,msg.getApikey(),null);
				//Set the source of the message as the pageUrl
				params.pageUrl = msg.getSourceOfMsg();
				
				Timestamp time = new Timestamp(new Date().getTime());
				params.created = time;
				params.messageTemplate = Widget.findMessageTemplate(conn, params.apikey);
				
				//set company profile parameters
				CoProfileSerializer profile = Company.findInfoByApikey(conn, msg.getApikey()); 
				profile.setCompanyId(msg.getCompanyId());
				params.compProfile = profile;
				
				//find all the widget rules
				List<MsgRule> rules = MsgRule.findWidgetMsgRules(conn, msg.getCompanyId(),msg.getApikey());
				MsgRule msgtRule = new MsgRule();
				Map<Boolean,Map<String, Object>> rule = msgtRule.msgActionPerformed(rules, msg.getCompanyId(), params);
				boolean rulePassed = rule.keySet().iterator().next();
				 
				/**
				  * end of rules on the inbound message
				 */
				
								
				if(rulePassed) {	        	
					//search for the default mailbox of the company
					MbSerializer cMb = InboundMailbox.findDefaultMailbox(conn, msg.getApikey());
					
					//set cMb to mb (default dropifi mailbox) if cMb is null:(user has no default mailbox)
					if(cMb == null){ 								 
						mb.setUsername(profile.getMainName()); 
						cMb = mb; 
						params.setAutoReplyTo(params.compProfile.getEmail(), params.compProfile.getUsername());
					}else{
						params.setAutoReplyTo(cMb.getEmail(),cMb.getUsername());
					} 
					
					params.companyMailbox = cMb;
					params.msgId = msg.getMsgId();
		        	DropifiMessage message = new DropifiMessage(msg); 
		        	 
		        	try{
						String intense = "";
						if(sentiment!=null && sentiment.getIntense()!=null){
							intense = sentiment.getIntense();
						}
						message.setSubjectToIntense(msg.getSubject(), intense);
					}catch(Exception e){
						play.Logger.log4j.info(e.getMessage(),e);
					}
		        	
		        	message.setCreated(params.created);
		        	
					int status = message.createWidgetMessage(conn);
					long countMsg =0l;
					if(status ==200){
						
						 //retrieve the id of the message
						Long messageId = DropifiMessage.getMId(conn,msg.getMsgId()); 
						SentimentSerializer senti = null;
						if(messageId!=null) { 
							//send a request for sentiment analysis to be saved to the database 
							 try{
								 ActorRef sentimentActor = actorOf(SentimentActor.class).start(); 			
								 senti = new SentimentSerializer(messageId,message.getEmail(),message.getCompanyId(),message.getSourceId(), message.getSourceOfMsg(), message.getTextBody(),sentiment);				
								 sentimentActor.tell(senti);  
							 }catch(Exception e){
								 play.Logger.log4j.info(e.getMessage(),e);
							 } 
						}
						
						 try{					 
							 CustomerMsgSource.saveSource(conn, message.getCompanyId(), message.getEmail(), message.getSourceOfMsg(), 1);
						 }catch(Exception e){
							 play.Logger.log4j.info(e.getMessage(),e); 
						 } 
						
						 //-------------------------------------------------------------------------------------------------------------------------------

						 try{	
		      				 if(message!=null){ 
		      					 MsgCounter.incNewCounter(conn,message.getCompanyId(), message.getSourceId(), message.getSourceOfMsg(),sentiment.getIntense());
		      				 }
		      				 
		      				 countMsg = message.getMailboxMessages(conn, message.getCompanyId());
		      			 }catch(Exception e){			      				 
		      				 play.Logger.log4j.info(e.getMessage(),e);
		      			 }

						 params.countMsg = countMsg;
						//create an address list of activated recipients of mailbox message
						Address[] reciAddress = WMsgRecipient.activeForwardRecipients(conn, msg.getApikey()); 
						params.setContactProfile(InboxSelector.findCustomerProfile(conn,params.companyId, params.sender));
						
						if(reciAddress != null && reciAddress.length>0){
			      			//send notification to the company as well as the recipients specified
		      				DropifiMailer.sendWidgetAutomaticNotification(conn,"", "",reciAddress,params); 				      		
		      			}else if(profile.getAccountType().equals(AccountType.Free.name()) && (reciAddress==null || reciAddress.length<=0)){
		      				/*No delivery email associated with the account. Send a notification to the user to upgrade to any of the
		      				 * paid plan to have full access to customization and other services*/
		      				reciAddress = new Address[1];
		      				reciAddress[0] = new InternetAddress(profile.getEmail(), profile.getUsername());
		      				DropifiMailer.sendWidgetAutomaticNotification(conn,"", "",reciAddress,params);
		      			}
						
						//execute the rule actions
		      			Map<String, Object> ruleAction = rule.get(rulePassed); 
		      			ruleAction.put("apikey", params.apikey);
		      			
		      			//send the actions to rule actor to execute the rule actions 
						ActorRef ruleActor = actorOf(RuleActor.class).start(); 
						ruleActor.tell(ruleAction);
						
						/*
						if(msg.getAttachments() != null){  
							 //save the file attached to the message to amazon S3
							 ActorRef attActor = actorOf(AttachmentActor.class).start();
							 for(AttSerializer att: msg.getAttachments()){	
								 att.id = messageId;						
								 attActor.tell(att);
							 } 		
						}
						*/ 
						
						if(countMsg>0){ 
							EventTrackSerializer event = new EventTrackSerializer();
							event.trackTotalMessages(DropifiTools.MIXPANEL_TOKEN, profile.getEmail(), countMsg,"Mailbox Messages");
						}
						
						InboxSelector.deleteAllCaches(profile,true);
					}
				}else{
					//Possible Reason: Stop Process Action
					play.Logger.log4j.info("Stop processing action encountered"); 
				} 			
	        }catch (Exception e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.error("Could save message: "+e.getMessage(),e);
			}finally{
				try{
					if(conn!=null)
						conn.close();
					
					
				}catch(Exception e){
					
				}
				if(this !=null && this.getContext()!=null)
		      		 this.getContext().stop();	
			} 	
	       
		}
		if(this !=null && this.getContext()!=null)
     		 this.getContext().stop();	
	}
	 
}	