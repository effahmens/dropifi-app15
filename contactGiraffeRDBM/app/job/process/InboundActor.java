package job.process; 

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import job.ConcurrentTask.EmailPullTask;
import models.InboundMailbox;
import resources.DCON;
import resources.EmailManager;
import resources.SchedulerSerializer;
import akka.actor.UntypedActor;

public class InboundActor extends UntypedActor{

	@Override
	public void onReceive(Object object) throws Exception {
		
		// TODO Auto-generated method stub
		if(object instanceof SchedulerSerializer){  
			Connection conn = null; 
			try{ 
				SchedulerSerializer sch = (SchedulerSerializer)object;
				if(sch!=null){ 
					//play.Logger.log4j.info("Check complete");
					conn = DCON.getDefaultConnection();  
					InboundMailbox mailbox = InboundMailbox.findMonitoredMailbox(conn, sch.id, sch.email);
					
					if(mailbox!=null && mailbox.isNextPull()){  
						//play.Logger.log4j.info("Testing mailbox 1");
						EmailManager manager = new EmailManager(mailbox,conn);
						manager.connect(conn);
					}/*else{ 
						if(mailbox!=null) {
							play.Logger.log4j.info("NextPullTime: "+mailbox.getNextPullTime().toString());
						}
					}*/
				} 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}finally{
				try{
					if(conn!=null)
						conn.close(); 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			 
			}
		
		}
		
		 if(this !=null && this.getContext()!=null){
	 			this.getContext().stop();
		 	}
		 
	}

}
