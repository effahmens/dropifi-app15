package job.process;

import java.sql.Connection;

import resources.DCON;
import resources.JPAHelper;
import resources.WidgetTrackerSerializer;
import view_serializers.MailSerializer;
import view_serializers.MbSerializer;
import adminModels.DropifiMailbox;
import adminModels.DropifiMailer;
import adminModels.MailingType;
import akka.actor.UntypedActor;

public class MailingActor extends UntypedActor{
 
	@Override
	public void onReceive(Object object) throws Exception {
		// TODO Auto-generated method stub 
		Connection conn =null;
		try{ 
			if(object instanceof MailSerializer) { 
				//JPAHelper.reset(); 
				
				MailSerializer mail = (MailSerializer)object;
				conn = DCON.getDefaultConnection();
				
				if(mail!= null){
					try{ 
						if(mail.getMailbox()==null){ 
							mail.setMailbox(DropifiMailbox.findByIdentifier(conn, MailingType.Support)); 
						} 
						DropifiMailer mailer = new DropifiMailer(new MbSerializer(mail.getMailbox()));
						
						//Set the campaign if the mailbox is mailgun
						if(mail.getMailbox().getMailserver().equals("Mailgun")){ 
							mailer.setMgCampaignId(play.Play.mode.isProd()?"dropifi_inapp_mail":"dropifi_inapp_dev");
						}
						
						mailer.sendSimpleMail(mail.getEmail(), mail.getName(),	mail.getSubject(),mail.getMessage(),null); 
						 
					}catch(Exception e){
						play.Logger.log4j.info(e.getMessage(),e);
					}
				}
	
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}finally{
			if(conn!=null)
				conn.close();
			
			if(this !=null && this.getContext()!=null)
	     		 this.getContext().stop();	
		}
		
	}

}
