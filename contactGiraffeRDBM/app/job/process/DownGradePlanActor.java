package job.process;

import java.sql.Connection;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import job.ConcurrentTask.DownGradePlanPullTask;
import models.Company;
import models.dsubscription.DownGradePlan;
import resources.DCON;
import resources.SchedulerSerializer;
import akka.actor.UntypedActor;

public class DownGradePlanActor extends UntypedActor{

	@Override
	public void onReceive(Object object) throws Exception {
		// TODO Auto-generated method stub
		
		if(object instanceof SchedulerSerializer){
			Connection conn = DCON.getDefaultConnection();
			try{
				//perform your job code here...				
				List<DownGradePlan> plans = Company.findAllPlanToDownGrade(conn);
				if(plans != null){
					int size = plans.size();
					play.Logger.log4j.info("DownGradePlan size : "+ size); 
					
					if(size>0){ 
						//create an instance of the DownGradePlanPullTask to execute each downgrade plan
						//on a separate thread  
						DownGradePlanPullTask pullPlan = new DownGradePlanPullTask(plans, 0, size);
						ForkJoinPool mainPool = new ForkJoinPool();		
						mainPool.invoke(pullPlan);
					}
				} 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}finally{
				conn.close();
			}
		}
	}

}
