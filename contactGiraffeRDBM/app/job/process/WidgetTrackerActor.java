package job.process;

import java.sql.Connection;

import play.cache.Cache;
import play.db.jpa.Transactional;

import models.Company;
import models.WiParameter;
import models.Widget;
import models.WidgetTracker;
import resources.AccountType; 
import resources.CacheKey;
import resources.DCON;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.EventTrackSerializer;
import resources.JPAHelper;
import resources.WidgetRespSerializer;
import resources.WidgetTrackerSerializer;
import resources.WixSerializer;
import akka.actor.UntypedActor;


public class WidgetTrackerActor extends UntypedActor{

	@Override 
	public void onReceive(Object object) throws Exception {
		//JPAHelper.reset();
		// TODO Auto-generated method stub
		if(object instanceof WidgetTrackerSerializer){ 
			//JPAHelper.begin();
			Connection conn = null;
			try{
				
				WidgetTrackerSerializer trackSer = (WidgetTrackerSerializer)object;
				if(trackSer!=null){
					conn = DCON.getDefaultConnection();
					WidgetTracker tracker = WidgetTracker.findByApikey(conn, trackSer.getApikey());
					
					if(tracker == null){
						//create a new track entry
						tracker = new WidgetTracker(trackSer.getApikey(), trackSer.getHost(),trackSer.getDomain());
						WidgetTracker.saveTracker(conn, tracker);
					}else{
						//update the existing track entry		
						tracker.setHost(trackSer.getHost());
						tracker.setDomain(trackSer.getDomain());
						WidgetTracker.updateTracker(conn, tracker);			 
					}
					
					/**
					 * Update the company site url
					 */
					
					Company.updateUrl(conn, trackSer.getApikey(), trackSer.getHost());
					String email = Company.findEmailByApikey(conn, trackSer.getApikey());
					
					EventTrackSerializer event = new EventTrackSerializer();
					event.trackUrl(DropifiTools.MIXPANEL_TOKEN, email, trackSer.getHost());
				}
			}catch(Exception e){
				//play.Logger.log4j.info(e.getMessage(),e);
				try{							
					if(conn!=null)
						conn.close(); 							
				}catch(Exception e1){
					//play.Logger.log4j.info(e1.getMessage(),e1);	
				}
			}finally{
				
				try{							
					if(conn!=null)
						conn.close();							
				}catch(Exception e){
					//play.Logger.log4j.info(e.getMessage(),e);	
				}

			 if(this !=null && this.getContext()!=null)
	      		 this.getContext().stop();	
			}
		}else if(object instanceof WixSerializer){
			try{
				WixSerializer wix = (WixSerializer)object;
				if(wix.instanceId!=null && wix.pluginType!=null && wix.pluginType.equals(EcomBlogType.Wix)){
					String cacheKey = CacheKey.genCacheKey(wix.apikey, CacheKey.Wix, "Premium_Package");
					String currentPlan = Cache.get(cacheKey, String.class);
					String plan = Company.wixPlan(wix.vendorProductId);
					
					if(currentPlan==null || !currentPlan.equals(plan)){
						JPAHelper.reset();
						JPAHelper.begin();
						Company found = Company.findByEcomBlogKey(wix.instanceId, EcomBlogType.Wix);
						String p = found.subscribeWixToPlan(wix.vendorProductId,wix.instanceId, EcomBlogType.Wix).getCurrentPlan().name();
						Cache.set(cacheKey, p);
						JPAHelper.commit();
						//play.Logger.log4j.info("Plan is not Cached");
					}/*else{
						play.Logger.log4j.info("Plan is Cached");
					}*/
				}  
			} catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);	
			}finally{ 

			 if(this !=null && this.getContext()!=null)
	      		 this.getContext().stop();	
			}
		}
			
	}

}
