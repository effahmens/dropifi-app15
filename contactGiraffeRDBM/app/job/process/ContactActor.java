package job.process;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.joda.time.DateTime;
import models.*;
import api.fullcontact.*;
import resources.*;
import akka.actor.UntypedActor;

public class ContactActor extends UntypedActor {

	@Override
	public void onReceive(Object object) throws Exception {
		JPAHelper.reset(); 
		/**
		 * Add business rules
		 */
		
		// TODO Auto-generated method stub
		Connection conn =  null;
		try{
			
			 
			if(object instanceof ContactSerializer){ 
				
				conn =  DCON.getDefaultConnection();  					
				ContactSerializer contact = (ContactSerializer) object;	
				
				CustomerProfile.fetchUpdateContactProfile(conn, contact,null);			
				//
				//
			}
		}catch(Exception e){
			play.Logger.log4j.info("Message: "+e.getMessage()); 
		}finally{
			if(conn != null){
				try{
					conn.close();
				}catch(Exception e){}
			}
			if(this !=null && this.getContext()!=null)
	     		 this.getContext().stop();	
		}
		
	}

	
}
