package job.process;

import java.sql.Connection;

import models.*;
import models.failed.SentimentFailed; 
import resources.*;
import akka.actor.UntypedActor; 
import resources.sentiment.lymbix.*;

public class SentimentActor extends UntypedActor {
	
	@Override
	public void onReceive(Object object) throws Exception {
		
		/**
		 * 
		 * 
		 * Add business rules here
		 * 
		 * 
		 * 
		 */
		
		// TODO Auto-generated method stub
 
		//check if the message object is an instance of SentimentSerializer 
		
		if(object instanceof SentimentSerializer){ 
			
			SentimentSerializer senti = (SentimentSerializer)object;			
			Connection conn = null; 
			 //add sentiment to message
			try{
	      	 if(senti !=null && senti.getMsgId()!=null) {
	      		
	      		 if((senti.getArticle() !=null) && (!senti.getArticle().isEmpty())){
	      			MsgSentiment sentiment = null; 
	      			conn = DCON.getDefaultConnection();
	      			 try{			      		 
	 			         //save the result to database
			        	 sentiment = new MsgSentiment(senti.getMsgId(),senti.getEmail(),senti.getCompanyId(),senti.getSourceOfMsg(),  senti.getSentiment());
			        	 sentiment.createSentiment(conn);  
			        	 	
			        	 play.Logger.info("Sentiment performed 1............................................."); 
			        	 
	      			 }catch(Exception err){
	      				 
	      				 try{      					 
				        	 //save the result to database
				        	 sentiment = new MsgSentiment(senti.getMsgId(),senti.getEmail(),senti.getCompanyId(),senti.getSourceOfMsg(),senti.getSentiment());
				        	 sentiment.createSentiment(conn);
				        	
				        	 play.Logger.info("Sentiment performed 2.............................................");
	      					 
	      				 }catch(Exception err1){
	      					 play.Logger.info(err1.getMessage(), err1);
	      				 }
	      			 }
	      			 
	      			try{
	      				
		      			SentimentType typeOfSenti = SentimentType.Neutral;
		      			if(sentiment !=null){
				        	 if(sentiment.getArticleSentiment().equalsIgnoreCase("Positive")){
				        		 typeOfSenti = SentimentType.Positive;
				        	 }else if(sentiment.getArticleSentiment().equalsIgnoreCase("Negative")){
				        		 typeOfSenti = SentimentType.Negative;
				        	 }else if(sentiment.getArticleSentiment().equalsIgnoreCase("NoSentiment")){
				        		 typeOfSenti = SentimentType.NoSentiment;
				        	 }
		      			}
		      			
			        	//add key words to message
			        	MsgKeyword.createKeywords(conn,senti.getCompanyId(), senti.getMsgId(), senti.getArticle(), typeOfSenti, senti.getSourceOfMsg(), sentiment.getCreatedDate());
	      			}catch(Exception e){
	      				play.Logger.info("Keyword problem",e.getMessage()); 
	      			}
	      			 
	      		 } 	        	
	      	 } 
			}catch(Exception e){
				
			}finally{
				if(conn!=null && !conn.isClosed()){
					try{conn.close();}catch(Exception e1){} 
				}
				
		      	 if(this !=null && this.getContext()!=null)
		      		 this.getContext().stop();	
			}
		}
	
	}

}
