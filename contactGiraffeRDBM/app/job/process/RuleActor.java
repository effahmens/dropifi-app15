package job.process;

import akka.actor.UntypedActor;

import java.sql.Connection;
import java.util.*;

import javax.mail.Address;

import resources.DCON;
import resources.JPAHelper;
import resources.RuleParameters;
import models.MainRule;
import models.RuleAction;
import models.WMsgRecipient;

public class RuleActor extends UntypedActor { 

	@Override
	public void onReceive(Object object) throws Exception {
		// TODO Auto-generated method stub
		//JPAHelper.reset(); 
		 Connection conn =null;
		try{
			
			if(object instanceof Map<?,?>) {
				
				Map<String,Object> ruleObject = (Map<String,Object>)object;
				if(ruleObject!=null){
					 List<RuleAction> actions = (List<RuleAction>) ruleObject.get("actions");
					 if(actions !=null){
						 Long companyId = (Long)ruleObject.get("companyId"); 
						 RuleParameters params = (RuleParameters)ruleObject.get("params");	
						 String apikey= (String)ruleObject.get("apikey");
						 
						 //fetch the list of active message recipient
						 conn = DCON.getDefaultConnection();
						 List<String> recipients = WMsgRecipient.findActiveRecipients(conn,apikey);						 						 
						 						 
						 MainRule.executeRuleAction(conn,actions, companyId, params,recipients);  
						 
					 }
				}
			} 		
			//stop the current actor after execution
			this.getContext().stop();	
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
			//stop the current actor after execution
			this.getContext().stop();
		}finally{
			if(conn!=null && !conn.isClosed())
				conn.close();
		}
		
	}

}
