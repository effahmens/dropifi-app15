package job.process;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import adminModels.DropifiMailbox;
import adminModels.MailingType;
import akka.actor.Actors;

import models.InboundMailbox;
import job.ConcurrentTask.EmailPullTask;
import play.jobs.*; 
import resources.MsgSerializer;
import view_serializers.MbSerializer;
import akka.actor.Scheduler;

//@Every("60s") 
public class InboundJob extends Job{
	
	/**
	   * Used to avoid running multiple instances of this job simultaneously.
	   */
	//private static final ReentrantLock runningLock = new ReentrantLock();
	//EmailPullTask pullEmail =null;
	 
	@Override
	public void doJob(){
		
		 // sb: try to acquire the lock. if the lock is in use, then just
	    // return immediately, because this job is already running.
	    // wait a half second before giving up.
		  try {    	   
			/*
			if (!runningLock.tryLock(250, TimeUnit.MILLISECONDS)) {
			  // maybe log a message here?
				play.Logger.log4j.info("running lock"); 
			  return;
			} 
			*/
		    
			//select all the inbound mailbox which are not currently pulling email			
			List<InboundMailbox> inbound = InboundMailbox.findAllUnLock(); 
			//findByIsLock(false); //InboundMailbox.findAll();			
			
			if(inbound != null){
				int size = inbound.size();
				//play.Logger.log4j.info("Mailboxes size: "+ size); 				
				if(size>0){  
					//create an instance of the EmailPullTask to execute each mailbox
					//on a separate thread 
					EmailPullTask pullEmail = new EmailPullTask(inbound, 0, inbound.size()); 
					if(pullEmail != null){					
						ForkJoinPool mainPool = new ForkJoinPool();					 
						mainPool.invoke(pullEmail); 						
						mainPool.shutdownNow();					 
					}
				}	
			} 
	  } catch (Exception e) {
			// TODO Auto-generated catch block
	    	play.Logger.log4j.info(e.getMessage());
		}finally { 
			// sb: we're finished; release the lock
			 /*  try{  
			    	if(runningLock.isLocked())
			    		runningLock.unlock();
			    }catch(Exception err){
			    	
			    }
			 */
			//call the garbage collector 
			
		} 
	}
   
}
