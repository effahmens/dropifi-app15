package job.process;

import play.libs.WS;
import play.libs.WS.HttpResponse;
import models.MessageManager;
import helper.redis.Redis;
import query.inbox.InboxSelector;
import resources.CacheKey;
import resources.DropifiTools;
import resources.JPAHelper;
import resources.MsgStatus; 
import view_serializers.CoProfileSerializer;
import akka.actor.UntypedActor;

public class InboxUpdateActor extends UntypedActor{

	@Override
	public void onReceive(Object object) throws Exception {
		// TODO Auto-generated method stub
		
		if(object instanceof CoProfileSerializer){ 
			try{ 
				CoProfileSerializer co = (CoProfileSerializer)object;
				if(co!=null && co.getApikey()!=null && co.getCompanyId()!=null){
					WS.url(DropifiTools.ServerURL+"/inboxupdater/webhooks/").setHeader("Content-Type", "application/json")
							.setHeader("Content-Length", "1").setParameter("apikey",co.getApikey()).setParameter("companyId", co.getCompanyId())
							.setParameter("secretKey", DropifiTools.InboxUpdater).post();
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		try{ 
			if(this !=null && this.getContext()!=null)
	      		 this.getContext().stop(); 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}

}
