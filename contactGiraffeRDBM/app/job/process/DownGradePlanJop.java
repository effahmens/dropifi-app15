package job.process;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.persistence.GenerationType;

import job.ConcurrentTask.DownGradePlanPullTask;

import models.CustomerProfile;
import models.InboundMailbox;
import models.dsubscription.DownGradePlan;

import play.Logger;
import play.jobs.Every;
import play.jobs.Job;

//@Every("240min")
public class DownGradePlanJop  extends Job {

	/**
	 * Used to avoid running multiple instances of this job simultaneously.
	 */
	private static final ReentrantLock runningLock = new ReentrantLock();

	@Override
	public void doJob(){
		// sb: try to acquire the lock. if the lock is in use, then just
	    // return immediately, because this job is already running.
	    // wait a half second before giving up.
		
	    try {
		   
			if (!runningLock.tryLock(500, TimeUnit.MILLISECONDS)) {
			  //maybe log a message here?
			 //play.Logger.log4j.info("running lock");
			  return;
			} 
	 
			//perform your job code here...				
			List<DownGradePlan> plans = DownGradePlan.findAllPlanToDownGrade();
			if(plans != null){
				int size = plans.size();
				play.Logger.log4j.info("DownGradePlan size : "+ size); 
				
				if(size>0){  			
					//create an instance of the DownGradePlanPullTask to execute each downgrade plan
					//on a separate thread  
					DownGradePlanPullTask pullPlan = new DownGradePlanPullTask(plans, 0, size);
					ForkJoinPool mainPool = new ForkJoinPool();		
					mainPool.invoke(pullPlan);  
				}
			} 
			
	    } catch (InterruptedException e) {
			// TODO Auto-generated catch block
	    	play.Logger.log4j.info(e.getMessage(),e);	    	
		}finally {
		      // sb: we're finished; release the lock
			 if(runningLock != null){
				 runningLock.unlock();
			 }
		} 
	}
}
