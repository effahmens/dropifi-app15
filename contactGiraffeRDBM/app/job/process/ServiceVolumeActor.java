package job.process;

import java.sql.Connection;
import java.util.Map;

import models.dsubscription.SubConsumeService;

import resources.DCON;
import resources.JPAHelper;
import resources.helper.ServiceVolumeSerializer;

import akka.actor.UntypedActor;

public class ServiceVolumeActor extends UntypedActor{

	@Override
	public void onReceive(Object object) throws Exception {
		// TODO Auto-generated method stub
		JPAHelper.reset(); 
		
		if(object instanceof ServiceVolumeSerializer){
			
			ServiceVolumeSerializer volSerializer = (ServiceVolumeSerializer)object;
			Connection conn = null;
			
			try{
				conn = DCON.getDefaultConnection();
				int result = SubConsumeService.updateConsumedVolume(conn, volSerializer);
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e); 
			}
		}
	}

}
