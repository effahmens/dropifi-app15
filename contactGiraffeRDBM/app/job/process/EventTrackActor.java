package job.process; 
import helper.DMailChimp;
import helper.GMailChimp;
import resources.DropifiTools;
import resources.EventTrackSerializer;
import resources.JPAHelper;
import akka.actor.UntypedActor;

public class EventTrackActor  extends UntypedActor{
	public static String MIXPANEL_TOKEN = "98374bf39bf3ea2b516de631d5769b76"; //"YOUR TOKEN";
	 
    public static long MILLIS_TO_WAIT = 10 * 1000;
	
	@Override
	public void onReceive(Object object) throws Exception { 
		// TODO Auto-generated method stub 
		try{
			if(object instanceof EventTrackSerializer) { 
				//JPAHelper.reset(); 

				MIXPANEL_TOKEN = DropifiTools.MIXPANEL_TOKEN;
				EventTrackSerializer event = (EventTrackSerializer)object;
				DMailChimp chimp = new DMailChimp(DropifiTools.MailchimpApikey, DropifiTools.MailchimpListId);
				
				switch(event.getEvent()){
					case "SIGNUP":
						event.trackSignup(MIXPANEL_TOKEN);
						break;
					case "LOGIN":
						event.trackLogin(MIXPANEL_TOKEN);
						break;
					case "SUBSCRIPTION":
						event.trackSubscription(MIXPANEL_TOKEN);
						break;
					case "ACCOUNT CANCELLED":
						event.trackCacncelAccount(MIXPANEL_TOKEN); 
						chimp.unsubscribeMemberInfo(event.getEmail().replace("delete_","")); 
						break;
					case "Track URL":
						event.trackUrl(MIXPANEL_TOKEN);
						break;
					case "REVENUE":
						event.trackCharge(MIXPANEL_TOKEN);
						break;
					case "NEWSLETTER":
						GMailChimp gchimp = new GMailChimp(DropifiTools.MailchimpApikey, DropifiTools.MailchimpNewsListId);
						gchimp.addMemberInfo(event.getEmail(), event.getName(), event.location, event.browser);
						break;
					case "KEYVALUE":
						event.trackValue(MIXPANEL_TOKEN);
						break;
				}

				if(event.getEvent().equals("SIGNUP") || event.getEvent().equals("LOGIN") || event.getEvent().equals("SUBSCRIPTION")){		
					chimp.cleanup();
					if(event.location!=null && event.location.trim().isEmpty()){
						event.location=null;
					}
					
					if(event.getName()!=null){
						event.setName(event.getName().trim());
					} 
					chimp.addMemberInfo(event.getEmail(), event.getName(), event.getWidgetType(), event.getAccountType(), event.location);
				}
				
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}finally{
			if(this !=null && this.getContext()!=null)
	     		 this.getContext().stop();
		}
	}

}
