package job.process;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.persistence.GenerationType;

import models.CustomerProfile;
import models.InboundMailbox;

import play.Logger;
import play.jobs.Every;
import play.jobs.Job;
import resources.ContactPullTask;

//@Every("360min")
public class ContactJob  extends Job{

	/**
	   * Used to avoid running multiple instances of this job simultaneously.
	   */
	private static final ReentrantLock runningLock = new ReentrantLock();

	@Override
	public void doJob(){
		// sb: try to acquire the lock. if the lock is in use, then just
	    // return immediately, because this job is already running.
	    // wait a half second before giving up.
		
	    try {
		   
			if (!runningLock.tryLock(500, TimeUnit.MILLISECONDS)) {
			  // maybe log a message here?
				play.Logger.log4j.info("running lock");
			  return;
			} 
	 
			//perform your job code here...		
			//select all the inbound mailbox for pulling email		
			
			//TOBE Implemented
			
			//the findAll method should be changed. Only customer profile without any of the following:  
			//facebook, twitter, linkedIn socialId should be selected
			
			List<CustomerProfile> customers = CustomerProfile.findAllWithoutSocialId();
			if(customers != null){ 				
				 
				int size = customers.size();
				play.Logger.log4j.info("Customer size: "+ size);
				
				if(size>0){
					
					ArrayList<CustomerProfile> customerArray = new ArrayList<CustomerProfile>(customers);
					
					//create an instance of the ContactPullTask to execute each customer
					//on a separate thread 
					
					ContactPullTask pullContact = new ContactPullTask(customerArray, 0, size);
					
					ForkJoinPool mainPool = new ForkJoinPool();			
					mainPool.invoke(pullContact);  
				}
			}
			
	    } catch (InterruptedException e) {
			// TODO Auto-generated catch block
	    	play.Logger.log4j.info(e.getMessage());	    	
		}finally {
		      // sb: we're finished; release the lock
			 if(runningLock != null){
				 runningLock.unlock();
			 }
		} 
	}
}
