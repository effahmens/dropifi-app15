package job.process;

import static akka.actor.Actors.actorOf;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.*;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import play.data.validation.Validation;
import play.db.jpa.Blob;
import play.db.jpa.JPA;
import play.utils.HTML;
import query.inbox.InboxSelector;
import models.Company;
import models.CompanyProfile;
import models.CustomerLocation;
import models.CustomerProfile;
import models.DMessageLocation;
import models.DropifiMessage;
import models.CustomerMsgSource;
import models.FileDocument;
import models.GeoLocation;
import models.InboundMailbox;
import models.MainRule;
import models.MsgAttachment;
import models.MsgCounter;
import models.MsgKeyword;
import models.MsgSentiment;
import models.RuleAction;
import models.WMsgRecipient;
import models.Widget;
import models.WidgetDefault;
import models.WidgetRule;
import resources.APIKEY;
import resources.AccountType;
import resources.DCON;
import resources.DropifiTools;
import resources.EmailValidity;
import resources.EventTrackSerializer;
import resources.FileType;
import resources.JPAHelper;
import resources.MailComposer;
import resources.MsgSerializer;
import resources.RuleParameters;
import resources.SQLInjector;
import resources.SentimentSerializer;
import resources.SentimentType;
import resources.WidgetRespSerializer;
import view_serializers.ClientSerializer;
import view_serializers.CoProfileSerializer;
import view_serializers.MbSerializer; 
import view_serializers.AttSerializer;
import adminModels.*;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import api.rackspace.RackspaceClient;
import api.sentiment.SentimentClient;
import resources.sentiment.SentimentBaseSerializer;
import resources.sentiment.SentinmentAPIType;
import resources.sentiment.lymbix.*;

/**
 * Process messages sent from the widget. Send "automatic response" to the sender of the message. Perform sentiment analysis and retrieve the full contact profile of the sender of the message.
 * Send a copy of the message to the intended recipients. If there are rules on the widget, execute the rules. finally save the message. 
 * @author philips 
 *
 */
public class WidgetActor extends UntypedActor{

	@Override 
	public void onReceive(Object object) {
		// TODO Auto-generated method stub
		//JPAHelper.reset();
		/** 
		 *  
		 * Add business rules 
		 * 
		 * 
		 */
		
		if(object instanceof WidgetRespSerializer){
			int status = 404;  
			WidgetRespSerializer widget = (WidgetRespSerializer)object;
			
			if(widget != null ) {
				 
				ClientSerializer cs = widget.getClientSerializer();				
				
				if(cs != null) {
					
					//check if the message is not a spam 
					 
					//save the message and perform sentiment analysis as well as pull the full contact profile of the customer 				
					Timestamp date = new Timestamp(new Date().getTime());
					
					MsgSerializer msg = new MsgSerializer( cs, widget.getCompanyId(), date, date, 
							widget.getWidgetId(), DropifiTools.WIDGET_MESSAGE);   
					
					Connection conn=null; 
					try{ 
						conn = DCON.getDefaultConnection();	 
						 /** 
						  * Perform Rules on the widget 
						  **/					 		
						 
						//Make a post request using any of the api source to determine the sentiment of the text
						SentimentClient sentiClient = new SentimentClient(DropifiTools.AlchemyApiKey, SentinmentAPIType.Alchemy, DropifiTools.removeHTML(msg.getTextBody()), "en");	 
						SentimentBaseSerializer sentiment =null;
						try{
							sentiment = sentiClient.analyze();
						}catch(Exception e){
							 play.Logger.log4j.info(e.getMessage(),e);
						}
						
						DropifiMailbox mailbox = DropifiMailbox.findByIdentifier(conn, MailingType.Notification);						
						MbSerializer mb = new MbSerializer(mailbox); 
			        	widget.setDropifiMailbox(mb);
			        	
						//set the parameters to be used in comparing the rules
						RuleParameters params = new RuleParameters(msg, sentiment, widget.getDropifiMailbox(),widget.getApikey(),widget.getLocation());
						params.pageUrl= cs.getPageUrl();
						Timestamp time = new Timestamp(new Date().getTime()); 
						params.created = time;
						params.messageTemplate = Widget.findMessageTemplate(conn, params.apikey);
						
						if(mailbox.getMailserver().equals("Mailgun")){
							params.setMgCampaignId( play.Play.mode.isProd()?"dropifi_widget_mail":"dropifi_widget_dev");
							params.autoReplyTo.put("msgCampaignId",play.Play.mode.isProd()?"dropifi_autoresponse_mail":"dropifi_autoresponse_dev");
						}
						
						//set company profile parameters
						CoProfileSerializer profile = Company.findInfoByApikey(conn, widget.getApikey()); 

						if(profile!=null){
							profile.setCompanyId(msg.getCompanyId());
		    				widget.setProfile(profile);
		    				params.compProfile = profile; 
		    			}
	    				
	    				String defaultCompanyName ="";		      				
	      				if(widget.getProfile()!=null){
	      					defaultCompanyName = widget.getProfile().getUsername();
	      					params.domain = widget.getProfile().getDomain();
	      				}	
	      				
						//find all the widget rules activated
						List<WidgetRule> rules = WidgetRule.findAll(conn, msg.getCompanyId(),true); 
						WidgetRule widgetRule = new WidgetRule(); 
						
						//check if any of the rule condition is met
						Map<Boolean,Map<String, Object>> rule = widgetRule.widgetActionPerformed(rules, msg.getCompanyId(), params); 
						boolean rulePassed = rule.keySet().iterator().next();
						 						
						 /**
						  * end of rules on the widget* 
						  */
						
						if(rulePassed){  
							//search for the default mailbox of the company
							MbSerializer cMb = InboundMailbox.findDefaultMailbox(conn, widget.getApikey()); 
							
							//set cMb to mb (default dropifi mailbox) if cMb is null:(user has no default mailbox)
							if(cMb == null){ 								 
								mb.setUsername(profile.getMainName()); 
								cMb = mb; 
								params.setAutoReplyTo(params.compProfile.getEmail(), params.compProfile.getUsername());
							}else{
								params.setAutoReplyTo(cMb.getEmail(),cMb.getUsername());
							}
							
							params.companyMailbox = cMb;
							
							//Mailbox fix
							//send a response to the sender of the message using the company's default mailbox
							//if and only if auto-response is activated
							
							//EmailValidity emaiV = new EmailValidity(widget.getClientSerializer().getEmail());
							int wstatus = 200;//emaiV.CheckDomain();   
							
							if((wstatus==200 || wstatus==201) && !params.hasSendAutoResponse){ 
								wstatus = DropifiMailer.sendWidgetAutomaticResponse(conn,widget, cMb,params.autoReplyTo); 	
								params.isResponseSent = wstatus==200?true:false;
							}

							if(wstatus == 200 || wstatus == 201){ 
								//check that the auto response was sent to the sender of the message and add
								//the status to the rule parameters								
								
								//generate a key for the message
								params.msgId = DropifiMessage.genMsgId(msg.getCompanyId(), msg.getEmail()); 
								msg.setMsgId(params.msgId); 
								
								//create an instance of DropifiMessage which will be used in saving the message from the widget
								DropifiMessage message = new DropifiMessage(msg,true);
								try{
									String intense = "";
									if(sentiment!=null && sentiment.getIntense()!=null){
										intense = sentiment.getIntense();
									}
									message.setSubjectToIntense(msg.getSubject(), intense);
								}catch(Exception e){
									play.Logger.log4j.info(e.getMessage(),e);
								}
								
								message.setCreated(params.created); 
								
								//check if the subject of the message is set else set the subject to intense subject 
								//add optional information from the widget such as phone number, country - city, referral url
								
								if(cs!= null){
									try{
										Map<String,String> optionals = new HashMap<String, String>(); 
										if(cs.getPhone() != null && !cs.getPhone().isEmpty()) 
											optionals.put("phone", cs.getPhone());
										 
										optionals.put("pageUrl", cs.getPageUrl());
										
										if(widget.getLocation()!=null){
											//Search location of contact if only IP address was returned
											//play.Logger.log4j.info("before: "+widget.getLocation());
											if(widget.getLocation().getIpAddress()!=null && (widget.getLocation().getCity()==null || widget.getLocation().getCity().trim().isEmpty())){ 	
												widget.setLocation(GeoLocation.findByMaxmind(DropifiTools.MaxmindUserId, DropifiTools.MaxmindLicenseKey, widget.getLocation().getIpAddress()));
												//play.Logger.log4j.info("after: "+widget.getLocation());
											} 
											optionals.put("location", widget.getLocation().getCityCountry());
										}
										message.setOptionals(optionals);
									}catch(Exception e){
										play.Logger.log4j.info(e.getMessage(),e);
									}
								}   
								
								 //save the message: (pull the contact profile of the sender, create the sender and set as sender of the message) 											
								 status = message.createWidgetMessage(conn);  
								 long countMsg = 0l;
								 if(status == 200) { 
									 Long messageId = DropifiMessage.getMId(conn,msg.getMsgId());  
									 SentimentSerializer senti = null;
									 if(messageId!=null){
										 //save the location of the messages

										try{  
											 GeoLocation location = widget.getLocation();
											 if(location!=null){												  
												 DMessageLocation mloc = new DMessageLocation(message.getMsgId(),message.getCompanyId(),widget.getApikey(),message.getEmail(),location);
												 mloc.saveLoaction(conn);
												
												 CustomerLocation cusLocation = new CustomerLocation(message.getCustomerId(),widget.getApikey(),message.getEmail(),location);
												 cusLocation.saveLoaction(conn);
												
												 CustomerProfile.updateLocation(conn,location.getCityCountry(), message.getEmail());
											 }
											 countMsg = message.getWidgetMessages(conn,message.getCompanyId());
											
										}catch(Exception e){
											 play.Logger.log4j.info(e.getMessage(),e);
										} 
										 
										 //send a request for sentiment analysis to be saved to the database
										/* try{
											 ActorRef sentimentActor = actorOf(SentimentActor.class).start(); 			
											 senti = new SentimentSerializer(messageId,message.getEmail(),message.getCompanyId(),message.getSourceId(), message.getSourceOfMsg(), message.getTextBody(),sentiment);				
											 sentimentActor.tell(senti);  
										 }catch(Exception e){
											 play.Logger.log4j.info(e.getMessage(),e);
										 }
										 */ 
										 try{
											 senti = new SentimentSerializer(messageId,message.getEmail(),message.getCompanyId(),message.getSourceId(), message.getSourceOfMsg(), message.getTextBody(),sentiment);				
									      	 if(senti !=null && senti.getMsgId()!=null) {
									      		
									      		 if((senti.getArticle() !=null) && (!senti.getArticle().isEmpty())){
									      			MsgSentiment msgSent = null;  
									      			 try{			      		 
									 			         //save the result to database
									      				msgSent = new MsgSentiment(senti.getMsgId(),senti.getEmail(),senti.getCompanyId(),senti.getSourceOfMsg(),  senti.getSentiment());
									      				msgSent.createSentiment(conn);  
									      			 }catch(Exception err){ 
									      			 }
									      			 
									      			try{
									      				
										      			SentimentType typeOfSenti = SentimentType.Neutral;
										      			if(msgSent !=null){
												        	 if(msgSent.getArticleSentiment().equalsIgnoreCase("Positive")){
												        		 typeOfSenti = SentimentType.Positive;
												        	 }else if(msgSent.getArticleSentiment().equalsIgnoreCase("Negative")){
												        		 typeOfSenti = SentimentType.Negative;
												        	 }else if(msgSent.getArticleSentiment().equalsIgnoreCase("NoSentiment")){
												        		 typeOfSenti = SentimentType.NoSentiment;
												        	 }
										      			}
										      			
											        	//add key words to message
											        	MsgKeyword.createKeywords(conn,senti.getCompanyId(), senti.getMsgId(), senti.getArticle(), typeOfSenti, senti.getSourceOfMsg(), msgSent.getCreatedDate());
									      			}catch(Exception e){  
									      			}
									      			 
									      		 } 	        	
									      	 } 
										 }catch(Exception e){ 
										 }
									 }
									 
									 try{					 
										 CustomerMsgSource.saveSource(conn, message.getCompanyId(), message.getEmail(), message.getSourceOfMsg(), 1);
									 }catch(Exception e){
										 play.Logger.log4j.info(e.getMessage(),e); 
									 } 
									 
									//-------------------------------------------------------------------------------------------------------------------------------
						      			
					      			 try{	
					      				 if(message!=null){
					      					 MsgCounter.incNewCounter(conn,message.getCompanyId(), message.getSourceId(), message.getSourceOfMsg(),sentiment.getSentiment());
					      				 }				      			 
					      			 }catch(Exception e){			      				 
					      				 //play.Logger.log4j.info("New Counter Called: "+ senti.getSourceOfMsg(),e);
					      			 }	      			 
					      			
					      		   //retrieve the file attached to the message from AWS S3 
					      			try{ 
					      				if(cs.getAttachmentId()!=null && !cs.getAttachmentId().isEmpty()){
					      					 					      					
					      					String fileName = FileDocument.findFileName(conn, params.apikey, FileType.WidgetAttachment, cs.getAttachmentId());
					      					if(fileName!=null){
					      						AttSerializer att = new AttSerializer(messageId, params.domain, params.sender, params.msgId, fileName, cs.getAttachmentId());
					      						att.apikey = params.apikey; 
					      						
					      						if(att.hasAttachedBlobFile()){
					      							MsgAttachment.addAttachment(att, conn);
					      							params.setAttachedFile(att); 
					      						}
					      					}
					      				}
					      			}catch(Exception e){
					      				play.Logger.log4j.info(e.getMessage(),e);
					      			}
					      			
					      			params.countMsg = countMsg;
					      			//create an address list of activated recipient of widget message 
					      			Address[] reciAddress = WMsgRecipient.activeRecipients(conn,widget.getApikey(),widget.getDefaultEmail(),defaultCompanyName);
				      				params.setContactProfile(InboxSelector.findCustomerProfile(conn,params.companyId, params.sender));
				      				
					      			if(reciAddress != null && reciAddress.length>0){
						      			//send notification to the company as well as the recipients specified
					      				DropifiMailer.sendWidgetAutomaticNotification(conn,defaultCompanyName, widget.getDefaultEmail(),reciAddress,params); 					      		
					      			}else if(profile.getAccountType().equals(AccountType.Free.name()) && (reciAddress==null || reciAddress.length<=0)){
					      				/*No delivery email associated with the account. Send a notification to the user to upgrade to any of the
					      				 * paid plan to have full access to customization and other services*/ 
					      				reciAddress = new Address[1];
					      				reciAddress[0] = new InternetAddress(profile.getEmail(), profile.getUsername());
					      				DropifiMailer.sendWidgetAutomaticNotification(conn,defaultCompanyName, widget.getDefaultEmail(),reciAddress,params); 	
					      				//play.Logger.log4j.info("No delivery email associated with the account");
					      			} 
					      			
									try {
										//execute the rule actions
						      			Map<String, Object> ruleAction = rule.get(rulePassed); 
						      			ruleAction.put("apikey", widget.getApikey());
						      			
						      			/*
						      			Send the actions to rule actor to execute the rule actions 
										ActorRef ruleActor = actorOf(RuleActor.class).start(); 
										ruleActor.tell(ruleAction);
										*/
										
										if(ruleAction!=null){
											 List<RuleAction> actions = (List<RuleAction>) ruleAction.get("actions");
											 if(actions !=null){
												 Long companyId = (Long)ruleAction.get("companyId"); 
												 RuleParameters ruleParams = (RuleParameters)ruleAction.get("params");	
												 String apikey= (String)ruleAction.get("apikey");
												 
												 //fetch the list of active message recipient
												 conn = DCON.getDefaultConnection();
												 List<String> recipients = WMsgRecipient.findActiveRecipients(conn,apikey);
												 MainRule.executeRuleAction(conn,actions, companyId, ruleParams,recipients);  
												 
											 }
										}
									}catch(Exception e){ 
									}					      			
								 } 
								 
								 //save the file attached to the message to amazon S3 
								 //NOT AVAILABLE FOR BETA RELEASE
								 
								 /***						  
								  ActorRef attActor = actorOf(AttachmentActor.class).start();
								  if(msg.getAttachments()!=null){ 
									 for(AttSerializer att: msg.getAttachments()){	
										 att.id = messageId;						
										 attActor.tell(att); 									
									 } 									 
								  }
								 ***/
								 
								 //track the total messages received by the user
								if(countMsg>0){
									 EventTrackSerializer event = new EventTrackSerializer();
									 event.trackTotalMessages(DropifiTools.MIXPANEL_TOKEN, profile.getEmail(), countMsg);
								 }
								
								InboxSelector.deleteAllCaches(profile,true);
							 }else{
								 play.Logger.log4j.info("Email address is not associated with any mailing server: "+status); 
							 }				
						}else{
							//Possible Reason: Stop Process Action
							play.Logger.log4j.info("Stop processing action encountered"); 
						}
					 
					}catch(Exception e){
						play.Logger.log4j.info("after connection: "+ e.getMessage(),e);						 
						status = 501;
					}finally{
						try{ 
							
							if(conn!=null)
								conn.close();							
						}catch(Exception e){}
					}
				
				}//end of ClientSerializer not null
				
			}//end of widget not null
			play.Logger.log4j.info("processing complete"); 
		}//end of check the type object
		
		try{
			
			if(this !=null && this.getContext()!=null)
	      		 this.getContext().stop(); 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
				
	}
		
}
