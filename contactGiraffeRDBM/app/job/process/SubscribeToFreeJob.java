package job.process;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import job.ConcurrentTask.DownGradePlanPullTask;
import models.dsubscription.DownGradePlan;
import play.jobs.Every;
import play.jobs.Job;

///** Fire at 12pm (noon) every day **/ 
//@On("0 0 12 * * ?")
//@Every("20min")
public class SubscribeToFreeJob extends Job{

	/**
	 * Used to avoid running multiple instances of this job simultaneously.
	 */
	private static final ReentrantLock runningLock = new ReentrantLock();

	@Override
	public void doJob(){
		// sb: try to acquire the lock. if the lock is in use, then just
	    // return immediately, because this job is already running.
	    // wait a half second before giving up.
		
	    try {
		   
			if (!runningLock.tryLock(500, TimeUnit.MILLISECONDS)) {
			  // maybe log a message here?
				play.Logger.log4j.info("running lock");
			  return;
			} 

	    } catch (InterruptedException e) {
			// TODO Auto-generated catch block
	    	play.Logger.log4j.info(e.getMessage(),e);	    	
		}finally {
		      // sb: we're finished; release the lock
			 if(runningLock != null){
				 runningLock.unlock();
			 }
		} 
	}
}
