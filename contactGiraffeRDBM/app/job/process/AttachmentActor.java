package job.process;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;

import models.MsgAttachment;

import resources.AWSS3;
import view_serializers.AttSerializer;
import resources.DCON;
import resources.DropifiTools;
import resources.EmailManager;
import resources.SentimentSerializer;
import akka.actor.UntypedActor;

public class AttachmentActor  extends UntypedActor{
	
	@Override
	public void onReceive(Object object) throws Exception {
		// TODO Auto-generated method stub
		
		/**
		 * 
		 * 
		 * Add business rules here
		 * 
		 * 
		 * 
		 */ 
		
		// TODO Auto-generated method stub		
		//check if the message object is an instance of SentimentSerializer 
		try{
			if(object instanceof AttSerializer){
				AttSerializer att = (AttSerializer)object; 			
				
				if(att !=null){
					att.uploadFileToS3(DropifiTools.AWSUserFileBUCKETNAME);
					play.Logger.log4j.info("Attachemnt in actor  = "+ att+" : Apikey = "+att.apikey);
				} 
			} 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
}
