/**
 * 
 */

package job.ConcurrentTask;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.RecursiveAction;
import javax.mail.MessagingException;

import resources.EmailManager;
import models.InboundMailbox;

public class EmailPullTask extends RecursiveAction {
	
	
	private final int low;
	private final int high;
	private static final int THRESHOLD = 1;	 	
	private final List<InboundMailbox> inbound;
	
	public EmailPullTask (List<InboundMailbox> inbound,int low,int high){
		this.low = low;
		this.high = high;		
		this.inbound = inbound;
	}	
	
	@Override
	protected void compute(){
		// TODO Auto-generated method stub
		
		if((high-low)<=THRESHOLD){
			if(low>high) return ;
			
			//loop through the available inbound mailbox(s) on this thread //and start pulling the email messages
			for(int i=low;i<high;i++){
				try { 
					InboundMailbox mailbox = inbound.get(i); 
					if(mailbox != null && mailbox.isNextPull()){ 
						//play.Logger.log4j.error("Found Monitored Mailbox: Company: "+mailbox.getCompanyId()+", Email: "+mailbox.getEmail());
						EmailManager email = new EmailManager(mailbox);
						email.connect(); 
					} 
				} catch (Exception e){ 
					// TODO Auto-generated catch block	
					play.Logger.log4j.error(e.getMessage(), e); 
				} 				
			}
			 
		}else{ 
			int mid =(high+low)>>>1; 			  
			EmailPullTask left = new EmailPullTask(inbound, low, mid);
			EmailPullTask right = new EmailPullTask(inbound, mid, high); 			
			invokeAll(left,right);
		}		
		 
	}

}
