package job.ConcurrentTask;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.concurrent.RecursiveAction;

import org.joda.time.DateTime;

import play.db.jpa.JPAEnhancer;
import play.libs.WS;

import resources.DCON;
import resources.DropifiTools;
import resources.JPAHelper;
import resources.pricing.WebhookEvent;

import models.Company;
import models.dsubscription.DownGradePlan;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubPaymentEvent;

public class DownGradePlanPullTask extends RecursiveAction{

	private  int low;
	private  int high;
	private static final int THRESHOLD = 1;	//for dynamic value, the value should always be read from a database  	
	private List<DownGradePlan> plans;
	
	public DownGradePlanPullTask(List<DownGradePlan> plans, int low, int high){
		this.low= low;
		this.high = high;
		this.plans = plans;
	}
	
	@Override
	protected void compute(){
		//TODO Auto-generated method stub
		JPAHelper.reset();
		
		if((high-low)<=THRESHOLD){
			if(low>high) return ;
			
			Connection conn = DCON.getDefaultConnection();
			for(int i=low;i<high;i++){				
				//search for the full contact profile of customer using fullContact API									
				try {
					DownGradePlan plan = plans.get(i);
					if(plan!=null&&!plan.isLocked() && !plan.isSuspended()){
						//lock the plan so that no other process will have access to it
						
						if(plan.updateLock(conn,true)){
							//determine if it is time to send notification
							try{
								Date time = new Date();
								WebhookEvent event = new WebhookEvent(DropifiTools.StripeSecretKey);
								if(plan.getCounter()<=3){
									if(time.after(plan.getNotifyDate())){		
										//set down grade date to 1 weeks
										DateTime datetime = new DateTime(time.getTime()).plusWeeks(1);
										plan.updateNotifyDate(conn, new Timestamp(time.getTime()), new Timestamp(datetime.toDate().getTime()));					
										event.sendFailureNotification(SubPaymentEvent.findEvent(plan.getApikey(), plan.getTypeId()),true);
									} 
								}else{
									//suspend the account
									if(plan.downGradePlan()){
										if(Company.downGradePlan(plan.getApikey())!=null){ 
											//send downgrade message 
											event.sendFailureNotification(SubPaymentEvent.findEvent(plan.getApikey(), plan.getTypeId()),false);
										}
									}
								}
							}catch(Exception e){
								play.Logger.log4j.info(e.getMessage(),e);
							}
							
							plan.updateLock(conn,false);
						}
					}
					
				}catch(Exception e) {
					// TODO Auto-generated catch block
					play.Logger.info("Addd full profile: " + e.getMessage(),e);
				} 			 				
			}
			
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}else{ 
			//divide the task
			int mid = (high+low)>>>1;
			DownGradePlanPullTask left = new DownGradePlanPullTask(plans, low, mid);
			DownGradePlanPullTask right = new DownGradePlanPullTask(plans, mid, high); 
			invokeAll(left,right);
		}
		
	}
}
