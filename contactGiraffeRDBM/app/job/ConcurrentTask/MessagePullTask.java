package job.ConcurrentTask;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import query.inbox.InboxSelector;
import query.inbox.MessageSerializer;
import resources.MsgStatus;

import models.CustomerProfile;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

public class MessagePullTask extends RecursiveTask<JsonArray> {
	private  int low;
	private  int high;
	public  int THRESHOLD = 1000;	//for dynamic value, the value should always be read from a database  	
	private List<Object[]> resultList;
	private Integer timezone;
	
	public MessagePullTask(List<Object[]> resultList, int low, int high,Integer timezone){
		this.low = low;
		this.high = high;
		this.resultList = resultList;
		this.timezone=timezone;
		//this.THRESHOLD = resultList.size()/2; 
	}
	
	@Override
	protected JsonArray compute() {
		// TODO Auto-generated method stub
		
		int mid = (high+low)>>>1;
		
		if((high-low)< THRESHOLD){
			JsonArray j = new JsonArray(); 		 
			 		 
			for(int m=low;m<high;m++){			 
				j.add(InboxSelector.getMessage(this.resultList.get(m),this.timezone)); 
			}			 
			return j;
			
		}else{ 			
 
			MessagePullTask task1 = new MessagePullTask(resultList, low, mid,this.timezone);			
			MessagePullTask task2 = new MessagePullTask(resultList, mid, high,this.timezone);
			
			task1.fork();
			task2.fork(); 
			task1.join().addAll(task2.join());
						  
			return task1.join();
		}		 
	}

}
