package adminModels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Query;

import play.data.validation.Required;
import play.db.jpa.Model;
import com.google.gson.annotations.SerializedName;
import java.util.*;

@Entity
public class MailboxConfig extends Model{

	@Required @Column(nullable=false, unique=true)
	private String mailserver;
	//imap/pop settings for inbound
	@Column(nullable=false, length=500) @Required
	@SerializedName("hostType") 
	private String hostType;		
	@Column(nullable=false, length=500) @Required
	@SerializedName("host") 
	private String host;
	@Column(nullable=false) @Required
	@SerializedName("port") 
	private Integer port;	

	//smtp setting for outbound
	@Column(nullable=false)
	@SerializedName("outHostType")
	private String outHostType;
	@Column(nullable=false)	
	@SerializedName("outHost")
	private String outHost;
	@Column(nullable=false)	
	@SerializedName("outPort")
	private Integer outPort;
		
	public MailboxConfig(String mailserver, String hostType, String host,
			Integer port, String outHostType, String outHost, Integer outPort) {
		 
		this.setMailserver(mailserver);
		this.setHostType(hostType);
		this.setHost(host);
		this.setPort(port);
		this.setOutHostType(outHostType);
		this.setOutHost(outHost);
		this.setOutPort(outPort);
	}
	
	public int addConfig(){
		
		MailboxConfig mbConfig = findByMailServer(this.getMailserver());
		
		if(mbConfig == null){
			try{
				return this.validateAndSave()?200:201;				
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e); 
				return 201;
			}
		}else{
			return mbConfig.saveUpdateParameters(this); 
		}
		 
	}
	
	public int updateConfig(MailboxConfig config){
		
		//check if the update mailbox config is not null
		if(config !=null){
			if(this.getMailserver().equalsIgnoreCase(config.getMailserver())){
				return saveUpdateParameters(config);		
			}else{
				//check if the
				MailboxConfig mbConfig = findByMailServer(config.getMailserver());
				if(mbConfig == null){
					this.setMailserver(config.getMailserver()); 
					return saveUpdateParameters(config);
				} 
			}		
			return 302; 
		}
		return 403;
	}
	
	private int saveUpdateParameters(MailboxConfig config) {
		
		if(config !=null){
			this.setHostType(config.getHostType());
			this.setHost(config.getHost());
			this.setPort(config.getPort());
			this.setOutHostType(config.getOutHostType());
			this.setOutHost(config.getOutHost());
			this.setOutPort(config.getOutPort());
			
			try{
				return this.validateAndSave()?200:201;
			}catch(Exception e){
				return 202;
			}
		}
		return 403;
	}
	
	//check if a similar mailserver is not greater
	public static MailboxConfig findByMailServer(String mailserver){
		try{
			return MailboxConfig.find("Select m from MailboxConfig m where m.mailserver=?", mailserver).first(); 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public static boolean  isSetup(){
		return MailboxConfig.findAll().size()>0;
	}
	
 	public static List<String> findAllMailserver(){ 
		
		Query q = MailboxConfig.find("select m.mailserver from MailboxConfig m order by m.id").query; 
		
		List<String> maillist = new ArrayList<String>(); 
		ListIterator<String> qserver = q.getResultList().listIterator();
		
		while(qserver.hasNext()){
			//String ms = qserver.next();
			String qs =qserver.next();// String.valueOf();
			maillist.add(qs);
		}
		
		return maillist;
	}

	public String getMailserver() {
		return mailserver;
	}

	public void setMailserver(String mailserver) {
		this.mailserver = mailserver;
	}

	public String getHostType() {
		return hostType;
	}

	public void setHostType(String hostType) {
		this.hostType = hostType;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getOutHostType() {
		return outHostType;
	}

	public void setOutHostType(String outHostType) {
		this.outHostType = outHostType;
	}

	public String getOutHost() {
		return outHost;
	}

	public void setOutHost(String outHost) {
		this.outHost = outHost;
	}

	public Integer getOutPort() {
		return outPort;
	}

	public void setOutPort(Integer outPort) {
		this.outPort = outPort;
	}	
	
	
}
