package adminModels;

import play.*;
import play.db.jpa.*;

import javax.persistence.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Entity
public class SystemApikey extends Model {
    
	@Column(nullable =false, unique=true)
	private String type;
	@Column(nullable =false)
	private String apikey;
	@Column(nullable =false)
	private String secretKey;
	private String publicKey;	
	
	public SystemApikey(String type, String apikey,String secretKey){
		this.setType(type);
		this.setApikey(apikey);
		this.setSecretKey(secretKey);
	}
	
	public SystemApikey(){} 
	
	public int createApikey(){ 
		SystemApikey found = SystemApikey.findApikey(this.getType());
		if(found == null || (found!=null&&found.getType().trim().isEmpty())){
			return this.validateAndSave()?200:201; 	
		}else {
			found.setApikey(this.getApikey());
			found.setPublicKey(this.getPublicKey());
			found.setSecretKey(this.getSecretKey());
			return found.validateAndSave()?200:501;
		}			 	 
	}
	
	public int updateApikey(String apikey, String secretKey, String publicKey){
		if(this != null && apikey != null && !apikey.isEmpty() && secretKey !=null && !secretKey.isEmpty()){
			this.setApikey(apikey);
			this.setSecretKey(secretKey);
			this.setPublicKey(publicKey); 
			return this.validateAndSave()?200:201;
		} 		
		return 505;
	}
	
	public static String findType(String type){
		return SystemApikey.find("Select s.type from SystemApikey s where s.type = ?", type).first();
	} 
	
	public static SystemApikey findApikey(String type){
		return SystemApikey.find("Select s from SystemApikey s where s.type = ?", type).first();
	} 
	
	public static SystemApikey findByType(Connection conn, String type) throws SQLException {
		SystemApikey apikey = null;
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select s.type, s.apikey, s.secretKey, s.publicKey " +
					"From SystemApikey s Where s.type = ? ");
			query.setString(1, type);
			ResultSet result = query.executeQuery(); 
			
			while(result.next()){				
				apikey = new SystemApikey();
				apikey.setType( result.getString("type")   ); 
				apikey.setApikey( result.getString("apikey") );
				apikey.setSecretKey( result.getString("secretKey") );
				apikey.setPublicKey( result.getString("publicKey") );   
				return apikey;
			}			
		}		
		return apikey;
	}
	
	public static String findType(Connection conn, String type) throws SQLException {
		String typeName = null;
		if(conn!=null && !conn.isClosed()){
			typeName = null;
			PreparedStatement query = conn.prepareStatement("Select  type From SystemApikey  Where  type = ? ");
			query.setString(1, type);
			ResultSet result = query.executeQuery(); 
			
			while(result.next()){				
				typeName = result.getString("type");
				return typeName;
			}			
		}else{
			typeName="_";
		}		
		return typeName;
	}
	
	public static String findApikey(Connection conn, String type) throws SQLException {
		String apikey = null;
		if(conn!=null && !conn.isClosed()) {
			PreparedStatement query = conn.prepareStatement("select apikey from SystemApikey where type = ? ");
			query.setString(1, type);
			ResultSet result = query.executeQuery();
			
			while(result.next()){				
				apikey = result.getString("apikey"); 
				return apikey;
			}			
		}		
		return apikey;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
}
