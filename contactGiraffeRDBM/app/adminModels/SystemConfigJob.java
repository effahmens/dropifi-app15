package adminModels;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import models.InboundMailbox;
import models.InvitationCode;

import controllers.Secure.Security;

import play.cache.Cache;
import play.db.jpa.JPA;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.mvc.Scope.Session;
import resources.CacheKey;
import resources.DropifiTools;
import resources.MsgSerializer;
import view_serializers.MbSerializer;  

//@OnApplicationStart
public class SystemConfigJob extends Job {
		
	@Override
	public void doJob() {
		try{ 
			Cache.clear();   
			CacheKey.deleteCache(CacheKey.getLymbixKey());
			CacheKey.deleteCache(CacheKey.getFullcontactKey());
			CacheKey.deleteCache(CacheKey.getStripeKey());
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
		 
		//add mailbox configurations
		//if(!MailboxConfig.isSetup()){
		// }
		List<MailboxConfig> mbConfigs = new LinkedList<MailboxConfig>(); 
		
		mbConfigs.add(new MailboxConfig("Gmail","imaps","imap.gmail.com",993,"smtps","smtp.gmail.com",465));
		mbConfigs.add(new MailboxConfig("Yahoo","imaps","imap.mail.yahoo.com",993,"smtps","smtp.mail.yahoo.com",465));
		mbConfigs.add(new MailboxConfig("Yahoo UK","imaps","imap.mail.yahoo.co.uk",995,"smtps","smtp.mail.yahoo.co.uk",465));
		mbConfigs.add(new MailboxConfig("Yahoo Plus","pop3s","plus.pop.mail.yahoo.com",995,"smtps","plus.smtp.mail.yahoo.com",465));
		mbConfigs.add(new MailboxConfig("Hotmail","pop3s","pop3.live.com",995,"smtp","smtp.live.com",587));	
		mbConfigs.add(new MailboxConfig("MSN","pop3s","pop3.email.msn.com",110,"smtps","smtp.email.msn.com",587));
		mbConfigs.add(new MailboxConfig("AOL","imaps","imap.aol.com", 993, "smtps","smtp.aol.com",465)); 
		mbConfigs.add(new MailboxConfig("GoDaddy","pop3s","pop.secureserver.net", 995, "smtps","smtpout.secureserver.net",465));
		mbConfigs.add(new MailboxConfig("Outlook","pop3","pop3.live.com", 995, "smtp","smtp.live.com",587));
		
		for(MailboxConfig mb: mbConfigs) {
			try{ 
				mb.addConfig();
			}catch(Exception e){
				play.Logger.log4j.error(e.getMessage(), e); 
			}
		}
		
		//Add Dropifi mailbox 
		MailboxConfig conf =  new MailboxConfig("Mailgun","imaps","imaps.mailgun.org", 995, "smtps",
				play.Play.configuration.getProperty("mail.smtp.host"),
				Integer.valueOf(play.Play.configuration.getProperty("mail.smtp.port")));
 	
		if(conf !=null){ 
			
			String password = 	play.Play.configuration.getProperty("mail.smtp.pass");
			String user = 	play.Play.configuration.getProperty("mail.smtp.user");
			
			if(play.Play.mode.isProd()){
				//production settings
				try { 
					DropifiMailbox 	dmb = new DropifiMailbox(MailingType.Notification, conf.getMailserver(), DropifiTools.noreplyUserName, DropifiTools.noreplyEmail, password,  
							conf.getOutHostType(), conf.getOutHost(), conf.getOutPort(),user);					
					dmb.createMailBox();
					
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					 play.Logger.log4j.info(e.getMessage(),e);
				}
				
				try{ 
					DropifiMailbox team = new DropifiMailbox(MailingType.Support, conf.getMailserver(), DropifiTools.teamUserName, DropifiTools.teamEmail, password,  
							conf.getOutHostType(), conf.getOutHost(), conf.getOutPort(),user);					
					team.createMailBox(); 
					
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					 play.Logger.log4j.info(e.getMessage(),e);
				}
				
			}else if(play.Play.mode.isDev()){
				//local settings
				try { 
					DropifiMailbox 	dmbDev = new DropifiMailbox(MailingType.Notification, conf.getMailserver(), DropifiTools.mailName, DropifiTools.mailEmail, password,
							conf.getOutHostType(), conf.getOutHost(), conf.getOutPort(),user);					
					dmbDev.createMailBox(); 
					
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
				}
				
				try{ 
					DropifiMailbox teamDev = new DropifiMailbox(MailingType.Support, conf.getMailserver(),DropifiTools.mailName, DropifiTools.mailEmail, password,  
							conf.getOutHostType(), conf.getOutHost(), conf.getOutPort(),user);				
					teamDev.createMailBox(); 
					
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					 
				}		 
			}
		} 
		
		//set all mailbox to unlock 
		try{
			InboundMailbox.em().createQuery("Update InboundMailbox set islock= ? where companyId>0").setParameter(1, false).executeUpdate();			
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		} 
		
		try{

			SystemApikey apikey1 = new SystemApikey(ApikeyType.FullContact.name(), DropifiTools.FullcontactApiKey, DropifiTools.FullcontactSecretKey); 	
			apikey1.createApikey(); 
			
			SystemApikey apikey2 = new SystemApikey(ApikeyType.Lymbix.name(), DropifiTools.LymbixApiKey, DropifiTools.LymbixSecretKey);	
			apikey2.createApikey();
			
			SystemApikey apikey3 = new SystemApikey(ApikeyType.Stripe.name(), DropifiTools.StripeSecretKey, DropifiTools.StripePublisahblekey);	
			apikey3.createApikey();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(), e); 
		}
		 
	} 
	
	
}
