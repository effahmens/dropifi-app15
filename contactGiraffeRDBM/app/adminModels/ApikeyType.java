package adminModels;
 
public enum ApikeyType {
    FullContact,
    Lymbix,
    AWS,
    Stripe
}
