package adminModels;

public enum MailingType {
	NewsLetter,
	Notification, 
	Support
}
