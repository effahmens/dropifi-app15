package adminModels;

import gmailoauth2grant.DGOauthGrant;
import gmailoauth2grant.DGOauthMail;
import gmailoauth2grant.OAuth2Authenticator;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Security;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address; 
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message; 
import javax.mail.Message.RecipientType;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.event.TransportEvent;
import javax.mail.event.TransportListener;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage; 
import javax.mail.internet.MimeMultipart;

import org.apache.commons.mail.HtmlEmail;

import api.aws.AWSSES;  

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.icegreen.greenmail.util.GreenMail;
import com.sun.mail.handlers.multipart_mixed;
import com.sun.mail.smtp.SMTPTransport;

import play.data.parsing.MultipartStream;
import play.db.jpa.Blob;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import view_serializers.AttSerializer;
import resources.DCON;
import resources.MailTemplate;
import resources.MailingProvider;
import resources.RuleParameters;
import resources.WidgetRespSerializer;
import resources.SmtpClient.GreenMailUtil;
import resources.SmtpClient.ServerSetup;
import models.CompanyProfile;
import models.DropifiMessage;
import models.DropifiResponse;
import models.InboundMailbox;
import models.gmailauth.GmailCredentials;
import view_serializers.ClientSerializer;
import view_serializers.CoProfileSerializer;
import view_serializers.MbSerializer;
import ymsg.support.AntiSpam;


public class DropifiMailer {
	
	private Store store; 				//for incoming mails
	private SMTPTransport transport; 	//for out going mails	 	
	private MbSerializer mailbox;
	private Session session;
    private AWSSES awsMail;
	private boolean isVerifiedEmail;	//determine whether the email is valid for sending messages through amazon simple mail
	private String errorMsg;
	private String apikey;
	private MailingProvider mailingProvider;
	private GmailCredentials gcred;
	private SMTPTransport gmailSMTP; 
	private String userAccount;
	private String mgCampaignId;
	
	public DropifiMailer(final MbSerializer mailbox) throws NullPointerException{
		if(mailbox==null)
			throw new NullPointerException("No mailbox with valid email, password and host settings found");
		

		this.setMailbox(mailbox);
		//if the email address is verified for simple mail sending then send the message using AWS SES
		this.isVerifiedEmail = false; //AWSSES.isVerifiedEmailAddress(mailbox.getEmail());
		
		if(this.isVerifiedEmail){
			this.awsMail = new AWSSES();
			this.session = this.awsMail.getSession();
			this.mailingProvider = MailingProvider.AWSSES;
		}else{	 
			
			// Setup properties
			Properties props = new Properties();
			setUserAccount();
			//create a session

			props.put("mail.smtp.host", mailbox.getOutHost()); 
			props.put("mail.smtp.port", mailbox.getOutPort()); 
			props.put("mail.smtp.from", mailbox.getEmail());
			props.put("mail.smtp.protocol", mailbox.getOutHostType());
			
			props.put("mail.transport.protocol", mailbox.getOutHostType()); 
			props.put("mail.smtp.auth", "true");   
			props.put("mail.smtp.starttls.enable","true"); 
			props.put("mail.smtp.EnableSSL.enable","true"); 
        	
			props.put("mail.smtps.host", mailbox.getOutHost()); 
			props.put("mail.smtps.auth", "true"); 
			
			props.put("mail.smtps.ssl.checkserveridentity", "false");
		    props.put("mail.smtps.ssl.trust", "*");
		    props.put("mail.smtp.ssl.checkserveridentity", "false"); 
		    props.put("mail.smtp.ssl.trust", "*"); 
		    
		    props.put("mail.smtp.timeout", "1000"); 
		    props.put("mail.smtp.connectiontimeout", "1000"); 

			props.put("mail.imaps.ssl.checkserveridentity", "false");
			props.put("mail.imaps.ssl.trust", "*");
			props.put("mail.imap.ssl.checkserveridentity", "false");
			props.put("mail.imap.ssl.trust", "*");
			props.put("mail.pop3.ssl.checkserveridentity", "false");
			props.put("mail.pop3.ssl.trust","*");
			
			
			this.session = Session.getInstance(props, new Authenticator(){ 
				protected PasswordAuthentication getPasswordAuthentication() {
				    return new PasswordAuthentication(userAccount, mailbox.getPassword());
				}
			});
		}
		
	}
	
	public DropifiMailer(String apikey,final MbSerializer mailbox,String mgCompaignId) throws Exception{
		this(apikey,mailbox);
		this.setMgCampaignId(mgCompaignId);
	}
	
	public DropifiMailer(String apikey,final MbSerializer mailbox) throws Exception{
		this.apikey = apikey;
		
		if(mailbox==null)
			throw new NullPointerException();
		

		this.setMailbox(mailbox);
		//if the email address is verified for simple mail sending then send the message using AWS SES
		this.isVerifiedEmail = false; //AWSSES.isVerifiedEmailAddress(mailbox.getEmail());
		if(this.isVerifiedEmail){
			this.awsMail = new AWSSES();
			this.session = this.awsMail.getSession();
			this.mailingProvider = MailingProvider.AWSSES;
		}else{
			this.gcred = null;
			if(mailbox.getMailserver().equalsIgnoreCase("Gmail")){ 
				Connection conn = DCON.getDefaultConnection(); 
				try{ 		
					this.gcred = GmailCredentials.retrieveCredential(conn,this.apikey, mailbox.getEmail());
					
					if(this.gcred!=null){ 
						if(GmailCredentials.isExpired(this.gcred)){ 
							Credential credential = DGOauthGrant.authorization_refresh(this.gcred.getRefreshToken()); 
							this.gcred = GmailCredentials.updateCredentials(conn,gcred, credential,this.gcred.getAccessCode()); 
						}  
						
						this.gmailSMTP = OAuth2Authenticator.connect_SMTP_server(this.gcred.getUserEmail(), this.gcred.getAccessToken(), false);
						this.session = OAuth2Authenticator.smtpsession;
						this.mailingProvider = MailingProvider.GMAILOAUTH;
					} 
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}finally{
					conn.close();
				} 
			}
			
			if(this.gcred == null){ 
				// Setup properties
				Properties props = new Properties();	
				setUserAccount();
				//create a session

				props.put("mail.smtp.host", mailbox.getOutHost()); 
				props.put("mail.smtp.port", mailbox.getOutPort()); 
				props.put("mail.smtp.from", mailbox.getEmail());
				props.put("mail.smtp.protocol", mailbox.getOutHostType());
				
				props.put("mail.transport.protocol", mailbox.getOutHostType()); 
				props.put("mail.smtp.auth", "true");   
				props.put("mail.smtp.starttls.enable","true"); 
				props.put("mail.smtp.EnableSSL.enable","true"); 
	        	
				props.put("mail.smtps.host", mailbox.getOutHost()); 
				props.put("mail.smtps.auth", "true"); 
				
				props.put("mail.smtps.ssl.checkserveridentity", "false");
			    props.put("mail.smtps.ssl.trust", "*");
			    props.put("mail.smtp.ssl.checkserveridentity", "false"); 
			    props.put("mail.smtp.ssl.trust", "*"); 
			    
			    props.put("mail.smtp.timeout", "1000"); 
			    props.put("mail.smtp.connectiontimeout", "1000"); 

				props.put("mail.imaps.ssl.checkserveridentity", "false");
				props.put("mail.imaps.ssl.trust", "*");
				props.put("mail.imap.ssl.checkserveridentity", "false");
				props.put("mail.imap.ssl.trust", "*");
				props.put("mail.pop3.ssl.checkserveridentity", "false");
				props.put("mail.pop3.ssl.trust","*");
				 
	        	
				this.session = Session.getInstance(props, new Authenticator(){ 
					protected PasswordAuthentication getPasswordAuthentication() {
					    return new PasswordAuthentication(userAccount, mailbox.getPassword());
					}
				});	
			}
		}
		
	}
	
	public DropifiMailer(InboundMailbox mailbox){
		// Setup properties		
		this(new MbSerializer(mailbox,true));
	}
	
	public DropifiMailer(String apikey,InboundMailbox mailbox) throws Exception{
		// Setup properties		
		this(apikey,new MbSerializer(mailbox,true));
	}
	
	public DropifiMailer(InboundMailbox mailbox, boolean allParams){
		// Setup properties
		this(new MbSerializer(mailbox,allParams));
	}
	
	public DropifiMailer(final InboundMailbox mailbox, boolean allParams,boolean verified){

		this.setMailbox(new MbSerializer(mailbox, true));
		
		// Setup properties
		Properties props = new Properties();	
		setUserAccount();
		//create a session

		props.put("mail.smtp.host", mailbox.getOutHost()); 
		props.put("mail.smtp.port", mailbox.getOutPort()); 
		props.put("mail.smtp.from", mailbox.getEmail());
		props.put("mail.smtp.protocol", mailbox.getOutHostType());
		
		props.put("mail.transport.protocol", mailbox.getOutHostType()); 
		props.put("mail.smtp.auth", "true");   
		props.put("mail.smtp.starttls.enable","true"); 
		props.put("mail.smtp.EnableSSL.enable","true"); 
    	
		props.put("mail.smtps.host", mailbox.getOutHost()); 
		props.put("mail.smtps.auth", "true"); 
		
		props.put("mail.smtps.ssl.checkserveridentity", "false");
	    props.put("mail.smtps.ssl.trust", "*");
	    props.put("mail.smtp.ssl.checkserveridentity", "false"); 
	    props.put("mail.smtp.ssl.trust", "*"); 
	    
	    props.put("mail.smtp.timeout", "1000"); 
	    props.put("mail.smtp.connectiontimeout", "1000"); 

		props.put("mail.imaps.ssl.checkserveridentity", "false");
		props.put("mail.imaps.ssl.trust", "*");
		props.put("mail.imap.ssl.checkserveridentity", "false");
		props.put("mail.imap.ssl.trust", "*");
		props.put("mail.pop3.ssl.checkserveridentity", "false");
		props.put("mail.pop3.ssl.trust","*");
					
		this.session = Session.getInstance(props, new Authenticator(){  
			protected PasswordAuthentication getPasswordAuthentication() {
			    return new PasswordAuthentication(userAccount, mailbox.getPassword());
			}
		});
	}
	
	public DropifiMailer(String mailserver, String email,String username,String password,String hostType,String host,int port,
			String outHostType,String outHost,Integer outPort,String authName){
		
		this(new MbSerializer(mailserver, email,username,password, hostType, host, port, outHostType, outHost, outPort,authName));
	}
	
	public DropifiMailer(DropifiMailbox mailbox){
		// Setup properties
		this(new MbSerializer(mailbox));
	}
	
	public DropifiMailer(String email,String fullName){
		this(new MbSerializer(email, fullName));
	}
 
	public static DropifiMailer getDefault(){	 
		return new DropifiMailer(DropifiMailbox.findByIdentifier(MailingType.Notification));
	}
	
	public static DropifiMailer getDefault(Connection conn) throws SQLException{ 
		return new DropifiMailer(DropifiMailbox.findByIdentifier(conn,MailingType.Notification)); 
	}
	
	//-----------------------------------------------------------------------------------------------------------
	//Mailers 
	
	public static int sendWidgetAutomaticNotification(Connection conn,String recipientName,String recipientEmail, RuleParameters params) throws SQLException{ 
		int notiStatus = 404;
		try{
			if(recipientEmail!=null && !recipientEmail.equals("other")){ 			
			
				//use the default dropifi mail setting to send a copy of the message to the company
				DropifiMailer ourMailer = new DropifiMailer(params.apikey,params.mailbox);
				 
				String compose = MailTemplate.forwarded(conn,params, "");
				 
				try {
					notiStatus = ourMailer.sendForwardMail(params.sender,params.getContactName(), recipientEmail, recipientName,params.subject, compose,params.getAttachedFile()); 
				}catch (Exception e) {
					//TODO Auto-generated catch block
					//try sending the message for the second time
					try {
						notiStatus = ourMailer.sendForwardMail(params.sender,params.getContactName(),recipientEmail, recipientName,params.subject, compose,params.getAttachedFile());
					} catch (Exception e1){ 					
						notiStatus = 501;
						play.Logger.log4j.info("could not send notification to company "+notiStatus);
					}
				}
	
			} 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return notiStatus;
	}
	
	public static int sendWidgetAutomaticNotification(Connection conn,String personal,String defaultEmail,Address[] recipients, RuleParameters params) throws SQLException{ 
		int notiStatus = 404;
		try{ 
			String compose = MailTemplate.forwarded(conn,params, "");  
			 
			//use the default dropifi mail setting to send a copy of the message to the company
			DropifiMailer ourMailer = new DropifiMailer(params.apikey,params.mailbox);
			ourMailer.setMgCampaignId(params.getMgCampaignId());
			
			try {
				notiStatus = ourMailer.sendForwardMail(params.sender,params.getContactName(), recipients,params.subject, compose,params.getAttachedFile());
			}catch (Exception e) {
				//TODO Auto-generated catch block
				//try sending the message for the second time
				try {
					notiStatus = ourMailer.sendForwardMail(params.sender,params.getContactName(),recipients,params.subject, compose,params.getAttachedFile());
				} catch (Exception e1){ 					
					notiStatus = 501;
					play.Logger.log4j.info("could not send notification to company " + notiStatus);
				}
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return notiStatus;
	}
	
	public static int sendFowardAutomaticNotification(Connection conn,String recipientEmail, RuleParameters params, String recipientName) throws SQLException{ 
		int notiStatus = 404;
		try{
			if(recipientEmail!=null && !recipientEmail.isEmpty()) {			

				String compose =  MailTemplate.reRouted(conn,params );
				
				//use the default dropifi mail setting to send a copy of the message to the company
				DropifiMailer ourMailer = new DropifiMailer(params.apikey,params.mailbox);
				ourMailer.setMgCampaignId(params.getMgCampaignId());
				
				try {
					notiStatus = ourMailer.sendForwardMail(params.sender,params.getContactName(), recipientEmail, recipientName,params.subject, compose,params.getAttachedFile());
				}catch (Exception e) { 
	
					//TODO Auto-generated catch block
					//try sending the message for the second time
					try {					
						notiStatus = ourMailer.sendForwardMail(params.sender,params.getContactName(),recipientEmail, "",params.subject, compose,params.getAttachedFile());
					} catch (Exception e1) {		
						notiStatus = 501;   
					}
				}
	
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return notiStatus;
	}

	public static int sendWidgetAutomaticResponse(Connection conn,WidgetRespSerializer widget) throws SQLException{
		//send a response to the sender of the message using the company's default mailbox;
		
		int status = 501;
		try{
			if(widget!=null &&  widget.getResponseActivated()) {
				ClientSerializer cs =widget.getClientSerializer();		
			 
				//send message acknowledgment to sender of widget message
				//used the default company widget subject and message 
				//DropifiMailer.widgetAutoResponseTemplate(cs.getEmail(), cs.getName(), widget.getDefaultMessage());	
				
				String compose = MailTemplate.autoResponse(conn,widget.getProfile(),cs.getEmail(), cs.getName(), widget.getDefaultSubject(), widget.getDefaultMessage());						
				DropifiMailer mailer  = DropifiMailer.getDefault(conn);
				try {
					status = mailer.sendSimpleMail(cs.getEmail(),cs.getName(), widget.getDefaultSubject(), compose,null);								
				}catch (Exception e) {
					// TODO Auto-generated catch block
					//try sending the message for the second time
					try {
						status = mailer.sendSimpleMail(cs.getEmail(),cs.getName(), widget.getDefaultSubject(),compose,null);				
					}catch (Exception e1) {
						// TODO Auto-generated catch block
						status = 404; 
					}
				}			
			}else{
				status = 201;
			} 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return status;
	}
	
	public static int sendWidgetAutomaticResponse(Connection conn,WidgetRespSerializer widget, MbSerializer mb, Map<String,String> autoReplyTo) throws SQLException{
		//send a response to the sender of the message using the company's default mailbox; 
		
		int status = 501;
		try{
			if(widget!=null &&  widget.getResponseActivated() && widget.getDefaultMessage()!=null && !widget.getDefaultMessage().trim().isEmpty()) { 
				String subject = widget.getDefaultSubject()!=null? widget.getDefaultSubject():"";
				ClientSerializer cs = widget.getClientSerializer();	
			 
				//send message acknowledgment to sender of widget message
				//used the default company widget subject and message 
				
				DropifiMailer mailer  = new DropifiMailer(widget.getApikey(),mb);
				mailer.setMgCampaignId(autoReplyTo.get("msgCampaignId"));
				
				String compose = MailTemplate.autoResponse(conn,widget.getProfile(),cs.getEmail(), cs.getName(), subject, widget.getDefaultMessage());
				
				//set the email and name of the person the message should be reply to
				String autoEmail =null, autoFullname =null;
				if(autoReplyTo!=null){
					autoEmail = autoReplyTo.get("email");
					autoFullname = autoReplyTo.get("fullname");
				}
				
				try {		
					
					status = mailer.sendAutoResponseMail(cs.getEmail(),cs.getName(), widget.getDefaultSubject(), compose,autoEmail,autoFullname);								
				}catch (Exception e) {
					// TODO Auto-generated catch block 
					//try sending the message for the second time
					status = 201;
					try {
						status = mailer.sendAutoResponseMail(cs.getEmail(),cs.getName(), widget.getDefaultSubject(),compose,autoEmail,autoFullname); 				
					}catch (Exception e1) {
						// TODO Auto-generated catch block	
						status = 201;  
					}
				}			 
			}else{
				status = 201;
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			status =201;
		}
		return status;
	}
	
	public static int sendWidgetAutomaticResponse(Connection conn,CoProfileSerializer compProfile,String email, String personal,String subject, String message, MbSerializer mailbox,Map<String,String> autoReplyTo){
		int status = 501;
		try{
			if(message==null){
				status = 200; 
			}else{
				
				subject =subject!=null?subject:"";
				DropifiMailer mailer  = new DropifiMailer(compProfile.getApikey(),mailbox); 
				
				//DropifiMailer.widgetAutoResponseTemplate(email, personal, message);
				String compose = MailTemplate.autoResponse(conn,compProfile,email, personal, subject, message);
				
				//set the email and name of the person the message should be reply to
				String autoEmail =null, autoFullname =null;
				if(autoReplyTo!=null){
					autoEmail = autoReplyTo.get("email");
					autoFullname = autoReplyTo.get("fullname");
				}
				
				try {
					status = mailer.sendAutoResponseMail(email, personal, subject, compose,autoEmail,autoFullname); 			 
				} catch(Exception e) {
					// TODO Auto-generated catch block
					status = 201;
					try{
						status = mailer.sendAutoResponseMail(email, personal, subject, compose,autoEmail,autoFullname);
						 
					}catch(Exception e1){
						play.Logger.log4j.error(e.getMessage(), e1);
					}			
				}
			}
		}catch(Exception e){
			status = 201;
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return status; 
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------
		
	//establish an inbound connection to a mailing server
	public Store connectStore() throws MessagingException, IOException {	
		setUserAccount(); 
		//connect to inbound store	
		this.store = session.getStore(this.mailbox.getHostType());	
		this.store.connect(this.mailbox.getHost(),this.mailbox.getPort(),this.userAccount, this.mailbox.getPassword());		 
		return store; 
	}
	
	//establish an outbound connection to a mailing server 
	public SMTPTransport  connectTransport(boolean hasPort) throws MessagingException, IOException{  
		this.transport =(SMTPTransport) session.getTransport(this.mailbox.getOutHostType());
		if(hasPort)
			this.transport.connect(this.mailbox.getOutHost(),this.mailbox.getOutPort(), this.userAccount, this.mailbox.getPassword()); 		 
		else
			this.transport.connect(this.mailbox.getOutHost(),this.userAccount,this.mailbox.getPassword());
		
		return transport;
	}
		
	public void setMailbox(MbSerializer mailbox) {
		this.mailbox = mailbox;
	}
	
	public MbSerializer getMailbox() {
		return mailbox;
	}
	
	
	//send a message using the default mailing address
	public int sendSimpleMail(String recipientEmal, String recipientName, String subject, String message,List<AttSerializer> fileAttachments) {
		int status = 501;
		try{
			//create a MimeMessage
			MimeMessage msg = new MimeMessage(this.session);
			Multipart multipart = new MimeMultipart();

			MimeBodyPart htmlPart = new MimeBodyPart();
	        htmlPart.setContent(new String(message.getBytes("UTF8"),"ISO-8859-1"), "text/html");
	        multipart.addBodyPart(htmlPart);
			
	        //attached files the message
	        this.setFileAttachment(fileAttachments, multipart);
	        
			//msg.setHeader("Content-Type", "text/html; charset=UTF-8");
			msg.setFrom(new InternetAddress(this.getMailbox().getEmail(), this.getMailbox().getUsername())); 
			
			if(recipientName!=null && recipientName.trim()!="")
				msg.addRecipient(RecipientType.TO, new InternetAddress(recipientEmal,recipientName));
			else
				msg.addRecipient(RecipientType.TO, new InternetAddress(recipientEmal));
			
			msg.setSubject(subject,"utf-8");
			msg.setContent(multipart); 
			//msg.setContent(message,"text/html"); 
			msg.setSentDate(new Date()); 
			//send the message to the recipients
			status = mailTo(msg,message); 
			
		}catch(SendFailedException e){
			play.Logger.log4j.info(e.getMessage(), e); 
			//e.printStackTrace();
			status =309; 
		}catch (MessagingException e) { 
			//TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(), e); 
			//e.printStackTrace();
			status =404;
		}catch(Exception e){
			status = 404;
			play.Logger.log4j.info(e.getMessage(),e); 
			//e.printStackTrace();
		}
		return status;
	}
	
	//send a message using the default mailing address
	public int sendAutoResponseMail(String recipientEmal, String recipientName, String subject, String message,String senderEmail,String senderName) {
			int status = 501;
			try{
				//create a MimeMessage
				MimeMessage msg = new MimeMessage(this.session);
				Multipart multipart = new MimeMultipart();

				MimeBodyPart htmlPart = new MimeBodyPart();
		        htmlPart.setContent(new String(message.getBytes("UTF8"),"ISO-8859-1"), "text/html");
		        multipart.addBodyPart(htmlPart);
				
				msg.setHeader("Content-Type", "text/html; charset=UTF-8");
				msg.setFrom(new InternetAddress(this.getMailbox().getEmail(), this.getMailbox().getUsername())); 
				
				if(recipientName!=null && recipientName.trim()!="")
					msg.addRecipient(RecipientType.TO, new InternetAddress(recipientEmal,recipientName));
				else
					msg.addRecipient(RecipientType.TO, new InternetAddress(recipientEmal));
				
				if(senderEmail!=null  && senderName!=null){
					Address[] addresses = new InternetAddress[1];				
					addresses[0] = new InternetAddress(senderEmail,senderName);
					msg.setReplyTo(addresses);
				}
				
				msg.setSubject(subject,"utf-8");
				msg.setContent(multipart); 
				msg.setSentDate(new Date());
				
				//send the message to the recipients
				status = mailTo(msg, message); 
				
			}catch(SendFailedException e){
				play.Logger.log4j.info(e.getMessage(), e);
				status =201;
			}catch (MessagingException e) { 
				//TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(), e);
				status =201;
			}catch(Exception e){
				status = 201; 
				play.Logger.log4j.info(e.getMessage(),e);
			}
			return status;
	}
	
	//use the default dropifi mailing setting to send an email on behalf of the sender of the message	
	public int sendForwardMail(String senderEmail,String senderName,Address[] recipients, String subject, String message, AttSerializer fileAttachment) { 
		try {
			//create a MimeMessage
			MimeMessage msg = new MimeMessage(this.session);
			Multipart multipart = new MimeMultipart();

			MimeBodyPart htmlPart = new MimeBodyPart();
	        htmlPart.setContent(new String(message.getBytes("UTF8"),"ISO-8859-1"), "text/html");
	        multipart.addBodyPart(htmlPart);
	         
	        try{  
	        	if(fileAttachment!=null && fileAttachment.getFileBlob()!=null){
	        		
	        		if(fileAttachment.fileName==null)fileAttachment.fileName=fileAttachment.getFileBlob().getFile().getName();
	        		
	        		MimeBodyPart attachment = new MimeBodyPart();
		        	DataSource source =    new FileDataSource(fileAttachment.getFileBlob().getFile());  
		        	attachment.setDataHandler(new DataHandler(source));
		        	attachment.setFileName(fileAttachment.fileName); 
		        	attachment.setHeader("CONTENT-ID", "file-attachment");
		        	multipart.addBodyPart(attachment);  
	        	}
	        }catch(Exception e){ 
	        	play.Logger.log4j.info(e.getMessage(),e);
	        }
	        
			//msg.setHeader("Content-Type", "text/html; charset=UTF-8");
			//msg.setFrom(new InternetAddress(this.getMailbox().getEmail()));
	        
			//set the sender of the message			
			if(senderEmail != null && !senderEmail.isEmpty()){
				
				msg.setFrom(new InternetAddress(this.getMailbox().getEmail(),senderName)); 			
				Address[] addresses = new InternetAddress[1];
				
				addresses[0] = new InternetAddress(senderEmail,senderName);
				msg.setReplyTo(addresses);
								 
			}else{
				return 503;
			}
			
			msg.addRecipient(RecipientType.TO, recipients[0]); 
			
			for(int i =1;i<recipients.length;i++){ 
				msg.addRecipient(RecipientType.CC, recipients[i]); 
			}
			
			//set the subject and content of the message
			msg.setSubject(subject,"utf-8");
			msg.setContent(multipart);
			msg.setSentDate(new Date());
			 
			 
			//send the message to the recipients
			return mailTo(msg,message);			
			
		}catch (MessagingException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return 501; 
	}
	
	//use the default dropifi mailing setting to send an email on behalf of the sender of the message	
	public int sendForwardMail(String senderEmail,String senderName, String recipientEmail,String recipientName, String subject, String message, AttSerializer attachedFile) {
		try {
			Address[] recipients = new InternetAddress[1];
			recipients[0] = new InternetAddress(recipientEmail, recipientName);
			return sendForwardMail(senderEmail,senderName,recipients,subject,message,attachedFile);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.error(e.getMessage(), e);
		}
		return 501;
	}
	 
	public int sendToMany(InternetAddress[] recipients, String subject,String message, AttSerializer fileAttachment)  throws AddressException, MessagingException, IOException{
	
		//create a MimeMessage
		MimeMessage msg = new MimeMessage(this.session);
		Multipart multipart = new MimeMultipart("alternative");

		MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(new String(message.getBytes("UTF8"),"ISO-8859-1"), "text/html");
        multipart.addBodyPart(htmlPart);
		
        //attached files the message
        this.setFileAttachment(fileAttachment, multipart);
        
		//msg.setHeader("Content-Type", "text/html; charset=UTF-8");
		msg.setFrom(new InternetAddress(this.getMailbox().getEmail(), this.getMailbox().getUsername()));
		
		//create recipients 		 	
		msg.addRecipients(RecipientType.TO, recipients);
		msg.setSubject(subject,"utf-8");
		msg.setContent(multipart);
		msg.setSentDate(new Date());
		
		//send the message to the recipients
		return mailTo(msg,message);
	}
	
	/**
	 * send an email to many recipients using TO, CC, BCC
	 * @param recipients
	 * @param ccs
	 * @param bccs TO,
	 * @param subject
	 * @param message
	 * @return
	 * @throws AddressException
	 * @throws MessagingException
	 * @throws IOException
	 */
	
	public int sendToMany(InternetAddress[] recipients, InternetAddress[] ccs,InternetAddress[] bccs,  String subject,String message, List<AttSerializer> fileAttachments) 
			throws AddressException, MessagingException, IOException{
		
		//create a MimeMessage
		MimeMessage msg = new MimeMessage(this.session);
		Multipart multipart = new MimeMultipart();

		MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(new String(message.getBytes("UTF8"),"ISO-8859-1"), "text/html");
        multipart.addBodyPart(htmlPart);
		
        //attached files the message
        this.setFileAttachment(fileAttachments, multipart);
        
		//msg.setHeader("Content-Type", "text/html; charset=UTF-8");
		msg.setFrom(new InternetAddress(this.getMailbox().getEmail() , this.getMailbox().getUsername()));
		
		//create recipients 		 	
		msg.addRecipients(RecipientType.TO, recipients);		
		
		if(ccs!=null && ccs.length>0){ 
			msg.addRecipients(RecipientType.CC, ccs); 
		}
		
		if(bccs!=null && bccs.length>0){
			msg.addRecipients(RecipientType.BCC, bccs);
		}
		
		msg.setSubject(subject,"utf-8");
		msg.setContent(multipart);
		msg.setSentDate(new Date());
				
		//send the message to the recipients
		return mailTo(msg,message);
	}
	
	/**
	 * send an email to many recipients using TO, CC, BCC
	 * @param recipients
	 * @param ccs
	 * @param bccs
	 * @param subject
	 * @param message
	 * @return
	 * @throws AddressException
	 * @throws MessagingException
	 * @throws IOException
	 */
	public int sendToMany(String recipients, String  ccs, String  bccs,  String subject,String message, List<AttSerializer> fileAttachments) 
			throws AddressException, MessagingException, IOException{
		
		//create a MimeMessage
		MimeMessage msg = new MimeMessage(this.session);
		Multipart multipart = new MimeMultipart();

		MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(new String(message.getBytes("UTF8"),"ISO-8859-1"), "text/html");
        multipart.addBodyPart(htmlPart);
	
        //attached files to the message
        this.setFileAttachment(fileAttachments, multipart);
        
		//msg.setHeader("Content-Type", "text/html; charset=UTF-8");
		msg.setFrom(new InternetAddress(this.getMailbox().getEmail(), this.getMailbox().getUsername()));

		//create TO recipients 	
		if(recipients!=null && !recipients.trim().isEmpty())
			msg.addRecipients(RecipientType.TO,  DropifiResponse.getAddresses(recipients));
		
		//create CC recipients 	
		if(ccs!=null && !ccs.trim().isEmpty())
			msg.addRecipients(RecipientType.CC, DropifiResponse.getAddresses(ccs)); 
		
		//create BCC recipients 	
		if(bccs!=null && !bccs.trim().isEmpty())
			msg.addRecipients(RecipientType.BCC, DropifiResponse.getAddresses(bccs));
		
		msg.setSubject(subject,"utf-8");
		msg.setContent(multipart); 
		msg.setSentDate(new Date());
	     
		//send the message to the recipients
		return mailTo(msg,message);
	}
	
	public int sendToMany(InternetAddress from,String recipients, String  ccs, String  bccs,  String subject,String message, AttSerializer fileAttachment) 
			throws AddressException, MessagingException, IOException{
		
		//create a MimeMessage
		MimeMessage msg = new MimeMessage(this.session);
		Multipart multipart = new MimeMultipart();

		MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(new String(message.getBytes("UTF8"),"ISO-8859-1"), "text/html");
        multipart.addBodyPart(htmlPart);
      
        //attached files the message
        this.setFileAttachment(fileAttachment, multipart);
        
		//msg.setHeader("Content-Type", "text/html; charset=UTF-8");
		msg.setFrom(from);

		//create TO recipients 	
		if(recipients!=null && !recipients.trim().isEmpty())
			msg.addRecipients(RecipientType.TO,  DropifiResponse.getAddresses(recipients));
		
		//create CC recipients 	
		if(ccs!=null && !ccs.trim().isEmpty())
			msg.addRecipients(RecipientType.CC, DropifiResponse.getAddresses(ccs)); 
		
		//create BCC recipients 	
		if(bccs!=null && !bccs.trim().isEmpty())
			msg.addRecipients(RecipientType.BCC, DropifiResponse.getAddresses(bccs));
		
		msg.setSubject(subject,"utf-8");
		msg.setContent(multipart); 
		msg.setSentDate(new Date());
	    msg.setSender(from);
		//send the message to the recipients
		return mailTo(msg,message);
	}
	
	public void setFileAttachment(List<AttSerializer> fileAttachments,Multipart multipart){
		try{
			if(fileAttachments!=null && multipart!=null){
				for(AttSerializer fileAttachment : fileAttachments){
		        	if(fileAttachment!=null && fileAttachment.getFileBlob()!=null){
		        		
		        		if(fileAttachment.fileName==null)fileAttachment.fileName=fileAttachment.getFileBlob().getFile().getName();
		        		
		        		MimeBodyPart attachment = new MimeBodyPart();
			        	DataSource source =    new FileDataSource(fileAttachment.getFileBlob().getFile());  
			        	attachment.setDataHandler(new DataHandler(source));
			        	attachment.setFileName(fileAttachment.fileName);
			        	attachment.setHeader("CONTENT-ID", "file-attachment");
			        	multipart.addBodyPart(attachment);
		        	}
		    	}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public void setFileAttachment(final AttSerializer fileAttachment,Multipart multipart){
		//List<AttSerializer> fileAttachments = 
		setFileAttachment(new LinkedList<AttSerializer>(){{ add(fileAttachment); }} , multipart);
	}
	
	private int mailTo(MimeMessage msg,String m) throws SendFailedException, MessagingException, IOException{
		
		if(this.awsMail!=null && this.isVerifiedEmail && this.mailingProvider.equals(MailingProvider.AWSSES)){
			//send the message using aws simple email sending settings only if the email is verified
			return this.awsMail.mailTo(msg);
		}else if(this.gmailSMTP!=null && this.mailingProvider.equals(MailingProvider.GMAILOAUTH)){
			//send the message using google authentication API
			return DGOauthMail.sendMail(this.gmailSMTP, msg);
		} 
		
		if(this.mailbox.getMailserver().equals("Mailgun")){
			msg.setHeader("X-Mailgun-Campaign-Id", this.getMgCampaignId());
		}
		
		Transport transport = this.connectTransport(false);
		//if(transport.isConnected()){ 
		msg.saveChanges();				 
		transport.sendMessage(msg, msg.getAllRecipients());				 
		transport.close();
			return 200; 
		// } 
		// return 404;
	} 

	public InternetAddress[] getAddresses(Collection<String> recipients){ 
		InternetAddress[] repAddress = new InternetAddress[recipients.size()];
		
		int i=0;
		for(String rep :recipients ){
			try {
				repAddress[i++] = new InternetAddress(rep);
			}catch (AddressException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}		
		return repAddress;
	}
	
	public InternetAddress[] getAddresses(String recipients){
		String[] reps = recipients.split(",");
		
		InternetAddress[] repAddress = new InternetAddress[reps.length];
 
		for(int i=0; i<reps.length;i++){
			try {
				repAddress[i++] = new InternetAddress(reps[i]); 
			} catch (AddressException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}		
		return repAddress;
	}
	
	private static int hasAuthenticationError(String message){
		if(message==null)
			return 608;
		
		message = message.toLowerCase(); 
		if(message.contains("authentication failed") || (message.contains("authentication") && message.contains("failed")))
			return 205;
		
		return 608;
	}
	//check that the mailbox settings are correct 
	public static int checkConnection(InboundMailbox mailbox){
		
		if(mailbox !=null){	
			try{
				DropifiMailer mailer = new DropifiMailer(mailbox,true,false); 
				//check if the mailbox setting is valid and exist
				
				boolean intrue = false;
				 
					try {
						intrue =true;// mailer.connectStore().isConnected();
						//System.out.println("inbound pass test: "+ intrue);
					} catch ( Exception e) {
						// TODO Auto-generated catch block
						//System.out.println("1. Inbound..............................................: "+ intrue+" \n");
						//System.out.println("Message: "+e1.getMessage()+"\n");
						//e1.printStackTrace();
						//play.Logger.log4j.error("Inbound: == "+e.getMessage(),e);
						return hasAuthenticationError(e.getMessage());
					} 
				
				boolean outtrue = false;
				 
					try {
						outtrue = mailer.connectTransport(true).isConnected();
						//System.out.println("outbound pass test: "+ outtrue);
					} catch (MessagingException e) {
						// TODO Auto-generated catch block 
						//e.printStackTrace();
						play.Logger.log4j.error("Outbound: == "+e.getMessage(),e);
						return hasAuthenticationError(e.getMessage());
					} catch (IOException e) {
						// TODO Auto-generated catch block 
						//e.printStackTrace();
						play.Logger.log4j.error("Outbound: == "+e.getMessage(),e);
						return hasAuthenticationError(e.getMessage());
					} //outgoing	 
				
				if(intrue && outtrue){
					return 200;
				
				}else{ //mailbox validation failed. could not connect to mail mailbox
					
					if(intrue==false && outtrue==false){ 
						return 609;
					}else if(intrue==false){
						return 610;
					}else if(outtrue==false){ 
						return 611;
					} 					 
				}			
		  } catch (Exception e) {
				// TODO Auto-generated catch block
			  	//e.printStackTrace();
				play.Logger.info(e.getMessage(), e);
				return 608;  			 
			} 
		}
		return 612;
	}
	
	public static int runSpamFilter(String u,String m){
		ymsg.support.AntiSpam spam = new AntiSpam();		 
		return spam.getViolations(u,m);	 
	}
	
	public boolean testSendingEmail(String to ,String from){
		ServerSetup setup =  new ServerSetup(this.getMailbox().getOutPort(), this.getMailbox().getEmail(), this.getMailbox().getOutHostType(),this.getMailbox().getOutHost());
		GreenMail greenMail= new GreenMail();
		greenMail.setUser(this.getMailbox().getEmail(),this.getMailbox().getEmail(),this.getMailbox().getPassword()); 		 
		//greenMail.start();
		 
		MimeMessage  msg =  GreenMailUtil.sendTextEmail(to, from, "subject", "message", this.session);
	 	boolean b = false;
		if(msg!=null )
			b ="body".equals(GreenMailUtil.getBody(msg));
		
		//greenMail.stop();
		return b;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	private void setUserAccount(){
		this.userAccount = mailbox.getEmail();
		if(mailbox.getAuthName() != null && !mailbox.getAuthName().trim().isEmpty())
			this.userAccount = mailbox.getAuthName();  
	}

	public String getMgCampaignId() {
		return mgCampaignId;
	}

	public void setMgCampaignId(String mgCampaignId) {
		this.mgCampaignId = mgCampaignId;
	}
}
