package adminModels;
/**
 * Provider mailing server settings for used by Dropifi sending email to company
 * @author phillips
 */

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
 
import com.google.gson.annotations.SerializedName;

import play.data.validation.*;
import play.db.jpa.Model;
import resources.Dcrypto;
import resources.SQLInjector;
import view_serializers.MbSerializer;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.*;

@Entity
public class DropifiMailbox extends Model {	 
	
	@SerializedName("mailserver") 
	@Column(nullable =false, unique=true)
	private MailingType identifier;
	
	@SerializedName("mailserver") 
	@Column(nullable =false)
	private String mailserver;
	
	@SerializedName("username") 
	@Column(nullable=false) @Required
	private String username; 
	
	@Column(nullable=false, length=100)  @Required @Email
	@SerializedName("email") 
	private String email;
	
	@SerializedName("password") 
	@Column(nullable=false, length=100) @Required
	private String password;
	
	@Column(nullable=false, length=100) @Required
	@SerializedName("hosttype") 
	private String hostType;
	
	@SerializedName("host") 
	@Column(nullable=false, length=100) @Required
	private String host;
	
	@SerializedName("port") 
	@Column(nullable=false) @Required
	private Integer port;	
  	
	@Required @Column(nullable=false)
	private byte[] secureKey; 
	
	@SerializedName("authName") 
	private String authName;
	
	public DropifiMailbox(MailingType identifier,String mailserver,String username, String email, String password,String hostType, 
			String host, Integer port,String authName) throws NoSuchAlgorithmException { 
		this(identifier,mailserver,username,email,password,hostType,host,port,true);
		this.setAuthName(authName);
	}
	public DropifiMailbox(MailingType identifier,String mailserver,String username, String email, String password,String hostType, 
			String host, Integer port) throws NoSuchAlgorithmException { 
		this(identifier,mailserver,username,email,password,hostType,host,port,true); 
	}
	
	public DropifiMailbox(MailingType identifier,String mailserver,String username, String email, String password,String hostType, 
			String host, Integer port, boolean activated) throws NoSuchAlgorithmException { 
		
		this.setSecureKey();
		this.setIdentifier(identifier);
		this.setMailserver(mailserver);
		this.setUsername(username);
		this.setEmail(email);
		this.setEncryptPassword(password);
		this.setHostType(hostType);
		this.setHost(host);
		this.setPort(port);
	}
	
	public DropifiMailbox(){}
	//-----------------------------------------------------------------------------------
 
	public int createMailBox(){
		// TODO Auto-generated method stub
		DropifiMailbox found = DropifiMailbox.findByIdentifier(this.getIdentifier());
		if(found == null){
			return this.validateAndSave()?200:501; 
		}else{
			found.setSecureKey(this.getSecureKey()); 
			found.setMailserver(this.getMailserver());
			found.setHost(this.getHost());
			found.setHostType(this.getHostType());
			found.setPort(this.getPort());
			found.setEmail(this.getEmail());
			found.setUsername(this.getUsername());
			found.setPassword(this.getPassword()); 
			found.setAuthName(this.getAuthName());
			return found.validateAndSave()?200:501;  
		}
	}
	
	public static boolean isSetup(){
		return DropifiMailbox.findAll().size()>0;
	}
	
	public static DropifiMailbox findByIdentifier(MailingType mailType){ 
		DropifiMailbox mailbox =  DropifiMailbox.find("byIdentifier", mailType).first();
		mailbox.decrpytPassword(mailbox.getPassword());
		return mailbox;
	}
	
	public static DropifiMailbox findByIdentifier(Connection conn,MailingType mailType) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select * from DropifiMailbox m where m.identifier =?");
			query.setInt(1, mailType.ordinal());			
			ResultSet result = query.executeQuery();
			while(result.next()){
				DropifiMailbox mailbox = new DropifiMailbox();
				//mailbox.setIdentifier(result.getidentifier);
				mailbox.setMailserver(result.getString("mailserver"));
				mailbox.setUsername(result.getString("username"));
				mailbox.setEmail(result.getString("email"));
				mailbox.setSecureKey(result.getBytes("secureKey")); 
				//mailbox.setPassword(result.getString("password"));
				mailbox.decrpytPassword(result.getString("password"));				
				mailbox.setHostType(result.getString("hostType"));
				mailbox.setHost(result.getString("host"));
				mailbox.setPort(result.getInt("port"));
				mailbox.setAuthName(result.getString("authName"));
				return mailbox; 
			} 
			
		}
		return null; 
	}
	
	public static String findEmailByIdentifier(Connection conn,MailingType mailType) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select m.email from DropifiMailbox m where m.identifier =?");
			query.setInt(1, MailingType.Notification.ordinal());			
			ResultSet result = query.executeQuery();
			while(result.next()){
				 String email = result.getString("email");
				 return (email != null)?email.trim():"";
			} 			
		}
		return null;
	}
	
	
	
	//----------------------------------------------------------------------------------
	//getters and setters
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username =  username ;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email =  email.trim();
	}

	public String decryptPassword() {
		SecretKeySpec key = new SecretKeySpec(this.getSecureKey(), "DES");		
		Dcrypto decrypter = new Dcrypto(key); 
		return  decrypter.decryptDES(this.password);
	}
	
	public String getPassword(){
		return this.password;
	}
	
	public void decrpytPassword(String password){
		//decrypt the password
		SecretKeySpec key = new SecretKeySpec(this.getSecureKey(), "DES");		
		Dcrypto decrypter = new Dcrypto(key);
		this.password = decrypter.decryptDES(password);
	}
 
	
	public void setEncryptPassword(String password) {
		//encrypt the password
		if(password !=null){
			SecretKeySpec key = new SecretKeySpec(this.getSecureKey(), "DES");
			Dcrypto encrypter = new Dcrypto(key);		
			this.password = encrypter.encryptDES(password);
		}
	}
	
	public void setPassword(String password){
		this.password = password;
	}

	public String getHostType() {
		return hostType.trim();
	}

	public void setHostType(String hostType) {
		this.hostType = hostType.trim();
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host.trim();
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
 
	public String getMailserver() {
		return mailserver;
	}

	public void setMailserver(String mailserver) {
		this.mailserver = mailserver;
	}

	public MailingType getIdentifier() {
		return identifier;
	}

	public void setIdentifier(MailingType identifier) {
		this.identifier = identifier;
	}
 	
	public byte[] getSecureKey() {
		return secureKey;
	}
	
	private void setSecureKey() throws NoSuchAlgorithmException{
		SecretKey key = KeyGenerator.getInstance("DES").generateKey();
		this.secureKey = key.getEncoded();	 	 
	}
	
	private void setSecureKey( byte[] secureKey){
		this.secureKey = secureKey;
	}

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}
}