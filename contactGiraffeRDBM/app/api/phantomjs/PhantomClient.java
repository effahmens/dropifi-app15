package api.phantomjs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class PhantomClient {

	public static String generateImage(String folderName, String screenShot){
		
		return execute("phantomjs rasterize.js bucket/scr.html shot.png");
	}
	
	public static String execute(String command){
		String result = null;
		try {
			Process p = Runtime.getRuntime().exec(command);  
			p.waitFor(); 
			BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			String line=reader.readLine();
			while(line!=null)  { 
				result += (line+"\n");
				line=reader.readLine(); 
			} 
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			result = e.getMessage();
			//play.Logger.log4j.info(e.getMessage(),e);
			e.printStackTrace();
		}  
		return result; 
	}
	
	/**
	 * create a folder if it doesn't exist 
	 */
	public static void create(String folder){
		folder = "company/"+folder+"_";
		long time = new Date().getTime();
		//System.out.println(execute("mkdir "+folder));
		System.out.println(execute("touch "+folder+time+".html"));
		System.out.println(execute("echo 'kofi is a boyer' >> "+folder+time+".html"));
	}
	
	public static void main(String[] args) { 
		create("banana");
		//System.out.println(execute("sed --version"));
	}
	
}
