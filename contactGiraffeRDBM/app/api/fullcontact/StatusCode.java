package api.fullcontact;

public class StatusCode {

	public static int EMAIL_IN_SEARCH = 202;

	public static int EMAIL_NOT_FOUND = 404;

	public static int INVALID_API_KEY = 403;

	public static int INVALID_EMAIL_ADDR = 422;

	public static int REQ_SUCCESS = 200;
}
