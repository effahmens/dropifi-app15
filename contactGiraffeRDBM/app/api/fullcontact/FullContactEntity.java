package  api.fullcontact;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class FullContactEntity implements Serializable {

	@SerializedName("contactInfo")
	private ContactInfo contactInfo;

	@SerializedName("demographics")
	public Demographics demographics;
	
	@SerializedName("socialProfiles")
	private List<SocialProfiles> socialProfiles;
	
	@SerializedName("organizations")
	private List<Organizations> organizations;

	@SerializedName("photos")
	private List<Photos> photos; 	
	
	@SerializedName("status")
	private int statusCode;
 
	//getters and setters
	public int getStatusCode() {
		return statusCode;
	}

	@Override
	public String toString() {
		return "FullContactEntity [contactInfo=" + contactInfo
				+ ", demographics=" + demographics + ", socialProfiles="
				+ socialProfiles + ", organizations=" + organizations
				+ ", photos=" + photos + ", statusCode=" + statusCode + "]";
	}

	
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public ContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public List<Organizations> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<Organizations> organizations) {
		this.organizations = organizations;
	}

	public List<SocialProfiles> getSocialProfiles() {
		return socialProfiles;
	}

	public void setSocialProfiles(List<SocialProfiles> socialProfiles) {
		this.socialProfiles = socialProfiles;
	}

	public List<Photos> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photos> photos) {
		this.photos = photos;
	}

	public Demographics getDemographics() {
		return this.demographics;
		/*if(demographics instanceof LinkedHashMap){
			LinkedHashMap<String, String> value = (LinkedHashMap<String, String>)demographics;
			
			Demographics demo = new Demographics();
			
			if(value !=null){
				
				demo.setAge(value.get("age"));
				demo.setAgeRange(value.get("ageRange"));
				demo.setGender(value.get("gender"));
				demo.setMaritalStatus(value.get("maritalStatus"));
				demo.setInfluencerScore(value.get("influencerScore"));
				
				Object object = value.get("locationGeneral");
				if(object instanceof ArrayList){
					ArrayList<String> locas =(ArrayList<String>) object;
					for(String loca:locas){
						demo.setLocationGeneral(loca);
					}
				}else if(object instanceof String){
					demo.setLocationGeneral((String)object);
				}
				
				demo.setChildren(value.get("children"));
				demo.setHomeOwnerStatus(value.get("homeOwnerStatus"));
				demo.setHouseholdIncome(value.get("householdIncome"));
				demo.setEducation(value.get("education"));
			}
			
			return demo;
		}
		return null;*/
	}
	
	public void setDemographics( Demographics  demographics) {
		this.demographics = demographics;
	}
	  
}
