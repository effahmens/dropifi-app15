package api.fullcontact;

import com.google.gson.annotations.SerializedName;

public class SocialProfiles {

	@SerializedName("typeId")
	private String typeId;
	
	@SerializedName("typeName")
	private String typeName;
	
	@SerializedName("url")
	private String profileUrl;

	@SerializedName("id")
	private String profileId;

	@SerializedName("birthday")
	private String birthday;

	@SerializedName("username")
	private String username;

	@SerializedName("headline")
	private String headline;

	@SerializedName("connections")
	private int connections;

	@SerializedName("currentStatus")
	private String currentStatus;

	@SerializedName("currentStatusTimestamp")
	private String currentStatusTimestamp;

	@SerializedName("bio")
	private String bio;

	@SerializedName("following")
	private String following;
	
	@SerializedName("followers")
	private String followers;
	
	@SerializedName("rss")
	private String rss;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getProfileBday() {
		return birthday;
	}

	public void setProfileBday(String profileBday) {
		this.birthday = profileBday;
	}

	public String getProfileUsername() {
		return username;
	}

	public void setProfileUsername(String profileUsername) {
		this.username = profileUsername;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getHeadline() {
		return headline;
	}

	public void setConnections(int connections) {
		this.connections = connections;
	}

	public int getConnections() {
		return connections;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatusTimestamp(String currentStatusTimestamp) {
		this.currentStatusTimestamp = currentStatusTimestamp;
	}

	public String getCurrentStatusTimestamp() {
		return currentStatusTimestamp;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getBio() {
		return bio;
	}
	
	public Integer getFollowing() {
		if(following !=null)
			return Integer.parseInt(following);
		
		return 0;
	}

	public void setFollowing(String following) {
		this.following = following;
	}

	public Integer getFollowers() {
		if(followers !=null)
			return Integer.parseInt(followers);
		
		return 0;
	}

	public void setFollowers(String followers) {
		this.followers = followers;
	}

	public String getRss() {
		return rss;
	}

	public void setRss(String rss) {
		this.rss = rss;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

}
