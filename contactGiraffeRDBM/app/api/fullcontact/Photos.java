package api.fullcontact;

import com.google.gson.annotations.SerializedName;

public class Photos {
	
	@SerializedName("type")
	private String photoType;

	@SerializedName("url")
	private String photoUrl;
	
	@SerializedName("isPrimary")
	private boolean isPrimary;

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getPhotoType() {
		return photoType;
	}

	public void setPhotoType(String photoType) {
		this.photoType = photoType;
	}
	
	public Boolean getIsPrimary() {
		return isPrimary; 
	}
	
	public void setIsPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}


}
