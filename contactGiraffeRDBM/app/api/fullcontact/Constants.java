package  api.fullcontact;

public class Constants {

	public static final String API_URL = "https://api.fullcontact.com/v2/person.json?";

	public static final String API_KEY_FORMAT = "apiKey={0}";

	public static final String EMAIL_FORMAT = "email={0}";

	public static final String TIMEOUT_SECONDS_FORMAT = "timeoutSeconds={0}";

	public static final String UTF_8_CHARSET = "UTF-8";
}
