package api.fullcontact;

import com.google.gson.annotations.SerializedName;

public class Organizations {

	@SerializedName("name")
	private String name;

	@SerializedName("startDate")
	private String startDate;

	@SerializedName("title")
	private String title;

	@SerializedName("isPrimary")
	private boolean isPrimary;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
	
}
