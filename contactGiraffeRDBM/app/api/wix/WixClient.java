package api.wix;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.apache.commons.codec.binary.Base64;
import com.google.gson.JsonObject;

public class WixClient {

	// initialization
	public static JsonNode getInstance(String instance, String secret){
		try{
			SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(), "HMACSHA256");
			Mac mac = Mac.getInstance("HMACSHA256");
			mac.init(secretKeySpec);
			
			// split the signed-instance
			Base64 base64 = new Base64(256, null, true);
			int idx = instance.indexOf(".");
			String signature = instance.substring(0, idx);
			String encodedJson = instance.substring(idx+1);
			  
			byte[] sig = base64.decode(signature.getBytes());
			byte[] mySig = mac.doFinal(encodedJson.getBytes()); 
			if (!Arrays.equals(mySig, sig))  {
			    throw new Exception("signatures do not match"); 
			}else { 
			    //objectMapper is jackson interface for reading JSON - one JSON serialization library in java
				ObjectMapper objectMapper = new ObjectMapper();
			    String payload = new String(base64.decode(encodedJson));
			    return objectMapper.readTree(payload);
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public static String generateDomain(String baseUrl){
		if(baseUrl!=null && !baseUrl.trim().isEmpty())
			return baseUrl.replace("http", "").replace("https", "").replace("#", "").replace(":","").replace("//","").replace(".", "_").replace("/", "_");
		
		return null;
	}
	
	public static String getString(JsonNode json){
		try{
			return json.getTextValue();
		}catch(Exception e){}
		return null;
	}
	
	public static String getVendorId(JsonNode json){
		try{
			return json.getTextValue()!=null?json.getTextValue().trim():"";
		}catch(Exception e){ 
		}
		return null;
	}
	
	public  static String getEncodedInstance(){
		JsonObject json = new JsonObject();
        
		json.addProperty("instanceId", "130d3551-7613-a280-d871-130d2213635c");
		json.addProperty("signDate", new Date().getTime());
		json.add("uid", null);
		json.add("permissions", null);
		//json.addProperty("vendorProductId", "Dropifi-Premium");
		Base64 base64 = new Base64(256, null, true);		
		return base64.encodeAsString(json.toString().getBytes());
		 
	}
}
