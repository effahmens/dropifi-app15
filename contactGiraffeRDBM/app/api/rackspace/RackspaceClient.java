package api.rackspace;

import java.io.File; 
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobRequestSigner;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.domain.MutableBlobMetadata;
import org.jclouds.blobstore.domain.internal.MutableBlobMetadataImpl;
import org.jclouds.http.HttpRequest;
import org.jclouds.io.MutableContentMetadata;
import org.jclouds.io.Payload;
import org.jclouds.io.Payloads;
import org.jclouds.io.payloads.BaseMutableContentMetadata;
import org.jclouds.openstack.swift.v1.blobstore.RegionScopedBlobStoreContext;
import org.jclouds.openstack.swift.v1.domain.Container;
import org.jclouds.openstack.swift.v1.domain.SwiftObject;
import org.jclouds.openstack.swift.v1.features.AccountApi;
import org.jclouds.openstack.swift.v1.features.ContainerApi;
import org.jclouds.openstack.swift.v1.features.ObjectApi;
import org.jclouds.rackspace.cloudfiles.v1.CloudFilesApi;
import org.jclouds.rackspace.cloudfiles.v1.domain.CDNContainer;
import org.jclouds.rackspace.cloudfiles.v1.features.CDNApi;

import play.db.jpa.Blob;
import resources.DropifiTools; 

import com.google.api.client.util.Charsets;
import com.google.common.io.ByteSource;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;
 
//import org.jclouds.rackspace.cloudfiles.
public class RackspaceClient {
	 
	private final BlobStore blobStore; 
	private final CloudFilesApi cloudFiles;
	
	public RackspaceClient(String username, String apiKey) { 
		ContextBuilder builder = ContextBuilder.newBuilder(Constants.PROVIDER).credentials(username, apiKey);
		blobStore = builder.buildView(RegionScopedBlobStoreContext.class).getBlobStore(Constants.REGION); 
		cloudFiles = blobStore.getContext().unwrapApi(CloudFilesApi.class);
	}
	
	public CloudFilesApi GetFileClient() {  
		return cloudFiles;
	}
	
	public void uploadObject(String conatinerName,File file,String fileName) {
		try {
		 ByteSource byteSource = Files.asByteSource(file);
		 Payload  payload = Payloads.newByteSourcePayload(byteSource);
		/* MutableContentMetadata contentMetadata = new BaseMutableContentMetadata();
		 contentMetadata.setContentLength(byteSource.size()); 
		 MutableBlobMetadata blob = new MutableBlobMetadataImpl();
		 blob.setName(fileName);
		 blob.setContentMetadata(contentMetadata);
		 payload.set*/
		 GetFileClient().getObjectApi(Constants.REGION, conatinerName).put(fileName, payload);
		}catch(Exception e) {}
	} 
	
	public static void putPublicFile(String conatinerName, File file,String fileName) { 
		RackspaceClient client = new RackspaceClient(DropifiTools.RackspaceUsername, DropifiTools.RackspaceApikey);
		client.uploadObject(conatinerName, file, fileName); 
	}
	
	public String generateRackspaceFileUrl(String cdnContainer,String folder,String filename){  
		return cdnContainer+File.separator+folder+File.separator+filename+"?v="+new Date().getTime();
	}
	
	public File getObject(String conatinerName,String objectName) { 
		try {
			ObjectApi objectApi = GetFileClient().getObjectApi(Constants.REGION, conatinerName);
			
			SwiftObject object = objectApi.get(objectName);
			//Write the object to a file
			File file = File.createTempFile(objectName, null);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			ByteStreams.copy(object.getPayload().openStream(), fileOutputStream);
			return file;
		}catch(Exception e) { 
		}
		return null;
	} 
	
	public  static Blob getBlob(String conatinerName, String folderName,String fileName){
		try {
			RackspaceClient client = new RackspaceClient(DropifiTools.RackspaceUsername, DropifiTools.RackspaceApikey);
			ObjectApi objectApi = client.cloudFiles.getObjectApi(Constants.REGION, conatinerName+File.separator+folderName); 
			SwiftObject object = objectApi.get(fileName);
			Blob blob = new Blob(); 
			blob.set(object.getPayload().openStream(), object.getPayload().getContentMetadata().getContentType());
			return blob;
		}catch(Exception e) {}
		
		return null;
	}
	
	public  static URI getURL(String containerName, String folderName,String fileName){
		try {
			//RackspaceClient client = new RackspaceClient(DropifiTools.RackspaceUsername, DropifiTools.RackspaceApikey);
			// Create a new ContextBuilder
			ContextBuilder builder = ContextBuilder.newBuilder(Constants.PROVIDER)
			        .credentials(DropifiTools.RackspaceUsername, DropifiTools.RackspaceApikey);

			// Access the RegionScopedBlobStore and get the Cloud Files API
			BlobStore blobStore = builder.buildView(RegionScopedBlobStoreContext.class).getBlobStore(Constants.REGION);
			CloudFilesApi cloudFilesApi = blobStore.getContext().unwrapApi(CloudFilesApi.class);

			// Get the AccountApi and update the temporary URL key if not set
			AccountApi accountApi = cloudFilesApi.getAccountApi(Constants.REGION);
			accountApi.updateTemporaryUrlKey("jnRB6#1sduo8YGUF&%7r7guf6f");

			// Get the temporary URL
			BlobRequestSigner signer = blobStore.getContext().getSigner();
			HttpRequest request = signer.signGetBlob(containerName+File.separator+folderName, fileName);
			URI tempUrl = request.getEndpoint();
			return tempUrl;
		}catch(Exception e) {
			//e.printStackTrace(); 
		}
		
		return null;
	}
	
	public InputStream getObjectStream(String conatinerName,String objectName,String suffix) { 
		try {
			ObjectApi objectApi = GetFileClient().getObjectApi(Constants.REGION, conatinerName);
			return objectApi.get(objectName).getPayload().openStream();
		}catch(Exception e) { 
		}
		return null;
	} 
	
	public SwiftObject getSwiftObject(String conatinerName,String objectName,String suffix) { 
		try {
			ObjectApi objectApi = GetFileClient().getObjectApi(Constants.REGION, conatinerName);
			return objectApi.get(objectName);
		}catch(Exception e) { 
		}
		return null;
	}
	
	public URI getObjectURI(String conatinerName,String objectName) { 
		try {
			ObjectApi objectApi = GetFileClient().getObjectApi(Constants.REGION, conatinerName);
			return objectApi.get(objectName).getUri();
		}catch(Exception e) { 
		}
		return null;
	}
	
	public Map<String,URI> getCDNUri(String conatinerName) { 
		try {
			CDNApi cdnApi = GetFileClient().getCDNApi(Constants.REGION); 
			final CDNContainer cdnContainer = cdnApi.get(conatinerName);
			return new HashMap<String,URI>(){{
				put("uri",cdnContainer.getUri());
				put("sslUri",cdnContainer.getSslUri());
				put("streamingUri",cdnContainer.getStreamingUri());
				put("iosUri",cdnContainer.getIosUri());
			}};
		}catch(Exception e){
		}
		return null;
	} 
	
	public String getSSLCDN(String conatinerName) {
		CDNApi cdnApi = GetFileClient().getCDNApi(Constants.REGION); 
		final CDNContainer cdnContainer = cdnApi.get(conatinerName);
		return cdnContainer.getSslUri().toString();
	}
	
	public boolean createContainer(String containerName) {
		try {
			ContainerApi containerApi = GetFileClient().getContainerApi(Constants.REGION);
			return containerApi.create(containerName);
		}catch(Exception e) {
			
		}
		return false;
	}
	
	public Container  getContainer(String containerName) {
		try {
			ContainerApi containerApi = GetFileClient().getContainerApi(Constants.REGION);
			return containerApi.get(containerName);
		}catch(Exception e) { 
		}
		return null;
	}
	
	public boolean  deleteContainer(String containerName) {
		try {
			ContainerApi containerApi = GetFileClient().getContainerApi(Constants.REGION);
			return containerApi.deleteIfEmpty(containerName);
		}catch(Exception e) { 
		}
		return false;
	}
	
	public URI enableCDN(String containerName) {
		try {
			CDNApi cdnApi = GetFileClient().getCDNApi(Constants.REGION);
			return cdnApi.enable(containerName);
		}catch(Exception e) { 
		}
		return null;
	}
	
	public boolean disableCDN(String containerName) {
		try {
			CDNApi cdnApi = GetFileClient().getCDNApi(Constants.REGION);
			return cdnApi.disable(containerName);
		}catch(Exception e) { 
		}
		return false;
	}
	
	public interface Constants {
		// The provider configures jclouds To use the Rackspace Cloud (US)
		// To use the Rackspace Cloud (UK) set the system property or default value to "rackspace-cloudfiles-uk"
		public static final String PROVIDER = System.getProperty("provider.cf", "rackspace-cloudfiles");
		public static final String REGION = System.getProperty("region", "IAD");
		public static final String CONTAINER_PUBLISH = "jclouds-example-publish";
		public static final String CONTAINER = "dropifi";
		public static final String FILENAME = "createObjectFromFile";
		public static final String SUFFIX = ".html";
	}
	 
}



