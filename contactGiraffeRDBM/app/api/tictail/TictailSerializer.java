package api.tictail;

import java.io.Serializable;
import java.sql.Timestamp;

import com.google.gson.annotations.SerializedName;

public class TictailSerializer implements Serializable{
	
	@SerializedName("access_token")
	private String accessToken;
	@SerializedName("token_type")
	private String tokenType;
	@SerializedName("expires_in")
	private long expires;
	@SerializedName("store")
	private TictailStore store;
	@SerializedName("code")
	private String code;
	
	@Override
	public String toString() {
		return "TictailSerializer [accessToken=" + accessToken + ", tokenType="
				+ tokenType + ", expires=" + expires + ", store=" + store + "]";
	}
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
	public Timestamp getExpires() {
		return new Timestamp(this.expires);
	}
	public void setExpires(long expires) {
		this.expires = expires;
	}
	public TictailStore getStore() {
		return store;
	}
	public void setStore(TictailStore store) {
		this.store = store;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
