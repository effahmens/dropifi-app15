package api.tictail;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class TictailVat implements Serializable{
	@SerializedName("applied_to_shipping")
	private boolean applied_to_shipping;
	@SerializedName("rate")
	private String rate;
	@SerializedName("region")
	private String region;
	@SerializedName("included_in_prices")
	private boolean included_in_prices;
	
	@Override
	public String toString() {
		return "TictailVat [applied_to_shipping=" + applied_to_shipping
				+ ", rate=" + rate + ", region=" + region
				+ ", included_in_prices=" + included_in_prices + "]";
	}

	public boolean isApplied_to_shipping() {
		return applied_to_shipping;
	}

	public void setApplied_to_shipping(boolean applied_to_shipping) {
		this.applied_to_shipping = applied_to_shipping;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public boolean isIncluded_in_prices() {
		return included_in_prices;
	}

	public void setIncluded_in_prices(boolean included_in_prices) {
		this.included_in_prices = included_in_prices;
	}
}
