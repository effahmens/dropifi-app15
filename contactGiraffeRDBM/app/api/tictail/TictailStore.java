package api.tictail;

import java.io.Serializable;
import java.sql.Timestamp;

import org.joda.time.DateTime;

import resources.EcomBlogType;

import com.google.gson.annotations.SerializedName;

public class TictailStore implements Serializable{

	@SerializedName("id")
	private String id;
	@SerializedName("name")
	private String name;
	@SerializedName("url")
	private String url; 
	@SerializedName("dashboard_url") 
	private String dashboardUrl;
	@SerializedName("storekeeper_email")
	private String email;
	@SerializedName("created_at")
	private String created;
	@SerializedName("modified_at")
	private String modified;
	@SerializedName("country") 
	private String country;
	@SerializedName("currency") 
	private String currency;
	@SerializedName("appstore_currency") 
	private String appstoreCurrency; 
	@SerializedName("vat")
	private TictailVat vat;
	 
	public static final String SEPARATOR = "_tic_";
	
	@Override
	public String toString() {
		return "TictailStore [id=" + id + ", name=" + name + ", url=" + url
				+ ", dashboardUrl=" + dashboardUrl + ", email=" + email
				+ ", created=" + created + ", modified=" + modified
				+ ", country=" + country + ", currency=" + currency
				+ ", appstoreCurrency=" + appstoreCurrency + ", vat=" + vat
				+ "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDashboardUrl() {
		return dashboardUrl;
	}

	public void setDashboardUrl(String dashboardUrl) {
		this.dashboardUrl = dashboardUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getCreated() {
		return Timestamp.valueOf(created);
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Timestamp getModified() {
		return Timestamp.valueOf(modified);
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAppstoreCurrency() {
		return appstoreCurrency;
	}

	public void setAppstoreCurrency(String appstoreCurrency) {
		this.appstoreCurrency = appstoreCurrency;
	}

	public TictailVat getVat() {
		return vat;
	}

	public void setVat(TictailVat vat) {
		this.vat = vat;
	}
	
	public String getSubdomain(){ 
		if(this.getId()!=null)
			return EcomBlogType.Tictail.name()+"_"+this.getId();
		
		return null;
	}
	
	public String getLoginEmail(){
		return this.getId()+SEPARATOR+this.getEmail();
		
	}
	
	public static String resetEmail(String email){
		if(!email.contains(SEPARATOR))
			return email;
		else
			return email.split(SEPARATOR)[1];
	}
}
