package api.tictail;

import com.google.gson.Gson;

import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import resources.DropifiTools;

public class TictailClient {
	
	private static final String ENDPOINT="https://tictail.com/oauth/token";
	private static final String REDIRECT_ENDPOINT="https://tictail.com/oauth/authorize";
	private static final String RETURN_ENDPOINT=DropifiTools.ServerURL+"/ecommerce/tictail/oauth";
	
	private String code;
	
	public TictailClient(String code){
		this.code =code;
	}
	
	public TictailSerializer GetClient(){
		try{
			HttpResponse response = WS.url(ENDPOINT)
			.setParameter("client_id", DropifiTools.TictailClientId)
			.setParameter("client_secret", DropifiTools.TictailClientSecret)
			.setParameter("code", this.code)
			.setParameter("grant_type", "authorization_code")
			.post();
			
			if(response!=null){
				return new Gson().fromJson(response.getJson().getAsJsonObject().toString(),TictailSerializer.class);
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}catch(Throwable e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public String getRedirectURL(){ 
		return REDIRECT_ENDPOINT+"?response_type="+this.code
				+ "&client_id="+DropifiTools.TictailClientId
				+ "&redirect_uri="+RETURN_ENDPOINT;
	}
	
}
