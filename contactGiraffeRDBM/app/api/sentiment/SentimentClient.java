package api.sentiment;

import java.io.IOException;

import com.google.gson.JsonSyntaxException;

import resources.sentiment.SentimentBaseSerializer;
import resources.sentiment.SentinmentAPIType;
import resources.sentiment.alchemy.AlchemyClient;
import resources.sentiment.alchemy.AlchemySerializer;
import resources.sentiment.lymbix.ArticleInfo;
import resources.sentiment.lymbix.LymbixClient;
import resources.sentiment.repustate.RepustateClient;
import resources.sentiment.repustate.RepustateSerializer;
import resources.sentiment.textalytics.TextalyticSerializer;
import resources.sentiment.textalytics.TextalyticsClient;

public class SentimentClient {

	private SentinmentAPIType APITYPE;
	private String text;
	private String apikey;
	private String lang; //en-general, en
	
	public SentimentClient(String apikey,SentinmentAPIType APITYPE, String text, String lang){
		this.apikey =apikey;
		this.APITYPE =APITYPE;
		this.text = text;
		this.lang = lang;
	}
	
	public SentimentBaseSerializer analyze(){
		SentimentBaseSerializer senti=null;
		if(this.APITYPE.equals(SentinmentAPIType.Repustate)){
			RepustateClient rclient = new RepustateClient(this.apikey);
			try {
				RepustateSerializer rs= rclient.analyze(this.text, this.lang);
				senti = new SentimentBaseSerializer(rs.getScore(), rs.getSentiment(), "", "");
			} catch (JsonSyntaxException | IOException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}else if(this.APITYPE.equals(SentinmentAPIType.Lymbix)){
			try {
				LymbixClient lclient = new LymbixClient(this.apikey);
				ArticleInfo art = lclient.tonalizeDetailed(this.text,null,false);
				senti = new SentimentBaseSerializer(art.ArticleSentiment.Score, art.ArticleSentiment.SentimentType, art.IntenseSentence.DominantEmotion, art.IntenseSentence.Sentence);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}else if(this.APITYPE.equals(SentinmentAPIType.Alchemy)){
			AlchemyClient aclient = new AlchemyClient(this.apikey); 
			try {
				AlchemySerializer as =  aclient.analyze(this.text, "json", "TEXT");
				senti = new SentimentBaseSerializer(as.getDocSentiment().getScore(), capitalize(as.getDocSentiment().getSentiment()), "", "");
			} catch (JsonSyntaxException | IOException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}else if(this.APITYPE.equals(SentinmentAPIType.Textalytics)){
			TextalyticsClient tclient = new TextalyticsClient(this.apikey);
			try {
				TextalyticSerializer ts = tclient.analyze(this.lang, this.text, "json");
				senti = new SentimentBaseSerializer(ts.getScore(), ts.getSentiment(), ts.getEmotion(), ts.getIntense());
			} catch (JsonSyntaxException | IOException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		return senti;
	}
	
	public String capitalize(String value){
		if(value ==null) return value;
		
		value = value.trim();
		char c =value.charAt(0);
		if(!Character.isUpperCase(c)){ 
			value = Character.toUpperCase(c)+ value.substring(1);
		} 
		return value;
	} 
}
