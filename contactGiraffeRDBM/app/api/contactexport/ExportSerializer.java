package api.contactexport;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class ExportSerializer implements Serializable{
	public ExportSerializer(){
		
	}
	/*
	 * @param id 
	 * @param name
	 * @param email
	 * @param gender
	 * @param option
	 */
	public ExportSerializer(int id, String name, String email, String gender, String option){
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.option = option;
	}
	private final int size=5;
	@SerializedName("name")
	private String name;
	
	@SerializedName("id")
	private int id;
	
	@SerializedName("email")
	private String email;
	
	@SerializedName("gender")
	private String gender;
	
	@SerializedName("option")
	private String option;
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String getOption() {
		return option;
	}
	public void setOptin(String option) {
		this.option = option;
	}
	public int getSize(){
		return this.size;
	}
	
	public String toString(){
		return "id: "+this.id+"  Name: "+this.name+" email: "+this.email+" option: "+this.option;
	}
	
	
}
