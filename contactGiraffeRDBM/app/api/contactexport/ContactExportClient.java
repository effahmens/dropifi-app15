package api.contactexport;

import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.FontCharset;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;

//import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import org.json.JSONObject;

import resources.helper.ServiceSerializer;

public class ContactExportClient {
	private String filename=null; 
	public ContactExportClient(String filename){
		this.filename = filename;
		
	}
	public String saveExcel(String[] keyOrder,List<Map<String, String>> contacts) {
		return saveExcel(null, keyOrder, contacts); 
	}
	public String saveExcel(ServiceSerializer service, String[] keyOrder,List<Map<String, String>> contacts) {
		FileOutputStream fileOut = null; 
		// start
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("List Of Customers");
		HSSFRow row = null;
		try{ 
			int rowNum =0,cellNum = 0;
			//create the header row columns
			row = sheet.createRow(rowNum++);
			for(String key : keyOrder) {
				row.createCell(cellNum++,HSSFCell.CELL_TYPE_STRING).setCellValue(key);
			}
			
			//add the row of data 
			for(Map<String,String> contact : contacts) {
				row = sheet.createRow(rowNum++);
				cellNum=0;
				for(String contactKey : keyOrder) {
					row.createCell(cellNum++,HSSFCell.CELL_TYPE_STRING).setCellValue(contact.get(contactKey));
				}
				
				if(rowNum>=49 && service!=null &&(service.getAccountType()==null || service.getAccountType().equals("Free")) ){
					row = sheet.createRow(2+rowNum++); 
					row.createCell(0, HSSFCell.CELL_TYPE_STRING).setCellValue("You can only download 50 contacts for a free account. Upgrade to download all contacts.");
					break;
				}
			}
		}
		catch(NullPointerException np){
			play.Logger.log4j.info(np.getMessage(),np);
		} 
		
		try {
			this.filename = this.filenameGenerator();
		    fileOut = new FileOutputStream(this.filename);
			wb.write(fileOut); 
		} catch (IOException io) {
			play.Logger.log4j.info(io.getMessage(),io); 
		} finally {
			try {
				fileOut.close();
			} catch (IOException e) { 
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		return this.filename;

	}
 
	public String filenameGenerator(){ 
		return "data"+ File.separator+"contacts"+File.separator +this.filename+"_"+new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date())+".xls";
	}

	public Map<String, String> mapAll(String ...args) {
		Map<String, String> map = new HashMap<String, String>();  
		map.put("Name", args[0]);
		map.put("Email", args[1]);
		map.put("Country", args[2]);
		map.put("Phone", args[3]);
		map.put("Company", args[4]);
		map.put("Gender", args[5]);
		map.put("Age", args[6]); 
		return map;
	}
	
	public String[] contactKeyOrder() {
		return new String[]{"Name","Email","Country","Phone","Gender","Age","Picture"};
	}

}
