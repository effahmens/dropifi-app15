package api.contactexport;

public enum ActionType {
	PDF,
	EXCEL,
	CSV
}
