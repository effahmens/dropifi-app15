package api.aws;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import resources.DropifiTools;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.simpleemail.AWSJavaMailTransport;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.ListVerifiedEmailAddressesResult;
import com.amazonaws.services.simpleemail.model.VerifyEmailAddressRequest;

public class AWSSES {
	private static String AWS_ACCESS_KEY=  DropifiTools.AWSAccessKey;
	private static String AWS_SECRET_KEY = DropifiTools.AWSSecretKey; 
	private Session session;
	
	public AWSSES(){
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "aws");
		props.setProperty("mail.aws.user", AWS_ACCESS_KEY);
        props.setProperty("mail.aws.password", AWS_SECRET_KEY);
        this.setSession( Session.getInstance(props));
	}
	
	public static AmazonSimpleEmailService GetSEClient(){
		AmazonSimpleEmailService sEClient = new AmazonSimpleEmailServiceClient(new AWSCredentials() {
			
			@Override
			public String getAWSSecretKey() {
				// TODO Auto-generated method stub
				return AWS_SECRET_KEY;
			}
			
			@Override
			public String getAWSAccessKeyId() {
				// TODO Auto-generated method stub
				return AWS_ACCESS_KEY;
			}
		});
		return sEClient;
	}
	
	public static void verifyEmailAddress(String address) {
        GetSEClient().verifyEmailAddress(new VerifyEmailAddressRequest().withEmailAddress(address));
    }
	
	public static boolean isVerifiedEmailAddress(String address){
		try{
	        ListVerifiedEmailAddressesResult verifiedEmails = GetSEClient().listVerifiedEmailAddresses();
	        if (verifiedEmails.getVerifiedEmailAddresses().contains(address)) return true;
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
        return false;
	}
	
	public int mailTo(MimeMessage msg){ 
		 int status = 200;
        try {
        	 Transport t = new AWSJavaMailTransport(this.getSession(), null);
             t.connect();
			 t.sendMessage(msg, msg.getAllRecipients());
			 t.close();
		} catch (AddressException e) {
			status = 305;
            play.Logger.log4j.info(e.getMessage(),e);
           /* System.out.println("Caught an AddressException, which means one or more of your "
              + "addresses are improperly formatted.");
            */
        } catch (MessagingException e){
        	play.Logger.log4j.info(e.getMessage(), e);
            /*System.out.println("Caught a MessagingException, which means that there was a "
              + "problem sending your message to Amazon's E-mail Service check the "
              + "stack trace for more information.");
            */
            status = 404;
        }
		return status;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	/*
	 static final String FROM = "efpam2012@gmail.com"; // Replace with your "From" address. This address must be verified.
	 static final String TO = "efpam2010@gmail.com"; // Replace with a "To" address. If you have not yet requested
	 // production access, this address must be verified.
	 static final String BODY = "This email was sent through the Amazon SES SMTP interface by using Java.";
	 static final String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";
	 // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
	 static final String SMTP_USERNAME = "AKIAIZNV2QLRQUT2SAIA"; // Replace with your SMTP username credential.
	 static final String SMTP_PASSWORD = "AmlEinMAb7pS+njtlhDC89dxFXEYYmQwMCggvQdLR54T"; // Replace  with your SMTP password.
	 // Amazon SES SMTP host name.
	 static final String HOST = "email-smtp.us-east-1.amazonaws.com";
	 // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
	 // STARTTLS to encrypt the connection.
	 static final int PORT = 587 ; 
	 
	 public static void main(String[] args) throws Exception {
	 // Create a Properties object to contain connection configuration  information.
	 Properties props = System.getProperties();
	 props.put("mail.transport.protocol", "smtp");
	 props.put("mail.smtp.port", PORT);
	 // Set properties indicating that we want to use STARTTLS to encrypt  the connection.
	 // The SMTP session will begin on an unencrypted connection, and then  the client
	 // will issue a STARTTLS command to upgrade to an encrypted connection.
	 props.put("mail.smtp.auth", "true");
	 props.put("mail.smtp.starttls.enable", "true");
	 props.put("mail.smtp.starttls.required", "true");
	 // Create a Session object to represent a mail session with the specified properties.
	 Session session = Session.getDefaultInstance(props);
	 // Create a message with the specified information.
	 MimeMessage msg = new MimeMessage(session);
	 msg.setFrom(new InternetAddress(FROM));
	 msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
	 msg.setSubject(SUBJECT);
	 msg.setContent(BODY,"text/plain");
	 // Create a transport.
	 Transport transport = session.getTransport();
	 // Send the message.
	  * 
	 try
	 {
		 System.out.println("Attempting to send an email through the Amazon SES SMTP interface...");
		 // Connect to Amazon SES using the SMTP username and password you specified above.
		 transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
		 // Send the email.
		 transport.sendMessage(msg, msg.getAllRecipients());
		 System.out.println("Email sent!");
	 } catch (Exception ex) {
		 System.out.println("The email was not sent.");
		 System.out.println("Error message: " + ex.getMessage());
	 }finally{
		 // Close and terminate the connection.
		 transport.close();
	 }
	 
	 }
	*/
	
}
