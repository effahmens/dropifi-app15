package api.aws; 

import java.util.*; 
import play.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;

import resources.DropifiTools;

public class AWSDynamoDB {
	private static String AWS_ACCESS_KEY;
	private static String AWS_SECRET_KEY;
	private AmazonDynamoDBClient client; 
	
	public AWSDynamoDB(String AWS_ACCESS_KEY,String AWS_SECRET_KEY){
		 this.AWS_ACCESS_KEY = AWS_ACCESS_KEY;
		 this.AWS_SECRET_KEY = AWS_SECRET_KEY;
		 this.client = GetDyClient();
	}
	
	public AmazonDynamoDBClient GetDyClient(){
		AmazonDynamoDBClient client = new AmazonDynamoDBClient(new AWSCredentials() {
			
			@Override
			public String getAWSSecretKey() {
				// TODO Auto-generated method stub
				return AWS_SECRET_KEY;
			}
			
			@Override
			public String getAWSAccessKeyId() {
				// TODO Auto-generated method stub
				return AWS_ACCESS_KEY;
			}
		});
		
		return client;
	}
	
	/**
	 * To create a table, you must provide the table name, its primary key, and the provisioned throughput values
	 * @param attributeName
	 * @param attributeType
	 * @param tableName
	 * @param keyType
	 * @param readCapacity
	 * @param writeCapacity
	 * @return
	 */
	public CreateTableResult createTable(String tableName,String attributeName,String attributeType,KeyType keyType,long readCapacity,long writeCapacity){
		try{
			ArrayList<AttributeDefinition> attributeDefinitions= new ArrayList<AttributeDefinition>();
			attributeDefinitions.add(new AttributeDefinition().withAttributeName(attributeName).withAttributeType(attributeType));
			        
			ArrayList<KeySchemaElement> ks = new ArrayList<KeySchemaElement>();
			ks.add(new KeySchemaElement().withAttributeName(attributeName).withKeyType(keyType));
			  
			ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput()
			    .withReadCapacityUnits(readCapacity)
			    .withWriteCapacityUnits(writeCapacity);
			        
			CreateTableRequest request = new CreateTableRequest()
			    .withTableName(tableName)
			    .withAttributeDefinitions(attributeDefinitions)
			    .withKeySchema(ks)
			    .withProvisionedThroughput(provisionedThroughput);
			
			return this.client.createTable(request);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	public UpdateTableResult updateTable(String tableName,long readCapacity,long writeCapacity ){
		 
		try{ 
			ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput()
			  .withReadCapacityUnits(readCapacity)
			  .withWriteCapacityUnits(writeCapacity);
	
			UpdateTableRequest updateTableRequest = new UpdateTableRequest()
			  .withTableName(tableName)
			  .withProvisionedThroughput(provisionedThroughput);
	
			return this.client.updateTable(updateTableRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	public DeleteTableResult deleteTable(String tableName ){ 
		try{
			DeleteTableRequest deleteTableRequest = new DeleteTableRequest()
			  .withTableName(tableName);
			return this.client.deleteTable(deleteTableRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	public void listTables(long limit){
		
	}
	
	public void listMyTables() {
        String lastEvaluatedTableName = null;
        do { 
            
            ListTablesRequest listTablesRequest = new ListTablesRequest()
            .withLimit(10)
            .withExclusiveStartTableName(lastEvaluatedTableName);
            
            ListTablesResult result = this.client.listTables(listTablesRequest);
            lastEvaluatedTableName = result.getLastEvaluatedTableName();
            
            for (String name : result.getTableNames()) {
                System.out.println(name);
            }
            
        } while (lastEvaluatedTableName != null);
    }
	
	/**
	 * Manipulating items
	 */
	
	/**
	 * The putItem method stores an item in a table. If the item exists, it replaces the entire item
	 * @param tableName
	 * @param item
	 * @return
	 */
	public PutItemResult putItem(String tableName,Map<String, AttributeValue> item){ 
		try{
			PutItemRequest putItemRequest = new PutItemRequest()
			  .withTableName(tableName)
			  .withItem(item);
			return this.client.putItem(putItemRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	/**
	 * The putItem method stores an item in a table. If the item exists, it replaces the entire item
	 * Along with the required parameters, you can also specify optional parameters to the putItem method. 
	 * For example, you can use an optional parameter to specify a condition for uploading an item. 
	 * If the condition you specify is not met, then the AWS Java SDK throws a ConditionalCheckFailedException.
	 * @param tableName
	 * @param item
	 * @param expected
	 * @param retVal
	 * @return
	 */
	public PutItemResult putItem(String tableName,Map<String, AttributeValue> item,Map<String, ExpectedAttributeValue> expected,ReturnValue retVal){ 
		try{
			PutItemRequest putItemRequest = new PutItemRequest()
			  .withTableName(tableName)
			  .withItem(item)
			  .withExpected(expected)
			  .withReturnValues(retVal);
			return this.client.putItem(putItemRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	/**
	 * Use the updateItem method to update existing attribute values, add new attributes to the existing collection, or delete attributes from the existing collection
	 * @param tableName
	 * @param key
	 * @param updateItems
	 * @param returnValue
	 * @return
	 */
	public UpdateItemResult updateItem(String tableName,HashMap<String, AttributeValue> key, Map<String, AttributeValueUpdate> updateItems,ReturnValue returnValue){
	
		try{
			UpdateItemRequest updateItemRequest = new UpdateItemRequest()
			  .withTableName(tableName)
			  .withKey(key).withReturnValues(returnValue)
			  .withAttributeUpdates(updateItems);
			            
			return this.client.updateItem(updateItemRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	/**
	 * Use the updateItem method to update existing attribute values, add new attributes to the existing collection, or delete attributes from the existing collection
	 * Along with the required parameters, you can also specify optional parameters for the updateItem method including an expected value that an attribute must have if the update is to occur. 
	 * If the condition you specify is not met, then the AWS Java SDK throws an ConditionalCheckFailedException.
	 * @param tableName
	 * @param key
	 * @param updateItems
	 * @param returnValue
	 * @param expectedValues
	 * @return
	 */
	public UpdateItemResult updateItem(String tableName,HashMap<String, AttributeValue> key, Map<String, AttributeValueUpdate> updateItems,ReturnValue returnValue,Map<String, ExpectedAttributeValue> expectedValues){
		
		try{
			UpdateItemRequest updateItemRequest = new UpdateItemRequest()
			  .withTableName(tableName)
			  .withAttributeUpdates(updateItems)
			  .withExpected(expectedValues)
			  .withKey(key).withReturnValues(returnValue);
			return this.client.updateItem(updateItemRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	/**
	 * The getItem method retrieves an item. To retrieve multiple items, you can use the batchGetItem method. 
	 * @param tableName
	 * @param key
	 * @return
	 */
	public Map<String, AttributeValue> getItem(String tableName,HashMap<String, AttributeValue> key){ 
		try{
			GetItemRequest getItemRequest = new GetItemRequest()
			    .withTableName(tableName)
			    .withKey(key);
	
			GetItemResult result = this.client.getItem(getItemRequest);
			return result.getItem();
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	/**
	 * The getItem method retrieves an item. To retrieve multiple items, you can use the batchGetItem method. 
	 * Along with the required parameters, you can also specify optional parameters for the getItem method
	 * @param tableName
	 * @param key
	 * @param attributesToGet
	 * @param consistentRead
	 * @return
	 */
	public Map<String, AttributeValue> getItem(String tableName,HashMap<String, AttributeValue> key, List<String> attributesToGet,boolean consistentRead){ 
		try{
			 
			GetItemRequest getItemRequest = new GetItemRequest()
			    .withTableName(tableName)
			    .withKey(key)
			    .withAttributesToGet(attributesToGet)
			    .withConsistentRead(consistentRead); 
	
			GetItemResult result = this.client.getItem(getItemRequest);
			return result.getItem();
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	/**
	 * The deleteItem method deletes an item from a table. 
	 * @param tableName
	 * @param key
	 * @return
	 */
	public DeleteItemResult deleteItem(String tableName,HashMap<String, AttributeValue> key){
		try{
			DeleteItemRequest deleteItemRequest = new DeleteItemRequest()
			    .withTableName(tableName)
			    .withKey(key);
			      	
			return this.client.deleteItem(deleteItemRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
	/**
	 * The deleteItem method deletes an item from a table.
	 * Along with the required parameters, you can also specify optional parameters for the DeleteItem method
	 * @param tableName
	 * @param key
	 * @param returnValue
	 * @param expectedValues
	 * @return
	 */
	public DeleteItemResult deleteItem(String tableName,HashMap<String, AttributeValue> key,ReturnValue returnValue, Map<String, ExpectedAttributeValue> expectedValues ){
		try{
			DeleteItemRequest deleteItemRequest = new DeleteItemRequest()
			    .withTableName(tableName)
			    .withKey(key)
			    .withExpected(expectedValues)
			    .withReturnValues(returnValue);
			      	
			return this.client.deleteItem(deleteItemRequest);
		}catch(Exception e){ Logger.log4j.info(e.getMessage(),e); }
		return null;
	}
	
}
