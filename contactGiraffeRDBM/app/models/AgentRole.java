package models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.google.gson.annotations.SerializedName;

import play.data.validation.Required;

@Embeddable
public class AgentRole implements Serializable {

	@SerializedName("roleName") @Required@Column(nullable = false)
	private String roleName;
	@SerializedName("created")
	private Timestamp created; 
	 
	public AgentRole(String roleName) {
		this.setRoleName(roleName);
		this.setCreated(new Timestamp(new Date().getTime()));
	}
	 
	@Override
	public String toString() {
		return "AgentRole [roleName=" + roleName + ", created=" + created + "]";
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	} 

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	
}
