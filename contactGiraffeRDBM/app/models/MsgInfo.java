package models;

import java.util.*;
import play.data.validation.*;
import play.db.jpa.Model;
import javax.persistence.*;

@Entity
public class MsgInfo extends Model{
	
	 
	public String msgId;
	public String country;
	public String state;
	public Integer latitude;
	public Integer longitude;
	public String browser;
	public String platform;
	
	@ElementCollection 
	@CollectionTable(name="REFERALSITE")
	public Collection<String>referalSites;
	
}
