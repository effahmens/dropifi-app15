package models;

import play.data.validation.*;
import play.db.jpa.Model;

import javax.persistence.*;

import org.bson.types.ObjectId; 

@Entity
public class Setting extends Model{
	
	//-------------------------------------------------------------------------------------------------------- 
	//instance variables

	@Required @OneToOne 
	private Company company; 
	
	@Required 
	@JoinColumn(referencedColumnName="id",table="Channel") 
	private Long channelId; 
	
	@Required
	@JoinColumn(referencedColumnName="id",table="MessageManager")	 
	private Long msgId;
	
	public Setting(Company company){ 
		this.setCompany(company);
	}
	
	public Setting createSetting() throws NullPointerException{
		//find setting
		try{
		 if(this.getCompany() == null) 
		   return null;
		
			Setting setting = findByCompanyId(this.getCompany().getId());


			
			if(setting != null){			
				return null; 
			} else {
				
				//save the setting
				setting = this.save(); 
				
				//create Channel and MessageManager
				Channel channel = new Channel(setting);
				channel = channel.createChannel(); 
				
				MessageManager msg = new MessageManager(setting).createMsgManager();	
				
				if(channel !=null && msg != null){
					
					//add Channel & MessageManager to setting		
					setting.setChannelId(channel.getId());							
					setting.setMsgManagerId( (msg.getId()));
	
					//validate to verify that all validation rules are met				
					if(setting.validateAndSave()){
						return setting; 
					}
					
				}
				return null;
			}	
		}catch(Exception e){
			play.Logger.log4j.info("debuging is here: "+e.getMessage(), e);
		}
		return null;
	}
	
	public static Setting findByCompanyId(Long companyId){
		return Setting.find("byCompany_Id", companyId).first();
	}

	//
	public static Setting findByApikey(String apikey){
		return Setting.find("select s from Setting s, Company c where s.company.id = c.id and c.apikey = ?", apikey).first();
	}
	
	
	//------------------------------------------------------------------------------------------------------------
	//getters and setters

	public Company getCompany() {		
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public Long getMsgManagerId() {
		return this.msgId;
	}
	
	private void setMsgManagerId(Long msgManagerId) {
		this.msgId = msgManagerId;
	}
	
	public MessageManager getMsgManager() {
		return MessageManager.findById(this.getMsgManagerId());
	}
	
	public Channel getChannel() {
		return Channel.findById(this.getChannelId());
	}
	

	public Long getChannelId() {
		return channelId;
	}
	

	private void setChannelId(Long channelId) {
		this.channelId = channelId;
	}
	
	
}
