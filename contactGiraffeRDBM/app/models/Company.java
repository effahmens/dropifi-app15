/**
 * @author philips effah mensah
 * @description 
 * @date 14/3/2012
 * 
 **/

package models;

import static akka.actor.Actors.actorOf;
import play.cache.Cache;
import play.data.validation.*; 

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import play.db.helper.JpqlSelect;
import play.db.jpa.*;
import play.libs.Codec;
import resources.*;
import resources.dstripe.StCustomer;
import view_serializers.ClientSerializer;
import view_serializers.CoProfileSerializer;
import view_serializers.MailSerializer;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey; 
import javax.persistence.*;

import job.process.EventTrackActor;
import job.process.WidgetActor;
import models.addons.AddonPlatform;
import models.addons.AddonSubscription;
import models.dsubscription.DownGradePlan;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubPlan;
import models.dsubscription.SubscriptionType;
import models.notification.EmailNotification;

import org.bouncycastle.i18n.filter.SQLFilter;
import org.hibernate.annotations.GenericGenerator; 
import org.hibernate.annotations.Index;

import akka.actor.ActorRef;

import com.google.gson.annotations.SerializedName;
import com.stripe.model.Subscription; 

@Entity
public class Company extends Model{    
	
	//-------------------------------------------------------------------------------------------------------- 
	//instance variables
	@Required @Column(unique=true, nullable=false)	 
	private String apikey;
	@Required @Email @Column(length=150, unique=true, nullable=false) 
	private String email; 
	@Column(length=100, nullable=false, unique=true)
	private String domain;
	
	@Column(nullable=false)
	private AccountType accountType; 
	
	@Embedded
	private CompanyProfile profile;
	
	@Column(nullable=false)
	private Timestamp created;
	@Column(nullable=false)
	private Timestamp updated; 
	
	//consists of all the company settings such as site, contact, channel ......	
	@OneToOne(fetch=FetchType.LAZY, cascade= CascadeType.ALL )	 
	private Setting setting;
	
	@Embedded
	private EcomBlogPlugin ecomBlogPlugin;
	
	@Transient
	private String password;
	@Transient
	private String username;
	
	public Company(String email, String domain,String username, String password,  AccountType accountType, CompanyProfile profile){
		this.setEmail(email);
		this.setDomain(domain);
		this.setUsername(username);
		this.setAccountType(accountType); 
		this.setPassword(password);
		this.setProfile(profile);
	}
	
	public Company(String email, String domain, String password,  AccountType accountType){
		this(email,domain,null,password,accountType,null); 
	}
	
	public Company(String email, String domain,String username, String password,  AccountType accountType){
		this(email,domain,username,password,accountType,null); 
	}
	
	public Company(String email,String password,AccountType accountType){
		this(email,null,null,password,accountType,null);
	}
	
	public Company(EcomBlogPlugin plugin, String password,CompanyProfile profile, String loginEmail) throws NullPointerException{
		this(loginEmail, plugin.getDomain(), profile.getName(), password, AccountType.Free, profile);
		this.setEcomBlogPlugin(plugin);
	}
	
	public Company(EcomBlogPlugin plugin, String password,CompanyProfile profile) throws NullPointerException{
		this(plugin.loginEmail(), plugin.getDomain(), profile.getName(), password, AccountType.Free, profile);
		this.setEcomBlogPlugin(plugin);
	}
	
	public Company(EcomBlogPlugin plugin,String domain,String password,CompanyProfile profile) throws NullPointerException{
		this(plugin.loginEmail(), domain, profile.getName(), password, AccountType.Free, profile);
		this.setEcomBlogPlugin(plugin);
	}
	
	public Company(EcomBlogPlugin plugin,String domain,String password,CompanyProfile profile,String username) throws NullPointerException{
		this(plugin.loginEmail(), domain, username, password, AccountType.Free, profile);
		this.setEcomBlogPlugin(plugin);
	}
		
	public Company createCompany(){
		return createCompany(SubscriptionType.MONTHLY.name(), null, null);
	} 
	
	/**
	 * Create a profile for a company if and only if a company
	 * with similar email is not created
	 * @return 
	 */ 	
	public Company createCompany(String paymentOption, String stripeToken, String stripeCardType){ 
		//search for channel using the companyId;
		  
		if(!this.getEmail().isEmpty() &&!this.getPassword().isEmpty() && !this.getDomain().isEmpty()){ 			
	 
			try{ 
				//set the apiKey of the company
				this.setApikey();
				Timestamp datetime = new Timestamp(new Date().getTime());
				this.setCreated(datetime);
				this.setUpdated(datetime);
				
				//validate to verify that all validation rules are met: returns 200 if record is save;
				if(this.validateAndSave()){			
					Company comp = this;											

					//verify the company information was created successfully
					if(comp !=null){
						
						//create an admin account 
						AccountUser accountAdmin = new AccountUser(this.getUsername(), this.getEmail(), this.getPassword(), comp.getApikey());
					    accountAdmin = accountAdmin.createAdmin();
					   
						if(accountAdmin !=null){
							//this.setAccountAdmin(accountAdmin);
							//create company setting and save
							Setting sett = new Setting(comp);
							sett = sett.createSetting();
							if(sett !=null){
								this.setSetting(sett);	
								//if(this.createDirectory())
								this.save();   
								
								//subscribe the user to a payment plan
								try{
									this.subscribeToPlan(paymentOption, stripeToken, stripeCardType);
									new EmailNotification(this.getApikey(), true).createNotification();
								}catch(Exception e){
									play.Logger.log4j.info("New Subscription: "+e.getMessage(),e);
								}
								return this.save();
							}  
						}
					}	
					
				} 
				//delete the account
				rollback(this);
				return null;
				
				//the email address is used by another user
			}catch(Exception err){
				play.Logger.log4j.info(err.getMessage(),err);
				try{
					rollback(this);
					return null;
				}catch(Exception e){
					play.Logger.log4j.info(err.getMessage(),e);
					return null;
				}
			 }
		}

		return null;
	}
	
	public void resetWixAccount(String instanceId){
		String publickey = Company.findPublicBySecretkey(instanceId, EcomBlogType.Wix);
		try{
			this.getEcomBlogPlugin().setEb_apikey(null);
			this.getEcomBlogPlugin().setEb_secretKey(null);
			this.validateAndSave();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
		CacheKey.delete(CacheKey.getWidgetTabKey(publickey));
		CacheKey.delete(CacheKey.getWixKey(instanceId)); 
		Cache.delete(CacheKey.getWidgetContentKey(publickey));
		 
	}
	
	public void loginSubscription() {
		try{
			SubCustomer sub = SubCustomer.findByAppikey(this.getApikey());
			if(sub==null && this.getAccountType().equals(AccountType.Free)){
				this.subscribeToPlan(SubscriptionType.MONTHLY.name(), "", "");
			}else if(sub!=null && sub.getSubConsumeServices()!=null && sub.getSubConsumeServices().size()<=5){
				sub.addConsumeServices();
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	//critical task: delete all information relating to a company account after unsuccessful signup 
	public static Company rollback(Company company){ 
		if(company!= null){ 
			try{			
				Widget w = Widget.findByApiKey(company.getApikey());
				if(w!=null && w.id !=null){
					w.setWidgetControls(null);
					w.save(); 
					Widget.delete("Delete from Widget d where d.apikey = ?", company.getApikey());  
				} 
				
				MsgCounter.delete("delete from MsgCounter m where m.companyId = ? and m.sourceOfMsg = ?", w.getCompanyId(),w.getClass().getSimpleName());
				WidgetDefault.delete("Delete from WidgetDefault w where w.apikey = ?", company.getApikey());			
				Channel.delete("Delete from Channel ch where ch.companyId = ?", company.getId());
				MessageManager.delete("Delete from MessageManager m where m.companyId = ?", company.getId()); 
				AccountUser.delete("Delete from AccountUser a where a.apikey = ?", company.getApikey());  
				EmailNotification.delete("Delete From EmailNotification e Where e.apikey=?", company.getApikey());
				//Setting.delete("Delete from Setting s where s.company.apikey = ?", company.getApikey());
				Company.delete("Delete from Company c where c.apikey = ?", company.getApikey());  
				return company;
			}catch(Exception e){ 
				//notify the technical officer of dropifi through email or sms 
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		return null;
	}
	
	/**
	* save the contact profile of the company. return 1 if profile is saved, 
	 * -1 if domain is taken by another user and 0 if profile is not saved
	 * @param domain
	 * @param name
	 * @param url
	 * @param industry
	 * @param country
	 * @param timezone
	 * @return
	 */
	public int updateProfile(String domain, String name,String url,String industry,String country, String timezone){
		//check that the domain is not used		 
		if(isDomainAvailable(domain) == false){
			this.setDomain(domain);		 
			return this.updateProfile(name, url, industry, country, timezone);
		} 
		return -1;
	}
	
	public static int changeEmail(String apikey, String currentEmail, String newEmail){
		int result = Company.updateUserEmail(apikey, currentEmail, newEmail);
			result = AccountUser.updateUserEmail(apikey, currentEmail, newEmail);
			result = DropifiResponse.updateUserEmail(apikey, currentEmail, newEmail); 
					 WixDefault.updateUserEmail(apikey, currentEmail, newEmail);
		return result;
	}
	
	public static int updateUserEmail(String apikey, String currentEmail, String newEmail){ 
		int result = Company.em().createQuery("Update Company c Set c.email = :newEmail Where c.apikey= :apikey and c.email= :currentEmail")
				.setParameter("apikey",apikey)
				.setParameter("currentEmail", currentEmail)
				.setParameter("newEmail", newEmail).executeUpdate();
		return result>=0?200:201;
	}
	
	/***
	 * Delete all the data associated with this account, the subscription data
	 * @param requestEmail
	 * @return
	 */
	public int cancelAccount(String cancelEmail, String cancelPassword,String cancelMessage){ 
		AccountUser user = AccountUser.findByPassword(this.getApikey(), cancelEmail, cancelPassword);
		
		if(user!=null){
			Date time = new Date();
			String deletekey = "deleted_"+this.getApikey()+"_"+time.getTime();
			
			Widget widget = Widget.findByApiKey(this.getApikey());		
			SubCustomer customer = SubCustomer.findByAppikey(this.getApikey());
			boolean canceled = true;
			if(customer!=null){
				canceled = customer.cancelCustomer(DropifiTools.StripeSecretKey); 
			}
			
			if(canceled){
				widget.cancelAccount();
				if(user.cancelAccount(deletekey)){ 
					AddonHelper.deleteSocialAccount(this.getApikey(), cancelEmail);
					this.clearSubscriptionFromCache(); 
					this.apikey = deletekey; 
					this.setDomain("deleted_"+this.getDomain()+"_"+time.getTime());
					this.setEmail("deleted_"+this.getEmail()+"_"+time.getTime());
					this.setEcomBlogPlugin(null);
					
					if(this.validateAndSave()){ 
						 //send a cancel message to the customer	 
						 String msg = MailTemplate.getCancelNotification(user.getProfile().getUsername(), cancelEmail);
						 MailSerializer mail = new MailSerializer(user.getProfile().getUsername(), cancelEmail,"","Your Dropifi account has been cancelled",msg);								
						 mail.tellMailingActor();
						 
						//send a cancel message to dropifi 
						MailSerializer mailNotify = new MailSerializer(DropifiTools.teamUserName, DropifiTools.teamEmail,"",cancelEmail+" has cancelled his  Dropifi Account","<p>Reason for cancelling</p><p>"+cancelMessage+"</p>");								
						mailNotify.tellMailingActor();
						
						try{
							//Track successful subscription
							EventTrackSerializer event = new EventTrackSerializer(email, "ACCOUNT CANCELLED",cancelMessage);
							ActorRef eventActor = actorOf(EventTrackActor.class).start();    
							eventActor.tell(event);  
						}catch(Exception e){
								play.Logger.log4j.error(e.getMessage(),e);
						}	
						 return 200;
					 }
				}
			}
			return 305;
		}
		return 501;
	}
	
	//===============================================================================
	//subscriptions
	/*public static SubCustomer subscribeToPlan(String apikey,long companyId,String domain,String email,String name, String accountType,String paymentOption,String stripeToken, String stripeCardType,String userType, String widgetType){
		return subscribeToPlan(apikey,companyId,domain,email,name, accountType,paymentOption,stripeToken,stripeCardType,userType,widgetType);

	}*/
	
	public static SubCustomer subscribeToPlan(String apikey,long companyId,String domain,String email,String name,String accountType,String paymentOption,String stripeToken, String stripeCardType,String userType, String widgetType,String coupon){
		SubCustomer customer = new SubCustomer(apikey, email, accountType, paymentOption); 
		customer.setName(name);
		customer = customer.createCustomer(stripeToken, stripeCardType, DropifiTools.StripeSecretKey,widgetType,coupon,DropifiTools.StripeAdditionalId);
		
		if(customer!=null){
			Subscription sub = customer.getSubscription();
			if(sub!=null){
				
				if(Company.updateAccountType(apikey,companyId,domain,accountType,false)==200){
										
					try{
						//Track successful subscription 
						EventTrackSerializer event = new EventTrackSerializer(email, "SUBSCRIPTION", true, accountType, userType, widgetType);
						event.setName(name);
						ActorRef eventActor = actorOf(EventTrackActor.class).start();   
						eventActor.tell(event);  
					}catch(Exception e){
						play.Logger.log4j.error(e.getMessage(),e);
					}
				
					DownGradePlan.resetPlan(apikey, accountType);  
				}
			}
		}
		return customer;
	}
	
	public static SubCustomer subscribeSignupToPlan(String apikey,long companyId,String domain,String email,String name,String accountType,String paymentOption,String userType, String widgetType){
		SubCustomer customer = new SubCustomer(apikey, email, accountType, paymentOption); 
		if(name==null || !name.trim().isEmpty()){
			name = AccountUser.findByName(email, apikey);
		}
		customer.setName(name); 
		customer = customer.createSigupCustomer(widgetType);
		if(customer!=null){			
			if(Company.updateAccountType(apikey,companyId,domain,accountType,false)==200){		
				try{ 
					//Track successful subscription 
					EventTrackSerializer event = new EventTrackSerializer(email, "SUBSCRIPTION", true, accountType, userType, widgetType);
					ActorRef eventActor = actorOf(EventTrackActor.class).start();   
					eventActor.tell(event);  
				}catch(Exception e){
					play.Logger.log4j.error(e.getMessage(),e);
				}
			
				DownGradePlan.resetPlan(apikey, accountType);  
			}
		}
		return customer;
	}
	
	/**
	 * Subscribe a user to the selected plan
	 * @param paymentOption
	 * @param stripeToken
	 * @param stripeCardType
	 * @return 
	 */
	public SubCustomer subscribeToPlan(String paymentOption,String stripeToken, String stripeCardType){
		performBackwardCompactibility();
		return subscribeSignupToPlan(this.getApikey(),this.getId(),this.getDomain(),this.getEmail(),this.getUsername(),this.getAccountType().name(), paymentOption,UserType.Admin.name(), this.getEcomBlogPlugin().getEb_pluginType().name());
	}
	
	public SubCustomer subscribeExistingToPlan(String paymentOption,String stripeToken, String stripeCardType){
		performBackwardCompactibility();
		return subscribeSignupToPlan(this.getApikey(),this.getId(),this.getDomain(),this.getEmail(),this.getUsername(),this.getAccountType().name(), paymentOption,UserType.Admin.name(), this.getEcomBlogPlugin().getEb_pluginType().name());
	}
	
	/**
	 * Convert a wix user to a paid or free user base on the value of the vendorProductId.
	 * @param vendorProductId
	 * @return
	 */
	public SubCustomer subscribeWixToPlan(String vendorProductId,String instanceId, EcomBlogType ecomBlogType){
		String plan = wixPlan(vendorProductId);
				
		if(this.getAccountType()!=null && this.getAccountType().name().equals(plan))
			return null;
		
		SubCustomer customer = subscribeSignupToPlan(this.getApikey(), this.getId(), this.getDomain(), this.getEmail(), null, plan, SubscriptionType.MONTHLY.name(),UserType.Admin.name(), ecomBlogType.name());
		if(customer!=null){
			WixDefault.updateAccountType(instanceId, AccountType.valueOf(plan)); 
		}
		return customer;
	} 
	
	public static String wixPlan(String vendorProductId){
		String plan = AccountType.Free.name();
		if(vendorProductId!=null && vendorProductId.equalsIgnoreCase(DropifiTools.WixVendorProductId)){
			plan = AccountType.Enterprise.name();
		}
		return plan;
	}
	
	public SubCustomer subscribeEcomPluginToPaidPlan(EcomBlogType ecomBlogType){
		
		SubCustomer cus = subscribeSignupToPlan(this.getApikey(), this.getId(), this.getDomain(), this.getEmail(), null, AccountType.Business.name(), SubscriptionType.MONTHLY.name(),UserType.Admin.name(), ecomBlogType.name());
		if(cus!=null){
			this.setAccountType(AccountType.valueOf(cus.getCurrentPlan().name()));
		}
		return cus;
	}
	
	public static SubCustomer subscribeEcomPluginToPaidPlan(String apikey,EcomBlogType ecomBlogType){
		 
		Company company = findByApikey(apikey);
		if(company!=null)
			return company.subscribeEcomPluginToPaidPlan(ecomBlogType);
		 
		return null;
	}
	
	public SubCustomer subscribeToFreePlan(EcomBlogType ecomBlogType){
		return subscribeSignupToPlan(this.getApikey(), this.getId(), this.getDomain(), this.getEmail(), null, AccountType.Free.name(), SubscriptionType.MONTHLY.name(),UserType.Admin.name(), ecomBlogType.name());
	}
	
	public static SubCustomer subscribeToFreePlan(String apikey,EcomBlogType ecomBlogType){
		Company company = findByApikey(apikey);
		if(company!=null)
			return company.subscribeToFreePlan(ecomBlogType);
		 
		return null;	
	}
	
	/**
	 * Check if the user has a debt/credit card tied to his account.
	 * @param apikey
	 * @return
	 */
	public static boolean hasCard(String apikey){
		SubCustomer cus = SubCustomer.findByAppikey(apikey); 
		if (cus!=null && cus.getCustomerId() !=null/*&& new StCustomer(DropifiTools.StripeSecretKey).retrieveCustomer(cus.getCustomerId())!=null*/ ) 
			return true;
	 
		return false;
	}
	
	private void performBackwardCompactibility(){
		try{
			boolean savedata = false;
			if(this.getEcomBlogPlugin()==null || this.getEcomBlogPlugin().getEb_pluginType()==null){
				this.setEcomBlogPlugin(new EcomBlogPlugin(this.getDomain(), "", "", EcomBlogType.Dropifi));
				savedata = true;
			}
			
			if(this.getAccountType()==null){
				this.setAccountType(AccountType.Free);
				savedata=true;
			}
			
			if(savedata){
				this.validateAndSave();
			}
			
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static boolean intiateDownGrade(String apikey,String typeId){ 
		Timestamp time = new Timestamp(new Date().getTime());
		DownGradePlan found = DownGradePlan.findByApikey(apikey);
		if(found==null){
			found = new DownGradePlan(apikey, false, false, 0, time);
			found.setTypeId(typeId);
			found = found.save();
		}
		
		//reset the date if this is the first downgrade request
		if(found.getCounter()==0){
			found.setCurrentDate(time); 
			found.setNotifyDate(time);
			found.setLocked(false);
		}
		
		//if(!found.isDowngrade())
		found.incrementCounter();
		
		found.setTypeId(typeId);
		found.setDowngrade(true);
		
		return found.validateAndSave();

	}
	public static List<DownGradePlan> findAllPlanToDownGrade(Connection conn){
		List<DownGradePlan> list = new LinkedList<DownGradePlan>();
		try {
			PreparedStatement query = conn.prepareStatement("Select * from DownGradePlan where downgrade = true");
			ResultSet result = query.executeQuery();
			
			while(result.next()){ 
				try{
					DownGradePlan plan = new DownGradePlan();
					plan.setCurrentPlan(resources.com.AccountType.valueOf(result.getInt("currentPlan")));
					plan.setApikey(result.getString("apikey"));
					plan.setTypeId(result.getString("typeId"));
					plan.setSuspended(result.getBoolean("suspended"));
					plan.setDowngrade(result.getBoolean("downgrade"));
					plan.setCounter(result.getInt("counter"));
					plan.setCurrentDate(result.getTimestamp("currentDate"));
					plan.setNotifyDate(result.getTimestamp("notifyDate"));
					plan.setLocked(result.getBoolean("locked"));
					list.add(plan);
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
		return list;
	}
	
	public static Subscription downGradePlan(String apikey){
		return systemGradePlan(apikey, AccountType.Free.name());
	}
	
	public static Subscription systemGradePlan(String apikey,String accountType){
		try{
			SubCustomer customer = SubCustomer.downGradeSubConsume(apikey,DropifiTools.StripeSecretKey,accountType,DropifiTools.StripeAdditionalId);
			if(customer!=null){
				Subscription sub = customer.getSubscription(); 
				if(sub!=null){
					Company c = Company.findByApikey(apikey);
					if(c!=null && Company.updateAccountType(apikey,c.getId(),c.getDomain(),accountType,true)==200){
						return sub;
					}
				}
			} 
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		
			return null;
	}
	
	private static int changeAccountType(String apikey, String accountType){
		int result= 501;
		try{
			result = Company.em().createQuery("Update Company c Set c.accountType= :accountType Where c.apikey= :apikey")
					.setParameter("accountType", AccountType.valueOf(accountType))
					.setParameter("apikey", apikey).executeUpdate();
			result = result>=0?200:201;
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return result;
	}
	
	public static int updateAccountType(String apikey,long companyId,String domain,String accountType,boolean isSystem){
		clearSubscriptionFromCache(apikey);
		
		int result = changeAccountType(apikey,accountType); 		
		if(result==200){ 
			//update all the services the user is subscribed to 
			if(isSystem){
				Widget.systemUpgradeWidget(apikey,domain, accountType);
			} else { 
				Widget.autoUpdateWidget(apikey,domain,accountType);
			}
			
			AccountUser.autoUpdateAgents(apikey);
			WMsgRecipient.autoUpateRecipients(apikey);
			WidgetRule.autoUpdateWidgetRule(apikey, companyId);
			InboundMailbox.autoUpateMailboxes(apikey);
		}
		
		return result;
	}
	
	public static void clearSubscriptionFromCache(String apikey){
		CacheKey.delListOfRepFromCache(apikey,CacheKey.Recipients);
		CacheKey.delListOfRepFromCache(apikey, CacheKey.Widget_Rules);
		CacheKey.delListOfRepFromCache(apikey, CacheKey.Agent);
		CacheKey.delListOfRepFromCache(apikey,CacheKey.Widget);
		CacheKey.delListOfRepFromCache(apikey,CacheKey.Analytics);
		CacheKey.delListOfRepFromCache(apikey,CacheKey.Sentiment);
		CacheKey.delListOfRepFromCache(apikey,CacheKey.Mailboxes);
		CacheKey.delListOfRepFromCache(apikey,CacheKey.MonitoredMailboxes);
	}
	
	public void clearSubscriptionFromCache(){
		clearSubscriptionFromCache(this.getApikey());
	}
	
	//=================================================================================
	//
	
	public static boolean isDomainAvailable(String domain){
		if(domain !=null){
			String dom = Company.find("select c.domain from Company as c where c.domain = ? ", domain).first();
			return (dom!=null && !dom.trim().isEmpty()); 
		}
		return false;
	}
	
	public static boolean hasDomain(String email){
		Query q =  Company.find("select c.domain from Company c, AccountUser u where c.email = u.email and u.email = ? ", email).query; 
		
		if(q != null){
			String d = q==null ? null: (String) q.getSingleResult();			 
			return (d == null) ? false : !d.trim().isEmpty();
		}
		return false;
	}
	
	public static int hasProfile(String apikey){
		List<Object[]> resultList =  Company.find("select c.apikey, c.profile.industry from Company as c where c.apikey = ? ", apikey).query.getResultList();
		 String foundApikey = null;
		 String foundIndustry = null;
		 if(resultList !=null){
			 
			 for(Object[] fields:resultList){
				 foundApikey = (String)fields[0];
				 foundIndustry = (String)fields[1];
			 } 
			 
			 if(foundApikey!=null && !foundApikey.trim().isEmpty() && foundApikey.equalsIgnoreCase(apikey)){
				 if(foundIndustry==null || foundIndustry.trim().isEmpty()){
					 return 1;
				 }
				 return 0;
			 }
		 }
		return -1;
	}
	
	public boolean hasDomain(){
		return this.getDomain()==null? false : !this.getDomain().trim().isEmpty();
	}
	
	public static boolean isEmailAvailable(String email){
		Query query =  AccountUser.em().createQuery("select u.profile.email from AccountUser u where u.profile.email = :email ");
		List<Object> resultList = query.setParameter("email", email).getResultList();
		if(resultList !=null){
			for(Object object: resultList){
				String e = (String)object; 
				return (e!=null&&!e.trim().isEmpty()&& e.trim().equalsIgnoreCase(email))?true:false;
			}
		}  
		return false; 
	}
	
	public static boolean isValidDomain(String domain){
		try{
			String sChar ="!@#$%^&*()+=-[]\\\';,./{}|\":<>?~";
			for(int i=0;i<domain.length();i++){
				if(sChar.indexOf(domain.charAt(i)) !=-1){
					return false;
				}
			}
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	public static String replaceSpecialChar(String domain){ 
		try{
			String sChar ="!@#$%^&*()+=-[]\\\';,./{}|\":<>?~";
			for(int i=0;i<domain.length();i++){
				if(sChar.indexOf(domain.charAt(i)) !=-1){
					domain = domain.replace(String.valueOf(domain.charAt(i)), "");
				}
			}
		}catch(Exception e){ 
			return domain;
		}
		return domain;
	}
 	
	//==================================================================================
 	//updaters
 	
	public int updateProfile(String domain ,CompanyProfile profile){
		Company company = Company.findByDomain(domain);
		
		if(company == null){
			this.setDomain(domain);		 
			return this.updateProfile(profile); 
		}
		
		return -1;
	}
	
	public int updateProfile(CompanyProfile profile){
		this.setProfile(profile);
		return this.validateAndSave()==true ? 200 : 201;
	}
	 
	public int updateProfile(String name,String url,String industry,String country, String timezone){
		return 	updateProfile(new CompanyProfile(name, url, timezone, industry, country));		 
	}
	
	public int updateProfilePhone(String name,String url,String industry,String country, String timezone,String phone){
		return 	updateProfile(new CompanyProfile(name, url, timezone, industry, country,phone));		 
	}
	
	public static int updateUrl(Connection conn,String apikey,String url) throws SQLException{
		try{
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Update Company Set url = ? Where apikey = ?");
				query.setString(1, url);
				query.setString(2, apikey);
				return query.executeUpdate()>0?200:201;			
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(), e);
		}
		return 505; 
	}
	
	public static int updateEbSecretKey(String apikey,String eb_secretKey, EcomBlogType ecomBlogType){ 
		Connection conn=null;
		try{
			conn = DCON.getDefaultConnection();
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Update Company c Set eb_secretKey = ?, eb_pluginType = ? Where apikey = ?"); 
				query.setString(1, eb_secretKey); 
				query.setInt(2, ecomBlogType.ordinal()); 
				query.setString(3, apikey); 
				return query.executeUpdate()>0?200:201;	
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(), e);
		}finally{
			try {
				if(conn!=null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(), e);
			}
		}
		return 505; 
	}
	
	//==================================================================================
	//finders
	
	public static Map<String,Object> findByEmailPassword(String email, UserType userType){		
		try{
			List<Object[]> result =Company.find("select c, u.hashPassword, u.profile.username from Company c, AccountUser u where (c.apikey = u.apikey)  and " +
			 		"(u.profile.email =?) and (u.userType = ?)", email, userType).query.getResultList();
			
			if(result!=null && result.size()>0){			
				for(Object[] fields : result){
					Map<String,Object> cred = new HashMap<String,Object>();
					cred.put("company", fields[0]);
					cred.put("password", fields[1]);
					cred.put("username", fields[2]);
					return cred; 
				}			
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
		
	}
	
	public static Map<String,Object> findByEmailPassword(long id, UserType userType){		
		try{
			List<Object[]> result =Company.find("select c, u.hashPassword, u.profile.username from Company c, AccountUser u where (c.apikey = u.apikey)  and " +
			 		"(c.id =?) and (u.userType = ?)", id, userType).query.getResultList();
			
			if(result!=null && result.size()>0){			
				for(Object[] fields : result){
					Map<String,Object> cred = new HashMap<String,Object>();
					cred.put("company", fields[0]);
					cred.put("password", fields[1]);
					cred.put("username", fields[2]);
					return cred; 
				}			
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
		
	}
	
	public static String findEmail(String email,String password, UserType userType){
		String hPassword = Codec.hexMD5(password);	
		
		return Company.find("select u.profile.email from Company c, AccountUser u where (c.apikey = u.apikey)  and " +
		 		"(u.profile.email =?) and (u.hashPassword =?)  and (u.userType = ?)", email, hPassword, userType).first();	
	}
	
 	public static Company findByEmail(String email){
		return Company.find("byEmail", email).first();
	}
	
	public static Company findByDomain(String domain){
		return Company.find("byDomain", domain).first();
	}
	
	 
	public static Company findByApikey(String apikey){
        Company found = Company.find("byApikey", apikey).first();
		return found;
	}
	
	/**
	 * check if the apikey is available
	 * @param apikey
	 * @return
	 */
	public static String findApikey(String apikey){
		return (String) ((apikey == null || apikey.trim().isEmpty() ) ? null : 
			Company.find("select c.apikey from Company c where c.apikey = ?", apikey).first()); 
	}
	
	public static String findApikey(String eb_name,String eb_apikey,String eb_secretKey){
		return Company.find("select c.apikey from Company c where c.ecomBlogPlugin.eb_name = ? and c.ecomBlogPlugin.eb_apikey=? and c.ecomBlogPlugin.eb_secretKey=?",
					eb_name,eb_apikey,eb_secretKey).first(); 
	}
	
	public static String findApikey(String eb_apikey,String eb_secretKey){
		return Company.find("select c.apikey from Company c where c.ecomBlogPlugin.eb_apikey=? and c.ecomBlogPlugin.eb_secretKey=?",eb_apikey,eb_secretKey).first();
	}
	
	public static String findApikey(long companyId){
		return Company.find("select c.apikey from Company c where c.id = ?", companyId).first(); 
	}
	
	public static String findApikey(Connection conn,long companyId){
		try{
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Select apikey From Company Where id = ?");
				query.setLong(1, companyId);
				ResultSet result = query.executeQuery();
				
				while(result.next()){
					return result.getString("apikey");
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null; 
	}
	
	/**
	 * Retrieve the apikey and domain of an account using the ecomblogPlugin api and secret key of the user account
	 * @param eb_apikey
	 * @param eb_secretKey
	 * @return
	 */
	public static Map<String,String> findApikeyDomain(String eb_apikey,String eb_secretKey){
		List<Object[]> resultList= Company.find("select c.apikey , c.domain from Company c where c.ecomBlogPlugin.eb_apikey=? and c.ecomBlogPlugin.eb_secretKey=?",eb_apikey,eb_secretKey).query.getResultList();
		
		return findApikeyDomain(resultList);
	}
	
	/**
	 * Retrieve the apikey and domain of an account using the ecomblogPlugin secret key of the user account
	 * @param eb_secretKey
	 * @return
	 */
	public static Map<String,String> findApikeyDomain(String eb_secretKey){
		List<Object[]> resultList= Company.find("select c.apikey , c.domain from Company c where c.ecomBlogPlugin.eb_secretKey=?",eb_secretKey).query.getResultList();
		
		return findApikeyDomain(resultList);
	}
	
	public static Map<String,String> findApikeyDomain(String publicKey,boolean isPublickey) {
		List<Object[]> resultList= Company.find("select c.apikey , c.domain from Company c, WidgetDefault w where (c.apikey = w.apikey) and (w.publicKey=?)",publicKey).query.getResultList();
		
		return findApikeyDomain(resultList);
	}
	
	private static Map<String,String> findApikeyDomain(List<Object[]> resultList){
		for(Object[] result : resultList){
			Map<String,String> map = new HashMap<String, String>();
			map.put("apikey", String.valueOf(result[0]));
			map.put("domain", String.valueOf(result[1]));
			return map;
		}
		return null;
	}
	
	public static String findDomain(String apikey){
		return (String) ((apikey == null || apikey.trim().isEmpty() ) ? null : 
			Company.find("Select c.domain from Company c where c.apikey = ?", apikey).first()); 
	}
	
	public static String findPublickey(String eb_apikey,String eb_secretKey){
		return Company.find("select  w.publicKey from Company c, WidgetDefault w where w.apikey=c.apikey and c.ecomBlogPlugin.eb_apikey=? and c.ecomBlogPlugin.eb_secretKey=?",eb_apikey,eb_secretKey).first();
	}
	
	public static String findPublickey(String eb_apikey,String eb_secretKey, EcomBlogType ecomBlogType){
		return Company.find("select  w.publicKey from Company c, WidgetDefault w where w.apikey=c.apikey and c.ecomBlogPlugin.eb_apikey=? and c.ecomBlogPlugin.eb_secretKey=? and c.ecomBlogPlugin.eb_pluginType=?",eb_apikey,eb_secretKey,ecomBlogType).first();
	}
	
	public static String findApikeyBySecretkey(String eb_secretKey, EcomBlogType ecomBlogType){
		return Company.find("select c.apikey from Company c where c.ecomBlogPlugin.eb_secretKey=? and c.ecomBlogPlugin.eb_pluginType=?",eb_secretKey,ecomBlogType).first();
	}
	
	public static String findPublicBySecretkey(String eb_secretKey, EcomBlogType ecomBlogType){
		return Company.find("Select w.publicKey from Company c, WidgetDefault w where w.apikey=c.apikey and c.ecomBlogPlugin.eb_secretKey=? and c.ecomBlogPlugin.eb_pluginType=?",eb_secretKey,ecomBlogType).first();
	}
		
	public static Long findCompanyId(String apikey){
		if(apikey != null)
			return Company.find("select c.id from Company c where c.apikey = ? ", apikey).first();
		return null;
	} 
	
	public static Company findByEmailOrDomain(String email,String domain){
		return Company.find("select c from Company c , AccountUser u " +
				" where   c.email = u.email  and (u.email=? or c.domain=? ) ", email,domain).first();  		 
	} 
	
	public static Map<String,Object> findProfile(String apikey){
		List<Object[]> resultList =  Company.find("select c.profile, u.profile.username from Company c , AccountUser u  where " +
				"(u.apikey=c.apikey) and (c.apikey=?) and (u.userType=?)", apikey,UserType.Admin).query.getResultList();
		
		Map<String,Object> profile = new HashMap<String,Object>();
		for(Object[] fields : resultList){
			int i=0;
			profile.put("profile", fields[i++]);
			profile.put("username", fields[i++]); 
			return profile;
		}
		
		return null; 
	}
	
	public static CompanyProfile findProfileByApikey(String apikey){
		return Company.find("select c.profile from Company c where c.apikey = ? ", apikey).first();
	}
	
	public static Company findByEcomBlog(String email, String name, EcomBlogType pluginType){
		return  Company.find("select c from Company c where c.ecomBlogPlugin.eb_email=? " +
				"and c.ecomBlogPlugin.eb_name=? and c.ecomBlogPlugin.eb_pluginType=?", email,name,pluginType).first();
	}
	
	public static Company findEcomBlogByEmail(String email, EcomBlogType pluginType){
		return  Company.find("select c from Company c where c.ecomBlogPlugin.eb_email=? " +
				"and c.ecomBlogPlugin.eb_pluginType=?", email,pluginType).first();
	}
	
	public static Company findByEcomBlog(String name, EcomBlogType pluginType){
		return Company.find("Select c from Company c where c.ecomBlogPlugin.eb_name=? and c.ecomBlogPlugin.eb_pluginType=?",name,pluginType).first(); 
	}
	
	/**
	 * Search for the company account details using the the EcomBlog apikey and secrete key
	 * @param apikey
	 * @param secreteKey
	 * @param pluginType
	 * @return
	 */
	public static Company findByEcomBlogKey(String eb_apikey,String eb_secretKey, EcomBlogType pluginType){
		return Company.find("select c from Company c where c.ecomBlogPlugin.eb_apikey=? and c.ecomBlogPlugin.eb_secretKey=? and c.ecomBlogPlugin.eb_pluginType=?",eb_apikey,eb_secretKey,pluginType).first(); 
	}
	
	public static AccountType findAccTypeByEcomBlogKey(String eb_apikey,String eb_secretKey, EcomBlogType pluginType){
		return Company.find("select c.accountType from Company c where c.ecomBlogPlugin.eb_apikey=? and c.ecomBlogPlugin.eb_secretKey=? and c.ecomBlogPlugin.eb_pluginType=?",eb_apikey,eb_secretKey,pluginType).first(); 
	}
	
	public static Company findByEcomBlogKey(String eb_secretKey, EcomBlogType ecomBlogType){
		return Company.find("Select c from Company c where c.ecomBlogPlugin.eb_secretKey=? and c.ecomBlogPlugin.eb_pluginType=?",eb_secretKey,ecomBlogType).first();
	}
	
	public static List<Company> findListByEcomBlogKey(String eb_secretKey, EcomBlogType ecomBlogType){
		return Company.find("Select c from Company c where c.ecomBlogPlugin.eb_secretKey=? and c.ecomBlogPlugin.eb_pluginType=?",eb_secretKey,ecomBlogType).fetch();
	}
	
	public static String findApikey(String shop, EcomBlogType pluginType){
		return Company.find("select c.apikey from Company c where c.ecomBlogPlugin.eb_name=? and c.ecomBlogPlugin.eb_pluginType=?", shop,pluginType).first();
	}
	
	public static String findPublicKey(String shop, EcomBlogType pluginType){
		try{
			return Company.find("Select w.publicKey from WidgetDefault w, Company c where (w.apikey = c.apikey) and (c.ecomBlogPlugin.eb_name=?) and (c.ecomBlogPlugin.eb_pluginType=?)", shop,pluginType).first();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			return null;
		}
	}
	
	public static  CompanyProfile findProfileByApikey(Connection conn,String apikey) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select c.name, c.url,c.timezone,c.industry, c.country From Company c Where c.apikey = ?");
			query.setString(1, apikey);
			ResultSet result = query.executeQuery();
			
			while(result.next()){
				CompanyProfile profile = new CompanyProfile(); 
				
				profile.setName(result.getString("name"));
				profile.setTimezone(result.getString("timezone")); 
				profile.setCountry(result.getString("country"));
				profile.setUrl(result.getString("url"));
				return profile;
			}
		}
		return null; 
	}
	
	public static  String findDomainByApikey(Connection conn,String apikey) throws SQLException{ 
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select c.domain From Company c Where c.apikey = ?");
			query.setString(1, apikey);
			ResultSet result = query.executeQuery();
			
			while(result.next()){				 
				return result.getString("domain"); 
			} 
		}
		return null; 
	}
	
	public static  String findEmailByApikey(Connection conn,String apikey) throws SQLException{ 
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select c.email From Company c Where c.apikey = ?");
			query.setString(1, apikey);
			ResultSet result = query.executeQuery();
			
			while(result.next()){				 
				return result.getString("email"); 
			} 
		}
		return null; 
	}
	public static CoProfileSerializer findInfoByApikey(Connection conn,String apikey) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select c.email, c.name, c.domain, c.url, a.username, w.buttonColor, w.buttonTextColor, c.accountType, g.responseColor From Company c, " +
					" AccountUser a, WidgetDefault w, Widget g  Where (a.email = c.email) And (a.apikey = c.apikey) And (w.apikey = c.apikey) And (g.apikey=c.apikey) And (c.apikey = ?)");
			query.setString(1, apikey);
			ResultSet result = query.executeQuery();
			
			while(result.next()){
				CoProfileSerializer profile = new CoProfileSerializer();				
				profile.setName(result.getString("name"));
				profile.setEmail(result.getString("email")); 
				profile.setUsername(result.getString("username"));	
				profile.setDomain(result.getString("domain"));
				profile.setUrl(result.getString("url"));
				profile.setColor(result.getString("buttonColor"));
				profile.setTextColor(result.getString("buttonTextColor"));
				
				//search for the logo url;
				profile.setApikey(apikey);
				profile.setLogoUrl(FileDocument.findFileUrl(conn, apikey, FileType.AutoReplyLogo));
				profile.setAccountType(AccountType.valueOf(result.getInt("accountType")).name());
				profile.setResponseColor(result.getString("responseColor"));
				return profile; 
			}
		}
		return null; 
	} 
	
	public static CoProfileSerializer findInfoByApikey(String apikey){ 
		List<Object[]> results = Company.em().createQuery("Select c.email, c.profile.name, c.domain, c.profile.url, a.profile.username, w.wiParameter.buttonColor, w.wiParameter.buttonTextColor, c.accountType, g.responseColor From Company c, " +
				" AccountUser a, WidgetDefault w, Widget g  Where (a.profile.email = c.email) And (a.apikey = c.apikey) And (w.apikey = c.apikey) And (g.apikey=c.apikey) And (c.apikey = :apikey)")
				.setParameter("apikey", apikey).getResultList();
			
		for(Object[] result : results){
			CoProfileSerializer profile = new CoProfileSerializer();
			int i = 0;
			profile.setEmail((String)result[i++]);  //email
			profile.setName((String)result[i++]); 	//"name" 
			profile.setDomain((String)result[i++]); //domain
			profile.setUrl((String)result[i++]); 	//url
			profile.setUsername((String)result[i++]); //username
			profile.setColor((String)result[i++]); //buttonColor 
			profile.setTextColor((String)result[i++]); //buttonTextColor
			profile.setAccountType(((AccountType)result[i++]).name()); //accountType
			profile.setResponseColor(result[i++]);
			profile.setLogoUrl(FileDocument.findFileUrl(apikey, FileType.AutoReplyLogo));
			profile.setApikey(apikey); 
			return profile; 
		} 
		return null; 
	} 
	
	public static EcomBlogPlugin findEcomBlogPlugin(String apikey){
		return Company.find("select c.ecomBlogPlugin from Company c where c.apikey=?",apikey).first();
	}
	
	//this method is called during authentication, do not modify
 	private static Map<String, String> getUserApikeyAndDomain(List<Object[]> resultList) {
		   // create empty map to store results in. If we don't find results, an empty hashmap will be returned
		   Map<String, String> results = null; 
		   		 		   
		   // place results in map
		   for (Object[] fields: resultList){ 	
			  results = new HashMap<String, String>();
			  int i=0;
			  Long companyId =(Long)fields[i++];
			  results.put("companyId",String.valueOf(companyId) );
		      results.put("apikey",(String)fields[i++]); 
		      results.put("domain",(String)fields[i++]); 
		      results.put("accountType",String.valueOf(fields[i++])); 
		      results.put("username",(String)fields[i++]);
		      results.put("hashPassword",(String)fields[i++]);
		      results.put("userType",String.valueOf(fields[i++]));
		      
		      Object ecom = fields[i++];
		      
		      if(ecom!=null && ecom instanceof EcomBlogType){
		    	  EcomBlogType ec = (EcomBlogType) ecom;
		    	  results.put("ecomBlogPlugin", ec.name());
		    	 
		      }else{
		    	  ecom = EcomBlogType.Dropifi;
		    	  results.put("ecomBlogPlugin",EcomBlogType.Dropifi.name());
		      } 
		      
		      Object time =fields[i++];
		      Date date = new Date();
		      if(time!=null && time instanceof Timestamp){
		    	  Timestamp t = (Timestamp)time;
		    	  date = new Date(t.getTime());
		      }
		      
		      results.put("created",date.toString());
		      results.put("url", (String)fields[i++]);
		      
		      Object activated = fields[i++];
		      if(activated!=null)
		    	  results.put("activated",String.valueOf(activated));
		      else
		    	  results.put("activated",null);
		      
		      Object phone = fields[i++];
		      if(phone!=null){
		    	  results.put("phone",String.valueOf(phone));
		      }else{
		    	  results.put("phone",null);
		      }
		      
		      break; 
		   }
		   
		   return results; 
	}
	
	//this method is called during authentication, do not modify
	public static Map<String,String> authenticate (String email, String password){
		// construct and run query
		// String hPassword = Codec.hexMD5(password);  	
		try{
			List<Object[]> resultList = Company.find("select c.id, c.apikey, c.domain, c.accountType, " +
				  	"u.profile.username, u.hashPassword , u.userType, c.ecomBlogPlugin.eb_pluginType, "+
					"c.created, c.profile.url, u.profile.activated, c.profile.phone from Company c, " +
				  	"AccountUser u where (c.apikey = u.apikey)  and (u.profile.email =?) ",email).query.setMaxResults(1).getResultList();
	
			return getUserApikeyAndDomain(resultList); 
			
		}catch(Exception e){
			return null;
		}
	}
	
	//==================================================================================
	//finders
	
	/**
	 * Authenticate Wordpress installation
	 * @param email
	 * @param temToken
	 * @param ecomBlog 
	 * @return
	 */
	public static Map<String,String> authenticate (String email, String temToken,EcomBlogType ecomBlog){
		 
		List<Object[]> resultList = Company.find("select c.id, c.apikey, c.domain, c.accountType, " +
		  		"u.profile.username, c.ecomBlogPlugin.eb_secretKey, u.userType, c.ecomBlogPlugin.eb_pluginType, c.created, c.profile.url, u.profile.activated, c.profile.phone from Company c, " + 
		  		"AccountUser u where (c.apikey = u.apikey)  and (u.profile.email =?) and (c.ecomBlogPlugin.eb_secretKey = ?) and (c.ecomBlogPlugin.eb_pluginType=?) ",
		  		email,temToken,ecomBlog).query.setMaxResults(1).getResultList();

		return getUserApikeyAndDomain(resultList);
	}
	
	public static Map<String,String> authenticateBlog (String email, String temToken,EcomBlogType ecomBlog){
		List<Object[]> resultList = Company.find("select c.apikey,c.ecomBlogPlugin.eb_pluginType, w.publicKey from Company c, AccountUser u, WidgetDefault w " +
				"where (c.apikey = u.apikey) and (w.apikey = c.apikey) and (u.profile.email =?) and (u.hashPassword=?) " +
				"and (c.ecomBlogPlugin.eb_pluginType=?)",email,temToken,ecomBlog).query.setMaxResults(1).getResultList();  
	
		Map<String,String> results = new HashMap<String, String>();
		for (Object[] fields: resultList){
			int i=0;
			results.put("apikey",(String)fields[i++]); 
			results.put("pluginType",String.valueOf(fields[i++])); 
		    results.put("publicKey",(String)fields[i++]);
		    break;
		}
		return results;
	}
	
	public static Map<String,String> authenticateBlog (String email, String temToken){
		List<Object[]> resultList = Company.find("select c.apikey,c.ecomBlogPlugin.eb_pluginType, w.publicKey from Company c, AccountUser u, WidgetDefault w " +
				"where (c.apikey = u.apikey) and (w.apikey = c.apikey) and (u.profile.email =?) and (u.hashPassword=?) and (u.userType=?)",  
				 email,temToken,UserType.Admin).query.setMaxResults(1).getResultList();  
	
		Map<String,String> results = new HashMap<String, String>();
		for (Object[] fields: resultList){
			int i=0;
			results.put("apikey",(String)fields[i++]); 
			results.put("pluginType",String.valueOf(fields[i++]));
		    results.put("publicKey",(String)fields[i++]); 
		    break; 
		}
		return results;
	}
 
	public boolean createDirectory(){
		String location = play.Play.applicationPath.getAbsolutePath()+"/company/"+this.getDomain();
		File dir = new File(location);  
		if(dir.exists())
			return true;
		else			
			return dir.mkdir();
	}
	
	//----------------------------------------------------------------------------------------
	//signup messages
	
	public String getSignUpMsg(){		 
		return MailTemplate.getSignUpMsg(this.getUsername());
	}
	
	public String getWelcomMessage(){
		return MailTemplate.getWelcomeMessage(this.getUsername(),this.getDomain());
	}
	
	public String getEcomBlogWelcomMessage(){
		return MailTemplate.getEcomBlogWelcomeMessage(this.getUsername());
	}
	
	public String getShopifySignUpMsg(){	 
		return MailTemplate.getShopifySignUpMsg(this.getUsername());
	}
	
	public boolean sendWelcomeMsg(EcomBlogType ecomBlog){
		long countMsg = this.countMessages();			    			 
		if(countMsg <= 0){
			Widget widget =  Widget.findByApiKey(this.getApikey());
			
			if(widget!=null){
				String message = ecomBlog.equals(EcomBlogType.Dropifi)?this.getWelcomMessage():this.getEcomBlogWelcomMessage();  
				
				ClientSerializer cs =null;
				if(play.Play.mode == play.Play.Mode.PROD)
					cs = new ClientSerializer("Kamil Nabong", "kamil@dropifi.com", "", "Let's get you off to a good start", message);
				else 
					cs = new ClientSerializer("Effah Mensah", "philips@dropifi.com", "", "Let's get you off to a good start", message);
				
				//Serialize the message and submit it to AKKA actor to process the message - sentiment analysis, contact profile
				WidgetRespSerializer resp = new WidgetRespSerializer(widget.getCompanyId(), widget.getApikey(),
						widget.getId(),	cs, widget.getDefaultEmail(),widget.getResponseSubject(),widget.getResponseMessage(),
						widget.getResponseActivated()); 									
				
				ActorRef widgetActor = actorOf(WidgetActor.class).start();   
				widgetActor.tell(resp);		
				return true;
			} 
		} 
		return false; 
	}
	
	//------------------------------------------------------------------------------------------
	//messages
	
  	public List<DropifiMessage> getMessages(){
		return DropifiMessage.find("byCompanyId", this.getId()).fetch(); 
	}
 	
 	public static List<DropifiMessage> getMessages(String apikey){ 
 		return DropifiMessage.find("select d from DropifiMessage d, Company c where (d.companyId = c.id) and c.apikey =?", apikey).fetch();
 	}
 	
 	public long countMessages(){
 		return (Long)DropifiMessage.find("select distinct count(d.companyId) from DropifiMessage d, Company c where (d.companyId = c.id) and (c.apikey = ?)",
 				this.getApikey()).query.getSingleResult(); 
 	}
 	
	//-------------------------------------------------------------------------------------------
	//customers
	
	public boolean containsCustomer(Customer customer){
		return findCustByCIdEmail(customer.getEmail()) != null;
	}
	
	public List<Customer> getCustomers(){
		return Customer.find("byCompanyId",this.getId()).fetch();
	}
	
	public Customer findCustByCIdEmail(String email){
		return Customer.findByEmail(this.getId(), email);
	}
	
	public SubPlan findSubscriptionPlan(){
		resources.com.AccountType acctType = resources.com.AccountType.valueOf(this.getAccountType().ordinal());
		return SubPlan.findByAccountType(acctType);
	} 
	
	//--------------------------------------------------------------------------------------------
	//retrieve all the company inbound mailbox
	public List<InboundMailbox> findAllInboundMb(){
		return this.setting.getChannel().getInboundMbs();
	} 
	
	//--------------------------------------------------------------------------------------------
	//agents
	public List<AccountUser> getAccountAgents(){
		return AccountUser.find("byCompanyIdAndUserType", this.getId(),UserType.Agent).fetch();
	}
	
	//--------------------------------------------------------------------------------------------
	//addons
	/*public static int installAddon(String apikey,String platformKey,String token){
		SubCustomer customer = SubCustomer.findByAppikey(apikey);
		if(customer!=null && customer.getCustomerId()!=null && !customer.getCustomerId().trim().isEmpty()){
			
			AddonPlatform platform = AddonPlatform.findByPlatformKey(platformKey);
			if(platform!=null){
				//subscribe the user to the selected addon plan
				Subscription sub = customer.subscribeToAddon(DropifiTools.StripeSecretKey, platform.getSubscriptionPlan());
				
				if(sub!=null){
					//retrieve the uid of the
					AddonSubscription addon = new AddonSubscription(uid, secretKey, customerId, subscriptionId)
				}
			}else{
				return 301;
			}
			
		}
		
		return 501;
	} 
	*/
	
	//--------------------------------------------------------------------------------------------
	//getters and setters
	
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain.toLowerCase();
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
		
	public Setting getSetting(){
		return this.setting;
	}
	
	private void setSetting(Setting setting){
		this.setting = setting;
	}
	
    public String getEmail(){
		return email;
	}
	    
    private String getPassword() {
		return password;
	}

	private void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email.toLowerCase();
	}
	
	public AccountUser getAccountAdmin(){
		return AccountUser.find("select u from AccountUser u, Company c where (u.companyId = c.id) and (c.id =?)" +
				" and (u.email= ?) and u.userType =?", this.getId(), this.getEmail(), UserType.Admin).first();
	}

	public String getApikey() {
		return apikey;
	} 

	private void setApikey(){ 
		this.apikey = Codec.hexSHA1(this.getEmail() + new Date().getTime());
	}

	public CompanyProfile getProfile() {
		return profile;
	}
	
	public void setProfile(CompanyProfile profile) {
		this.profile = profile;
		if(profile!=null && this.getUsername()==null){
			this.setUsername(profile.getName());
		}
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	private String getUsername() {
		return username!=null?username:"";
	}
	
	private void setUsername(String username) {
		this.username = username;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public static List<String> usedDomains(){
		List<String> domains = new LinkedList<String>();
		domains.add("login");
		domains.add("customers");
		domains.add("pricing");
		domains.add("dropifi");
		domains.add("index");
		domains.add("sitemap");
		domains.add("sitemap.xml");
		domains.add("features");
		domains.add("signup");
		domains.add("company");
		domains.add("domain");
		domains.add("logout");
		domains.add("widget");
		domains.add("apps");
		domains.add("clientswidgets");
		domains.add("reset_password");
		domains.add("reset_user_password");
		domains.add("reset_user_email");
		domains.add("create_new_password");
		domains.add("reset");
		domains.add("forgot_password");		
		domains.add("legal_terms");
		domains.add("ecommerce");
		domains.add("message_display");
		domains.add("robot");
		domains.add("robots");
		domains.add("robots.txt");
		domains.add("addon");
		domains.add("addons");
		domains.add("thirdparty");
		domains.add("thirdparties");
		return domains;
	}

	public EcomBlogPlugin getEcomBlogPlugin() {
		return ecomBlogPlugin;
	}

	public void setEcomBlogPlugin(EcomBlogPlugin ecomBlogPlugin) {
		this.ecomBlogPlugin = ecomBlogPlugin;
	}
}
