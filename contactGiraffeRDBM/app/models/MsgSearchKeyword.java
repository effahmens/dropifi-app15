package models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import play.data.validation.Required;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;

@Embeddable
public class MsgSearchKeyword implements Serializable{
	
	@Column(nullable=false)
	private Long companyId; 
	
	@SerializedName("keyword")
	@Column(nullable=false) @Required
 	private String keyword;
	
	@SerializedName("activated")
	@Column(nullable=false) @Required 
	private Boolean activated;
	
	@SerializedName("created")
	@Column(nullable=false)
	private Timestamp created;
	
	@SerializedName("updated")
	@Column(nullable=false)
	private Timestamp updated;
	
	@Transient
	@SerializedName("id")
	private String id; 
	
	//---------------------------------------------------------------------------------
	//constructor
	
	public MsgSearchKeyword(String keyword, Boolean activated) {	 
		this.setKeyword(keyword);
		this.setActivated(activated);
	}
		
	//--------------------------------------------------------------------------------
	//getters and setters
	
	public static JsonArray toJsonArray(List<MsgSearchKeyword> msgKeywords){
		JsonArray arrayJson = new JsonArray();
		if(msgKeywords !=null){
			Gson gson = new Gson();
			ListIterator<MsgSearchKeyword> msgKeyword = msgKeywords.listIterator();
			
			int n=1;
			while(msgKeyword.hasNext()){
				MsgSearchKeyword msg = msgKeyword.next();
				msg.setId(String.valueOf(n++)); 
				arrayJson.add(gson.toJsonTree(msg)); 
			}
		}
		
		return arrayJson;
	}
	
	public static List<MsgSearchKeyword> getMsgSearchKeyword(List<String> listKeywords){
		List<MsgSearchKeyword> keywords = new LinkedList<MsgSearchKeyword>();
		for(String listKeyword : listKeywords){
			JsonObject json =(JsonObject) new JsonParser().parse(listKeyword);
			keywords.add(new MsgSearchKeyword(json.get("keyword").getAsString(),json.get("activated").getAsBoolean()));
		}
		return keywords;
	}
	
	@Override
	public String toString() {
		return "MsgSearchKeyword [keyword=" + keyword + ", activated="
				+ activated + ", created=" + created + ", updated=" + updated
				+ ", id=" + id + "]";
	}

	public String getKeyword() {
		return keyword;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MsgSearchKeyword other = (MsgSearchKeyword) obj;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		return true;
	}

	public void setKeyword(String keyword) {
		this.keyword =keyword==null?null: keyword.trim();
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public Long getCompanyId() {
		return companyId;
	}
	

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
 
}
