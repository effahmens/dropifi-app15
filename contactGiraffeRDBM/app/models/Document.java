package models;

import java.io.Serializable;
import java.sql.Connection;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;

import com.google.gson.annotations.SerializedName;

@Embeddable
public class Document implements Serializable{
	
	@EmbeddedId
	@SerializedName("fileName")
	private String fileName;
	@SerializedName("fileType")
	private String fileType; 
	@SerializedName("comment") 
	private String comment;
	
	
	public Document(String fileName, String fileType, String comment) {		 
		this.fileName = fileName;
		this.fileType = fileType;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "Document [fileName=" + fileName + ", fileType=" + fileType
				+ ", comment=" + comment + "]";
	}

	//-----------------------------------------------------------------------------------------------
	//getters and setters
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}	 

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
