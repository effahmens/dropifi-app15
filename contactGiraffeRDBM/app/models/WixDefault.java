package models;

import java.awt.Color;
import java.util.Collection;
import java.util.Iterator;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;

import models.dsubscription.SubServiceName;

import play.data.validation.Required;
import play.db.jpa.Model;
import resources.AccountType;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.WidgetField;
import resources.CacheKey;
import resources.WixDefaultSerializer;
import resources.helper.ServiceSerializer;
import view_serializers.WCParameter;

@Entity
public class WixDefault extends Model{
	@Required @Column(nullable=false,unique=true)
	private String instanceId;
	@Index(name="userId")
	private String userId;
	private String compId;
	private String origCompId;
	@Embedded
	private WiParameter wiParameter;
	@Index(name="apikey")
	private String apikey;
	@Column(length=100000)
	private String html;
	private AccountType accountType;
	private String email;
	
	public WixDefault(String instanceId, String userId) { 
		this(instanceId,userId,null,null);
	}
	
	public WixDefault(String instanceId, String userId, String compId,
			String origCompId) { 
		this(instanceId,userId,compId,origCompId,null,null); 
	}
	
	public WixDefault(String instanceId, String userId, String compId,
			String origCompId,AccountType accountType,String email) {
		super();
		this.instanceId = instanceId;
		this.userId = userId;
		this.compId = compId;
		this.origCompId = origCompId;
		this.accountType = accountType;
		this.email = email;
	} 
	
	public WixDefault createWixDefault(){
		this.setAccountType(AccountType.Free);
		Widget widget = new Widget("",0l).getUnSavedWidget(this.getInstanceId());
		widget.setHasCaptcha(false);
		return this.updateWixDefault(widget);
	}
	
	private Widget updateWixDefault(WiParameter params){ 
		Widget widget = new Widget("",0l).getUnSavedWidget(this.getInstanceId(),params);
		widget.setHasCaptcha(false);
		return widget;
	} 
	
	public WixDefault updateWixDefault(Widget widget){
		this.setWiParameter(widget.getWiParameter());
		String publicKey = Company.findPublicBySecretkey(this.getInstanceId(),EcomBlogType.Wix); 
		
		this.setHtml(WidgetDefault.generateHtmlTwo(widget, (publicKey!=null?publicKey:""), this.getApikey()));
 		return this.save(); 
	} 
	
	public WixDefault updateWixDefault(String buttonPosition,String buttonColor,String buttonText){
		WiParameter wiParam = this.getWiParameter();
		if(buttonPosition!=null &&!buttonPosition.trim().isEmpty()){
			wiParam.setButtonPosition(buttonPosition);
		}
		
		if(buttonColor!=null &&!buttonColor.trim().isEmpty()){
			wiParam.setButtonColor(buttonColor);
		}
		
		if(buttonText!=null &&!buttonText.trim().isEmpty()){
			wiParam.setButtonText(buttonText);
		}
		
		Widget widget = Widget.findByApiKey(this.getApikey());
		
		if(widget==null)
			widget = new WixDefault(this.getInstanceId(), this.getUserId()).updateWixDefault(wiParam);
		else
			widget.setWiParameter(wiParam);
		
		this.setWiParameter(wiParam); 
		
		//this.setHtml(this.generateHtml(widget));
		String publicKey = Company.findPublicBySecretkey(this.getInstanceId(),EcomBlogType.Wix); 
		this.setHtml(WidgetDefault.generateHtmlTwo(widget, (publicKey!=null?publicKey:""), this.getApikey()));
		return this.save();
	}
	
	public static void updateUserEmail(String apikey, String currentEmail, String newEmail){
		try{
			WixDefault wix = WixDefault.findByApikey(apikey);
			if(wix!=null && wix.getEmail().equals(currentEmail)){
				wix.setEmail(newEmail);
				wix.save();
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static void updateAccountType(String instanceId, AccountType accountType){
		try{
			WixDefault wix = WixDefault.findByInstanceId(instanceId);
			if(wix!=null){
				wix.setAccountType(accountType);
				wix.save();
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static WixDefault getWixDefault(String instanceId,String uid,String compId,String origCompId){ 
		WixDefault found = findByInstanceId(instanceId);
		if(found==null){
			found = new WixDefault(instanceId, uid,compId,origCompId);
			found = found.createWixDefault();
		}else{
			found.setCompId(compId);
			found.setOrigCompId(origCompId); 
			Widget widget = found.updateWixDefault(found.getWiParameter()); 
			found = found.updateWixDefault(widget);
		}
		return found; 
	}
	
	public static WixDefaultSerializer getWixDefaultSerializer(String apikey){
		WixDefault wix = WixDefault.findByApikey(apikey);
		if(wix!=null){ 
			return new WixDefaultSerializer(wix);
		}
		return null;
	} 
	
	public static WixDefault findByInstanceId(String instanceId){
		return WixDefault.find("Select w from WixDefault w Where w.instanceId=? ", instanceId).first();
	}
	
	/**
	 * 
	 * @param instanceId
	 * @return
	 */
	public static WixDefault findWix(String instanceId){
		WixDefault wix = findByInstanceId(instanceId);
		try{
			if(wix!=null && (wix.getAccountType()==null || wix.getEmail()==null)){
				Company company = Company.findByEcomBlogKey(instanceId, EcomBlogType.Wix);
				if(company!=null){
					wix.setEmail(company.getEmail());
					wix.setAccountType(company.getAccountType());
					wix.updateWixDefault(Widget.findByApiKey(company.getApikey())); 
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return wix;
	}
	
	public static WixDefault findByInstanceId(String instanceId,String uid){
		return WixDefault.find("Select w from WixDefault w where w.instanceId=? and w.userId=?", instanceId,uid).first();
	}
	
	public static WixDefault findByApikey(String apikey){
		return WixDefault.find("Select w from WixDefault w where w.apikey=?", apikey).first();
	} 
	
	public static WiParameter findParamsByInstanceId(String instanceId){
		return WixDefault.find("Select w.wiParameter from WixDefault w where w.instanceId=?", instanceId).first(); 
	}
	
	public static WiParameter findParamsByApikey(String apikey){
		return WixDefault.find("Select w.wiParameter from WixDefault w where w.apikey=?", apikey).first();
	}
	
	public static String findHtmlByInstanceId(String instanceId){
		return WixDefault.find("Select w.html from WixDefault w where w.instanceId=?", instanceId).first();
	}
	
	//===============================================================================
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public WiParameter getWiParameter() {
		return wiParameter;
	}
	public void setWiParameter(WiParameter wiParameter) {
		this.wiParameter = wiParameter;
		this.wiParameter.setButtonTextColor(this.wiParameter.setImageText(Widget.FILENAME,wiParameter.getButtonText(), this.getInstanceId()));
	}
	
	public String getApikey() {
		return apikey;
	}
	
	public void setApikey(String apikey) {
		this.apikey = apikey!=null?apikey:"";
	}
	
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}

	public String getCompId() {
		return compId;
	}

	public void setCompId(String compId) {
		this.compId = compId;
	}

	public String getOrigCompId() {
		return origCompId;
	}

	public void setOrigCompId(String origCompId) {
		this.origCompId = origCompId;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
