package models;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Id;

import play.db.jpa.Model;

public class CompanyLocation extends Model{
	@Id @Column(nullable=false,unique=true)
	private String apikey;
	@Embedded
	private GeoLocation location;
}
