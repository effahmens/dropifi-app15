package models.review;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;
import org.joda.time.DateTime;

import play.db.jpa.Model;
import resources.EcomBlogType;

@Entity
public class ReviewApp extends Model{ 

	@Column(unique=true, nullable=false)
	private String apikey;
	
	private EcomBlogType pluginType;
	
	private Timestamp lastReview;
	private Timestamp nextReview;
	
	private Timestamp created;
	
	 @Index(name="hasReview")
	private boolean hasReview;
	
	public ReviewApp(String apikey, EcomBlogType pluginType){
		this.setApikey(apikey);
		this.setPluginType(pluginType);
		this.setHasReview(false);
	}
	
	public ReviewApp(String apikey, String pluginType){
		this.setApikey(apikey); 
		this.setPluginType(pluginType);
		this.setHasReview(false);
	}
	
	//-----------------------------------------------------------------------------------------------------
	// 
	
	private boolean addReview(){ 
		DateTime time = new DateTime();
		
		//set tha last review date
		Timestamp lastDate = new Timestamp(time.now().toDate().getTime());
		this.setLastReview(lastDate);
		
		//set the next review date-time
		DateTime next = time.now().plusMinutes(5);	 
		this.setNextReview(next.toDate().getTime());
		
		return this.validateAndSave();  		
	}
	
	public static int updateReview(String apikey, boolean isReviewed){   
		DateTime time = new DateTime(); 
		
		//set the next review date-time
		DateTime next = (isReviewed ? time.now().plusWeeks(4) : time.now().plusMinutes(2));
		
		int result = ReviewApp.em().createQuery("Update ReviewApp r SET r.nextReview = :nextReview, r.lastReview = :lastReview, r.hasReview = :hasReview Where r.apikey = :apikey")
		.setParameter("apikey", apikey).setParameter("hasReview", false)
		.setParameter("nextReview", new Timestamp(next.toDate().getTime())).setParameter("lastReview", new Timestamp(time.now().toDate().getTime())).executeUpdate();
		
		return result>=1?200:201;
	} 
	
	public ReviewApp findReview(){
		return findReview(this.getApikey());
	}
	
	public static ReviewApp findReview(String apikey){
		return ReviewApp.find("Select r From ReviewApp r where r.apikey=?", apikey).first();
	}
	
	public boolean showReview(Timestamp created){
		boolean show =false;
		ReviewApp review = ReviewApp.findReview(this.getApikey());
		if(review==null){ 
			//create a new review and save
			if(this.addReview())
				review = this; 
			else
				return false; 
		}else{
			review.setPluginType(this.getPluginType());
		} 
		
		//check if user has to give the next review		
		DateTime timeNow = new DateTime();
		
		if(timeNow.isAfter(review.getNextReview().getTime())){
			//check if the user has not reviewed
			if(!review.hasReview() && EcomBlogType.isEcomBogType(review.getPluginType()) ){ 
				try{
				review.save(); 
				}catch(Exception e){}
				show = true;
			}
		}
		return show;
	}
	
	
	//-----------------------------------------------------------------------------------------------------
	//getters and setters

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public EcomBlogType getPluginType() {
		return pluginType;
	}

	public void setPluginType(EcomBlogType pluginType) {
		this.pluginType = pluginType;
	}
	
	public void setPluginType(String pluginType) {
		if(EcomBlogType.isEcomBogType(pluginType))
			this.pluginType =EcomBlogType.valueOf(pluginType);
		else
			this.pluginType = EcomBlogType.Dropifi;
	}

	public Timestamp getLastReview() {
		return lastReview;
	}

	public void setLastReview(Timestamp lastReview) {
		this.lastReview = lastReview;
	}

	public Timestamp getNextReview() {
		return nextReview;
	}

	public void setNextReview(Timestamp nextReview) {
		this.nextReview = nextReview;
	}
	
	public void setNextReview(Long nextReview) {
		this.nextReview = new Timestamp(nextReview);
	}

	public boolean hasReview() {
		return hasReview;
	}

	public void setHasReview(boolean hasReview) {
		this.hasReview = hasReview;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
 
	
	
}
