/**
 * @author phillips effah mensah
 * @description 
 * @date 14/3/2012
 *  
 **/

package models;

import play.data.validation.*;
import play.db.jpa.Blob;
import play.db.jpa.Model; 
import dsubscription.resources.com.AccountType;
import models.dsubscription.SubServiceName;
import play.data.validation.*;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.SQLInjector;
import resources.TextToImage;
import resources.WidgetControlType;
import resources.WidgetControlValidator;
import resources.WidgetField;
import resources.helper.ServiceSerializer;
import view_serializers.ConSerializer;
import view_serializers.WiSerializer;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.Index;

import api.tictail.TictailStore;


@Entity
public class Widget extends Model{

	//-------------------------------------------------------------------------------------------------------- 
	//instance variables
	@Required @Column(nullable=false,unique=true) 
	private Long companyId; 
	@Required	@Column(nullable = false, unique=true) 
	private String apikey;
	
	@Column(length=10000)
	private String header;
	private WiParameter wiParameter;
	
	@Column(length=10000)
	private String defaultMessage;
	@Column(nullable=false)
	private String defaultEmail;
	@Column(length=10000)
	private String responseSubject;
	@Column(length=10000)
	private String responseMessage;
	@Column(nullable=false)
	private Boolean responseActivated;
	private String responseColor;
	
	@Column(length=10000)
	private String sendButtonText;	 
	private String sendButtonColor;
	
	private String messageTemplate; 
	
	@Transient
	private String plugin;
		
	@ElementCollection(targetClass = WidgetControl.class, fetch=FetchType.EAGER) 
	@CollectionTable(name="widgetControl", joinColumns= @JoinColumn(name="widget_id")) 	 
	private Collection<WidgetControl> widgetControls;
	 
	private Boolean hasCaptcha; 
	private String captchaValue;
	
	@Transient
	public static final String FILENAME="tab_text.png";
	//--------------------------------------------------------------------------------------------------------
	//constructor:
	
	public Widget(String apikey,long companyId){		 
		 this.apikey = apikey;		  
		 this.setCompanyId(companyId);
		 this.setDefaultEmail("other");		 
		 this.setDefaultMessage("Thank you");
		 
		 this.setResponseActivated(true);
		 this.setResponseSubject("We will get back to you soon");
		 this.setResponseMessage("Thank you for contacting us! <br>We have received your request and will get back to you very soon.");
		 this.setResponseColor("#0D60B3");
		 
		 this.setSendButtonText("Send Message");
		 this.setSendButtonColor("#1E7BB8");
		 
		 this.setHasCaptcha(true);
		 this.setCaptchaValue("What's the result of");
		 
		 this.widgetControls = new LinkedList<WidgetControl>();
		 
		 this.setMessageTemplate("Dropifi Template");
	}
	
	public Widget(String apikey,long companyId,String defaultEmail) { 		 
		 this.apikey = apikey;		  
		 this.setCompanyId(companyId);
		 this.setDefaultEmail(defaultEmail); 		 
		 this.setDefaultMessage("Thank you"); 
		 
		 this.setResponseActivated(true);
		 this.setResponseSubject("We will get back to you soon");
		 this.setResponseMessage("Thank you for contacting us! <br>We have received your request and will get back to you very soon.");
		 this.setResponseColor("#0D60B3");
		 
		 this.setSendButtonText("Send Message");
		 this.setSendButtonColor("#1E7BB8"); 
		 
		 this.setHasCaptcha(true);
		 this.setCaptchaValue("What's the result of");
		 
		 this.widgetControls = new LinkedList<WidgetControl>(); 
		 
		 this.setMessageTemplate("Dropifi Template");
	}
	
	public Widget(WiParameter params){
		this.setWiParameter(params);
	}
	
	public Widget(PreviewWidget widget){
		 this.apikey = widget.getApikey(); 
		 this.header = widget.getHeader();
		 
		 this.setCompanyId(widget.getCompanyId());
		 this.setDefaultEmail(widget.getDefaultEmail()); 		 
		 this.setDefaultMessage(widget.getDefaultMessage()); 
		 
		 this.setResponseActivated(widget.getResponseActivated());
		 this.setResponseSubject(widget.getResponseSubject());
		 this.setResponseMessage(widget.getResponseMessage());
		 this.setResponseColor(widget.getResponseColor());
		 
		 this.setSendButtonText(widget.getSendButtonText());
		 this.setSendButtonColor(widget.getSendButtonColor()); 
		 
		 this.setHasCaptcha( widget.getHasCaptcha()); 
		 this.setCaptchaValue(widget.getCaptchaValue());
		 
		 this.widgetControls = widget.getWidgetControls(); 
		 this.setWiParameter(widget.getWiParameter());
		 
		 this.setMessageTemplate(widget.getMessageTemplate());
	}
	
	public Widget(){} 
	
	//--------------------------------------------------------------------------------------------------------
	//widget: 
	
	@Override
	public String toString() {
		return "Widget [companyId=" + companyId + ", apikey=" + apikey
				+ ", header=" + header + ", wiParameter=" + wiParameter
				+ ", defaultMessage=" + defaultMessage + ", defaultEmail="
				+ defaultEmail + ", responseSubject=" + responseSubject
				+ ", responseMessage=" + responseMessage
				+ ", responseActivated=" + responseActivated
				+ ", responseColor=" + responseColor + ", sendButtonText="
				+ sendButtonText + ", sendButtonColor=" + sendButtonColor
				+ ", messageTemplate=" + messageTemplate + ", widgetControls="
				+ widgetControls + ", hasCaptcha=" + hasCaptcha + ", captchaValue=" + captchaValue + "]";
	} 
	
	/**
	 * get an instance of the widget with default control values with out saving the widget
	 * @return
	 */
	public Widget getUnSavedWidget(String domain){
		this.setTitle("Contact Us");
		this.setWiParameter(new WiParameter("#0D60B3", "Contact Us",DropifiTools.defaultFont,"right",35,"#0D60B3",domain,FILENAME)); 
		//add default controls to the widget
		this.addDefaultControls();		
		return this;
	} 

	public Widget getUnSavedWidget(String domain,WiParameter param){
		this.setTitle("Contact Us");
		this.setWiParameter(new WiParameter(param.getButtonColor(), param.getButtonText(),DropifiTools.defaultFont,param.getButtonPosition(),35,"#0D60B3",domain,FILENAME)); 
		//add default controls to the widget
		this.addDefaultControls();
		return this;
	}
	
	public Widget createWidget(){
		
		if(apikey !=null && !apikey.isEmpty()){  
			
			this.setTitle("Contact Us");
			this.setWiParameter(new WiParameter("#0D60B3", "Contact Us",DropifiTools.defaultFont,"right",35,"#0D60B3")); 
			//add default controls to the widget
			this.addDefaultControls();
			
			try{ 				
				Widget widget = this.save();			 
				
				if(widget.getId() !=null && widget.getApikey()!=null){
					MsgCounter counter = new MsgCounter(widget.getCompanyId(), widget.getId(), widget.getClass().getSimpleName()+" Message"); 
					if(counter.createCounter()==200){				
						WidgetDefault defaultWidget = new WidgetDefault(widget); 					
						defaultWidget = defaultWidget.createWidgetDefault(); 						
						return defaultWidget != null ? widget:null; 
					}
				}
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		
		return null;
		
	}
	
	public int updateWidget(){ 
		Widget widget = this.save(); 
		WidgetDefault widgetDefault = WidgetDefault.findByApiKey(this.getApikey());
		
		if(widgetDefault !=null){
			//generateAsyncSnippet(widgetDefault.getPublicKey()); 
			if(widgetDefault.updateWidgetDefault(widget) != null ){
				return 200;
			}		
		}  			 
		return 360; 
	}
	
	private void addDefaultControls(){
		
		int i=1;
		this.widgetControls = new LinkedList<WidgetControl>();
		this.widgetControls.add(new WidgetControl(WidgetField.name, "your full name", WidgetControlType.Text, WidgetControlValidator.None, i++, true, false,"your name is required"));
		this.widgetControls.add(new WidgetControl(WidgetField.email,"your email", WidgetControlType.Email, WidgetControlValidator.Email, i++, true, true,"your email is required"));
		this.widgetControls.add(new WidgetControl(WidgetField.phone,"your phone", WidgetControlType.Text, WidgetControlValidator.None, i++, true, false,"your phone number is required"));
		this.widgetControls.add(new WidgetControl(WidgetField.subject,"your subject", WidgetControlType.Text, WidgetControlValidator.Email, i++, true, true,"the subject of the message is required"));
		this.widgetControls.add(new WidgetControl(WidgetField.message,"how can we help you?", WidgetControlType.TextArea, WidgetControlValidator.None, i++, true, true,"your message is required"));			
		this.widgetControls.add(new WidgetControl(WidgetField.attachment,"attach files", WidgetControlType.Attachment, WidgetControlValidator.None, i++, true, false,"you have not attached any file"));
	
	}
	
	private void addDefaultControls(Collection<WidgetControl> widgetControls){
		this.widgetControls = new LinkedList<WidgetControl>();
		for(WidgetControl control : widgetControls){
			this.widgetControls.add(control);
		}
	}

	/**
	 * Called any time a user subscribed to a Plan 
	 * enable dropifi branding on the widget if the plan subscribed to does not permit removal of branding
	 * @param apikey
	 */
	public static void autoUpdateWidget(String apikey,String domain, String currentPlan){
		try{
			Widget widget = Widget.findByApiKey(apikey);
			
			if(widget!=null){
				WiParameter param = widget.getWiParameter();
				widget.updateWidget(domain, widget,param);
				
				//save the existing widget setting
				if(!currentPlan.equalsIgnoreCase(AccountType.Free.name())){
					WidgetDownGrade downgrade = new WidgetDownGrade(apikey, AccountType.valueOf(currentPlan), param);
					downgrade.createUpate();  
				}else if(currentPlan.equalsIgnoreCase(AccountType.Free.name())){
					/**
					 * Revert the user settings to default Dropifi settings if the user subscribe to the free plan.
					 */ 
					widget.updateWidget(new Widget(widget.getApikey(), widget.getCompanyId(), widget.getDefaultEmail()).getUnSavedWidget(domain));
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	private static void updateWidget(String domain, Widget widget, WiParameter param){
		try{
			ServiceSerializer service = CacheKey.getServiceSerializer(widget.getApikey(),CacheKey.Widget, SubServiceName.Branding);
			if(service==null || service.hasExceeded || param.getButtonFont()==null || param.getButtonFont().trim().isEmpty()){
				widget.getWiParameter().setButtonFont(DropifiTools.defaultFont);
			}
			
			if(widget.getSendButtonColor()==null || widget.getSendButtonColor().trim().isEmpty()){ 	
				 widget.setSendButtonColor("#1E7BB8"); 
			}
			
			if(widget.getSendButtonText()==null || widget.getSendButtonText().trim().isEmpty()){
				widget.setSendButtonText("Send Message"); 
			}
			
			if(widget.getMessageTemplate()==null){
				widget.setMessageTemplate("Dropifi Template");
			}
			
			widget.getWiParameter().setButtonTextColor(widget.getWiParameter().setImageText(FILENAME,param.getButtonText(), domain)); 
			widget.updateWidget();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	/**
	 * Called any time an invoice.payment_succeeded or charge.succeeded webhook event is processed.
	 * if the the user account is suspended, the system will automatically upgrade the user to the plan subscribed
	 * @param apikey
	 * @param domain
	 * @param currentPlan
	 * 
	 */
	public static void systemUpgradeWidget(String apikey,String domain, String currentPlan){
		try{ 		
			Widget widget = Widget.findByApiKey(apikey);
 
			if(widget!=null){
				WiParameter param = null; 
				WidgetDownGrade dow = WidgetDownGrade.findByApikey(apikey);
				if(dow!=null && !dow.getCurrentPlan().name().equalsIgnoreCase(currentPlan)){
					param = dow.getWiParameter();
				}else{
					param = widget.getWiParameter();
				}
				
				
				if(currentPlan.equalsIgnoreCase(AccountType.Free.name())){
					/**
					 * Revert the user settings to default Dropifi settings if the user subscribe to the free plan.
					 */ 
					widget.updateWidget(new Widget(widget.getApikey(), widget.getCompanyId(), widget.getDefaultEmail()).getUnSavedWidget(domain));
				}else{ 
					widget.updateWidget(domain, widget,param);
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public int updateWidgetAfterSignUp(String domain) {
		this.getWiParameter().setButtonTextColor(this.getWiParameter().setImageText(FILENAME,this.getWiParameter().getButtonText(), domain));
		return this.updateWidget();		 
	}
	
	public int updateWidget(WiParameter widget,String domain){
		this.setWiParameter(new WiParameter(widget.getButtonColor(), widget.getButtonText(), widget.getButtonFont(),
				widget.getButtonPosition(),widget.getButtonPercent(), widget.getButtonTextColor(),domain,FILENAME));
		
		return this.updateWidget();
	}
	
	public int updateWidget(WiSerializer widget){ 
		
		if(widget != null){ 
			//set the widget parameters
			this.setTitle(widget.getWidgetheader());
			this.setWiParameter(new WiParameter(widget.getButtonColor(), widget.getButtonText(), widget.getButtonFont(),
					widget.getButtonPosition(),widget.getButtonPercent(), widget.getButtonTextColor(),widget.getDomain(),FILENAME ) );
			
			this.setDefaultEmail(widget.getDefaultEmail());
			this.setDefaultMessage(widget.getDefaultMessage()); 
			this.setResponseSubject(widget.getResponseSubject());
			this.setResponseMessage(widget.getResponseMessage()); 
			this.setResponseColor(widget.getResponseColor());
			
			this.setHasCaptcha(widget.isHasCaptcha());
			this.setCaptchaValue(widget.getCaptchaValue());
			
			if(this.getResponseColor()==null || this.getResponseColor().trim().isEmpty()){
				this.setResponseColor("#1E7BB8");
			}
			 			
			this.setSendButtonText(widget.getSendButtonText());
			this.setSendButtonColor(widget.getSendButtonColor());
			
			if(widget.getSendButtonColor()==null || widget.getSendButtonColor().trim().isEmpty()){ 	
				 this.setSendButtonColor("#1E7BB8"); 
			}
			
			if(widget.getSendButtonText()==null || widget.getSendButtonText().trim().isEmpty()){
				this.setSendButtonText("Send Message");
			}
			
			if(widget.getMessageTemplate()!=null)
				this.setMessageTemplate(widget.getMessageTemplate());
			
			//set the widget control parameters
			for(WidgetControl control: this.getWidgetControls()){
				
				switch(control.getCondId().ordinal()){
					case 0: //attachment
						control.setTitle( widget.getAttachment());
						control.setActivated(widget.isActattachment());
						break;
					case 1: //Email
						control.setTitle(widget.getEmail());
						break;
					case 2: //name
						control.setTitle(widget.getName());
						control.setActivated(widget.isActname());
						break;
					case 3: //message
						control.setTitle(widget.getMessage());
						break;
					case 4: //phone
						control.setTitle(widget.getPhone());
						control.setActivated(widget.isActphone());
						break;
					case 5: //subject
						control.setTitle(widget.getSubject());
						control.setActivated(widget.isActsubject());
						break;			
				}
			}
			return this.updateWidget(); 
		}
		return 501;
	}
	
	public int updateWidget(Widget widget){
		
		if(widget != null){ 
			//set the widget parameters
			this.setTitle(widget.getTitle());
			this.setWiParameter(widget.getWiParameter());
			
			this.setDefaultEmail(widget.getDefaultEmail());
			this.setDefaultMessage(widget.getDefaultMessage()); 
			this.setResponseSubject(widget.getResponseSubject());
			this.setResponseMessage(widget.getResponseMessage()); 
			this.setResponseColor(widget.getResponseColor());
			
			this.setHasCaptcha(widget.getHasCaptcha());
			this.setCaptchaValue(widget.getCaptchaValue());
			
			if(this.getResponseColor()==null || this.getResponseColor().trim().isEmpty()){
				this.setResponseColor("#1E7BB8");
			}
			 			
			this.setSendButtonText(widget.getSendButtonText());
			this.setSendButtonColor(widget.getSendButtonColor());
			
			if(widget.getSendButtonColor()==null || widget.getSendButtonColor().trim().isEmpty()){ 	
				 this.setSendButtonColor("#1E7BB8"); 
			}
			
			if(widget.getSendButtonText()==null || widget.getSendButtonText().trim().isEmpty()){
				this.setSendButtonText("Send Message");
			}
			
			if(widget.getMessageTemplate()!=null)
				this.setMessageTemplate(widget.getMessageTemplate());
			
			 
			//set the widget control parameters
			this.addDefaultControls(widget.getWidgetControls()); 
			return this.updateWidget(); 
		}
		return 501;
	}
	
	public int updateWidgetErrorMsg(HashMap<String,String> errorMsgs){
		//set the errorMsg for the controls
		if(errorMsgs!=null){
			try{
				for(WidgetControl control: this.getWidgetControls()){ 
					String value = errorMsgs.get(control.getCondId().name());
					if(value!=null && value.trim().length()>=1)
						control.setErrorMsg(value);
				}
				return this.updateWidget(); 
			}catch(Exception e){}
		}
		return 501;
	}
	
	public static int updateDefaultEmail(String apikey, String currentEmail,String newEmail){
		int result = Widget.em().createQuery("Update Widget w Set w.defaultEmail=:newEmail Where w.apikey=:apikey and w.defaultEmail=:currentEmail")
				.setParameter("newEmail", newEmail)
				.setParameter("apikey", apikey)
				.setParameter("currentEmail", currentEmail).executeUpdate();
		return result>=0?200:201;	 
	}
	
	public String embedCodeMsg(String username,String email){
		 return "Hi,\n\n"+(username!=null?username:email) +" will like you to put the code snippet in the header tag of the html page.";
	}
	
	@Deprecated
	public String generatePluginDev(String publicKey){ 
		StringBuilder script = new StringBuilder(); 
		script.append("<script type='text/javascript' src='"+DropifiTools.ServerURL+"/public/client/javascripts/iframe/new_widget/dropifi_widget.min.js'></script>"); 
		script.append("<script type='text/javascript'>document.renderDropifiWidget('"+publicKey+"');</script>");

		this.plugin = script.toString(); 
		return this.plugin;
	}
	
	@Deprecated
	public String generateAsyncPluginDev(String publicKey){
		this.plugin ="<script type='text/javascript' id='dropifiwidget_script' data-widgetkey='"+publicKey+"' src='"+DropifiTools.ServerURL+"/public/client/javascripts/iframe/new_widget/dropifi_widget.min.js'></script>";  
		return this.plugin;
	}
	
	@Deprecated
	public String generatePlugin(String publicKey) {  
		StringBuilder script = new StringBuilder();
		//replace this line with the full directory to the online store of the file
				
		script.append("<script type='text/javascript'  src='//api.dropifi.com/widget/dropifi_widget.min.js'></script>");
		script.append("<script type='text/javascript'>document.renderDropifiWidget('"+publicKey+"');</script>"); 
		 
		this.plugin = script.toString(); 		
		return this.plugin; 
	} 
	
	@Deprecated
	public String generateAsyncPlugin(String publicKey) { 
		//replace this line with the full directory to the online store of the file 
		this.plugin="<script type='text/javascript' id='dropifiwidget_script' data-widgetkey='"+publicKey+"'  src='//s3.amazonaws.com/dropifi/js/widget/dropifi_widget.min.js'></script>"; 	
		return this.plugin; 
	} 
	
	public String generateAsyncSnippet(String publicKey,String pluginType) { 
		return " <script>"+
					"var _dropifi = {'publicKey':'"+publicKey+"','pluginType':'"+pluginType+"'};"+
					"(function() {"+
					"   var s = document.getElementsByTagName('script')[0]"+
					"     , p = document.createElement('script');"+
					"    p.async = 'async';"+
					"    p.src = '//api.dropifi.com/widget/dropifi_widget.min.js';"+ 
					"    s.parentNode.insertBefore(p, s);"+
					"})();"+
					"</script> "
		 		; 
	}
	
	public String generateAsyncSnippetDev(String publicKey,String pluginType) { 
		return " <script>"+
					"var _dropifi = {'publicKey':'"+publicKey+"','pluginType':'"+pluginType+"'};"+
					"(function() {"+
					"   var s = document.getElementsByTagName('script')[0]"+
					"     , p = document.createElement('script');"+
					"    p.async = 'async';"+
					"    p.src = '//appengine.dropifi.com/public/clientmessenger/widget/dropifi_widget.min.js';"+
					"    s.parentNode.insertBefore(p, s);"+
					"})();"+
					"</script> " ; 
	}
	
	public String getPlugin(){
		return this.plugin;
	} 
	
	public static void updateImageText(Connection conn, String apikey,String buttonText,String buttonColor,String buttonFont,String buttonPosition){
		
		 try{
			 String domain = Company.findDomainByApikey(conn, apikey);
			 if(domain!=null){
				 
				TextToImage textToImage = new TextToImage("tab_text.png", buttonText, domain, buttonColor,buttonPosition);
				textToImage.setTextFont(buttonFont);
				String imageText = textToImage.transform();
				String textColor = Integer.toHexString(textToImage.getTextColor().getRGB()).replaceFirst("ff","#");
			 
				String[] tables = {"Widget","WidgetDefault"};
				
				for(String table : tables){
					PreparedStatement query = conn.prepareStatement("Update "+table+" w Set w.buttonTextColor=?, w.imageText=? Where w.apikey =?");
					query.setString(1, textColor);
					query.setString(2,imageText);
					query.setString(3,apikey);				
					query.executeUpdate();
				}
			 }
		 }catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(),e);
		 } 
	} 
	
	public static void updateImageText(Connection conn,String apikey, WiParameter param){
		updateImageText(conn, apikey, param.getButtonText(), param.getButtonColor(),param.getButtonFont(),param.getButtonPosition());
	}
	
	public void cancelAccount(){
		WidgetDefault defaultWidget = WidgetDefault.findByApiKey(this.getApikey());
		if(defaultWidget!=null){
			//delete the old value in cache
			CacheKey.deleteCache(CacheKey.getWidgetContentKey(defaultWidget.getPublicKey()));
			CacheKey.deleteCache(CacheKey.getWidgetTabKey(defaultWidget.getPublicKey()));
			defaultWidget.delete();	
			WidgetDownGrade dgrade = WidgetDownGrade.findByApikey(this.getApikey());
			if(dgrade!=null){
				dgrade.delete();
			}
		}
		this.delete();
	}
	//--------------------------------------------------------------------------------------------------------
	//Widget controls: 
	
 	public int addControl(WidgetControl control){
		
		if(!containsControl(control.getCondId())){ 
			 		
			if(this.widgetControls.add(control)){
				return this.validateAndSave() ? 200:201; 			
			}
			return 203;
		}
		return 302;  
	}
 		
	public int updateControl(WidgetField condId, WidgetControl newControl){
		WidgetControl control = findControlByCondId(condId);
		
		if(control != null){ 
			
			//check if the update control and the existing has the same title
			if(!control.getCondId().equals(newControl.getCondId())){
				//check for duplication of names
				if(containsControl(newControl.getCondId()) ){					
					return 302;
				}else{
					control.setTitle(newControl.getTitle());
				}				
			}else{
				control.setTitle(newControl.getTitle());
			}
			
			control.setType(newControl.getType());
			control.setPosition(newControl.getPosition());
			control.setActivated(newControl.getActivated());			 
			control.setRequired(newControl.getRequired());
			control.setValidator(newControl.getValidator()); 
			
			return this.validateAndSave() ? 200:201; 			 
		}
		return 501;	 
	}
	
	public int updateConActivated(WidgetField condId, boolean activated){ 		
		WidgetControl control = findControlByCondId(condId);
		
		if(control !=null){
			control.setActivated(activated);			
			this.save();
			
			WidgetDefault widgetDefault = WidgetDefault.findByApiKey(this.getApikey());			
			if(widgetDefault.updateWidgetDefault(this) !=null )
				return 200;
			return 360;
		} 		
		return 501;
	}
	
	public int updateConTitle(WidgetField condId, String title){ 		
		WidgetControl control = findControlByCondId(condId);
		
		if(control !=null){
			control.setTitle(title);			 
			this.save();
			return 200;
		} 		
		return 501;
	}
	
	public int  updateConActivated(String condId, boolean activated){ 
		WidgetField fieldId =null;
		
		//control id lookup
		if(condId.equals(WidgetField.attachment.toString()))
			fieldId = WidgetField.attachment;
		else if(condId.equals(WidgetField.email.toString()))
			fieldId = WidgetField.email;
		else if(condId.equals(WidgetField.message.toString()))
			fieldId = WidgetField.message;
		else if(condId.equals(WidgetField.name.toString()))
			fieldId = WidgetField.name;
		else if(condId.equals(WidgetField.phone.toString()))
			fieldId = WidgetField.phone;
		else if(condId.equals(WidgetField.subject.toString()))
			fieldId = WidgetField.subject;		
		
		return fieldId!=null? updateConActivated(fieldId, activated):506;
	}
	
	public int updateRespActivated(boolean activated){ 		 
		this.setResponseActivated(activated);
		this.save(); 
		return 200;
	}
	
	public int updateHasCaptcha(boolean activated){ 		 
		this.setHasCaptcha(activated);
		this.save(); 
		return 200;
	}
	
	public int updateWidgetActivated(boolean activated){
		this.wiParameter.setActivated(activated);
		this.save();
		
		WidgetDefault widgetDefault = WidgetDefault.findByApiKey(this.getApikey());	
		
		if(widgetDefault!=null)
			return widgetDefault.updateWidgetActivated(activated);
		
		return 501;
	}
	
	public int removeControl(WidgetField condId){
		WidgetControl control = findControlByCondId(condId);
		if(control !=null){
			if(this.widgetControls.remove(control)){		 
				this.save();
				return 200;
			}
			return 201;
		}
		return 501;
	}
		
	public WidgetControl findControlByCondId(WidgetField condId){
		if(condId != null){
			Iterator<WidgetControl> listControl = this.widgetControls.iterator();
			
			while(listControl.hasNext()){
				WidgetControl control = listControl.next();
				
				if(control.getCondId().equals(condId))
					return control;
			}
		}
		return null; 
	}
	
	public boolean containsControl(WidgetField condId){
		return findControlByCondId(condId)!= null;
	}
		
	public Collection<WidgetControl> getWidgetControls(){
		//TOBE implemented
		return this.widgetControls;
	}
		
	public List<ConSerializer> getSerializeControls(){
		
		List<ConSerializer> conSer = new ArrayList<ConSerializer>();
		Iterator<WidgetControl> controls = this.widgetControls.iterator();
		
		while(controls.hasNext()){
			conSer.add(new ConSerializer(controls.next()));
		}
		return conSer;
	}
		
	public void setWidgetControls(Collection<WidgetControl> widgetControls){
		this.widgetControls = widgetControls;
	}
	
	public static Widget findByApiKey(String apikey){
		return Widget.find("byApikey", apikey).first();  
	}
	
	public static Widget findByPublickey(String publicKey){
		return Widget.find("select w from Widget w, WidgetDefault d where (w.apikey = d.apikey) and (d.publicKey = ?)", publicKey).first();
	}
	
	public static Widget findByPlugin(String ecomName, EcomBlogType ecomType){
		return Widget.find("select w from Widget w, Company c where (w.apikey = c.apikey) and " +
				"(c.ecomBlogPlugin.eb_name = ?) and (c.ecomBlogPlugin.eb_pluginType=?)", ecomName, ecomType).first(); 
	}

	public static String findEmailByApikey(String apikey){
		return Widget.find("select w.defaultEmail from Widget w where w.apikey =? ", apikey).first();
	}
	
	public static Widget findByComIdEMail(Long companyId,String email){
		if(companyId != null && email !=null && !email.trim().isEmpty()){			
			return Widget.find("select w from Widget w where (w.defaultEmail != 'other') and" +
					" (w.companyId = ?) and w.defaultEmail = ?",companyId,email).first(); 
		}
		return null;
	}
	
	public static Widget findByCompanyId(Long companyId){
		if(companyId != null){			
			return Widget.find("select w from Widget w where (w.defaultEmail != 'other') and" +
					" (w.companyId = ?) ",companyId).first(); 
		}
		return null;
	}
	
	public static Boolean findActivated(String apikey) {
		Boolean act = Widget.find("Select w.wiParameter.activated From Widget w Where w.apikey =? ",apikey).first();
		if(act==null) act =true;
		
		return act;
	}
	
	public static Boolean findHasCaptcha(String publicKey) {
		return Widget.find("Select w.hasCaptcha from Widget w, WidgetDefault d Where w.apikey=d.apikey And d.publicKey=?",publicKey).first();  
	} 
	
	//--------------------------------------------------------------------------------------------------------
	//Using ElasticSearch to query the Widget Model
	public static  Widget searchByApiKey(String apikey){
		/*try{
			SearchResults<Widget> widgets = ElasticSearch.search(QueryBuilders.fieldQuery("apikey",apikey), Widget.class);
			if(widgets!=null && widgets.objects!=null && widgets.totalCount>0)
				return widgets.objects.get(0);
		}catch(Exception e){ play.Logger.log4j.info(e.getMessage(),e); }
		*/ 
		return findByApiKey(apikey);
	}
	
	//--------------------------------------------------------------------------------------------------------
	//getters and setters of instance variables
	
	public static String findMessageTemplate(Connection conn,String apikey){
		try {
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Select messageTemplate From Widget Where apikey=?");
				query.setString(1, apikey);
				
				ResultSet result = query.executeQuery();
				while(result.next()){
					return result.getString("messageTemplate");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public String getTitle() {
		return header;
	}
	
	public void setTitle(String title) {
		this.header = title;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String submitMessage) {
		this.defaultMessage = submitMessage;
	}

	public String getApikey(){
		return this.apikey;
	}

	public String getDefaultEmail() {
		return defaultEmail;
	}

	public void setDefaultEmail(String defaultEmail) { 
		if(!defaultEmail.contains(TictailStore.SEPARATOR))
			this.defaultEmail = defaultEmail;
		else
			this.defaultEmail = defaultEmail.split(TictailStore.SEPARATOR)[1];
	}

	public String getResponseSubject() {
		return responseSubject;
	}
	
	public void setResponseSubject(String responseSubject) {
		this.responseSubject = responseSubject;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Boolean getResponseActivated() {
		return responseActivated;
	}
	
	public void setResponseActivated(Boolean responseActivated) {
		this.responseActivated = responseActivated;
	} 

	public WiParameter getWiParameter(){
		return this.wiParameter;
	}

	public static WiParameter findByWidgetApikey(String apikey){
		return Widget.find("select w.wiParameter from Widget w where w.apikey = ?", apikey).first();
	}

	public void setWiParameter(WiParameter wiParameter) {
		this.wiParameter = wiParameter;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getSendButtonText() {
		return sendButtonText;
	}

	public void setSendButtonText(String sendButtonText) {
		this.sendButtonText = sendButtonText;
	}

	public String getSendButtonColor() {
		return sendButtonColor;
	}

	public void setSendButtonColor(String sendButtonColor) {
		this.sendButtonColor = sendButtonColor;
	}

	public String getResponseColor() {
		return responseColor;
	}

	public void setResponseColor(String responseColor) {
		this.responseColor = responseColor;
	}

	public String getMessageTemplate() {
		return messageTemplate;
	}

	public void setMessageTemplate(String messageTemplate) {
		this.messageTemplate = messageTemplate;
	}
	
	public String getHeader(){
		return header;
	}

	public Boolean getHasCaptcha() {
		return hasCaptcha;
	}

	public void setHasCaptcha(Boolean hasCaptcha) {
		this.hasCaptcha = hasCaptcha;
	}

	public String getCaptchaValue() {
		return captchaValue;
	}

	public void setCaptchaValue(String captchaValue) {
		this.captchaValue = captchaValue;
	}
	
}
