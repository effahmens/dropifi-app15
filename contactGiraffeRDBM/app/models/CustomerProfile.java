
package models;

import static akka.actor.Actors.actorOf; 

import java.sql.*; 
import java.util.*;
import java.util.Date;

import play.data.validation.*;
import play.data.validation.*;
import play.db.jpa.JPA;
import play.db.jpa.Model;
import play.db.jpa.Transactional;
import resources.APIKEY;
import resources.ContactSerializer;
import resources.DCON;
import resources.DropifiTools;
import resources.ModelManager;
import resources.SQLInjector;
import scala.Char;

import javax.annotation.PostConstruct;
import javax.persistence.*;

import job.process.ContactActor;
import models.failed.FullContactFailed;

import org.hibernate.annotations.Index;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import adminModels.SystemApikey;
import akka.actor.ActorRef;
import api.fullcontact.ContactInfo;
import api.fullcontact.Demographics;
import api.fullcontact.FullContactEntity;
import api.fullcontact.FullContactException;
import api.fullcontact.Organizations;
import api.fullcontact.Photos;
import api.fullcontact.SocialProfiles;


/**
 * Consist of the demographic ,social, geographical, and contact information of a company
 * contact  
 */

@NamedQueries({
	@NamedQuery(name="CustomerProfile.findWithoutSocial", 
			    query="SELECT DISTINCT c FROM CustomerProfile c WHERE c.id NOT IN "+
				"(SELECT DISTINCT s.customerProfile FROM  CustSocialProfile s " +
				"WHERE s.type IN ('facebook','twitter') ) ORDER BY c.id ") 
})

@Entity 
public class CustomerProfile extends Model {
	
	@Email	@Column(unique=true, updatable=false)
	private String email;
	private String fullName;	
	//demographic information of contact
	@Column(length= 10)
	private String gender;
	private Integer age;
	private String ageRange;
	@DateTimeFormat
	private Date birthDate;
	private String maritalStatus; 
	@Column(length=1000) 
	private String image; 
	@Column(length=1000)
	private String bio; 
	
	@ElementCollection
	@CollectionTable(name="CustPhoto")
	@MapKeyEnumerated(EnumType.STRING)
	@MapKeyColumn(name="type", length=100, nullable=false)
	@Column(name="url", nullable=false)
	private Map<String,String> photos; 
	
	private String locationGeneral; 
	
	 
	private String jobCompany;
	private String jobTitle;
	
	//household information of contact
	private String houseOwnerStatus;
	private String householdIncome; 
		
	//social profile of contact
	@OneToMany(mappedBy="customerProfile")
	private List<CustSocialProfile> socialProfile; 
	
	//organizational information of contact
	@OneToMany(mappedBy="customerProfile")
	private List<CustOrganization> custOrganization; 
	
	//------------------------------------------------------------------
	//temporal fields
		
	//------------------------------------------------------------------------------------
	//constructor
	
	public CustomerProfile(String email, String fullname,Integer age,String ageRange,
			Date birthDate,String maritalStatus,String locationGeneral,String householdIncome,
			String houseOwnerStatus){
		
		this.setEmail(email);
		this.setFullName(fullname);
		this.setAge(age);
		this.setAgeRange(ageRange);
		this.setBirthDate(birthDate);
		this.setMaritalStatus(maritalStatus);
		this.setLocationGeneral(locationGeneral);
		this.setHouseholdIncome(householdIncome);
		this.setHouseOwnerStatus(houseOwnerStatus);
	}
	
	public CustomerProfile(String email, String fullname) {		 
		this.setEmail(email);
		this.setFullName(fullname);
	}
	
	public CustomerProfile(){ 
		
	} 

	//---------------------------------------------------------------------------------
	//	
	public static CustomerProfile findByEmail(String email){
		return CustomerProfile.find("byEmail", email).first();
	}	
	
	//save customer profile
	public CustomerProfile saveProfile(long companyId){
		CustomerProfile profile = CustomerProfile.findByEmail(this.email);
		
		if(profile != null){ 
			profile.fullName = this.fullName;
			return profile.save(); 			
		}else{
			
			CustomerProfile prof = this.save(); 
			
			//add the full contact profile of a customer
			//Connection conn = DCON.getDefaultConnection();
			try {
				addFullProfile(prof.getId(), this.email,companyId); 
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return prof; 
		}		 
	}
	@Transactional
	public static int updateLocation(String location,String email){
		return CustomerProfile.em().createQuery("Update CustomerProfile c Set c.locationGeneral = :location Where c.email=:email")
		.setParameter("location", location).setParameter("email", email).executeUpdate(); 
	}
	
 
	public static int updateLocation(Connection conn,String location,String email){
		try {
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Update CustomerProfile c Set c.locationGeneral = ? Where c.email=?");
				query.setString(1, location);
				query.setString(2, email);
				return query.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return -1;
	}
	// 
	public static ResultSet findByEmail(Connection conn, String email) throws SQLException {
		
		if(conn!=null && !conn.isClosed()){
			PreparedStatement profile = conn.prepareStatement("SELECT * FROM CustomerProfile WHERE email = ?");
			profile.setString(1, email);
			profile.setMaxFieldSize(1); 
			ResultSet presult = profile.executeQuery();					 
			return presult; 
		} 		
		return null;		
	}
	
	
	
	public static ResultSet findIdByEmail(Connection conn, String email) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){ 
			PreparedStatement profile = conn.prepareStatement("SELECT c.id, c.fullName FROM CustomerProfile c WHERE c.email = ?");
			 profile.setString(1, email);
			 ResultSet presult = profile.executeQuery(); 			 
			 return presult;
		}
		return null;		
	}
	
	public static CustomerProfile getCustomerProfile(Connection conn,String email) throws SQLException {
		
		if(conn!=null && !conn.isClosed()){
			//search for the
			ResultSet result = findByEmail(conn, email);
			
			if(result != null){
				
				while(result.next()){  
					
					CustomerProfile profile = new CustomerProfile();
					//retrieve the profile information of the customer
					profile.id = result.getLong("id");
					profile.setEmail(result.getString("email"));
					profile.setFullName(result.getString("fullName"));
					profile.setGender(result.getString("gender"));
					profile.setAge(result.getInt("age"));
					profile.setAgeRange(result.getString("ageRange"));
					profile.setBirthDate(result.getDate("birthDate"));
					profile.setMaritalStatus(result.getString("maritalStatus")); 
					profile.setLocationGeneral(result.getString("locationGeneral"));			
					profile.setHouseholdIncome(result.getString("householdIncome"));
					profile.setHouseOwnerStatus(result.getString("houseOwnerStatus"));					
					profile.setBio(result.getString("bio"));					
					profile.setJobCompany(result.getString("jobCompany")); 
					profile.setJobTitle(result.getString("jobTitle"));
					profile.setImage(result.getString("image"));
					
					return profile;
				}
			}			
		}
		return null;
	}
	
	public static void updateFullName(Connection conn,String email, String fullName) throws SQLException{
		PreparedStatement updateState = conn.prepareStatement("update CustomerProfile SET fullName=?  where email=?");
		updateState.setString(1,fullName );
		updateState.setString(2,email );
		updateState.executeUpdate(); 
	}
	
	//update the contact info of a customer
	public void updateProfile(Connection conn,String fullname,String age,String ageRange,String gender,
		String maritalStatus,String locationGeneral,String householdIncome,
		String houseOwnerStatus,String image, String bio,String jobCompany, String jobTitle) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){
			if(this!= null && this.getId() !=null){
				
				//create an update statement
				PreparedStatement updateState = conn.prepareStatement("update CustomerProfile SET fullName=?, age=?, ageRange=?, gender=?," +
						"maritalStatus=?, locationGeneral=?, householdIncome=?,houseOwnerStatus=?, image=?, bio=?, jobCompany=?,jobTitle=? where email=?");
				
				 
				int i = 1;			
				Integer ageValue = setProfileAge(age, ageRange);
				fullname =  (fullname == null || fullname.equals("") )? this.getFullName():fullname;
				fullname = DropifiTools.convertEmailToName(this.getEmail(), fullname);
				updateState.setString(i++, fullname);
				updateState.setInt(i++, ageValue); 
				
				//set the age range
				String rangeValue = setAgeRange(ageValue, ageRange); 
				
				updateState.setString(i++, SQLInjector.escape( (rangeValue) )) ;
				updateState.setString(i++, SQLInjector.escape( ((gender ==null) 			|| (gender.equals("")) ? this.getGender():gender.toLowerCase()) ));			 
				updateState.setString(i++, SQLInjector.escape( ((maritalStatus ==null) 		|| (maritalStatus.equals("")) ? this.getMaritalStatus():maritalStatus) ));			
				updateState.setString(i++, SQLInjector.escape( ((locationGeneral ==null ) 	|| (locationGeneral.equals("")) ? this.getLocationGeneral():locationGeneral) ));
				updateState.setString(i++, SQLInjector.escape( ((householdIncome ==null) 	|| (householdIncome.equals("")) ? this.getHouseholdIncome():householdIncome) ));			
				updateState.setString(i++, SQLInjector.escape( ((houseOwnerStatus ==null) 	|| (houseOwnerStatus.equals(""))? this.getHouseOwnerStatus():houseOwnerStatus) ));
	            updateState.setString(i++, SQLInjector.escape( ((image==null) 				|| (image.equals(""))? this.getImage():image)));
	            updateState.setString(i++, SQLInjector.escape( ((bio==null) 				|| (bio.equals("")) ? this.getBio() : bio))); 
	            
	            updateState.setString(i++, SQLInjector.escape( ((jobCompany==null) 			|| 	(jobCompany.equals("")) ? this.getJobCompany(): jobCompany))); 
	            updateState.setString(i++, SQLInjector.escape( ((jobTitle==null)			||	(jobTitle.equals("")) ? this.getJobTitle() : jobTitle))); 
	            
				updateState.setString(i++, this.getEmail() );
				
				//execute the update query
				updateState.executeUpdate();
				 
				if(fullname !=null && !fullname.trim().isEmpty() && this.getEmail()!=null && !this.getEmail().isEmpty()){
					Customer.updateFullName(conn, this.getEmail(), fullname);
				} 
			}
			
		}
							
	}
	
	public String setAgeRange(Integer age,String range){
		if(age!=null){
			if(age <19){ 		 
				return "0 - 18";
			}else if(age<26){	 
				return "19 - 25";
			}else if(age<41){	 
				return "26 - 40";
			}else{
				return "45 - Above";
			}			
		}
		return "N/A"; 
	}
	
	public Integer setProfileAge(String age,String range){
		try{
			if(age!=null && !age.trim().equalsIgnoreCase("0")){
				 return Integer.parseInt(age);
			}else if (range!=null && !range.trim().isEmpty()){
				if(range.contains("-")){
					String[] ran = range.split("-"); 				
					
					int sum = 0;
					for(int i=0;i<ran.length;i++){
						sum += Integer.parseInt(ran[i]);
					} 	
					int value = sum/ran.length;
					return value>0?value:null;
				}			 
			}
		}catch(Exception e){
			
		}	
		return this.getAge()!=null?this.getAge():null;
	}
	
	public int addSocialProfile(Connection conn,String email,Long profileId, String type, String url,String socialId, String username, 
			int connections,String currentStatus,String bio,Integer following,Integer followers,Date birthDate) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){
			//check if a socialProfile with the same type is created		
			CustSocialProfile social = CustSocialProfile.findByCusProfileId(conn, this.getId(), type);
			
			if(social != null && social.getId() !=null){
				
				//update the socialProfile
				return social.updateSocialProfile(conn, url, socialId, username, connections, currentStatus, bio, following, followers,birthDate);
			
			}else{
				
				//create a new socialProfile for customer
				CustSocialProfile soci = new CustSocialProfile(email,profileId,type, url, socialId,username, connections, currentStatus, bio, following, followers,birthDate);
				return soci.insertSocialProfile(conn);
			}	
		}
		return -1;
	}
	
	public int addOrganization(Connection conn,CustOrganization organ) throws SQLException{
		//check if an organization with the same name is created		
		CustOrganization  org = CustOrganization.findOrganByEmail(conn,this.getEmail(), organ.getName()); 		
		if(org != null){
			return org.updateOrgan(conn,organ);
		}else{
			organ.setProfileId(this.getId());
			return organ.insertOrgan(conn);
		}		 
	}
	
	
	//------------------------------------------------------------------------------------------------------
	//add, update, select profile photos
	
	/**
	 * Add a new type of photo url to customer if it does not exist or
	 * Update an existing photo url
	 * @param conn
	 * @param type
	 * @param url
	 * @return
	 * @throws SQLException
	 */
	public int savePhoto(Connection conn,String type,String url) throws SQLException{ 	
		
		if(conn!=null && !conn.isClosed() && type !=null && url !=null){
			
			//search for the photo
			ResultSet result = findByPhotoType(conn, this.getId(), type);
			
			if(result.next()){
				//update photo info
				return updatePhoto(conn, type, url);
			}else{
				//insert photo info
				return insertPhoto(conn, type, url);
			}
		}
		return -1;
	} 
	
	private int insertPhoto(Connection conn,String type,String url) throws SQLException{ 	
		if(conn!=null && !conn.isClosed()){
			PreparedStatement insertPhoto = conn.prepareStatement("INSERT INTO CustPhoto (CustomerProfile_id, type,url)" +
					"VALUE(?,?,?)");
			
			int i=1;
			insertPhoto.setLong(i++, this.getId());
			insertPhoto.setString(i++, SQLInjector.escape(type));
			insertPhoto.setString(i++,SQLInjector.escape(url));
			
			return insertPhoto.executeUpdate();
		}
		return -1;
	}
	
	private int updatePhoto(Connection conn,String type,String url) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			PreparedStatement updatePhoto = conn.prepareStatement("UPDATE CustPhoto SET url=? WHERE CustomerProfile_id=? AND type=?");
			
			int i=1; 		
			updatePhoto.setString(i++, SQLInjector.escape(url));
			updatePhoto.setLong(i++, this.getId());
			updatePhoto.setString(i++, SQLInjector.escape(type));
					
			return updatePhoto.executeUpdate();
		}
		return -1;
	}
	
	public static ResultSet findByPhotoType(Connection conn,Long customerProfilId,String type) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			PreparedStatement selectPhoto = conn.prepareStatement("SELECT * FROM CustPhoto Where CustomerProfile_id=? and type=?");
			
			int i=1;
			selectPhoto.setLong(i++, customerProfilId);
			selectPhoto.setString(i++, SQLInjector.escape(type));
			selectPhoto.setMaxRows(1);
			ResultSet result = selectPhoto.executeQuery(); 
			return result;
		}
		return null;
	}
	
	
	//-------------------------------------------------------------------------------------------------------
	public static void fetchUpdateContactProfile(Connection conn, ContactSerializer contact,Long companyId) throws SQLException{
		fetchUpdateContactProfile(conn, contact, companyId,null);
	}
	/**
	 * search for the full contact profile of customer using fullContact API	
	 * @param customer
	 * @throws SQLException 
	 */
	
	public static void fetchUpdateContactProfile(Connection conn, ContactSerializer contact,Long companyId,String location) throws SQLException{
		 
		CustomerProfile customer = CustomerProfile.getCustomerProfile(conn, contact.getEmail());
		
		FullContactEntity entity = contact.getEntity();
		
		String fullname=null, ageRange=null, gender=null, locationGeneral = null, maritalStatus=null, 
				houseOwnerStatus=null, householdIncome=null,bio=null,jobCompany=null,jobTitle=null, image =null;
		Integer age = null;	 		 
		
		if(entity != null){
		 
			//----------------------------------------------------------------------------------------------------
			//Getting list of Photos
			
			List<Photos> photos = entity.getPhotos();  			
								
			if(photos != null) { 
				
				ListIterator<Photos> photoList = photos.listIterator();
				
				boolean hasPhoto = false;
				while(photoList.hasNext()){
					Photos photo = photoList.next();
				
					if(photo != null){
						try{
							 						
							if(photo.getIsPrimary() && photo.getPhotoUrl()!=null &&  !photo.getPhotoUrl().isEmpty()){
								image = photo.getPhotoUrl();
								hasPhoto = true;
							}
							
							if(hasPhoto == false && photo!=null && photo.getPhotoUrl()!=null &&  !photo.getPhotoUrl().isEmpty())
								image = photo.getPhotoUrl();
						
							//save the profile photo of contact
							customer.savePhoto(conn, photo.getPhotoType(), photo.getPhotoUrl());
							play.Logger.log4j.info("Photo added"); 
							
						}catch(Exception e){
							e.printStackTrace();
							play.Logger.log4j.info("Error Photo: " + e.getMessage());
						}
					}
				}
			}
			
			//----------------------------------------------------------------------------------------------------
			//Getting list of Social profiles
			List<SocialProfiles> socials = entity.getSocialProfiles();
				
			if(socials != null){
				boolean hasLinkedIn=false;
				boolean hasTwitter = false;
				boolean hasPlus = false;
				
				for (SocialProfiles social : socials) { 
					play.Logger.log4j.info("Social info: "+social.getTypeId());
					//save the social profile of the contact
					try{	
						 //get the profile birth date
						String bd = social.getProfileBday();
						Date biday = (bd == null ? null: new Date(Long.parseLong(bd)));
					 				
						 customer.addSocialProfile(conn,customer.getEmail(), customer.getId(),social.getTypeId(),social.getProfileUrl(),social.getProfileId(),
						   social.getProfileUsername(),social.getConnections(),social.getCurrentStatus(),
						   social.getBio(), social.getFollowing(), social.getFollowers(),biday);
						 
						 if(social.getTypeId() !=null  && social.getBio() !=null && !social.getBio().trim().isEmpty()){
							 
							 if(social.getTypeId().equalsIgnoreCase("linkedin") ){ 
								 bio = social.getBio(); 
								 hasLinkedIn = true;
								 continue;
							 }
							  
							 if(hasLinkedIn == false && social.getTypeId().equalsIgnoreCase("twitter") ){
								 bio = social.getBio(); 
								 hasTwitter = true;
								 continue;
							 }
							 
							 if(hasLinkedIn == false && hasTwitter == false  && social.getTypeId().equalsIgnoreCase("googleplus") ){
								 bio = social.getBio(); 
								 hasPlus = true;
								 continue;
							 }
							 
							 if(hasLinkedIn == false && hasTwitter == false && hasPlus==false )
								 bio = social.getBio(); 
							 
							play.Logger.log4j.info("Bio: "+bio);
						 }
					}catch(Exception e){
						 
						play.Logger.log4j.info(e.getMessage(),e);
					}
				}
			}
						
			//----------------------------------------------------------------------------------------------------
			//Getting list of Organizations
			
			List<Organizations> organizations = entity.getOrganizations();		
			if(organizations !=null) {
				 boolean hasOrgan = false;
				for (Organizations org : organizations) { 	
					try{
						//TOBE implemented
						Timestamp date = null;
						if(org!= null && org.getStartDate() !=null){
							DateTime d = DateTime.parse(org.getStartDate());								
						 	date  = new Timestamp(d.toDate().getTime()); 
						}
						
						play.Logger.log4j.info("ProfileId: "+customer.getId());
						
						CustOrganization organ = new CustOrganization(customer.getId(),customer.getEmail(), org.getName(), org.getTitle(), date, org.isPrimary());
						organ.saveOrganization(conn);
						
						//set the default job title of customer and only if isPrimary is true
						if(org !=null && !org.getName().trim().isEmpty()){
							if(org.isPrimary()){
								jobCompany = org.getName();
								jobTitle= org.getTitle();
								hasOrgan = true;
								continue;
							}
							
							if(hasOrgan == false){
								jobCompany = org.getName();
								jobTitle= org.getTitle();
							}
						}
					}catch(Exception e){ 
						play.Logger.log4j.info("Error organization: " + org.getName()+" "+ e.getMessage(),e); 
					}
					
				}
			}
			
			play.Logger.log4j.info("End ofContact Actor..........................................");

			//----------------------------------------------------------------------------------------------------
			//Getting Contact Info
			ContactInfo contactInfo = entity.getContactInfo();	

			//Getting Demographics Info
			Demographics demographics = entity.getDemographics();
			
			if(contactInfo != null && demographics !=null){
				try{ 					 
					 
					fullname = contactInfo.getFullName();					
					String pAge = demographics.getAge();
					//age =  pAge == null?null:Integer.parseInt(pAge);
					gender = demographics.getGender();
					ageRange = demographics.getAgeRange();				 
					locationGeneral =(location==null || location.isEmpty()? demographics.getLocationGeneral():location);
					maritalStatus = demographics.getMaritalStatus();
					houseOwnerStatus =demographics.getHomeOwnerStatus();
					householdIncome = demographics.getHouseholdIncome(); 							
					
					//save the demographic information of the contact
					customer.updateProfile(conn,fullname, pAge, ageRange, gender,maritalStatus,locationGeneral, 
							householdIncome, houseOwnerStatus,image, bio,jobCompany,jobTitle);  
					
					//update the company's customer fullname
					/*if(fullname !=null && !fullname.trim().isEmpty()){
						if(companyId!=null){ 
							Customer.updateFullName(conn, companyId, contact.getEmail(), fullname);
						}else{//all companies
							Customer.updateFullName(conn, contact.getEmail(), fullname);
						}
					}
					*/
				}catch(Exception e) {					 
					play.Logger.log4j.info("Contact Info: " + e.getMessage(),e);  
				}
			}

			//end of contact update
		}
	} 
	public static void addFullProfile(Long profileId, String email,Long companyId) throws SQLException{
		 addFullProfile(profileId, email, companyId,null);
	}
	public static void addFullProfile(Long profileId, String email,Long companyId,String location) throws SQLException {
		
		if(email !=null && !email.trim().isEmpty()){
			Connection conn = DCON.getDefaultConnection();
			if(conn!=null && !conn.isClosed()) {
				try {
					
					//set full contact APIKEY
					APIKEY.setFullContactApikey(DropifiTools.FullcontactApiKey);
					
					//establish a connection to the fullContact API and make a request to pull the contact information
					ContactSerializer contact = new ContactSerializer(APIKEY.fullContact,profileId, email);
 	
					//process the response to retrieve the detail information of the customer
					contact.pullContactEntity();						
					 
					int status = contact.getEntity().getStatusCode();
					
					play.Logger.log4j.info("FullContact Status: ------ "+status);
					if(status==200){ 
						
						//first implementation: the pulling of contact profile is assigned to an akka actor
						//send the result as AKKA message (AKKA actor will handle the saving to the database)
						
						/*
							ActorRef contactActor = actorOf(ContactActor.class).start();	
							contactActor.tell(contact);  //contact is a serialize object
						*/
						
						//second implementation: the current thread handles the pulling of contact profile
						//this is to ensure that the system complete pulling the info before other processes are continued
						
						try{
							fetchUpdateContactProfile(conn, contact,companyId,location); 
						}catch(Exception e){
							play.Logger.log4j.info(e.getMessage(),e);
						} 
						
					} else { 	
						
						//send a full contact failure report
						
						/*String msg =""; 
						
						if(status==202){ 							 
							msg = "202"; //- currently being processed Your request is currently being processed.
										 // You can check again later to see the request has been processed.";							 
					    }else if(status == 403 || status == 404 || status == 410){						    	
					    	msg = "Your API key is invalid, missing, or has exceeded its quota.";
					    }
					     							
						*/
						
						String msg = String.valueOf(status); 
						
						try {							 
							FullContactFailed failure = new FullContactFailed(email, status, msg); 									
							failure.insert(conn);							
						} catch (SQLException e){
							//TODO Auto-generated catch block
							play.Logger.log4j.info("FullContact failure insert : ------ " + e.getMessage());
						}						
					}
					
				}catch (NullPointerException | FullContactException  e) {
					// TODO Auto-generated catch block							
					try {  
						//send a full contact failure report
						
						FullContactFailed failure = new FullContactFailed(email, 403, e.getMessage()); 	
						failure.insert(conn);
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block 	
						play.Logger.log4j.info("FullContact failure insert : ------ "+e1.getMessage());
					}					
				}finally{
					try{
						conn.close();
					}catch(Exception ee){
						
					}
				}
			}
		}
	}
	
	/**
	 * search for all customers without facebook, twitter or linkedIn profile 		 
	 * @return
	 */	
	public static List<CustomerProfile> findAllWithoutSocialId(){ 		
		
		//search for all customers without facebook, twitter or linkedIn profile 			
		//test
		EntityManager em = CustomerProfile.em(); 
		return em.createNativeQuery("CustomerProfile.findWithoutSocial", CustomerProfile.class).getResultList();		 
	}
	
	//------------------------------------------------------------------------------------------------------
	//getters and setters
		
 	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = SQLInjector.escape(email);
	}
	
	public String getFullName() {
		return fullName;
	}
	
    public void setFullName(String fullName) {
		this.fullName = SQLInjector.escape(fullName);
	}
	
    public String getGender() {
		return gender;
	}
	
    public void setGender(String gender) {
		this.gender = SQLInjector.escape(gender);
	}

	public Integer getAge() {
		return age;
	}
	
	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAgeRange() {
		return ageRange;
	}
	
	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Map<String, String> getPhotos() {
		return photos;
	}

	public void setPhotos(Map<String, String> photos) {
		this.photos = photos;
	}

	public String getLocationGeneral() {
		return locationGeneral;
	}
	
	public void setLocationGeneral(String locationGeneral) {
		this.locationGeneral = SQLInjector.escape(locationGeneral);
	}

	public String getHouseOwnerStatus() {
		return houseOwnerStatus;
	}
	
	public void setHouseOwnerStatus(String houseOwnerStatus) {
		this.houseOwnerStatus = SQLInjector.escape(houseOwnerStatus);
	}

	public String getHouseholdIncome() {
		return householdIncome;
	}
	
	public void setHouseholdIncome(String householdIncome) {
		this.householdIncome = SQLInjector.escape(householdIncome);
	}

	public List<CustSocialProfile> getSocialProfile() {
		return socialProfile;
	}
	
	public List<CustOrganization> getOrganizations() {
		return custOrganization;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}
	
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = SQLInjector.escape(maritalStatus);
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getJobCompany() {
		return jobCompany;
	}

	public void setJobCompany(String jobCompany) {
		this.jobCompany = jobCompany;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	
}
