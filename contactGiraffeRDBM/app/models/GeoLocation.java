package models;

import java.io.Serializable;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Embeddable; 

import play.mvc.Before; 

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.maxmind.geoip2.WebServiceClient;
import com.maxmind.geoip2.model.CityResponse;

import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;

@Embeddable
public class GeoLocation implements Serializable {
	
	//@SerializedName("city")
	private String city;
	//@SerializedName("region_code")
	private String region;
	//@SerializedName("region")
	private String regionName;
	//@SerializedName("zipcode")
	private String postalCode;
	//@SerializedName("country_code")
	private String countryCode;
	//@SerializedName("country")
	private String countryName;
	//@SerializedName("latitude")
	private double latitude;
	//@SerializedName("longitude")
	private double longitude;
	//@SerializedName("ip")
	private String ipAddress;
	private Timestamp created;
	 
	public GeoLocation(String city, String region, String regionName,
			String postalCode, String countryCode, String countryName,
			double latitude, double longitude, String ipAddress) {
		super();
		this.city = city;
		this.region = region;
		this.regionName = regionName;
		this.postalCode = postalCode;
		this.countryCode = countryCode;
		this.countryName = countryName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.ipAddress = ipAddress;
	}
	public GeoLocation(String ipAddress){this.ipAddress = ipAddress;}
	
	@Override
	public String toString() {
		return "GeoLocation [city=" + city + ", region=" + region
				+ ", regionName=" + regionName + ", postalCode=" + postalCode
				+ ", countryCode=" + countryCode + ", countryName="
				+ countryName + ", latitude=" + latitude + ", longitude="
				+ longitude + ", ipAddress=" + ipAddress + "]";
	}
	
	@Before
	private void beforeSave(){
		Date date = new Date(); 
		this.setCreated(new Timestamp(date.getTime()));
	}
	
	
	/**
	 * Use the ip address to search through a geoip database for the country, city, latitude and longitude
	 */
	public static  GeoLocation findLocationByIP(String ip){
		try{
			//String ENDPOINT ="http://freegeoip.net/json/"+ip;
			//String ENDPOINT ="http://api.db-ip.com/addrinfo"; 
			String ENDPOINT ="http://www.telize.com/geoip/"+ip;
			HttpResponse response = WS.url(ENDPOINT) 
					//.setParameter("addr", ip)
					//.setParameter("api_key", "2d2f95d7098eb22ef4a74bc4e581148954597ab9")
					.get(); 
			if(response!=null){
				return new Gson().fromJson(response.getJson().getAsJsonObject().toString(),GeoLocation.class);
			}	
		}catch(Exception e){ 
			play.Logger.log4j.info(e.getMessage(),e);
		}catch(Throwable e){
			play.Logger.log4j.info(e.getMessage(),e); 
		}
		return new GeoLocation(ip);
	}
	
	public static GeoLocation findByMaxmind(int userId,String license_key, String ip){
		try{
			
			WebServiceClient client = new WebServiceClient.Builder(userId, license_key).build();
			CityResponse response = client.city(InetAddress.getByName(ip));
			 
			if(response!=null){
				return new GeoLocation(
					response.getCity().getName(), 
					response.getMostSpecificSubdivision().getIsoCode(), 
					response.getMostSpecificSubdivision().getName(), 
					response.getPostal().getCode(), 
					response.getCountry().getIsoCode(), 
					response.getCountry().getName(), 
					response.getLocation().getLatitude(), 
					response.getLocation().getLongitude(), 
					ip
				); 
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
		return new GeoLocation(ip); 
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getCityCountry(){	
		if(this.getCity()!=null && !this.getCity().isEmpty()){
			return this.getCity()+" , "+this.getCountryName();
		}else{
			return this.getCountryName();
		}
	}
 
	public Timestamp getCreated() {
		return created;
	}
 
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	
	
	
}
