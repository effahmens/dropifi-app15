package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Index;

import com.google.gson.annotations.SerializedName;

import dsubscription.resources.com.AccountType;

import play.db.jpa.Model;

@Entity 
public class WidgetDownGrade extends Model{
	@Column(nullable=false, unique=true)
	private String apikey;
	private AccountType currentPlan;
	private WiParameter wiParameter;
	
	public WidgetDownGrade(String apikey, AccountType currentPlan,WiParameter wiParameter) {
		super();
		this.apikey = apikey;
		this.currentPlan = currentPlan;
		this.wiParameter = wiParameter;
	}
	
	public WidgetDownGrade(){}
	
	//======================================================
	//finders
	
	public WidgetDownGrade createUpate(){
		WidgetDownGrade found = WidgetDownGrade.findByApikey(this.getApikey());
		if(found==null){
			found = this.save();
		}else{
			found.setCurrentPlan(this.getCurrentPlan());
			found.setWiParameter(this.getWiParameter());
			found = found.save();
		}
		return found;
	}
	
	public static WidgetDownGrade findByApikey(String apikey){
		return WidgetDownGrade.find("Select w from WidgetDownGrade w Where w.apikey=?", apikey).first();
	}
	
	public static WidgetDownGrade findByApikey(Connection conn, String apikey){
		try { 
			
			if(conn!=null && conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Select w from WidgetDownGrade w Where w.apikey=?");
				query.setString(1, apikey);
				
				ResultSet result = query.executeQuery();
				
				while(result.next()){
					return getWDownGrade(result);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public static WidgetDownGrade getWDownGrade(ResultSet result) throws SQLException{

		WidgetDownGrade grade = new WidgetDownGrade();
		grade.id = result.getLong("id");
		grade.setApikey(result.getString("apikey"));
		grade.setCurrentPlan(AccountType.valueOf(result.getInt("currentPlan")));
		
		WiParameter param = new WiParameter();
		param.setButtonColor(result.getString("buttonColor"));
		param.setButtonTextColor(result.getString("buttonTextColor"));
		param.setButtonFont(result.getString("buttonFont"));
		param.setButtonPercent(result.getInt("buttonPercent"));
		param.setButtonPosition(result.getString("buttonPosition"));
		param.setButtonText(result.getString("buttonText"));
		param.setImageText(result.getString("imageText"));
		param.setNormalText(result.getString("normalText"));
 
		grade.setWiParameter(param); 
		return grade;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public AccountType getCurrentPlan() {
		return currentPlan;
	}

	public void setCurrentPlan(AccountType currentPlan) {
		this.currentPlan = currentPlan;
	}

	public WiParameter getWiParameter() {
		return wiParameter;
	}

	public void setWiParameter(WiParameter wiParameter) {
		this.wiParameter = wiParameter;
	}
	
	
}
