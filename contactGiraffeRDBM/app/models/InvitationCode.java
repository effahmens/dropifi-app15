package models;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import play.db.jpa.Model;

@Entity
public class InvitationCode extends Model {
	
	@Column(nullable = false)
	private Boolean activated;
	@Column(nullable = false, unique = true)
	private String code;	
	
	private String email;
	@Column(nullable = false)
	private Timestamp created;
	@Column(nullable = false)
	private Timestamp updated;
		
	public InvitationCode(boolean activated, String code) {
		this.setActivated(activated);
		this.setCode(code);
	} 	
	
	public InvitationCode(String email,boolean activated, String code) {
		this.setEmail(email);
		this.setActivated(activated);
		this.setCode(code);
	} 	
	
	public int saveInvitation(){
		if(this.getCode() !=null && !this.getCode().trim().isEmpty()){
			InvitationCode invite = InvitationCode.findByCode(this.getCode());
			if(invite == null || invite.getId()==null){
				 
				Timestamp time = new Timestamp(new Date().getTime());
				this.setCreated(time);
				this.setUpdated(time);				  
				InvitationCode inviteCode = this.save();
				if(inviteCode!=null && inviteCode.getCode().equalsIgnoreCase(this.getCode())){
					return 200; 
				}else{
					return 201; 
				}
			}
			return 305;
		}
		return 501;
	}
	
	public static InvitationCode findByEmail(String email, boolean activated){
		return InvitationCode.find("byEmailAndActivated", email,activated).first();	 
	}
	
	public static InvitationCode findByCode(String code, boolean activated){
		if(code!=null){
			code = code.trim();		 
			return InvitationCode.find("byCodeAndActivated", code,activated).first(); 	
		}
		return null;
	}
	
	public static InvitationCode findByCode(String code){
		if(code!=null){
			code = code.trim();
			return InvitationCode.find("byCode", code).first(); 
		}
			
		return null;
	}

	
	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}

	
	
	
}
