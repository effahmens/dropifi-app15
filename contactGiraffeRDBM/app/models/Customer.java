package models;

import static akka.actor.Actors.actorOf;

import models.failed.FullContactFailed;

import org.bson.types.ObjectId;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.IndexColumn;

import akka.actor.ActorRef;
import api.fullcontact.FullContactException;

import com.mysql.jdbc.Statement;

import play.data.validation.*;
import play.db.jpa.Model;
import resources.APIKEY;
import resources.ContactSerializer;
import resources.CustomerType;
import resources.DCON;
import resources.SQLInjector;

import javax.persistence.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import job.process.ContactActor;

@Entity 
public class Customer extends Model{	
	
	@Index(name="companyId")@Column(nullable=false)
	private Long companyId;
	
	@Index(name="email")@Required @Column(nullable=false)
	private String email;	
	@Index(name="fullname")
	private String fullname;
	private String phone; 
	
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private CustomerProfile profile;
	
	@Transient 
	private List<DropifiMessage>messages;  
	
	@Index(name="isActive")
	private boolean isActive;

	private Timestamp created;
	private Timestamp updated;
	
	public Customer(long company,String email,String fullname){	
		this.setEmail(email);
		this.setCompanyId(company);
		this.setProfile(email, fullname);
		this.setFullname(fullname);
		this.setEmail(email); 
	}
		
	//add customer to company
 	public Customer saveCustomer()throws NullPointerException{ 
		//create a profile for the customer
		CustomerProfile pro = this.profile.saveProfile(this.companyId); 
				
		Customer customer = findByEmail(this.companyId, this.email);
		if(customer != null){

			customer.setProfile(pro);
			customer.email = this.email;
			return customer.save();
		}else{
			this.setProfile(pro);
			return this.save();
		}
	}
		
	public static ResultSet findByEmail(Connection conn,Long companyId, String email) throws SQLException{
		 
		if(conn!=null && !conn.isClosed()){
			 PreparedStatement custState = conn.prepareStatement("SELECT id, profile_id,fullname FROM Customer where email= ? AND companyId =?");
			 custState.setString(1, email);
			 custState.setLong(2, companyId);
			 ResultSet cresult = custState.executeQuery();		 
			 return cresult;  
		}
		return null;
	}
	
	public static Long saveCustomer(Connection conn,Long companyId,String email,String fullname,String phone) throws SQLException{
		return saveCustomer(conn, companyId, email, fullname, phone,null);
	}
	
	/**
	 * 
	 * @param conn
	 * @param companyId
	 * @param email
	 * @param fullname
	 * @return
	 * @throws SQLException
	 */
	public static Long saveCustomer(Connection conn,Long companyId,String email,String fullname,String phone,String location) throws SQLException {
		
		if(conn!=null && !conn.isClosed()){
			//email = SQLInjector.escape(email); 
			 			
			 //search for customer
			 ResultSet  cresult = findByEmail(conn, companyId, email);
			 
			 //if customer is found, return the customer id
			 if(cresult != null && cresult.next()==true ){ 	
				 
				 String cusName = cresult.getString("fullname");
				 //update the last contact date for the customer
				 if(fullname!=null && !fullname.isEmpty()){
					 updateFullName(conn,email,fullname);
					 CustomerProfile.updateFullName(conn, email, fullname);
				 }else if(cusName==null || cusName.trim().isEmpty()){
					 ResultSet presult = CustomerProfile.findIdByEmail(conn, email); 
					 while(presult.next()){
						 updateFullName(conn,email, presult.getString("fullName")); 
						 break;
					 } 
				 }
				 
				 if(phone!=null && !phone.isEmpty()){
					 updatePhone(conn,email,phone,companyId); 
				 }
				 
				 //update the contact profile if the basic info (social, demographic information) of the contact is not available
				 if(!CustSocialProfile.hasSocials(conn, email)){
					 long profileId = cresult.getLong("profile_id");
					 CustomerProfile.addFullProfile(profileId, email,companyId);
				 } 
				
				 return cresult.getLong("id");	
			 }else{ //if customer is not found, create a customer 				
				
				 //search for the profile of the customer by email
				 ResultSet presult = CustomerProfile.findIdByEmail(conn, email);
				 boolean pnext = (presult == null) ? false : presult.next(); 
				 
				 
				 if(pnext && (fullname == null || fullname.isEmpty()) )
					 fullname = presult.getString("fullName");
				  
				 //create a new customer and to be assigned to message
				 PreparedStatement customer = conn.prepareStatement("INSERT INTO Customer (email,fullname,phone,companyId,created,updated,isActive,profile_id) VALUE(?,?,?,?,?,?,?,?)");
				 int cus =1;
				 customer.setString(cus++,email);
				 customer.setString(cus++,fullname); 
				 customer.setString(cus++,phone); 
				 customer.setLong(cus++, companyId);
				 
				 //add the date the customer was created
				 Timestamp time = new Timestamp(new Date().getTime()); 
				 customer.setTimestamp(cus++, time);
				 customer.setTimestamp(cus++, time);
				 customer.setBoolean(cus++, false);
				 
				 				 
				 //if profile is found, set as profile of customer
				 if(pnext){					 
					 customer.setLong(cus++, presult.getLong("id"));
					 customer.executeUpdate();    
					 //presult.updateString("fullname", fullname)
					 CustomerProfile.updateFullName(conn, email, fullname);
				 }else{ //create a new profile
					
				     //create a new customer profile and save to database						
					 PreparedStatement cusprofile = conn.prepareStatement("INSERT INTO CustomerProfile (email,fullName,age,ageRange,gender,maritalStatus,locationGeneral,jobCompany,jobTitle)" +
					 		" VALUE(?,?,?,?,?,?,?,?,?)");
					 
					 int i=1;
					 cusprofile.setString(i++, email);
					 cusprofile.setString(i++, fullname);

					 cusprofile.setInt(i++, 0);
					 cusprofile.setString(i++, "N/A");
					 
					 cusprofile.setString(i++, "N/A");
					 cusprofile.setString(i++, "N/A");
					 cusprofile.setString(i++,(location==null||location.isEmpty()?"N/A":location));
					 
					 cusprofile.setString(i++,"N/A");
					 cusprofile.setString(i++, "N/A");
					 
					 cusprofile.executeUpdate();	
					 
					 //search for the new created profile and assign as profile of customer					 
					 ResultSet cpresult = CustomerProfile.findIdByEmail(conn,email); 					 
					 boolean next = (cpresult == null) ? false : cpresult.next(); 					 				
					 
					 if(next){		
						 Long  profileId = cpresult.getLong("id");					 
						 customer.setLong(cus++,profileId); 						 							
						 //search for the full contact profile of customer using fullContact API
						 
						//save the customer information
						customer.executeUpdate(); 

						try{	
							 //add the demographic, social and photo detail to customer profile
							 //using full Contact API
							 CustomerProfile.addFullProfile(profileId, email,companyId,location);
							 
						 }catch(Exception err){
							  
						 } 
						 
					 }else{
						 customer.setLong(cus++, 0l); 	
						 customer.executeUpdate();  
					 }					  
				 } 	
				  				 
				 //search for the customer
				 cresult = findByEmail(conn, companyId, email);
				 
				 if(cresult.next())
					 return cresult.getLong("id");
				 
				 return null; 
			 }
			 
		}
		return null;
	} 
	
	public static int updateFullName(Connection conn, Long companyId,String email,String fullname) throws SQLException{
		 
		if(conn!=null && !conn.isClosed()){
			if(fullname!= null && !fullname.trim().isEmpty()){
				PreparedStatement query = conn.prepareStatement("Update Customer set updated =?, fullname =? where companyId=? and email=?");
				Timestamp time = new Timestamp(new Date().getTime());  
				query.setTimestamp(1, time);
				query.setString(2, fullname);			
				query.setLong(3, companyId);
				query.setString(4, email); 				
				return query.executeUpdate();  
			}
		}
		
		return -1;
	}
	
	public static int updateFullName(Connection conn,String email,String fullname) throws SQLException{
		 
		if(conn!=null && !conn.isClosed()) {
			PreparedStatement query = conn.prepareStatement("Update Customer set updated = ?, fullname = ? where email=?");
			Timestamp time = new Timestamp(new Date().getTime());  
			query.setTimestamp(1, time);
			query.setString(2, fullname); 			 
			query.setString(3, email); 
			 			
			return query.executeUpdate();
		}		
		return -1;
	}
	
	public static int updatePhone(Connection conn,String email,String phone,long companyId) throws SQLException{
		 
		if(conn!=null && !conn.isClosed()) {
			PreparedStatement query = conn.prepareStatement("Update Customer set updated = ?, phone = ? where email=? and companyId =?");
			Timestamp time = new Timestamp(new Date().getTime());  
			query.setTimestamp(1, time);
			query.setString(2, phone); 			 
			query.setString(3, email); 
			query.setLong(4, companyId); 
			 			
			return query.executeUpdate();
		}		
		return -1;
	}
		
	public static int updateIsCustomer(Long companyId,String email, boolean isActive){
		if(companyId!=null && email!=null){ 
			int num = Customer.em().createQuery("UPDATE Customer c SET c.isActive = ?1 WHERE " +
					 "c.companyId =?2 AND c.email=?3").setParameter(1, isActive).setParameter(2, companyId)
					 .setParameter(3, email).executeUpdate();  
			return num >0?200:201; 			
		}
		return 501;
	}
	
	public static int updateIsCustomer(Connection conn, Long companyId,String email, boolean isActive) throws SQLException{
		if(companyId!=null && email!=null && conn!=null && !conn.isClosed()){ 
			
			PreparedStatement query = conn.prepareStatement("UPDATE Customer c SET c.isActive = ? WHERE c.companyId = ? AND c.email= ?");
			query.setBoolean(1, isActive);
			query.setLong(2, companyId);
			query.setString(3, SQLInjector.escape(email));
			return query.executeUpdate()> 0 ? 200:201; 
		}
		return 501;
	}
	
	public static boolean isCustomer(String apikey,String email){
		Connection conn = null;
		try{
			conn = DCON.getDefaultConnection();
			if(conn != null && !conn.isClosed()){
				 PreparedStatement query = conn.prepareStatement("select u.isActive from Customer u, Company c where " +
				 		"u.companyId = c.id and c.apikey = ? and u.email = ?");
				 
				 query.setString(1, apikey);
				 query.setString(2, email);
				 ResultSet result = query.executeQuery();
				 
				 while(result.next()){
					 return result.getBoolean("isActive"); 
				 } 
				 
			}
		}catch(Exception err){
			play.Logger.log4j.info(err.getMessage(), err);
		} 
		return false;
	}
	

	//-----------------------------------------------------------------------------------------------------------------------
	//search
	public static List<String> getEmailNameList(long companyId){
		List<Object[]> resultList = Customer.find("Select c.email, c.fullname from Customer c where c.companyId = :companyId").setParameter("companyId", companyId).fetch();
		
		LinkedList<String> emailList = new LinkedList<String>();

		for(Object[] fields : resultList) {
			for(Object field : fields){
				if(field!=null){
					String f = (String)field;
					if(!f.isEmpty())
						emailList.add(f); 
				}
			}
		}

				 
		return emailList;
	}
	
	public static List<String> getEmailList(long companyId){
		return Customer.find("Select c.email from Customer c where c.companyId = :companyId").setParameter("companyId", companyId).fetch(); 
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------------
	//getters and setters
	
  	public void setProfile(CustomerProfile profile){
		this.profile = profile;
	}
		
    public void setProfile(String email,String fullname){
		this.profile = new CustomerProfile(email, fullname);	 
	}
    	
	public CustomerProfile getProfile(){
		return this.profile;
	}
 		
 	public static Customer findByEmail(Long companyId,String email){
		return Customer.find("byCompany_idAndemail", companyId,email).first();
	}
	
	public String getEmail() {
		return email;
	}
	
	private void setEmail(String email) {
		this.email = email;
	}
	
	public String getFullname() {
		return fullname;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
	public Company getCompany() {
		return Company.findById(this.companyId);
	}
	
	public Long getCompanyId(){
		return this.companyId;
	}
	
	private void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
		
	//--------------------------------------------------------------------
	//messages
	
	public List<DropifiMessage> getMessages(){
		//TOBE implemented
		return null;
	}
	
	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

		
}
