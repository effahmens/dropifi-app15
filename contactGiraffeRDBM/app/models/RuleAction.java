package models;

import java.io.Serializable;
import play.data.validation.*;
import play.db.jpa.Model;
import resources.SQLInjector;

import javax.persistence.*;

import com.google.gson.annotations.SerializedName;

@Embeddable
public class RuleAction  implements Serializable{

	@SerializedName("action")
	private String action;	 
	@SerializedName("expectation")
	private String expectation;
		
	public RuleAction(String action, String expectation){		
		this.setAction(action);
		this.setExpectation(expectation); 
	}
	
	public RuleAction(){}
 
	//-----------------------------------------------------------------------------------
	//Overridden methods
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((expectation == null) ? 0 : expectation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleAction other = (RuleAction) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (expectation == null) {
			if (other.expectation != null)
				return false;
		} else if (!expectation.equals(other.expectation))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RuleAction [action=" + action + ", expectation=" + expectation
				+ "]";
	}
	 
	public String getAction() {
		return action;
	}
	

	public void setAction(String action) {
		this.action =  action!=null?action:"";
	}
	

	public String getExpectation() {
		return expectation;
	}
	

	public void setExpectation(String expectation) {
		this.expectation =  expectation!=null?expectation:"" ;
	}
		
}
