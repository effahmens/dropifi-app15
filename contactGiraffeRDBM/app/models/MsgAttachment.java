package models;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import play.data.validation.*;
import play.db.jpa.Model;
import view_serializers.AttSerializer;
import resources.DCON;
import resources.SQLInjector;

import javax.persistence.*;

import com.google.gson.annotations.SerializedName;

@Embeddable
public class MsgAttachment implements Serializable{ 
	
	@SerializedName("msgId")
	private String msgId;
	@Embedded
	@SerializedName("document")
	private Document document;
	
	public MsgAttachment(String msgId, Document document){
		this.setMsgId(msgId);	
		this.setDocument(document);
	}
	
	public MsgAttachment(){
		
	} 
	
	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public Document getDocument() {
		return document;
	}
	
	public void setDocument(Document document) {
		this.document = document;
	}
	
	public int addAttachment(long messageId,Connection conn){
		//check if the connection to the database is not closed
		try{
			if(conn!=null && !conn.isClosed()){ 				
				PreparedStatement state = conn.prepareStatement("INSERT INTO attachments (msg_id, comment,fileName,fileType,msgId)  VALUES (?,?,?,?,?)");
				
				int i=1;
				
				state.setLong(i++, messageId);
				state.setString(i++,this.getDocument().getComment());
				state.setString(i++,SQLInjector.escape(this.getDocument().getFileName()));
				state.setString(i++,SQLInjector.escape(this.getDocument().getFileType()));
				state.setString(i++,SQLInjector.escape(this.getMsgId()));
				return state.executeUpdate();
			}
		}catch(Exception e){}
		return -1;
	}
	public static int addAttachment(AttSerializer att,Connection conn) throws SQLException{
		
		if(att !=null){ 
			//check if the connection to the database is not closed
			if(conn!=null && !conn.isClosed()){ 				
				PreparedStatement state = conn.prepareStatement("insert into attachments (msg_id, comment,fileName,fileType,msgId) " +
						"values (?,?,?,?,?)");
				
				int i=1;
				
				state.setLong(i++, att.id);
				state.setString(i++,att.comments);
				state.setString(i++,SQLInjector.escape(att.fileName));
				state.setString(i++,SQLInjector.escape(att.fileType));
				state.setString(i++,SQLInjector.escape(att.msgId));
				return state.executeUpdate(); 
			}
		}
		
		return -1;
	}
	
	public static int[] addAttachment(List<AttSerializer> listAtt,Connection conn) throws SQLException{
		
		if(listAtt !=null){
			//check if the connection to the database is not closed
			if(conn!=null && !conn.isClosed()){ 
				Statement query = conn.createStatement();
				
				for(AttSerializer att : listAtt){
					PreparedStatement state = conn.prepareStatement("Insert into attachments (msg_id, comment,fileName,fileType,msgId) values (?,?,?,?,?) ");
					
					int i=1; 
					
					state.setLong(i++, att.id);
					state.setString(i++,"");
					state.setString(i++,SQLInjector.escape(att.fileName));
					state.setString(i++,SQLInjector.escape(att.fileType));
					state.setString(i++,SQLInjector.escape(att.msgId));
					
					query.addBatch(state.toString());
				}				
				return query.executeBatch();
			}
		}
		
		return new int[0];
	}

}
