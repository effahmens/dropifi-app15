package models.failed;

import java.sql.*;

import javax.persistence.*;

import org.hibernate.annotations.IndexColumn;

import play.data.validation.Email;
import play.db.jpa.Model;
import resources.SQLInjector;

@Entity
public class FullContactFailed extends Model {
	
	@Email @IndexColumn(name="email")
	@Column(nullable = false)
	private String email;
	private Integer statusCode;
	@Column(length=500)
	private String errorMsg;
	private Timestamp createdDate;
	
	public FullContactFailed(String email, Integer statusCode, String errorMsg ) {
		 
		this.setEmail(email);
		this.setStatusCode(statusCode);
		this.setErrorMsg(errorMsg);
		this.setCreatedDate();
	}
	
	public FullContactFailed(){}
	
	//---------------------------------------------------------------------------------------------------------------------
	
	public int insert(Connection conn) throws SQLException{
		
		if(conn != null && !conn.isClosed()){
			
			//check if the failed report has been logged 
			FullContactFailed failed = FullContactFailed.findByEmail(conn, this.email);
			
			if(failed == null){ 
				
				//create an insert statement for the fail report
				PreparedStatement insertState = conn.prepareStatement("insert into FullContactFailed (email,statusCode,errorMsg,createdDate) " +
						"value(?,?,?,?)");
				
				int i=1;
				insertState.setString(i++, this.getEmail());
				insertState.setInt(i++, this.getStatusCode());
				insertState.setString(i++, this.getErrorMsg()); 	
				insertState.setTimestamp(i++, this.getCreatedDate());
				
				//execute insert query
				return insertState.executeUpdate();
			}
		}
		
		return -1;
	}
	
	public static FullContactFailed findByEmail(Connection conn,String email) throws SQLException{
		
		PreparedStatement select = conn.prepareStatement("select * from FullContactFailed where email = ? ");
		select.setString(1, email);
		
		ResultSet result = select.executeQuery();
		
		FullContactFailed confailed = null;
		while(result.next()){
			confailed = new FullContactFailed();
			confailed.id = result.getLong("id");
			confailed.setEmail(result.getString("email"));
			confailed.setErrorMsg(result.getString("errorMsg"));
			confailed.createdDate = result.getTimestamp("createdDate");			
			
		}
		
		return confailed;
	}
	
	//---------------------------------------------------------------------------------------------
	//getters and setters

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = SQLInjector.escape(errorMsg);
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	@PrePersist
	private void setCreatedDate() {		 
		this.createdDate = new Timestamp(new java.util.Date().getTime());
	}
		
}
