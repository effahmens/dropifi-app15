package models.failed;

import java.sql.*;

import javax.persistence.*;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.IndexColumn;

import play.db.jpa.Model;
import resources.SQLInjector;

/**
 * keeps list of all messages that the system failed in performing sentiment analysis on 
 * @author phillips
 *
 */

@Entity
public class SentimentFailed extends Model {

	@IndexColumn(name="msgId")
	@Column(nullable=false)
	private Long msgId;
	@Column(length=1000)
	private String content;
	@Column(length=500)
	private String errorMsg;
	private Timestamp createdDate;
		
	public SentimentFailed(Long msgId, String content,String errorMsg) {
		 
		this.setMsgId(msgId);
		this.setContent(content);
		this.setErrorMsg(errorMsg);
		this.setCreatedDate();
	}
	
	//add a sentiment failure report to database 
	public int insert(Connection conn) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){ 
			
			//create the insert statement
			PreparedStatement insertState = conn.prepareStatement("insert into SentimentFailed (msgId,content,errormsg,createddate)" +
					"value (?,?,?,?)");
			
			int i=1;
			insertState.setLong(i++, this.getMsgId());
			insertState.setString(i++, this.getContent());
			insertState.setString(i++, this.getErrorMsg());
			insertState.setTimestamp(i++, this.getCreatedDate());
			
			//execute insert query
			int fa = insertState.executeUpdate(); 	
			try{
				conn.close();
			 }catch(Exception err){ 
					
			 }
			return fa;
		}
		
		return -1;
	}
	
	//-----------------------------------------------------------------------------------------
	//getters and setters 
	
	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}
	
	public Long getMsgId(){
		return this.msgId;
	} 

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	
	@PrePersist
	private void setCreatedDate() {
		this.createdDate = new Timestamp(new java.util.Date().getTime());
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = SQLInjector.escape(errorMsg);
	}
		
}
