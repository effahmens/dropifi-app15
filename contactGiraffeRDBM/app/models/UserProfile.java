package models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;

import play.data.validation.Email;
import play.data.validation.Required;

import com.google.gson.annotations.SerializedName;

@Embeddable
public class UserProfile  implements Serializable{
	
	@SerializedName("email")
	@Column(nullable=false,unique=true) @Required @Email
	private String email;
	@SerializedName("username")
	private String username;
	
	@SerializedName("agentRoles")
	@ElementCollection(targetClass = AgentRole.class)
	@CollectionTable(name="agentRoles", joinColumns=@JoinColumn(name="accagent_id"))
	private Collection<AgentRole> agentRoles;
	
	@SerializedName("created")
	private Timestamp created;
	@SerializedName("updated")
	private Timestamp updated;
	@SerializedName("lastLogin")
	private Timestamp lastLogin;
	
	@Transient @SerializedName("roles")
	private String roles;
	
	@SerializedName("activated")
	@Index(name="activated") @Column(nullable=true)
	private Boolean activated;
	
	@SerializedName("priorityLevel")
	private int priorityLevel;
	@SerializedName("knownActivation")
	private Boolean knownActivation;
	
	@Transient @SerializedName("uid")
	public int uid; 
	
	public UserProfile(String email, String username,
			Collection<AgentRole> agentRoles, Timestamp created,
			Timestamp updated, Timestamp lastLogin) {

		this.setEmail(email);
		this.setUsername(username);
		this.setAgentRoles(agentRoles);
		this.setCreated(created);
		this.setUpdated(updated);
		this.setLastLogin(lastLogin); 
	}
	
	public UserProfile(String username, String email){
		this.setUsername(username);
		this.setEmail(email);
	}

	@Override
	public String toString(){ 
		return "UserProfile [email=" + email + ", username=" + username
				+ ", agentRoles=" + agentRoles + ", created=" + created
				+ ", updated=" + updated + ", lastLogin=" + lastLogin
				+ ", roles=" + roles + ", activated=" + activated + ", uid="
				+ uid + "]";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Collection<AgentRole> getAgentRoles() {
		return agentRoles;
	}

	public void setAgentRoles(Collection<AgentRole> agentRoles) {
		this.agentRoles = agentRoles;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public Timestamp getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin; 
	}
	
	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public Boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public int getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(int priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Boolean isKnownActivation() {
		return knownActivation;
	}

	public void setKnownActivaton(boolean knownActivation) {
		this.knownActivation = knownActivation;
	}
 
}
