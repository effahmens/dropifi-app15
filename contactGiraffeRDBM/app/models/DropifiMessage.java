package models;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Collection;
import java.util.Date;
import java.sql.*; 
import javax.persistence.*; 

import org.hibernate.annotations.Index;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject; 
import akka.actor.ActorRef;
import akka.actor.Actors;  
import play.data.validation.*;
import play.db.jpa.JPA;
import play.db.jpa.Model; 

import resources.*;
import view_serializers.AttSerializer;
import view_serializers.ClientSerializer;


@Entity 
public class DropifiMessage extends Model { 
	
	@Column(unique=true, updatable=false)
	private String msgId;
	
	//companyId	
	@Index(name="companyId") @Column(nullable=false)
	private Long companyId;
	 
	//information about the sender of the message	 
    @Index(name="email")
	private String email; 

    @Index(name="customerId")
	private Long customerId;
	
	//applicable to only messages sent from the widget
	private MsgInfo msgInfo;	 
	
	///describes the state of the message: New, Open, Pending or Closed	 
	private MsgStatus status;  	 
	
	@Column(length=10000)
	private String subject;
	
	@Column(length=100000)	
	private String textBody;
	
	@Column(length=100000)
    private String htmlBody;
		 
	private Timestamp receivedDate;	 
	private Timestamp sentDate;	
	private Timestamp created;
	private Timestamp updated;
	
	//key value fields which holds optional info on the widget. 
	//E.g. phone : 0234453, usability : easy
	@ElementCollection(fetch=FetchType.LAZY)		 
	@Column(name="msg_opt")
	private Map<String, String>optionals;
	
	@Transient
	private List<MsgKeyword>keywords;
	
	//describe whether the message was received from widget or through an inbound mailbox
	//create keys: sourceOfMsg (widget message or Inbound message) and sourceId(a unique id which identify the source); 
	@Index(name="sourceOfMsg")
	private String sourceOfMsg;
	@Index(name="sourceId")
	private Long sourceId;
		
	@ElementCollection(targetClass = MsgAttachment.class,fetch=FetchType.LAZY)
	@CollectionTable(name="attachments", joinColumns=@JoinColumn(name="msg_id"))
	private Collection<MsgAttachment> attachments;
	
	@ElementCollection 
	@Column(name="tag")
	private Collection<String> tags;
		
	@ElementCollection(targetClass=String.class, fetch=FetchType.LAZY)
	@Column(name="note")
	private Collection<String> notes;
	
	@Index(name="isArchived")
	private boolean isArchived; 
	
	//Temporal fields: not add to table as fields 
	
	@Transient
	private long inboundId;
	@Transient
	private int numOfMsg;
	@Transient
	private String fullname;  
		
	public DropifiMessage(String subject, String textBody){
		this.setSubject(subject);
		this.setTextBody(textBody);
	}
	
	public DropifiMessage(String email,MsgInfo msgInfo, MsgStatus status,String subject, String body,String htmlBody, 
			List<MsgKeyword> keywords,	Long SourceId,String sourceOfMsg) {	
		
		this.setEmail(email);
		this.setMsgInfo(msgInfo);
	    this.setStatus(status);
		this.setSubject(subject);
		this.setTextBody(body);
		this.setHtmlBody(htmlBody);
		this.setKeywords(keywords);
		this.setSourceId(SourceId);
		this.setSourceOfMsg(sourceOfMsg);
	}
		 
	public DropifiMessage(String email, MsgInfo msgInfo, MsgStatus status,String subject, String body,String htmlBody, List<MsgKeyword> keywords,	
			Long sourceId,String sourceOfMsg , Timestamp  receivedDate,Timestamp sentDate) { 	
		
		//this.setCompany((company));
		this.setEmail(email);			
		this.setStatus(status);
		this.setSubject(subject);
		this.setTextBody(body);		
		this.setHtmlBody(htmlBody);
		
		this.setSourceId(sourceId);
		this.setSourceOfMsg(sourceOfMsg);
		this.setReceivedDate(receivedDate);	
		
		this.setSentDate(sentDate); 	 
		this.setKeywords(keywords); 
		this.setMsgInfo(msgInfo);	 
	} 
	
	public DropifiMessage(long companyId){
		this(null,null,null,null,null,null,new LinkedList<MsgKeyword>(), 0l,"");
		this.setCompanyId(companyId);
	}
	
	public DropifiMessage(MsgSerializer msg){ 	
				
		this(msg.getEmail(),null,MsgStatus.New,msg.getSubject(),
			msg.getTextBody(), msg.getHtmlBody(),null, msg.getSourceId(),msg.getSourceOfMsg(), msg.getReceivedDate(), 
			msg.getSentDate()
		);
	
		this.setMsgId(msg.getMsgId());
		this.companyId= msg.getCompanyId();
		this.inboundId = msg.getInboundMb_id();
		this.numOfMsg = msg.getNumOfMsg();
		this.fullname = msg.getFullname();
		this.optionals = msg.getOptionals(); 
	}
	
	public DropifiMessage(MsgSerializer msg, boolean iswidget){		
		
		this(msg.getEmail(),null,MsgStatus.New,msg.getSubject(),
			msg.getTextBody(), msg.getHtmlBody(),null, msg.getSourceId(),msg.getSourceOfMsg(), msg.getReceivedDate(),
			msg.getSentDate()  
		);  
		
		this.setMsgId(msg.getMsgId()); 
		this.companyId= msg.getCompanyId();		 
		this.numOfMsg = msg.getNumOfMsg();
		this.fullname = msg.getFullname();
		this.optionals = msg.getOptionals(); 
	}
	
	//-----------------------------------------------------------------------------------------------------
	//saving,updating, deleting, searching and filtering messages
	
	/**
	 * Applicable to inbound messages
	 * @param conn
	 * @throws Exception
	 */
	public int createMessage(Connection conn) throws Exception { 
		
	 	if(conn != null && !conn.isClosed()){ 			 
	 		 
	 		//save the sender of the email as customer of the recipient company
	 		String phone = "";
	 		if(optionals!=null){
	 			try{
	 				phone = optionals.get("phone");
	 			}catch(Exception e){
	 				play.Logger.log4j.info(e.getMessage(),e);
	 			}
	 		}
	 		
			Long customer = Customer.saveCustomer(conn, companyId, this.getEmail(), this.fullname,phone); 		   
			this.setCustomerId(customer);
			
			 //create an insert statement for message
		    PreparedStatement msg  = conn.prepareStatement("insert into DropifiMessage " +
			 		"(msgId,companyId,customerId, email,subject,textBody,htmlBody,status,sourceId,sourceOfMsg," + 
			 		"receivedDate,sentDate,created,updated,isArchived)" +
			 		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");			
			 				 		 
			 //query parameters
		     int i = 1;
		     
		     //generate a key for the message
		     //this.msgId = this.genMsgId(customer);
		     msg.setString(i++, this.getMsgId());
			 msg.setLong(i++, this.companyId);
			 msg.setLong(i++, customer);
			 msg.setString(i++, SQLInjector.escape(this.getEmail()));	
			 msg.setString(i++, SQLInjector.escape(this.getSubject()));	
			 msg.setString(i++, SQLInjector.escape(this.getTextBody()));
			 msg.setString(i++, SQLInjector.escape(this.getHtmlBody()));			
			 msg.setInt(i++, this.getStatus().ordinal());
			 msg.setLong(i++, this.getSourceId());
			 msg.setString(i++, this.getSourceOfMsg());				
			 msg.setTimestamp(i++, this.getReceivedDate());
			 msg.setTimestamp(i++, this.getSentDate());
			 
			 //add the date and time the message was save in the database
			 Timestamp time = new Timestamp(new Date().getTime());
			 msg.setTimestamp(i++, time);
			 msg.setTimestamp(i++, time);
			 msg.setBoolean(i++, false);
			 
			 int status = msg.executeUpdate()>0?200:201;
			 Long messageId = DropifiMessage.getMId(conn,this.getMsgId()); 
			 saveUpdateOptions(conn, messageId);
			 return status;
	 	}else{
	 		play.Logger.log4j.info("no connection");
	 	}
	 	return 501;
	}
	
	/**
	 * Applicable to widget messages
	 * @param conn
	 * @throws Exception
	 */
	public int createWidgetMessage(Connection conn) throws Exception { 
	 
	 	if(conn != null && !conn.isClosed()){ 			 
	 		
	 		//save the sender of the email as customer of the recipient company
	 		String phone = "", location=null;
	 		if(optionals!=null){
	 			try{
	 				phone = optionals.get("phone");
	 			}catch(Exception e){
	 				play.Logger.log4j.info(e.getMessage(),e);
	 			}
	 			
	 			try{
	 				location = optionals.get("location");
	 			}catch(Exception e){
	 				play.Logger.log4j.info(e.getMessage(),e);
	 			}
	 		}
	 		
			Long customer = Customer.saveCustomer(conn, companyId, this.getEmail(), this.fullname, phone,location);		   
		    this.setCustomerId(customer);
			 //create an insert statement for message
		    PreparedStatement msg  = conn.prepareStatement("insert into DropifiMessage " + 
			 		"(msgId,companyId,customerId, email,subject,textBody,htmlBody,status,sourceId,sourceOfMsg," +
			 		"receivedDate,sentDate,created,updated,isArchived) " +
			 		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");   			
			 				 		 
			 //query parameters 
		     int i = 1;
		     
		     //generate a key for the message 
		     //this.msgId = this.genMsgId(customer);
		     msg.setString(i++,this.getMsgId());
			 msg.setLong(i++, this.getCompanyId());
			 msg.setLong(i++, customer);
			 msg.setString(i++, this.getEmail());	
			 msg.setString(i++, this.getSubject());	
			 msg.setString(i++,this.getTextBody());
			 msg.setString(i++,this.getHtmlBody());			
			 msg.setInt(i++, this.getStatus().ordinal());
			 msg.setLong(i++, this.getSourceId());
			 msg.setString(i++, this.getSourceOfMsg());				
			 msg.setTimestamp(i++, this.getReceivedDate());
			 msg.setTimestamp(i++, this.getSentDate()); 
			 
			 //add the date and time the message was save in the database
			 Timestamp time = new Timestamp(new Date().getTime());
			 msg.setTimestamp(i++, time);
			 msg.setTimestamp(i++, time);
			 msg.setBoolean(i++, false);
			 
			 //save the message to database
			 msg.executeUpdate(); 
			 Long messageId = DropifiMessage.getMId(conn,this.getMsgId()); 
			 saveUpdateOptions(conn, messageId);
			 return 200;  
	 	}
	 	return 501;
	}
	
	private void saveUpdateOptions(Connection conn, long messageId){
		if(this.getOptionals() != null && this.getOptionals().size()>0){ 
			
			try{ 
			 Set<String> keys = optionals.keySet();				 
			 Iterator<String> listKeys = keys.iterator();
			 //Statement state = conn.createStatement();
			 
			 while(listKeys.hasNext()){ 
				 //add the optional information on the widget to the message
				 PreparedStatement opt  = conn.prepareStatement("Insert into DropifiMessage_optionals (DropifiMessage_id,optionals_KEY,msg_opt) value (?,?,?) ");
				 
				 int o = 1;
				 String key = listKeys.next(); 
				 opt.setLong(o++, messageId);
				 opt.setString(o++,  key);
				 opt.setString(o++, optionals.get(key)); 
				 opt.executeUpdate();				 
			 }				 
			 //
			 //state.executeBatch();
			}catch(Exception e){
				play.Logger.log4j.info(e.getMessage(),e);
			}
		 }
	}
	
	
	/**
	 * Applicable to messages sent from the widget
	 */
	
	public void createMessage() {		
		//save the sender of the email as customer of the recipient company
		Customer customer = new Customer(this.getCompanyId(), this.getEmail(), this.fullname); 
		customer = customer.saveCustomer();
		if(customer!=null)
			this.setCustomerId(customer.getId());  
		
		  //generate a key for the message
	     this.msgId = this.genMsgId(customer.getId()); 
	      		 
		Timestamp time = new Timestamp(new Date().getTime());
		this.setCreated(time);
		this.setUpdated(time);
		
		this.save();
	}
	
	//generate a unique easy id
	private String genMsgId(Long customerId){
		Date date = new Date();
		String mId = this.companyId.toString()+customerId.toString()+"_"+ date.getTime();
		return String.valueOf(this.getEmail()+"_"+ Long.parseLong(mId));
	}
	
	public static String genMsgId(Long companyId, String email){
		Date date = new Date();
		String mId = String.valueOf(companyId +"_"+ date.getTime()); 
		return String.valueOf(email+"_"+mId);
	} 
	
	public static boolean containsInboundMailbox(Long companyId, String sourceOfMsg){
		return DropifiMessage.find("byCompanyIdAndSourceOfMsg", companyId,sourceOfMsg).first() !=null;		 
	}
	
	//search for the customer this message belongs
	public Customer findByEmail() {		
		if(this.getCompany() != null){
			return Customer.findByEmail(this.getCompany().getId(), this.email);
		}
		return null;
	} 
	
	public static JsonObject retrieveSubjectMessage(long id,String msgId,String email){
		List<Object[]> resultList = DropifiMessage.find("select d.subject, d.htmlBody, d.created from DropifiMessage d where (d.id =? ) and (d.email =?) and (d.msgId = ?) ", 
				id, email, msgId).query.setMaxResults(1).getResultList();
		
		if(resultList !=null){			
			for(Object[] fields : resultList){ 
				
				int i =0;
				JsonObject json = new JsonObject(); 				
				json.addProperty("subject", (String)(fields[i]!=null?fields[i++]:""));
				json.addProperty("message", (String)(fields[i]!=null?fields[i++]:""));	
				String dataTime = ((Timestamp) (fields[i]!=null?fields[i++]:0l) ).toString();
				json.addProperty("created", dataTime);
				return json; 
			} 
		}
		return null; 
	} 
	
	public static List<AttSerializer> retrieveAttachments(long id,String msgId,String sender){ 
		List<AttSerializer> fileAttachments = new LinkedList<AttSerializer>(); 
		Connection conn = DCON.getDefaultConnection();
		try{
			PreparedStatement attQuery = conn.prepareStatement("SELECT fileName, comment, fileType FROM attachments WHERE msg_id=? and msgId=?");
			attQuery.setLong(1, id);
			attQuery.setString(2, msgId); 
			ResultSet result = attQuery.executeQuery();
			while(result.next()){
				fileAttachments.add(new AttSerializer(id, "", sender, msgId, result.getString("fileName"),result.getString("comment")));
			}
			
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(),e);
			}
		}
		return fileAttachments;
	}
	
	public static Long getMId(Connection conn, String msgId) throws SQLException{
		//check if the msgId is not empty
		if(!msgId.trim().isEmpty()){		 
			if(conn!=null && !conn.isClosed()){
				 PreparedStatement dmid = conn.prepareStatement("select id from DropifiMessage WHERE msgId = ?");
				 dmid.setString(1, msgId);
				 dmid.setMaxRows(1); 
				 
				 ResultSet result = dmid.executeQuery();	
				 Long id = null;
				 
				 while(result.next()){				 
					id = result.getLong("id"); 
				 }
				 		 
				 return id;
			}
		}
		 return null;
	}
	 
	public static int updateStatus(Long id,String msgId, MsgStatus status){
		EntityManager em = JPA.em();		
		Timestamp updated = new Timestamp(new Date().getTime());
		int result = em.createQuery("UPDATE DropifiMessage d SET d.status = :status, d.updated = :updated where d.id = :id")
		.setParameter("status", status).setParameter("updated",updated).setParameter("id", id).executeUpdate();		
		return result>0?200:201; 
	}
	
	//do not modify
	public static int updateOpenStatus(Long id,String msgId){
		EntityManager em = JPA.em();
		Timestamp updated = new Timestamp(new Date().getTime());
		int result = em.createQuery("UPDATE DropifiMessage d SET d.status = :status, d.updated = :updated where d.id = :id and d.status = :newStatus")
		.setParameter("status", MsgStatus.Open).setParameter("id", id).setParameter("newStatus", MsgStatus.New).setParameter("updated",updated).executeUpdate(); 		
		return result>0?200:201;
	}
	
	//do not modify
	public static int updatePendingStatus(Long id,String msgId){
		EntityManager em = JPA.em();	
		Timestamp updated = new Timestamp(new Date().getTime());
		int result = em.createQuery("UPDATE DropifiMessage d SET d.status = :status, d.updated = :updated where d.id = :id and d.status = :openStatus")
		.setParameter("status", MsgStatus.Pending).setParameter("updated",updated).setParameter("id", id).setParameter("openStatus", MsgStatus.Open).executeUpdate(); 		
		return result>0?200:201; 
	}
	
	//do not modify
	public static int updateResolvedStatus(Long id,String msgId){
		EntityManager em = JPA.em();
		Timestamp updated = new Timestamp(new Date().getTime());
		int result = em.createQuery("UPDATE DropifiMessage d SET d.status = :status, d.updated = :updated where d.id = :id and (d.status = :openStatus or d.status= :pendStatus)")
		.setParameter("status", MsgStatus.Resolved).setParameter("updated",updated).setParameter("id", id).setParameter("openStatus", MsgStatus.Open).setParameter("pendStatus", MsgStatus.Pending)
		.executeUpdate(); 		
		return result>0?200:201; 
	}
	
	public static long getMessages(long companyId){
		
		try{
			return (long) JPA.em().createQuery("Select count(d.companyId) From DropifiMessage d Where d.companyId=:companyId and d.isArchived =:isArchived")
					.setParameter("companyId", companyId).setParameter("isArchived",false).getSingleResult();
		}catch(Exception e){ 
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return 0l;
	}
	
	public static long getMessages(Connection conn,long companyId){
		
		try{
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Select count(d.companyId) as msgCount From DropifiMessage d Where d.companyId=? and d.isArchived =?");
				query.setLong(1, companyId);
				query.setBoolean(2, false);
				 
				ResultSet result = query.executeQuery();
				while(result.next()){
					return result.getLong("msgCount");
				}
			}
		}catch(Exception e){ 
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return 0l;
	}
	
   public static long getWidgetMessages(Connection conn,long companyId){
	   return getSourceMessages(conn,companyId,"=");
   }
   
   public static long getMailboxMessages(Connection conn,long companyId){
		return getSourceMessages(conn,companyId,"<>");
	}
   
   private static long getSourceMessages(Connection conn,long companyId, String condition){
    	try{
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Select count(d.companyId) as msgCount From DropifiMessage d Where d.companyId=? and d.sourceOfMsg "+condition+" ?");
				query.setLong(1, companyId);
				query.setString(2,  DropifiTools.WIDGET_MESSAGE);
				 
				ResultSet result = query.executeQuery();
				while(result.next()){
					return result.getLong("msgCount");
				}
			}
		}catch(Exception e){ 
			play.Logger.log4j.info(e.getMessage(), e);
		}
		return 0l;
    }
	
	//----------------------------------------------------------------------------------------------------
	
	//adding keywords
	public int addKeyword(Connection conn,Collection<String> keywords){
		return 0;
	}
 
	//adding and removing message tags
	public Collection<String> getTag() {
		return tags;
	}
	
	public boolean addTag(String tag) {
		return this.tags.add(tag);
	}
	
	public boolean removeTag(String tag){
		return this.tags.remove(tag);
	}

	//adding and removing notes
	public Collection<String> getNote() {
		return notes;
	}

	public boolean addNote(String note) {
		return this.notes.add(note);
	}
	
	public boolean removeNote(String note) {
		return this.notes.remove(note); 
	}
	
	//Applicable to only messages sent from the widget
	
	//adding and removing optional information about the message: applicable to widget message	 
	public Map<String, String> getOptionals() {
		return optionals;
	}

	public void setOptionals(Map<String, String> optionals) {
		this.optionals = optionals;
	}
		
	//give details like the country,city, longitude, latitude, referral site e.t.c of the person
	//sending the message 
	
	public MsgInfo getMsgInfo() {
		return msgInfo;
	}

	public void setMsgInfo(MsgInfo msgInfo) {
		this.msgInfo = msgInfo;
	}

	//--------------------------------------------------------------------------------------------------------
	//deleting messages
	
	public static int deleteMessage(long companyId, List messageIds){
		 
		EntityManager em = JPA.em(); 
		Timestamp updated = new Timestamp(new Date().getTime());
		int result = em.createQuery("UPDATE DropifiMessage d SET d.isArchived = :isArchived, d.updated = :updated where d.id in (:id) and d.companyId = :companyId")
	                 .setParameter("id", messageIds).setParameter("companyId", companyId).setParameter("updated",updated).setParameter("isArchived",true).executeUpdate(); 	
		
		result = em.createQuery("UPDATE MsgSentiment d SET d.isArchived = :isArchived where d.messageId in (:messageId) and d.companyId = :companyId")
                .setParameter("messageId", messageIds).setParameter("companyId", companyId).setParameter("isArchived",true).executeUpdate();  	

		return result>0?200:201;
	} 
	
	//--------------------------------------------------------------------------------------------------------
	//search messages
	 	
	//------------------------------------------------------------------------------------------------------
	//getters and setters
			
	//status of the message: New, Open, Pending, Closed
	 
 	public MsgStatus getStatus() {
		return status;
	}

	public void setStatus(MsgStatus status) {
		this.status = status;
	}
	
	//sender of message
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//subject of the message
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {		 
		this.subject = subject;
	}
	
	public void setSubjectToIntense(String subject,String intense) {
		try{
		if(subject!=null && !subject.isEmpty())
			this.subject = DropifiTools.removeHTML(subject);
		else if(intense!=null && !intense.isEmpty()){
			this.subject = intense;
		}else{
			this.subject = this.getTextBody();
		}
		}catch(Exception e){
			this.subject = "";
		}
	}

	//text/plain format of the message body
	public String getTextBody() {
		return textBody;
	}

	public void setTextBody(String textBody) {
		this.textBody = DropifiTools.removeHTML(textBody); 
	}

	//text/html format of the message body
	 
	public String getHtmlBody() {
		return htmlBody;
	}
	
	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}
	 
	public List<MsgKeyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<MsgKeyword> keywords) {
		this.keywords = keywords;
	}
	
	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}
	 
	public Collection<MsgAttachment>  getAttachment() {
		return attachments;
	}

	public void setAttachment(Collection<MsgAttachment> attachments){
		this.attachments = attachments;
	}
	 	 		
	//the company the message belongs
	public Company getCompany() {
		return Company.findById(this.getCompanyId());
	} 
	
	public long getCompanyId(){
		return this.companyId;
	}
	
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	   
	//date and time the message was sent and received in the mailbox
	public Timestamp getReceivedDate() {
		return  receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = new Timestamp(receivedDate.getTime());
	}

	public Timestamp getSentDate() {
		return  sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = new Timestamp(sentDate.getTime());
	}

	public String getSourceOfMsg() {
		return sourceOfMsg;
	}

	public void setSourceOfMsg(String sourceOfMsg) {
		this.sourceOfMsg = sourceOfMsg;
	}
	
	public String getMsgId(){
		return this.msgId;
	}
	
	public void setMsgId(String msgId){
		  this.msgId = msgId;
	}
	
	public Long getCustomerId(){
		return this.customerId;
	}
	
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public boolean isArchived() {
		return isArchived;
	}

	public void setArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}

	 	
}