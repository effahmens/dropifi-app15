/**
 * @author phillips effah mensah
 * @description 
 * @date 14/3/2012
 *  
 **/

package models;

import play.data.validation.*;
import play.db.jpa.Blob;
import play.db.jpa.Model; 
import dsubscription.resources.com.AccountType;
import models.dsubscription.SubServiceName;
import play.data.validation.*;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.SQLInjector;
import resources.TextToImage;
import resources.WidgetControlType;
import resources.WidgetControlValidator;
import resources.WidgetField;
import resources.helper.ServiceSerializer;
import view_serializers.ConSerializer;
import view_serializers.WiSerializer;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.Index;


@Entity
public class PreviewWidget extends Model{

	//-------------------------------------------------------------------------------------------------------- 
	//instance variables
	@Required @Column(nullable=false,unique=true) 
	private Long companyId; 
	@Required	@Column(nullable = false, unique=true)
	private String apikey;
	
	@Column(length=10000)
	private String header;
	private WiParameter wiParameter;
	
	@Column(length=10000)
	private String defaultMessage;
	@Column(nullable=false)
	private String defaultEmail;
	@Column(length=10000)
	private String responseSubject;
	@Column(length=10000)
	private String responseMessage;
	@Column(nullable=false)
	private Boolean responseActivated;
	private String responseColor;
	
	@Column(length=10000)
	private String sendButtonText;	 
	private String sendButtonColor;
	
	private String messageTemplate; 
	
	@Transient
	private String plugin;
		
	@ElementCollection(targetClass = WidgetControl.class, fetch=FetchType.EAGER) 
	@CollectionTable(name="previewWidgetControl", joinColumns= @JoinColumn(name="preWidget_id")) 	 
	private Collection<WidgetControl> preWidgetControls;
 
	private Boolean hasCaptcha;
	private String captchaValue;
	@Transient
	private static final String FILENAME="preview_tab_text.png";
	//--------------------------------------------------------------------------------------------------------
	//constructor:
	
	public PreviewWidget(String apikey,long companyId){		 
		 this.apikey = apikey;		  
		 this.setCompanyId(companyId);
		 this.setDefaultEmail("other");		 
		 this.setDefaultMessage("Thank you");
		 
		 this.setResponseActivated(true);
		 this.setResponseSubject("We will get back to you soon");
		 this.setResponseMessage("Thank you for contacting us! <br>We have received your request and will get back to you very soon.");
		 this.setResponseColor("#0D60B3");
		 
		 this.setSendButtonText("Send Message");
		 this.setSendButtonColor("#1E7BB8");
		 
		 this.setHasCaptcha(true);
		 this.setCaptchaValue("What's the result of");
		 
		 this.preWidgetControls = new LinkedList<WidgetControl>();
		 
		 this.setMessageTemplate("Dropifi Template");
	}
	
	public PreviewWidget(String apikey,long companyId,String defaultEmail) { 		 
		 this.apikey = apikey;		  
		 this.setCompanyId(companyId);
		 this.setDefaultEmail(defaultEmail); 		 
		 this.setDefaultMessage("Thank you"); 
		 
		 this.setResponseActivated(true);
		 this.setResponseSubject("We will get back to you soon");
		 this.setResponseMessage("Thank you for contacting us! <br>We have received your request and will get back to you very soon.");
		 this.setResponseColor("#0D60B3");
		 
		 this.setSendButtonText("Send Message");
		 this.setSendButtonColor("#1E7BB8"); 
		  
		 this.setHasCaptcha(true);
		 this.setCaptchaValue("What's the result of");
		 
		 this.preWidgetControls = new LinkedList<WidgetControl>(); 
		 
		 this.setMessageTemplate("Dropifi Template");
	}
	
	public PreviewWidget(WiParameter params){
		this.setWiParameter(params);
	}
	
	public PreviewWidget(Widget widget){
		 this.apikey = widget.getApikey();  
		 this.setCompanyId(widget.getCompanyId());
		 
		 this.header = widget.getHeader();
		
		 this.setDefaultEmail(widget.getDefaultEmail()); 		 
		 this.setDefaultMessage(widget.getDefaultMessage()); 
		 
		 this.setResponseActivated(widget.getResponseActivated());
		 this.setResponseSubject(widget.getResponseSubject());
		 this.setResponseMessage(widget.getResponseMessage());
		 this.setResponseColor(widget.getResponseColor());
		 
		 this.setSendButtonText(widget.getSendButtonText());
		 this.setSendButtonColor(widget.getSendButtonColor()); 
		  
		 this.addDefaultControls(widget.getWidgetControls()); 
		 this.setWiParameter(widget.getWiParameter());
		 
		 this.setMessageTemplate(widget.getMessageTemplate());
		 
		 this.setHasCaptcha(widget.getHasCaptcha());
		 this.setCaptchaValue(widget.getCaptchaValue());
	} 
	
	public PreviewWidget(){}
	 
	
	//--------------------------------------------------------------------------------------------------------
	//widget:  
	
	@Override
	public String toString() {
		return "PreviewWidget [companyId=" + companyId + ", apikey=" + apikey
				+ ", header=" + header + ", wiParameter=" + wiParameter
				+ ", defaultMessage=" + defaultMessage + ", defaultEmail="
				+ defaultEmail + ", responseSubject=" + responseSubject
				+ ", responseMessage=" + responseMessage
				+ ", responseActivated=" + responseActivated
				+ ", responseColor=" + responseColor + ", sendButtonText="
				+ sendButtonText + ", sendButtonColor=" + sendButtonColor
				+ ", messageTemplate=" + messageTemplate
				+ ", preWidgetControls=" + preWidgetControls + ", hasCaptcha="
				+ hasCaptcha + ", captchaValue=" + captchaValue  + "]";
	}

	/**
	 * get an instance of the PreviewWidget with default control values with out saving the PreviewWidget
	 * @return
	 */
	public PreviewWidget getUnSavedWidget(String domain){
		this.setTitle("Contact Us");
		this.setWiParameter(new WiParameter("#0D60B3", "Contact Us",DropifiTools.defaultFont,"right",35,"#0D60B3",domain,FILENAME)); 
		//add default controls to the PreviewWidget
		this.addDefaultControls();		
		return this;
	} 
	
	public PreviewWidget getUnSavedWidget(String domain,WiParameter param){
		this.setTitle("Contact Us");
		this.setWiParameter(new WiParameter(param.getButtonColor(), param.getButtonText(),DropifiTools.defaultFont,param.getButtonPosition(),35,"#0D60B3",domain,FILENAME)); 
		//add default controls to the PreviewWidget
		this.addDefaultControls();
		return this;
	}
	
	public PreviewWidget createWidget(){
		try{ 
			if(apikey !=null && !apikey.isEmpty()){  
				if(this.getTitle()==null)
					this.setTitle("Contact Us");
				
				if(this.getWiParameter() ==null)
					this.setWiParameter(new WiParameter("#0D60B3", "Contact Us",DropifiTools.defaultFont,"right",35,"#0D60B3")); 
				
				//add default controls to the PreviewWidget 
				if(this.getWidgetControls()==null || this.getWidgetControls().size()<5)
					this.addDefaultControls();
				
				return this.save();
			}
		}catch(Exception e){
			 play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
		
	}
	
	public int updateWidget(){ 
		return this.validateAndSave()?200:360;
	}
	
	public static PreviewWidget createUpdate(Widget widget,String domain){ 
		PreviewWidget pre = findByApiKey(widget.getApikey());
		
		if(pre==null){
			pre = new PreviewWidget(widget).createWidget();
		}else{
			 //pre.updateWidget(widget);
		}
		
		return pre;
	}
	
	public static int createUpdate(WiSerializer widget,String apikey){
		PreviewWidget pre = findByApiKey(apikey);
		
		if(pre==null){
			pre = new PreviewWidget(Widget.findByApiKey(apikey)).createWidget(); 
		} 
		
		return pre.updateWidget(widget); 
	}
	
	private void addDefaultControls(){
		int i=1;
		this.preWidgetControls = new LinkedList<WidgetControl>();
		this.preWidgetControls.add(new WidgetControl(WidgetField.name, "your full name", WidgetControlType.Text, WidgetControlValidator.None, i++, true, false,"your name is required"));
		this.preWidgetControls.add(new WidgetControl(WidgetField.email,"your email", WidgetControlType.Email, WidgetControlValidator.Email, i++, true, true,"your email is required"));
		this.preWidgetControls.add(new WidgetControl(WidgetField.phone,"your phone", WidgetControlType.Text, WidgetControlValidator.None, i++, true, false,"your phone number is required"));
		this.preWidgetControls.add(new WidgetControl(WidgetField.subject,"your subject", WidgetControlType.Text, WidgetControlValidator.Email, i++, true, true,"the subject of the message is required"));
		this.preWidgetControls.add(new WidgetControl(WidgetField.message,"how can we help you?", WidgetControlType.TextArea, WidgetControlValidator.None, i++, true, true,"your message is required"));			
		this.preWidgetControls.add(new WidgetControl(WidgetField.attachment,"attach files", WidgetControlType.Attachment, WidgetControlValidator.None, i++, true, false,"you have not attached any file"));
	}
	
	private void addDefaultControls(Collection<WidgetControl> widgetControls){
		this.preWidgetControls = new LinkedList<WidgetControl>();
		for(WidgetControl control : widgetControls){
			this.preWidgetControls.add(control);
		}
	}

	/**
	 * Called any time a user subscribed to a Plan 
	 * enable dropifi branding on the widget if the plan subscribed to does not permit removal of branding
	 * @param apikey
	 */
	public static void autoUpdateWidget(String apikey,String domain, String currentPlan){
		try{
			PreviewWidget widget = PreviewWidget.findByApiKey(apikey);
			
			if(widget!=null){
				WiParameter param = widget.getWiParameter();
				widget.updateWidget(domain, widget,param);
				
				//save the existing widget setting
				/*if(!currentPlan.equalsIgnoreCase(AccountType.Free.name())){
					WidgetDownGrade downgrade = new WidgetDownGrade(apikey, AccountType.valueOf(currentPlan), param);
					downgrade.createUpate();
				}*/
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	private static void updateWidget(String domain, PreviewWidget widget, WiParameter param){
		try{ 
			if(widget.getSendButtonColor()==null || widget.getSendButtonColor().trim().isEmpty()){ 	
				 widget.setSendButtonColor("#1E7BB8"); 
			}
			
			if(widget.getSendButtonText()==null || widget.getSendButtonText().trim().isEmpty()){
				widget.setSendButtonText("Send Message"); 
			}
			
			if(widget.getMessageTemplate()==null){
				widget.setMessageTemplate("Dropifi Template");
			}
			
			widget.getWiParameter().setButtonTextColor(widget.getWiParameter().setImageText(FILENAME,param.getButtonText(), domain)); 
			widget.updateWidget();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
 
	public int updateWidget(WiParameter widget,String domain){
		this.setWiParameter(new WiParameter(widget.getButtonColor(), widget.getButtonText(), widget.getButtonFont(),
				widget.getButtonPosition(),widget.getButtonPercent(), widget.getButtonTextColor(),domain,FILENAME));
		return this.updateWidget();
	}
	
	public int updateWidget(WiSerializer widget){
		
		if(widget != null){ 
			//set the widget parameters
			this.setTitle(widget.getWidgetheader());
			this.setWiParameter(new WiParameter(widget.getButtonColor(), widget.getButtonText(), widget.getButtonFont(),
					widget.getButtonPosition(),widget.getButtonPercent(), widget.getButtonTextColor(),widget.getDomain(),FILENAME) );
			
			this.setDefaultEmail(widget.getDefaultEmail());
			this.setDefaultMessage(widget.getDefaultMessage()); 
			this.setResponseSubject(widget.getResponseSubject());
			this.setResponseMessage(widget.getResponseMessage()); 
			this.setResponseColor(widget.getResponseColor());
			
			if(this.getResponseColor()==null || this.getResponseColor().trim().isEmpty()){
				this.setResponseColor("#1E7BB8");
			}
			 			
			this.setSendButtonText(widget.getSendButtonText());
			this.setSendButtonColor(widget.getSendButtonColor());
			
			this.setHasCaptcha(widget.isHasCaptcha());
			this.setCaptchaValue(widget.getCaptchaValue());
			
			if(widget.getSendButtonColor()==null || widget.getSendButtonColor().trim().isEmpty()){ 	
				 this.setSendButtonColor("#1E7BB8"); 
			}
			
			if(widget.getSendButtonText()==null || widget.getSendButtonText().trim().isEmpty()){
				this.setSendButtonText("Send Message");
			}
			
			if(widget.getMessageTemplate()!=null)
				this.setMessageTemplate(widget.getMessageTemplate());
			
			//set the widget control parameters
			for(WidgetControl control: this.getWidgetControls()){
				
				switch(control.getCondId().ordinal()){
					case 0: //attachment
						control.setTitle( widget.getAttachment());
						break;
					case 1: //Email
						control.setTitle(widget.getEmail());
						break;
					case 2: //name
						control.setTitle(widget.getName());
						break;
					case 3: //message
						control.setTitle(widget.getMessage());
						break;
					case 4: //phone
						control.setTitle(widget.getPhone());
						break;
					case 5: //subject
						control.setTitle(widget.getSubject());
						break;			
				}
			}
			return this.updateWidget(); 
		}
		return 501;
	}
	
	public int updateWidget(Widget widget){
		
		if(widget != null){ 
			//set the widget parameters
			this.setTitle(widget.getTitle());
			this.setWiParameter(widget.getWiParameter());
			
			this.setDefaultEmail(widget.getDefaultEmail());
			this.setDefaultMessage(widget.getDefaultMessage()); 
			this.setResponseSubject(widget.getResponseSubject());
			this.setResponseMessage(widget.getResponseMessage()); 
			this.setResponseColor(widget.getResponseColor());
			
			this.setHasCaptcha(widget.getHasCaptcha());
			this.setCaptchaValue(widget.getCaptchaValue());
			
			if(this.getResponseColor()==null || this.getResponseColor().trim().isEmpty()){
				this.setResponseColor("#1E7BB8");
			}
			 			
			this.setSendButtonText(widget.getSendButtonText());
			this.setSendButtonColor(widget.getSendButtonColor());
			
			if(widget.getSendButtonColor()==null || widget.getSendButtonColor().trim().isEmpty()){ 	
				 this.setSendButtonColor("#1E7BB8"); 
			}
			
			if(widget.getSendButtonText()==null || widget.getSendButtonText().trim().isEmpty()){
				this.setSendButtonText("Send Message");
			}
			
			if(widget.getMessageTemplate()!=null)
				this.setMessageTemplate(widget.getMessageTemplate());
			
			 
			//set the widget control parameters
			this.addDefaultControls(widget.getWidgetControls()); 
			return this.updateWidget(); 
		}
		return 501;
	}
	
	public int updateWidgetErrorMsg(HashMap<String,String> errorMsgs){
		//set the errorMsg for the controls
		if(errorMsgs!=null){
			try{
				for(WidgetControl control: this.getWidgetControls()){ 
					String value = errorMsgs.get(control.getCondId().name());
					if(value!=null && value.trim().length()>=1)
						control.setErrorMsg(value);
				}
				return this.updateWidget(); 
			}catch(Exception e){}
		}
		return 501;
	}
	
	public static int updateDefaultEmail(String apikey, String currentEmail,String newEmail){
		int result = PreviewWidget.em().createQuery("Update PreviewWidget w Set w.defaultEmail=:newEmail Where w.apikey=:apikey and w.defaultEmail=:currentEmail")
				.setParameter("newEmail", newEmail)
				.setParameter("apikey", apikey)
				.setParameter("currentEmail", currentEmail).executeUpdate();
		return result>=0?200:201;	 
	}
	 
	public String getPlugin(){
		return this.plugin;
	} 
	
	public static void updateImageText(Connection conn, String apikey,String buttonText,String buttonColor,String buttonFont,String buttonPosition){
		
		 try{
			 String domain = Company.findDomainByApikey(conn, apikey);
			 if(domain!=null){
				 
				TextToImage textToImage = new TextToImage("pre_tab_text.png", buttonText, domain, buttonColor,buttonPosition);
				textToImage.setTextFont(buttonFont);
				String imageText = textToImage.transform(); 
				String textColor = Integer.toHexString(textToImage.getTextColor().getRGB()).replaceFirst("ff","#");
			 
				//String[] tables = {"PreviewWidget"};
				
				//for(String table : tables){
					PreparedStatement query = conn.prepareStatement("Update PreviewWidget w Set w.buttonTextColor=?, w.imageText=? Where w.apikey =?");
					query.setString(1, textColor);
					query.setString(2,imageText);
					query.setString(3,apikey);				
					query.executeUpdate();
				//}
			 }
		 }catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(),e);
		 } 
	} 
	
	public static void updateImageText(Connection conn,String apikey, WiParameter param){
		updateImageText(conn, apikey, param.getButtonText(), param.getButtonColor(),param.getButtonFont(),param.getButtonPosition());
	}
 
	public  WidgetDefault getWidgetDefault(String publicKey){
		WidgetDefault def = new WidgetDefault(this.getApikey(),publicKey);
		if(def!=null){
			def.setWiParameter(this.getWiParameter());
			def.setHtml(def.generateHtmlTwo( new Widget(this)));
		}
		return def;
	}
	//--------------------------------------------------------------------------------------------------------
	//Widget controls: 
	
 	public int addControl(WidgetControl control){
		
		if(!containsControl(control.getCondId())){ 
			 		
			if(this.preWidgetControls.add(control)){
				return this.validateAndSave() ? 200:201; 			
			}
			return 203;
		}
		return 302;  
	}
 		
	public int updateControl(WidgetField condId, WidgetControl newControl){
		WidgetControl control = findControlByCondId(condId);
		
		if(control != null){ 
			
			//check if the update control and the existing has the same title
			if(!control.getCondId().equals(newControl.getCondId())){
				//check for duplication of names
				if(containsControl(newControl.getCondId()) ){					
					return 302;
				}else{
					control.setTitle(newControl.getTitle());
				}				
			}else{
				control.setTitle(newControl.getTitle());
			}
			
			control.setType(newControl.getType());
			control.setPosition(newControl.getPosition());
			control.setActivated(newControl.getActivated());			 
			control.setRequired(newControl.getRequired());
			control.setValidator(newControl.getValidator()); 
			
			return this.validateAndSave() ? 200:201; 			 
		}
		return 501;	 
	}
	
	public int updateConActivated(WidgetField condId, boolean activated){ 		
		WidgetControl control = findControlByCondId(condId);
		
		if(control !=null){
			control.setActivated(activated);			
			this.save();
			return 200;
		} 		
		return 501;
	}
	
	public int updateConTitle(WidgetField condId, String title){ 		
		WidgetControl control = findControlByCondId(condId);
		
		if(control !=null){
			control.setTitle(title);			 
			this.save();
			return 200;
		} 		
		return 501;
	}
	
	public int  updateConActivated(String condId, boolean activated){
		WidgetField fieldId =null;
		
		//control id lookup
		if(condId.equals(WidgetField.attachment.toString()))
			fieldId = WidgetField.attachment;
		else if(condId.equals(WidgetField.email.toString()))
			fieldId = WidgetField.email;
		else if(condId.equals(WidgetField.message.toString()))
			fieldId = WidgetField.message;
		else if(condId.equals(WidgetField.name.toString()))
			fieldId = WidgetField.name;
		else if(condId.equals(WidgetField.phone.toString()))
			fieldId = WidgetField.phone;
		else if(condId.equals(WidgetField.subject.toString()))
			fieldId = WidgetField.subject;		
		
		return fieldId!=null? updateConActivated(fieldId, activated):506;
	}
	
	public int updateRespActivated(boolean activated){ 		 
		this.setResponseActivated(activated);
		this.save(); 
		return 200;
	}
	
	public int updateHasCaptcha(boolean activated){ 		 
		this.setHasCaptcha(activated);
		this.save(); 
		return 200;
	}
	
	public int removeControl(WidgetField condId){
		WidgetControl control = findControlByCondId(condId);
		if(control !=null){
			if(this.preWidgetControls.remove(control)){		 
				this.save();
				return 200;
			}
			return 201;
		}
		return 501;
	}
		
	public WidgetControl findControlByCondId(WidgetField condId){
		if(condId != null){
			Iterator<WidgetControl> listControl = this.preWidgetControls.iterator();
			
			while(listControl.hasNext()){
				WidgetControl control = listControl.next();
				
				if(control.getCondId().equals(condId))
					return control;
			}
		}
		return null; 
	}
	
	public boolean containsControl(WidgetField condId){
		return findControlByCondId(condId)!= null;
	}
		
	public Collection<WidgetControl> getWidgetControls(){
		//TOBE implemented
		return this.preWidgetControls;
	}
		
	public List<ConSerializer> getSerializeControls(){
		
		List<ConSerializer> conSer = new ArrayList<ConSerializer>();
		Iterator<WidgetControl> controls = this.preWidgetControls.iterator();
		
		while(controls.hasNext()){
			conSer.add(new ConSerializer(controls.next()));
		}
		return conSer;
	}
		
	public void setWidgetControls(Collection<WidgetControl> widgetControls){
		this.preWidgetControls = widgetControls;
	}
	
	public static PreviewWidget findByApiKey(String apikey){
		return PreviewWidget.find("byApikey", apikey).first();  
	}
	
	public static PreviewWidget findByPublickey(String publicKey){
		return PreviewWidget.find("select w from PreviewWidget w, WidgetDefault d where (w.apikey = d.apikey) and (d.publicKey = ?)", publicKey).first();
	}
	
	public static PreviewWidget findByPlugin(String ecomName, EcomBlogType ecomType){
		return PreviewWidget.find("select w from PreviewWidget w, Company c where (w.apikey = c.apikey) and " +
				"(c.ecomBlogPlugin.eb_name = ?) and (c.ecomBlogPlugin.eb_pluginType=?)", ecomName, ecomType).first(); 
	}

	public static String findEmailByApikey(String apikey){
		return PreviewWidget.find("select w.defaultEmail from PreviewWidget w where w.apikey =? ", apikey).first();
	}
	
	public static PreviewWidget findByComIdEMail(Long companyId,String email){
		if(companyId != null && email !=null && !email.trim().isEmpty()){			
			return PreviewWidget.find("select w from PreviewWidget w where (w.defaultEmail != 'other') and" +
					" (w.companyId = ?) and w.defaultEmail = ?",companyId,email).first(); 
		}
		return null;
	}
	
	public static PreviewWidget findByCompanyId(Long companyId){
		if(companyId != null){			
			return PreviewWidget.find("select w from PreviewWidget w where (w.defaultEmail != 'other') and" +
					" (w.companyId = ?) ",companyId).first(); 
		}
		return null;
	}
	
	public static Boolean findHasCaptch(String publicKey) {
		return Widget.find("Select w.hasCaptcha from PreviewWidget w, WidgetDefault d Where w.apikey=d.apikey And d.publicKey=?",publicKey).first();  
	}
	
	//--------------------------------------------------------------------------------------------------------
	//Using ElasticSearch to query the PreviewWidget Model
	public static  PreviewWidget searchByApiKey(String apikey){
		/*try{
			SearchResults<PreviewWidget> widgets = ElasticSearch.search(QueryBuilders.fieldQuery("apikey",apikey), PreviewWidget.class);
			if(widgets!=null && widgets.objects!=null && widgets.totalCount>0)
				return widgets.objects.get(0);
		}catch(Exception e){ play.Logger.log4j.info(e.getMessage(),e); }
		*/ 
		return findByApiKey(apikey);
	}
	
	//--------------------------------------------------------------------------------------------------------
	//getters and setters of instance variables
	
	public static String findMessageTemplate(Connection conn,String apikey){
		try {
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Select messageTemplate From PreviewWidget Where apikey=?");
				query.setString(1, apikey);
				
				ResultSet result = query.executeQuery();
				while(result.next()){
					return result.getString("messageTemplate");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public String getTitle() {
		return header;
	}
	
	public void setTitle(String title) {
		this.header = title;
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public void setDefaultMessage(String submitMessage) {
		this.defaultMessage = submitMessage;
	}

	public String getApikey(){
		return this.apikey;
	}

	public String getDefaultEmail() {
		return defaultEmail;
	}

	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	public String getResponseSubject() {
		return responseSubject;
	}
	
	public void setResponseSubject(String responseSubject) {
		this.responseSubject = responseSubject;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Boolean getResponseActivated() {
		return responseActivated;
	}
	
	public void setResponseActivated(Boolean responseActivated) {
		this.responseActivated = responseActivated;
	} 

	public WiParameter getWiParameter(){
		return this.wiParameter;
	}

	public static WiParameter findByWidgetApikey(String apikey){
		return PreviewWidget.find("select w.wiParameter from PreviewWidget w where w.apikey = ?", apikey).first();
	}

	public void setWiParameter(WiParameter wiParameter) {
		this.wiParameter = wiParameter;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getSendButtonText() {
		return sendButtonText;
	}

	public void setSendButtonText(String sendButtonText) {
		this.sendButtonText = sendButtonText;
	}

	public String getSendButtonColor() {
		return sendButtonColor;
	}

	public void setSendButtonColor(String sendButtonColor) {
		this.sendButtonColor = sendButtonColor;
	}

	public String getResponseColor() {
		return responseColor;
	}

	public void setResponseColor(String responseColor) {
		this.responseColor = responseColor;
	}

	public String getMessageTemplate() {
		return messageTemplate;
	}

	public void setMessageTemplate(String messageTemplate) {
		this.messageTemplate = messageTemplate;
	}
	
	public String getHeader(){
		return header;
	}

	public Boolean getHasCaptcha() {
		return hasCaptcha;
	}

	public void setHasCaptcha(Boolean hasCaptcha) {
		this.hasCaptcha = hasCaptcha;
	}

	public String getCaptchaValue() {
		return captchaValue;
	}

	public void setCaptchaValue(String captchaValue) {
		this.captchaValue = captchaValue;
	}
}
