package models;

import static akka.actor.Actors.actorOf;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*; 

import javax.persistence.*;

import org.hibernate.annotations.Index;

import models.dsubscription.SubServiceName;
import job.cache.WRCacheActor;
import akka.actor.ActorRef;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;

import play.cache.Cache;
import play.db.jpa.Model;
import resources.CacheKey;
import resources.DropifiTools;
import resources.RuleGroup;
import resources.RuleType;
import resources.helper.ServiceSerializer;

@Entity
public class WidgetRule extends MainRule implements Serializable{
	
	@Column(nullable=false) @Index(name="messageManagerId")
	private Long messageManagerId; 	
	@Column(nullable=false) @Index(name="companyId")
	private Long companyId;
	
	@Column(length=10000, nullable=false)
	@SerializedName(value="name")
	private String name;

	@Column(length=100, nullable=false)
	@SerializedName(value="type")
	private RuleType type;

	@Column(nullable=false)
	@SerializedName(value="activated")
	private Boolean activated;
	@SerializedName(value="rulegroup")
	private RuleGroup ruleGroup;	
	 
	@ElementCollection(targetClass = RuleCondition.class)
	@CollectionTable(name="widgetRuleCondition", joinColumns=@JoinColumn(name="msgCond_Id"))
	public Collection<RuleCondition> ruleConditions;
	
	@ElementCollection(targetClass = RuleAction.class)
	@CollectionTable(name="widgetRuleAction", joinColumns=@JoinColumn(name="msgAct_Id")) 
	public Collection<RuleAction> ruleActions;
	
	private Timestamp created; 
	private Timestamp updated;
	
	private int priorityLevel;
	private Boolean knownActivation;
	//------------------------------------------------------------------------------------
	//constructor
	
	public WidgetRule(long companyId,String name, boolean activated, RuleGroup ruleGroup) {		 
		//TODO Auto-generated constructor stub 			 
		this.setName(name);
		this.setType(RuleType.Widget);
		this.setActivated(activated);
		this.setRuleGroup(ruleGroup); 
		this.setCompanyId(companyId);
		this.ruleConditions = new LinkedList<RuleCondition>();
		this.ruleActions = new LinkedList<RuleAction>();
	}
	
	public WidgetRule(long companyId,String name){
		this(companyId,name,true, RuleGroup.ALL);
	}
	
	public WidgetRule(){}
	
	//-------------------------------------------------------------------------------------
	//Rule: create, update, remove and find widget rule
	
	public int createOrUpdateRule(){ 
		//TODO Auto-generated method stub	 
		
		if(this !=null && this.getMessageManagerId()!= null && this.getCompanyId()!=null){ 
			
			WidgetRule rule = findByName(this.getCompanyId(), this.getName());			
			if(rule == null){ 
				//save the new message rule
				 this.setPriorityLevel((int)new Date().getTime());
				 Timestamp time = new Timestamp(new Date().getTime()); 
				 this.setCreated(time);
				 this.setUpdated(time); 
				 return this.validateAndSave()?200:201; 				 
			}else{ 
				return 305;
			} 
		}
		
		return 501;		
	}
	
	public static int orderRules(long companyId,List<String> rules){
		if(rules!=null && rules.size()>0){ 
			for(String recipient : rules){
				 JsonObject json = (JsonObject) new JsonParser().parse(recipient);
				 WidgetRule found = findByName(companyId,json.get("name").getAsString());
				 if(found!=null){ 
					 found.setPriorityLevel((int)new Date().getTime());
					 Timestamp time = new Timestamp(new Date().getTime());
					 found.setCreated(time);
					 found.setUpdated(time);
					 found.validateAndSave();
				 }
			}
			return 200;
		}
		return 501;
	}
	
	public static int updateRule(Long companyId,String originalName, String name, boolean activated, RuleGroup ruleGroup,List<RuleCondition> ruleConditions,List<RuleAction> ruleActions){
		WidgetRule rule = WidgetRule.findByName(companyId, originalName);
		
		if(rule !=null && rule.getId()!=null){
			//the new name is not equal to the original plan 
			
			if(!originalName.equalsIgnoreCase(name)){
				WidgetRule found = WidgetRule.findByName(companyId, name);
				
				if(found!=null && found.getId()!=null)
					return 305;
				
				rule.setName(name);
			}
			rule.setUpdated(new Timestamp(new Date().getTime()));
			rule.setActivated(activated);
			rule.setRuleGroup(ruleGroup) ;	
			rule.ruleActions = ruleActions;		
			rule.ruleConditions = ruleConditions;
			return rule.validateAndSave()?200:201;
		}
		return 501;
	}
	
	public static int removeMsgRule(long companyId,String name){
		WidgetRule rule = WidgetRule.findByName(companyId, name);
		if(rule != null){
			return rule.delete()!=null ? 200:201;			 
		}
		return 501;	
	}
	
	public static int updateActivated(long companyId,String name, boolean activated){
		int status = WidgetRule.em().createQuery("Update WidgetRule m Set m.activated = ? Where m.companyId =? And m.name = ? And m.type =?")
		.setParameter(1, activated).setParameter(2,companyId).setParameter(3,name).setParameter(4,RuleType.Widget).executeUpdate(); 
		return status>0?200:201;
	}
		
	public static WidgetRule findByName(Long companyId, String name){
		return WidgetRule.find("byCompanyIdAndTypeAndName",companyId,RuleType.Widget,name).first();
	}
	
	public static List<WidgetRule> findAll(Long companyId){
		return  WidgetRule.find("Select w  from WidgetRule w Where w.companyId=? And w.type=? order by w.priorityLevel ASC",companyId,RuleType.Widget).fetch(); 
	}
	
	public static List<WidgetRule> findSortedRules(Long companyId){
		return WidgetRule.find("Select w  from WidgetRule w Where w.companyId=? And w.type=? order by w.priorityLevel ASC",companyId,RuleType.Widget).fetch(); 
	}
	
	private static WidgetRule getWidgetRule(Connection conn,ResultSet result ) throws SQLException{
		WidgetRule rule = new WidgetRule();
		
		rule.id = result.getLong("id"); 
		rule.setActivated(result.getBoolean("activated"));
		rule.setCompanyId(result.getLong("companyId"));
		rule.setMessageManagerId(result.getLong("messageManagerId"));
		rule.setName(result.getString("name"));
		rule.setRuleGroup(RuleGroup.getType(result.getInt("ruleGroup")));
		rule.setType(RuleType.valueOf(result.getInt("type")));
		rule.setCreated(result.getTimestamp("created"));
		rule.setUpdated(result.getTimestamp("updated"));
		
		//search for all the conditions attached to this rule
		rule.ruleConditions = findRuleConditions(conn ,rule.getCompanyId(),rule.getId(), rule.getName());
		
		//search for all the actions attached to this rule;
		rule.ruleActions = findRuleActions(conn ,rule.getCompanyId(),rule.getId(), rule.getName()); 
		return rule;
	}
	public static List<WidgetRule> findAll(Connection conn,long companyId) {
		List<WidgetRule> rules = new LinkedList<WidgetRule>();
		try{
			if(conn!=null && !conn.isClosed()){ 
				
				PreparedStatement query = conn.prepareStatement("select w.activated, w.id, w.companyId,w.messageManagerId, w.name, w.ruleGroup, w.type, w.updated, w.created" +
						" from WidgetRule w, Company c where  w.companyId = c.id  and w.type = ? and c.id = ? order by w.priorityLevel ASC");
				
				query.setInt(1, RuleType.Widget.ordinal());
				query.setLong(2, companyId);			
				ResultSet result = query.executeQuery();
				 
				while(result.next()){
					rules.add(getWidgetRule(conn, result));
				}
			}
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		return rules;
	}
	
	public static List<WidgetRule> findAll(Connection conn,long companyId,boolean activated) {
		List<WidgetRule> rules = new LinkedList<WidgetRule>();
		try{
			if(conn!=null && !conn.isClosed()){ 
				
				PreparedStatement query = conn.prepareStatement("select w.activated, w.id, w.companyId,w.messageManagerId, w.name, w.ruleGroup, w.type, w.updated, w.created" +
						" from WidgetRule w, Company c where  w.companyId = c.id  and w.type = ? and c.id = ? and w.activated=? order by w.priorityLevel ASC");
				
				query.setInt(1, RuleType.Widget.ordinal());
				query.setLong(2, companyId);
				query.setBoolean(3, activated);
				ResultSet result = query.executeQuery();
				 
				while(result.next()){
					rules.add(getWidgetRule(conn, result));
				}
			}
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		return rules;
	}
	
	/***
	 * Called any time a user subscribed to a Plan 
	 * Loop through the list of widget rules and deactivated all widget rules which index number is more than service required volume
	 * @param apikey
	 * @param companyId
	 */
	public static void autoUpdateWidgetRule(String apikey,long companyId){
		try{
			List<WidgetRule> rules = findAll(companyId);
			
			if(rules!=null){
				ListIterator<WidgetRule> rulelist = rules.listIterator();
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Widget_Rules, SubServiceName.BusinessRules);
				long index = 1;
				while(rulelist.hasNext()){ 
					WidgetRule rule = rulelist.next();
					boolean act = true;
					if( !(index++ <=service.getThreshold().getVolume()) ){
						rule.setKnownActivaton(rule.getActivated());
						act = false;
					}else{
						if(rule.isKnownActivation()!=null && !rule.getActivated()){
							act = rule.isKnownActivation();
						}else{
							act = rule.getActivated();
							rule.setKnownActivaton(act);
						}
												
					}
					updateActivated(companyId, rule.getName(), act);
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	@Deprecated
	public static void resetCache(int status,String apikey,long companyId, String identifier){
		if(status == 200){
			//clear the associate cache for the rule list and send an akka actor message to reset the cache
			String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Widget_Rules, identifier);				
			Cache.delete(cacheKey);
					
			ActorRef cacheActor = actorOf(WRCacheActor.class).start();  
			cacheActor.tell(DropifiTools.createCacheMap(cacheKey,identifier, companyId));
		}
	}
	
	public static Collection<RuleCondition> findRuleConditions(Connection conn ,Long companyId,Long id, String name) throws SQLException{
		Collection<RuleCondition> conditions = new LinkedList<RuleCondition>();
		if(conn !=null && !conn.isClosed()) {
			
			PreparedStatement query = conn.prepareStatement("select r.msgCondition, r.msgComparison, r.msgExpectation from " +
					" widgetRuleCondition r, WidgetRule w where r.msgCond_Id = w.id and w.id = ? and w.name = ? and w.companyId = ? order by w.priorityLevel ASC");
			
			query.setLong(1, id);
			query.setString(2, name);		 
			query.setLong(3, companyId);
			
			ResultSet result = query.executeQuery();
		
			while(result.next()) {				
				RuleCondition condition = new RuleCondition();
				
				condition.setCondition(result.getString("msgCondition"));
				condition.setComparison(result.getString("msgComparison"));
				condition.setExpectation(result.getString("msgExpectation"));
				
				conditions.add(condition); 
			}			
		}
		return conditions;
	}
	
	public static Collection<RuleAction> findRuleActions(Connection conn ,Long companyId,Long id, String name) throws SQLException{
		Collection<RuleAction> actions = new LinkedList<RuleAction>();
		if(conn !=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select r.action, r.expectation from " +
					" widgetRuleAction r, WidgetRule w where r.msgAct_Id = w.id and w.id = ? and w.name = ? and w.companyId = ? order by w.priorityLevel ASC");
			
			query.setLong(1, id);
			query.setString(2, name);		 
			query.setLong(3, companyId);
			
			ResultSet result = query.executeQuery();
			
			while(result.next()){
				
				RuleAction action = new RuleAction(); 
				action.setAction(result.getString("action"));				 
				action.setExpectation(result.getString("expectation"));				
				
				actions.add(action);				
			}			
		}
		return actions;
	}
	
	//-------------------------------------------------------------------------------------
	//RULE CONDITIONS: adding, removing and updating msgRule condition
 
	@Override
 	public Collection<RuleCondition> getConditions() {
		// TODO Auto-generated method stub
		return this.ruleConditions;
	}

	@Override
	public RuleCondition addCondition(RuleCondition condition) {
		// TODO Auto-generated method stub
		//check that a similar rule condition with the same parameter values do not exist
		
		RuleCondition cond = findConditionByAll(condition.getCondition(), condition.getComparison(), condition.getExpectation());
		
		if(this != null && cond == null){
			//save the RuleCondition if all parameter conditions are satisfied
			this.ruleConditions.add(condition);
			if(this.validateAndSave())
				return condition;
		}			
		return null;
	}
	
	public void addConditions(List<RuleCondition> conditions) {
		this.ruleConditions= conditions;
	}

	@Override
	public boolean updateCondition(Long id, RuleCondition condition) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeCondition(String condition, String comparison,String expectation) {
		// TODO Auto-generated method stub
		return removeCondition(new RuleCondition(condition, comparison, expectation));
	}
	
	public boolean removeCondition(RuleCondition condtion) {
		// TODO Auto-generated method stub
		return this.ruleConditions.remove(condtion);
	}
		
	@Override
	public RuleCondition findConditionByAll(String condition, String comparison,
			String expectation) {
		// TODO Auto-generated method stub
		
		RuleCondition control = new RuleCondition(condition, comparison, expectation);
		//return RuleCondition.findConditionByAll(this.getId(), condition, comparison, expectation);
		Iterator<RuleCondition> cond = this.ruleConditions.iterator();
		
		while(cond.hasNext()) {
			RuleCondition ruleCond = cond.next(); 			
			if(ruleCond.equals(control)){
				return ruleCond;
			}
		}
		
		return null;
	}

	
	//-----------------------------------------------------------------------------------------------
	//RULE ACTIONS: adding, removing and updating msgRule condition
	
	@Override
	public RuleAction addAction(RuleAction action) {
		// TODO Auto-generated method stub
		
		//check that a similar rule condition with the same parameter values do not exist
		RuleAction foundAction = findActionByAll(action.getAction(), action.getExpectation());
		
		if(this != null && foundAction == null){
			 this.ruleActions.add(action); 			 
			 if(this.validateAndSave())
				 return action;
		}
		return null;
	}
	
	public void addActions(List<RuleAction> actions){
		this.ruleActions = actions;
	}

	@Override
	public boolean updateAction(Long id, RuleAction action) {
		// TODO Auto-generated method stub
		return false;
	}
 	
	@Override
	public boolean removeAction(String action, String expectation) {
		// TODO Auto-generated method stub
		return removeAction(new RuleAction(action,expectation));
	}
	
	public boolean removeAction(RuleAction action){
		return this.ruleActions.remove(action);
	}
 	
	@Override
	public RuleAction findActionByAll(String action, String expectation) {
		// TODO Auto-generated method stub
		RuleAction control = new RuleAction(action, expectation); 		 
		Iterator<RuleAction> ruleAction = this.ruleActions.iterator();
		
		while(ruleAction.hasNext()){
			RuleAction ruleAct = ruleAction.next(); 			
			if(ruleAct.equals(control)){
				return ruleAct;
			}
		}
		return null;
	}

	@Override
	public Collection<RuleAction> getActions() {
		// TODO Auto-generated method stub
		return this.ruleActions;
	}
	
	//-------------------------------------------------------------------------------------------
	//cache
 	
	//--------------------------------------------------------------------------------------------
	//getters and setters
	
	public Long getMessageManagerId() {
		return this.messageManagerId; 
	}
	
	public void setMessageManagerId(long messageManagerId) {
		this.messageManagerId = messageManagerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RuleType getType() {
		return type;
	}

	public void setType(RuleType type) {
		this.type = type;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public RuleGroup getRuleGroup() {
		return ruleGroup;
	}
	
	public void setRuleGroup(RuleGroup ruleGroup) {
		this.ruleGroup = ruleGroup;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public int getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(int priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Boolean isKnownActivation() {
		return knownActivation;
	}

	public void setKnownActivaton(boolean knownActivation) {
		this.knownActivation = knownActivation;
	}
 
}
