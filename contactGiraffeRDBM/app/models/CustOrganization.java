package models;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import play.data.validation.*;
import play.db.jpa.Model; 
import resources.DCON;
import resources.SQLInjector;

import javax.persistence.*;

import org.hibernate.annotations.Index;

@Entity
public class CustOrganization extends Model{
	
	@ManyToOne
	private CustomerProfile customerProfile; 
	@Column(nullable=false) @Index(name="email")
	private String email;
	private String name;	 
	private String title;	 
	private Timestamp startDate;		
	private boolean isPrimary;

	@Transient
	private long profileId;
		
	
	public CustOrganization(CustomerProfile customerProfile, String name,
			String title, Timestamp startDate, boolean isPrimary) {
		
		this.customerProfile = customerProfile;
		this.name = name;
		this.title = title;
		this.startDate = startDate;
		this.isPrimary = isPrimary;
	}
	
	public CustOrganization(Long profileId,String email, String name,
			String title, Timestamp startDate, Boolean isPrimary) {
		
		this.setProfileId(profileId);
		this.email = email;
		this.name = name;
		this.title = title;
		this.startDate = startDate;
		this.isPrimary = isPrimary!=null?false:isPrimary;
	}
	
	public CustOrganization(){ 
	}
	
	//-------------------------------------------------------------------
	
	public void saveOrganization(Connection conn) throws SQLException{
		CustOrganization organ = findOrganByEmail(conn,this.getEmail(), this.getName());
		if(organ ==null)
			this.insertOrgan(conn);  
		else
			organ.updateOrgan(conn,this);
	}
	
	public static int insertOrgan(Connection conn, List<CustOrganization> organs) throws SQLException{
		
		if(organs !=null){ 
			 int num = 0;
			if(conn!=null && !conn.isClosed()){	 		
				
				 for(CustOrganization organ:organs){					
						PreparedStatement query = conn.prepareStatement("insert into CustOrganization " +
								"(CustomerProfile_id, email,name,title,startDate, isPrimary) value (?,?,?,?,?,?)");
						
						int i =1;
						query.setLong(i++,    	organ.getProfileId());
						query.setString(i++, 	SQLInjector.escape(organ.getEmail()));
						query.setString(i++, 	SQLInjector.escape(organ.getName()));
						query.setString(i++, 	SQLInjector.escape(organ.getTitle()));
						query.setTimestamp(i++,  organ.getStartDate()); 
						query.setBoolean(i++,	organ.isPrimary() );
						query.executeUpdate();		
						num++;
				 }
				 
			}	
			return num;
		}
		
		return -1;
	}
	
	public int insertOrgan(Connection conn) throws SQLException{ 		
	 
		int status = 501;
		if(conn!=null && !conn.isClosed()){
 
			PreparedStatement query = conn.prepareStatement("INSERT INTO CustOrganization (CustomerProfile_id,email,name,title,startDate,isPrimary) VALUE (?,?,?,?,?,?)");
			
			int i =1;
			query.setLong(i++,    	this.getProfileId());
			query.setString(i++, 	SQLInjector.escape(this.getEmail() ));
			query.setString(i++, 	SQLInjector.escape(this.getName() ));
			query.setString(i++, 	SQLInjector.escape(this.getTitle() ));
			query.setTimestamp(i++,   this.getStartDate()); 
			query.setBoolean(i++,	this.isPrimary() );
			
			status = query.executeUpdate()>0?200:201;
		}
		
		return status;
	}
	
	public int updateOrgan(Connection conn,CustOrganization organ) throws SQLException{
	 
		if(conn !=null && !conn.isClosed() && organ!=null){
			
			//create an update statement
			PreparedStatement query = conn.prepareStatement("UPDATE CustOrganization set title=?,startDate=?,isPrimary=?  WHERE customerProfile_id=? AND name=?"); 		
			
			//add the parameter values to the statement
			int i =1;
			query.setString(i++,SQLInjector.escape( (organ.getTitle()==null? this.getTitle() : organ.getTitle()) ));
			query.setTimestamp(i++, organ.getStartDate());
			query.setBoolean(i++, organ.isPrimary());
 
			//where parameters 
			query.setLong(i++, this.getProfileId());
			query.setString(i++, this.getName());
			
			//execute the update query			
			return query.executeUpdate()>0?200:201; 
			 
		} 
		return 501;
	}
	
	public static int updateOrgan(List<CustOrganization> organs) throws SQLException{
		/*if(organs!=null){
			Connection conn = DCON.getDefaultConnection();
			
			if(conn !=null && !conn.isClosed()){
			
				for(CustOrganization organ:organs){
									
				}
			}
			
		}	*/	
		return -1;
	}
	
	public static CustOrganization findOrganByEmail(Connection conn,String email,String name) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			
			//create select statement
			PreparedStatement selectOrgan = conn.prepareStatement("SELECT * FROM CustOrganization WHERE email=?  and name=?");
			
			//add the parameter values
			int i =1;
			selectOrgan.setString(i++, email); 
			selectOrgan.setString(i++, name);
			selectOrgan.setMaxRows(1);
			//submit the query to the database
			ResultSet result = selectOrgan.executeQuery();
			
			//
			CustOrganization organ = null;		
			while(result.next()) {  
				organ = new CustOrganization();	
				organ.id = result.getLong("id");
				organ.setProfileId(result.getLong("CustomerProfile_id"));
				organ.setEmail(result.getString("email"));
				organ.setName(result.getString("name"));
				organ.setPrimary(result.getBoolean("isPrimary"));
				organ.setTitle(result.getString("title"));
				organ.setStartDate(result.getTimestamp("startDate")); 
				break;
			}
			return organ;
		}
		
		return null;
	}
	
	public static List<CustOrganization> findAll(Connection conn,String email) throws SQLException{
		if(conn!=null && !conn.isClosed()){ 
			//
			List<CustOrganization> organs = new ArrayList<CustOrganization>();
			//create select statement
			PreparedStatement selectOrgan = conn.prepareStatement("SELECT * FROM CustOrganization WHERE email=?"); 
			
			//add the parameter values
			int i =1;
			selectOrgan.setString(i++, email); 			 
			
			//submit the query to the database
			ResultSet result = selectOrgan.executeQuery();
			
			//
			CustOrganization organ = new CustOrganization();
			
			while(result.next()) { 				
				organ.id = result.getLong("id");
				organ.setProfileId(result.getLong("CustomerProfile_id"));
				organ.setEmail(result.getString("email"));
				organ.setName(result.getString("name"));
				organ.setPrimary(result.getBoolean("isPrimary"));
				organ.setTitle(result.getString("title"));
				organ.setStartDate(result.getTimestamp("startDate"));
				organs.add(organ);
			}
			
			return organs;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	//getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean primary) {
		this.isPrimary = primary;
	}
	
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}

	public long getProfileId() {
		return profileId;
	}

	public void setProfileId(long profileId) {
		this.profileId = profileId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
