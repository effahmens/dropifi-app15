package models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import play.data.validation.*;
import play.db.jpa.Model;

import javax.persistence.*;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;

@Embeddable
public class MsgSubject implements Serializable {

	@Column(nullable=false, length=10000) @Required
	@SerializedName("subject")
	private String subject;	
	@Column(nullable=false) @Required
	@SerializedName("activated")
	private Boolean activated;
	
	@SerializedName("created")
	private Timestamp created;
	@SerializedName("updated")
	private Timestamp updated;
	
	@Transient
	@SerializedName("id")
	private String id; 
	
	public MsgSubject(String subject, Boolean enabled){
		this.setSubject(subject);
		this.setActivated(enabled);
	}
	
	//----------------------------------------------------------------------------
	//overridden methods
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MsgSubject other = (MsgSubject) obj;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}
	
	//----------------------------------------------------------------------------------
	//getters and setters
	@Override
	public String toString() {
		return "MsgSubject [subject=" + subject + ", activated=" + activated
				+ ", created=" + created + ", updated=" + updated + ", id="
				+ id + "]";
	}

	public static JsonArray toJsonArray(List<MsgSubject> msgSubjects){
		JsonArray arrayJson = new JsonArray();
		if(msgSubjects != null){
			Gson gson = new Gson();
			ListIterator<MsgSubject> msglist = msgSubjects.listIterator();
			
			int n=1;
			while(msglist.hasNext()){
				MsgSubject msg =msglist.next();
				msg.setId(String.valueOf(n++)); 
				arrayJson.add(gson.toJsonTree(msg)); 
			}
		}
		return arrayJson;
	}
	
	public static List<MsgSubject> getMsgSubject(List<String> listSubjects){
		List<MsgSubject> subjects = new LinkedList<MsgSubject>();
		for(String listSubject : listSubjects){ 
			JsonObject json =(JsonObject) new JsonParser().parse(listSubject);
			subjects.add(new MsgSubject(json.get("subject").getAsString(),json.get("activated").getAsBoolean()));
		}
		return subjects;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject==null?null:subject.trim(); 
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	
	public String getId() {
		return id;
	}
	

	public void setId(String id) {
		this.id = id;
	}
	
}
