package models;

import java.sql.*;
import java.util.*;
import java.util.Date;

import javax.mail.Message;
import javax.mail.MessagingException;

import play.data.validation.*;
import play.db.jpa.Model;
import resources.DCON;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class LastPull extends Model{
	 
	@Required @OneToOne
	private InboundMailbox inboundMb;		 
	private Timestamp receivedDate;	 
	private Timestamp sentDate;
	private Integer msgindex;
	
	public LastPull(InboundMailbox inboundMb, Date receivedDate,Date sentDate,int msgindex){		 
		this.setInboundMb(inboundMb);
		this.setReceivedDate(receivedDate);
		this.setSentDate(sentDate);
		this.setMsgindex(msgindex);
	}
	
	public LastPull(InboundMailbox inboundMb, Long receivedDate,Long sentDate,int msgindex){		 
		this.setInboundMb(inboundMb);
		this.setReceived(receivedDate);
		this.setSent(sentDate);
		this.setMsgindex(msgindex);
	}
	
	public LastPull(InboundMailbox inboundMb, Timestamp receivedDate,Timestamp sentDate,int msgindex){		 
		this.setInboundMb(inboundMb);
		this.setReceived(receivedDate);
		this.setSent(sentDate);
		this.setMsgindex(msgindex);
	}
	
	public LastPull saveLastPull(InboundMailbox mailbox) {
		if(mailbox == null)
			throw new NullPointerException("Failed creating lastPull");
		
		//verify that similar mailbox is not created for 
		LastPull lastPull  = LastPull.findByInboundId(mailbox.getId());
		
		if(lastPull !=null && this != null){
			
			//update lastPull record with the new information
			lastPull.setInboundMb(mailbox); 			 
			lastPull.setReceivedDate(this.receivedDate);				
			lastPull.setSentDate(this.sentDate);
			lastPull.setMsgindex(this.msgindex); 			 
			
			//validate before saving data to database: returns true if all field pass validation test
			if(lastPull.validateAndSave()){
				return lastPull;
			}else{
				return null;
			} 
		}else{
			
			//validate before saving data to database: returns true if all fields pass validation test
			this.setInboundMb(mailbox);
			if(this.validateAndSave()){				
				return this;
			}
			return null; 
		}		
	}
	
	public static LastPull findCreateLastPull(InboundMailbox mailbox, Timestamp receivedDate, Timestamp sentDate , int msgindex){
		try{
			//verify that similar mailbox is not created for 
			LastPull lastPull  = LastPull.findByInboundId(mailbox.getId());
			
			if(lastPull !=null){
				return lastPull;
			}else{ 
				//validate before saving data to database: returns true if all fields pass validation test
				lastPull = new LastPull(mailbox, receivedDate, sentDate, msgindex);
				if(lastPull.validateAndSave()){			
					return lastPull;
				} 
			}	
		}catch(Exception e){ 
		}
		return null; 
	}
	
	public static LastPull findCreateLastPull(Connection conn,InboundMailbox mailbox, Timestamp receivedDate, Timestamp sentDate , int msgindex){
		 
		try{
			//verify that similar mailbox is not created for 
			LastPull lastPull  = LastPull.findByInboundId(conn,mailbox);
			
			if(lastPull !=null){
				return lastPull;
			}else{  
				return createInboundMailbox(conn, mailbox, receivedDate, sentDate, msgindex);
			}	
		}catch(Exception e){ 
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null; 
	}
	
	public static LastPull findByInboundId(Connection conn, InboundMailbox mailbox) {
		LastPull lpull = null;
		try { 
			if(conn != null && !conn.isClosed()){
				PreparedStatement pstate = conn.prepareStatement("Select * from LastPull where inboundMb_id = ?");
				pstate.setLong(1, mailbox.getId()); 
				ResultSet result = pstate.executeQuery();
				
				while(result.next()){
					//
					Long id = result.getLong("id");			 
					Timestamp received = result.getTimestamp("receivedDate");					 
					Timestamp sent = result.getTimestamp("sentDate");
					int msgindex = result.getInt("msgindex");
					lpull = new LastPull(mailbox, received, sent, msgindex);
					lpull.id = id;	
					break;
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			 play.Logger.log4j.info(e.getMessage(),e);
		}
		return lpull;	 
	} 
	
	public static LastPull createInboundMailbox(Connection conn, InboundMailbox mailbox, Timestamp receivedDate, Timestamp sentDate , int msgindex){
		 
		try {
			PreparedStatement query = conn.prepareStatement("Insert into InboundMailbox (inboundMb_id, receiveDate,sentDate,msgindex) Values(?,?,?,?)");
			query.setLong(1, mailbox.getId());
			query.setTimestamp(2, receivedDate);
			query.setTimestamp(3, sentDate);
			query.setInt(4, msgindex);
			
			if(query.executeUpdate()>=0){
				return new LastPull(mailbox, receivedDate, sentDate, msgindex);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	/**
	 * 
	 * @param conn
	 * @param mailbox
	 * @param receivedDate
	 * @param sentDate
	 * @param index
	 * @return
	 * @throws SQLException
	 */
	public static int updateLastPull(Connection conn,Long mb_id, Timestamp receivedDate,Timestamp sentDate,int index) throws SQLException{
		int result = -1; 
		if(conn !=null && !conn.isClosed()){
			PreparedStatement updatelast = conn.prepareStatement("Update LastPull Set receivedDate= ?, sentDate =?, msgindex =? Where inboundMb_id =?");
			updatelast.setTimestamp(1, receivedDate);
			updatelast.setTimestamp(2, sentDate);
			
			//where parameter values
			updatelast.setInt(3, index);
			updatelast.setLong(4, mb_id);
			result = updatelast.executeUpdate();
		} 
		return result;
	}
	
	public static int updateLastPull(Long mb_id, Timestamp receivedDate,Timestamp sentDate,int index) throws SQLException{
		int result = -1;  
		try{
			result = LastPull.em().createQuery("Update LastPull Set receivedDate= ?, sentDate =?, msgindex =? Where inboundMb_id =?")
			.setParameter(1, receivedDate)
			.setParameter(2, sentDate)
			.setParameter(3, index)
			.setParameter(4, mb_id) 
			.executeUpdate();			
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}  
		
		return result;
	}
	
	public static LastPull findByInboundId(Long inbound_id){
		return LastPull.find("byInboundMb_Id", inbound_id).first(); 
	}
				
	public static LastPull findByEmail(Long inboundMb_Id,String email){
		return LastPull.find("byEMailAndInboundMb_id", inboundMb_Id,email).first();
	}
	
	/**
	 * returns the number of messages to be pull;	
	 * @param inboundMb
	 * @param message
	 * @return
	 * @throws MessagingException
	 * @throws SQLException
	 */
	public static int getLast(Connection conn,InboundMailbox mailbox, Message[] message) {		 	
		int startpull= -400;
		try{
			int msgLength= message.length;
			//play.Logger.log4j.info("Last Pull of Messages. "+ msgLength);
			//defines the initial number of messages that should be pulled
			int totalMsg = 2; 
			//Threshold value should be read from the database
			final int THRESHOLD = (msgLength>totalMsg)?totalMsg:msgLength;  
				 
			//return -400 if the requirement for pull inbox message is not met
			if(msgLength>=1){
				Message msg = message[msgLength-1];	 
				Date rtime = msg.getReceivedDate()!=null?msg.getReceivedDate():msg.getSentDate();
				//play.Logger.log4j.info("Date Recieved: "+msg.getSentDate()); 
				Timestamp msgLastdate = new Timestamp(rtime.getTime());
				
				LastPull lastPull = findCreateLastPull(conn,mailbox, msgLastdate, null, -1); 
				
				Timestamp lastdate = lastPull.getReceivedDate();
			
				if(lastdate!= null){ 
					//play.Logger.log4j.info("Date format: DB: "+lastdate+" IN: "+msgLastdate); 
											
					//compute number of back search to be performed 
					int backsearch = (msgLength>=3) ? 3: msgLength;
					
					if(lastPull.msgindex == -1){
						startpull = THRESHOLD; 
					}else if(lastdate.compareTo(msgLastdate) == 0){
						startpull = - 400; 				
					}else if(lastPull.msgindex < msgLength){				
						startpull = (msgLength - lastPull.msgindex);
						//play.Logger.log4j.info("In startpull: "+startpull);
					}else if(lastdate.compareTo(msgLastdate) == -1){				
						int found = backSearchPull(message,lastdate,backsearch);
						//play.Logger.log4j.info("friendly search"+ found);
						startpull = found > -1 ? (msgLength - (found+1)): - 400; 			
					}else{
						//retrieve the last message received date and use to search for the last pool message
						//play.Logger.log4j.info("In binary search");
						int found =  backSearchPull(message,lastdate,backsearch);
						startpull = found > -1 ? (msgLength - (found+1)): - 400; 		
					}
					
				}else{
					startpull = THRESHOLD;
				}	
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		} 
		return startpull; 
	}
	
	/**
	 * returns the number of messages to be pull;	
	 * @param inboundMb
	 * @param message
	 * @return
	 * @throws MessagingException
	 * @throws SQLException
	 */
	public static int getLast(InboundMailbox mailbox, Message[] message) {		 	
		int startpull= -400;
		try{
			int msgLength= message.length;
			//play.Logger.log4j.info("Last Pull of Messages. "+ msgLength);
			//defines the initial number of messages that should be pulled
			int totalMsg = 2; 
			//Threshold value should be read from the database
			final int THRESHOLD = (msgLength>totalMsg)?totalMsg:msgLength;  
				 
			//return -400 if the requirement for pull inbox message is not met
			if(msgLength>=1){
				Message msg = message[msgLength-1];	
				Date rtime = msg.getReceivedDate()!=null?msg.getReceivedDate():msg.getSentDate();
				Timestamp msgLastdate = new Timestamp(rtime.getTime());
				LastPull lastPull = findCreateLastPull(mailbox, msgLastdate, null, -1); 
				//play.Logger.log4j.info("Date Recieved: "+msg.getSentDate()); 
				
				Timestamp lastdate = lastPull.getReceivedDate();
			
				if( lastdate!= null){	
					
					//play.Logger.log4j.info("Date format: DB: "+lastdate+" IN: "+msgLastdate); 
											
					//compute number of back search to be performed 
					int backsearch = (msgLength>=3) ? 3: msgLength;
					
					if(lastPull.msgindex == -1){
						startpull = THRESHOLD; 
					}else if(lastdate.compareTo(msgLastdate) == 0){
						startpull = - 400; 				
					}else if(lastPull.msgindex < msgLength){				
						startpull = (msgLength - lastPull.msgindex);
						//play.Logger.log4j.info("In startpull: "+startpull);
					}else if(lastdate.compareTo(msgLastdate) == -1){				
						int found = backSearchPull(message,lastdate,backsearch);
						//play.Logger.log4j.info("friendly search"+ found);
						startpull = found > -1 ? (msgLength - (found+1)): - 400; 			
					}else{
						//retrieve the last message received date and use to search for the last pool message
						//play.Logger.log4j.info("In binary search");
						int found =  backSearchPull(message,lastdate,backsearch);
						startpull = found > -1 ? (msgLength - (found+1)): - 400; 		
					}
					
				}else{
					startpull = THRESHOLD;
				}	
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		 
		return startpull; 
	}
	
	public static int ternarySearchPull(Message[] a,Date value,int start,int end) 
			throws MessagingException{
		
	    if (start>end)
	        return -1;
	        		
	    //int mid  = ((end-start)/2) + start;
	    
	    int midOne =(start*2+end) / 3; // start+ (end-start) / 3;
	    int midTwo =(start+end*2) / 3;// start + 2*(end-start) / 3;
	    
	    if(a[start].getReceivedDate().compareTo(value)==0)
	    	return start;
	    else if(a[end].getReceivedDate().compareTo(value)==0)
	    	return end;
	    else if (a[midOne].getReceivedDate().compareTo(value)==0) 
	        return midOne;
	    else if (a[midTwo].getReceivedDate().compareTo(value)==0) 
		    return midTwo;
	    else if ( value.compareTo( a[midOne].getReceivedDate()) < 0 ) 
	        return binarySearchPull(a, value, start, midOne-1);
	    else if ( value.compareTo( a[midTwo].getReceivedDate()) >0 ) 
	        return binarySearchPull(a, value, midTwo+1, end);
	    else 
	        return binarySearchPull(a, value, midOne, midTwo);
	      
	}
	
	public static  int binarySearchPull(Message[] a, Date value, int start, int end)  throws MessagingException {
        // Only need to check if the interval got inverted.
        if (start > end) {
            return -1;
        }
        
        // Find the middle:
        int mid = (start + end) / 2;
        
        if(a[start].getReceivedDate().compareTo(value) == 0){
        	return start;
        }
        else if (a[end].getReceivedDate().compareTo(value) ==0) {
            return end;
        }
        else if (a[mid].getReceivedDate().compareTo(value) == 0) {
            return mid;
        }
        else if (value.compareTo(a[mid].getReceivedDate()) < 0) {
            // Search the left half: A[start],...,A[mid-1]
            return binarySearchPull (a, value, start, mid-1);
        }
        else {
            // Search the right half: A[mid+1],...,A[end]
            return binarySearchPull (a, value, mid+1, end);
        }
    }

	/**
	 * BackSearch: iterate a list of messages from last the message till specified number of messages. 
	   For each iteration a date value is compared to the received date of the message. 
	   If the compared date is created, the index of the message is returned 
	 */
	public static int backSearchPull(Message[] arrMsg, Date value,int limit){
		if(value==null)
			return -1;
		
	    int len =	arrMsg.length;
		for(int i =(len-1); i>= (len-limit) ; i--){
			try {
				Message msg = arrMsg[i];
				Date rtime = msg.getReceivedDate()!=null?msg.getReceivedDate():msg.getSentDate();
				if(rtime!=null && value.compareTo(rtime)==1)
					return i;
			} catch (MessagingException e) {
				// TODO Auto-generated catch block				 
			}
		}
		
		return -1;
	}
	
	public static int removeLastPull(Long mailboxId){		
		return LastPull.delete("delete from LastPull where inboundMb_id = ?", mailboxId);
	}
	
	//----------------------------------------------------------------------------------------
	//getters and setters
	
	//last date and time the message was sent and received in the mailbox
	public Timestamp getReceivedDate() {
		 return receivedDate;		 
	}

	public void setReceivedDate(Date receivedDate) {
		if(receivedDate != null)
			this.receivedDate = new Timestamp(receivedDate.getTime());
		this.receivedDate = null;
	}

	public void setReceived(Timestamp receivedDate) {
		this.receivedDate =  receivedDate;
	}
	
	public void setReceived(Long receivedDate) {
		this.receivedDate = new Timestamp(receivedDate) ;
	}
	
	public Timestamp getSentDate() {
		return  sentDate;
	}

	public void setSentDate(Date sentDate) {
		if(sentDate !=null)
			this.sentDate = new Timestamp(sentDate.getTime());
		this.sentDate = null;
	}
	
	public void setSent(Timestamp sentDate) {
		this.sentDate = sentDate;
	}
	
	public void setSent(Long sentDate) {
		this.sentDate = new Timestamp(sentDate);
	} 
	
	public InboundMailbox getInboundMb() {
		return inboundMb;
	}

	public void setInboundMb(InboundMailbox inboundMb) {
		this.inboundMb = inboundMb;
	}

	public Integer getMsgindex() {
		return msgindex;
	}

	public void setMsgindex(Integer msgindex) {
		this.msgindex = msgindex;
	}

}
