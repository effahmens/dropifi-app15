package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;

import play.db.jpa.Model;
import play.db.jpa.Transactional;
import play.mvc.Before;

@Entity
public class DMessageLocation extends Model{
	@Column(unique=true, updatable=false)
	private String msgId;
	
	//companyId	
	@Index(name="companyId") @Column(nullable=false)
	private Long companyId;
	
	@Index(name="apikey") @Column(nullable=false)
	private String apikey;
	@Index(name="email")
	private String email; 
	
	@Embedded
	private GeoLocation location;
	
	
	
	public DMessageLocation(String msgId, Long companyId, String apikey,
			String email, GeoLocation location) {
		super();
		this.msgId = msgId;
		this.companyId = companyId;
		this.apikey = apikey;
		this.email = email;
		this.location = location;
	}
	
	public DMessageLocation(){}
	
	@Transactional
	public void AddUpdate(){
		Date date = new Date(); 
		this.getLocation().setCreated(new Timestamp(date.getTime()));
		this.save();
	}
	
	public void saveLoaction(Connection conn){
		try{
 
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Insert into DMessageLocation (msgId,companyId,apikey,email,city,region,regionName,postalCode,countryCode,countryName,latitude,longitude,ipAddress,created) " +
						" value(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				
				int i =1;
				query.setString(i++, this.getMsgId());
				query.setLong(i++, this.getCompanyId());
				query.setString(i++, this.getApikey());
				query.setString(i++, this.getEmail());
				query.setString(i++, this.getLocation().getCity());
				query.setString(i++, this.getLocation().getRegion());
				query.setString(i++, this.getLocation().getRegionName());
				query.setString(i++, this.getLocation().getPostalCode());
				query.setString(i++, this.getLocation().getCountryCode());
				query.setString(i++, this.getLocation().getCountryName());
				query.setDouble(i++, this.getLocation().getLatitude());
				query.setDouble(i++, this.getLocation().getLongitude());
				query.setString(i++, this.getLocation().getIpAddress());
				this.getLocation().setCreated(new Timestamp(new Date().getTime()));
				query.setTimestamp(i++, this.getLocation().getCreated());
				
				query.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
	} 
	
	public static List<DMessageLocation> findByEmail(String apikey,String email){
		return CustomerLocation.find("Select l From DMessageLocation l Where l.apikey=? and l.email=?", apikey,email).fetch();
	}
	
	public static DMessageLocation findByMsgId(String apikey,String email,String msgId){
		return CustomerLocation.find("Select l From DMessageLocation l Where l.apikey=? and l.email=? and l.msgId=?", apikey,email,msgId).first();
	}
	
	public static GeoLocation findLocation(String apikey,String email){
		return CustomerLocation.find("Select l.location From DMessageLocation l Where l.apikey=? and l.email=?", apikey,email).first();
	}
	
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public GeoLocation getLocation() {
		return location;
	}
	public void setLocation(GeoLocation location) {
		this.location = location; 
	}
}
