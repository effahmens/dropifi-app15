package models;

import java.io.Serializable;

import play.data.validation.*;
import play.db.jpa.Model;
import resources.SQLInjector;

import javax.persistence.*;

import com.google.gson.annotations.SerializedName;

@Embeddable 
public class RuleCondition implements Serializable{

	@SerializedName("condition")
	private String msgCondition;
	@SerializedName("comparison") 
	private String msgComparison;	 
	@SerializedName("expectation")
	private String msgExpectation;
		
	public RuleCondition(String condition, String comparison, String expectation){		 
		this.setCondition(condition);
		this.setComparison(comparison);
		this.setExpectation(expectation); 
	} 
	
	public RuleCondition(){	} 

	//---------------------------------------------------------------------------------------------
	//getters and setters
	
	
	public String getCondition() {
		return msgCondition;
	}

	@Override
	public String toString() {
		return "RuleCondition [msgCondition=" + msgCondition
				+ ", msgComparison=" + msgComparison + ", msgExpectation="
				+ msgExpectation + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((msgComparison == null) ? 0 : msgComparison.hashCode());
		result = prime * result
				+ ((msgCondition == null) ? 0 : msgCondition.hashCode());
		result = prime * result
				+ ((msgExpectation == null) ? 0 : msgExpectation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleCondition other = (RuleCondition) obj;
		if (msgComparison == null) {
			if (other.msgComparison != null)
				return false;
		} else if (!msgComparison.equals(other.msgComparison))
			return false;
		if (msgCondition == null) {
			if (other.msgCondition != null)
				return false;
		} else if (!msgCondition.equals(other.msgCondition))
			return false;
		if (msgExpectation == null) {
			if (other.msgExpectation != null)
				return false;
		} else if (!msgExpectation.equals(other.msgExpectation))
			return false;
		return true;
	}

	public void setCondition(String condition) {
		this.msgCondition = condition!=null?condition:"";
	}

	public String getComparison() {
		return msgComparison;
	}

	public void setComparison(String comparer) {
		this.msgComparison = comparer!=null?comparer:"";
	}

	public String getExpectation() {
		return msgExpectation;
	}

	public void setExpectation(String expectation) {
		this.msgExpectation = expectation!=null?expectation:"";
	}

}
