package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;

import play.db.jpa.Model;
import resources.DCON;
import resources.SQLInjector;
import resources.SentimentType;

@Entity
public class MsgKeyword extends Model{
	
	@Index(name="companyId")@Column(nullable=false)
	private Long companyId;
	@Index(name="messageId") @Column(nullable=false)
	private Long messageId;
	@Column(nullable=false, length=10000)
	private String keyword;
	@Column(nullable=false)
	private SentimentType sentiment;
	private Timestamp createdDate;
	@Index(name="sourceOfMsg")
	private String sourceOfMsg;
	
	public MsgKeyword(Long companyId, Long messageId, String keyword, SentimentType sentiment,
			Timestamp createdDate,String sourceofMsg) {
		
		this.setCompanyId(companyId);
		this.setMessageId(messageId);
		this.setKeyword(keyword);
		this.setSentiment(sentiment);
		this.setCreatedDate(createdDate);
		this.setSourceOfMsg(sourceofMsg);
	}

	//-----------------------------------------------------------------------------------------------------
	//defined methods
		
	public static int createKeyword(Connection conn,List<MsgKeyword> keywords) throws SQLException{
		int status = 501; 
		
		if(keywords!=null){
			 			
			if(conn!=null && !conn.isClosed()) {
				 
				for(MsgKeyword keyword : keywords){
					if(keyword!=null && keyword.getCompanyId()!=null && keyword.getMessageId()!=null && keyword.getKeyword()!=null && keyword.getSentiment()!=null){
						try{
							int i=1;						
							PreparedStatement query = conn.prepareStatement("Insert into MsgKeyword (companyId, messageId, keyword,sentiment,sourceOfMsg,createdDate) VALUE (?,?,?,?,?,?) ");						
							
							query.setLong(i++, keyword.getCompanyId());
							query.setLong(i++, keyword.getMessageId());
							query.setString(i++, SQLInjector.escape(keyword.getKeyword()));
							query.setInt(i++, keyword.getSentiment().ordinal());						 
							query.setString(i++,SQLInjector.escape(keyword.getSourceOfMsg()));
							query.setTimestamp(i++, keyword.getCreatedDate());
							 
							//add query to batch						 
							status = query.executeUpdate()> -1 ? 200:201; 
							//play.Logger.log4j.info("Keyword added: "+status+"Size: "+keywords.size());	
						}catch(Exception e){
							play.Logger.log4j.info(e.getMessage(),e);
						}
					}	
				}				
			}
		} 
		return status;
	}
	
	public static int createKeywords(Connection conn,Long companyId,Long messageId,String message, SentimentType sentiment, String sourceOfMsg, Timestamp createdDate) throws SQLException{
		List<String> keywords = findAllSearchKeywords(conn,companyId); 
		 
		if(keywords!=null){
			List<MsgKeyword> msgKeyword = new LinkedList<MsgKeyword>();
			
			for(String keyword:keywords){
				message = message.toLowerCase();
				keyword = keyword.toLowerCase(); 
				if(message.contains(keyword)){
					msgKeyword.add(new MsgKeyword(companyId, messageId, keyword, sentiment, createdDate, sourceOfMsg)); 
				}
			}
			return createKeyword(conn,msgKeyword);
		}		
		return 501;
	}
	
	public static List<String> findAllSearchKeywords(Connection conn,Long companyId) throws SQLException{ 
		List<String> keywords = new LinkedList<String>();
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("SELECT keyword FROM SearchKeywords  WHERE companyId=? and activated = ?");
			
			query.setLong(1, companyId);
			query.setBoolean(2, true);		
			ResultSet result =  query.executeQuery();
			
			while(result.next()){
				 keywords.add(result.getString("keyword"));
			}			
		} 		
		return keywords;
	}
	
	//------------------------------------------------------------------------------------------------------
	//getters and setters

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public SentimentType getSentiment() {
		return sentiment;
	}

	public void setSentiment(SentimentType sentiment) {
		this.sentiment = sentiment;
	}

	
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getSourceOfMsg() {
		return sourceOfMsg;
	}

	public void setSourceOfMsg(String sourceOfMsg) {
		this.sourceOfMsg = sourceOfMsg;
	}
	
		
}
