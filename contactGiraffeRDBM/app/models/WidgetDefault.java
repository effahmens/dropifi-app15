/**
 * @author phillips effah mensah
 * @description 
 * @date 14/3/2012
 * 
 **/

package models;

import java.awt.Color;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*; 

import play.Play;
import play.cache.Cache;
import play.data.validation.*;
import play.db.jpa.JPA;
import play.db.jpa.Model;
import play.libs.Codec;
import resources.CacheKey;
import resources.DCON;
import resources.DropifiTools;
import resources.EcomBlogType;
import resources.SQLInjector;
import resources.WidgetControlType;
import resources.WidgetField;
import resources.helper.ServiceSerializer;
import view_serializers.WCParameter;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.persistence.*;

import models.dsubscription.SubConsumeService;
import models.dsubscription.SubServiceName;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Index;

import com.google.gson.annotations.SerializedName;
import com.redis.Msg;

@Entity
public class WidgetDefault extends Model{
	
	//-------------------------------------------------------------------------------------------------------- 
	//instance variables
	@Column(nullable=false, unique=true)  
	private String apikey; 
	@Column(length=100000)
	private String html; 
	 @Index(name="publicKey") @Column(nullable=false)
	private String publicKey;
	@Embedded
	private WiParameter wiParameter;
	 
	private String socialUID;
	//--------------------------------------------------------------------------------------------------------
	//constructor
	
	@Transient
	private Widget widget;
	
	public WidgetDefault(Widget widget) {	
		this.setApikey(widget.getApikey());
		this.setWiParameter(widget.getWiParameter());
		this.widget = widget;
	}
	
	public WidgetDefault(String apikey,String publicKey){
		this.setApikey(apikey);
		this.setPublicApikey(publicKey);
	}
	
	//-------------------------------------------------------------------------------------------------------
	//create the widget
	
	private void clearCache(){
		//delete the old value in cache
		CacheKey.deleteCache(CacheKey.getWidgetContentKey(this.getPublicKey()));
		CacheKey.deleteCache(CacheKey.getWidgetTabKey(this.getPublicKey()));
		CacheKey.deleteCache(CacheKey.genCacheKey(this.getPublicKey(), CacheKey.Widget, "HtmlContent"));
		CacheKey.deleteCache(CacheKey.genCacheKey(this.getPublicKey(), CacheKey.Widget,"HasCaptcha"));

	} 
	
	public WidgetDefault createWidgetDefault(){	
		if(this.widget !=null){	 
			clearCache();
			this.setPublicApikey(this.widget.getApikey()); 
			this.setHtml(this.generateHtmlTwo(this.widget));
			return this.save()!=null ?this:null;
		}
		return null;
	}
	
	
	
	public WidgetDefault updateWidgetDefault(Widget widget){ 
		try {
			if(widget!=null) {
				clearCache();	
				this.setWiParameter(widget.getWiParameter());
				this.setHtml(this.generateHtmlTwo(widget)); 
				return this.save();
			}
		}catch(Exception e) { 
		}
		return this;
	}
	
	public void updateSocialUID(String socialUID){
		try {
			clearCache();
			this.setSocialUID(socialUID);
			this.save();
		}catch(Exception e) { 
		}
	}
	
	public void updateHtml(Widget widget){
		try {
			if(widget!=null) {
				clearCache();
				this.setHtml(this.generateHtmlTwo(widget));
				this.save();
			}
		}catch(Exception e) {
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static void clearCache(String publicKey){
		//delete the old value in cache
		CacheKey.deleteCache(CacheKey.getWidgetContentKey(publicKey));
		CacheKey.deleteCache(CacheKey.getWidgetTabKey(publicKey));
		CacheKey.deleteCache(CacheKey.genCacheKey(publicKey, CacheKey.Widget, "HtmlContent"));
		CacheKey.deleteCache(CacheKey.genCacheKey(publicKey, CacheKey.Widget,"HasCaptcha"));

	}
	
	public static int updateHtml(String publicKey, String html){
		Connection conn =null;
		try {
			 conn = DCON.getDefaultConnection();
			 PreparedStatement query =conn.prepareStatement("Update WidgetDefault w Set w.html = ? Where w.publicKey =?");
			 query.setString(1, html);
			 query.setString(2,publicKey);
			 int n=  query.executeUpdate();
			 clearCache(publicKey) ;
			 return n;
		}catch(Exception e) {
			play.Logger.log4j.info(e.getMessage(),e);
		}finally {
			if(conn!=null) {
				
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return -1;
	}
	
	@Deprecated
  	public String generateHtml(Widget widget){
  		
		StringBuilder widHtml = new StringBuilder();
		if(widget!=null && widget.getApikey()!= null && this.getPublicKey() !=null ){
			ServiceSerializer service = CacheKey.findServiceSerializerBrand(this.getApikey(),SubServiceName.Branding);
			
			String bfont = this.getWiParameter().getButtonFont();
			bfont = bfont.replace("SansSerif", "sans-serif");
			String font = "style='font-family:"+bfont+" !important;'";
			
			//create a title tag
			widHtml.append("<div class='cheetahWidget2012_holder' "+font+"> <div class='cheetahWidget2012_disclaimer'>");
			//add the title of the widget
			widHtml.append( widget.getTitle()); 
			widHtml.append("</div> <div id='cheetahWidget2012_callback'> </div> <div id='cheetahWidget2012_content'> <form id='cheetahWidget2012_contactForm_sub'>");
			
			//---------------------------------------------------------------------
			//create the container for the various widget fields
			
			Iterator<WidgetControl> controls = widget.getWidgetControls().iterator();
			WidgetControl attControl = null;
			while(controls.hasNext()){
				WidgetControl con = controls.next();
				if(con.getActivated() ){
					widHtml.append("<div class='cheetahWidget2012_form_input'>");
					
					switch(con.getType().ordinal()){
						case  7: //subject text field
							if(con.getCondId().equals(WidgetField.subject)){
								
								//Only add list of dropdown subjects to paid users
								//if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free")){
									MessageManager manager = MessageManager.findByApikey(apikey); 
									if(manager !=null){
										Collection<MsgSubject> subjects = manager.getSubjects();
										if(subjects!=null && subjects.size()>0){
											Iterator<MsgSubject>listSubject = subjects.iterator();
											StringBuilder subBuild = new StringBuilder();
											
											subBuild.append("<div id='cheetahWidget2012_dropdown'>");
											subBuild.append("<img width='16' height='16' src='https://s3.amazonaws.com/dropifi/images/widget/client/contact_subject.png'>");
											subBuild.append("<select "+font+" class='cheetahWidget2012_subject' id='cheetahWidgetField_"+con.getCondId()+"' name ='"+con.getCondId().toString()+"' >");
											int counter=0;
											while(listSubject.hasNext()){
												MsgSubject sub = listSubject.next();
												
												//check if the subject is activated
												if(sub.getActivated()){
													subBuild.append("<option value='"+sub.getSubject()+"' id='opt"+sub.getSubject()+"'>"+sub.getSubject()+"</option>");
													counter++;
												}
											}
											subBuild.append("</select></div>");
											
											if(counter>0){
												widHtml.append(subBuild);
												break;
											}
										}
									} 
								//}
							} 
							String className ="cheetahWidget2012_"; 
							if(con.getCondId().equals(WidgetField.name)){
								className+="name";
								widHtml.append("<span id ='cheetahWidgetError_name' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							}else if(con.getCondId().equals(WidgetField.subject)){
								className+="subject";
								widHtml.append("<span id ='cheetahWidgetError_subject' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							}else if(con.getCondId().equals(WidgetField.phone)){
								className+="phone";
								widHtml.append("<span id ='cheetahWidgetError_phone' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />"); 
							}
							
							widHtml.append("<input "+font+" class='"+className+" widget_clear_error' type='text' id='cheetahWidgetField_"+con.getCondId()+"' name='"+con.getCondId().toString()+"' placeholder =\""+ con.getEscapeTitle() +"\" />");
			
							break;
						case  2: //email field 
							widHtml.append("<span id ='cheetahWidgetError_email' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							widHtml.append("<input "+font+" class='cheetahWidget2012_email widget_clear_error' type='text' id='cheetahWidgetField_"+con.getCondId()+"' name='"+con.getCondId().toString()+"' placeholder =\""+con.getEscapeTitle()+"\" />");
							break;
						case  8: //message text area  					
							widHtml.append("<span id ='cheetahWidgetError_message' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							widHtml.append("<div id='cheetahWidget2012_div_textarea'>");
							widHtml.append("<p>"+con.getTitle()+"</p> <textarea "+font+" class='widget_clear_error' placeholder='...' id='cheetahWidgetField_"+con.getCondId()+"' name='"+con.getCondId().toString()+"'></textarea>");
							
							//if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free") && !service.getAccountType().equals("FreeTrial")){
								//search for attachment
								Iterator<WidgetControl> attControls = controls;
								while(attControls.hasNext()){
									WidgetControl att = attControls.next();  
									if(att.getActivated() && att.getCondId().equals(WidgetField.attachment)){
										attControl = att; 
										break;
									}
								}
							//} 
							
							widHtml.append("</div>"); 		 
							break;
						case  0://file field
							break;
					} 		
					
					widHtml.append("</div>"); 
				}
			}
			 
			widHtml.append("</form>");
			if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free") && !service.getAccountType().equals("Starter")){
				if(attControl!=null){
					widHtml.append("<form name='fileupload' id='fileupload' "+font+" class='cheetahWidget2012_form_input' action='/clientswidgets/uploadwidgetfile' method='post' enctype='multipart/form-data'>");
					widHtml.append("<span><img width='13' height='13' src='https://s3.amazonaws.com/dropifi/images/widget/client/contact_attachment.png'>"+attControl.getTitle()+"</span>");
					widHtml.append("<span id ='cheetahWidgetError_attachment' class = 'cheetahWidgetError_field'></span>");
					widHtml.append("<span id='cheetahWidgetField_fileContent'><input "+font+" type='file' id='cheetahWidgetField_attachment' name ='uploadfile' maxlength = '1' /></span><span id='cheetahWidgetField_attachment_close'>x</span>"); 
					widHtml.append("<input type='hidden' id='url' name='url' value='/clientswidgets/uploadwidgetfile' />");
					widHtml.append("<input type='hidden' id='attachmentId' name='attachmentId' />");
					widHtml.append("<input type='hidden' id='sender' name='sender' />");
					widHtml.append("<input type='hidden' id='publickey' name='publickey' value='"+this.getPublicKey()+"' />");
					widHtml.append("<span style='display:none !important' id='droppedfiles'></span>");
					widHtml.append("<input type='submit' name='submit' id='submitAttachment' value='Upload Files'/>");
					widHtml.append("</form>");
				}
			}
			 
			widHtml.append("<div class='cheetahWidget2012_submit_input' "+font+" >");
			
			
			if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free")){
				if(widget.getHasCaptcha()!=null && widget.getHasCaptcha()==true) {
					if(widget.getCaptchaValue()==null || widget.getCaptchaValue().trim().isEmpty())
						widget.setCaptchaValue("What's the result of");
					widHtml.append("<p class='cheetahWidget2012_captcha'>"+widget.getCaptchaValue()+"<span class='cheetahWidget2012_captcha_input'></span></p><p <span id ='cheetahWidgetError_captcha' class = 'cheetahWidgetError_field'>></p>");
				}
			}else {
				if(widget.getHasCaptcha() == null || widget.getHasCaptcha()==true) 
					widget.updateHasCaptcha(false);
			}
			 			
			String key = this.getPublicKey();
			String sendColor= widget.getSendButtonColor();
			String sendText = widget.getSendButtonText();
			if(widget.getSendButtonColor()==null){
				sendColor = "#1E7BB8";
			}
			
			if( widget.getSendButtonText()==null){
				sendText ="Send Message";
			}
			
			String shadow = DropifiTools.decreaseColor(widget.getSendButtonColor());
			
			//check if the send button background color is dark or light
			//set the font color to white if dark else black
			Color fcolor = DropifiTools.isBrighten(sendColor)?Color.BLACK:Color.WHITE;
			String fontColor =Integer.toHexString(fcolor.getRGB()).replaceFirst("ff","#");
			widHtml.append("<input "+font+" type='button' ref_fontColor='"+fontColor+"' ref_shadow='"+shadow+"' ref_color='"+sendColor+"' class='cheetahWidget2012_button' value='"+sendText+"' id='btnSave' onclick=\"javascript:doSaveCheetahWidget('"+key+"')\" /></div>"); 
			
			//----------------------------------------------------------------------
			widHtml.append("</div></div>"); 
			//add dropifi Branding			
			if(service==null || service.hasExceeded){
				widHtml.append(" <div id='cheetahWidget2012_footer'><a style='text-decoration:none;padding-top:5px;' target='_blank' href='https://www.dropifi.com'><img id='dropifiWidget_powered_by_dropifi' src='https://s3.amazonaws.com/dropifi/images/powered_by_dropifi.png'/></a> </div>");
			}else{
				widHtml.append(" <div id='cheetahWidget2012_footer'><a style='text-decoration:none;padding-top:5px;' target='_blank' href='https://www.dropifi.com'></a> </div>");
			}
		}
		//return the html version of the widget if save is successful 		
		return widHtml == null ? "":  widHtml.toString();
	}
	
	public String generateHtmlTwo(Widget widget) {
		return generateHtmlTwo(widget,this.getPublicKey(),this.getApikey());
	}
  	
  	public static String generateHtmlTwo(Widget widget,String publicKey,String apikey) {
  		StringBuilder widHtml = new StringBuilder();
		if(widget!=null && widget.getApikey()!= null && publicKey !=null ){
			ServiceSerializer service = CacheKey.findServiceSerializerBrand(apikey,SubServiceName.Branding);
			
			widHtml.append("<div id='formContent'>"); 
			//widHtml.append("<form action='/clientmessenger/widget_iframe_sending_message' method='POST' enctype='multipart/form-data'>");
			
			widHtml.append("<p id='header'>"+widget.getHeader()+"</p>");
            
			Iterator<WidgetControl> controls = widget.getWidgetControls().iterator();
			//WidgetControl attControl = null;
			while(controls.hasNext()){
				WidgetControl con = controls.next();
				if(con.getActivated() ){ 
					switch(con.getType().ordinal()){
						case  7: //subject text field
							if(con.getCondId().equals(WidgetField.subject)){
								
								//Only add list of dropdown subjects to paid users
								//if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free")){
									MessageManager manager = MessageManager.findByApikey(apikey); 
									if(manager !=null){
										Collection<MsgSubject> subjects = manager.getSubjects();
										if(subjects!=null && subjects.size()>0){
											Iterator<MsgSubject>listSubject = subjects.iterator();
											StringBuilder subBuild = new StringBuilder(); 
											
											subBuild.append("<p class='input-group'> <span class='input-group-addon glyphicon glyphicon-pencil'></span>");
											subBuild.append("<select id='widgetField_"+con.getCondId()+"' name='cs."+con.getCondId().toString()+"' class='form-control' required>"); 
											
											int counter=0;
											while(listSubject.hasNext()){
												MsgSubject sub = listSubject.next(); 
												//check if the subject is activated
												if(sub.getActivated()){
													subBuild.append("<option value='"+sub.getSubject()+"' id='opt"+sub.getSubject()+"'>"+sub.getSubject()+"</option>");
													counter++;
												}
											}
											subBuild.append("</select>");
											subBuild.append("<span style='display:none' id='widgetError_"+con.getCondId()+"'>"+con.getErrorMsg()+"</span> </p>");
											
											if(counter>0){
												widHtml.append(subBuild);
												break;
											}
										}
									}
							}
							
							if(con.getCondId().equals(WidgetField.name)){
								widHtml.append("<p class='input-group'><span class='input-group-addon glyphicon glyphicon-user'></span>"); 
								widHtml.append("<input id='widgetField_"+con.getCondId()+"' type='text' name='cs."+con.getCondId().toString()+"' class='form-control' placeholder = \""+ con.getEscapeTitle() +"\" required>");
							}else if(con.getCondId().equals(WidgetField.subject)){
								widHtml.append("<p class='input-group'><span class='input-group-addon glyphicon glyphicon-pencil'></span>");
								widHtml.append("<input id='widgetField_"+con.getCondId()+"' type='text' name='cs."+con.getCondId().toString()+"' class='form-control' placeholder = \""+ con.getEscapeTitle() +"\" required>");
							}else if(con.getCondId().equals(WidgetField.phone)){
								widHtml.append("<p class='input-group'><span class='input-group-addon glyphicon glyphicon-phone'></span>");
								widHtml.append("<input id='widgetField_"+con.getCondId()+"' type='text' name='cs."+con.getCondId().toString()+"' class='form-control' placeholder = \""+ con.getEscapeTitle() +"\" >");
							}
							widHtml.append("<span style='display:none' id='widgetError_"+con.getCondId()+"'>"+con.getErrorMsg()+"</span> </p>");
							//widHwidHtmltml.append("<input type='text' id='widgetField_"+con.getCondId()+"' name='"+con.getCondId().toString()+"' placeholder =\""+ con.getEscapeTitle() +"\" />");
							break;
						case  2: //email field 
							widHtml.append("<p class='input-group'><span class='input-group-addon glyphicon glyphicon-envelope'></span>");
							widHtml.append("<input id='widgetField_"+con.getCondId()+"' type='email' name='cs."+con.getCondId().toString()+"' class='form-control' placeholder = \""+ con.getEscapeTitle() +"\" required>");
							widHtml.append("<span style='display:none' id='widgetError_"+con.getCondId()+"'>"+con.getErrorMsg()+"</span> </p>");
							break;
						case  8:
							widHtml.append("<p> <textarea id='widgetField_"+con.getCondId()+"' name='cs."+con.getCondId().toString()+"' rows='3' class='form-control' cols='20' placeholder = \""+ con.getEscapeTitle() +"\" required></textarea>");
							widHtml.append("<span style='display:none' id='widgetError_"+con.getCondId()+"'>"+con.getErrorMsg()+"</span> </p>");
							break;
						case  0:
							if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free") && !service.getAccountType().equals("Starter")){
								widHtml.append("<p><span id='fileContent'><input id='widgetField_"+con.getCondId()+"' name='uploadfile' type='file' /></span> <span id='clearfile' title='remove file'>x</span></p>");
								widHtml.append("<input type='hidden' id='attachmentId' name='cs.attachmentId' value=''/>");
								widHtml.append("<input type='hidden' id='fileType' name='fileType' value=''/>");
							}
							break;
					}  
				}
			}
			
			try {
				//Add captcha to the form
				if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free")){
					if(widget.getHasCaptcha()!=null && widget.getHasCaptcha()==true) {
						if(widget.getCaptchaValue() ==null || widget.getCaptchaValue().trim().isEmpty())
							widget.setCaptchaValue("What's the result of");
						
						widHtml.append("<p><span>"+widget.getCaptchaValue()+" </span><span id='captchaText'></span><input id = 'captchaResult' name='cs.captchaResult' type= 'text' class='form-control' required></p>"); 
						widHtml.append("<input type='hidden' id='captchaCode' name ='cs.captchaCode'>");
					}
				}else { 
					if(widget.getId()!=null && ( widget.getHasCaptcha()==null || widget.getHasCaptcha() == true)) {
						try {
							widget.updateHasCaptcha(false);
						}catch(Exception e) {
						}
					}
				} 
			}catch(Exception e) {
				play.Logger.log4j.info(e.getMessage(),e);
			}
			
			String sendColor= widget.getSendButtonColor();
			String sendText = widget.getSendButtonText();
			if(widget.getSendButtonColor()==null){
				sendColor = "#1E7BB8";
			}
			
			if( widget.getSendButtonText()==null){
				sendText ="Send Message";
			}
			
			//String shadow = DropifiTools.decreaseColor(widget.getSendButtonColor()); 
			//check if the send button background color is dark or light
			//set the font color to white if dark else black
			Color fcolor = DropifiTools.isBrighten(sendColor)?Color.BLACK:Color.WHITE;
			String fontColor =Integer.toHexString(fcolor.getRGB()).replaceFirst("ff","#"); 
			
			//Add hidden values;
			widHtml.append("<input type='hidden' id='publicKey' name='publicKey' value='"+publicKey+"' />");
			widHtml.append("<input type='hidden' id='ipAddress' name='location.ipAddress' value=''/>");
			widHtml.append("<input type='hidden' id='pageUrl' name='cs.pageUrl' value=''/>");
			widHtml.append("<input type='hidden' id='isPreview' name='isPreview' value='false'/>");
			
			if(publicKey!=null && !publicKey.trim().isEmpty()) {
				widHtml.append("<p class='dropifi_submit_input'> <input id='sendMail' style='background-color:"+sendColor+";color:"+fontColor+";border-color:"+
						sendColor+"' class='btn btn-primary' name='sendMail' value='"+sendText+"' type='submit'> </p>");
			}else { 
				widHtml.append("<p class='dropifi_submit_input'>Connect to a Dropifi account via the Wix setting page, to start sending messages</p>");
			}
			
			//widHtml.append("<p style='display:none !important'><input style='display:none !important' type='submit' name='submit' id='submitForm'/></p>");
			
			//widHtml.append("</form>");
			
			//add dropifi Branding			
			if(service==null || service.hasExceeded){
				widHtml.append("<p id='poweredBy'> <a target='_blank' href='//www.dropifi.com'>Powered By Dropifi</a></p>");
			}
			widHtml.append("</div>"); 
		}
		
		//return the html version of the widget if save is successful 		
		return widHtml == null ? "":  widHtml.toString();
  	}
  	
  	@Deprecated
  	public String generateHtml(PreviewWidget widget){
  		 
		StringBuilder widHtml = new StringBuilder();
		if(widget!=null && widget.getApikey()!= null && this.getPublicKey() !=null ){
			ServiceSerializer service = CacheKey.findServiceSerializerBrand(this.getApikey(),SubServiceName.Branding);
			
			String bfont = this.getWiParameter().getButtonFont();
			bfont = bfont.replace("SansSerif", "sans-serif");
			String font = "style='font-family:"+bfont+" !important;'";
			
			//create a title tag
			widHtml.append("<div class='cheetahWidget2012_holder' "+font+"> <div class='cheetahWidget2012_disclaimer'>");
			//add the title of the widget
			widHtml.append( widget.getTitle()); 
			widHtml.append("</div> <div id='cheetahWidget2012_callback'> </div> <div id='cheetahWidget2012_content'> <form id='cheetahWidget2012_contactForm_sub'>");
			
			//---------------------------------------------------------------------
			//create the container for the various widget fields
			
			Iterator<WidgetControl> controls = widget.getWidgetControls().iterator();
			WidgetControl attControl = null;
			while(controls.hasNext()){
				WidgetControl con = controls.next();
				if(con.getActivated() ){
					widHtml.append("<div class='cheetahWidget2012_form_input'>");
					
					switch(con.getType().ordinal()){
						case  7: //subject text field
							if(con.getCondId().equals(WidgetField.subject)){ 
								MessageManager manager = MessageManager.findByApikey(apikey);  
								if(manager !=null){
									Collection<MsgSubject> subjects = manager.getSubjects();
									if(subjects!=null && subjects.size()>0){
										Iterator<MsgSubject>listSubject = subjects.iterator();
										StringBuilder subBuild = new StringBuilder();
										
										subBuild.append("<div id='cheetahWidget2012_dropdown'>");
										subBuild.append("<img width='16' height='16' src='https://s3.amazonaws.com/dropifi/images/widget/client/contact_subject.png'>");
										subBuild.append("<select "+font+" class='cheetahWidget2012_subject' id='cheetahWidgetField_"+con.getCondId()+"' name ='"+con.getCondId().toString()+"' >");
										int counter=0;
										while(listSubject.hasNext()){
											MsgSubject sub = listSubject.next();
											
											//check if the subject is activated
											if(sub.getActivated()){
												subBuild.append("<option value='"+sub.getSubject()+"' id='opt"+sub.getSubject()+"'>"+sub.getSubject()+"</option>");
												counter++;
											}
										}
										subBuild.append("</select></div>");
										
										if(counter>0){
											widHtml.append(subBuild);
											break;
										}
									}
								}  
							}
							
							String className ="cheetahWidget2012_"; 
							if(con.getCondId().equals(WidgetField.name)){
								className+="name";
								widHtml.append("<span id ='cheetahWidgetError_name' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							}else if(con.getCondId().equals(WidgetField.subject)){
								className+="subject";
								widHtml.append("<span id ='cheetahWidgetError_subject' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							}else if(con.getCondId().equals(WidgetField.phone)){
								className+="phone";
								widHtml.append("<span id ='cheetahWidgetError_phone' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />"); 
							}
							
							widHtml.append("<input "+font+" class='"+className+" widget_clear_error' type='text' id='cheetahWidgetField_"+con.getCondId()+"' name='"+con.getCondId().toString()+"' placeholder =\""+ con.getEscapeTitle() +"\" />");
			
							break;
						case  2: //email field 
							widHtml.append("<span id ='cheetahWidgetError_email' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							widHtml.append("<input "+font+" class='cheetahWidget2012_email widget_clear_error' type='text' id='cheetahWidgetField_"+con.getCondId()+"' name='"+con.getCondId().toString()+"' placeholder =\""+con.getEscapeTitle()+"\" />");
							break;
						case  8: //message text area  					
							widHtml.append("<span id ='cheetahWidgetError_message' class = 'cheetahWidgetError_field'></span><input type='hidden' id='e"+con.getCondId()+"' value='"+con.getErrorMsg()+"' />");
							widHtml.append("<div id='cheetahWidget2012_div_textarea'>");
							widHtml.append("<p>"+con.getTitle()+"</p> <textarea "+font+" class='widget_clear_error' placeholder='...' id='cheetahWidgetField_"+con.getCondId()+"' name='"+con.getCondId().toString()+"'></textarea>");
							
							//if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free") && !service.getAccountType().equals("FreeTrial")){
								//search for attachment
								Iterator<WidgetControl> attControls = controls;
								while(attControls.hasNext()){
									WidgetControl att = attControls.next();  
									if(att.getActivated() && att.getCondId().equals(WidgetField.attachment)){
										attControl = att; 
										break;
									}
								}
							//} 
							
							widHtml.append("</div>"); 		 
							break;
						case  0://file field
							break;
					} 		
					
					widHtml.append("</div>"); 
				}
			}
			widHtml.append("</form>");
			
			//add form attachment code
			if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free") && !service.getAccountType().equals("Starter")){
				if(attControl!=null){
					widHtml.append("<form name='fileupload' id='fileupload' "+font+" class='cheetahWidget2012_form_input' action='/clientswidgets/uploadwidgetfile' method='post' enctype='multipart/form-data'>");
					widHtml.append("<span><img width='13' height='13' src='https://s3.amazonaws.com/dropifi/images/widget/client/contact_attachment.png'>"+attControl.getTitle()+"</span>");
					widHtml.append("<span id ='cheetahWidgetError_attachment' class = 'cheetahWidgetError_field'></span>");
					widHtml.append("<span id='cheetahWidgetField_fileContent'><input "+font+" type='file' id='cheetahWidgetField_attachment' name ='uploadfile' maxlength = '1' /></span><span id='cheetahWidgetField_attachment_close'>x</span>"); 
					widHtml.append("<input type='hidden' id='url' name='url' value='/clientswidgets/uploadwidgetfile' />");
					widHtml.append("<input type='hidden' id='attachmentId' name='attachmentId' />");
					widHtml.append("<input type='hidden' id='sender' name='sender' />");
					widHtml.append("<input type='hidden' id='publickey' name='publickey' value='"+this.getPublicKey()+"' />");
					widHtml.append("<span style='display:none !important' id='droppedfiles'></span>");
					widHtml.append("<input type='submit' name='submit' id='submitAttachment' value='Upload Files'/>");
					widHtml.append("</form>");
				}
			}
			
			widHtml.append("<div class='cheetahWidget2012_submit_input' "+font+" >");
			
			if(service!=null && service.getAccountType()!=null && !service.getAccountType().equals("Free")){
				if(widget.getHasCaptcha()==true) {
					if(widget.getCaptchaValue()==null || widget.getCaptchaValue().trim().isEmpty())
						widget.setCaptchaValue("What's the result of");
					
					widHtml.append("<p class='cheetahWidget2012_captcha'>"+widget.getCaptchaValue()+"<span class='cheetahWidget2012_captcha_input'></span></p><p <span id ='cheetahWidgetError_captcha' class = 'cheetahWidgetError_field'>></p>");
				}
			}else {
				if(widget.getHasCaptcha()==true) widget.updateHasCaptcha(false);
			}
			
			String key = this.getPublicKey();
			String sendColor= widget.getSendButtonColor();
			String sendText = widget.getSendButtonText();
			
			if(widget.getSendButtonColor()==null){
				sendColor = "#1E7BB8";
			}
			
			if(widget.getSendButtonText()==null){
				sendText = "Send Message";
			}
			
			String shadow = DropifiTools.decreaseColor(widget.getSendButtonColor());
			
			//check if the send button background color is dark or light.
			//set the font color to white if dark else black
			Color fcolor = DropifiTools.isBrighten(sendColor)?Color.BLACK:Color.WHITE;
			String fontColor =Integer.toHexString(fcolor.getRGB()).replaceFirst("ff","#");
			widHtml.append("<input "+font+" type='button' ref_fontColor='"+fontColor+"' ref_shadow='"+shadow+"' ref_color='"+sendColor+"' class='cheetahWidget2012_button' value='"+sendText+"' id='btnSave' onclick=\"javascript:doSaveCheetahWidget('"+key+"')\" /></div>"); 
			
			//----------------------------------------------------------------------
			widHtml.append("</div></div>"); 
			//add dropifi Branding			
			if(service==null || service.hasExceeded){
				widHtml.append(" <div id='cheetahWidget2012_footer'><a style='text-decoration:none;padding-top:5px;' target='_blank' href='https://www.dropifi.com'><img id='dropifiWidget_powered_by_dropifi' src='https://s3.amazonaws.com/dropifi/images/powered_by_dropifi.png'/></a> </div>");
			}else{
				widHtml.append(" <div id='cheetahWidget2012_footer'><a style='text-decoration:none;padding-top:5px;' target='_blank' href='https://www.dropifi.com'></a> </div>");
			}
		}
		//return the html version of the widget if save is successful 		
		return widHtml == null ? "":  widHtml.toString();
	}
  	
  	public int updateWidgetActivated(boolean activated){
  		clearCache();	 
		this.wiParameter.setActivated(activated);
		this.save();
		return 200;
	}
	
 	public static WidgetDefault findByPublicKey(String publicKey){
		return WidgetDefault.find("byPublicKey", publicKey).first();
	}
 	
 	public static String getApikey(String publicKey){
 		return WidgetDefault.find("select w.apikey from WidgetDefault w where w.publicKey = ? ", publicKey).first();		
 	}
 	
 	public static String getPublicApikey(String apikey){
 		return WidgetDefault.find("select w.publicKey from WidgetDefault w where w.apikey = ? ", apikey).first();		
 	} 
 	
  	public static WidgetDefault findByApiKey(String apikey){
		return WidgetDefault.find("byApiKey", apikey).first();
	}
  	
	public static WidgetDefault findByPluginType(String shop,EcomBlogType pluginType){
		return WidgetDefault.find("select w from WidgetDefault w , Company c " +
				"where (w.apikey = c.apikey) and (c.ecomBlogPlugin.eb_name=?) and (c.ecomBlogPlugin.eb_pluginType=?) ", shop, pluginType).first(); 
	} 
	
  	public static WiParameter findParamByPublicKey(String publicKey){ 
		return Widget.find("select w.wiParameter from WidgetDefault w where w.publicKey = ?", publicKey).first();
	}
	
	public static String findHtmlCode(String apikey){
		return WidgetDefault.find("select w.html from WidgetDefault w where w.apikey = ? ",apikey).first();
	}
	
	public static String findHtmlCodeByPKey(String publickey){ 
		return WidgetDefault.find("select w.html from WidgetDefault w where w.publicKey = ?",publickey).first(); 
	}
	
	public static String findHtmlCodeByPlugin(String shop,EcomBlogType pluginType){  
		return WidgetDefault.find("select w.html from WidgetDefault w , Company c  " +
				"where (w.apikey = c.apikey) and (c.ecomBlogPlugin.eb_name=?) and (c.ecomBlogPlugin.eb_pluginType=?) ",shop, pluginType).first(); 
	}
	
	//--------------------------------------------------------------------------------------------------------
	//render widget html to the view
	
	public static String renderWidget(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl){
		String widget_content = "<!DOCTYPE html>" +
				"<html><head> <meta charset='UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=7, IE=8, IE=9'/>" + 
				"<link rel='stylesheet' media='screen' href='https://s3.amazonaws.com/dropifi/css/widget/dropifi_widget.iframe.min_v2.css?v=20' />" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js?v=19'></script>" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/jquery.form.js?v=18'></script>" + 
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/easyXDM.js?v=19'></script>" + 
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/dropifi_widget_content_v2.js?v=20'></script>" + 
				"<script type='text/javascript'> var Dropifi = {}; Dropifi.hostUrl='"+hostUrl+"';" +
					" Dropifi.buttonPosition='"+buttonPosition+"'; Dropifi.pluginType='"+pluginType+"'; " +
					" Dropifi.publicKey='"+publicKey+"'; Dropifi.requestUrl='"+requestUrl+"';</script>"+ 
				"<script src='//j.maxmind.com/js/geoip.js' type='text/javascript' ></script>"+
				"<script src='//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js' type='text/javascript'></script>"+
				"</head>" +
				"<body style='margin:0px;padding:0px;overflow:hidden'>"+ 				 
		        "<div id = 'cheetahWidget2012_contactForm'>" + (html!=null?html.trim():"") + "</div>" +
		        "<input type='hidden' id='change_iframe_height' />" +		         
		        "</body></html>";
		
		return widget_content;
	} 
	
	public static String renderWidgetPopOut(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl,String bgcolor){
		String widget_content ="<!DOCTYPE html>" +
				"<html><head> <meta charset='UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=7, IE=8, IE=9'/>" + 
				"<link rel='stylesheet' media='screen' href='https://s3.amazonaws.com/dropifi/css/widget/dropifi_widget.iframe.popout_v2.css?v=22' />" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js?v=20'></script>" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/jquery.form.js?v=20'></script>" +   
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/dropifi_widget_content.popout_v2.js?v=22'></script>" + 
				"<script type='text/javascript'> var Dropifi = {}; Dropifi.hostUrl='"+hostUrl+"';" +
				" Dropifi.buttonPosition='"+buttonPosition+"'; Dropifi.pluginType='"+pluginType+"'; " +
				" Dropifi.publicKey='"+publicKey+"'; Dropifi.requestUrl='"+requestUrl+"'; Dropifi.bgColor='"+bgcolor+"';</script>"+
				"<script src='//j.maxmind.com/js/geoip.js' type='text/javascript' ></script>"+
				"<script src='//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js' type='text/javascript'></script>"+
				"</head>" +
				"<body style='margin:0px;padding:0px;overflow:hidden'>"+ 				 
		        "<div id = 'cheetahWidget2012_contactForm'>" + (html!=null?html.trim():"") + "</div>" +		        		         
		        "</body></html>";
		
		return widget_content;
	}
	 
	public static String renderWixWidgetPopOut(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl,String bgcolor){
		String widget_content ="<!DOCTYPE html>" +
				"<html><head> <meta charset='UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=7, IE=8, IE=9'/>" + 
				"<link rel='stylesheet' media='screen' href='https://s3.amazonaws.com/dropifi/css/widget/dropifi_widget.iframe.popout_v2.css?v=22' />" +
				"<script type='text/javascript' src='//sslstatic.wix.com/services/js-sdk/1.24.0/js/Wix.js?v=20'></script>"+
				"<script type='text/javascript' src='https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js?v=20'></script>" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/jquery.form.js?v=20'></script>" +   
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/dropifi_widget_content.popout_v2.js?v=22'></script>" + 
				"<script type='text/javascript'> var Dropifi = {}; Dropifi.hostUrl='"+hostUrl+"';" +
				" Dropifi.buttonPosition='"+buttonPosition+"'; Dropifi.pluginType='"+pluginType+"'; " +
				" Dropifi.publicKey='"+publicKey+"'; Dropifi.requestUrl='"+requestUrl+"'; Dropifi.bgColor='"+bgcolor+"';</script>"+
				"<script src='//j.maxmind.com/js/geoip.js' type='text/javascript' ></script>"+
				"<script src='//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js' type='text/javascript'></script>"+
				"</head>" +
				"<body style='margin:0px;padding:0px;overflow:hidden'>"+ 				 
		        "<div id = 'cheetahWidget2012_contactForm'>" + (html!=null?html.trim():"") + "</div>" +		        		         
		        "</body></html>";
		
		return widget_content;
	}
	
	public static String renderWidgetDEV(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl){
		String widget_content ="<!DOCTYPE html>" +
				"<html><head> <meta charset='UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=7, IE=8, IE=9'/>" + 
				"<link rel='stylesheet' media='screen' href='http://54.215.4.180/public/client/stylesheets/dropifi_widget.iframe.min.css?v=17' />"+
				"<script type='text/javascript' src='https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js?v=16'></script>" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/jquery.form.js?v=19'></script>" + 
				"<script type='text/javascript' src='http://54.215.4.180/public/client/javascripts/easyXDM.js?v=16'></script>" + 
				"<script type='text/javascript' src='http://54.215.4.180/public/client/javascripts/iframe/dropifi_widget_content.js?v=15'></script>" + 
				"<script type='text/javascript'> var Dropifi = {}; Dropifi.hostUrl='"+hostUrl+"';" +
					" Dropifi.buttonPosition='"+buttonPosition+"'; Dropifi.pluginType='"+pluginType+"'; " +
					" Dropifi.publicKey='"+publicKey+"'; Dropifi.requestUrl='"+requestUrl+"';</script>"+
				"<script src='//j.maxmind.com/js/geoip.js' type='text/javascript' ></script>"+
				"<script src='//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js' type='text/javascript'></script>"+
				"</head>" +
				"<body style='margin:0px;padding:0px;overflow:hidden'>"+				 
		        "<div id = 'cheetahWidget2012_contactForm' >" + (html!=null?html.trim():"") + "</div>" +
		        "<input type='hidden' id='change_iframe_height' />" +		      
		        "</body></html>";
		return widget_content;
	}
	
	public static String renderWidgetPopOutDEV(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl,String bgcolor){
		String widget_content ="<!DOCTYPE html>" +
				"<html><head> <meta charset='UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=7, IE=8, IE=9'/>" + 
				"<link rel='stylesheet' media='screen' href='http://54.215.4.180/public/client/stylesheets/wix_css/dropifi_widget.iframe.popout.css?v=18' />"+
				"<script type='text/javascript' src='https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js?v=18'></script>" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/jquery.form.js?v=18'></script>" +
				"<script type='text/javascript' src='http://54.215.4.180/public/client/javascripts/iframe/wix_js/dropifi_widget_content.popout.js?v=18'></script>" + 
				"<script type='text/javascript'> var Dropifi = {}; Dropifi.hostUrl='"+hostUrl+"';" +
					" Dropifi.buttonPosition='"+buttonPosition+"'; Dropifi.pluginType='"+pluginType+"'; " +
					" Dropifi.publicKey='"+publicKey+"'; Dropifi.requestUrl='"+requestUrl+"'; Dropifi.bgColor='"+bgcolor+"';</script>"+
					"<script src='//j.maxmind.com/js/geoip.js' type='text/javascript' ></script>"+
					"<script src='//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js' type='text/javascript'></script>"+
				"</head>" +
				"<body style='margin:0px;padding:0px;overflow:hidden'>"+ 				 
		        "<div id = 'cheetahWidget2012_contactForm' name ='' method='post'>" + (html!=null?html.trim():"") + "</div>" +		          
		        "</body></html>";
		
		return widget_content;
	}
	
	public static String renderWixWidgetPopOutDEV(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl,String bgcolor){
		String widget_content ="<!DOCTYPE html>" +
				"<html><head> <meta charset='UTF-8'> <meta http-equiv='X-UA-Compatible' content='IE=7, IE=8, IE=9'/>" + 
				"<link rel='stylesheet' media='screen' href='http://54.215.4.180/public/client/stylesheets/wix_css/dropifi_widget.iframe.popout.css?v=18' />"+
			    "<script type='text/javascript' src='//sslstatic.wix.com/services/js-sdk/1.24.0/js/Wix.js?v=18'></script>"+
				//"<script type='text/javascript' src='https://s3.amazonaws.com/ccheetah/js/jquery-1.7.2.min.js?v=18'></script>" +
				"<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'></script>" +
				"<script type='text/javascript' src='https://s3.amazonaws.com/dropifi/js/widget/jquery.form.js?v=18'></script>" +
				"<script type='text/javascript' src='http://54.215.4.180/public/client/javascripts/iframe/wix_js/dropifi_widget_content.popout.js?v=18'></script>" + 
				"<script type='text/javascript'> var Dropifi = {}; Dropifi.hostUrl='"+hostUrl+"';" +
					" Dropifi.buttonPosition='"+buttonPosition+"'; Dropifi.pluginType='"+pluginType+"'; " +
					" Dropifi.publicKey='"+publicKey+"'; Dropifi.requestUrl='"+requestUrl+"'; Dropifi.bgColor='"+bgcolor+"';</script>"+
					"<script src='//j.maxmind.com/js/geoip.js' type='text/javascript' ></script>"+
					"<script src='//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js' type='text/javascript'></script>"+
				"</head>" +
				"<body style='margin:0px;padding:0px;overflow:hidden'>"+ 				 
		        "<div id = 'cheetahWidget2012_contactForm' name ='' method='post'>" + (html!=null?html.trim():"") + "</div>" +		          
		        "</body></html>";
		
		return widget_content;
	}
	
	public static String renderWidgetContent(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey, String popout,String requestUrl,String bgcolor){
		/*String widget_content = "";
		
		if(popout != null && popout.equalsIgnoreCase("POPOUT")){	
			if(play.Play.mode.isDev())						
				widget_content = WidgetDefault.renderWidgetPopOutDEV(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor);
			else
				widget_content = WidgetDefault.renderWidgetPopOut(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor);
		}else{
			if(play.Play.mode.isDev())						
				widget_content = WidgetDefault.renderCombinedWidgetDEV(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl);
			else
				widget_content = WidgetDefault.renderCombinedWidget(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl);
		}*/
		
		return (html!=null?html.trim():"");
	}
	
	public static String renderWidgetContent(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl){
		/*String widget_content = "";
		if(play.Play.mode.isDev())						
			widget_content = WidgetDefault.renderWidgetDEV(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl);
		else
			widget_content = WidgetDefault.renderWidget(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl);	 
		*/
		return (html!=null?html.trim():"");
	}
	
	public static String renderWidgetPopoutContent(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl, String bgcolor){
		/*String widget_content = "";
		if(play.Play.mode.isDev())					
			widget_content = WidgetDefault.renderWidgetPopOutDEV(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor);
		else
			widget_content = WidgetDefault.renderWidgetPopOut(html, buttonPosition,pluginType,hostUrl,publicKey,requestUrl,bgcolor);	 
		*/
		return (html!=null?html.trim():"");
	}
	
	public static String renderWixWidgetPopoutContent(String html,String buttonPosition,String pluginType,String hostUrl,String publicKey,String requestUrl, String bgcolor){
		 
		return (html!=null?html.trim():"");
	}
	
	
	//--------------------------------------------------------------------------------------------------------
	//getters and setters of instance variables
 
	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {  
		this.html  = html;
	}
	
	public String getApikey() {
		return apikey;
	}
	
	private String setApikey(String apikey) {
		return this.apikey = apikey;
	}
	
	private void setPublicApikey(String apikey) {
		if(apikey != null){ 			 		 		
			this.publicKey = Codec.hexMD5(apikey)+"-"+ new Date().getTime();			
		} 
	}
	
	public String getPublicKey(){
		return this.publicKey;
	}
	
	public WiParameter getWiParameter() {
		return wiParameter;
	}
	
	public void setWiParameter(WiParameter wiParameter) {
		this.wiParameter = wiParameter;
	}

	public String getSocialUID() {
		return socialUID;
	}

	public void setSocialUID(String socialUID) {
		this.socialUID = socialUID;
	}
	 	
}
