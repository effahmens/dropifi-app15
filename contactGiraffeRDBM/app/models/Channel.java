/**
 * @author philips effah mensah
 * @description 
 * @date 14/3/2012 
 * 
 **/

package models;
 
import play.data.validation.*;
import play.db.jpa.Model;
import javax.persistence.*;

import java.util.*;

import models.gmailauth.GmailCredentials;

import org.bson.types.ObjectId;
import org.hibernate.annotations.Index;

@Entity
public class Channel extends Model{
	
	@Required @Unique
	@Column(nullable=false, unique=true)
	private Long companyId;
	
	@Required @Unique
	@Column(nullable=false, unique=true)
	private Long settingId;
	
	@Required 
	@Column(nullable=false, unique=true) 
	private  Long widgetId; 
	
	@OneToMany(mappedBy="channel", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	private List<InboundMailbox> inboundMbs;
	
	@Column(nullable=false, unique=true)
	private String apikey;
	
	@Transient
	private String defaultEmail;
	@Transient
	private String domain;
	
	public Channel(Setting setting){
		
		this.setSettingId(setting.getId());
		Company company = setting.getCompany();
		this.setCompanyId(company.getId());
		this.setApikey(company.getApikey());
		this.setDefaultEmail(company.getEmail());
		this.domain = company.getDomain();
	}
	
	public Channel createChannel() throws NullPointerException{
		//search for channel using the companyId;
		try{
		 if(this.getCompanyId()==null) 
			 throw new NullPointerException("Failed creating channel.");
			
			Channel channel = findByCompanyId(this.getCompanyId()); 
			
			if(channel !=null){			
				return null;
			}else{	
				
				//create a widget and add to the channel
				Widget widget = new Widget(this.getApikey(), this.getCompanyId(), this.getDefaultEmail()).createWidget();
				
				 
				if(widget !=null){
					this.setWidgetId( widget.getId());
					
					if(this.validateAndSave()){
						try{
							widget.updateWidgetAfterSignUp(this.domain);					 
						}catch(Exception e){
							play.Logger.log4j.info(e.getMessage(),e);
						}
						return this;
					}
				}
				return null; 			 
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			return null;
		}
		 
	}
	
	public static Channel findByCompanyId(Long companyId){
		return Channel.find("byCompanyId", companyId).first();
	}
	
	public static Channel findByApikey(String apikey){
		return Channel.find("select ch from Channel ch, Company c where ch.companyId = c.id and " +
				"c.apikey = ?", apikey).first();
	}
	
	public static Channel findByDomain(String domain){
		return Channel.find("select ch from Channel ch, Company c where ch.companyId = c.id and " +
				"c.domain = ?", domain).first();
	}
	
	//------------------------------------------------------------------------------
	//Widget		
	
	public  Long  getWidgetId(){
		return this.widgetId;
	}
	
	private void setWidgetId(Long widgetId) {
		this.widgetId = widgetId;
	}
	
	//return the channel widgets
	public  Widget  getWidget(){
		return Widget.findById(this.getWidgetId());
	}

	//------------------------------------------------------------------------------
	//inboundMailbox
	
	//add a mailbox to the list of company mailboxes
	public int addInboundMb(InboundMailbox mailbox, String domain,boolean isOAuth){	
		
		if(mailbox != null && this.getCompanyId() !=null){			 
			mailbox.setCompanyId(this.getCompanyId());
			mailbox.setChannel(this);			
			return mailbox.createMailBox(domain,isOAuth);
		}else{
			return 403;
		}
	}
	
	public int updateInboundMb(InboundMailbox oldMailbox, InboundMailbox mailbox,String domain,boolean isOAuth){
		
		//set the companyId of the mailbox
		if(oldMailbox != null && mailbox !=null){
			mailbox.setCompanyId(this.getCompanyId());	
			return oldMailbox.updateMailbox(mailbox,domain,isOAuth); 
		}
		return 505; 
	}
	
	public int removeInboundMb(InboundMailbox mailbox){
		if(mailbox==null)
			return 501;
 
		int re = mailbox.removeMailbox();
		if(re==200){ 
			List<AccountUser> users = AccountUser.findAllAgent(this.getApikey());
			
			for(AccountUser user : users){				 
				user.removeRole(mailbox.getEmail());
			} 
			
			if(mailbox.getMailserver().equals("Gmail")){
				GmailCredentials gmail = GmailCredentials.retrieveCredential(this.getApikey(), mailbox.getEmail());
				if(gmail!=null){
					gmail.removeCredentials();
				}
			}
		}
		return re;
	}
		
	public InboundMailbox findInboundMb(String email){		 
		return (email==null) ? null : InboundMailbox.findByEmail(this.companyId, email.trim(),false); 
	}	
		
	public List<InboundMailbox> getInboundMbs(){
		return this.inboundMbs;
	}
	
	public static List<InboundMailbox> findAllInboundMb(String apikey){
		return InboundMailbox.findAllMailboxes(apikey, false);
	}
 
	//-----------------------------------------------------------------------------
	//getters and setters
	
	public Long getCompanyId() {
		return companyId;
	}
	
	public Company getCompany(){
		return Company.findById(this.companyId); 
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	public Setting getSetting() {
		return Setting.findById(this.getSettingId());
	}
	
	public Long getSettingId() {
		return settingId;
	}

	private void setSettingId(Long settingId) {
		this.settingId = settingId;
	}
	
	public String getApikey() {
		return apikey;
	}
	
	private void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getDefaultEmail() {
		return defaultEmail;
	}

	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = (defaultEmail!=null&&!defaultEmail.trim().isEmpty())?defaultEmail:"other";
	}
		
}
