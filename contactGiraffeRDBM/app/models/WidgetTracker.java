package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;
import org.joda.time.DateTime;

import play.cache.Cache;
import play.db.jpa.Model;
import play.libs.WS.HttpResponse;
import resources.CacheKey;
import resources.DropifiTools;
import resources.EcomBlogType;

@Entity
public class WidgetTracker extends Model{ 
	@Column(nullable =false) @Index(name="apikey")
	private String apikey;
	@Column(nullable =false,length=100000)
	private String host;
	@Column(nullable =false, length=100000)
	private String domain;
	@Column(nullable =false)
	private Timestamp installed;
	@Column(nullable =false)
	private Timestamp updated;	
	@Column(nullable =false)
	private Long counter;
	
	public WidgetTracker(String apikey, String host,String domain){
		this.setApikey(apikey);
		this.setHost(host);
		this.setDomain(domain);
	}
	
	public WidgetTracker(){}

	//=======================================================
	//updaters
	
	public static int saveTracker(Connection conn, WidgetTracker tracker) throws SQLException{
		if(tracker!=null && tracker.getApikey()!=null && conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Insert into WidgetTracker (apikey,host,domain,installed,updated,counter) value (?,?,?,?,?,?)");
			int i =1;
			
			query.setString(i++, tracker.getApikey()); 
			query.setString(i++, tracker.getHost());
			query.setString(i++, tracker.getDomain());
			//set time
			Timestamp time = new Timestamp(new Date().getTime());
			query.setTimestamp(i++,time);
			query.setTimestamp(i++,time);	
			query.setLong(i++, 0l);
			return query.executeUpdate()>0?200:201;
		}
		return 502;
	}
	
	public static int updateTracker(Connection conn, WidgetTracker tracker) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Update WidgetTracker Set host=? ,domain=?, updated =?, counter =counter+1 Where apikey =?");
			int i =1;			
			query.setString(i++, tracker.getHost());
			query.setString(i++, tracker.getDomain()); 
			
			Timestamp time = new Timestamp(new Date().getTime());
			query.setTimestamp(i++,time); 
			query.setString(i++, tracker.getApikey());
			
			return query.executeUpdate()>0?200:201; 
		}
		return 502; 
	}
	
	//===============================================================================
	//finders Widget
	
	public static WidgetTracker findByApikey(Connection conn, String apikey) throws SQLException{

		PreparedStatement query = conn.prepareStatement("Select id, apikey, host, domain, installed, updated,counter From WidgetTracker Where apikey = ? ");
		query.setString(1, apikey);
		query.setMaxRows(1);
		ResultSet result = query.executeQuery();
		return getWidgetTracker(result);
			 
	}
	
	public static WidgetTracker findByApikey(Connection conn, String apikey,String host) throws SQLException {
		PreparedStatement query = conn.prepareStatement("Select id, apikey, host, domain, installed, updated,counter From WidgetTracker Where apikey = ? and host = ?");
		
		query.setString(1, apikey);
		query.setString(2, host);
		query.setMaxRows(1);
		ResultSet result = query.executeQuery();
		return getWidgetTracker(result);
			 
	}
	
	public static WidgetTracker findByApikey(String apikey,String host){
		return WidgetTracker.find("select w from WidgetTracker w where w.apikey=? and w.host=?", apikey,host).first();
	}
	
	public static WidgetTracker findByApikey(String apikey){
		return WidgetTracker.find("select w from WidgetTracker w where w.apikey=?", apikey).first();
	}
		
	private static WidgetTracker getWidgetTracker(ResultSet result) throws SQLException{
		while(result.next()){
			WidgetTracker tracker = new WidgetTracker(); 
			tracker.id = result.getLong("id"); 
			tracker.setApikey(result.getString("apikey")); 
			tracker.setHost(result.getString("host"));
			tracker.setDomain(result.getString("domain")); 
			tracker.setInstalled(result.getTimestamp("installed"));
			tracker.setUpdated(result.getTimestamp("updated"));
			tracker.setCounter(result.getLong("counter"));
			return tracker;
		}
		return null;
	}
  
	/**
	 * Verify that a user has place the widget on their site and it is active for the past 15 days
	 * @param apikey
	 * @return
	 */
	public boolean hasWidgetInstalled(EcomBlogType ecomBlogType){
		return isInstalled(this, ecomBlogType);
	}
	
	/**
	 * Verify that a user has place the widget on their site and it is active for the past 15 days
	 * @param apikey
	 * @return
	 */
	public static boolean hasWidgetInstalled(String apikey,EcomBlogType ecomBlogType){
		return isInstalled(WidgetTracker.findByApikey(apikey), ecomBlogType);
	}
	
	/**
	 * Verify that a user has place the widget on their site and it is active for the past 15 days
	 * or has setup a monitored mailbox
	 * @param apikey
	 * @param companyId
	 * @return
	 */
	public static boolean hasWidgetMailboxInstalled(String apikey, long companyId,EcomBlogType ecomBlogType){
		Boolean verify = false;
		try{
			//String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.WidgetTracker, DropifiTools.WIDGET_TRACKER); 
			//verify = Cache.get(cacheKey, Boolean.class);
			
			//if(verify == null || verify==false){ 
			verify = isInstalled(WidgetTracker.findByApikey(apikey), ecomBlogType);
				//CacheKey.set(cacheKey, verify,null);
			//}
			
			//if(!verify){ verify = CacheKey.getMonitoredMailboxes(apikey, companyId)>0?true:false; }
			
		}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}
		
		if(verify==null)
			verify = false;
		
		return verify;
	}
	
	private static boolean isInstalled(WidgetTracker track,EcomBlogType ecomBlogType){
		boolean verify = false;	//check whether the widget is being used at the site. 
		if(ecomBlogType.equals(EcomBlogType.Wix)){// set the default tracking of wix site to true initiate install
			verify = true;
		}
		
		if(track!=null && track.getId()>0 && !track.getHost().trim().isEmpty()){ 			
			
			if(track.getCounter()>=0){   				
				//check if the site has been active for the past 15days. If not flag verify as false;
				if(!verify){
					 Timestamp time = new Timestamp(new Date().getTime());
					 Timestamp time2 = track.getUpdated();
					 long diff = time.getTime() - time2.getTime();
					 
					 if(diff<=1209600000){  
						verify = true;
					 } 				 
				}
				
				//check if the widget code is within the html content of the site
				if(!verify){ 
					//check the widget	 
					String url = track.getDomain().contains("http")? track.getDomain():"http://"+ track.getHost(); 
					verify = validateHost(url,track.getApikey(), ecomBlogType); 
					
					if(!verify){ 
						url = track.getHost().contains("http")? track.getHost():"http://"+ track.getHost();
						verify = validateHost(url,track.getApikey(),ecomBlogType);	
					}
				}
				  
			}
		}else{
			verify = true;		
		} 
	 
		return verify;
	}
	
	/**
	 * Search for the wigdet code on the website of the user
	 * @param url
	 * @param ecomBlogType
	 * @return
	 */
	private static boolean validateHost(String url,String apikey,EcomBlogType ecomBlogType){ 
		boolean verify =false;
		try{ 	
			HttpResponse res = DropifiTools.restWS(url);
			
			if(res==null || res.getStatus()!=200) 
				return true;
				
			String content = res!=null?res.getString():"";	
			if(ecomBlogType.equals(EcomBlogType.Shopify)){ 
				//play.Logger.log4j.info(content);
				verify = content.contains("dropifi_widget.shopify.js");
			}else if(ecomBlogType.equals(EcomBlogType.Tictail)){
				verify = true; //content.contains("dropifi_widget.tictail.js");
			}else{	
				String publickey = WidgetDefault.getPublicApikey(apikey);
				
				if(ecomBlogType.equals(EcomBlogType.Dropifi)){
					verify = (content.contains("dropifi_widget.min.js") && content.contains(publickey)); 
				}else if(ecomBlogType.equals(EcomBlogType.Wordpress)){
					verify = (content.contains("dropifi_widget.wordpress.js") && content.contains(publickey)); 
				}else if(ecomBlogType.equals(EcomBlogType.Magento)){
					verify = (content.contains("dropifi_widget.magento.js") && content.contains(publickey));
				}else if(ecomBlogType.equals(EcomBlogType.Wix)){
					verify = (content.contains("dropifi_widget.wix.min.js") && content.contains(publickey));
				}else if(ecomBlogType.equals(EcomBlogType.PrestaShop)){
					verify = (content.contains("dropifi_widget.prestashop.js") && content.contains(publickey));
				}else if(ecomBlogType.equals(EcomBlogType.CashieCommerce)){
					verify = (content.contains("dropifi_widget.cashiecommerce.js") && content.contains(publickey));
				}
				
				if(!verify){
					verify = (content.contains("dropifi_widget.min.js") && content.contains(publickey));
				}
			} 
		}catch(Exception e){
			verify=true;
			play.Logger.log4j.info(e.getMessage(),e); 
		}
		return verify;
	}
	
	//================================================================================
	//getters and setters
	
 	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host!=null?host.toLowerCase():"";
	}

	public Timestamp getInstalled() {
		return installed;
	}

	public void setInstalled(Timestamp installed) {
		this.installed = installed;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain!=null?domain.toLowerCase():"";
	}

	public Long getCounter() {
		return counter;
	}

	public void setCounter(Long counter) {
		this.counter = counter;
	}
	
	
}
