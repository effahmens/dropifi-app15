/**
 * @author phillips effah mensah
 * @description 
 * @date 14/3/2012
 * 
 **/

package models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*; 

import play.data.validation.*;
import play.db.jpa.Model;
import resources.RuleType;
import view_serializers.QrSerializer;

import javax.persistence.*;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

@Embeddable
public class QuickResponse implements Serializable{
	
	@Required @Column(nullable=false, length=10000)
	@SerializedName("title")
	private String title; 		
	@Required 	@Column(nullable=false, length=10000)
	@SerializedName("subject")
	private String subject;	
	@Required @Column(nullable=false, length=1000000)
	@SerializedName("message")
	private String message;
	
	@Required @Column(nullable=false)
	@SerializedName("activated")
	private boolean activated;
	@SerializedName("permission") 
	private Integer permission;
	
	@SerializedName("created")
	private Timestamp created;
	@SerializedName("updated")
	private Timestamp updated;
	
	@Transient
	@SerializedName("id")
	private String id;
	public QuickResponse(String title,String subject,String message, boolean activated, int permission) {
		 
		this.setTitle(title);		 
		this.setSubject(subject);
		this.setMessage(message);
		this.setActivated(activated); 
		this.setPermission(permission);
	}
	public QuickResponse(){}
	@Override
	public String toString() {
		return "QuickResponse [title=" + title + ", subject=" + subject
				+ ", message=" + message + ", activated=" + activated
				+ ", permission=" + permission + ", created=" + created
				+ ", updated=" + updated + "]";
	}

	
	public static JsonArray toJsonArray(List<QuickResponse> quickResponses){
		JsonArray arrayJson = new JsonArray();
		if(quickResponses != null){
			Gson gson = new Gson();
			ListIterator<QuickResponse> qrlist = quickResponses.listIterator();
			 
			int n=1;
			while(qrlist.hasNext()){
				QuickResponse qr =qrlist.next();
				qr.setId(String.valueOf(n++));
				arrayJson.add(gson.toJsonTree(qr)); 
			}
		} 		
		return arrayJson;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title ==null?null: title.trim();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean getActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Integer getPermission() {
		return permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}
	
	public Timestamp getCreated() {
		return created;
	}
	
	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
