package models;

import java.io.Serializable; 

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import org.hibernate.type.CharacterType;

import resources.TextToImage;

import com.google.gson.annotations.SerializedName;

@Embeddable
public class WiParameter implements Serializable{
	
	@SerializedName("activated")
	private Boolean activated;
	
	@SerializedName("buttonColor")
	private String 	buttonColor;
	
	@SerializedName("buttonText")
	private String 	buttonText;
	
	@SerializedName("buttonTextColor")
	private String buttonTextColor;
	
	@SerializedName("buttonPosition") 
	private String 	buttonPosition;
	
	@SerializedName("buttonPercent") 
	private Integer buttonPercent;
	
	@SerializedName("normalText") 
	private String 	normalText;	
	
	@SerializedName("imageText") 
	private String imageText;
	
	@SerializedName("buttonFont")
	private String buttonFont;
		
	@Transient
	private static final long serialVersionUID = 1L;
	
	public WiParameter(String buttonColor,String buttonText,String buttonFont,String buttonPosition, Integer buttonPercent,String buttonTextColor,String domain, String fileName) {		 
		this.setActivated(true);
		this.setButtonColor(buttonColor); 
		this.setButtonPosition(buttonPosition);
		this.setNormalText(buttonText);
		this.setButtonText(buttonText);		
		this.setButtonPercent(buttonPercent);		
		this.setButtonTextColor(buttonTextColor);
		this.setButtonFont(buttonFont);
		this.setButtonTextColor(this.setImageText(fileName,buttonText, domain)); 
	}
	
	public WiParameter(String buttonColor, String buttonText,String buttonFont,String buttonPosition, Integer buttonPercent,String buttonTextColor) {
		this.setActivated(true);
		this.setButtonColor(buttonColor); 
		this.setButtonPosition(buttonPosition);
		this.setNormalText(buttonText);
		this.setButtonText(buttonText);		
		this.setButtonPercent(buttonPercent);	
		this.setButtonFont(buttonFont);
		this.setButtonTextColor(buttonTextColor); 
		//this.setImageText(buttonText, domain);
	}
	
 
	@Override
	public String toString() {
		return "WiParameter [activated=" + activated + ",buttonColor=" + buttonColor + ", buttonText="
				+ buttonText + ", buttonTextColor=" + buttonTextColor
				+ ", buttonPosition=" + buttonPosition + ", buttonPercent="
				+ buttonPercent + ", normalText=" + normalText + ", imageText="
				+ imageText + ", buttonFont=" + buttonFont + "]";
	}

	public WiParameter(){
		
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public String getButtonColor() {
		return buttonColor;
	}

	public void setButtonColor(String buttonColor) {
		this.buttonColor = buttonColor;
	}

	public String getButtonText() {
		return buttonText;
	}

	public void setButtonText(String buttonText) {
		this.buttonText = buttonText;
	}

	public String getButtonPosition() {
		return buttonPosition;
	}

	public void setButtonPosition(String buttonPosition) {
		this.buttonPosition = buttonPosition;
	}

	public Integer getButtonPercent() {
		return buttonPercent;
	}
	
	public void setButtonPercent(Integer buttonPercent) {
		this.buttonPercent = buttonPercent;
	}
	
	private String convertToImageText(String text){
		String imageFormat = "";
		if(text != null && !text.trim().isEmpty()){
			
			text = text.trim();
			text = text.replace(';', ' ');
			char [] ctext = text.toCharArray();
			String prefix = "R"; //buttonPosition.equalsIgnoreCase("left")?"R":"R"; 
			 			 
			for(char c : ctext) {
				//determine the type of the character
				if(Character.isLetter(c)) {
					//determine the case of the character
					if(Character.isUpperCase(c)){
						imageFormat += prefix+"C"+c+".png;"; 
					} else if(Character.isLowerCase(c)){
						imageFormat += prefix+c +".png;";
					}
				}else if(Character.isSpaceChar(c)) {
					imageFormat +="SP" + ".png;";
				}else if(Character.isDigit(c)){
					imageFormat += prefix+c+".png;"; 
				} 
			}
		}
		return imageFormat;
	}
	
	public String getNormalText() {
		return normalText;
	}

	public void setNormalText(String normalText) {
		this.normalText = convertToImageText(normalText); 
	}

	public String getImageText() {
		return imageText;
	}
 
	public String setImageText(String fileName,String imageText, String domain) {
		TextToImage textToImage = new TextToImage(fileName, imageText, domain, this.getButtonColor(),this.getButtonPosition());
		textToImage.setTextFont(this.getButtonFont());
		this.imageText = textToImage.transform();
		return Integer.toHexString(textToImage.getTextColor().getRGB()).replaceFirst("ff","#"); 
	}
	
	public void setImageText(String imageText) {
		this.imageText = imageText;
	}

	public String getButtonTextColor() {
		return buttonTextColor;
	}

	public void setButtonTextColor(String buttonTextColor) {
		this.buttonTextColor = buttonTextColor;
	}

	public String getButtonFont() {
		return buttonFont;
	}

	public void setButtonFont(String buttonFont) {
		this.buttonFont = buttonFont;
	}
	
	
}
