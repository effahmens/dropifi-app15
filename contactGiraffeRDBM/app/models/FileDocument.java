package models;
/**
 * Create a document used for uploading files to S3. A reference of the file uploaded is saved as an object in
 * the database
 */
import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;

import play.db.jpa.Model;
import resources.AWSS3;
import resources.DropifiTools;
import resources.FileType;
 
import api.rackspace.RackspaceClient;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.google.gson.annotations.SerializedName;
 
@Entity
public class FileDocument extends Model implements Serializable{
	
	
	@Column(nullable=false) @Index(name="apikey")
	private String apikey;
	@SerializedName("fileName")
	@Column(nullable=false)
	private String fileName;
	@SerializedName("fileType")
	private FileType fileType;
	@SerializedName("fileUrl")
	private String fileUrl; 
	
	public FileDocument(String apikey,String fileName, FileType fileType) {	
		this(apikey,fileName,fileType,null);
	}
	
	public FileDocument(String apikey,String fileName, FileType fileType,String fileUrl) {	
		this.apikey = apikey;
		this.fileName = fileName;
		this.fileType = fileType;
		this.fileUrl =fileUrl;
	}
	
	public FileDocument(String apikey,FileType fileType){
		this(apikey,null,fileType);
	}

	/**
	 * upload the file to S3 and set it as a public view file
	 * @param BUCKETNAME
	 * @param folder
	 * @param file
	 * @return 
	 */
	public PutObjectResult uploadPublicFileToS3(String BUCKETNAME, String folder, File file){
		
		PutObjectResult result = AWSS3.putPublicFile(BUCKETNAME,folder,file,this.getFileName()); 
		//save the file properties to db
		
		Date date = new Date();
		String url ="https://s3.amazonaws.com/"+BUCKETNAME+"/"+folder+"/"+this.getFileName()+"?v="+date.getTime();
		this.setFileUrl(url);
		this.saveFileDocument();
		return result;
	}
	
	/**
	 * upload the file to rackspace and set it as a public view file
	 * @param BUCKETNAME
	 * @param folder
	 * @param file
	 * @return 
	 */
	public String uploadPublicFileToRackspace(String BUCKETNAME, String folder, File file){ 
		try {
			RackspaceClient client = new RackspaceClient(DropifiTools.RackspaceUsername, DropifiTools.RackspaceApikey);
			client.uploadObject(BUCKETNAME+File.separator+ folder, file, this.getFileName());
			
			//save the file properties to db  
			String url = client.generateRackspaceFileUrl(client.getSSLCDN(BUCKETNAME), folder, this.getFileName());
			this.setFileUrl(url);
			this.saveFileDocument();
			return url;
		}catch(Exception e ) {
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public FileDocument saveFileDocument(){
		FileDocument found = findDocument(this.getApikey(), this.getFileType(), this.getFileName());
		if(found!=null){
			found.setFileName(this.getFileName());
			found.setFileUrl(this.getFileUrl());
			return found.save();
		}
		return this.save();
	}
	
	public static FileDocument findDocument(String apikey, FileType fileType, String fileName){
		return FileDocument.find("Select f from FileDocument f where f.apikey=? and f.fileType=? and f.fileName=?", apikey,fileType,fileName).first();
	}
	
	public static String findFileUrl(String apikey, FileType fileType, String fileName){
		return FileDocument.find("Select f.fileUrl from FileDocument f where f.apikey=? and f.fileType=? and f.fileName=?", apikey,fileType,fileName).first();
	}
	
	public static String findFileUrl(Connection conn,String apikey, FileType fileType, String fileName){
		try {
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query =conn.prepareStatement("Select fileUrl from FileDocument  where apikey=? and fileType=? and fileName=?");
				int i=1;
				query.setString(i++, apikey);
				query.setInt(i++, fileType.ordinal());
				query.setString(i++, fileName);
				ResultSet result = query.executeQuery();
				
				while(result.next()){
					return result.getString("fileUrl");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return "";
	}
	
	public static String findFileName(Connection conn,String apikey, FileType fileType, String attachmentId){
		try {
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query =conn.prepareStatement("Select fileName from FileDocument where apikey=? and fileType=? and fileUrl=?");
				int i=1;
				query.setString(i++, apikey);
				query.setInt(i++, fileType.ordinal());
				query.setString(i++, attachmentId);
				ResultSet result = query.executeQuery(); 
				
				while(result.next()){
					return result.getString("fileName");
				}
			} 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public static String findFileUrl(String apikey, FileType fileType){
		return findFileUrl(apikey, fileType, fileType.name().toLowerCase()+".png");
	}
	
	public static String findFileUrl(Connection conn,String apikey, FileType fileType){
		return findFileUrl(conn, apikey, fileType,fileType.name().toLowerCase()+".png");
	} 
	
	public static FileDocument deleteFileDocument(String apikey, FileType fileType, String fileName){
		FileDocument found = findDocument(apikey, fileType, fileName);
		if(found!=null){
			return found.delete();
		}
		return null;
	}
	
	public static FileDocument deleteFileDocument(String apikey, FileType fileType){
		return deleteFileDocument(apikey, fileType,fileType.name().toLowerCase()+".png");
	}
	
	public static String retrieveFileExtension(File file){
		return retrieveFileExtension(file.getName());
	}
	
	public static String retrieveFileExtension(String filename){
		String extension = "";
		if(filename!=null){ 
			extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
		}
		return extension; 
	}
	
	public static Map<String,Double> getFileSize(File file){
		HashMap<String, Double> sizes = new HashMap<String,Double>();
		if(file!=null && file.exists()){
			
			double bytes = file.length();
			double kilobytes = (bytes / 1024);
			double megabytes = (kilobytes / 1024);
			double gigabytes = (megabytes / 1024);
			double terabytes = (gigabytes / 1024);
			double petabytes = (terabytes / 1024);
			double exabytes = (petabytes / 1024);
			double zettabytes = (exabytes / 1024);
			double yottabytes = (zettabytes / 1024);
			
			sizes.put("bytes", bytes);
			sizes.put("kilobytes", kilobytes);
			sizes.put("megabytes", megabytes);
			sizes.put("gigabytes", gigabytes);
			sizes.put("terabytes", terabytes);
			sizes.put("petabytes", petabytes);
			sizes.put("exabytes", exabytes);
			sizes.put("zettabytes", zettabytes);
			sizes.put("yottabytes", yottabytes);
		} 
		return sizes;
	}
	
	public static boolean isImageFile(String exten){
		switch(exten.toLowerCase().trim()){
			case "png": case "jpeg": case "jpg": case "gif": case "ico":
				return true;
			default:
				return false;
		}
	}
	
	/**
	 * As a security measure to prevent potential viruses, Dropifi doesn't allow you to send or receive executable files (such as files ending in .exe). 
	 * Executable files can contain harmful code that might cause malicious software to download to your computer. 
	 * In addition, Dropifi doesn't allow you to send or receive corrupted files, files that don't work properly.
	 * @param exten
	 * @return
	 */
	public static boolean isBlockedAttachFile(String fileName){
		String exten = retrieveFileExtension(fileName);
		String extens[] = ".ade, .adp, .bat, .chm, .cmd, .com, .cpl, .exe, .hta, .ins, .isp, .jse, .lib, .mde, .msc, .msp, .mst, .pif, .scr, .sct, .shb, .sys, .vb, .vbe, .vbs, .vxd, .wsc, .wsf, .wsh".split(",");   
		for(String ext : extens){
			if(ext.trim().equalsIgnoreCase(exten))
				return true;
		}
		return false;
	}
	
	//-----------------------------------------------------------------------------------------------
	//getters and setters
	public String getFileName() {
		return fileName;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}	 

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

}
