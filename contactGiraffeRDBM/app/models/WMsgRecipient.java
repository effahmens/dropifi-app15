package models;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.Column;
import javax.persistence.Entity;

import models.dsubscription.SubConsumeService;
import models.dsubscription.SubServiceName;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.annotations.Index;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;
 

import play.cache.Cache;
import play.data.validation.Email;
import play.db.jpa.Model;
import resources.CacheKey;
import resources.DCON;
import resources.DropifiTools;
import resources.helper.ServiceSerializer;
import view_serializers.RecipientSerializer;

@Entity
public class WMsgRecipient extends Model{
   
	@Column(nullable=false) @Index(name="apikey")
	private String apikey;
	@Column(nullable=false)
	private String username;
	@Column(nullable=false) @Email 
	private String email;
	@Column(nullable=false) @Index(name="activated")
	private boolean activated;
	@Column(nullable=false)
	private Timestamp created;
	@Column(nullable=false)
	private Timestamp updated;
	private int priorityLevel;
	private Boolean knownActivation;
	private Boolean isMailboxForward;
	
	public WMsgRecipient(String apikey, String username, String email, boolean activated,Boolean isMailboxForward) { 
		
		this.setApikey(apikey);
		this.setUsername(username);
		this.setEmail(email);
		this.setActivated(activated); 
		this.setIsMailboxForward(isMailboxForward);
	}
	
	public WMsgRecipient(String apikey,RecipientSerializer recipient){
		this(apikey,recipient.getUsername(),recipient.getEmail(),recipient.isActivated(),recipient.getIsMailboxForward());
	}
	
	//=========================================================================================================
	@Override
	public String toString() {
		return "WMsgRecipient [apikey=" + apikey + ", username=" + username
				+ ", email=" + email + ", activated=" + activated
				+ ", created=" + created + ", updated=" + updated + ", isMailboxForward=" + isMailboxForward + "]";
	} 
	
	public int saveRecipient(){
		//check if the recipient is not created
		WMsgRecipient found = WMsgRecipient.findRecipient(this.getApikey(), this.getEmail()); 
		if(found != null && found.getId()!=null){
			return 302;	//duplication of recipients
		}
		this.setPriorityLevel((int)new Date().getTime());
		Timestamp time = new Timestamp(new Date().getTime());
		this.setCreated(time);
		this.setUpdated(time);
		return this.validateAndSave()?200:501;
	}
	
	public static int orderRecipients(String apikey,List<String> recipients){
		if(recipients!=null && recipients.size()>0){ 
			for(String recipient : recipients){
				 JsonObject json = (JsonObject) new JsonParser().parse(recipient);
				 WMsgRecipient found = WMsgRecipient.findRecipient(apikey,json.get("email").getAsString());
				 if(found!=null){ 
					 found.setPriorityLevel((int)new Date().getTime());
					 Timestamp time = new Timestamp(new Date().getTime());
					 found.setCreated(time);
					 found.setUpdated(time);
					 found.validateAndSave();
				 }
			}
			return 200;
		}
		return 501;
	}
	
	public int updateRecipient(RecipientSerializer recipient){
		//if the email address of the recipient to updated is not the same as the serialized email,
		//then check if the new email address is not created already
		if(!recipient.getEmail().equalsIgnoreCase(this.getEmail())){
			WMsgRecipient found = WMsgRecipient.findRecipient(this.getApikey(), recipient.getEmail());
			
			if(found != null && found.getId()!=null) 			 
				return 302; //duplication of recipients
			
		} 
		
		this.setUsername(recipient.getUsername());
		this.setActivated(recipient.isActivated());
		this.setEmail(recipient.getEmail()); 	 
		this.setUpdated(new Timestamp(new Date().getTime()));
		this.setIsMailboxForward(recipient.getIsMailboxForward());
		
		return this.validateAndSave()?200:501;
	}
	
	public static WMsgRecipient findRecipient(String apikey, String email){
		return WMsgRecipient.find("byApikeyAndEmail", apikey,email).first(); 		 
	}

	public static List<WMsgRecipient> findRecipients(String apikey){
		return WMsgRecipient.find("Select r From WMsgRecipient r Where r.apikey=? order by r.priorityLevel ASC",apikey).fetch();
	}
	
	public static List<WMsgRecipient> findSortedRecipients(String apikey){
		return WMsgRecipient.find("Select r From WMsgRecipient r Where r.apikey=? order by r.priorityLevel ASC",apikey).fetch();
	}
	
	public static List<String> recipients(String apikey){
		List<String> result = WMsgRecipient.find("Select r.email from WMsgRecipient r where r.apikey = ?", apikey).query.getResultList();			
		return result; 
	} 
	
	public static List<String> findRecipients(Connection conn, String apikey) throws SQLException{
		
		if(conn != null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select r.email from WMsgRecipient r where r.apikey = ? order by r.priorityLevel ASC");
			query.setString(1, apikey); 
			
			ResultSet result = query.executeQuery();
			List<String> recipients = new LinkedList<String>();
			while(result.next()){
				recipients.add(result.getString("email"));
			}
			return recipients;
		}
		return null;
	}
	
	public static List<String> findActiveRecipients(Connection conn, String apikey) throws SQLException {
		
		if(conn != null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select r.email from WMsgRecipient r where r.apikey = ? and r.activated = ? order by r.priorityLevel ASC");
			query.setString(1, apikey); 
			query.setBoolean(2, true); 
			
			ResultSet result = query.executeQuery();
			List<String> recipients = new LinkedList<String>();
			while(result.next()){
				recipients.add(result.getString("email")); 
			}
			return recipients;
		}
		return null;
	} 
	
	/**
	 * Contains an array of Address of all active Message Recipients which messages from mailbox should be forwarded to
	 * @param conn
	 * @param apikey
	 * @return
	 * @throws SQLException
	 */
	public static Address[] activeForwardRecipients(Connection conn, String apikey) throws SQLException {
		if(conn != null && !conn.isClosed()){
			 			
			PreparedStatement query = conn.prepareStatement("Select r.username, r.email from WMsgRecipient r where r.apikey = ? and r.activated = ? and r.isMailboxForward=? order by r.priorityLevel ASC");
			query.setString(1, apikey); 
			query.setBoolean(2, true);
			query.setBoolean(3, true);
			
			return getActiveRecipients(query);
		}
		return null;
	}
	/**
	 * Contains an array of Address of all active Message Recipients which messages from mailbox should be forwarded to
	 * Email forward to the same mailbox email is denied
	 * @param conn
	 * @param apikey
	 * @param mailboxEmail
	 * @return
	 * @throws SQLException
	 */
	public static Address[] activeForwardRecipients(Connection conn, String apikey,String mailboxEmail) throws SQLException {
		if(conn != null && !conn.isClosed()){
 			
			PreparedStatement query = conn.prepareStatement("Select r.username, r.email from WMsgRecipient r where r.apikey = ? and r.activated = ? and r.isMailboxForward=? order by r.priorityLevel ASC");
			query.setString(1, apikey); 
			query.setBoolean(2, true);
			query.setBoolean(3, true);
			
			return getActiveRecipients(query,mailboxEmail);
		}
		return null;
	}
	
	public static Address[] activeRecipients(Connection conn, String apikey) throws SQLException { 
		if(conn != null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select r.username, r.email from WMsgRecipient r where r.apikey = ? and r.activated = ? order by r.priorityLevel ASC");
			query.setString(1, apikey); 
			query.setBoolean(2, true); 
			
			return getActiveRecipients(query);
		}
		return null; 
	}
	
	private static Address[] getActiveRecipients(PreparedStatement query) throws SQLException{
		//create a temporary arrayList to populate the email addresses and and  covert
		ArrayList<InternetAddress> addressList = new ArrayList<InternetAddress>();

		if(query != null){
							
			ResultSet result = query.executeQuery();		
			while(result.next()){
				try {					 
					addressList.add(new InternetAddress(result.getString("email"), result.getString("username")));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.error(e.getMessage(), e);
				} 
			} 
			Address[] recipients = new InternetAddress[addressList.size()];
			
			int i =0;
			for(Address add: addressList){
				recipients[i++] = add; 
			} 
			
			return recipients; 
		} 
		
		return null;
	}
	
	private static Address[] getActiveRecipients(PreparedStatement query,String filterEmail) throws SQLException{
		//create a temporary arrayList to populate the email addresses and covert
		ArrayList<InternetAddress> addressList = new ArrayList<InternetAddress>();
		
		if(query != null){ 
			ResultSet result = query.executeQuery();		
			while(result.next()){
				try {			
					String email = result.getString("email");
					if(!filterEmail.equals(email))
						addressList.add(new InternetAddress(email, result.getString("username")));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.error(e.getMessage(), e);
				} 
			}
			 
			Address[] recipients = new InternetAddress[addressList.size()];
			
			int i =0;
			for(Address add: addressList){
				recipients[i++] = add; 
			} 
			
			return recipients; 
		}
		 
		
		return null;
	}
	
	public static Address[] activeRecipients(Connection conn, String apikey,String defaultEmail,String defaultUsername) throws SQLException, UnsupportedEncodingException {
		try{
			boolean isDefault = (defaultEmail!=null && !defaultEmail.equals("other"));
			
			InternetAddress[] addressList = new InternetAddress[isDefault?1:0];
			if(addressList.length>0){
				addressList[0] = new InternetAddress(defaultEmail,defaultUsername);	 
			}
			
			Address[] recipients = activeRecipients(conn, apikey);		 
			
			if(addressList.length>0 && recipients!=null){
				return (Address[]) ArrayUtils.addAll(addressList, recipients);
			}else if(addressList.length < 1 && recipients != null){
				return recipients;
			}else if(addressList.length>0 && recipients == null){
				return addressList;  
			} 		
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return null;
	}
	
	public static int deleteRecipient(String apikey, String email){		
		return WMsgRecipient.delete("Delete from WMsgRecipient w where w.apikey=? and w.email =?", apikey,email) >0?200:501;	
	}
	
	public int activateRecipient(boolean activated){
		try{
			this.setActivated(activated);
			this.setUpdated(new Timestamp(new Date().getTime()));
			return this.validateAndSave() ? 200:501;
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return 501;
	}
	
	public static JsonArray toJsonArray(List<WMsgRecipient> recipients){ 
		JsonArray arrayJson = new JsonArray();
		if(recipients != null){
			Gson gson = new Gson();
			ListIterator<WMsgRecipient> msglist = recipients.listIterator();
			
			long id = 1; 
			while(msglist.hasNext()){ 
				WMsgRecipient rec = msglist.next();
				RecipientSerializer recipient = new RecipientSerializer(id++,rec);				 
				arrayJson.add(gson.toJsonTree(recipient));
			}
		}
		return arrayJson;		
	}
	
	/**
	 * Called any time a user subscribed to a Plan 
	 * Loop through the list of recipients and deactivated all recipients which index number is more than service required volume
	 * @param apikey
	 * @return
	 */
	public static void autoUpateRecipients(String apikey){
		try{
			List<WMsgRecipient> recipients = findRecipients(apikey);
			if(recipients!=null){
				ListIterator<WMsgRecipient> msglist = recipients.listIterator();
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Recipients, SubServiceName.Recipient);
				long index = 1; 
				while(msglist.hasNext()){ 
					WMsgRecipient rec = msglist.next();
					boolean act = true; 
					if(!(index++ <=service.getThreshold().getVolume()) ){
						rec.setKnownActivaton(rec.isActivated());
						act = false; 
					}else{
						if(rec.isKnownActivation()!=null && !rec.isActivated()){
							act = rec.isKnownActivation();
						}else{
							act = rec.isActivated();
							rec.setKnownActivaton(act);
						}
					}
					
					rec.activateRecipient(act);
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	//=========================================================================================================
	//Cache
	
	//=========================================================================================================
	
	
	public String getApikey() {
		return apikey;
	}
	
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public int getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(int priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Boolean isKnownActivation() {
		return knownActivation;
	}

	public void setKnownActivaton(boolean knownActivation) {
		this.knownActivation = knownActivation; 
	}

	public Boolean getIsMailboxForward() {
		return isMailboxForward;
	}

	public void setIsMailboxForward(Boolean isMailboxForward) {
		this.isMailboxForward = isMailboxForward;
	}
	
	
}
