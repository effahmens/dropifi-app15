package models;

import play.data.validation.*;
import play.db.jpa.Model;
import javax.persistence.*;

@Embeddable
public class Site {
	
	public String name;
	public String url;
	public String timezone;
	
	public Site(String name, String url, String timezone){
		this.name = name;
		this.url = url;
		this.timezone = timezone;
	}
}
