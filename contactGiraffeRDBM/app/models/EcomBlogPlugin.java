package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Columns; 

import com.shopifyOAuthen.api.client.ShopifyClient;
import com.shopifyOAuthen.api.endpoints.ScriptTagsService;
import com.shopifyOAuthen.api.resources.ScriptTag;
import com.shopifyOAuthen.connectors.ShopifyAPIConnect;

import play.data.validation.Email;
import resources.DropifiTools;
import resources.EcomBlogType;

/**
 * Provide information about ecommerce shop such as Shopify, Magento or Blogger like Wordpress, Tumblr 
 * @author phillips
 *
 */
@Embeddable
public class EcomBlogPlugin implements Serializable{
	
	private String eb_email;
	private String eb_name;
	private String eb_apikey;
	private String eb_secretKey;
	private EcomBlogType eb_pluginType;
	
	public EcomBlogPlugin(String eb_email, String eb_name, String eb_apikey,
			String eb_secretKey, EcomBlogType eb_pluginType) {
		super(); 
		
		this.setEb_email(eb_email);
		this.setEb_name(eb_name);
		this.setEb_apikey(eb_apikey);
		this.setEb_secretKey(eb_secretKey);
		this.setEb_pluginType(eb_pluginType); 
	}
	
	public EcomBlogPlugin(String eb_name, String eb_apikey,
			String eb_secretKey, EcomBlogType eb_pluginType) {
		super(); 
		this.setEb_name(eb_name);
		this.setEb_apikey(eb_apikey);
		this.setEb_secretKey(eb_secretKey);
		this.setEb_pluginType(eb_pluginType); 
	}

	public String getEb_email() {
		return eb_email;
	}
	
	
	//====================================================================
	//other methods
	
	 public boolean hasEcomBlog(EcomBlogType pluginType){
		 return (pluginType != null && (pluginType.compareTo(this.getEb_pluginType()))==0)?true:false;			  
	 }
	 
	 public static boolean hasEcomBlog(String email,String name, EcomBlogType pluginType){
		 EcomBlogPlugin plugin = EcomBlogPlugin.findEcomBlogPlugin(email.toLowerCase(), name.toLowerCase(),pluginType); 
		 return  plugin!=null ? plugin.hasEcomBlog(pluginType):false;	  
	 }
	 
	 public static boolean hasEcomBlog(String name, EcomBlogType pluginType){
		 EcomBlogPlugin plugin = EcomBlogPlugin.findEcomBlogPlugin(name,pluginType); 
		 
		 return  plugin!=null ? plugin.hasEcomBlog(pluginType):false;	  
	 }
	
	 
	 public boolean addShopifyScriptTag(String timestamp){
		 
		 if(this.getEb_pluginType().compareTo(EcomBlogType.Shopify) != 0)
			 return false;//throw new IllegalAccessException("The type of this ecommerce is not Shopify.");
		 
		 try{
			
			 ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, 
					 this.getEb_name(),this.getEb_apikey(),timestamp,this.getEb_secretKey());
			 
			 ShopifyClient client = new ShopifyClient(connect.getCredential());			 
			 ScriptTagsService scriptTag = client.constructService(ScriptTagsService.class);
			  
			 String jsScript; 
			 
			 if(play.Play.mode.isDev())  
			   jsScript="//appengine.dropifi.com/public/client/javascripts/iframe/new_widget/dropifi_widget.shopify.js"; 	
			 else
				 jsScript= "//s3.amazonaws.com/dropifi/js/widget/dropifi_widget.shopify.js"; 
			 
			 List<LinkedHashMap> tags = scriptTag.getScriptTagsBySrc(jsScript);
			 
			//play.Logger.info("Access Token 3: " + tags.size());
		    //add the script to the shop's script tags
			 
		    if(tags.size()<=0){
			   ScriptTag dscript = new ScriptTag();
			   dscript.setEvent("onload");
			   dscript.setSrc(jsScript);			
			   scriptTag.createScriptTag(dscript);
		    }
		    
		 } catch (Exception e){
			 play.Logger.log4j.info(e.getMessage(), e);
			 return false;
		 }		 
		 
		 return true;
	 }
	 
	 public boolean addShopifyOAuthScriptTag() {
		 return addShopifyOAuthScriptTag(this.getEb_apikey(), "", "");
	 }
	 
	 public boolean addShopifyOAuthScriptTag(String access_token, String timestamp, String signature){
		 
		 if(this.getEb_pluginType().compareTo(EcomBlogType.Shopify) != 0)
			 return false; //throw new IllegalAccessException("The type of this ecommerce is not Shopify.");
		 
		 try{
			if(access_token==null)
				access_token = this.getEb_apikey();
			
			ShopifyAPIConnect connect = new ShopifyAPIConnect(DropifiTools.shopifiApikey, DropifiTools.shopifiSecretKey, 
					 this.getEb_name(),access_token,timestamp,signature);
			ScriptTagsService scriptTag = connect.GetClient().constructService(ScriptTagsService.class);
			 
			 String jsScript;
			 if(play.Play.mode.isDev()) { 
			   jsScript = "//appengine.dropifi.com/public/client/javascripts/iframe/new_widget/dropifi_widget.shopify.js";
			   
			 } else {  
				 	jsScript = "//api.dropifi.com/widget/dropifi_widget.shopify.js";  
				 	
					try {
						 //Remove old scripttag 
						List<LinkedHashMap> oldtags = scriptTag.getScriptTagsBySrc("//s3.amazonaws.com/dropifi/js/widget/dropifi_widget.shopify.js"); 
						for(LinkedHashMap tag : oldtags) { 
							scriptTag.deleteScriptTag(Integer.valueOf(tag.get("id").toString()));
						}
					}catch(Exception e) {
						play.Logger.log4j.info(e.getMessage(),e);
					}
					
					try {
						//Remove old scripttag 
						List<LinkedHashMap> oldtags = scriptTag.getScriptTagsBySrc("https://s3.amazonaws.com/dropifi/js/widget/dropifi_widget.shopify.js"); 
						for(LinkedHashMap tag : oldtags) {
							scriptTag.deleteScriptTag(Integer.valueOf(tag.get("id").toString()));
						}
					}catch(Exception e) {
						play.Logger.log4j.info(e.getMessage(),e);
					}  
			 }
			 
			 List<LinkedHashMap> tags = null;
			 
			 try {
				 tags = scriptTag.getScriptTagsBySrc(jsScript);
			 }catch(Exception e) {
				 play.Logger.log4j.info(e.getMessage(), e);
			 }
			 
			 //add the script to the shop's script tags 
			 if(tags==null || tags.size()<=0){
				  ScriptTag dscript = new ScriptTag(); 
				  dscript.setEvent("onload");
				  dscript.setSrc(jsScript);
				  scriptTag.createScriptTag(dscript);
			 }
		 } catch (Exception e){
			 play.Logger.log4j.info(e.getMessage(), e);
			 return false;
		 }		 
		 
		 return true;
	 }
	 
	//====================================================================
	//finders 	
	public static EcomBlogPlugin findEcomBlogPlugin(String email, String name, EcomBlogType pluginType){
		return  Company.find("select c.ecomBlogPlugin from Company c where c.ecomBlogPlugin.eb_email=? " +
					"and c.ecomBlogPlugin.eb_name=? and c.ecomBlogPlugin.eb_pluginType=?", email,name,pluginType).first();
	}
	
	public static EcomBlogPlugin findEcomBlogPlugin(String name, EcomBlogType pluginType){
		return  Company.find("select c.ecomBlogPlugin from Company c where c.ecomBlogPlugin.eb_name=? and c.ecomBlogPlugin.eb_pluginType=?",name,pluginType).first();
	}
	
	public static List<EcomBlogPlugin> findEcomBlogPlugin(EcomBlogType pluginType){
		return  Company.find("select c.ecomBlogPlugin from Company c where c.ecomBlogPlugin.eb_pluginType=? And c.apikey not like ?",pluginType,"delete_%").fetch();
	}
	
	public static String findEcomBlogPluginName(String apikey,EcomBlogType pluginType){
		return  Company.find("select c.ecomBlogPlugin.eb_name from Company c where c.apikey and c.ecomBlogPlugin.eb_pluginType=?",apikey,pluginType).first();
	}
		
	public static EcomBlogPlugin findEcomBlogPlugin(String email, String name){
		return Company.find("select c.ecomBlogPlugin from Company c where c.ecomBlogPlugin.eb_email=? and c.ecomBlogPlugin.eb_name=?", email,name).first();
	}
	
	//=====================================================================
	//setters and getters
	
	public void setEb_email(String eb_email) {
		this.eb_email = eb_email;
	}

	public String getEb_name() {
		return eb_name;
	}

	public void setEb_name(String eb_name) {
		this.eb_name = eb_name;
	}

	public String getEb_apikey() {
		return eb_apikey;
	}

	public void setEb_apikey(String eb_apikey) {
		this.eb_apikey = eb_apikey;
	}

	public String getEb_secretKey() {
		return eb_secretKey;
	}

	public void setEb_secretKey(String eb_secretKey) {
		this.eb_secretKey = eb_secretKey;
	}

	public EcomBlogType getEb_pluginType() {
		return eb_pluginType;
	}

	public void setEb_pluginType(EcomBlogType eb_pluginType) {
		this.eb_pluginType = eb_pluginType;
	}
	
	public String loginEmail(){
		return this.getEb_email();
	}
	
	public String getDomain(){
		//replace any appearance of special character like . : with _
		switch(this.getEb_pluginType().ordinal()){
			case 2:
				return this.getEb_name().replace('.', '_').replace(':', '_').trim(); 
			case 5:
				return this.getDomain(); 
			case 10:
				return this.getEb_name();
			default:
				return null;
		
		}
	}
	
	 
}
