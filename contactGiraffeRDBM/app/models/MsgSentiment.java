package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.sql.Timestamp; 

import javax.persistence.*;

import org.hibernate.annotations.Index;

import models.dsubscription.SubServiceName;
import models.dsubscription.SubServiceType; 
import play.db.jpa.*;
import resources.CacheKey;
import resources.SQLInjector;
import resources.helper.ServiceSerializer;
import resources.sentiment.SentimentBaseSerializer;
import resources.sentiment.lymbix.*;

/**
 * provides the emotional definition, tone and meaning of a message
 */
@Entity
public class MsgSentiment  extends Model{

	@Index(name="messageId") @Column(nullable = false)
	private Long messageId;
	@Index(name="email") @Column(nullable = false)
	private String email;
	@Index(name="companyId") @Column(nullable = false)
	private Long companyId;
	
	//summary of the sentiment on a message
	@Column(length=1000)
	private String intenseSentence;
	private String dominantEmotion;
	private String articleSentiment;
	private Double score;
	private Double intensity;
	private Double clarity;
	
	/**
	 * detail sentiment on message
	 */
	
	//positives
	private Double AffectionFriendliness;	 
	private Double EnjoymentElation;	 
	private Double AmusementExcitement;	 
	private Double ContentmentGratitude;
	
	//negatives
	private Double SadnessGrief;	 
	private Double AngerLoathing;	
	private Double FearUneasiness;	
	private Double HumiliationShame;
	
	private Timestamp createdDate;
	@Index(name="sourceofMsg")
	private String sourceofMsg;
	
	@Index(name="isArchived")
	private boolean isArchived; 
	
	//------------------------------------------------------------------------------------------
	//constructor
	
 	public MsgSentiment(Long messageId,String email,Long companyId,String sourceOfMsg, ArticleInfo articleInfo){
		
		this.setMessageId(messageId);
		this.setEmail(email);
		this.setCompanyId(companyId);		
		
		if(articleInfo!=null){
			this.setIntenseSentence(articleInfo.IntenseSentence.Sentence);		
			this.setDominantEmotion(articleInfo.DominantCategory);
			this.setArticleSentiment(articleInfo.ArticleSentiment.SentimentType);
			this.setScore(articleInfo.ArticleSentiment.Score);
			this.setIntensity(articleInfo.IntenseSentence.Intensity);
			this.setClarity(articleInfo.Clarity);
			
			this.setAffectionFriendliness(articleInfo.AffectionFriendliness);
			this.setEnjoymentElation(articleInfo.EnjoymentElation);
			this.setAmusementExcitement(articleInfo.AmusementExcitement);
			this.setContentmentGratitude(articleInfo.ContentmentGratitude);
			
			this.setSadnessGrief(articleInfo.SadnessGrief);
			this.setAngerLoathing(articleInfo.AngerLoathing);
			this.setFearUneasiness(articleInfo.FearUneasiness);
			this.setHumiliationShame(articleInfo.HumiliationShame);
		}else{
			this.setIntenseSentence("");		
			this.setDominantEmotion("NoSentiment");
			this.setArticleSentiment("NoSentiment");
			this.setScore(0.0);
			this.setIntensity(0.0);
			this.setClarity(0.0);
			
			this.setAffectionFriendliness(0.0);
			this.setEnjoymentElation(0.0);
			this.setAmusementExcitement(0.0);
			this.setContentmentGratitude(0.0);
			
			this.setSadnessGrief(0.0);
			this.setAngerLoathing(0.0);
			this.setFearUneasiness(0.0);
			this.setHumiliationShame(0.0);
		}
		this.setSourceofMsg(sourceOfMsg);
		this.setArchived(false);
	}
 	
public MsgSentiment(Long messageId,String email,Long companyId,String sourceOfMsg,SentimentBaseSerializer sentiment){
		
		this.setMessageId(messageId);
		this.setEmail(email);
		this.setCompanyId(companyId);		
		
		if(sentiment!=null){
			this.setIntenseSentence(sentiment.getIntense());		
			this.setDominantEmotion(sentiment.getEmotion());
			this.setArticleSentiment(sentiment.getSentiment());
			this.setScore(sentiment.getScore()); 
		}else{
			this.setIntenseSentence("");		
			this.setDominantEmotion("NoSentiment");
			this.setArticleSentiment("NoSentiment");
			this.setScore(0.0); 
		}
		
		this.setIntensity(0.0);
		this.setClarity(0.0);
		
		this.setAffectionFriendliness(0.0);
		this.setEnjoymentElation(0.0);
		this.setAmusementExcitement(0.0);
		this.setContentmentGratitude(0.0);
		
		this.setSadnessGrief(0.0);
		this.setAngerLoathing(0.0);
		this.setFearUneasiness(0.0);
		this.setHumiliationShame(0.0);
		this.setSourceofMsg(sourceOfMsg);
		this.setArchived(false);
	}
 
	//--------------------------------------------------------------------------------------------
	//methods
	
	public int createSentiment(Connection conn) throws SQLException{
		
		if(conn !=null && !conn.isClosed()){
						
			//create an insert statement for MsgSentiment
			PreparedStatement sentState = conn.prepareStatement(
				"INSERT INTO MsgSentiment (AffectionFriendliness,AmusementExcitement,AngerLoathing,ContentmentGratitude,"+
				"EnjoymentElation,FearUneasiness,HumiliationShame,SadnessGrief,articleSentiment,clarity,"+
				"dominantEmotion,intenseSentence,intensity,score,messageId,email,companyId,createdDate,sourceofMsg,isArchived)" +
				"VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
			); 
			
			int i=1;
			
			//add the sentiment values to the query
			sentState.setDouble(i++, this.getAffectionFriendliness());
			sentState.setDouble(i++, this.getAmusementExcitement());
			sentState.setDouble(i++, this.getAngerLoathing());
			sentState.setDouble(i++, this.getContentmentGratitude());
			sentState.setDouble(i++, this.getEnjoymentElation());
			sentState.setDouble(i++, this.getFearUneasiness());
			sentState.setDouble(i++, this.getHumiliationShame());
			sentState.setDouble(i++, this.getSadnessGrief());
			sentState.setString(i++,  this.getArticleSentiment());
			sentState.setDouble(i++, this.getClarity());
			sentState.setString(i++,  this.getDominantEmotion());
			sentState.setString(i++,  this.getIntenseSentence());
			sentState.setDouble(i++, this.getIntensity());			 
			sentState.setDouble(i++, this.getScore());
			sentState.setLong(i++, 	 this.messageId); 
			sentState.setString(i++,  this.getEmail());
			sentState.setLong(i++,   this.getCompanyId()); 
			
			//set created date
			this.setCreatedDate();
			sentState.setTimestamp(i++, this.getCreatedDate());
			sentState.setString(i++, this.getSourceofMsg());
			sentState.setBoolean(i++, this.isArchived());
			//add the sentiment to database
			return sentState.executeUpdate()>0?200:201;
		 
		}
		
		return 501;
	}
	
	public boolean hasExceededLimit(String apikey){
		return hasExceededLimit(this.getCompanyId(), apikey);
	}
	
	public static boolean hasExceededLimit(long companyId,String apikey){
		try{
			ServiceSerializer service = CacheKey.getServiceSerializer(apikey, CacheKey.Sentiment, SubServiceName.Sentiment);
			if(service!=null){
				 long consumedVolume = countSentiments(companyId);
				return service.hasExceedLimit(SubServiceType.FixedVolume, consumedVolume);
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return false;
	}
	
	public static long countSentiments(long companyId){
		try{
			return MsgSentiment.find("Select count(m.companyId) From MsgSentiment m Where m.companyId=?", companyId).first(); 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return 0l;
	} 
	//-------------------------------------------------------------------------------------------
	//getters and setters
	
	@PrePersist
	private void setCreatedDate(){		 
		this.createdDate = new Timestamp(new  Date().getTime()); 
	}
	
	public Timestamp getCreatedDate(){
		return this.createdDate;
	}
	
 	public String getIntenseSentence() {
		return intenseSentence;
	}
	
	private void setIntenseSentence(String intenseSentence) {
		this.intenseSentence = intenseSentence;
	}
	
	public String getDominantEmotion() {
		return dominantEmotion;
	}
	
	private void setDominantEmotion(String dominantEmotion) {
		this.dominantEmotion =  dominantEmotion;
	}
	
	public String getArticleSentiment() {
		return articleSentiment;
	}

	private void setArticleSentiment(String articleSentiment) {
		this.articleSentiment = articleSentiment;
	}
	
	public Double getScore() {
		return score;
	}

	private void setScore(Double score) {
		this.score = score;
	}
	
	public Double getIntensity() {
		return intensity;
	}

	private void setIntensity(Double intensity) {
		this.intensity = intensity;
	}
	
	public Double getClarity() {
		return clarity;
	}

	private void setClarity(Double clarity) {
		this.clarity = clarity;
	}
	
	public Double getAffectionFriendliness() {
		return AffectionFriendliness;
	}

	private void setAffectionFriendliness(Double affectionFriendliness) {
		AffectionFriendliness = affectionFriendliness;
	}
	
	public Double getEnjoymentElation() {
		return EnjoymentElation;
	}

	private void setEnjoymentElation(Double enjoymentElation) {
		EnjoymentElation = enjoymentElation;
	}
	
	public Double getAmusementExcitement() {
		return AmusementExcitement;
	}

	private void setAmusementExcitement(Double amusementExcitement) {
		AmusementExcitement = amusementExcitement;
	}
	
	public Double getContentmentGratitude() {
		return ContentmentGratitude;
	}

	private void setContentmentGratitude(Double contentmentGratitude) {
		ContentmentGratitude = contentmentGratitude;
	}
	
	public Double getSadnessGrief() {
		return SadnessGrief;
	}

	private void setSadnessGrief(Double sadnessGrief) {
		SadnessGrief = sadnessGrief;
	}
	
	public DropifiMessage getMessage() {
		return DropifiMessage.findById(this.messageId);
	}

	private void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	
	public Double getAngerLoathing() {
		return AngerLoathing;
	}

	private void setAngerLoathing(Double angerLoathing) {
		AngerLoathing = angerLoathing;
	}

	public Double getFearUneasiness() {
		return FearUneasiness;
	}

	private void setFearUneasiness(Double fearUneasiness) {
		FearUneasiness = fearUneasiness;
	}

	public Double getHumiliationShame() {
		return HumiliationShame;
	}

	private void setHumiliationShame(Double humiliationShame) {
		HumiliationShame = humiliationShame;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getSourceofMsg() {
		return sourceofMsg;
	}

	public void setSourceofMsg(String sourceofMsg) {
		this.sourceofMsg = sourceofMsg;
	}

	public boolean isArchived() {
		return isArchived;
	}

	public void setArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}

	
}
