package models;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.persistence.*;

import org.hibernate.annotations.Index;

import models.addons.AddonSubscription;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubServiceName;
import adminModels.DropifiMailer;

import com.apache.commons.codec.binary.Hex;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

import play.data.validation.*;
import play.db.jpa.*;
import play.libs.Codec;
import resources.AddonHelper;
import resources.CacheKey;
import resources.DCON;
import resources.DropifiTools;
import resources.UserType; 
import resources.addons.AddonStatus;
import resources.helper.ServiceSerializer;
import view_serializers.MailSerializer;
import view_serializers.MbSerializer;
import view_serializers.UserRoleSerializer;

@Entity
public class AccountUser extends Model implements Serializable{

	@Embedded 
	private UserProfile profile;
	
	@Column(nullable=false) @Required
	private String hashPassword;	
	
	@Transient 
	private String password; 
	
	@Column(nullable = false) @Required
	private UserType userType;
	
	@Index(name="apikey") @Column(nullable = false)
	private String apikey; 
	
	@Column(nullable = false)
	private boolean confirmed;
 
	@Column(nullable = false)
	private boolean isReset; 
	
	private String resetPassword;
	private String resetEmail; 
	
	public AccountUser(String username,String email, String password,String apikey){ 
		this.setProfile(new UserProfile(username, email));
		this.setPassword(password); 	
		this.setApikey(apikey);
	}
	
	public AccountUser(String email,String apikey){
		this("",email,"",apikey);
	}
	
	public AccountUser(String username, String email,String apikey){
		this(username,email,"",apikey);
	}
	
	
	//---------------------------------------------------------------------------------------------------
	//
 
	public AccountUser createAdmin(){
		this.setUserType(UserType.Admin);
		this.setConfirmed(true);
		this.setIsReset(false);
		this.getProfile().setActivated(true);
		return createUser();
	}
	
	public int createAgent(){
		this.setUserType(UserType.Agent);
		
		//check if a user with similar email is not created
		if(findByEmail(this.getProfile().getEmail()) == null){
			
			//randomly generate a password
			String password = Codec.hexMD5(String.valueOf(new Date().getTime()));
			//extract the first five characters			
			password = (password.length()>5?password.substring(1,5):password);
			this.setPassword(password); 
			this.setIsReset(false); 
			//send a message to the user to confirm the account creation
			DropifiMailer mailer = DropifiMailer.getDefault();
			int status = 200; 
			try{
				
				//Set the campaign if the mailbox is mailgun
				if(mailer.getMailbox().getMailserver().equals("Mailgun")){
					mailer.setMgCampaignId(play.Play.mode.isProd()?"dropifi_inapp_mail":"dropifi_inapp_dev");
				}
				  
				//temporary template
				StringBuilder message = new StringBuilder();
				message.append("<div><div>Hi, "+this.profile.getUsername()+", </div>");
				message.append("<div>Login into your Dropifi Account with the following credentials</div>");
				message.append("<div>Email: <span>"+ this.profile.getEmail()+"</span></div>");
				message.append("<div>Password: <span>"+password+"</span></div>");
				message.append("<div><a href='http://www.dropifi.com/login'>click this link to login - http://www.dropifi.com/login</a></div></div>");
				
				status = mailer.sendSimpleMail(this.profile.getEmail(), this.profile.getUsername(),	"You have been added as a user at dropifi.com", message.toString(),null);
				
			} catch (Exception e) {  
				// TODO Auto-generated catch block
				status = 602;
			}
			
			if(status ==200){
				this.setConfirmed(false);
				this.getProfile().setActivated(true);
				return createUser()!=null?200:201;
			}
			
			return status;
		}
		return 305;
	}
	
	public static int updateAgent(String email,String apikey, String username,List<String>userRoles ){
		AccountUser user = AccountUser.findByAgent(email, apikey);
		if(user != null){
			if(userRoles==null){
				userRoles = new LinkedList<String>();
				userRoles.add(DropifiTools.WIDGET_MESSAGE);
				userRoles.add(DropifiTools.ANALYTICS);
			} 
			
			user.getProfile().setUsername(username);
			user.getProfile().setActivated(true);
			user.addRoles(userRoles);
			long datetime = new Date().getTime();
			user.getProfile().setUpdated(new Timestamp(datetime));
			return user.save() !=null? 200:201; 
		}		
		return 501;
	}
	
	public static int updateUserEmail(String apikey, String currentEmail, String newEmail){ 
		int result = DropifiResponse.em().createQuery("Update AccountUser a Set a.profile.email = :newEmail Where a.apikey =:apikey and a.profile.email= :currentEmail")
				.setParameter("apikey",apikey)
				.setParameter("currentEmail", currentEmail)
				.setParameter("newEmail", newEmail).executeUpdate();
		return result>0?200:201;
	}
	
	public int createAgent(List<String>userRoles){
		//add agent roles
		if(userRoles==null){
			userRoles = new LinkedList<String>();
			userRoles.add(DropifiTools.WIDGET_MESSAGE);
			userRoles.add(DropifiTools.ANALYTICS);
		} 
		
		this.addRoles(userRoles);
		return createAgent();
	}
		
	private AccountUser createUser(){
		long datetime = new Date().getTime();
		this.profile.setCreated(new Timestamp(datetime));
		this.profile.setUpdated(new Timestamp(datetime));
		
		if(this.validateAndSave()){
			return this;
		}
		return null;
	}
	
	
	
	public int changeEmail(String email){
		try{
			String currentEmail = this.getProfile().getEmail();
			this.getProfile().setEmail(email);
			this.setResetEmail(null);
			if(this.validateAndSave()){
				if(this.isAdmin()){
					Company.updateUserEmail(this.getApikey(), currentEmail, email); 
					SubCustomer.updateEmail(this.getApikey(), currentEmail,email);
					WixDefault.updateUserEmail(this.getApikey(), currentEmail, email);
					
					AddonHelper.updateEmail(this.getApikey(), email);
				}
				Widget.updateDefaultEmail(this.getApikey(), currentEmail, email);
				DropifiResponse.updateUserEmail(this.getApikey(), currentEmail, email);		
				return 200;
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			return 201;
		}
		
		return 501; 
	}
	
	private void addRoles(List<String>userRoles){
		List<AgentRole> agentRoles = new ArrayList<AgentRole>();
		if(userRoles !=null){
			ListIterator<String> listRoles = userRoles.listIterator();				
			
			while(listRoles.hasNext()){
				String role = listRoles.next();
				if(!this.containsRole(role))
					agentRoles.add(new AgentRole(role));
			}
		}
		this.setAgentRoles(agentRoles);	
	}
	
	/*
	public static int updateLoginDate(String email, String apikey){ 
		try{
			EntityManager em = AccountUser.em();
			return	em.createQuery("update AccountUser u set u.profile.lastLogin = ? where (u.profile.email = ?) and u.apikey =?")
					.setParameter(1, new Timestamp(new Date().getTime())).setParameter(2, email)
					.setParameter(3, apikey).executeUpdate(); 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			return 1;
		}
	}
	*/
	
	public static int updateLoginDate(String email, String apikey){ 
		Connection conn=null;
		try{
			conn = DCON.getDefaultConnection();
			PreparedStatement query = conn.prepareStatement("Update AccountUser set lastLogin=? where email=? and apikey=?");
			Timestamp time = new Timestamp(new Date().getTime());
			query.setTimestamp(1, time);
			query.setString(2, email);
			query.setString(3, apikey);
			return query.executeUpdate();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e); 
			return 1; 
		}finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.info(e.getMessage(),e); 
				}
			}
		}
 
	}
		
	public static int updateFullName( Connection conn,String fullName, String apikey){ 
		try{
			PreparedStatement query = conn.prepareStatement("Update AccountUser set username = ? where  apikey =?");
			query.setString(1, fullName);
			query.setString(2, apikey);
			return query.executeUpdate();
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			return -1;			
		}
	}
	
	
	public static int updateFullName(String fullName, String apikey){ 
		try{
		EntityManager em = AccountUser.em();
		return	em.createQuery("Update AccountUser u Set u.profile.username = :fullName where  u.apikey =:apikey")
				.setParameter("fullName", fullName)
				.setParameter("apikey", apikey).executeUpdate(); 
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
			return  -1;	
		}
	}
	 
	public boolean cancelAccount(String deletekey){
		if(this.isAdmin()){
			List<AccountUser> agents = findAllAgent(this.getApikey());
			
			for(AccountUser agent : agents){
				 agent.delete();
			}
			Date time = new Date();
			this.setApikey(deletekey);
			this.getProfile().setEmail("delete_"+this.getProfile().getEmail()+"_"+time.getTime());
			this.setResetPassword("deleted_"+this.getHashPassword()+"_"+time.getTime());
			return this.validateAndSave();
		}
		return false;
	}
	
	public static AccountUser findByAdmin( String email,String apikey){
		return  findByUserType(email,UserType.Admin,apikey);
	}
	
	public static AccountUser findByAgent(String email,String apikey){
		return findByUserType(email,UserType.Agent,apikey);
	}
	
	public static AccountUser findByUserType(String email, UserType userType, String apikey){
		return (AccountUser)AccountUser.find("select u from AccountUser u where (u.profile.email=?) and " +
				"(u.userType =?) and (u.apikey =?)",email,userType,apikey).first();
	}
	
	public static List<UserProfile> findAllAgentProfile(String apikey){
		List<UserProfile> userProfile = new ArrayList<UserProfile>();
		if(apikey!=null){
			List<AccountUser> accounts = AccountUser.find("Select u from AccountUser u where (u.apikey = ?) and (u.userType =?)", apikey, UserType.Agent).fetch();
			
			if(accounts != null){
				ListIterator<AccountUser> listAccount = accounts.listIterator();
				
				while(listAccount.hasNext()){
					AccountUser  user = listAccount.next();
					userProfile.add(user.getProfile()); 
				}
			}
		}
		return userProfile;
	}
	
	public static List<AccountUser> findAllAgent(String apikey){ 
		return AccountUser.find("select u from AccountUser u where (u.apikey = ?) and (u.userType =?)", apikey, UserType.Agent).fetch(); 
	}
	
	public static AccountUser findByEmailAndApikey(String email, String apikey){
		if(email !=null && apikey!=null){
			return AccountUser.find("select u from AccountUser u, Company c where (u.apikey = c.apikey) and " +
				"(c.apikey =?) and u.profile.email = ? ", apikey,email).first(); 
		}
		return null;
	}
	
	/**
	 * Find the AccountUser profile. The password should not be hash
	 * @param apikey
	 * @param email
	 * @param password
	 * @return
	 */
	public static AccountUser findByPassword(String apikey,String email, String password){
		if(email !=null && apikey!=null){		 
			return AccountUser.find("select u from AccountUser u, Company c where (u.apikey = c.apikey) and " +
				"(c.apikey =?) and u.profile.email = ? and u.hashPassword = ?", apikey,email,Codec.hexMD5(password)).first(); 
		}
		return null;
	}
	
	public static AccountUser findByEmailResetKey(String email, String resetkey){
		if(email !=null && resetkey!=null){ 		 
			return AccountUser.find("select u from AccountUser u Where u.profile.email = ? and u.resetEmail = ?", email, resetkey).first(); 
		}
		return null;
	}
	
	public static AccountUser findByEmail(String email){
		if( email != null )
			return AccountUser.find("select u from AccountUser u where u.profile.email = ? ",email).first() ;
		return null;
	}
	
	public static String findByName(String email,String apikey){
		if( email != null && apikey!=null )
			return AccountUser.find("select u.profile.username from AccountUser u where u.profile.email = ? and u.apikey=?",email,apikey).first() ;
		return "";
	}
	
	public static AccountUser findByResetKey(String resetPassword,String email){
		if( email != null )
			return AccountUser.find("select u from AccountUser u where " +
					"u.resetPassword=? and u.profile.email = ? and u.isReset = ?",resetPassword,email,true).first() ;
		return null; 
	}
	
	public static Map<String,String> findUserEmailName(String apikey){
		Connection conn = DCON.getDefaultConnection();
		try{
			if(conn!=null && !conn.isClosed()){ 
				PreparedStatement query = conn.prepareStatement("Select distinct u.email as email, u.username as username from AccountUser u where u.apikey =?");
				query.setString(1, apikey);
				ResultSet result = query.executeQuery();
				Map<String, String> users = new HashMap<String,String>();
				while(result.next()){ 
					users.put(result.getString("email"), result.getString("username")); 
				}
				return users;
			}
		}catch(Exception e){
			return null;
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.info(e.getMessage(), e);
			}
		}
		return null;
	}
	
 	public int changePassword(String current, String newPassword){
		String pass = Codec.hexMD5(current);
		if(pass.compareTo(this.getHashPassword()) == 0 ){
			this.setPassword(newPassword); 
			this.setHashPassword(); 
			return this.validateAndSave() ? 200:201; 
		}else{
			return 501;
		} 	 
	}
 	
 	public int changePassword(String newPassword){		 
		this.setPassword(newPassword); 
		this.setHashPassword();
		this.setIsReset(false);
		this.setResetPassword("");
		return this.validateAndSave() ? 200:201;
	}
	
	public int requestPasswordReset(){  
		this.setResetPassword(Codec.hexMD5(new Date().toString()));
		this.setIsReset(true);    
		return this.save() !=null ? 200:201; 	 
	}
	
	public int requestEmailReset(){ 
		this.setResetEmail(Codec.hexMD5(new Date().toString()));
		return this.validateAndSave() ? 200:201;	 
	}
	
	/**
	 * Reset password email message
	 * @return
	 */
	public String getResetMessage(){
		StringBuilder msg = new StringBuilder();
		msg.append("<!DOCTYPE html><html><head><title>Request For Reset Of Your Dropifi Password</title></head><body>");
		msg.append("<div><p>Hi "+this.getProfile().getUsername()+", </p>");
		msg.append("<p>Sorry, you forgot your Dropifi.com password. Smile, a wonderful day awaits you!</p>");
		msg.append("<p>Just head over to <a href='"+DropifiTools.ServerURL+"/reset_user_password/"); 
		msg.append(this.getProfile().getEmail());
		msg.append("/");
		msg.append(this.getResetPassword());
		msg.append("/");
		msg.append(this.isReset());
		msg.append("'>"+DropifiTools.ServerURL+"/reset_user_password</a> and create a new password.</p>"); 
		msg.append("<p>If you have any questions, send them our way via team@dropifi.com</p>"); 
		msg.append("<p>Have a wonderful day engaging your customers with Dropifi!</p></div>");
		msg.append("</body></html>");

		return msg.toString();
	}
	
	public String getResetEmailMessage(String newEmail){
		StringBuilder msg = new StringBuilder();
		msg.append("<!DOCTYPE html><html><head><title>Request For Reset Of Your Dropifi Password</title></head><body>");
		msg.append("<div><p>Hi "+this.getProfile().getUsername()+", </p>");
		msg.append("<p>You made a request to reset your account email. Smile, a wonderful day awaits you!</p>");
		msg.append("<p>Just click this link <strong><a href='"+DropifiTools.ServerURL+"/reset_user_email?current=");
		msg.append(this.getProfile().getEmail());
		msg.append("&email=");
		msg.append(newEmail);
		msg.append("&resetkey=");
		msg.append(this.getResetEmail());
		msg.append("'>"+DropifiTools.ServerURL+"/reset_user_email</a> </strong>to reset your email.</p>");
		msg.append("<p>If you have any questions, send them our way via team@dropifi.com</p>"); 
		msg.append("<p>Have a wonderful day engaging your customers with Dropifi!</p></div>");  
		msg.append("</body></html>");
		
		return msg.toString();
	}
	
 	public static Collection<AgentRole> findAgentRole(String apikey,String email){
		if(apikey!=null && email!=null){
			AccountUser result = AccountUser.find("select a from AccountUser a where a.apikey = ? and a.profile.email = ?",
					apikey, email).first();
			return result!=null?result.profile.getAgentRoles():null;  
		}
		return null;
	}
 	
	public static int deleteAgent(String email,String apikey){
		if(email !=null && apikey!=null){
			AccountUser user = findByAgent(email,apikey);
			if(user !=null){
				return user.delete()!=null?200:201;
			}
		}
		return 501; 
	}
	
	public boolean isAgent(){
		return UserType.Agent.compareTo(this.getUserType()) == 0;
	}
	
	public boolean isAdmin(){
		return UserType.Admin.compareTo(this.getUserType())==0;
	}

	public int validUser(String email, String password){
		
		String hPassword = Codec.hexMD5(password.trim());
		email = email.trim();
		
		if(this.profile.getEmail().equals(email)){
			if(this.getHashPassword().equals(hPassword))
				return 0; 	//the email and password do not match with the user email and password supplied
			else
				return -1;  //the password do not match;
		}else{
			return -2;		//the email address do not match;
		}
	}
	
	public static JsonArray toJsonArray(List<UserProfile> userProfiles){
		JsonArray arrayJson = new JsonArray();
		if(userProfiles!=null){
			Gson gson = new Gson();
			ListIterator<UserProfile> userList = userProfiles.listIterator();		
			int uid = 1;
			while(userList.hasNext()){
				UserProfile prof = userList.next();
				prof.uid = uid++;
				arrayJson.add(gson.toJsonTree(prof));
			}
		}
		return arrayJson;
	}
	
	public static List<String> getAssignedMailboxes(String apikey, String userEmail, UserType userType ){
		List<String> sources =   new LinkedList<String>(); 		
		
		if(userType.compareTo(UserType.Admin) == 0){
			sources = InboundMailbox.findAllEmail(apikey);
			sources.add(DropifiTools.WIDGET_MESSAGE); 
		}else{
			Collection<AgentRole> roles = AccountUser.findAgentRole(apikey, userEmail);	
			
			if(roles != null){
				for(AgentRole role: roles){				 
					if(!role.getRoleName().equalsIgnoreCase("Analytics")){
						sources.add(role.getRoleName()); 
					}
				} 
			}				 
		} 
		return sources; 
	}
	
	public static List<String> getComposeMailboxes(String apikey, String userEmail,UserType userType){
		List<String> sources =   new LinkedList<String>(); 		
		
		if(userType.compareTo(UserType.Admin) == 0){
			sources = InboundMailbox.findAllEmail(apikey);			 
		}else{
			Collection<AgentRole> roles = AccountUser.findAgentRole(apikey, userEmail);				
			if(roles != null){
				for(AgentRole role: roles){				 
					if(!role.getRoleName().equalsIgnoreCase(DropifiTools.WIDGET_MESSAGE) && !role.getRoleName().equalsIgnoreCase("Analytics")){
						if(!sources.contains(role.getRoleName()))
							sources.add(role.getRoleName());
					}
				} 
			}				 
		} 
		return sources; 
	}
	
	public static String getQueryMailboxes(String apikey, String userEmail, UserType userType ){
		String mailbox ="";
		List<String> sources = getAssignedMailboxes(apikey, userEmail, userType);
		if(sources !=null){
			int len = sources.size();
			int start = 1;
			for(String source : sources){
				mailbox += "'"+source+"'";
				if(start < len) 
					mailbox +=" , ";
				
				start++;
			}
		} 		 
		return mailbox; 
	} 
	
	/**
	 * Called any time a user subscribed to a Plan 
	 * Loop through the list of agents(Support staff) and deactivated all agents which index number is more than service required volume
	 * @param apikey
	 */
	public static void autoUpdateAgents(String apikey){
		try{
			List<AccountUser> agents = findAllAgent(apikey);
			
			if(agents!=null){
				ListIterator<AccountUser> agentlist = agents.listIterator();
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.Agent, SubServiceName.SupportAgent);
				long index = 1;  
				while(agentlist.hasNext()){ 
					AccountUser agent = agentlist.next();
					boolean act = true;
					if( !(index++ <=service.getThreshold().getVolume()) ){ 
						agent.getProfile().setKnownActivaton(agent.getProfile().isActivated());
						act = false;
					}else{
						if(agent.getProfile().isKnownActivation()!=null && !agent.getProfile().isActivated()){
							act = agent.getProfile().isKnownActivation();
						}else{
							act = agent.getProfile().isActivated();
							agent.getProfile().setKnownActivaton(act);
						}
					} 
					agent.activateUser(act);
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
 
	//-----------------------------------------------------------------------------------------------------------
	//account agent
	public  int addAgentRole(String role ){ 
		if(this != null){
			List<String> userRoles = new LinkedList<String>();
			if(this.getRoles()==null){  
				userRoles.add(DropifiTools.WIDGET_MESSAGE);
				userRoles.add(DropifiTools.ANALYTICS);
			}
			userRoles.add(role);   
			this.addRoles(userRoles);
			long datetime = new Date().getTime();
			this.getProfile().setUpdated(new Timestamp(datetime));
			return this.save() !=null? 200:201; 
		}		
		return 501;
	}
	
	public  int updateAgentRole(String role, String uprole ){ 
		if(this != null){
			List<String> userRoles = new LinkedList<String>();
			if(this.getRoles()==null){  
				userRoles.add(DropifiTools.WIDGET_MESSAGE);
				userRoles.add(DropifiTools.ANALYTICS);
			}
			if(!role.equals(uprole)){
				this.removeRole(uprole);
			}	
			userRoles.add(uprole);  
			this.addRoles(userRoles);
			long datetime = new Date().getTime();
			this.getProfile().setUpdated(new Timestamp(datetime));
			return this.save() !=null? 200:201; 
		}		
		return 501;
	}
	
	public static int addAgentRole(String email,String apikey, String username,String role ){
		AccountUser user = AccountUser.findByAgent(email, apikey);
		if(user != null){
			List<String> userRoles = new LinkedList<String>();
			if(user.getRoles()==null){  
				userRoles.add(DropifiTools.WIDGET_MESSAGE);
				userRoles.add(DropifiTools.ANALYTICS);
			}
			userRoles.add(role); 
			user.getProfile().setUsername(username);
			user.getProfile().setActivated(true);
			user.addRoles(userRoles);
			long datetime = new Date().getTime();
			user.getProfile().setUpdated(new Timestamp(datetime));
			return user.save() !=null? 200:201; 
		}		
		return 501;
	}
	
 	public int addRole(AgentRole role){ 	 
		
		if(!containsRole(role.getRoleName())){
			Timestamp time = new Timestamp(new Date().getTime());
			role.setCreated(time);			 
			 if(this.profile.getAgentRoles().add(role)){ 
				 return this.validateAndSave() ? 200:201; 	
			 }
			 return 203;
		 } 			
		 return 501;
	}
		
	public boolean containsRole(String role){
		return this.findRoleByName(role) != null; 
	}
	
	public void setAgentRoles(Collection<AgentRole> agentRoles){
		if(this.profile != null)
			this.profile.setAgentRoles(agentRoles);
	}
	
	public AgentRole findRoleByName(String role){
		if(this.profile !=null && this.profile.getAgentRoles()==null)
			return null;
		
		role = role.trim(); 		
		if(!role.isEmpty()){
			 Iterator<AgentRole> listRole = this.profile.getAgentRoles().iterator();
			
			while(listRole.hasNext()){
				AgentRole key = listRole.next();
				if(key!=null && key.getRoleName().equalsIgnoreCase(role))
					return key;
			}
		}
		return null; 
	}
		
	public int removeRole(String role){
		 
		for(AgentRole r: this.profile.getAgentRoles()){
			
			if(r.getRoleName().equalsIgnoreCase(role)){
				
				boolean b =this.profile.getAgentRoles().remove(r);
				
				if(b){
					this.save();
					return 200;
				}
				return 201;
			}
		}
		return 501;
	}
	
	public Collection<AgentRole> getRoles(){
		return this.profile.getAgentRoles();
	}
		
	public List<UserRoleSerializer> assignedRoles(){
		List<String> mailboxes =  InboundMailbox.findAllEmail(this.getApikey()); 
    	mailboxes.add(DropifiTools.WIDGET_MESSAGE);
    	     	
    	Collection<AgentRole> listRoles = this.getRoles();
    	
    	//create a list of user roles
    	List<UserRoleSerializer> userRoles = new ArrayList<UserRoleSerializer>();
    	
    	 ListIterator<String> listMailbox = mailboxes.listIterator();
    	 int n = 1;
    	 while(listMailbox.hasNext()){
    		 
    		String mailbox = listMailbox.next();
    		UserRoleSerializer userRole = new UserRoleSerializer(n++,mailbox);    		
    		
    		Iterator<AgentRole> roles = listRoles.iterator();
    		
    		while(roles.hasNext()){
    			AgentRole role = roles.next(); 
    	
    			//check if the role name and the mailbox match. if true mark role as activated     		
    			if(role.getRoleName().equalsIgnoreCase(mailbox)){
    				userRole.setActivated(true);
    				break;
    			}
    		}     		 
    		userRoles.add(userRole);
    	 }
    	
		return userRoles;
	}
	
	public static List<UserRoleSerializer> unAssignedRoles(String apikey){
		List<String> mailboxes =  InboundMailbox.findAllEmail(apikey); 
    	mailboxes.add(DropifiTools.WIDGET_MESSAGE);
    	
    	//create a list of user roles
    	List<UserRoleSerializer> userRoles = new ArrayList<UserRoleSerializer>();
    	
    	 ListIterator<String> listMailbox = mailboxes.listIterator();
    	 int n =1;
    	 while(listMailbox.hasNext()){    		 
    		String mailbox = listMailbox.next();    		 
    		userRoles.add(new UserRoleSerializer(n++,mailbox));
    	 }
    	
		return userRoles;
	}
	
	public int activateUser(boolean activated){
		int result = 501;
		try{
			if(this.isAgent()){
				this.getProfile().setActivated(activated);
				this.getProfile().setUpdated(new Timestamp(new Date().getTime()));
				result = this.validateAndSave() ? 200:201;
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return result;
	}
	
	//-----------------------------------------------------------------------------------------------------------
	//getters and setters 
	
	public String getHashPassword() {
		return hashPassword;
	}

	public void setHashPassword() {
		this.hashPassword =  Codec.hexMD5(this.getPassword());
		 
	}
 
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password; 
		this.setHashPassword();
	}

	public UserType getUserType() {
		return userType;
	}

	private void setUserType(UserType userType) {
		this.userType = userType;
	}
	 
	public String getApikey() {
		return apikey;
	}
	
	private void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public boolean getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	public UserProfile getProfile() {
		return profile;
	}
	
	public void setProfile(UserProfile profile) {
		this.profile = profile;
	}

	public boolean isReset() {
		return isReset;
	}

	public void setIsReset(boolean reset) {
		this.isReset = reset;
	}

	public String getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(String resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getResetEmail() {
		return resetEmail;
	}

	public void setResetEmail(String resetEmail) {
		this.resetEmail = resetEmail;
	}
			
}
