package models;
/**
 * Provider mailing server settings for receiving email from clients(customers) to company 
 * @author phillips
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import models.dsubscription.SubServiceName;

import org.bson.types.ObjectId;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.IndexColumn; 
import org.joda.time.DateTime;
import org.json.JSONObject;

import adminModels.DropifiMailer;

import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
 





import play.Play;
import play.data.validation.*;
import play.db.jpa.Model;
import resources.CacheKey;
import resources.DCON;
import resources.Dcrypto;
import resources.DropifiTools;
import resources.RuleGroup;
import resources.RuleType;
import resources.SQLInjector;
import resources.helper.ServiceSerializer;
import view_serializers.MbSerializer;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException; 
import javax.persistence.*;  

@Entity
public class InboundMailbox extends Model implements Serializable {
	
	@SerializedName("mailserver") @Index(name="mailserver")
	private String mailserver;	
	@Column(nullable=false) @Required @Index(name="companyId") @SerializedName("companyId")
	private Long companyId;
	@Column(nullable=false) @Required
	@SerializedName("username") 
	private String username;
	
	@Column(nullable=false, length=1000)  @Required @Email
	@SerializedName("email") 
	private String email; 
	@SerializedName("password") 
	@Column(nullable=false, length=1000) @Required 
	private String password;
	
	@SerializedName("authName") 
	private String authName;
	
	//imap/pop settings for inbound
	@Column(nullable=false, length=1000) @Required
	@SerializedName("hostType") 
	private String hostType;
	@Column(nullable=false, length=1000) @Required
	@SerializedName("host") 
	private String host;
	@Column(nullable=false) @Required
	@SerializedName("port") 
	private Integer port;	

	//smtp setting for outbound
	@Column(nullable=false)
	@SerializedName("outHostType")
	private String outHostType;
	@Column(nullable=false)	
	@SerializedName("outHost")
	private String outHost;
	@Column(nullable=false)	
	@SerializedName("outPort")
	private Integer outPort;
	
	@Index(name="activated") @Column(nullable=false)
	@SerializedName("activated")
	private boolean activated;
		
	@OneToOne(mappedBy="inboundMb", cascade= CascadeType.ALL)  
	private LastPull lastPull;	//contains date information of the last information pulled 	
	
	@ManyToOne @Required
	@JoinColumn(name="channel_id",nullable=false)
	private Channel channel;
	
	@Index(name="islock") @Column(nullable=false)	
	private boolean islock;	
	@Column(nullable=false)
	private boolean ishidden;
	
	@SerializedName("created") 
	private Timestamp created;
	@SerializedName("updated") 
	private Timestamp updated;
	
	@Required @Column(nullable=false)
	private byte[] secureKey; 
	
	@Required @Column(nullable=false)  @Index(name="domain")
	private String domain;
	
	private boolean isDefault;
	
	@Index(name="monitored")
	private Boolean monitored;
	
	private String emailFilters;
	
	private Timestamp nextPullTime;
	
	private Integer priorityLevel;
	private Boolean knownActivation;
	 
	//-----------------------------------------------------------------------------------------
	//constructor
	
	public InboundMailbox(String mailserver,String username, String email, String password,
			String hostType, String host, int port,String outHostType,String outHost,int outPort) 
					throws NoSuchAlgorithmException {	
		// TODO Auto-generated constructor stub
		this(mailserver,username, email, password, hostType, host, port,outHostType, outHost,outPort,true); 		 
	}
	
	public InboundMailbox(String mailserver, String username, String email, String password,
			String hostType, String host, int port,String outHostType,String outHost,int outPort,boolean activated) 
					throws NoSuchAlgorithmException{ 
		
		// TODO Auto-generated constructor stub
		this.setSecureKey();
		this.setUsername(username);
		this.setEmail(email);
		this.setPassword(password);
		this.setHostType(hostType);
		this.setHost(host);
		this.setPort(port); 	
		this.setActivated(activated);		 
		this.setMailserver(mailserver);
		this.setOutHostType(outHostType);
		this.setOutHost(outHost);
		this.setOutPort(outPort);
	}
 
	public InboundMailbox(){
		 
	} 
	
	@Override
	public String toString() {
		return "InboundMailbox [mailserver=" + mailserver + ", companyId="
				+ companyId + ", username=" + username + ", email=" + email
				+ ", password=" + password + ", authName=" + authName
				+ ", hostType=" + hostType + ", host=" + host + ", port="
				+ port + ", outHostType=" + outHostType + ", outHost="
				+ outHost + ", outPort=" + outPort + ", activated=" + activated
				+ ", lastPull=" + lastPull + ", islock=" + islock
				+ ", ishidden=" + ishidden + ", created=" + created
				+ ", updated=" + updated + ", domain=" + domain
				+ ", isDefault=" + isDefault + ", monitored=" + monitored
				+ ", emailFilters=" + emailFilters + ", nextPullTime="
				+ nextPullTime + ", priorityLevel=" + priorityLevel
				+ ", knownActivation=" + knownActivation + "]";
	}
	
	//---------------------------------------------------------------------------------------------
	//methods for manipulating mailboxes
	
	@PrePersist
	public void preUpdate(){
		this.setUpdated(new Timestamp(new Date().getTime()));
	} 
	
	public int createMailBox(String domain,boolean isOAuth){
		// TODO Auto-generated method stub 	 
		
		//verify that a mailbox with similar email for a company is not created by
		//searching for the mailbox using the companyId and email address of the mailbox
		InboundMailbox mailbox = InboundMailbox.findByEmail(this.getCompanyId(),this.getEmail(),domain); 				
		if(mailbox != null || this.getId() != null){
			//if(mailbox.getIshidden() == false)
			//	return 302; 
			return mailbox.updateMailbox(this,domain,isOAuth);
		}else{
			
			if(domain!=null){
				try { 
					//check if the user settings specified is correct
					int conStatus = checkConnection(this, isOAuth); //(isOAuth==true) ? 200: DropifiMailer.checkConnection(this); 
					
					
					if(conStatus==200){
						this.setDomain(domain);
						//set created date
						this.setCreated(new Timestamp(new Date().getTime())); 					
						
						//validate to verify that all validation rules are met: returns true if record is save;	
						this.setIslock(false);
						this.setIshidden(false); 						
						
						if(!Validation.hasErrors()){
							this.setPriorityLevel((int)new Date().getTime()); 
							/*
							if(this.getMonitored()==null){
								this.setMonitored(true);
							}
							*/ 
							//this.setMonitored(true);
							this.save();
							
							//check if email filters are set
							this.createFilterRules();
							
							InboundMailbox mb = this.setLastPull(null, null, -1); 
							if(mb!=null){ 									
								MsgCounter counter = new MsgCounter(mb.getCompanyId(), mb.getId(), mb.getEmail());
								if(counter.createCounter()==200){
									return 200;
								}else{
									this.getLastPull().delete();
									this.delete();
									return 201;
								} 
							}
							return 202;
						}	
						return 201;
					}else{
						return conStatus;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.info(e.getMessage(),e);
					return 201;		
				} 
			}
			return 501;
		}		 
	}
	
	public static int orderMailboxes(long companyId,String domain,List<String> mailboxes){
		if(mailboxes!=null && mailboxes.size()>0){ 
			for(String mailbox : mailboxes){
				 JsonObject json = (JsonObject) new JsonParser().parse(mailbox); 
				 InboundMailbox found = InboundMailbox.findByEmail(companyId, json.get("email").getAsString(), domain);
				 if(found!=null){
					 found.setPriorityLevel((int)new Date().getTime());
					 Timestamp time = new Timestamp(new Date().getTime());
					 found.setCreated(time);
					 found.setUpdated(time);
					 found.validateAndSave();
				 }
			}
			return 200;
		}
		return 501;
	}
	
	private int checkConnection(InboundMailbox mailbox, boolean isOAuth){
		mailbox.setAuthName(null);
		int conStatus = (isOAuth==true)? 200 : DropifiMailer.checkConnection(mailbox);
		
		if(conStatus==205){ //check using the authName as username if the error is authentication error
			mailbox.setAuthName(this.getEmail().trim().split("@")[0]);
			conStatus = DropifiMailer.checkConnection(mailbox);
		}
		return conStatus;
	}
	
	public int updateMailbox(InboundMailbox mailbox,String domain,boolean isOAuth) {
		// TODO Auto-generated method stub
		
		if(mailbox !=null && this !=null){
						
			//update an existing mailbox without changing the email
			if(this.getEmail().equalsIgnoreCase(mailbox.getEmail())){  	
				
				//check that other settings have not change						
				int conStatus = 200;
				if(this.isChanged(mailbox)){ //check using the email as username 
					
					conStatus =checkConnection(mailbox, isOAuth); //(isOAuth==true)? 200 : DropifiMailer.checkConnection(mailbox); 	 
				}	
				
				if(conStatus == 200){							
					return this.saveUpdateParameters(mailbox); 
				} 
				return conStatus; 
			}else{
				
				//update an existing mailbox as well as changing the email
				Channel channel = this.getChannel();
				
				//check if the mailbox has being used in pulling messages. if true. hide the mailbox
				if(DropifiMessage.containsInboundMailbox(this.getCompanyId(),this.getEmail())){ 
					//test connection to mailbox
					int conStatus = 200;
					if(this.isChanged(mailbox)){
						 
						conStatus =checkConnection(mailbox, isOAuth); //(isOAuth==true)?200: DropifiMailer.checkConnection(mailbox); 
					}	
					
					if(conStatus==200){
						this.setIshidden(true);
						this.save(); 
						return channel.addInboundMb(mailbox,domain,isOAuth);
					}else{ //mailbox validation failed. could not connect to mail mailbox
						return conStatus;
					}
				}else{ 
					
					//check that the update Mailbox email is not created already
					InboundMailbox createdMailbox = InboundMailbox.findByEmail(this.getCompanyId(), mailbox.getEmail(), domain);
					 
					if(createdMailbox == null){	//no mailbox associated with this new mailbox
						 
						int conStatus =checkConnection(mailbox, isOAuth); // (isOAuth==true)?200 : DropifiMailer.checkConnection(mailbox); 	
						
						if(conStatus ==200){
							mailbox.setAuthName(null);
						}else{ //check using the authName as username
							mailbox.setAuthName(this.getEmail().trim().split("@")[0]);
							conStatus = DropifiMailer.checkConnection(mailbox);
						}
						
						if(conStatus==200){
							this.setEmail(mailbox.getEmail()); 									
							return this.saveUpdateParameters(mailbox); 	
						}else{ //mailbox validation failed. could not connect to mail mailbox
							return conStatus;
						}
						
					}else if(createdMailbox.getIshidden() == true){ // mailbox exist but it is hidden
						
						//check if there have been changes to the mailbox settings
						int conStatus = 200;
						if(this.isChanged(mailbox)){								
							conStatus = checkConnection(mailbox, isOAuth); //(isOAuth==true)?200: DropifiMailer.checkConnection(mailbox);
						}	
						
						if(conStatus==200){	
							createdMailbox.setIslock(false);
							int status = createdMailbox.saveUpdateParameters(mailbox); 
							if(status == 200){
								//update the last pull
								LastPull lastPull = LastPull.findByInboundId(this.getId());
								if(lastPull !=null && lastPull.getReceivedDate() !=null){
									 
									//update lastPull record with the new information
									lastPull.setInboundMb(mailbox); 			 
									lastPull.setReceivedDate(null);				
									lastPull.setSentDate(null);
									lastPull.setMsgindex(null); 	
									lastPull.save(); 											
								}
							}
							return status;
							
						}else{ //mailbox validation failed. could not connect to mail mailbox
							return conStatus;
						}
						
					}else{//mailbox is already created
						return 302; 
					}
				}				
			}
		}
		return 505; 
	}
	
	private int saveUpdateParameters(InboundMailbox mailbox){	
		int status=403;
		if(mailbox != null && this !=null){
			 this.setMailserver(mailbox.getMailserver());
			 this.setUsername(mailbox.getUsername());				 
			 this.setPassword(mailbox.getPassword());
			 this.setHost(mailbox.getHost());
			 this.setHostType(mailbox.getHostType());
			 this.setPort(mailbox.getPort());
			 this.setOutHost(mailbox.getOutHost());
			 this.setOutHostType(mailbox.getOutHostType());
			 this.setOutPort(mailbox.getOutPort());			 
			 this.setUpdated(new Timestamp(new Date().getTime()));
			 
			 this.setActivated(mailbox.getActivated());
			 this.setIshidden(false);
			 
			 this.setMonitored(mailbox.getMonitored());
			 this.setEmailFilters(mailbox.getEmailFilters());
			 
			 if(this.getPriorityLevel()==null){
				this.setPriorityLevel(0);
			 }
			 
			 if(this.getMonitored()==null){
				this.setMonitored(false);
			 }
			 
			 this.setAuthName(mailbox.getAuthName());
			 
			 try{				 
				status= this.validateAndSave() ? 200:201;	
				//set email filter rules
				this.createFilterRules(); 
			 }catch(Exception e){
				 play.Logger.log4j.info(e.getMessage());
				status = 201;
			 }
		}
		return status;
	}
	
	public void createFilterRules(){
		String filters = this.getEmailFilters()+",no-reply@dropifi.com,team@dropifi.com"; 
		MsgRule.createFilterRules(filters, this.getCompanyId(), this.getId());
	}
	
	//activate and deactivate a mailbox
	public int updateActivated(boolean activated){ 
		this.setActivated(activated);
		this.setUpdated(new Timestamp(new Date().getTime()));
		try{
			return this.validateAndSave()?200:201;
		}catch(Exception e){
			return 201;
		}		 
	}
	
	//activate and deactivate a mailbox
	public int updateMonitored(boolean monitored){ 
		this.setMonitored(monitored);
		this.setUpdated(new Timestamp(new Date().getTime()));
		try{
			return this.validateAndSave()?200:201;
		}catch(Exception e){ 
			return 201; 
		} 
	}
	 
 	public int removeMailbox() { 
		// TODO Auto-generated method stub
 		if(this!=null && this.getId() != null){
 			try{
		 		//check if a message is associated with this mailbox. if true, deactivate and hide the mailbox
		 		if(DropifiMessage.containsInboundMailbox(this.getCompanyId(), this.getEmail())){
		 			//deactivate and hide the mailbox
		 			this.setActivated(false);
		 			this.setIshidden(true);
		 			this.setMonitored(false);
		 			this.save();
		 		}else{ 		
					if(LastPull.removeLastPull(this.getId()) == 1 )	{	
						MsgCounter.delete("Delete from MsgCounter m where m.companyId = ? and m.sourceOfMsg = ?", this.getCompanyId(),this.getEmail());
						InboundMailbox.delete("Delete from InboundMailbox where companyId = ? and email = ? and domain =?", this.getCompanyId(),this.getEmail(),this.getDomain());						
					}
		 		}
		 		//remove the widget// change to update statement
		 		Widget widget = Widget.findByComIdEMail(this.getCompanyId(),this.getEmail());
		 		if(widget != null){ 
		 			widget.setDefaultEmail("other"); 
		 			widget.save();
		 		} 
 			}catch(Exception e){
 				return 201;
 			}
 			
	 		return 200;
 		}
		return 501;
	}
 	
 	/**
	 * Called any time a user subscribed to a Plan 
	 * Loop through the list of recipients and deactivated all recipients which index number is more than service required volume
	 * @param apikey
	 * @return
	 */
	public static void autoUpateMailboxes(String apikey) {
		try{
			List<InboundMailbox> mailboxes = findAllMailboxes(apikey);
			if(mailboxes!=null){
				ListIterator<InboundMailbox> maillist = mailboxes.listIterator();
				ServiceSerializer service = CacheKey.getServiceSerializer(apikey,CacheKey.MonitoredMailboxes, SubServiceName.MonitoredMailbox);
				long index = 1; 
				while(maillist.hasNext()){
					InboundMailbox mail = maillist.next();
					if(mail!=null){
						boolean moni = true; 
						
						if(!(index++ <=service.getThreshold().getVolume()) ){
							mail.setKnownActivation(mail.getMonitored());
							moni = false; 
						}else{
							if(mail.getKnownActivation()!=null && mail.getMonitored()!=null && !mail.getMonitored()){
								moni = mail.getKnownActivation();
							}else{
								moni = mail.getMonitored()!=null? mail.getMonitored():false;
								mail.setKnownActivation(moni);
							}
						}
						mail.updateMonitored(moni);
					}
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}

	public static boolean containsMailbox(InboundMailbox mailbox,boolean hidden){
		// TODO Auto-generated method stub
		if(mailbox !=null && mailbox.getCompanyId() != null && mailbox.getEmail() != null){
			return findByEmail(mailbox.getCompanyId(), mailbox.getEmail(),hidden) !=null;
		}
		return true; 
	}
	
	public static boolean containsMailbox(InboundMailbox mailbox){
		// TODO Auto-generated method stub
		if(mailbox !=null && mailbox.getCompanyId() != null && mailbox.getEmail() != null){
			return findByEmail(mailbox.getCompanyId(), mailbox.getEmail(),mailbox.getDomain()) !=null;
		}
		return true; 
	}

	//find an email where the hidden status is false
	public static InboundMailbox findByEmail(Long companyId, String email,boolean hidden){
		
		if(companyId != null && email != null && !email.trim().isEmpty()){			
			return InboundMailbox.find("byCompanyIdAndEmailAndIshidden", companyId,email,hidden).first();		
		}
		return null;
	}
	
	public static InboundMailbox findByMonitoredEmail(Long companyId, String email,boolean islock){ 
		if(companyId != null && email != null && !email.trim().isEmpty()){			
			return InboundMailbox.find("byCompanyIdAndEmailAndIslockAndActivatedAndMonitoredAndIshidden", companyId,email.trim(),islock,true,true,false).first();		
		}
		return null;
	}
	
	//find an email where the hidden status is false
	public static InboundMailbox findByEmail(Long companyId, String email){
		
		if(companyId != null && email != null && !email.trim().isEmpty()){			
			return InboundMailbox.find("byCompanyIdAndEmail", companyId,email.trim()).first();		
		}
		return null;
	}
	
    public static InboundMailbox findByEmail(Long companyId, String email,String domain){ 
		if(companyId != null && email != null && !email.trim().isEmpty()){			
			return InboundMailbox.find("byCompanyIdAndEmailAndDomain", companyId,email.trim(),domain).first();		
		}
		return null;
	}
	
	//find an email where the hidden status is false
	public static InboundMailbox findByEmail(String email){		
		if(email != null && !email.trim().isEmpty()){			
			return InboundMailbox.find("byEmail", email).first();	
		}
		return null;
	}
	
	public InboundMailbox setLastPull(Date dateReceived,Date dateSent,int index){  		
			LastPull last = new LastPull(this,dateReceived, dateSent, index);		 
			this.lastPull = last.saveLastPull(this);			
			return this.save(); 	 
	}
	
	/**
	 * Retrieve all mailboxes which monitored and activated status is true 
	 * @param islock
	 * @return
	 */
	public static List<InboundMailbox> findAllByIsLock(Boolean islock){
		if(islock != null){
			return InboundMailbox.find("byIslockAndActivatedAndMonitoredAndIshidden", islock,true,true,false).fetch();
		}
		return null;
	}
	
	/**
	 * Retrieve all mailboxes which monitored and activated status is true 
	 * @param islock
	 * @return
	 * @throws SQLException 
	 */
	public static List<InboundMailbox> findAllByIsLock(Connection conn, Boolean islock){
		List<InboundMailbox> list = new LinkedList<InboundMailbox>();
		try{
		if(islock != null){
			PreparedStatement query = conn.prepareStatement("Select * From InboundMailbox Where islock=? And activated=true And monitored=true And ishidden=false order by priorityLevel ASC");
			query.setBoolean(1, islock);
			ResultSet result = query.executeQuery();
			while(result.next()){
				try{
					list.add(getMailbox(conn, result));
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return list;
	}
	
	/**
	 * Retrieve all activated 'monitored mailboxes' that are currently not pulling messages
	 * @return
	 */
	public static List<InboundMailbox> findAllUnLock(){
		return findAllByIsLock(false);
	}
	
	/**
	 * Retrieve all activated 'monitored mailboxes' that are currently not pulling messages
	 * @return
	 */
	public static List<InboundMailbox> findAllUnLock(Connection conn){
		return findAllByIsLock(conn,false);
	}
	
	public static List<String> findAllEmail(String apikey){
		return InboundMailbox.find("select m.email from InboundMailbox m , Company c where (m.activated = true) and (m.companyId = c.id) and (c.apikey =?) order by m.priorityLevel ASC", 
				apikey).fetch(); 
	}
	
	public static List<String> findAllMonitoredEmail(String apikey){
		return InboundMailbox.find("select m.email from InboundMailbox m , Company c where (m.monitored = true)  and (m.companyId = c.id) and (c.apikey =?) order by m.priorityLevel ASC", 
				apikey).fetch();
	}
	
	public static List<String> findAllMonitoredEmail(Connection conn,String apikey) throws SQLException{

		List<String> filters = new LinkedList<String>();
		if(conn!=null && conn.isClosed()){
			PreparedStatement query  = conn.prepareStatement("select m.email from InboundMailbox m , Company c where (m.monitored = true)  and (m.companyId = c.id) and (c.apikey =?) order by m.priorityLevel ASC");
			query.setString(1, apikey);
			ResultSet result = query.executeQuery();
			while(result.next()){ 
				filters.add(result.getString("email"));
			} 
		}
		return filters;
	}
 
	public static List<InboundMailbox> findAllMailboxes(String apikey,boolean ishidden){
		return InboundMailbox.find("select m from InboundMailbox m , Company c where (m.companyId = c.id) and (m.ishidden = ?) and (c.apikey =?) order by m.priorityLevel ASC", 
				ishidden,apikey).fetch(); 
	} 
	
	public static List<InboundMailbox> findAllMailboxes(String apikey){
		return InboundMailbox.find("select m from InboundMailbox m , Company c where (m.companyId = c.id) and (c.apikey =?) order by m.priorityLevel ASC", apikey).fetch(); 
	} 
	
	public static InboundMailbox findByApikeyAndEmail(String apikey, String email){
		InboundMailbox resultList = InboundMailbox.find("select m from InboundMailbox m , Company c where " +
				"(m.companyId = c.id) and (c.apikey =?) and (m.activated = ?)  and (m.email=?) ",apikey,true, email).first(); 
		 return resultList;
	}
	
	private static InboundMailbox getMailbox(Connection conn,ResultSet result) throws SQLException{
		 InboundMailbox mailbox = new InboundMailbox();
		 mailbox.id = result.getLong("id");
		 mailbox.setSecureKey(result.getBytes("secureKey"));
		 mailbox.setUsername(result.getString("username"));
		 mailbox.setEmail(result.getString("email"));
		 mailbox.setMPassword(result.getString("password"));
		 mailbox.setMailserver(result.getString("mailserver"));
		 mailbox.setCompanyId(result.getLong("companyId"));
		 mailbox.setHost(result.getString("host"));
		 mailbox.setPort(result.getInt("port"));
		 mailbox.setHostType(result.getString("hostType"));
		 mailbox.setOutHost(result.getString("outHost"));
		 mailbox.setOutPort(result.getInt("outPort"));
		 mailbox.setOutHostType(result.getString("outHostType"));
		 mailbox.setActivated(result.getBoolean("activated")); 
		 mailbox.setMonitored(result.getBoolean("monitored"));
		 mailbox.setEmailFilters(result.getString("emailFilters"));
		 mailbox.setNextPullTime(result.getTimestamp("nextPullTime"));
		 mailbox.setIshidden(result.getBoolean("ishidden"));
		 mailbox.setKnownActivation(result.getBoolean("knownActivation"));
		 mailbox.setPriorityLevel(result.getInt("priorityLevel"));
		 mailbox.setDefault(result.getBoolean("isDefault"));
		 
		 try{
			 mailbox.setAuthName(result.getString("authName"));
		 }catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}
		 //mailbox.setLastPull(LastPull.findByInboundId(conn, mailbox));
		 return mailbox;
	}
	
	public static InboundMailbox findByApikeyAndEmail(Connection conn,String apikey,String email) throws SQLException{
		if(conn!=null && conn.isClosed()){
			PreparedStatement query  = conn.prepareStatement("select m.id, m.secureKey,m.username,m.email,m.password,m.mailserver, " +
					"m.companyId,m.host,m.hostType, m.port,m.outHost,m.outHostType, m.outPort, m.activated, m.monitored, m.emailFilters" +
					", m.nextPullTime,  m.authName  from InboundMailbox m , Company c where (m.companyId = c.id) and (c.apikey =?) and (m.activated = ?)  and (m.email=?) ");
			query.setString(1, apikey);
			query.setBoolean(2, true);
			query.setString(3, email);
			ResultSet result = query.executeQuery();
			
			while(result.next()){ 
				 InboundMailbox mailbox = new InboundMailbox();
				 mailbox.id = result.getLong("id");
				 mailbox.setSecureKey(result.getBytes("secureKey"));
				 mailbox.setUsername(result.getString("username"));
				 mailbox.setEmail(result.getString("email"));
				 mailbox.setMPassword(result.getString("password"));
				 mailbox.setMailserver(result.getString("mailserver"));
				 mailbox.setCompanyId(result.getLong("companyId"));
				 mailbox.setHost(result.getString("host"));
				 mailbox.setPort(result.getInt("port"));
				 mailbox.setHostType(result.getString("hostType"));
				 mailbox.setOutHost(result.getString("outHost"));
				 mailbox.setOutPort(result.getInt("outPort"));
				 mailbox.setOutHostType(result.getString("outHostType"));
				 mailbox.setActivated(result.getBoolean("activated")); 
				 mailbox.setMonitored(result.getBoolean("monitored"));
				 mailbox.setEmailFilters(result.getString("emailFilters"));
				 mailbox.setNextPullTime(result.getTimestamp("nextPullTime"));
				 try{
					 mailbox.setAuthName(result.getString("authName"));
					 }catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}
				 return mailbox;
			}
		}
		return null;
	}
	
	public static InboundMailbox findMonitoredMailbox(Connection conn,long companyId,String email) throws SQLException{
		 
		PreparedStatement query  = conn.prepareStatement("select m.id, m.secureKey,m.username,m.email,m.password,m.mailserver, " +
				"m.companyId,m.host,m.hostType, m.port,m.outHost,m.outHostType, m.outPort, m.activated, m.monitored, m.emailFilters" + 
				",m.ishidden,m.knownActivation,m.priorityLevel,m.isDefault, m.nextPullTime,  m.authName  from InboundMailbox m , Company c where (m.companyId = c.id) and (c.id =?) and (c.apikey not like ?) and (m.email=?) " +
				"and m.islock=false and m.activated=true and m.monitored=true and m.ishidden=false ");
		query.setLong(1, companyId);
		query.setString(2, "delete_%");
		query.setString(3, email);
		ResultSet result = query.executeQuery(); 
		
		while(result.next())
			return getMailbox(conn, result);
		
		return null;
		 
	}
	
	//return the first mailbox in the list of mailboxes created by the company
	public static MbSerializer findDefaultMailbox(Connection conn,String apikey) throws SQLException{
		if(conn!=null && !conn.isClosed()){ 
			PreparedStatement query  = conn.prepareStatement("select m.secureKey,m.username,m.email,m.password,m.mailserver, " +
					"m.host,m.hostType, m.port,m.outHost,m.outHostType, m.outPort, m.activated, m.authName from InboundMailbox m , Company c where " +
					"(m.companyId = c.id) and (c.apikey =?) and (m.activated = ?) and (m.ishidden=false)"); 
			query.setString(1, apikey); 
			query.setBoolean(2, true);
			query.setMaxRows(1);
			
			ResultSet result = query.executeQuery();
			
			while(result.next()){ 
				 MbSerializer mailbox = new MbSerializer();
				 mailbox.setUsername(result.getString("username"));
				 mailbox.setEmail(result.getString("email"));
				 mailbox.setPassword(decryptPassword(result.getBytes("secureKey"), result.getString("password")));
				 mailbox.setMailserver(result.getString("mailserver"));
				 
				 mailbox.setHost(result.getString("host"));
				 mailbox.setPort(result.getInt("port"));
				 mailbox.setHostType(result.getString("hostType"));
				 
				 mailbox.setOutHost(result.getString("outHost"));
				 mailbox.setOutPort(result.getInt("outPort"));
				 mailbox.setOutHostType(result.getString("outHostType"));
				 
				 mailbox.setActivated(result.getBoolean("activated")); 
				 try{
					 mailbox.setAuthName(result.getString("authName"));
				 }catch(Exception e){play.Logger.log4j.info(e.getMessage(),e);}
				 return mailbox; 
			}
		}
		return null;
	}
	
	public static MbSerializer findByApikey(String apikey){
		InboundMailbox mb =InboundMailbox.find("select m from InboundMailbox m , Company c where (m.activated = true) and (m.companyId = c.id) and (c.apikey =?)", 
				apikey).first();
		return new MbSerializer(mb,true);
	}
 
  	public static int lockMailbox(Connection conn,Long companyId, Long inboundId) 
			throws SQLException, Exception{		
		return lock(conn, companyId, inboundId, true);
	} 
	
	public static int lockMailbox(Connection conn,InboundMailbox mailbox) 
			throws SQLException, Exception{		
		return lock(conn,mailbox.getCompanyId(), mailbox.getId(), true);
	}
	
	public int lockMailbox(){		
		return lock(true);
	}
	
	public static void unlockMailboxes(){
		try{
			InboundMailbox.em().createQuery("Update InboundMailbox set islock= ? where companyId>0").setParameter(1, false).executeUpdate();			
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}  
	}
	
	public  boolean checkLockState(){
		return this.islock;
	}
	
	public static boolean checkLockState(Connection conn,long companyId, long mailboxId, String email) throws SQLException{
		 	
		if(conn!=null && !conn.isClosed()){
			
			PreparedStatement query = conn.prepareStatement("select islock from InboundMailbox  where companyId= ? " +
					"and id = ? and email = ? and activated = ?");
			
			int i=1;
			query.setLong(i++, companyId);
			query.setLong(i++, mailboxId);
			query.setString(i++,email);
			query.setBoolean(i++, true);
			query.setMaxRows(1); 			 
			
			ResultSet result  = query.executeQuery();
			boolean check = false;
			while(result.next()){
				check = result.getBoolean("islock");
				break;
			} 
			return check;
		}
		
		
		return false;
	}
	
	
	public void unLockMailbox(){
		lock(false);
	}
	
	public static int unLockMailbox(Connection conn,InboundMailbox mailbox) 
			throws SQLException, Exception{		
		return lock(conn, mailbox.getCompanyId(), mailbox.getId(), false);
	}
	
	public static int unLockMailbox(Connection conn,Long companyId, Long inboundId) 
			throws SQLException, Exception{ 		
		return lock(conn, companyId, inboundId, false);
	}
	
	public static long countMailbox(Long companyId){
		return InboundMailbox.count("byCompanyId", companyId);
	}
	
	public static long countMonitoredMailbox(Long companyId){
		return InboundMailbox.count("byCompanyIdAndMonitored", companyId,true);
	}
	
 	private static int lock(Connection conn, Long companyId, Long inboundId, boolean lock) throws SQLException, Exception{
		
		if(conn!=null && !conn.isClosed()){
			
			//create an update statement
			PreparedStatement updateState = conn.prepareStatement("UPDATE InboundMailbox SET islock = ? WHERE companyId = ? AND id = ?"); 
			
			int i=1;
			updateState.setBoolean(i++, lock);
			updateState.setLong(i++, companyId);
			updateState.setLong(i++,inboundId);
			return updateState.executeUpdate()>0?200:201; 
		}  
		return 501;
	}
  
 	
  private  int lock(boolean lock){
		try{
			return InboundMailbox.em().createQuery("UPDATE InboundMailbox SET islock = ? WHERE companyId = ? AND id = ?")
	 				.setParameter(1, lock)
	 				.setParameter(2, this.getCompanyId())
	 				.setParameter(3, this.getId())
	 				.executeUpdate()>0?200:201;		
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e); 
		}  
		return 501;
	}
	
	//check if there have been changes to mailbox settings
	public boolean isChanged(InboundMailbox mailbox){
		
		if(mailbox != null && this != null){
			if(!this.getMailserver().equalsIgnoreCase(mailbox.getMailserver()) ){
				return true; 
			}
			
			/*	
		 	if(!this.getUsername().equalsIgnoreCase(mailbox.getUsername())){
				return true;
			}
			*/
			
			if(!this.getEmail().equalsIgnoreCase(mailbox.getEmail())){
				return true;
			}
			
			if(!this.getPassword().equalsIgnoreCase(mailbox.getPassword())){
				return true;
			}
			
			if(this.getMailserver().equalsIgnoreCase("other")){
				//check for changes in inbound settings
				if(!this.getHostType().equalsIgnoreCase(mailbox.getHostType())){
					return true;
				}
				
				if(!this.getHost().equalsIgnoreCase(mailbox.getHost())){
					return true;
				}
				
				if(this.getPort() != mailbox.getPort()){
					return true;
				}
				
				//check for changes in the outbound settings
				if(!this.getOutHostType().equalsIgnoreCase(mailbox.getOutHostType())){
					return true;
				}
				
				if(!this.getOutHost().equalsIgnoreCase(mailbox.getOutHost())){
					return true;
				}
				
				if(this.getOutPort() != mailbox.getOutPort()){
					return true;
				}
				
			}
			
		}
		
		return false;
	}
	
	public boolean isNextPull(){
		boolean pull = false;
		//String ms = this.getMailserver().toLowerCase();
		Connection conn = null;
		try {
			
			//JsonParser parser = new JsonParser();
			int checkTime = DropifiTools.getEmailCheckTime(this.getMailserver().toLowerCase());  //parser.parse(new FileReader(DropifiTools.SystemConfigFile)).getAsJsonObject().getAsJsonObject("emailCheckTime").get(ms).getAsInt();
			DateTime time = DateTime.now();
			
			if(this.getNextPullTime()==null || this.getNextPullTime().getTime()<time.getMillis()){ 
				time = time.plusMinutes(checkTime);
				this.setNextPullTime(new Timestamp(time.toDate().getTime())); 
				conn = DCON.GetConnectionClient();
				if(conn!=null && !conn.isClosed()){
					PreparedStatement query = conn.prepareStatement("UPDATE InboundMailbox SET nextPullTime=? WHERE companyId = ? AND id = ?");
					query.setTimestamp(1, this.getNextPullTime());
					query.setLong(2, this.getCompanyId());
					query.setLong(3, this.getId()); 
					pull = query.executeUpdate()>0?true:false;
				}
				 
			}
			//play.Logger.log4j.info(this.getMailserver().toLowerCase()+" : "+checkTime+" , Pull: "+pull);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		} finally{
			/*if(conn!=null){
				try{
				conn.close();
				}catch(Exception e){}
			}*/
		}
		return pull; 
	}
	
	/**
	 * Check the next pull time without creating any DB connection
	 * @param bool
	 * @return
	 */
	public boolean isNextPull(boolean bool){
		boolean pull = false; 
		try { 
			int checkTime = DropifiTools.getEmailCheckTime(this.getMailserver().toLowerCase());
			DateTime time = DateTime.now();
			
			if(this.getNextPullTime()==null || this.getNextPullTime().getTime()<time.getMillis()){ 
				time = time.plusMinutes(checkTime);
				this.setNextPullTime(new Timestamp(time.toDate().getTime()));
				
				InboundMailbox.em().createQuery("Update InboundMailbox Set nextPullTime= ? Where companyId=? And id=?")
				.setParameter(1, this.getNextPullTime())
				.setParameter(2, this.getCompanyId())
				.setParameter(3, this.getId())
				.executeUpdate();		
				pull = true;
			}  
		} catch (Exception e) { 
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
			pull = isNextPull();
		}  
		return pull; 
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	//getters and setters
	
 	public LastPull getLastPull() {
		return lastPull;
	}
 	
 	public void setLastPull(LastPull lastPull) {
		 this.lastPull=lastPull;
	}

	public Company getCompany(){			 
		Company comp= Company.find("byId", 1l).first();		 
		return comp;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = SQLInjector.escape(username.trim());
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = SQLInjector.escape(email.trim());
	}

	public String getPassword() {
		SecretKeySpec key = new SecretKeySpec(this.getSecureKey(), "DES"); 
		
		Dcrypto decrypter = new Dcrypto(key);
		return decrypter.decryptDES(this.password);
	}
	
	public static String decryptPassword(byte[]secureKey,String password) {
		SecretKeySpec key = new SecretKeySpec(secureKey, "DES"); 
		
		Dcrypto decrypter = new Dcrypto(key);
		return decrypter.decryptDES(password);
	}

	public void setPassword(String password){
		//encrypt the password
		if(password !=null){
			SecretKeySpec key = new SecretKeySpec(this.getSecureKey(), "DES");
			Dcrypto encrypter = new Dcrypto(key);		
			this.password = encrypter.encryptDES(password);
		}
	}
	
	public void setMPassword(String password){
		this.password = password;
	}

	public String getHostType() {
		return hostType.trim();
	}

	public void setHostType(String hostType) {
		this.hostType =  hostType.trim();
	}
	
	public String getHost() {
		return host;
	}
	
	public void setHost(String host) {
		this.host =host!=null?host.trim():host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	
	public boolean getIslock() {
		return islock;
	}
	
	public void setIslock(boolean islock) {
		this.islock = islock;
	}

	public boolean getActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public boolean getIshidden() {
		return ishidden;
	}

	public void setIshidden(boolean ishidden) {
		this.ishidden = ishidden;
	}
	
	public String getMailserver() {
		return mailserver;
	}

	public void setMailserver(String mailserver) {
		this.mailserver = mailserver;
	}

	public String getOutHostType() {
		return outHostType;
	}

	public void setOutHostType(String outHostType) {
		this.outHostType = outHostType;
	}

	public Integer getOutPort() {
		return outPort;
	}

	public void setOutPort(Integer outPort) {
		this.outPort = outPort;
	}

	public String getOutHost() {
		return outHost;
	}

	public void setOutHost(String outHost) {
		this.outHost = outHost!=null?outHost.trim():outHost;
	}
	
	public Timestamp getCreated() {
		return created;
	}

	private void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	private void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public byte[] getSecureKey() {
		return secureKey;
	}
	
	private void setSecureKey() throws NoSuchAlgorithmException{
		SecretKey key = KeyGenerator.getInstance("DES").generateKey();
		this.secureKey = key.getEncoded();
	}
	
	private void setSecureKey(byte[] secureKey) {
		this.secureKey = secureKey;		 
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getMonitored() {
		return monitored;
	}

	public void setMonitored(Boolean monitored) {
		this.monitored = monitored;
	}

	public String getEmailFilters() {
		return emailFilters;
	}

	public void setEmailFilters(String emailFilters) {
		this.emailFilters = emailFilters;
	}
	
	public Timestamp getNextPullTime() {
		return nextPullTime;
	}

	public void setNextPullTime(Timestamp nextPullTime) {
		this.nextPullTime = nextPullTime; 
	}

	public Integer getPriorityLevel() {
		return priorityLevel;
	}

	public void setPriorityLevel(Integer priorityLevel) {
		this.priorityLevel = priorityLevel;
	}

	public Boolean getKnownActivation() {
		return knownActivation;
	}

	public void setKnownActivation(Boolean knownActivation) {
		this.knownActivation = knownActivation;
	}

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}
	
}
