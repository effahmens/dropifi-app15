package models;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.IndexColumn;
import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement; 
import com.google.gson.JsonObject;

import adminModels.DropifiMailer;

import play.data.validation.Email;
import play.db.jpa.JPA;
import play.db.jpa.Model; 
import query.inbox.SentMsg_Serializer;  
import view_serializers.AttSerializer;
import resources.DCON;
import resources.DropifiTools;
import resources.ResponseStatus;

@Entity
public class DropifiResponse extends Model{
	
	@Index(name="companyId") @Column(nullable=false)
	private Long companyId;
	@Index(name="apikey") @Column(nullable=false)
	private String apikey;
	@Index(name="messageId")
	private Long messageId;
	@Index(name="msgId")
	private String msgId;
	@Index(name="sender") @Column(nullable=false) @Email
	private String sender;
	
	@ElementCollection(targetClass=String.class)
	@Column(name="recipient")
	private Collection<String>recipient;
	
	@Column(nullable=false)
	private String mainRecipient;
	
	@Column( length=100000)
	private String subject;
	@Column(nullable=false, length = 100000) 
	private String message; 
	
	@ElementCollection(targetClass=String.class)
	@Column(name="ccAddress")
	private Collection<String>ccAddress;
	
	@ElementCollection(targetClass=String.class)
	@Column(name="bccAddress")
	private Collection<String>bccAddress;
	
	@Column(nullable=false)
	private boolean isSent;
	
	@Column(nullable=false)
	private Timestamp created;
	@Column(nullable=false)
	private Timestamp updated;
	
	@Column(nullable=false)
	private String userEmail; 
	
	@Index(name="isArchived")
	private boolean isArchived;

	public DropifiResponse(long companyId, String apikey, long messageId,
			String sender, Collection<String> recipient, String subject, 
			String message, Collection<String> ccAddress,Collection<String> bccAddress) {
		 
		this.setCompanyId(companyId); 
		this.setApikey(apikey);
		this.setMessageId(messageId);
		this.setSender(sender);
		this.setRecipient(recipient);
		this.setSubject(subject);
		this.setMessage(message);
		this.setCcAddress(ccAddress);
		this.setBccAddress(bccAddress);
		this.setArchived(false); 
	} 
	
	public DropifiResponse(){}
	 
	//-------------------------------------------------------------------------------------------------------------
	//
	
	public DropifiResponse saveResponse(String userEmail){ 
		
		try{ 
			if(this!=null && this.getCompanyId() !=null){
				
				Timestamp time = new Timestamp(new Date().getTime());				 
				this.setUpdated(time);	
				this.setUserEmail(userEmail);				 
				//the main recipient of message: first email in the list of TO email addresses					
				for(String rep : this.getRecipient()){
					this.setMainRecipient(rep);					 
					break;
				}
				 
				this.setArchived(false);
				DropifiResponse resp = this.save();				
				return resp; 
			}	
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e); 
		 
		}
		return null;
	}
	
		
	public int sendMessage(String recipient, String cc,String bcc,List<AttSerializer> fileAttachments){		
		Channel channel = Channel.findByApikey(this.getApikey());		
		if(channel!=null){
			 InboundMailbox mailbox = channel.findInboundMb(this.getSender());		 
			 if(mailbox != null) {
				 try {
					 DropifiMailer mailer = new DropifiMailer(channel.getApikey(), mailbox); 						
									
					int status = mailer.sendToMany(recipient,cc,bcc, this.getSubject(), this.getMessage(),fileAttachments);
					 
					if(status == 200){
						this.setSent(true);
						this.save(); 
					} 
					
					return status; 
				}catch (Exception e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.error(e.getMessage(), e);  
				}
			} 
			 return 502;
		}
		return 501;
	}
	
	public int sendMessage(List<AttSerializer> fileAttachments){		
		Channel channel = Channel.findByApikey(this.getApikey());		
		if(channel!=null){
			 InboundMailbox mailbox = channel.findInboundMb(this.getSender());		 
			 if(mailbox != null) {
				 try {
					 DropifiMailer mailer = new DropifiMailer(channel.getApikey(),mailbox);	 
					int status = mailer.sendToMany( getAddresses(this.getRecipient()), getAddresses(this.getCcAddress()), getAddresses(this.getBccAddress()), 
							this.getSubject(), this.getMessage(),fileAttachments);
					 
					if(status == 200) {
						this.setSent(true);
						this.save();
					}					
					return status;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.error(e.getMessage(), e);
					return 410;
				}
			} 
			 return 502;
		}
		return 501;
	}
		
	public static InternetAddress[] getAddresses(Collection<String> recipients){
		InternetAddress[] repAddress = null; 

		if(recipients != null){
			repAddress = new InternetAddress[recipients.size()];		
			int i=0;
			for(String rep : recipients ){
				try {				
					
					if(rep!=null && !rep.trim().isEmpty()){
						rep = rep.trim();
						repAddress[i++] = new InternetAddress(rep);  
					}
				} catch (AddressException e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.error(e.getMessage(), e);
				}
			}	
		}
		return repAddress;
	}
	
	public static InternetAddress[] getAddresses(String recipients){
		String[] reps = recipients.split(","); 
		InternetAddress[] repAddress = new InternetAddress[reps.length]; 		
		for(int i=0; i<reps.length;i++){
			try {
				repAddress[i] = new InternetAddress(reps[i]); 
			} catch (AddressException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
		}		
		return repAddress;
	}
	
	public static Collection<String> recipient(String recipients){
		
		if(recipients!=null && !recipients.trim().isEmpty()){
			Collection<String> repAddress = new LinkedList<String>();
			if(recipients.contains(",")){
				String[] reps = recipients.split(","); 				 
				for(int i=0; i<reps.length;i++){
					String value = reps[i];
					if(value!=null && !value.trim().isEmpty()) 
						repAddress.add(value); 
				}	
			}else{				 
				repAddress.add(recipients); 
			}
			return repAddress; 
		}
		return null; 
	}
	
	public static String recipient(Collection<String> recipients){
		
		String to= "";
		int count =0;
		int size = recipients.size();
		if(size>0){
			for(String rep : recipients){
				to += rep +(++count<size?",":"");
			}
		}
		return to; 
	}
	
	public static DropifiResponse findByApikeyAndId(String apikey,long respId){
		return DropifiResponse.find("byApikeyAndId",apikey,respId).first();
	}
	
	public static Long findId(String apikey,long respId){
		return DropifiResponse.find("Select r.id From DropifiResponse r where r.apikey =? and r.id =?",apikey,respId).first(); 
	}
	
	public static JsonArray repliedBy (String apikey, String msgId, long messageId,String userEmail) {
		
		JsonArray jsonArray = new JsonArray(); 		
		List<Object[]> resultList = DropifiResponse.find("Select r.userEmail, r.subject , r.message, r.updated from DropifiResponse r " +
				"where r.apikey=? and r.msgId =? and r.messageId =? and r.isSent = ? order by r.id desc", apikey,msgId, messageId,true).query.getResultList(); 
		
		for(Object[] fields : resultList) {
			jsonArray.add(jsonReplyer(fields,userEmail));  
		} 
		 
		return jsonArray;
	}
	
	public static List<DropifiResponse> findAllReplies(String apikey){
		return DropifiResponse.find("byApikeyAndIsSent", apikey,true).fetch(); 
	} 
	
	//create a jsonArray of sent messages consisting of TO recipient, subject, date, and sender of message 
	public static JsonArray findSentMessages(String apikey,String userEmail,boolean isSent,Integer timezone){
		List<Object[]>resultList = DropifiResponse.find("select r.id, r.messageId , r.msgId, r.mainRecipient, r.subject, r.created, r.userEmail from DropifiResponse r " +
				"where (r.apikey =?) and (r.isSent =?) and (r.isArchived=false) order by r.id desc",apikey,isSent).query.getResultList(); 
		
		JsonArray jsonArray = new JsonArray(); 
		for(Object[] fields : resultList){			 
			jsonArray.add(getSentMessage(fields, userEmail, isSent,timezone));
		} 
		return jsonArray;
	}
	
	public static JsonArray findTaskSentMessages(String apikey,String userEmail,boolean isSent,Integer timezone){ 
		List<Object[]>resultList = DropifiResponse.find("select r.id, r.messageId , r.msgId, r.mainRecipient, r.subject, r.created, r.userEmail from DropifiResponse r " +
				"where (r.apikey =?) and (r.isSent =?) and (r.isArchived=false) order by r.id desc",apikey,isSent).query.getResultList(); 
		
		JsonArray jsonArray = new JsonArray();
		for(Object[] fields : resultList){	 
			jsonArray.add(getSentMessage(fields, userEmail, isSent,timezone));
		} 
		return jsonArray; 
	}
 	
	/**
	 * find the last team member who replied to the message
	 * @param apikey
	 * @param msgId
	 * @param messageId
	 * @param userEmail
	 * @return
	 */
	
 	public static JsonObject lastRepliedBy (String apikey, String msgId, long messageId,String userEmail) {
		 
		List<Object[]> resultList = DropifiResponse.find("Select r.userEmail, r.subject , r.message, r.updated from DropifiResponse r " +
				"where r.apikey=? and r.msgId =? and r.messageId =? and r.isSent =true order by r.id desc", apikey,msgId, messageId).query.setMaxResults(1).getResultList();
		 
		 for(Object[] fields : resultList)
			 return jsonReplyer(fields,userEmail);
		 
		 return null; 
	} 
 	
 	public static JsonObject findMessage(String apikey,long replyId,String userEmail,boolean isSent,Integer timezone){
 		DropifiResponse response = DropifiResponse.find("select r from DropifiResponse r Where r.apikey =? And (r.id =?) And r.isSent=?", apikey,replyId,isSent).first();
 		return toJsonObject(response, userEmail,timezone);
 	}
 	
 	public static JsonObject findMessage(String apikey,String msgId, long replyId,String userEmail,boolean isSent,Integer timezone){
 		DropifiResponse response = DropifiResponse.find("select r from DropifiResponse r Where r.apikey =? And (r.id =? OR r.msgId=?) And r.isSent=?", apikey,replyId,msgId,isSent).first();
 		return toJsonObject(response, userEmail,timezone);
 	}
 	
 	public static JsonArray findMessages(String apikey,String msgId, String userEmail,Integer timezone){
 		List<DropifiResponse >responses = DropifiResponse.find("select r from DropifiResponse r Where (r.apikey =?) And (r.msgId=?) and (r.isSent=true)", apikey,msgId).fetch();
 		JsonArray jsonArray = new JsonArray();
 		Map<String,String> users = AccountUser.findUserEmailName(apikey);
 		for(DropifiResponse response: responses){
 			jsonArray.add(toJsonObject(response, users.get(response.getUserEmail()),timezone)); 
 		}
 		return jsonArray;
 	}
 	
 	public static JsonArray findMessagesList(String apikey,String msgId, String userEmail,Integer timezone){
 		
 		 
 		List<DropifiResponse >responses = DropifiResponse.find("select r from DropifiResponse r Where (r.apikey =?) And (r.msgId=?) and (r.isSent=true)", apikey,msgId).fetch();
 		JsonArray jsonArray = new JsonArray();
 		Map<String,String> users = AccountUser.findUserEmailName(apikey);
 		for(DropifiResponse response: responses){
 			jsonArray.add(toJsonObject(response, users.get(response.getUserEmail()),timezone)); 
 		}
 		return jsonArray;
 	}
 	

	//--------------------------------------------------------------------------------------------------------------
 	
		
	private static JsonObject jsonReplyer(Object[] fields,String userEmail){
		JsonObject replyer = new JsonObject();
		int i =0;
		String user = (String)fields[i++];
		replyer.addProperty("userName",user.equalsIgnoreCase(userEmail)?"ME":user );
		replyer.addProperty("subject", (String)fields[i++]);			
		replyer.addProperty("message", (String)fields[i++]);  
		Timestamp time = (Timestamp)fields[i++];
		replyer.addProperty("updated", time.toString());
		return replyer;
	}
	
	public static JsonObject jsonReplyer(DropifiResponse response,String userEmail){
		JsonObject replyer = new JsonObject();
		 
		String user = response.getUserEmail();
		replyer.addProperty("userName",user.equalsIgnoreCase(userEmail)?"ME":user );
		replyer.addProperty("subject", response.getSubject());			
		replyer.addProperty("message", response.getMessage());  		 
		replyer.addProperty("updated", response.getUpdated().toString());
		return replyer;
	}
	
	private static JsonElement getSentMessage(Object[] fields,String userEmail,boolean isSent,Integer timezone){ 
		Gson gson = new Gson();
		SentMsg_Serializer reply = new SentMsg_Serializer(fields, userEmail, isSent,timezone);		 
		return gson.toJsonTree(reply);  
	} 
	
	private static JsonElement getSentMessage(Object[] fields,String userEmail,boolean isSent,int index, Integer timezone){ 
		JsonObject json = new JsonObject();
		SentMsg_Serializer reply = new SentMsg_Serializer(fields, userEmail, isSent,timezone);
		json.addProperty("id", reply.getId());
		json.addProperty("content", reply.getSentMessage(index)); 
		return json;
	} 
		
	public static JsonObject toJsonObject(DropifiResponse response,String userName,Integer timezone){ 
		JsonObject json = new JsonObject();
		
		if(response == null) return json;
		
		response.setCreated(response.getCreated(), timezone);
		//Date date = new Date(response.getCreated().getTime());
		json.addProperty("id", response.getId());
		json.addProperty("msgId", response.getMsgId());
		json.addProperty("messageId", response.getMessageId()); 
		json.addProperty("from", response.getSender());
		json.addProperty("to", DropifiResponse.recipient(response.getRecipient()));
		json.addProperty("cc", DropifiResponse.recipient(response.getCcAddress()));
		//json.addProperty("bcc", DropifiResponse.recipient(response.getBccAddress()));
		json.addProperty("subject", response.getSubject()); 
		json.addProperty("message", response.getMessage());
		json.addProperty("created", DropifiTools.toDateString(response.getCreated())); 
		json.addProperty("userName",userName); 
		json.addProperty("userEmail",response.getUserEmail()); 
		json.addProperty("isSent", response.isSent()); 
		
		return json;
	}
	
	public static JsonObject retrieveSubjectMessage(String apikey,long id,String msgId,String email) {
		List<Object[]> resultList = DropifiResponse.find("select d.subject, d.message ,d.created, d.userEmail from DropifiResponse d " +
				"where (d.apikey=?) and (d.id =? ) and (d.mainRecipient =?)", apikey,id, email).query.setMaxResults(1).getResultList();
		
		if(resultList !=null){			
			for(Object[] fields : resultList){  
				
				int i =0;
				JsonObject json = new JsonObject(); 				
				json.addProperty("subject", (String)(fields[i]!=null?fields[i++]:""));
				json.addProperty("message", (String)(fields[i]!=null?fields[i++]:""));	
				String dataTime = ((Timestamp)(fields[i]!=null?fields[i++]:"")).toString();
				json.addProperty("created", dataTime);
				json.addProperty("userEmail", (String)(fields[i]!=null?fields[i++]:""));
				return json;  
			} 
		}
		return null; 
	} 
	
	public static JsonObject retrieveSubjectMessageOLD(String apikey,long id,String msgId,String email) {
		List<Object[]> resultList = DropifiResponse.find("select d.subject, d.message ,d.created, d.userEmail from DropifiResponse d " +
				"where (d.apikey=?) and (d.id =? ) and (d.msgId = ?) and (d.mainRecipient =?) ", apikey,id, msgId, email).query.setMaxResults(1).getResultList();
		
		if(resultList !=null){			
			for(Object[] fields : resultList){  
				
				int i =0;
				JsonObject json = new JsonObject(); 
	
				json.addProperty("subject", (String)(fields[i]!=null?fields[i++]:""));
				json.addProperty("message", (String)(fields[i]!=null?fields[i++]:""));	
				
				String dataTime = DropifiTools.toDateString((Timestamp)(fields[i]!=null?fields[i++]:""));
				
				json.addProperty("created", dataTime);
				json.addProperty("userEmail", (String)(fields[i]!=null?fields[i++]:""));
				
				return json;  
			} 
		}
		return null; 
	} 
	

	//--------------------------------------------------------------------------------------------------------------
	//delete responses
	
	public static int deleteMessage(long companyId, List messageIds){
		 
		EntityManager em = JPA.em();   		
		//Timestamp updated = new Timestamp(new Date().getTime()); 
		int result = em.createQuery("DELETE from DropifiResponse d where d.id in (:id) and d.companyId = :companyId")
	                 .setParameter("id", messageIds).setParameter("companyId", companyId).executeUpdate();

		return result>0?200:201;
		 
	}
	
	@SuppressWarnings("unused")
	private static int deleteCC(EntityManager em, List messageIds){ 
		 
		int result = em.createQuery("DELETE from DropifiResponse_ccAddress d where d.DropifiResponse_id in (:id)")
                .setParameter("id", messageIds).executeUpdate();
		//PreparedStatement qCC =	conn.prepareStatement("DELETE from DropifiResponse_ccAddress d where d.DropifiResponse_id in ()");	
		return result;
	}
	
	@SuppressWarnings("unused")
	private static int deleteBCC(EntityManager em,List messageIds){
	 
		int result = em.createQuery("DELETE from DropifiResponse_bccAddress d where d.DropifiResponse_id in (:id)")
                .setParameter("id", messageIds).executeUpdate();
		//PreparedStatement qCC =	conn.prepareStatement("DELETE from DropifiResponse_ccAddress d where d.DropifiResponse_id in ()");	
		return result;
	}
	
	@SuppressWarnings("unused")
	private static int deleteRecipient(EntityManager em,List messageIds){
		//Connection conn = DCON.getDefaultConnection(); 
		int result = em.createQuery("DELETE from DropifiResponse_recipient d where d.DropifiResponse_id in (:id)")
                .setParameter("id", messageIds).executeUpdate();
		//PreparedStatement qCC =	conn.prepareStatement("DELETE from DropifiResponse_ccAddress d where d.DropifiResponse_id in (?)");
		 
		return result;
	}
	
	//--------------------------------------------------------------------------------------------------------------
	//update
	
	/**
	 * This method is called anytime a user changes the email address
	 * @param apikey
	 * @param currentEmail
	 * @param newEmail
	 * @return
	 */
	public static int updateUserEmail(String apikey, String currentEmail, String newEmail){ 	 
		int result = DropifiResponse.em().createQuery("Update DropifiResponse r Set r.userEmail = :newEmail Where r.apikey= :apikey and r.userEmail= :currentEmail")
				.setParameter("newEmail", newEmail)
				.setParameter("apikey",apikey)
				.setParameter("currentEmail", currentEmail).executeUpdate();
		return result>=0?200:201;
	}
	
	
	//--------------------------------------------------------------------------------------------------------------
	//getters and setters
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Collection<String> getRecipient() {
		return recipient;
	}

	public void setRecipient(Collection<String> recipient) {
		this.recipient = recipient;
	}
	
	public void setRecipient(String recipients) {
		this.setRecipient(DropifiResponse.recipient(recipients));
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Collection<String> getCcAddress() {
		return ccAddress;
	}

	public void setCcAddress(Collection<String> ccAddress) {
		this.ccAddress = ccAddress;
	}

	public Collection<String> getBccAddress() {
		return bccAddress;
	}

	public void setBccAddress(Collection<String> bccAddress) {
		this.bccAddress = bccAddress;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	public void setCreated(Timestamp created,Integer timezone){
		try{
			DateTime zone = new DateTime(created.getTime());
			if(timezone!=null){
				zone = zone.plusHours(timezone);
			}
			 
			this.setCreated(new Timestamp(zone.toDate().getTime()));
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public boolean isSent() {
		return isSent;
	}

	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getUserEmail() {
		return userEmail;
	}
	
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail; 
	}

	public String getMainRecipient() {
		return mainRecipient;
	}
	
	public void setMainRecipient(String mainRecipient) {
		this.mainRecipient = mainRecipient;
	}

	public boolean isArchived() {
		return isArchived;
	}

	public void setArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}
		
}
