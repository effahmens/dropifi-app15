/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import play.mvc.Controller;

/**
 * 
 * @author davidosei
 * 
 */

public class StaticPage extends Controller {
    
    public static void page(String page){
        render(page+".html");
    }
}
