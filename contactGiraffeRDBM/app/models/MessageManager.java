/**
 * @author phillips effah mensah
 * @description 
 * @date 14/3/2012
 * 
 **/

package models;

import play.cache.Cache;
import play.data.validation.*;
import play.db.jpa.*;
import resources.CacheKey;
import resources.DropifiTools;
import resources.RuleType;
import view_serializers.SubjectSerializer;

import javax.persistence.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import org.bson.types.ObjectId; 
import org.hibernate.annotations.Index;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Entity
public class MessageManager extends Model {
		 
	@Required @Unique @Column(nullable=false, unique=true)
	private Long companyId;
		 
	@Required @Column(unique=true, nullable=false)
	private Long settingId;
	
	//Message Rules: widget, inbound, outbound, filter
	@Transient
	private List<MsgRule> msgRules; 
	@Transient
	private List<WidgetRule> widgetRules; 
	
	@ElementCollection(targetClass = QuickResponse.class,fetch=FetchType.LAZY)
	@CollectionTable(name="quickResponses", joinColumns=@JoinColumn(name="msgqr_id")) 
	private Collection<QuickResponse> quickResponses;
	
	//message subject
	@ElementCollection(targetClass = MsgSubject.class,fetch=FetchType.LAZY)
	@CollectionTable(name="msgSubject", joinColumns=@JoinColumn(name="msgsub_id"))	 	 
	private Collection<MsgSubject> msgSubject; 
	
	@ElementCollection(targetClass = MsgSearchKeyword.class,fetch=FetchType.LAZY)
	@CollectionTable(name="SearchKeywords", joinColumns=@JoinColumn(name="msgkey_id"))
	private Collection<MsgSearchKeyword> searchkeywords;
		
	public MessageManager(Setting setting){
		this.setSettingId(setting.getId());
		this.setCompanyId(setting.getCompany().getId());
	}
	
	//----------------------------------------------------------------------------------------
	
	public MessageManager createMsgManager()  throws NullPointerException{ 
		
		try{
		Setting sett = this.getSetting();		
		 if(sett == null)			 
		    throw new NullPointerException("Failed creating message manager. Null pointer exception.");
		
		
		Company company = sett.getCompany(); 		
		if(company != null){
			MessageManager msg = findByCompanyId(company.getId());
			
			if(msg != null){
				return null;
			}else{				
				return this.save();				 
			}
		}
		return null;
		}catch(Exception e){
			return null;
		}
		 
	}
	
	public static MessageManager findByCompanyId(Long companyId){
		return MessageManager.find("byCompanyId", companyId).first();
	}
	
	public static MessageManager findByApikey(String apikey){
		return MessageManager.find("select m from MessageManager m, Company c where m.companyId = c.id and " +
				"c.apikey = ?", apikey).first();
	}
	
	public static Collection<MsgSubject> findSubjectsByApikey(String apikey){
		return (Collection) MessageManager.find("select m.msgSubject from MessageManager m, Company c where m.companyId = c.id and " +
				"c.apikey = ?", apikey).fetch();
	}
	
	public static List<MsgSearchKeyword> findKeywordsByApikey(String apikey){
		return MessageManager.find("select m.searchkeywords from MessageManager m, Company c where m.companyId = c.id and " +
				"c.apikey = ?", apikey).fetch();
	}
	
	public static MessageManager findByDomain(String domain){
		return Channel.find("select m from MessageManager m, Company c where m.companyId = c.id and " +
				"c.domain = ?", domain).first();
	}
	
	//----------------------------------------------------------------------------------------
	//Message Rules:: a set of rules on messages in the inbox of user
	 
	public int addMsgRule(MsgRule rule){
		rule.setMessageManagerId(this.getId()); 
		rule.setCompanyId(this.getCompanyId());
		return rule.createOrUpdateRule(); 	 
	}	 
	
	public boolean updatMsgRule(Object id, MsgRule newRule){
		return false;
	}
	
	public boolean removeMsgRule(Object id){
		return false;
	}
	
	public int removeMsgRule(String name){
		return MsgRule.removeMsgRule(this.getCompanyId(), name);
	}
		
	public MsgRule findMsgRuleByName(String name){
		return MsgRule.findByName(this.getId(), name);
	}
	
	public List<MsgRule> findAllMsgRule(){
		return MsgRule.find("byMessageManager_id", this.getId()).fetch(); 
	}
	
	public List<MsgRule> getMsgRule() {
		return msgRules;
	}

	//----------------------------------------------------------------------------------------
	//Widget Rules:: a set of rules on messages received from the contact form (dropifi widget)
	
	public int addWidgetRule(WidgetRule rule){
		rule.setMessageManagerId(this.getId());
		rule.setCompanyId(this.getCompanyId());
		return rule.createOrUpdateRule(); 		 
	}	 
	
	public boolean updatWidgetRule(Object id, WidgetRule newRule){
		return false;
	}
	
	public boolean removeWidgetRule(Object id){
		return false;
	}
	
	public boolean removeWidgetRule(String name){
		WidgetRule rule = WidgetRule.findByName(this.getId(),name);
		if(rule != null){
			rule.delete();
			return true;
		}
		return false; 
	}
		
	public WidgetRule findWidgetRuleById(Object id){
		return null;
	}
	
	public List<WidgetRule> getWidgetRule() {
		return widgetRules;
	}

	public void setWidgetRule(List<WidgetRule> widgetRule) {
		this.widgetRules = widgetRule;
	}	
	
	//--------------------------------------------------------------------------------------------
	//QuickResponse: allows agent/admin to perform tasks such as sending reply to customers quickly			
	
	public int addQuickResponse(QuickResponse quickResponse){
		
		if(!containsQuickResponse(quickResponse.getTitle())){ 
			
			Timestamp time = new Timestamp(new Date().getTime());
			quickResponse.setCreated(time);
			quickResponse.setUpdated(time);
			
			if(this.quickResponses.add(quickResponse)){
				return this.validateAndSave() ? 200:201; 			
			}
			return 203;
		}
		return 302;  
	}
		
	public int updateQuickResponse(String title, QuickResponse newQuickResponse){
		QuickResponse qr = findQRByTitle(title);
		if(qr != null){ 
			
			//check if the update QR and the existing has the same title
			if(!qr.getTitle().equalsIgnoreCase(newQuickResponse.getTitle())){
				//check for duplication of names
				if(containsQuickResponse(newQuickResponse.getTitle()) ){					
					return 302;
				}else{
					qr.setTitle(newQuickResponse.getTitle());
				}				
			}else{
				qr.setTitle(newQuickResponse.getTitle());
			} 
			
			qr.setSubject(newQuickResponse.getSubject());
			qr.setMessage(newQuickResponse.getMessage());
			qr.setActivated(newQuickResponse.getActivated());
			qr.setPermission(newQuickResponse.getPermission());
			qr.setUpdated(new Timestamp(new Date().getTime()));
			
			return this.validateAndSave() ? 200:201; 			 
		}
		return 501;
		 
	}
	
	public int updateQRActivated(String title, boolean activated){ 		
		QuickResponse qr = findQRByTitle(title);
		
		if(qr !=null){
			qr.setActivated(activated);
			qr.setUpdated(new Timestamp(new Date().getTime()));
			this.save();
			return 200;
		} 		
		return 501;
	}
	
	public int removeQuickResponse(String title){
		QuickResponse qr = findQRByTitle(title);
		if(qr !=null){
			if(this.quickResponses.remove(qr)){		 
				this.save();
				return 200;
			}
			return 201;
		}
		return 501;
	}
		
	public QuickResponse findQRByTitle(String title){
		title = title.trim();
		if(!title.isEmpty()){
			 Iterator<QuickResponse> listQR = this.quickResponses.iterator();
			
			while(listQR.hasNext()){
				QuickResponse tit = listQR.next();
				if(tit.getTitle().equalsIgnoreCase(title))
					return tit;
			}
		}
		return null; 
	}
	
	public boolean containsQuickResponse(String title){
		return findQRByTitle(title) != null;
	}
		
	public Collection<QuickResponse> getQuickResponses(){
		//TOBE implemented
		return this.quickResponses;
	}
	
	public static List<QuickResponse> findQuickResponsesByApikey(String apikey){
		List resultList = MessageManager.find("select m.quickResponses from MessageManager m, Company c where m.companyId = c.id and " +
				"c.apikey = ?", apikey).query.getResultList();
		
		return (List<QuickResponse>)resultList;
	}
	
	public static List<String> listOfQuickResponseTitles(String apikey){
		MessageManager msg = MessageManager.findByApikey(apikey); 
		
		Collection<QuickResponse> quickResponses =  msg.getQuickResponses();
		List<String> responseList = new LinkedList<String>();
		
		for(QuickResponse response : quickResponses) { 
			
			if(response.getActivated()== true)
				responseList.add(response.getTitle());
				
		}		 
		return responseList;
	} 
	
	public static QuickResponse findQuickResponse(Connection conn, long companyId, String title) throws SQLException{
		if(conn!=null && !conn.isClosed()){
			
			PreparedStatement query = conn.prepareStatement("select q.subject, q.message, q.activated from quickResponses q, " +
					"MessageManager m where q.msgqr_id = m.id and q.title  =? and  m.companyId =? ");
			query.setString(1, title);
			query.setLong(2, companyId);
			query.setMaxRows(1);
			
			ResultSet result = query.executeQuery();
			
			while(result.next()){
				QuickResponse response = new QuickResponse();
				response.setSubject(result.getString("subject"));
				response.setMessage(result.getString("message"));
				return response;
			}
		}
		
		return null;
	}
	
	//----------------------------------------------------------------------------------------
	//MessageSubjects: a list of subject for categorizing incoming messages
	//E.g. {Feedback, Suggestion,......}
		
	public int addSubject(MsgSubject subject){ 		
		 
		if(!containsSubject(subject.getSubject())){
			 Timestamp time = new Timestamp(new Date().getTime());
			 subject.setCreated(time);
			 subject.setUpdated(time);
			 if(this.msgSubject.add(subject)){
				 return this.validateAndSave() ? 200 : 201; 	
			 }
			 return 203;
		 } 			
		 return 302;
	}
	
	/**
	 * Add a list of subjects by first removing all existing subjects
	 * */
	public int addSubjects(List<MsgSubject> subjects){
		try{
			if(subjects!=null && subjects.size()>0){
				this.msgSubject = new LinkedList<MsgSubject>();
				for(MsgSubject subject : subjects){
					//if(!containsSubject(subject.getSubject())){
						Timestamp time = new Timestamp(new Date().getTime());
						subject.setCreated(time);
						subject.setUpdated(time);
						this.msgSubject.add(subject);
					//} 
				}
				return this.validateAndSave() ? 200 : 201; 	
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return 302;
	}
		
	public boolean containsSubject(String subject){
		return this.findSubjectByName(subject) != null;
	}
		
	public int updateSubject(String subject, MsgSubject newMsgSubject){
		
		MsgSubject ms = findSubjectByName(subject);
		if(ms != null){ 
			
			//check if the update subject and the existing has the same name
			if(!ms.getSubject().equalsIgnoreCase(newMsgSubject.getSubject())){
				
				//check for duplication of names
				if(containsSubject(newMsgSubject.getSubject()) ){					
					return 302;
				}else{
					ms.setSubject(newMsgSubject.getSubject());
				}				
			}else{
				ms.setSubject(newMsgSubject.getSubject());
			} 
			
			ms.setActivated(newMsgSubject.getActivated());	
			ms.setUpdated(new Timestamp(new Date().getTime())); 
			
			return this.validateAndSave() ? 200:201; 			 
		}
		return 501;
	}
	
	public int updateSubjectActivated(String subject, boolean activated){ 		
		MsgSubject sub = findSubjectByName(subject); 
		
		if(sub !=null){
			sub.setActivated(activated);
			sub.setUpdated(new Timestamp(new Date().getTime())); 
			this.save();
			return 200;
		} 		
		return 501;
	}
	
	public MsgSubject findSubjectByName(String subject){
		subject = subject.trim();
		
		if(!subject.isEmpty()){
			Iterator<MsgSubject> listSubject = this.msgSubject.iterator();
			
			while(listSubject.hasNext()){
				
				MsgSubject sub = listSubject.next();
				if(sub.getSubject().equalsIgnoreCase(subject))
					return sub;
				
			}
		}
		return null; 
	}
		
	public int removeSubject(String subject){
		//return this.subjects.remove(findSubjectByName(subject));	
		MsgSubject ms = findSubjectByName(subject);
		
		if(ms !=null){
			if(this.msgSubject.remove(ms)){		 
				this.save();
				return 200; 
			}
			return 201;
		}
		return 501;
	}
	
	public boolean removeAllSubjects(){
		this.msgSubject=new LinkedList<MsgSubject>();;
		return this.validateAndSave();
	}
	
	public Collection<MsgSubject> getSubjects(){
		return this.msgSubject;
	}
	
	public static List<MsgSubject> findAllMsgSubjects(String apikey) {
		MessageManager msg = MessageManager.findByApikey(apikey);
		if(msg!=null){
			return (List<MsgSubject>) msg.msgSubject;
		}
		return null;
	}
	
	public static List<MsgSubject> findAllMsgSubjects(long companyId) {
		MessageManager msg = MessageManager.findByCompanyId(companyId);
		if(msg!=null){
			return (List<MsgSubject>) msg.msgSubject;
		}
		return null;
	}
	
	public static List<String> listOfMsgSubjects(String apikey){
		List<MsgSubject> resultList = MessageManager.findAllMsgSubjects(apikey);
		List<String> subjects = new LinkedList<String>();
		for(MsgSubject subject: resultList){ 
			subjects.add(subject.getSubject());
		}		
		return subjects; 
	}
	
	public static List<String> listOfMsgSubjects(Connection conn, long companyId) throws SQLException{
		List<String> subjects =null;
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("Select subject From msgSubject Where msgsub_id =?");
			query.setLong(1, companyId);
			ResultSet result = query.executeQuery(); 
			subjects =new LinkedList<String>();
			while(result.next()){
				subjects.add(result.getString("subject"));
			}
		}
		return subjects;
	}
	
	public static List<String> listOfMsgSubjects(String apikey, boolean activated){
		List<MsgSubject> resultList = MessageManager.findAllMsgSubjects(apikey);
		List<String> subjects = new LinkedList<String>();
		for(MsgSubject subject: resultList){
			if(subject.getActivated()==activated)
				subjects.add(subject.getSubject()); 
		}		
		return subjects;
	}
	
	public static List<HashMap<String,String>> mapOfMsgSubjects(String apikey){
		List<MsgSubject> resultList = MessageManager.findAllMsgSubjects(apikey);
		//List<String> subjects = new LinkedList<String>();
		List<HashMap<String,String>> subjects =  new LinkedList<HashMap<String,String>>();
		int id =0;
		for(MsgSubject subject: resultList){
			//if(subject.getActivated())			
			//JsonObject json = new JsonObject();
			HashMap<String,String> map = new HashMap<String,String>();
			
			map.put("id",String.valueOf((id++)));
			map.put("subTitle",subject.getSubject());
			map.put("subject",subject.getSubject().trim().replace(' ', '_'));
			map.put("numOfMsg","0");
			
			subjects.add(map);
		}		
		return subjects;
	}
	
	public static List<SubjectSerializer> listOfMsgSubjectsSer(String apikey){
		String cacheKey = CacheKey.genCacheKey(apikey, CacheKey.Subjects,"listSer"); 
		List<SubjectSerializer>subjects = Cache.get(cacheKey, List.class);
		
		if(subjects==null){
			List<MsgSubject> resultList = MessageManager.findAllMsgSubjects(apikey);
			subjects = new LinkedList<SubjectSerializer>();
			int id =0;
			for(MsgSubject subject: resultList){ 
				subjects.add(new SubjectSerializer(id++, subject.getSubject().trim().replace(' ', '_'), subject.getSubject(), 0l));
			}
			CacheKey.set(cacheKey, subjects); 
		} 
		return subjects;
	}
	
	public static String queryMsgSubjects(String apikey){
		List<MsgSubject> resultList = MessageManager.findAllMsgSubjects(apikey);
		String subjects="";
		int size = resultList.size();
		int counter = 0;
		for(MsgSubject subject: resultList){
			//if(subject.getActivated())
			subjects += "'"+subject.getSubject()+"'";
			if(++counter<size)
				subjects +=",";
		}		
		return subjects;
	}
	
	public static String queryMsgSubjects(long companyId){
		List<MsgSubject> resultList = MessageManager.findAllMsgSubjects(companyId);
		String subjects = ""; 
		int size = resultList.size();
		int counter = 0; 
		for(MsgSubject subject: resultList){
			//if(subject.getActivated())
			subjects += "'"+subject.getSubject()+"'"; 
			
			if(++counter<size)
				subjects +=",";
		}		
		return !subjects.equals("")?subjects:null; 
	}
	
	
	//----------------------------------------------------------------------------------------
	//Message SearchKeywords: a list of keywords for identifying incoming messages
	//E.g. {money, i love your product, ......}
		
	public int addKeyword(MsgSearchKeyword keyword){ 		 
		
		if(!containsKeyword(keyword.getKeyword())){
			Timestamp time = new Timestamp(new Date().getTime());
			keyword.setCompanyId(this.getCompanyId()); 
			keyword.setCreated(time);
			keyword.setUpdated(time);
			 if(this.searchkeywords.add(keyword)){
				 return this.validateAndSave() ? 200:201; 	
			 }
			 return 203;
		 } 			
		 return 302;
	}
	
	public int addKeywords(List<MsgSearchKeyword> keywords){ 		
		if(keywords!=null && keywords.size()>0){
			this.searchkeywords = new LinkedList<MsgSearchKeyword>();
			for(MsgSearchKeyword keyword: keywords){
				//if(!containsKeyword(keyword.getKeyword())){
					Timestamp time = new Timestamp(new Date().getTime());
					keyword.setCompanyId(this.getCompanyId()); 
					keyword.setCreated(time);
					keyword.setUpdated(time);
					this.searchkeywords.add(keyword);
				//}
			}
			return this.validateAndSave() ? 200:201; 	
		}
		return 302;
	}
		
	public boolean containsKeyword(String keyword){
		return this.findKeywordByName(keyword) != null; 
	}
		
	public int updateKeyword(String keyword, MsgSearchKeyword newSearchKeyword){
	 		
		MsgSearchKeyword kw = findKeywordByName(keyword);
		
		if(kw != null){ 
			
			//check if the update QR and the existing has the same title
			if(!kw.getKeyword().equalsIgnoreCase(newSearchKeyword.getKeyword()) ){
				
				//check for duplication of names
				if(containsKeyword(newSearchKeyword.getKeyword()) ){					
					return 302; 
				}else{
					kw.setKeyword(newSearchKeyword.getKeyword());
				}				
			}else{
				kw.setKeyword(newSearchKeyword.getKeyword());
			} 
			
			kw.setActivated(newSearchKeyword.getActivated());	
			kw.setUpdated(new Timestamp(new Date().getTime())); 
			
			return this.validateAndSave() ? 200:201; 			 
		}
		return 501;
		
	}
	
	public int updateKeywordActivated(String keyword, boolean activated){ 		
		MsgSearchKeyword key = findKeywordByName(keyword); 
		
		if(key !=null){
			key.setActivated(activated);
			key.setUpdated(new Timestamp(new Date().getTime())); 
			this.save();
			return 200;
		} 		
		return 501;
	}
	
	public MsgSearchKeyword findKeywordByName(String keyword){
		
		keyword = keyword.trim(); 
		
		if(!keyword.isEmpty()){
			 Iterator<MsgSearchKeyword> listKeyword = this.searchkeywords.iterator();
			
			while(listKeyword.hasNext()){
				MsgSearchKeyword key = listKeyword.next();
				if(key.getKeyword().equalsIgnoreCase(keyword))
					return key;
			}
		}
		return null; 
	}
		
	public int removeKeyword(String keyword){
		
		MsgSearchKeyword key = findKeywordByName(keyword);
		
		if(key !=null){
			if(this.searchkeywords.remove(key)){
				this.save();
				return 200; 
			} 
			
			return 201;
		}
		return 501;
	}
	
	public Collection<MsgSearchKeyword> getKeywords(){
		return this.searchkeywords;
	}
	
	
	//----------------------------------------------------------------------------------------
	//getters and setters
 
	private void setSettingId(Long setting) {
		this.settingId = setting;
	}
	
	public Long getSettingId(){
		return this.settingId;
	}
	
	public Setting getSetting(){
		return Setting.findById(this.getSettingId());
	}

	public Long getCompanyId() {
		return companyId;
	}
	
	public Company getCompany(){
		return Company.findById(this.companyId); 
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
}
