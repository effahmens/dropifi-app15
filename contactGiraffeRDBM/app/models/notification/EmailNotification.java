package models.notification; 

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import play.data.validation.Required;
import play.db.jpa.Model;
import play.libs.Codec;

@Entity
public class EmailNotification extends Model{ 
	@Required @Column(nullable=false, unique=true)
	private String notifyKey;
	@Required @Column(nullable=false, unique=true)
	private String apikey; 
	@Required
	private boolean isSubscribed;
	@Required
	private Timestamp created; 
	@Required
	private Timestamp updated;
	
	public EmailNotification(String apikey, boolean isSubscribed) {
		super(); 
		this.apikey = apikey; 
		this.isSubscribed = isSubscribed; 
	} 
	
	public int createNotification(){
		try{ 
			if(findByApikey(this.getApikey())==null){
				this.setNotifyKey();
				this.setCreated(new Timestamp(new Date().getTime()));
				this.setUpdated(this.getCreated());
				return this.validateAndSave()?200:501;
			}else{
				return 305;
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return 501;
	} 
	
	public boolean updateNotification(boolean isSubscribed){
		this.setSubscribed(isSubscribed);
		this.setUpdated(new Timestamp(new Date().getTime()));
		return this.validateAndSave();
	}
	
	public static boolean subscribeToNotification(String apikey){
		EmailNotification found = findByApikey(apikey);
		if(found!=null){
			return found.updateNotification(true);
		}
		return false;
	}
	
	/**
	 * Set the status of the email notification to either true or false using the notifyKey as the search term
	 * @param notifyKey
	 * @param isSubscribed
	 * @return
	 */
	public static boolean unSubscribeToNotification(String notifyKey, boolean isSubscribed){
		EmailNotification found = findByNotifyKey(notifyKey); 
		if(found!=null){
			return found.updateNotification(isSubscribed);
		}
		return false;
	}
	
	public static EmailNotification findByApikey(String apikey){
		return EmailNotification.find("byApikey", apikey).first();
	}
	
	public static EmailNotification findByNotifyKey(String notifyKey){ 	
		return EmailNotification.find("byNotifyKey", notifyKey).first();
	}
	
	private void setNotifyKey(){ 
		this.notifyKey = Codec.hexSHA1(this.getApikey() + new Date().getTime());
	}
	
	public String getNotifyKey() {
		return notifyKey;
	}
	
	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public boolean isSubscribed() {
		return isSubscribed;
	}

	public void setSubscribed(boolean isSubscribed) {
		this.isSubscribed = isSubscribed;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	
	
}
