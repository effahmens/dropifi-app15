/**
 * @description 
 * @author phillips effah mensah
 * @date 14/3/2012
 *  
 **/

package models;

import static akka.actor.Actors.actorOf;
import play.data.validation.*;
import play.db.jpa.Model;
import resources.DCON;
import resources.RuleActionType;
import resources.RuleCompareType;
import resources.RuleConditionType;
import resources.RuleGroup;
import resources.RuleParameters;
import resources.RuleType;

import javax.mail.Address;
import javax.persistence.*;

import adminModels.DropifiMailer;
import akka.actor.ActorRef;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import job.process.MessageActor;
import job.process.RuleActor;

public abstract class  MainRule extends Model {

	//------------------------------------------------------------------------------------
	//Rule Conditions and Actions
	
	public Map<Boolean, Map<String, Object>> widgetActionPerformed(List<WidgetRule> rules, Long companyId,RuleParameters params) {
		 
		Map<Boolean, Map<String, Object>> outPut = new HashMap<Boolean, Map<String,Object>>();
		if( rules != null && companyId !=null && params !=null){
			List<RuleAction> listActions = new LinkedList<RuleAction>();
			
			//search through the list of WidgetRules and create a list of actions to executed
			boolean hasStopProcess = false;
			for(WidgetRule rule : rules){
				if(executeRuleCondition(rule.ruleConditions, rule.getRuleGroup(), params)){
					for(RuleAction action : rule.ruleActions){
									
						if(action.getAction().equals(RuleActionType.stop_processing.name())){//priority search for stop processing	
							//play.Logger.log4j.info("Action: "+action.getAction());
							hasStopProcess = true;
							break;
						}else if(!listActions.contains(action)){//check that the action set is not added already			
							listActions.add(action);		
							
							//determine if the action type is Send Automatic Response
							if(RuleActionType.valueOf(action.getAction()).equals(RuleActionType.send_automatic_response_to_sender)){
								params.hasSendAutoResponse = true;
							}
						}
					} 
					
					if(hasStopProcess){	//stop processing
						listActions = null;
						outPut.put(false, null); 						 
						return outPut; 
					}
				} 				
			}
			
			Map<String, Object> ruleObject = new HashMap<String, Object>(); 
			if(listActions!=null){				
				ruleObject.put("actions", listActions);
				ruleObject.put("companyId", companyId);
				ruleObject.put("params", params);
				
				//send the actions to rule actor to execute the rule actions 
				//ActorRef ruleActor = actorOf(RuleActor.class).start(); 
				//ruleActor.tell(ruleObject);								 
			}
			outPut.put(true, ruleObject);
			return outPut;
		}
		outPut.put(true, null);
		return outPut; 
	}
	
	public Map<Boolean, Map<String, Object>> msgActionPerformed(List<MsgRule> rules, Long companyId,RuleParameters params){
		Map<Boolean, Map<String, Object>> outPut = new HashMap<Boolean, Map<String,Object>>();
		if( rules != null && companyId !=null && params !=null){
			List<RuleAction> listActions = new LinkedList<RuleAction>();
			
			//search through the list of WidgetRules and create a list of actions to executed
			boolean hasStopProcess = false;
			for(MsgRule rule : rules){
				if(executeRuleCondition(rule.ruleConditions, rule.getRuleGroup(), params)){
					for(RuleAction action : rule.ruleActions){
									
						if(action.getAction().equals(RuleActionType.stop_processing.name())){//priority search for stop processing	
							//play.Logger.log4j.info("Action: "+action.getAction());
							hasStopProcess = true;
							break;
						}else if(!listActions.contains(action)){//check that the action set is not added already			
							listActions.add(action);		
							
							//determine if the action type is Send Automatic Response
							if(RuleActionType.valueOf(action.getAction()).equals(RuleActionType.send_automatic_response_to_sender)){
								params.hasSendAutoResponse = true;
							}
						}
					} 
					
					if(hasStopProcess){	//stop processing
						listActions = null;
						outPut.put(false, null); 						 
						return outPut; 
					}
				} 				
			}
			
			Map<String, Object> ruleObject = new HashMap<String, Object>(); 
			if(listActions!=null){				
				ruleObject.put("actions", listActions);
				ruleObject.put("companyId", companyId);
				ruleObject.put("params", params);
				
				//send the actions to rule actor to execute the rule actions 
				//ActorRef ruleActor = actorOf(RuleActor.class).start(); 
				//ruleActor.tell(ruleObject);								 
			}
			outPut.put(true, ruleObject);
			return outPut;
		}
		outPut.put(true, null);
		return outPut;  
	}

	public static int executeRuleAction(Connection conn,List<RuleAction> actions,long companyId,RuleParameters params, List recipients){ 
		 
		//perform the actions on the rule;
		for(RuleAction action : actions) {
			RuleActionType type = RuleActionType.valueOf(action.getAction());
			//play.Logger.log4j.info("failed sending auto response to "+ action.getAction());
			switch(type.ordinal()){ 
				case 0:	//send_automatic_response_to_sender	
					//verify that an auto response has not being sent already to the sender of the message
					//play.Logger.log4j.info("autoresponse message to " + action.getExpectation());
					if(!params.isResponseSent){
								
						try{
							
							//search for the auto-response	
							QuickResponse response = MessageManager.findQuickResponse(conn, companyId, action.getExpectation());
							if(response !=null) {
								int rsstatus = DropifiMailer.sendWidgetAutomaticResponse(conn,params.compProfile,params.sender, params.getContactName(), 
										response.getSubject(),response.getMessage(),params.companyMailbox,params.autoReplyTo);
								
								//set the status of auto-response sent to 'true' if the auto-response was successfully delivered to the sender
								params.isResponseSent = rsstatus==200?true:false;
							}
						} catch (Exception e) {
							//TODO Auto-generated catch block
							//play.Logger.log4j.info("failed sending auto response to "+ params.sender, e);
						} 
					} 
					break;
				case 1:	 //forward_to_other_email_address
					//check if the recipient email address is not in the list of recipient email address					    
				    String recipient = action.getExpectation(); 
				    if(!recipients.contains(recipient)){
				    	 //play.Logger.log4j.info("foward message to " + recipient);
						try {
							DropifiMailer.sendFowardAutomaticNotification(conn,action.getExpectation(), params, params.domain);
							recipients.add(recipient); 
							//play.Logger.log4j.info("Forwarded message to " + action.getExpectation());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							//play.Logger.log4j.info("failed fowarding message to " + action.getExpectation(), e);
						} 
				    }else{
				    	//play.Logger.log4j.info("already fowarded message to " + action.getExpectation());
				    } 
					
					break;
				case 2:	//mark_as_customer
					try {
						Customer.updateIsCustomer(conn,companyId, params.sender, true);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						//play.Logger.log4j.info("failed updating customer status "+action.getExpectation(), e);
					}
					break;					
				case 3:	//stop_processing
					return 707;
			}
		}
		
		try {
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			play.Logger.info(e.getMessage(), e);
		}
		return 701;
	}
	
	private boolean executeRuleCondition(Collection<RuleCondition> conditions, RuleGroup ruleGroup, RuleParameters params){
		boolean isMet = false;
		int success = 0;
		
		//get the status of the customer - whether is a customer or prospect
		Boolean isCustomer = null;//Customer.isCustomer(params.apikey, params.sender);
		
		for(RuleCondition condition : conditions) {
			RuleConditionType type = RuleConditionType.valueOf(condition.getCondition()); 
			RuleCompareType compareType = RuleCompareType.valueOf(condition.getComparison());			 
			switch(type.ordinal()){
				case 0:						//from 
					success = compareRule(compareType, params.sender, condition.getExpectation())?(++success):success;
					break;
				case 1:						//subject
					success = compareRule(compareType, params.subject, condition.getExpectation())?(++success):success;
					break;
				case 2:						//entire_message
					success = compareRule(compareType, params.message, condition.getExpectation())?(++success):success;
					break;
				case 3:						//sender_is_a_customer	
					if(isCustomer == null){
						isCustomer = Customer.isCustomer(params.apikey, params.sender);
						params.isCustomer =String.valueOf(isCustomer);
					}
					
					condition.setExpectation(condition.getComparison());
					success = compareRule(compareType, params.isCustomer, condition.getExpectation())?(++success):success;
					break;
				case 4:						//sender_is_a_prospect
					if(isCustomer == null){
						isCustomer = Customer.isCustomer(params.apikey, params.sender);
						params.isCustomer = String.valueOf(isCustomer); 
					}
					
					condition.setExpectation(condition.getComparison());
					success = compareRule(compareType, params.isCustomer, condition.getExpectation())?(++success):success;
					break;
				case 5:						//contact_name
					success = compareRule(compareType, params.getContactName(), condition.getExpectation())?(++success):success;
					break;
				case 6:						//contact_email_address
					success = compareRule(compareType, params.sender, condition.getExpectation())?(++success):success;
					break;					
				case 7:						//message_sentiment
					success = compareRule(compareType, params.sentiment, condition.getExpectation())?(++success):success;
					break;
				case 8:						//message_emotion
					success = compareRule(compareType, params.emotion, condition.getExpectation())?(++success):success; 
					break;
				case 9:						//location_country
					success = compareRule(compareType, params.country, condition.getExpectation())?(++success):success;
					break;
				case 10:					//location_state
					success = compareRule(compareType, params.state, condition.getExpectation())?(++success):success; 
					break;
				case 11:				   //location_city
					success = compareRule(compareType, params.city, condition.getExpectation())?(++success):success; 
					break;
				case 12:				//page_url
					success = compareRule(compareType, params.pageUrl, condition.getExpectation())?(++success):success;
					//play.Logger.log4j.info(params.pageUrl+" Condition : Expectation "+condition.getExpectation());
					break;
			} 			
		}
		
		//base on the ruleGroup to determine if the conditions are met
		switch(ruleGroup.ordinal()){
			case 0:				//all conditions met (AND - operator)
				isMet = (success == conditions.size())?true:false;
				if(isMet){
					params.condition = redirectCondition(conditions);
				}
				//play.Logger.info("All Condtion Met"+isMet, isMet);
				break;
			case 1:				//some of the conditions met (OR - operator)
				isMet = success > 0 ? true:false;
				if(isMet){
					params.condition = redirectCondition(conditions);
				}
				//play.Logger.info("Any Condtion Met"+isMet, isMet);
				break;
		}		
		return isMet;
	}
	
	private String redirectCondition(Collection<RuleCondition> conditions){
		String cond ="";
		if(conditions!=null){
			for(RuleCondition condition: conditions){
				cond +="If("+condition.getCondition()+" "+condition.getComparison()+" "+condition.getExpectation()+")<br/>";
			}
		}
		return cond;
	} 
	
	private boolean compareRule(RuleCompareType type,String content,String expectation){
		boolean isMet = false;
		
		if(content==null)return isMet;
		
		content = content.trim().toLowerCase();
		expectation = expectation.trim().toLowerCase(); 
		int index = type.ordinal();
		switch(index){
			case 0: case 1: case 2: case 3: case 4: case 5: 		//general comparison
				switch(index){
					case 0:						//contains
						isMet = content.contains(expectation);
						break;
					case 1:						//does_not_contains
						isMet = !content.contains(expectation);
						break;
					case 2:						//begins_with
						isMet = content.startsWith(expectation);
						break;
					case 3:						//ends_with
						isMet = content.endsWith(expectation);
						break;
					case 4:						//is equal to
						isMet = content.equals(expectation);
						break;
					case 5:						//is not equal to
						isMet = !content.equals(expectation);
						break;					 
				}
				break;
				
			case 6: case 7: 			//customer prospect comparison
				//fetch the status of the contact from the database
				
				switch(index){
					case 6:						//customer
						isMet = content.equals(expectation); 					 
						break;
					case 7:						//prospect
						isMet = content.equals(expectation);						 
						break;
				}
				break;
				
			case 8: case 9: case 10:			//sentiment comparison
				switch(index){
					case 8:						//positive
						isMet = content.equals("positive"); 
						break;
					case 9:						//negative
						isMet = content.equals("negative"); 
						break;
					case 10:					//neutral
						isMet = content.equals("neutral"); 
						break;
				}
				break;
			case 11: case 12: case 13: case 14: case 15: case 16: case 17: case 18: 	//emotion comparison
				switch(index){
					case 11:					//affection_friendliness
						isMet = content.equals("affection_friendliness"); 
						break;
					case 12:					//amusement_excitment
						isMet = content.equals("amusement_excitment");
						break;
					case 13:					//contentment_gratitude
						isMet = content.equals("contentment_gratitude");
						break;
					case 14:					//enjoyment_elation
						isMet = content.equals("enjoyment_elation");
						break;
					case 15:					//anger_loathing
						isMet = content.equals("anger_loathing");
						break;
					case 16:					//fear_uneasiness
						isMet = content.equals("fear_uneasiness");
						break;
					case 17:					//humailiation_shame
						isMet = content.equals("humailiation_shame");
						break;
					case 18:					//sadness_grief
						isMet = content.equals("sadness_grief");
						break;
				}
				break;
		}
		
		return isMet;
	}
	
	//-------------------------------------------------------------------------------------
	//conditions
	
	public abstract RuleCondition addCondition(RuleCondition condition);
	
	public abstract boolean updateCondition(Long id, RuleCondition condition); 
		
	public abstract boolean removeCondition(String condition, String comparison,String expectation);
 	
	public abstract RuleCondition findConditionByAll(String condition, String comparison,String expectation); 
	
	public abstract Collection<RuleCondition> getConditions(); 
	
	 
	//-------------------------------------------------------------------------------------
	//actions 
		
	public abstract RuleAction addAction(RuleAction action);
	
	public abstract boolean updateAction(Long id, RuleAction action); 
	
	public abstract boolean removeAction(String action, String expectation);
		
	public abstract RuleAction findActionByAll(String action, String expectation); 
	
	public abstract Collection<RuleAction> getActions(); 
	
	
	//groups
	public abstract RuleGroup getRuleGroup(); 
	//--------------------------------------------------------------------------------------
	//	 
	
	//check that a rule with similar name is not created
	//if similar name exist, append "Copy(number) of "to the beginning of the name
	public static String nameCheck(List listRule, String ori, String name, int count){
		return "";
	}
	
	//-----------------------------------------------------------------------------------------
	//getters and setters
	
	public abstract Long getMessageManagerId();
	
	public abstract void setMessageManagerId(long messageManager) ;

	public abstract String getName();

	public abstract void setName(String name);

	public abstract RuleType getType();

	public abstract void setType(RuleType type) ;
	
	public abstract Boolean getActivated();

	public abstract void setActivated(Boolean activated);	 
	
}
