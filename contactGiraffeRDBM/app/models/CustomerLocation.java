package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

import play.db.jpa.Model;
import play.db.jpa.Transactional;

@Entity
public class CustomerLocation  extends Model{
	@Column(nullable=false)
	private Long customerId;	
	@Column(nullable=false)
	private String apikey;
	@Column(nullable=false)
	private String email;
	@Embedded
	private GeoLocation location;
 
	public CustomerLocation(Long customerId, String apikey, String email,GeoLocation location) {
		super();
		this.customerId = customerId;
		this.apikey = apikey;
		this.email = email;
		this.location = location;
	}
	
	public CustomerLocation(){}
	
	@Transactional
	public CustomerLocation addUpdateLocation(){
		CustomerLocation found = findByEmail(this.getApikey(), this.getEmail());
		
		if(found!=null){
			if(this.hasLocation()){
				Date date = new Date(); 
				this.getLocation().setCreated(new Timestamp(date.getTime()));
				found.setLocation(this.getLocation());
				found = found.save();
			}
		}else{
			found = this.save();
		}
		
		return found;
	}
	
	public void saveLoaction(Connection conn){
		if(isCreated(conn, this.getApikey(), this.getEmail())){
			updateLocation(conn);
		}else{
			insertLocation(conn);
		}
	} 
	
	private void insertLocation(Connection conn){
		try{
			 
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Insert into CustomerLocation (customerId,apikey,email,city,region,regionName,postalCode,countryCode,countryName,latitude,longitude,ipAddress,created) " +
						" value(?,?,?,?,?,?,?,?,?,?,?,?,?)");
				
				int i =1;
				query.setLong(i++, this.getCustomerId());
				query.setString(i++, this.getApikey());
				query.setString(i++, this.getEmail());
				query.setString(i++, this.getLocation().getCity());
				query.setString(i++, this.getLocation().getRegion());
				query.setString(i++, this.getLocation().getRegionName());
				query.setString(i++, this.getLocation().getPostalCode());
				query.setString(i++, this.getLocation().getCountryCode());
				query.setString(i++, this.getLocation().getCountryName());
				query.setDouble(i++, this.getLocation().getLatitude());
				query.setDouble(i++, this.getLocation().getLongitude());
				query.setString(i++, this.getLocation().getIpAddress());
				this.getLocation().setCreated(new Timestamp(new Date().getTime()));
				query.setTimestamp(i++, this.getLocation().getCreated());
				
				query.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	private void updateLocation(Connection conn){
		try{
			 
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Update CustomerLocation Set city=?,region=?,regionName=?,postalCode=?,countryCode=?,countryName=?,latitude=?,longitude=?,ipAddress=? " +
						" Where customerId=? and apikey=? and email=?");
				
				int i =1; 
				query.setString(i++, this.getLocation().getCity());
				query.setString(i++, this.getLocation().getRegion());
				query.setString(i++, this.getLocation().getRegionName());
				query.setString(i++, this.getLocation().getPostalCode());
				query.setString(i++, this.getLocation().getCountryCode());
				query.setString(i++, this.getLocation().getCountryName());
				query.setDouble(i++, this.getLocation().getLatitude());
				query.setDouble(i++, this.getLocation().getLongitude());
				query.setString(i++, this.getLocation().getIpAddress());
				//this.getLocation().setCreated(new Timestamp(new Date().getTime()));
				//query.setTimestamp(i++, this.getLocation().getCreated());
				query.setLong(i++, this.getCustomerId());
				query.setString(i++, this.getApikey());
				query.setString(i++, this.getEmail());
				
				query.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static CustomerLocation findByEmail(String apikey,String email){
		return CustomerLocation.find("Select l From CustomerLocation l Where l.apikey=? and l.email=?", apikey,email).first();
	}
	
	public static GeoLocation findLocation(String apikey,String email){
		return CustomerLocation.find("Select l.location From CustomerLocation l Where l.apikey=? and l.email=?", apikey,email).first();
	}
	
	private static boolean isCreated(Connection conn, String apikey,String email){
		try {
			if(conn!=null && !conn.isClosed()){
				PreparedStatement query = conn.prepareStatement("Select c.customerId from CustomerLocation c Where c.apikey=? and c.email=?");
				int i =1;
				query.setString(i++, apikey);
				query.setString(i++, email);
				ResultSet result = query.executeQuery();
				while(result.next()){
					//result.getLong("customerId");
					return true;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			play.Logger.log4j.info(e.getMessage(),e);
		}

		return false;
	}
	
	
	public boolean hasLocation(){
		GeoLocation loc = this.getLocation();
		if(loc!=null){
			if(loc.getCity()!=null && loc.getCountryName()!=null){
				return true;
			}
		}
		return false;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public GeoLocation getLocation() {
		return location;
	}

	public void setLocation(GeoLocation location) {
		this.location = location;
	}
	
	
}
