package models;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*; 

import javax.persistence.*;

import org.hibernate.annotations.Index;

import com.google.gson.annotations.SerializedName;

import controllers.Widget_Rules;

import play.db.jpa.Model;
import play.libs.F;
import play.libs.F.Promise;
import resources.CacheKey;
import resources.DropifiTools;
import resources.RuleGroup;
import resources.RuleType;

@Entity
public class MsgRule extends MainRule implements Serializable{
	@Column(nullable=false) @Index(name="messageManagerId")
	private Long messageManagerId; 	
	@Column(nullable=false) @Index(name="companyId")
	private Long companyId; 
	
	@Column(length=100, nullable=false)
	@SerializedName(value="name")
	private String name;

	@Column(length=100, nullable=false)
	@SerializedName(value="type")
	private RuleType type;

	@Column(nullable=false)
	@SerializedName(value="activated")
	private Boolean activated;
	@SerializedName(value="rulegroup")
	private RuleGroup ruleGroup;	
	  
	@ElementCollection(targetClass = RuleCondition.class)
	@CollectionTable(name="msgRuleCondition", joinColumns=@JoinColumn(name="msgCond_Id"))
	public Collection<RuleCondition> ruleConditions;
	
	@ElementCollection(targetClass = RuleAction.class)
	@CollectionTable(name="msgRuleAction", joinColumns=@JoinColumn(name="msgAct_Id")) 
	public Collection<RuleAction> ruleActions;
	
	private Timestamp created; 
	private Timestamp updated;
	
	//------------------------------------------------------------------------------------
	//constructor
	
	public MsgRule(long companyId,String name, boolean activated, RuleGroup ruleGroup) {		 
		//TODO Auto-generated constructor stub 			 
		this.setName(name);
		this.setType( RuleType.Message);
		this.setActivated(activated);
		this.setRuleGroup(ruleGroup); 
		this.setCompanyId(companyId);
		this.ruleConditions = new LinkedList<RuleCondition>();
		this.ruleActions = new LinkedList<RuleAction>();
	}
	
	public MsgRule(long companyId,String name){
		this(companyId,name,true, RuleGroup.ALL);
	} 
	
	public MsgRule(){
		 
	} 
	
	//-------------------------------------------------------------------------------------
	//Rule: create, update, remove and find widget rule
	
	public int createOrUpdateRule(){ 
		//TODO Auto-generated method stub	 
		
		if(this !=null && this.getMessageManagerId()!= null && this.getCompanyId()!=null){ 
			
			MsgRule rule = findByName(this.getCompanyId(), this.getName());			
			if(rule == null){ 
				//save the new message rule
				 Timestamp time = new Timestamp(new Date().getTime());
				 this.setCreated(time);
				 this.setUpdated(time); 
				 return this.validateAndSave()?200:201; 				 
			}else{ 
				return 305;
			} 
		}
		return 501;
	}
	
	public static void createFilterRules(String emailfilters,long companyId, long inboundId){
		try{
			MsgRule.removeMsgRule(companyId,inboundId);
			
			if(emailfilters!=null && !emailfilters.isEmpty()){
				String[] filters = emailfilters.split(",");
				for(String filter : filters){
					filter = filter.trim().toLowerCase(); 
					if(!filter.isEmpty()){
						MsgRule rule = new MsgRule(companyId, (inboundId+"_"+filter), true, RuleGroup.ANY);
						rule.setType(RuleType.Message);
						
						List<RuleCondition>conditions = new LinkedList<RuleCondition>();
						conditions.add(new RuleCondition("from", "contains", filter));
						rule.addConditions(conditions);
						
						List<RuleAction>actions = new LinkedList<RuleAction>();
						actions.add(new RuleAction("stop_processing", "true"));
						rule.addActions(actions);
						
						MessageManager msg = MessageManager.findByCompanyId(companyId); 
						msg.addMsgRule(rule);
					}
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
	}
	
	public static List<MsgRule> appendRuleFilters(Connection conn,long companyId,String apikey,List<MsgRule> rules){
		try{
			List<String> filters = InboundMailbox.findAllMonitoredEmail(conn,apikey);
			filters.add(DropifiTools.mailEmail);
			filters.add(DropifiTools.teamEmail);
			for(String filter: filters){
				MsgRule rule = new MsgRule(companyId, ("Other"+"_"+filter), true, RuleGroup.ANY);
				rule.setType(RuleType.Message);
				
				List<RuleCondition>conditions = new LinkedList<RuleCondition>();
				conditions.add(new RuleCondition("from", "contains", filter));
				rule.addConditions(conditions);
				
				List<RuleAction>actions = new LinkedList<RuleAction>();
				actions.add(new RuleAction("stop_processing", "true"));
				rule.addActions(actions);
				rules.add(rule);
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		} 
		return rules;
	}
	
	public static int updateRule(Long companyId,String originalName, String name, boolean activated, RuleGroup ruleGroup,List<RuleCondition> ruleConditions,List<RuleAction> ruleActions){
		MsgRule rule = MsgRule.findByName(companyId, originalName);
		if(rule !=null && rule.getId()!=null){
			//the new name is not equal to the original plan 
			
			if(!originalName.equalsIgnoreCase(name)){
				MsgRule found = MsgRule.findByName(companyId, name);
				if(found!=null && found.getId()!=null)
					return 305;
				
				rule.setName(name);
			}
			rule.setUpdated(new Timestamp(new Date().getTime()));
			rule.setActivated(activated);
			rule.setRuleGroup(ruleGroup) ;	
			rule.ruleActions = ruleActions;		
			rule.ruleConditions = ruleConditions;
			return rule.validateAndSave()?200:201; 
		}
		return 501;
	}
	
	public static int removeMsgRule(long companyId,String name){
		MsgRule rule = MsgRule.findByName(companyId, name);
		if(rule != null){
			return rule.delete()!=null ? 200:201;			 
		}
		return 501;	
	}
	
	public static int removeMsgRule(long companyId,long inboundId){ 
		String inmae = (inboundId+"_%"); 
		return MsgRule.delete("Delete From MsgRule m Where m.companyId=? and m.name like ?", companyId,inmae);
	}
	
	public static int updateActivated(long companyId,String name, boolean activated){
		int status = MsgRule.em().createQuery("Update MsgRule m Set m.activated = ? Where m.companyId =? And m.name = ? And m.type =?")
		.setParameter(1, activated).setParameter(2,companyId).setParameter(3,name).setParameter(4, RuleType.Message).executeUpdate();  
		return status>0?200:201; 
	}
		
	public static MsgRule findByName(Long companyId, String name){
		return MsgRule.find("byCompanyIdAndTypeAndName",companyId,RuleType.Message,name).first();
	}
	
	public static List<MsgRule> findAll(Long companyId){
		return MsgRule.find("byCompanyIdAndType",companyId,RuleType.Message).fetch();
	}
	
	public static List<MsgRule> findAll(Connection conn,long companyId) { 
		List<MsgRule> rules = new LinkedList<MsgRule>();
		try{
		if(conn!=null && !conn.isClosed()){ 
			
			PreparedStatement query = conn.prepareStatement("Select w.activated, w.id, w.companyId,w.messageManagerId, w.name, w.ruleGroup, w.type, w.updated, w.created" +
					" from MsgRule w, Company c where  w.companyId = c.id  and w.type = ? and c.id = ?");
			
			query.setInt(1, RuleType.Message.ordinal());
			query.setLong(2, companyId);			
			ResultSet result = query.executeQuery();
			 
			while(result.next()){ 
				MsgRule rule = new MsgRule();
				
				rule.id = result.getLong("id"); 
				rule.setActivated(result.getBoolean("activated"));
				rule.setCompanyId(result.getLong("companyId"));
				rule.setMessageManagerId(result.getLong("messageManagerId"));
				rule.setName(result.getString("name"));
				rule.setRuleGroup(RuleGroup.getType(result.getInt("ruleGroup")));
				rule.setType(RuleType.valueOf(result.getInt("type")));
				rule.setCreated(result.getTimestamp("created"));
				rule.setUpdated(result.getTimestamp("updated"));
				//search for all the conditions attached to this rule
				rule.ruleConditions = findRuleConditions(conn ,rule.getCompanyId(),rule.getId(), rule.getName());
				//search for all the actions attached to this rule;
				rule.ruleActions = findRuleActions(conn ,rule.getCompanyId(),rule.getId(), rule.getName()); 
				rules.add(rule);
			}
			
		}
		}catch(Exception e){
			play.Logger.log4j.error(e.getMessage(), e);
		}
		return rules;
	}
	
	public static List<MsgRule> findWidgetMsgRules(Connection conn,long companyId,String apikey) throws SQLException{
		List<MsgRule> rules = findAll(conn,companyId);
		List<WidgetRule> wrules = WidgetRule.findAll(conn,companyId,true);
		
		for(WidgetRule wrule : wrules){
			MsgRule rule = new MsgRule(); 
			
			rule.setActivated(wrule.getActivated());
			rule.setCompanyId(wrule.getCompanyId());
			rule.setMessageManagerId(wrule.getMessageManagerId());
			rule.setName(wrule.getName());
			rule.setRuleGroup(wrule.getRuleGroup());
			rule.setType(wrule.getType());
			rule.setCreated(wrule.getCreated());
			rule.setUpdated(wrule.getUpdated());
			//search for all the conditions attached to this rule
			rule.ruleConditions = WidgetRule.findRuleConditions(conn ,wrule.getCompanyId(),wrule.getId(), wrule.getName());
			//search for all the actions attached to this rule;
			rule.ruleActions = WidgetRule.findRuleActions(conn ,wrule.getCompanyId(),wrule.getId(), wrule.getName()); 
			rules.add(rule);
		}
		
		rules = appendRuleFilters(conn,companyId, apikey, rules);
		/*F.Promise<List<Object>> rules1 =  (Promise<List<Object>>) findAll(conn,companyId);
		F.Promise<List<Object>> rules2 =  (Promise<List<Object>>) WidgetRule.findAll(conn,companyId,true);
		Promise<List<List<Object>>> promises = F.Promise.waitAll(rules1,rules2);
		
		 
		await(promises, new F.Action<List<List<Object>>>() {

			@Override
			public void invoke(List<List<Object>> result) {
				// TODO Auto-generated method stub
				
			} 
			
		});
		*/
		return rules;
	}
	
	private static Collection<RuleCondition> findRuleConditions(Connection conn ,Long companyId,Long id, String name) throws SQLException{
		Collection<RuleCondition> conditions = new LinkedList<RuleCondition>();
		if(conn !=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select r.msgCondition, r.msgComparison, r.msgExpectation from " +
					" msgRuleCondition r, MsgRule w where r.msgCond_Id = w.id and w.id = ? and w.name = ? and w.companyId = ?");
			query.setLong(1, id);
			query.setString(2, name);		 
			query.setLong(3, companyId);
			
			ResultSet result = query.executeQuery();
		
			while(result.next()){
				
				RuleCondition condition = new RuleCondition();
				condition.setCondition(result.getString("msgCondition"));
				condition.setComparison(result.getString("msgComparison"));
				condition.setExpectation(result.getString("msgExpectation"));
				
				conditions.add(condition);
			}			
		}
		return conditions;
	}
	
	private static Collection<RuleAction> findRuleActions(Connection conn ,Long companyId,Long id, String name) throws SQLException{
		Collection<RuleAction> actions = new LinkedList<RuleAction>();
		if(conn !=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select r.action, r.expectation from " +
					" msgRuleAction r, MsgRule w where r.msgAct_Id = w.id and w.id = ? and w.name = ? and w.companyId = ?");
			
			query.setLong(1, id);
			query.setString(2, name);		 
			query.setLong(3, companyId);
			
			ResultSet result = query.executeQuery();
			
			while(result.next()) {
				
				RuleAction action = new RuleAction(); 
				action.setAction(result.getString("action"));				 
				action.setExpectation(result.getString("expectation"));				
				
				actions.add(action);				
			}			
		}
		return actions;
	}
	
	//-------------------------------------------------------------------------------------
	//RULE CONDITIONS: adding, removing and updating msgRule condition
 
	@Override
	public Collection<RuleCondition> getConditions() {
		// TODO Auto-generated method stub
		return this.ruleConditions;
	}

	@Override
	public RuleCondition addCondition(RuleCondition condition) {
		// TODO Auto-generated method stub
		//check that a similar rule condition with the same parameter values do not exist
		
		RuleCondition cond = findConditionByAll(condition.getCondition(), condition.getComparison(), condition.getExpectation());
		
		if(this != null && cond == null){
			//save the RuleCondition if all parameter conditions are satisfied
			this.ruleConditions.add(condition);
			if(this.validateAndSave())
				return condition;
		}			
		return null;
	}
	
	
	
	public void addConditions(List<RuleCondition> conditions) {
		this.ruleConditions= conditions;
	}

	@Override
	public boolean updateCondition(Long id, RuleCondition condition) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeCondition(String condition, String comparison,String expectation) {
		// TODO Auto-generated method stub
		return removeCondition(new RuleCondition(condition, comparison, expectation));
	}
	
	public boolean removeCondition(RuleCondition condtion) {
		// TODO Auto-generated method stub
		return this.ruleConditions.remove(condtion);
	}
		
	@Override
	public RuleCondition findConditionByAll(String condition, String comparison,
			String expectation) {
		// TODO Auto-generated method stub
		
		RuleCondition control = new RuleCondition(condition, comparison, expectation);
		//return RuleCondition.findConditionByAll(this.getId(), condition, comparison, expectation);
		Iterator<RuleCondition> cond = this.ruleConditions.iterator();
		
		while(cond.hasNext()){
			RuleCondition ruleCond = cond.next(); 			
			if(ruleCond.equals(control)){
				return ruleCond;
			}
		}
		
		return null;
	}

	
	//-----------------------------------------------------------------------------------------------
	//RULE ACTIONS: adding, removing and updating msgRule condition
	
	@Override
	public RuleAction addAction(RuleAction action) {
		// TODO Auto-generated method stub
		
		//check that a similar rule condition with the same parameter values do not exist
		RuleAction foundAction = findActionByAll(action.getAction(), action.getExpectation());
		
		if(this != null && foundAction == null){
			 this.ruleActions.add(action); 			 
			 if(this.validateAndSave())
				 return action;
		}
		return null;
	}
	
	public void addActions(List<RuleAction> actions){
		this.ruleActions = actions;
	}

	@Override
	public boolean updateAction(Long id, RuleAction action) {
		// TODO Auto-generated method stub
		return false;
	}
 	
	@Override
	public boolean removeAction(String action, String expectation) {
		// TODO Auto-generated method stub
		return removeAction(new RuleAction(action,expectation));
	}
	
	public boolean removeAction(RuleAction action){
		return this.ruleActions.remove(action);
	}
 	
	@Override
	public RuleAction findActionByAll(String action, String expectation) {
		// TODO Auto-generated method stub
		RuleAction control = new RuleAction(action, expectation); 		 
		Iterator<RuleAction> ruleAction = this.ruleActions.iterator();
		
		while(ruleAction.hasNext()){
			RuleAction ruleAct = ruleAction.next(); 			
			if(ruleAct.equals(control)){
				return ruleAct;
			}
		}
		return null;
	}

	@Override
	public Collection<RuleAction> getActions() {
		// TODO Auto-generated method stub
		return this.ruleActions;
	}
 	
	//--------------------------------------------------------------------------------------------
	//getters and setters
	
	public Long getMessageManagerId() {
		return this.messageManagerId; 
	}
	
	public void setMessageManagerId(long messageManagerId) {
		this.messageManagerId = messageManagerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RuleType getType() {
		return type;
	}

	public void setType(RuleType type) {
		this.type = type;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public RuleGroup getRuleGroup() {
		return ruleGroup;
	}
	
	public void setRuleGroup(RuleGroup ruleGroup) {
		this.ruleGroup = ruleGroup;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	
	public Long getCompanyId() {
		return companyId;
	}
	

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
 
}
