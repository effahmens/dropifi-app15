/**
 * @class:       WidgetControl 
 * @author:      phillips effah mensah
 * @description: WidgetControl holds data about a unique field like email, name,
 *               subject, or message on widget. Each control has a unique title which identifies it                
 * 
 * @date 14/3/2012
 * 
 **/

package models;

import play.data.validation.*;
import play.db.jpa.Model;
import resources.WidgetControlType;
import resources.WidgetControlValidator;
import resources.WidgetField;
import view_serializers.ConSerializer;

import javax.persistence.*;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.*;

/**
 * WidgetControl holds data about a unique field like email, name,
 * subject, or message on a widget. Each field has a unique title which identifies it  * 
 **/
@Embeddable
public class WidgetControl implements Serializable{ 
	
	//-------------------------------------------------------------------------------------------------------- 
	//instance variables
	 
	private WidgetField condId; 
	@SerializedName("title")
	private String title;
	@SerializedName("type")
	private WidgetControlType type;
	@SerializedName("validator")
	private WidgetControlValidator validator;
	@SerializedName("required")
	private Boolean required;
	@SerializedName("position")
	private Integer position;
	@SerializedName("activated")
	private Boolean activated;
	
	@SerializedName("errorMsg")
	@Column(length=10000)
	private String errorMsg;
	
	//--------------------------------------------------------------------------------------------------------
	//constructor
	
	public WidgetControl(WidgetField condId, String title, WidgetControlType type, WidgetControlValidator validator,
			 Integer position, Boolean required,Boolean activated){
		
		this.setCondId(condId);
		this.setTitle(title);
		this.setType(type);
		this.setValidator(validator);		 
		this.setPosition(position);
		this.setRequired(required);
		this.setActivated(activated);
	}
	
	public WidgetControl(WidgetField condId, String title, WidgetControlType type, WidgetControlValidator validator,
			 Integer position, Boolean required,Boolean activated,String errorMsg){
		
		this.setCondId(condId);
		this.setTitle(title);
		this.setType(type);
		this.setValidator(validator);		 
		this.setPosition(position);
		this.setRequired(required);
		this.setActivated(activated);
		this.setErrorMsg(errorMsg);
	}
			
	@Override
	public String toString() {
		return "WidgetControl [condId=" + condId + ", title=" + title
				+ ", type=" + type + ", validator=" + validator + ", required="
				+ required + ", position=" + position + ", activated="
				+ activated + ", errorMsg=" + errorMsg + "]";
	}
 
	public static JsonArray toJsonArray(List<WidgetControl> controls){
		JsonArray arrayJson = new JsonArray();
		if(controls != null){
			Gson gson = new Gson();
			ListIterator<WidgetControl> qrlist = controls.listIterator();
			 
			while(qrlist.hasNext()){
				WidgetControl control =qrlist.next();
				ConSerializer conser = new ConSerializer(control); 
				arrayJson.add(gson.toJsonTree(conser)); 
			}
		} 		
		return arrayJson;
	}
		
	//--------------------------------------------------------------------------------------------------------
	//getters and setters of instance variables

 	public String getTitle() {
		return title;
	}
 	public String getEscapeTitle() {
		return title==null?this.getType().name():title.replace("\"", "'");
	}

	public void setTitle(String title) {
		this.title = (title ==null || title.trim().isEmpty()) ?this.getCondId().toString():title;
	}

	public WidgetControlType getType() {
		return type;
	}

	public void setType(WidgetControlType type) {
		this.type = type;
	}

	public WidgetControlValidator getValidator() {
		return validator;
	}

	public void setValidator(WidgetControlValidator validator) {
		this.validator = validator;
	}
 
	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}
	
	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}
	
	public Boolean getActivated() {
		return activated;
	}
	
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}
	
	public WidgetField getCondId() {
		return condId;
	}
	
	public void setCondId(WidgetField condId) {
		this.condId = condId;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	
}
