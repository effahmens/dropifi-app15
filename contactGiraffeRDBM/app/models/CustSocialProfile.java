

package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import play.data.validation.*;
import play.db.jpa.Model; 
import resources.SQLInjector;

import javax.persistence.*;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class CustSocialProfile  extends Model {
	
	@ManyToOne	 
	private CustomerProfile customerProfile;
	@Index(name="email")
	private String email;
	@Transient
	private Long profileId; 	
	@Index(name="type")@Column(length=50, updatable=false)	 
	private String type; 
	@Index(name="url")
	private String url;
	@Column(length=1000)
	private String socialId;
	@DateTimeFormat
	private Date birthday;
	@Column(length=100)
	private String username;
	private Integer connections;
	@Column(length=50)
	private String currentStatus;
	@Column(length=1000)
	private String bio;
	private Integer following;	
	private Integer followers;
		
	//----------------------------------------------------------------------------------------
	//constructor
	
	public CustSocialProfile(String email,Long profileId,String type, String url, String socialId,String username, Integer connections,
			String currentStatus, String bio, Integer following,Integer followers, Date birthday) {		
		
		this.setEmail(email);
		this.profileId = profileId;
		this.type = type;
		this.url = url;
		this.socialId = socialId;
		this.username = username;
		this.connections = connections;
		this.currentStatus = currentStatus;
		this.bio = bio;
		this.following = following;
		this.followers = followers;
		this.birthday = birthday;
	}
	
	public CustSocialProfile(){
		
	}
	
	//-------------------------------------------------------------------------------------------------------------

	public int insertSocialProfile(Connection conn) throws SQLException{
		int status = 501;
		
		if(conn !=null && !conn.isClosed()){
			
			//create an insert statement
			PreparedStatement insertSocial = conn.prepareStatement("INSERT INTO CustSocialProfile " +
					" (email,CustomerProfile_id,type,socialId,username,bio,url,currentstatus,connections,following,followers,birthday) " +
					" VALUE(?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			//add the parameter values to the statement
			int i =1;
			
			insertSocial.setString(i++, this.getEmail());
			insertSocial.setLong(i++, this.getProfileId());				 
			insertSocial.setString(i++, this.getType());
			insertSocial.setString(i++, this.getSoicalId());
			insertSocial.setString(i++, this.getUsername());
			insertSocial.setString(i++, this.getBio());
			insertSocial.setString(i++, this.getUrl());
			insertSocial.setString(i++, this.getCurrentStatus());			 
			insertSocial.setInt(i++, this.getConnections());
			insertSocial.setInt(i++, this.getFollowing());
			insertSocial.setInt(i++, this.getFollowers());
			insertSocial.setDate(i++, (java.sql.Date) this.getBirthday()); 
			
			//submit query to the database
			status = insertSocial.executeUpdate()>0?200:201;
			
		}
		return status;
		
	}
	
	public int updateSocialProfile(Connection conn,String url, String socialId,String username, Integer connections,
			String currentStatus, String bio, Integer following,Integer followers,Date birthDate) throws SQLException{
		int status =501;
		if(conn !=null && !conn.isClosed()){
			
			//create an update statement
			PreparedStatement updateSocial = conn.prepareStatement("UPDATE CustSocialProfile " +
					"SET socialId=?,username=?,bio=?,url=?,currentstatus=?, connections=?,following=?,followers=?,birthDay=? " +
					"WHERE customerProfile_id=? AND type=?"); 		
			
			//add the parameter values to the statement
			int i =1;
			
			updateSocial.setString(i++, (socialId==null? this.getSoicalId():socialId));
			updateSocial.setString(i++, (username==null? this.getUsername():username));
			updateSocial.setString(i++, bio);
			updateSocial.setString(i++, url);
			updateSocial.setString(i++, currentStatus);			 
			updateSocial.setInt(i++, connections);
			updateSocial.setInt(i++, following);
			updateSocial.setInt(i++, followers);
			updateSocial.setDate(i++, (java.sql.Date) birthDate);
			
			//where parameters
			updateSocial.setLong(i++, this.getProfileId());
			updateSocial.setString(i++, this.getType());
			
			//execute the update query			
			status = updateSocial.executeUpdate()>0?200:201;
		} 
		
		return status;
	}
	
	public static CustSocialProfile findByCusProfileId(Connection conn,Long cusProfileId,String type) throws SQLException{
		CustSocialProfile social =null;
		if(conn !=null && !conn.isClosed() && cusProfileId !=null){
			
			//create select statement
			PreparedStatement selectSocial = conn.prepareStatement("SELECT * FROM CustSocialProfile " +
					"WHERE CustomerProfile_id=? AND type=?");
			
			//add the parameter values
			int i =1;
			selectSocial.setLong(i++, cusProfileId);
			selectSocial.setString(i++, type);
			selectSocial.setMaxRows(1);
			//submit the query to the database
			ResultSet result = selectSocial.executeQuery();

			while(result.next()) { 
				social = new CustSocialProfile();
				
				social.id = result.getLong("id");
				social.setProfileId(result.getLong("CustomerProfile_id"));
				social.setUrl(result.getString("url"));
				social.setBio(result.getString("bio"));
				social.setBirthday(result.getDate("birthday"));
				social.setSocialId(result.getString("socialId"));
				social.setUsername(result.getString("username"));
				social.setCurrentStatus(result.getString("currentStatus"));
				social.setFollowers(result.getInt("followers"));
				social.setFollowing(result.getInt("following"));
				social.setConnections(result.getInt("connections")); 
				break;
			}	
			
		}		
		
		return social;
	}
	
	public static List<CustSocialProfile> findByCusProfileId(Connection conn,Long cusProfileId) throws SQLException{
		List<CustSocialProfile> socials = new LinkedList<CustSocialProfile>();
		if(conn !=null && !conn.isClosed() && cusProfileId !=null){
			
			//create select statement
			PreparedStatement selectSocial = conn.prepareStatement("SELECT * FROM CustSocialProfile WHERE CustomerProfile_id=?");
			
			//add the parameter values
			int i =1;
			selectSocial.setLong(i++, cusProfileId);		 
			
			//submit the query to the database
			ResultSet result = selectSocial.executeQuery();
			socials = new LinkedList<CustSocialProfile>();
			while(result.next()) { 
				CustSocialProfile social = new CustSocialProfile();
				
				social.id = result.getLong("id");
				social.setProfileId(result.getLong("CustomerProfile_id"));
				social.setUrl(result.getString("url"));
				social.setBio(result.getString("bio"));
				social.setBirthday(result.getDate("birthday"));
				social.setSocialId(result.getString("socialId"));
				social.setUsername(result.getString("username"));
				social.setCurrentStatus(result.getString("currentStatus"));
				social.setFollowers(result.getInt("followers"));
				social.setFollowing(result.getInt("following"));
				social.setConnections(result.getInt("connections")); 
				socials.add(social);
			}	
			
		}		
		
		return socials;
	}
	
	public static List<CustSocialProfile> findByCusProfileId(Connection conn,String email) throws SQLException{
		List<CustSocialProfile> socials = new LinkedList<CustSocialProfile>();
		if(conn !=null && !conn.isClosed() && email !=null){
			
			//create select statement
			PreparedStatement selectSocial = conn.prepareStatement("SELECT * FROM CustSocialProfile WHERE email=?");
			
			//add the parameter values
			int i =1;
			selectSocial.setString(i++, email);		 
			
			//submit the query to the database
			ResultSet result = selectSocial.executeQuery();
			socials = new LinkedList<CustSocialProfile>();
			while(result.next()) { 
				CustSocialProfile social = new CustSocialProfile(); 				
				social.id = result.getLong("id");
				social.setType(result.getString("type"));
				social.setProfileId(result.getLong("CustomerProfile_id"));
				social.setUrl(result.getString("url"));
				social.setBio(result.getString("bio"));
				social.setBirthday(result.getDate("birthday"));
				social.setSocialId(result.getString("socialId"));
				social.setUsername(result.getString("username"));
				social.setCurrentStatus(result.getString("currentStatus"));
				social.setFollowers(result.getInt("followers"));
				social.setFollowing(result.getInt("following"));
				social.setConnections(result.getInt("connections")); 
				socials.add(social);
			}	
			
		}		
		
		return socials;
	}

	//returns the number of social profiles a customer hav
	private static Long countProfile(Connection conn, String email){ 
		
		try{
		if(conn!= null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select count(c.socialId) as socials from CustSocialProfile c where c.email =?");
			query.setString(1, email);
			
			ResultSet result = query.executeQuery(); 
			while(result.next()){
				return result.getLong("socials"); 
			}
			return 0l;
		} 
		}catch(Exception e){
			play.Logger.info(e.getMessage(), e);
		}
		return -1l;
	}
	
	public static boolean hasSocials(Connection conn, String email){
		return countProfile(conn,email) > 0 ? true : false; 
	}
	
	//----------------------------------------------------------------------------------------
	//getters and setters
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = SQLInjector.escape(type);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = SQLInjector.escape(url);
	}

	public String getSoicalId() {
		return socialId;
	}

	public void setSocialId(String profileId) {
		this.socialId = SQLInjector.escape(profileId);
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
 
	public CustomerProfile getCustomerProfile() {
		return customerProfile;
	}

	public void setCustomerProfile(CustomerProfile customerProfile) {
		this.customerProfile = customerProfile;
	}
	
	public void setConnections(Integer connections) {
		this.connections = connections;
	}

	public Integer getConnections() {
		return connections;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setBio(String bio) {
		this.bio = SQLInjector.escape(bio);
	}

	public String getBio() {
		return bio;
	}
	
	public Integer getFollowing() {
		return following;
	}

	public void setFollowing(Integer following) {
		this.following = following;
	}

	public Integer getFollowers() {
		return followers;
	}

	public void setFollowers(Integer followers) {
		this.followers = followers;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = SQLInjector.escape(email);
	}
	
}
