package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Query;

import play.db.jpa.Model;
import resources.DCON;

@Entity
public class MsgCounter extends Model{

	@Column(nullable=false)
	private Long companyId;
	@Column(nullable=false)
	private Long sourceId;
	@Column(nullable=false)
	private String sourceOfMsg;
	@Column(nullable=false)
	private long inbox;
	@Column(nullable=false)
	private long sent;
	@Column(nullable=false)
	private long draft;
	@Column(nullable=false)
	private long newMsg;
	@Column(nullable=false)
	private long openMsg;
	@Column(nullable=false)
	private long pendingMsg;
	@Column(nullable=false)
	private long resolvedMsg;
	@Column(nullable=false)
	private long positive;
	@Column(nullable=false)
	private long neutral;
	@Column(nullable=false)
	private long negative;
	@Column(nullable=false)
	private long nosentiment;
	
	public MsgCounter(Long companyId, Long sourceId, String sourceOfMsg, long inbox, long sent,
			long draft, long newMsg, long openMsg, long pendingMsg,
			long resolvedMsg, long positive, long neutral, long negative, long nosentiment) {
		
		this.setSourceId(sourceId);
		this.setCompanyId(companyId);
		this.setSourceOfMsg(sourceOfMsg);
		this.setInbox(inbox);
		this.setSent(sent);
		this.setDraft(draft);
		this.setNewMsg(newMsg);
		this.setOpenMsg(openMsg);
		this.setPendingMsg(pendingMsg);
		this.setResolvedMsg(resolvedMsg);
		this.setPositive(positive);
		this.setNeutral(neutral);
		this.setNegative(negative);
		this.setNoSentiment(nosentiment);
	}
	
	public MsgCounter(Long companyId, Long sourceId, String sourceOfMsg){
		this(companyId,sourceId,sourceOfMsg,0,0,0,0,0,0,0,0,0,0,0);
	}

	public int createCounter(){
		MsgCounter msg = findMsgCounter(this.getCompanyId(), this.getSourceId(), this.getSourceOfMsg());
		
		if(msg != null){
			return 302;
		}else {
			msg = new MsgCounter(this.getCompanyId(), this.getSourceId(), this.getSourceOfMsg());
			return msg.validateAndSave()?200:201;
		}		 
	}
	
	public static int incNewCounter(Connection conn,Long companyId, Long sourceId, String sourceOfMsg,String sentiment) throws SQLException{
		if(companyId!=null && sourceId !=null && sourceOfMsg !=null){
						
			if(conn!=null && !conn.isClosed()){
				String senti =sentimentField(sentiment);
				 
				PreparedStatement query = conn.prepareStatement("Update MsgCounter Set inbox = (inbox+1), newMsg=(newMsg+1), "+
				senti+"=("+senti+"+1) Where companyId =? And sourceId =? And sourceOfMsg = ?"); 
				
				int i =1;
				query.setLong(i++, companyId);
				query.setLong(i++, sourceId); 
				query.setString(i++, sourceOfMsg);
				
				int status = query.executeUpdate()>0?200:201;	
 
				return status;
			}
		}
		return 501;
	}
	
	public static int incNewCounter(Long companyId, Long sourceId, String sourceOfMsg,String sentiment, boolean isWidget){
		if(companyId!=null && sourceId !=null && sourceOfMsg !=null){
			String senti =sentimentField(sentiment);
			Query q = MsgCounter.em().createQuery(" Update MsgCounter c Set c.inbox = (c.inbox + 1) , c.newMsg=(c.newMsg+1), " +
					senti+"=("+senti+"+1) Where (c.companyId = :companyId) And (c.sourceId = :sourceId) And (c.sourceOfMsg = :sourceOfMsg) ");
		
			q.setParameter("companyId", companyId).setParameter("sourceId",sourceId).setParameter("sourceOfMsg", sourceOfMsg);
			return q.executeUpdate()>-1?200:201;
		}
		return 501;
	}
	
	public static String sentimentField(String sentiment){
		String senti ="nosentiment";
		if(sentiment !=null && !senti.trim().isEmpty()){
			senti = sentiment.toLowerCase();
		}
		return senti;
	}
	
	public static int incOpenCounter(Long companyId,String sourceOfMsg,String sentiment){
		if(companyId!=null && sourceOfMsg !=null){
			String senti = sentimentField(sentiment);
			Query q = MsgCounter.em().createQuery(" Update MsgCounter c Set c.newMsg = (c.newMsg - 1) , c.inbox =(c.inbox-1), c.openMsg =(c.openMsg+1), " +
					senti+"=("+senti+"-1) Where c.companyId =:companyId And c.sourceOfMsg = :sourceOfMsg");
		
			q.setParameter("companyId", companyId).setParameter("sourceOfMsg", sourceOfMsg);
			return q.executeUpdate()>0?200:201;
		}
		return 501;
	}
	
	public static int incPendingCounter(Long companyId,String sourceOfMsg){
		if(companyId!=null && sourceOfMsg !=null){			 
			Query q = MsgCounter.em().createQuery(" Update MsgCounter c Set c.openMsg = (c.openMsg - 1) , c.pendingMsg = (c.pendingMsg+1) " +
					" Where c.companyId = :companyId And c.sourceOfMsg = :sourceOfMsg ");
		
			q.setParameter("companyId", companyId).setParameter("sourceOfMsg", sourceOfMsg);
			return q.executeUpdate()>0?200:201; 
		}
		return 501;
	}
	
	public static int incResolvedCounter(Long companyId, String sourceOfMsg,boolean isOpen){
		if(companyId!=null && sourceOfMsg !=null){
			Query q;
			if(isOpen){
				q = MsgCounter.em().createQuery(" Update MsgCounter c Set c.openMsg = (c.openMsg - 1) , c.resolvedMsg = (c.resolvedMsg+1) " +
					" Where c.companyId =:companyId And c.sourceOfMsg = :sourceOfMsg");
			}else{
				q = MsgCounter.em().createQuery(" Update MsgCounter c Set c.pendingMsg = (c.pendingMsg - 1) , c.resolvedMsg = (c.resolvedMsg+1) " +
						" Where (c.companyId = :companyId) And c.sourceOfMsg = :sourceOfMsg ");
			}
			
			q.setParameter("companyId", companyId).setParameter("sourceOfMsg", sourceOfMsg);
			return q.executeUpdate()>0?200:201;
		}
		return 501;
	}
	
	public static MsgCounter findMsgCounter(Long companyId,Long sourceId,String sourceOfMsg){		
		return MsgCounter.find("byCompanyIdAndSourceIdAndSourceOfMsg", companyId, sourceId, sourceOfMsg).first(); 
	}
	
	public static MsgCounter findMsgCounter(Long companyId,String sourceOfMsg){		
		return MsgCounter.find("byCompanyIdAndSourceOfMsg", companyId,sourceOfMsg).first(); 
	}
	
	//--------------------------------------------------------------------------------------------------------
	//getters and setters
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getSourceOfMsg() {
		return sourceOfMsg;
	}

	public void setSourceOfMsg(String sourceOfMsg) {
		this.sourceOfMsg = sourceOfMsg;
	}

	public long getInbox() {
		return inbox;
	}

	public void setInbox(long inbox) {
		this.inbox = inbox;
	}

	public long getSent() {
		return sent;
	}

	public void setSent(long sent) {
		this.sent = sent;
	}

	public long getDraft() {
		return draft;
	}

	public void setDraft(long draft) {
		this.draft = draft;
	}

	public long getNewMsg() {
		return newMsg;
	}

	public void setNewMsg(long newMsg) {
		this.newMsg = newMsg;
	}

	public long getOpenMsg() {
		return openMsg;
	}

	public void setOpenMsg(long openMsg) {
		this.openMsg = openMsg;
	}

	public long getPendingMsg() {
		return pendingMsg;
	}

	public void setPendingMsg(long pendingMsg) {
		this.pendingMsg = pendingMsg;
	}

	public long getResolvedMsg() {
		return resolvedMsg;
	}

	public void setResolvedMsg(long resolvedMsg) {
		this.resolvedMsg = resolvedMsg;
	}

	public long getPositive() {
		return positive;
	}

	public void setPositive(long positive) {
		this.positive = positive;
	}

	public long getNeutral() {
		return neutral;
	}

	public void setNeutral(long neutral) {
		this.neutral = neutral;
	}

	public long getNegative() {
		return negative;
	}

	public void setNegative(long negative) {
		this.negative = negative;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	
	public long getNoSentiment() {
		return nosentiment;
	}
	

	public void setNoSentiment(long nosentiment) {
		this.nosentiment = nosentiment;
	}
	
	
	
	
}
