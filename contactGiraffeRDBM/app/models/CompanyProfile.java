package models;
import java.io.Serializable;

import play.data.validation.*;
import play.db.jpa.Model;
import javax.persistence.*;

import com.google.gson.annotations.SerializedName;

@Embeddable
public class CompanyProfile implements Serializable {
	
	@Required 
	@SerializedName("name")
	public String name;
	@SerializedName("url") @Column(length=1000)
	public String url;
	@SerializedName("timezone")
	public String timezone;
	@Required
	@SerializedName("industry")
	public String industry;
	@Required
	@SerializedName("country")
	public String country; 
	
	@SerializedName("phone")
	public String phone;
	
	public CompanyProfile(String name, String url, String timezone,
			String industry, String country) {
		 
		this(name,url,timezone,industry,country,null);
	}
	
	public CompanyProfile(String name, String url, String timezone,
			String industry, String country,String phone) {		 
		this.name = name; 
		this.url = url;
		this.timezone = timezone;
		this.industry = industry;
		this.country = country;
		this.phone =phone;
	}

	public CompanyProfile(){}

	@Override
	public String toString() {
		return "CompanyProfile [name=" + name + ", url=" + url + ", timezone="
				+ timezone + ", industry=" + industry + ", country=" + country
				+ ", phone=" + phone + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	} 
	
	
	
}
