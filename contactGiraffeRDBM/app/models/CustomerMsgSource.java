package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;

import play.db.jpa.Model;

@Entity
public class CustomerMsgSource extends Model{

	@Column(nullable =false) @Index(name="companyId")
	private long companyId;
	@Column(nullable =false) @Index(name="email")
	private String email;
	@Column(nullable =false) @Index(name="sourceOfMsg")
	private String sourceOfMsg;
	@Column(nullable =false)
	private long numOfMsg;
	@Column(nullable =false)
	private Timestamp created;
	@Column(nullable = false)
	private Timestamp updated;
	
	public CustomerMsgSource(long companyId, String email, String sourceOfMsg,
			long numOfMsg, Timestamp created, Timestamp updated) {
		 
		this.setCompanyId(companyId);
		this.setEmail(email);
		this.setSourceOfMsg(sourceOfMsg);
		this.setNumOfMsg(numOfMsg);
		this.setCreated(created);
		this.setUpdated(updated);
	}
	
	public CustomerMsgSource(long companyId, String email, String sourceOfMsg,long numOfMsg) {
		 
		this.setCompanyId(companyId);
		this.setEmail(email);
		this.setSourceOfMsg(sourceOfMsg);
		this.setNumOfMsg(numOfMsg);		 
	}
	
	//------------------------------------------------------------------------------
	//
	public static int saveSource(Connection conn,long companyId, String email, String source, long numOfMsg) throws SQLException{ 
		if(hasSource(conn, companyId, email, source)==false){
			return insertSource(conn, companyId, email, source, numOfMsg);  
		}
		return updateSource(conn, companyId, email, source);
	}
	
	public static CustomerMsgSource findByEmail(Connection conn,long companyId, String email, String source) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select * from CustomerMsgSource where companyId=? and email =? and sourceOfMsg =?");
			int i=1;
			query.setLong(i++, companyId);
			query.setString(i++, email);
			query.setString(i++, source);
			query.setMaxRows(1);
			ResultSet result = query.executeQuery();
			
			while(result.next()){
				CustomerMsgSource custSource = new CustomerMsgSource(
						result.getLong("companyId"),
						result.getString("email"), 
						result.getString("sourceOfMsg"),  
						result.getLong("numOfMsg"), 
						result.getTimestamp("created"), 
						result.getTimestamp("updated")
				);
				custSource.id = result.getLong("id");
				return custSource;
			}
		}
		return null;
	}
	
	public static boolean hasSource(Connection conn,long companyId, String email, String source) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){
			PreparedStatement query = conn.prepareStatement("select email from CustomerMsgSource where companyId=? and email =? and sourceOfMsg =?");
			int i=1;
			query.setLong(i++, companyId);
			query.setString(i++, email);
			query.setString(i++, source);
			query.setMaxRows(1);
			ResultSet result = query.executeQuery();
			
			while(result.next()){
				 String found = result.getString("email");
				 return found.equalsIgnoreCase(email);
			}
		}
		return false;
	}
	
	public static int insertSource(Connection conn,Long companyId, String email, String source, long numOfMsg) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){
			if(companyId !=null && email!=null && source!=null){
				PreparedStatement query = conn.prepareStatement("insert into CustomerMsgSource (companyId, email, sourceOfMsg, numOfMsg,created,updated) " +
						"value(?,?,?,?,?,?)");
				int i=1;
				query.setLong(i++, companyId);
				query.setString(i++, email);
				query.setString(i++, source);
				query.setLong(i++, numOfMsg);
				Timestamp time = new Timestamp(new Date().getTime());
				query.setTimestamp(i++, time);
				query.setTimestamp(i++, time);
				
				return query.executeUpdate()>0?200:201;
			}
		}
		return 501;
	}
	
	public static int updateSource(Connection conn,Long companyId, String email, String source) throws SQLException{
		
		if(conn!=null && !conn.isClosed()){
			if(companyId !=null && email!=null && source!=null){
				PreparedStatement query = conn.prepareStatement("update CustomerMsgSource set numOfMsg = (numOfMsg+1) , updated = ?" +
						" where companyId=? and email =?  and sourceOfMsg =?");
						 
				int i=1;			 
				Timestamp time = new Timestamp(new Date().getTime());
				query.setTimestamp(i++, time);		
				query.setLong(i++, companyId);
				query.setString(i++, email);
				query.setString(i++, source);
					
				return query.executeUpdate()>0?200:201;		
			}
		}
		return 501;
	}
	
	
	//------------------------------------------------------------------------------

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSourceOfMsg() {
		return sourceOfMsg;
	}

	public void setSourceOfMsg(String sourceOfMsg) {
		this.sourceOfMsg = sourceOfMsg;
	}

	public long getNumOfMsg() {
		return numOfMsg;
	}

	public void setNumOfMsg(long numOfMsg) {
		this.numOfMsg = numOfMsg;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	
	
	
	
}
