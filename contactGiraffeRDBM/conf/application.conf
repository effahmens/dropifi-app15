# This is th main configuration file for the application.
# ~~~~~
application.name = Dropifi RDBMAddon

#Application mode
# ~~~~~
# Set to dev to enable instant reloading and other development help.
# Otherwise set to prod.
#application.mode=dev
application.mode=prod

# Secret key 
# ~~~~~
# The secret key is used to secure cryptographics functions
# If you deploy your application to several instances be sure to use the same key !
application.secret=RYqcjScxx6QiUvyp3AHUnyMfU38tC9YIGTkPyrLQ8jKkjcmeqItf9kIMxwdIS4Kg


# i18n
# ~~~~~
# Define locales used by your application.
# You can then place localized messages in conf/messages.{locale} files
# application.langs=fr,en,ja

# Date format
# ~~~~~
date.format=yyyy-MM-dd
# date.format.fr=dd/MM/yyyy

# Server configuration
# ~~~~~
# If you need to change the HTTP port, uncomment this (default is set to 9000)
http.port=80 
https.port=443

#Custom USER_AGENT header value for web services requests.
http.userAgent=Dropifi 4.0

#Sets the proxy request to SSL, overriding the X-Forwarded-Proto and X-Forwarded-SSL HTTP header values � the protocol originally requested by the client. 
XForwardedProto=https
#
# By default the server listen for HTTP on the wilcard address.
# You can restrict this.
# http.address=127.0.0.1
#
# Use this if you don't host your Play application at the root of the domain
# you're serving it from. This parameter has no effect when deployed as a
# war, because the path will be handled by the application server.
# http.path=/

# Session configuration
# ~~~~~~~~~~~~~~~~~~~~~~
# By default, session will be written to the transient PLAY_SESSION cookie.
# The cookies are not secured by default, only set it to true
# if you're serving your pages through https.
application.session.cookie=PLAY
# application.session.maxAge=1h
# application.session.secure=true

# Session/Cookie sharing between subdomain
# ~~~~~~~~~~~~~~~~~~~~~~
# By default a cookie is only valid for a specific domain. By setting
# application.defaultCookieDomain to '.example.com', the cookies
# will be valid for all domains ending with '.example.com', ie:
# foo.example.com and bar.example.com
# application.defaultCookieDomain=.example.com

# JVM configuration
# ~~~~~
# Define which port is used by JPDA when application is in debug mode (default is set to 8000)
# jpda.port=8000
#
# Java source level => 1.5, 1.6 or 1.7 (experimental)
#java.source=1.7
 
#PROD
#jvm.memory = -XX:PermSize=2024m -XX:MaxPermSize=10024 -Xms10024m -Xmx20024m -XX:+UseParallelOldGC 
jvm.memory = -XX:PermSize=2024m -XX:MaxPermSize=7024 -Xms7024m -Xmx14024m 
#DEV
#jvm.memory = -XX:PermSize=5024m -XX:MaxPermSize=10024 -Xms5024m -Xmx5024m


# Log level
# ~~~~~
# Specify log level for your application.
# If you want a very customized log, create a log4j.properties file in the conf directory
# application.log=INFO
#
# More logging configuration
%test.application.log=INFO
%test.application.log.path=/log4j.xml

application.log.path=/log4j.properties
application.log.system.out=off

# Database configuration
# ~~~~~ 
# Enable a database engine if needed.
#
# To quickly set up a development database, use either:
#   - mem : for a transient in memory database (H2 in memory)
#   - fs  : for a simple file written database (H2 file stored)
# db=mem
#
# To connect to a local MySQL5 database, use:
# db=mysql://user:pwd@host/database
#
#Rackspace - Production
db.url = {DBHOST}
db.driver=com.mysql.jdbc.Driver
db.user={DBUSER}
db.pass={DBPASSWORD}

prod.jpa.ddl=update

# To connect to a local PostgreSQL9 database, use:
# db=postgres://user:pwd@host/database
#
# If you need a full JDBC configuration use the following :
# db.url=jdbc:postgresql:database_name
# db.driver=org.postgresql.Driver
# db.user=root
# db.pass=secret
#
# Connections pool configuration :
db.pool.timeout=10000
db.pool.maxSize=100
db.pool.minSize=10
#
# If you want to reuse an existing Datasource from your application server, use:
# db=java:/comp/env/jdbc/myDatasource
#
# When using an existing Datasource, it's sometimes needed to destroy it when
# the application is stopped. Depending on the datasource, you can define a
# generic "destroy" method :
# db.destroyMethod=close

# JPA Configuration (Hibernate)
# ~~~~~
#
# Specify the custom JPA dialect to use here (default to guess):
# jpa.dialect=org.hibernate.dialect.PostgreSQLDialect
#
# Specify the ddl generation pattern to use. Set to none to disable it 
# (default to update in DEV mode, and none in PROD mode):
jpa.ddl=none
#
# Debug SQL statements (logged using DEBUG level):
# jpa.debugSQL=true
#
# You can even specify additional hibernate properties here:
# hibernate.use_sql_comments=true
# ...
#
# Store path for Blob content
attachments.path=data/attachments

# Memcached configuration
# ~~~~~ 
# Enable memcached if needed. Otherwise a local cache is used.
memcached=enabled

#development
# Specify memcached host (default to 127.0.0.1:11211)
memcached.host=127.0.0.1:11211

#
# Or you can specify multiple host to build a distributed cache
#localhost - development
#memcached.1.host=127.0.0.1:11211
#memcached.2.host=127.0.0.1:11212
#

#production
#memcached.host=dropifimemcache.xgo9co.cfg.use1.cache.amazonaws.com:11211
            
#Redis memory
redis.dev.host=127.0.0.1
redis.prod.host=127.0.0.1
redis.port=6379
redis.timeout=0


# HTTP Response headers control for static files
# ~~~~~
# Set the default max-age, telling the user's browser how long it should cache the page.
# Default is 3600 (one hour). Set it to 0 to send no-cache.
# This is only read in prod mode, in dev mode the cache is disabled.
# http.cacheControl=3600

# If enabled, Play will generate entity tags automatically and send a 304 when needed.
# Default is true, set it to false to deactivate use of entity tags.
# http.useETag=true

#Disable the HTTP response header that identifies the HTTP server as Play. For example:
http.exposePlayServer=false

# Custom mime types
# mimetype.xpi=application/x-xpinstall

# WS configuration
# ~~~~~
# Default engine is Async Http Client, uncomment to use
# the JDK's internal implementation
# webservice = urlfetch
# If you need to set proxy params for WS requests
# http.proxyHost = localhost
# http.proxyPort = 3128
# http.proxyUser = jojo
# http.proxyPassword = jojo

# Mail configuration
# ~~~~~ 
# Default is to use a mock Mailer
mail.smtp=mock

# Or, specify mail host configuration
# mail.smtp.host=127.0.0.1
# mail.smtp.user=admin
# mail.smtp.pass=
# mail.smtp.channel=ssl

# Url-resolving in Jobs
# ~~~~~~
# When rendering templates with reverse-url-resoling (@@{..}) in Jobs (which do not have an inbound Http.Request),
# ie if sending a HtmlMail, Play need to know which url your users use when accessing your app.
# %test.application.baseUrl=http://162.209.125.20/
prod.application.baseUrl=https://www.dropifi.com/

# Jobs executor
# ~~~~~~
# Size of the Jobs pool
play.jobs.pool=10

# Execution pool
# ~~~~~
# Default to 1 thread in DEV mode or (nb processors + 1) threads in PROD mode.
# Try to keep a low as possible. 1 thread will serialize all requests (very useful for debugging purpose)
play.pool=2

# Open file from errors pages
# ~~~~~
# If your text editor supports opening files by URL, Play! will
# dynamically link error pages to files 
#
# Example, for textmate:
# play.editor=txmt://open?url=file://%s&line=%s

# Testing. Set up a custom configuration for test mode
# ~~~~~
#%test.module.cobertura=${play.path}/modules/cobertura
%test.application.mode=prod
%test.db.url=jdbc:h2:mem:play;MODE=MYSQL;LOCK_MODE=0
%test.jpa.ddl=update 
%test.mail.smtp=mock

#AKKA settings
akka.default-dispatcher.core-pool-size-max = 1024
akka.debug.receive = on

s3.bucket={S3BUCKET}
aws.access.key = {AWSACCESSKEY}
aws.secret.key = {AWSSECRETKEY}

aws.bucket.widgettab.prod={S3TABBUCKET}
aws.bucket.widgettab.dev={S3TABBUCKET}

rackspace.apikey={RACKSPACEAPIKEY}
rackspace.username=dropifi

#cargo.remote.username = 
#cargo.remote.password = 
#cargo.tomcat.manager.url=  

#bees.api.key={BEESAPIKEY}
#bees.api.secret={BEESAPIKEY}
#bees.api.name ={BEESAPINAME}
#bees.api.domain ={BEESDOMAIN}

play.netty.clientAuth=need

#SSL Certificate
certificate.key.file=conf/host.key
certificate.file=conf/host.crt

# In case your key file is password protected
#certificate.password=secret
trustmanager.algorithm=JKS

google.analytics.code={GOOGLEANALYTICS}
#play.bytecodeCache=false 

#Default mail settings
mail.smtp.host=smtp.mailgun.org
mail.smtp.port=465
mail.smtp.channel=ssl
mail.smtp.user={SMTPUSER}
mail.smtp.pass={SMTPPASS}
mail.from={MAILFROM}
mailgun.campaign.id={CAMPAIGNID}

#
dropifi.admin.username={ADMINUSER}
dropifi.admin.password={ADMINPASS}
invite.accesskey={ADMINACCESSKEY}
dropifi.inboxupdate ={INBOXUPDATEKEY}

#Server url
dropifi.dev.url=https://appengine.dropifi.com
dropifi.prod.url=https://www.dropifi.com

#
dropifi.mail.username=Philips Effah
dropifi.mail.email=philips@dropifi.com
#dropifi.mail.password 

dropifi.intromail.username={WELCOMENAME}
dropifi.intromail.email={WELCOMEEMAIL}

#
dropifi.noreply.username =No Reply - Dropifi 
dropifi.noreply.email =no-reply@dropifi.com 
#dropifi.noreply.password 

#
dropifi.team.username =Dropifi Team
dropifi.team.email =team@dropifi.com
#dropifi.team.password 

#Production Dropifi Contact Widget for Shopify
dropifi.shopify.apikey={SHOPIFYAPIKEY}
dropifi.shopify.secretkey={SHOPIFYSECRETKEY}

#Development Test - Dropifi Contact Widget for Shopify
dropifi.shopifyDev.apikey={SHOPIFYAPIKEY}
dropifi.shopifyDev.secretkey={SHOPIFYSECRETKEY} 
ropifi.shopify.secretkey={SHOPIFYSECRETKEY} 

#Dropifi Mixpanel API
dropifi.mixpanel.DevAPI={MIXPANELKEY}
dropifi.mixpanel.ProdAPI={MIXPANELKEY}

#Stripe secret and publishable key
#PROD
stripe.secret.key={STRIPEKEY}
stripe.publishable.key={STRIPEPUBLISHEDKEY}
stripe.additionalId.key={STRIPEPREFIX}

#DEV
stripe.dev.secret.key={STRIPEKEY} 
stripe.dev.publishable.key={STRIPEPUBLISHEDKEY}

#Wix apikey and secret key
#prod mode
wix.apikey={WIXAPIKEY}
wix.secretkey={WIXSECRETKEY}
#dev mode
wix.dev.apikey={WIXAPIKEY}
wix.dev.secretkey={WIXSECRETKEY}
wix.vendorProductId=Dropifi-Premium

#MailChimp
#prod mode
mailchimp.prod.apikey={MAILCHIMPAPIKEY}
mailchimp.prod.listId={MAILCHIMPLISTID}
mailchimp.prod.newsletterListId={MAILCHIMPNEWSLETTERID} 

#Context IO
contextio.apikey={CONTEXTIO.APIKEY}
contextio.secret={CONTEXTIO.SECRET}

#Sentiment analyzes API keys..
#
#Lymbix
lymbix.apikey={LYMBIXAPIKEY}
lymbix.secretkey={LYMBIXSECRETKEY}

#Repustate
repustate.apikey={REPUSTATE}

#Alchemy
alchemy.apikey={ALCHEMYAPIKEY}

#Textalytics
textalytics.apikey={TEXTALYTICSAPIKEY}

#Fullcontact
fullcontact.apikey={FULLCONTACTAPIKEY}
fullcontact.secretkey={FULLCONTACTSECRETKEY}


#Newrelic
newrelic.apikey={NEWRELICAPIKEY}

#Tictail API
tictail.clientId={TICTAILCLIENTID}
tictail.clientSecret={TICTAILSECRETKEY} 

#Social Review
social.title=Social Review
#social.platformKey=

#Geo location
maxmind.userid={MAXMINDUSERID}
maxmind.license_key={MAXMINDLICENSEKEY}

#Press - js and css compressing module
#module.press=${play.path}/modules/press-1.0.36
#press.enabled=true
#press.cache=Change
#press.cache.clearEnabled=true
#press.inMemoryStorage=false
#press.htmlCompatible=true

#Green Script - for compressing  js and css files
#The GreenScript module
#module.greenscript=${play.path}/modules/greenscript-1.2.8b

# Default greenscript.dir.js point to /public/javascripts
greenscript.dir.js=/public/javascripts
#
# Default dir.css point to /public/stylesheets
greenscript.dir.css=/public/stylesheets
#
# Default minimized file folder point to /public/gs
greenscript.dir.minimized=/public/gs

# Enable/Disable minimize .By Default minimize is turned on in prod mode and turned off in dev mode
greenscript.minimize=false
#

# By default compress is turned on in prod mode and turned off in dev mode
greenscript.compress=false
#

# By default cache is turned on in prod mode and turned off in dev mode
greenscript.cache=false

# Enable/Disable in-memory cache This item is by default false
greenscript.cache.inmemory=false
#
# Enable/Disable LESS support is turned off
greenscript.less.enabled=false
#
# Enable/Disable inline script processing By default the configuration is turned off
greenscript.inline.process=false
#
# Set css file last-modified timestamp check frequence. default to 10 seconds
greenscript.css.cache.check=2s
#
# Set js file last-modified timestamp check frequence. default to 10 seconds
greenscript.js.cache.check=2s

#Email Check Time
emailCheckTime.gmail=5
emailCheckTime.hotmail=15
emailCheckTime.yahoo=5
emailCheckTime.yahoo_uk=5
emailCheckTime.yahoo_plus=5
emailCheckTime.msn=15
emailCheckTime.godaddy=5
emailCheckTime.aol=1
emailCheckTime.other=15
