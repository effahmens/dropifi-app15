
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import models.GeoLocation;
import models.InboundMailbox;

import org.joda.time.DateTime;
import org.json.JSONException;

import adminModels.DropifiMailer;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.apache.commons.codec.DecoderException;
import com.apache.commons.codec.binary.Hex;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sun.mail.smtp.SMTPTransport;

import play.libs.Codec;
import play.libs.Mail;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import resources.AWSS3;
import resources.DropifiTools;
import resources.EmailValidity;
import resources.addons.SocialReviewClient; 


public class DemoTest {
	 
	public static void main(String[] args) {  
		/*  
		System.out.println(GeoLocation.findByMaxmind(74469, "BpNM0uWjy3Fg","41.191.244.227")+" "); 
		
		 
		String url="https://tictail.com/dashboard/store/sulkylongearedowl";
		if(url.contains("store/"))
			System.out.println(url.split("store/")[1]);
		 
		try{
		Properties props = System.getProperties();
        props.put("mail.smtps.host","smtp.mailgun.org");
        props.put("mail.smtps.auth","true");
        Session session = Session.getInstance(props, null);
        
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("philips@dropifi.com"));
        msg.setRecipients(Message.RecipientType.TO,
        InternetAddress.parse("efpam2012@gmail.com", false));
        msg.setSubject("Hello");
        msg.setText("Testing some Mailgun awesomness");
         
			msg.setSentDate(new Date());
		
        SMTPTransport t =
            (SMTPTransport)session.getTransport("smtps");
        t.connect("smtp.mailgun.org", "postmaster@dropifi.com", "mangoboyers");
        t.sendMessage(msg, msg.getAllRecipients());
        System.out.println("Response: " + t.getLastServerResponse());
        t.close();
	} catch (MessagingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		//System.out.println(SocialReviewClient.getSocialUID("efpam2013@gmail.com").toString());
	 
  
		try {
			InboundMailbox mailbox = new InboundMailbox("Outlook", "Philips Effah", "nabong04@outlook.com",
					"", "pop3", "pop3.live.com", 995,"smtp","smtp.live.com", 587);
			
			 InboundMailbox mailbox = new InboundMailbox("Hotmail", "No Reply", "no-reply@dropifi.com",
					"mangoboyers", "imaps", "imap.mailgun.org", 993,"smtps", "smtp.mailgun.org", 465);
			//"smtp","smtp.live.com",587 "smtps", "smtp.mailgun.org", 465
			mailbox.setAuthName("postmaster@dropifi.com"); 
			//DropifiMailer.checkConnection(mailbox); 
		 
			DropifiMailer mail = new DropifiMailer(mailbox);
			System.out.println("Test: "+mail.checkConnection(mailbox)); 
			//mail.sendSimpleMail("philips@dropifi.com", "Philips", "Light at the end of the tunnel", "Just a test 4", null);
		} catch (NoSuchAlgorithmException e) { 
			// TODO Auto-generated catch block 
			e.printStackTrace(); 
		}catch(Exception e){ 
			e.printStackTrace();
		}
		
	 
		try {
			Process p =Runtime.getRuntime().exec("ls -l");
			p.waitFor();
			
			BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream())); 
			String line=reader.readLine(); 
			while(line!=null) 
			{ 
			System.out.println(line); 
			line=reader.readLine(); 
			} 

		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("hello world");
		DecimalFormat twoDForm = new DecimalFormat("####.####");
		double amount= Double.valueOf(twoDForm.format(1995));
		 
		System.out.println("Amount: "+amount);
		
		int x = 5950;
		DecimalFormat df = new DecimalFormat("#.00"); // Set your desired format here.
		System.out.println(df.format(x/100.0));
		long time =1367341912;
		
		DateTime date = new DateTime(time* 1000);
		System.out.println("Time: "+ date);
		
		 AWSS3.GetS3Client();
		textToImage("screnshot2"," Contact ");   
		String fonts[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		
		for(String font : fonts){
			System.out.println("<option value='"+font+"'>"+font+"</option>");
		}
		
		//SendMail email = new SendMail("info@hedysoft.com", "philips@dropifi.com", "Happy", "hear from you");
		//email.send();
		/* String strHTML= "<html>"+
                 "<head>"+
                 "<title>Convert HTML to Text String </title>"+
                 "</head>"+

                 "<body>"+
                 "This is HTML String of java's source code  \"my program\""+
                 "</body>"+
                 "</html>";
	*/
	/*	String strHTML ="kofi id"; 
     String stringWithoutHTML=removeHTML(strHTML);

     System.out.println(stringWithoutHTML);
	
		EmailValidity emaiV = new EmailValidity("phillips.mensah@meltwater.org"); 						
		int wstatus = emaiV.CheckDomain();  
		System.out.println("Valid "+ wstatus);
		
	 System.out.println(capitalize(" p"));
	 */ 
	
	  try {
		System.out.println(Hex.decodeHex("81dd234486ebc6cb5dfe9ba5ecb319c0".toCharArray()));
		byte[] messageDigest = Codec.decodeBASE64("81dd234486ebc6cb5dfe9ba5ecb319c0");
		
		StringBuilder hexString = new StringBuilder();
		for(int i=0;i<messageDigest.length;i++) {
		String hex = Integer.toHexString(0xFF & messageDigest[i]);
		if (hex.length() == 1) {
		    // could use a for loop, but we're only dealing with a single byte
		    hexString.append('0');
		}
		hexString.append(hex);
		}
		System.out.println("Test "+hexString.toString());
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	public static String capitalize(String value){
		if(value ==null) return value;
		
		value = value.trim();
		char c =value.charAt(0);
		if(!Character.isUpperCase(c)){ 
			value = Character.toUpperCase(c)+ value.substring(1);
		} 
		return value;
	}
	
	public static String removeHTML(String htmlString)
    {
          // Remove HTML tag from java String    
        String noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");

        // Remove Carriage return from java String
        noHTMLString = noHTMLString.replaceAll("\r", "<br/>");

        // Remove New line from java string and replace html break 
        noHTMLString = noHTMLString.replaceAll("\n", " ");
        noHTMLString = noHTMLString.replaceAll("\'", "&#39;");
        noHTMLString = noHTMLString.replaceAll("\"", "&quot;");
        return noHTMLString;
    }
	
	public static void textToTransparentImage(String infileName, String outfileName) { 
		try{
		  final File in = new File(infileName);
	      final BufferedImage source = ImageIO.read(in);  

	      final int color = source.getRGB(0, 0);

	      final Image imageWithTransparency = makeColorTransparent(source, new Color(color));

	      final BufferedImage transparentImage = imageToBufferedImage(imageWithTransparency);

	      final File out = new File("./test/images/" + outfileName + ".png");
	      ImageIO.write(transparentImage, "PNG", out);
	      System.out.println("successfully created");
	      
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void textToImage(String fileName,String text){
        
        //create String object to be converted to image
       String sampleText = text;
 
        //Image file name
       fileName = "./public/sample/" + fileName + ".png"; 
       
       //String fileNameTwo =fileName = "./public/images/sample/screenshot3.png";
        
        //create a File Object
        File newFile = new File(fileName);
        //File newFileTwo = new File(fileNameTwo);
         
        //create the font you wish to use
        Font font = new Font("Trebuchet MS", Font.ROMAN_BASELINE, 17);
        
        //create the FontRenderContext object which helps us to measure the text
        FontRenderContext frc = new FontRenderContext(null, true, true);
         
        //get the height and width of the text
        Rectangle2D bounds = font.getStringBounds(sampleText, frc); 
        int w = (int)bounds.getWidth() +5;
        int h = (int) bounds.getHeight()+5 ;
        
        //create a BufferedImage object 
        BufferedImage bi = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
         
        
       Graphics2D g = bi.createGraphics(); 
       //Graphics g =  bi.getGraphics();
        g.clearRect(0, 0, 0, 0);
        //set color and other parameters
       g.setBackground(new Color(0,0,0,0));
       
       //g.fillRect(0, 0, w, h); 
       g.setColor(Color.WHITE);
       g.setFont(font);      
        
       g.drawString(sampleText,(int)bounds.getX(),(int)-bounds.getY());      
       g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OUT, 0.0f));       
      
       //releasing resources
       g.dispose();  
      
        //creating the file 
       try {
		ImageIO.write(bi, "PNG", newFile); 
		
		bi = rotate90ToLeft(bi);
		
		ImageIO.write(bi, "PNG", newFile); 
		String bucketName ="dropifi_widgetImage"; 
		String folderName = "test";
		
		AWSS3.putPublicFile(bucketName, folderName, newFile,fileName);
		 
		System.out.println("successfully created");   
		//textToTransparentImage(fileName, "screenshot_out");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}   
       
 }
	
	public static BufferedImage rescale(BufferedImage bufferedImage, int angle, int w, int h) {

		AffineTransform   tx = new AffineTransform();
        tx.rotate(angle,w / 2, h / 2);//(radian,arbit_X,arbit_Y)

        AffineTransformOp   op = new AffineTransformOp(tx,AffineTransformOp.TYPE_BILINEAR);
        return op.filter(bufferedImage,null);//(sourse,destination)

	}
	
	public static BufferedImage rotate90ToLeft( BufferedImage inputImage ){
		//The most of code is same as before
		        int width = inputImage.getWidth();
		        int height = inputImage.getHeight();
		        BufferedImage returnImage = new BufferedImage( height, width , inputImage.getType()  );
		//We have to change the width and height because when you rotate the image by 90 degree, the
		//width is height and height is width :)

		        for( int x = 0; x < width; x++ ) {
		                for( int y = 0; y < height; y++ ) {
		                        returnImage.setRGB(y, width - x - 1, inputImage.getRGB( x, y  )  ); 
		//Again check the Picture for better understanding
		                }
		                }
		        return returnImage;

		}




	
	/**
	    * Make provided image transparent wherever color matches the provided color.
	    *
	    * @param im BufferedImage whose color will be made transparent.
	    * @param color Color in provided image which will be made transparent.
	    * @return Image with transparency applied.
	    */
	   public static Image makeColorTransparent(final BufferedImage im, final Color color)
	   {
	      final ImageFilter filter = new RGBImageFilter()
	      {
	         // the color we are looking for (white)... Alpha bits are set to opaque
	         public int markerRGB = color.getRGB() | 0xFFFFFFFF;

	         public final int filterRGB(final int x, final int y, final int rgb)
	         {
	            if ((rgb | 0xFF000000) == markerRGB)
	            {
	               // Mark the alpha bits as zero - transparent
	               return 0x00FFFFFF & rgb;
	            }
	            else
	            {
	               // nothing to do
	               return rgb;
	            }
	         }
	      };

	      final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
	      return Toolkit.getDefaultToolkit().createImage(ip);
	   }
	
	   /**
	    * Convert Image to BufferedImage.
	    *
	    * @param image Image to be converted to BufferedImage.
	    * @return BufferedImage corresponding to provided Image.
	    */
	   private static BufferedImage imageToBufferedImage(final Image image)
	   {
	      final BufferedImage bufferedImage =
	         new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
	      final Graphics2D g2 = bufferedImage.createGraphics();
	      g2.drawImage(image, 0, 0, null);
	      g2.dispose();
	      return bufferedImage;
	    }

	
	   private Image TransformGrayToTransparency(BufferedImage image)
	   {
	     ImageFilter filter = new RGBImageFilter()
	     {
	       public final int filterRGB(int x, int y, int rgb)
	       {
	         return (rgb << 8) & 0xFF000000; 
	       }
	     };

	     ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
	     return Toolkit.getDefaultToolkit().createImage(ip);
	   }
	   
	   public static void functionJava(){
		   final List<Integer> prices = Arrays.asList(10, 15, 20, 25, 30, 45, 50);
		   //final double totalOfDiscountedPrices = prices.stream().map((Integer price) -> price * 0.9).sum();
	   }
 
}
