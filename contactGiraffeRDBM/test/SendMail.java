
import java.util.Properties; 
 
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
public class SendMail {
 
    private String from;
    private String to;
    private String subject;
    private String text;
 
    public SendMail(String from, String to, String subject, String text){
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.text = text;
    }
 
    public void send(){
 
        Properties props = new Properties();
        props.put("mail.smtp.protocol", "smtp");
        props.put("mail.smtp.host", "smtpout.secureserver.net");
        props.put("mail.smtp.port", "80");
 
        Session mailSession = Session.getDefaultInstance(props, new Authenticator() {
        	protected PasswordAuthentication getPasswordAuthentication() {
			      return new PasswordAuthentication("info@hedysoft.com", "03qlSV45");
			}
		});
        Message simpleMessage = new MimeMessage(mailSession);
 
        InternetAddress fromAddress = null;
        InternetAddress toAddress = null;
        try {
            fromAddress = new InternetAddress(from);
            toAddress = new InternetAddress(to);
        } catch (AddressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
 
        try {
            simpleMessage.setFrom(fromAddress);
            simpleMessage.setRecipient(RecipientType.TO, toAddress);
            simpleMessage.setSubject(subject);
            simpleMessage.setText(text);
            
            InternetAddress [] add = {new InternetAddress("philips@dropifi.com")};
            //Transport.send(simpleMessage);
            Transport tt = mailSession.getTransport("smtp");
            tt.connect("smtpout.secureserver.net", "info@hedysoft.com", "03qlSV45");
            if(tt.isConnected()){
            	 tt.sendMessage(simpleMessage,add);
            }else{
            	System.out.println("failed sending....");
            }
           
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            System.out.println(e.getMessage());
            e.getStackTrace();
        }
    }
}