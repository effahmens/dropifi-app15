import java.util.Random;

import resources.DropifiTools;

import com.shopify.api.APIAuthorization;
import com.shopify.api.client.ShopifyClient;
import com.shopify.api.credentials.Credential;
import com.shopify.api.credentials.ShopifyCredentialsStore;
import com.shopify.api.endpoints.API;
import com.shopify.api.endpoints.AuthAPI;
import com.shopify.api.endpoints.BaseShopifyService;
import com.shopify.api.endpoints.EndpointImpl;
import com.shopify.api.endpoints.ProductsService;
import com.shopify.api.endpoints.ScriptTagsService;
import com.shopify.api.endpoints.ShopsService;
import com.shopify.api.resources.MGShop;
import com.shopify.api.resources.Shop;
 


public class PluginTest {

	public static void main(String[] args) {
		/*
			String myshop_apikey="ebe04ac365d89af767378fb865064cf5", myshop_secreKey="c0b721a1f94089c84ad76976795e2ae7", 
					myshop_domain="ebert-and-sons1712.myshopify.com", password="";
			
			ShopifyClient client = new ShopifyClient(new Credential(myshop_apikey, myshop_secreKey,  myshop_domain)); 
			
			ProductsService productAPI = client.constructService(ProductsService.class); 
			int count = productAPI.getCount();
	 
			System.out.println(count); 
		*/
		
		int valueOne =  generateRandomNumbers(2);
		int valueTwo =  generateRandomNumbers(2); 
		String operand ="";
		int result=0;
		int operandValue = (int)(Math.random()*2);
		if(operandValue==0) {
			result = valueOne + valueTwo; operand="+";
		}else {
			result = valueOne - valueTwo; operand="-";
		}
		
		System.out.println(valueOne + operand +valueTwo+" = "+result);
		System.out.println();
	}
	
	public static int generateRandomNumbers(int length) {
		String nums = "";

        Random randomGenerator = new Random(); 
        while(true) {
        	if(nums.length()>=length) 
        		break;
        	
            nums += randomGenerator.nextInt(10000); 
        }
        return Integer.valueOf(nums.substring(0, length));
	}
}
