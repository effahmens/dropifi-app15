package query;

import helper.DCON;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.persistence.Query;

import dsubscription.resources.com.AccountType;

import play.db.jpa.JPA;

public class SubFinder {
	public static List<Map<String,Object>> listOfUsers(){
		List<Map<String,Object>> users = new LinkedList<>();
		Connection conn = DCON.getDefaultConnection();
		try{
			if(conn!=null && !conn.isClosed()){	
				PreparedStatement query =conn.prepareStatement("Select c.apikey, u.email, u.username, c.accountType, c.created from Company c, AccountUser u Where (c.apikey=u.apikey) and (u.email not like ?)");
				query.setString(1, "delete_%");
				final ResultSet result = query.executeQuery();
				while(result.next()){
					Map<String,Object> user = new HashMap<String, Object>(){{
						put("uid",result.getString("apikey"));
						put("email",result.getString("email"));
						put("name",result.getString("username"));
						put("type",AccountType.valueOf(result.getInt("accountType")).name());
						put("created",result.getString("created"));
					}};
					users.add(user);
				}
			}
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}
		return users;
	}
	
}
