package controllers;

import helper.DCON;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.apache.commons.collections.map.HashedMap;

import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.Util;

public class Shopify extends Controller{

	public static void migration(@Required String migrate_key){ 
		List<Map<String, Object>> migrates = new LinkedList<Map<String, Object>>();
		if(!validation.hasErrors() && migrate_key.equals(play.Play.configuration.getProperty("dropifi.admin.password"))){
			play.Logger.log4j.info("Migrate_key:" + migrate_key);
			migrates =performMigration();
		} 
		
		render(migrates); 
	}
	
	public static void migrateAPI(@Required String migrate_key){
		migration(migrate_key);
	}
	
	@Util
	public static List<Map<String, Object>> performMigration(){
		List<Map<String, Object>> migrates = new LinkedList<Map<String, Object>>();
		Connection conn=null;
		try{
			conn = DCON.getDefaultConnection();
			PreparedStatement query = conn.prepareStatement("Select apikey, eb_apikey, eb_secretKey, eb_name From Company Where eb_pluginType=2");
			
			ResultSet result = query.executeQuery();
			int counter=0;
			while(result.next()){
				try{ 
					String new_eb_apikey = play.libs.Codec.hexMD5(play.Play.configuration.getProperty("shopify.apikey")+result.getString("eb_apikey"));
					String apikey =result.getString("apikey");
					String eb_apikey = result.getString("eb_apikey");
					PreparedStatement uquery = conn.prepareStatement("Update Company Set eb_apikey=?, eb_secretKey=? Where eb_pluginType=2 And apikey =?");
					
					uquery.setString(1, new_eb_apikey);
					uquery.setString(2, eb_apikey);
					uquery.setString(3, apikey);
					counter += uquery.executeUpdate();
					
					Map<String,Object> migrate = new HashMap<String,Object>();
					migrate.put("apikey",apikey);
					migrate.put("eb_apikey",new_eb_apikey);
					migrate.put("eb_secretKey",eb_apikey);
					migrate.put("eb_name",result.getString("eb_name"));
					
					migrates.add(migrate);
				}catch(Exception e){
					play.Logger.log4j.info(e.getMessage(),e);
				}
				
			}
			play.Logger.log4j.info("Total Updates: "+ counter);
		}catch(Exception e){
			play.Logger.log4j.info(e.getMessage(),e);
		}finally{
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					play.Logger.log4j.info(e.getMessage(),e);
				}
			}
		}
		return migrates;
	}
}
