package controllers;

import java.util.List;

import models.addons.AddonPlatform;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import resources.addons.AddonType;

public class Addons extends Controller{
	@Before
	static void args(){
		ForceSSL.secureDevMode();
	}
	
	public static void index(){
		List<AddonPlatform> addons = AddonPlatform.findAllAddons();
		render(addons);
	}
	
	public static void createAddon(@Required String title,@Required String type, @Required String subscriptionPlan,
			@Required String description,@Required double price,String endpoint, String shortImage,String largeImage){
		 
		flash.put("title", title);
		flash.put("type", type); 
		flash.put("subscriptionPlan", subscriptionPlan);
		flash.put("description", description);
		flash.put("price", price);
		flash.put("endpoint", endpoint);
		flash.put("shortImage", shortImage);
		flash.put("largeImage", largeImage);
		
		if(validation.hasErrors()){  
    		flash.put("info", "Required fields not specified, ensure that all required fields are specified");
			index();
		}
		
		AddonType addonType= AddonType.valueOf(type);
		flash.put("typeValue", addonType);
		
		AddonPlatform platform = new AddonPlatform(subscriptionPlan, title, description, shortImage, largeImage, endpoint, price, addonType);
		try{
			if(platform.createAddon()){
				flash.put("info", "AddonPlatform "+title+" created successfully");
			} else {
				flash.put("info", "AddonPlatform "+title+" could not be created");
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			flash.put("info", e.getMessage());
			play.Logger.log4j.info(e.getMessage(),e);
		}
		
		index();
	}
	
	public static void updateAddon(boolean isKeyReset,@Required String platformKey,@Required String token,@Required String secretKey, @Required String title,
			@Required String type, @Required String subscriptionPlan,@Required String description,@Required double price,String endpoint, String shortImage,String largeImage,boolean activated){
		
		AddonPlatform addon = new AddonPlatform(platformKey,token,secretKey,subscriptionPlan, title, description, shortImage, largeImage, endpoint, price, AddonType.valueOf(type),activated);
		
		try {
			if(AddonPlatform.updateAll(addon, isKeyReset)){
				flash.put("info", "AddonPlatform "+title+" updated successfully");
			}else{
				flash.put("info", "AddonPlatform "+title+" could not be updated");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			flash.put("info", e.getMessage());
			play.Logger.log4j.info(e.getMessage(),e);
		}
		index();
	}
}
