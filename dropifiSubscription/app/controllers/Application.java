package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends Controller {

	@Before
	static void args(){
		ForceSSL.secureDevMode();
	}
	
    public static void index() {
        render();
    }

}