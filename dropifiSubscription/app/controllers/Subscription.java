package controllers;

import helper.APIKEY;

import java.util.List;
import java.util.Map;

import javax.naming.directory.NoSuchAttributeException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import models.dsubscription.SubCoupon;
import models.dsubscription.SubCustomer;
import models.dsubscription.SubPlan;
import models.dsubscription.SubService;
import models.dsubscription.SubServiceName;
import models.dsubscription.SubServiceType; 

import play.data.validation.Required;
import play.mvc.*;
import query.SubFinder;
import resources.com.AccountType;
 
public class Subscription extends Controller {
	
	@Before
	static void args(){
		ForceSSL.secureDevMode();
	}
	
    public static void index() {    	
    	List<SubPlan> subPlans = SubPlan.findAllPlans();
        render(subPlans);
    }
    
    public static void findPlan(@Required Integer type){
     
    	SubPlan subPlan = SubPlan.findByAccountType(AccountType.valueOf(type));
    	if(subPlan==null)
    		subPlan = new SubPlan();
    	
    	JsonObject json = new JsonObject();
    	Gson gson = new Gson();
    	json.add("subPlan", gson.toJsonTree(subPlan));    	
    	renderJSON(json.toString());
    }
    
    public static void createPlan(@Required Integer type, @Required Double amount, @Required Double yearlyAmount,String description,String additionalID){
    	String  info = "Over here";
    	flash.put("type", type);
    	flash.put("additionalID", additionalID);  
    	flash.put("amount", amount);
    	flash.put("yearlyAmount", yearlyAmount); 
    	flash.put("description", description);  
    	
    	if(validation.hasErrors()){
    		info = "The type , monthly and yearly amount of Subscription Plan is required"; 
    		flash.put("info", info);
    		index();
    	}
    	
    	AccountType accountType = AccountType.valueOf(type);
    	SubPlan plan = new SubPlan(accountType, amount,yearlyAmount,description);
    	
    	flash.put("typeValue", accountType.name()); 
    	flash.put("plan", plan.createSubPlan(APIKEY.StripeSecretKey,additionalID));   	    	    	
    	flash.put("info", info);
    	index();
    }
    
    public static void deletePlan(@Required Integer type,String additionalID){
    	String  info = "Over here";
    	flash.put("type", type); 
    	
    	if(validation.hasErrors()){
    		info = "The type , monthly and yearly amount of Subscription Plan is required"; 
    		flash.put("info", info);
    		index(); 
    	} 
    	
    	//play.Logger.log4j.info(type);
    	AccountType accountType = AccountType.valueOf(type);
    	flash.put("deleted", SubPlan.removePlan(accountType,APIKEY.StripeSecretKey,additionalID));
    	
    	index(); 
    }
    
    public static void createService(@Required Integer planType, @Required String serviceName,@Required String serviceType,
    		@Required Long volume,@Required boolean activated,@Required Integer duration,@Required boolean unlimited, String description){
    	
    	flash.put("planType", planType);
    	flash.put("serviceName", serviceName);
    	flash.put("serviceType", serviceType);
    	flash.put("volume", volume);
    	flash.put("activated", activated);
    	flash.put("duration", duration);
    	flash.put("unlimited", unlimited); 
    	
    	if(validation.hasErrors()){
    		flash.put("serviceInfo", "ensure that you specify values for all the fields");
    		index();
    	}
    	
    	//play.Logger.info("activated: "+ activated);
    	AccountType accountType = AccountType.valueOf(planType);
    	flash.put("planValue", accountType.name());
    	SubPlan foundPlan = SubPlan.findSubPlan(accountType);
    	
    	if(foundPlan == null) {
    		flash.put("serviceInfo", "Subscription Plan "+accountType.name() +" is not created"); 
        	index();     		 
    	} 
    	
    	SubServiceName serName = SubServiceName.valueOf(serviceName);
    	SubServiceType serType = SubServiceType.valueOf(serviceType);
    	SubService service = null;
    	try { 
			service = new SubService(serName,serType,volume,activated, duration,unlimited,description);
		} catch (NumberFormatException | NoSuchAttributeException e) {
			// TODO Auto-generated catch block
			flash.put("serviceInfo", e.getMessage()); 
	    	index();
		}    	
    	 
    	boolean serviceAdded = foundPlan.addSubService(service);     	
    	flash.put("serviceInfo",serviceAdded?"service created successfully":"service was not created");  
    	flash.keep();    	
    	index();
    }
    
    public static void updateService(@Required String accountType, @Required String serviceName,@Required String serviceType,
    		@Required Long volume,@Required boolean activated,@Required Integer duration,@Required boolean unlimited){
    	int status = 501;
    	if(!validation.hasErrors()){
    		try {
				status = SubService.updateService(accountType, serviceName, serviceType, volume, activated, duration, unlimited);
			} catch (NumberFormatException | NoSuchAttributeException e) {
				// TODO Auto-generated catch block
				play.Logger.log4j.error(e.getMessage(), e);
			}
    	} 
    	
		play.Logger.log4j.info("Service updated: "+status); 
    	index();
    }
    
    public static void users(){
    	List<Map<String,Object>> users = SubFinder.listOfUsers();
    	render(users);
    }

    public static void coupon(List<String> coupons){
    	render(coupons);
    }
    
    public static void generateCoupon(String coupon,int length){
    	coupon(SubCoupon.addCoupons(length, coupon,APIKEY.StripeSecretKey));
    }
    
}
