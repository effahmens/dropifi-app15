package controllers;

import java.util.List;

import models.pluginintegration.Integration;
import play.mvc.Controller;
import resources.pluginintegration.EcomBlogType;

public class Integrations extends Controller{

	public static void index(){
		List<Integration>integs = Integration.findAll();
		render(integs);  
	}
	
	public static void createIntegration(String ecomBlogType, boolean isAuthorized,boolean newCode){ 
		try{ new Integration(ecomBlogType, isAuthorized).createUpdate(newCode); 
		}catch(Exception e){ play.Logger.log4j.info(e.getMessage(),e); } 
		index();
	}
	
	public static void updateIntegration(String apikey,String authCode,String ecomBlogType, boolean isAuthorized,boolean newCode){
		try{ new Integration(apikey,authCode,ecomBlogType,isAuthorized).createUpdate(newCode);
		}catch(Exception e){ play.Logger.log4j.info(e.getMessage(),e);  } 
		index();
	}
	
	public static void deleteIntegration(String apikey){
		try{Integration.delete(apikey);}catch(Exception e){play.Logger.log4j.info(e.getMessage(),e); }
		index();
	}
}
