package controllers;
import play.Play;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Util; 

public class ForceSSL extends Controller{
	
	/** Called before every request to ensure that HTTPS is used. */
	@Util 
    public static void redirectToHttps() {
		//XForwardedProto
        //if it's not secure, but amazon has already done the SSL processing then it might actually be secure after all
		if (play.Play.mode.isProd()) {
			if(!request.secure && request.headers.get("x-forwarded-proto") != null) {
	            request.secure = request.headers.get("x-forwarded-proto").values.contains("https");        
	        }
	        
	       //redirect if it's not secure url; 
	        if (!request.secure){ 
	            String url = redirectHostHttps() + request.url;
	            request.secure = true;
	            redirect(url); 
	        }
		}else{
			secureDevMode(); 
		}
	}

    /** Renames the host to be https://, handles both Amazon and local testing. */
    @Util 
    public static String redirectHostHttps(){
        
    	if (play.Play.mode.isDev()) {
            String[] pieces = request.host.split(":"); 
            String httpsPort = (String) play.Play.configuration.get("http.port");
            return "https://" + pieces[0] + ":" + httpsPort;
        }else {
            if (!request.host.endsWith("www.dropifi.com")) {
            	return "https://" + request.host;
            } else {
            	return "https://www.dropifi.com";               
            }
        }
    	
    }  
    
    @Util
    public static void secureDevMode(){
    	//if(play.Play.mode.isDev()){
	    	String fpassword = flash.get("whenwekill");
			String fusername = flash.get("whenwedie");
							
			if(fpassword == null && fusername==null){
				
				flash.put("whenwekill", "women");
				flash.put("whenwedie", "asmen");
				unauthorized("Dropifi- Authentication");
			} else{
	
				flash.remove("whenwekill");
				flash.remove("whenwedie"); 
			
				String username = request.user;
				String password = request.password; 
				
				String adminName =Play.configuration.getProperty("dropifi.admin.username");  
				String adminPassword = Play.configuration.getProperty("dropifi.admin.password");
				
				if(username!=null && username.compareTo(adminName)==0 && password!=null && password.compareTo(adminPassword)==0){
					/*//report admin login by sending email and sms
					String mailName = DropifiTools.mailName; Play.configuration.getProperty("dropifi.mail.username");
					String mailEmail = "efpam2013@gmail.com";//DropifiTools.mailEmail; Play.configuration.getProperty("dropifi.mail.email");
					
					MailSerializer mail = new MailSerializer(mailName,mailEmail,"","Demo App", DropifiTools.reportAdminLogin(request.url,"Demo App", request.remoteAddress));								
					ActorRef mailActor = actorOf(MailingActor.class).start(); 
					mailActor.tell(mail);*/
				}else{
					unauthorized("Dropifi- Authentication");
				} 
			}
    	//}
    }
}
