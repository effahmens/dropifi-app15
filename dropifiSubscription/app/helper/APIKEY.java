package helper;

import play.Play;

public class APIKEY {
	
	public static final String StripeSecretKey =  Play.configuration.getProperty("stripe.secret.key");  
	public static final String StripePublisahblekey = Play.configuration.getProperty("stripe.publishable.key");
	
}
